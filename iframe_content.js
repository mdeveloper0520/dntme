/**
 * Blur Copyright (c) 2008-2015 by Abine, Inc. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Abine, Inc. ("Confidential Information"), subject
 * to the Non-Disclosure Agreement and/or License Agreement you entered
 * into with Abine. You shall use such Confidential Information only
 * in accordance with the terms of said Agreement(s). Abine makes
 * no representations or warranties about the suitability of the
 * software. The software is provided with ABSOLUTELY NO WARRANTY
 * and Abine will NOT BE LIABLE for ANY DAMAGES resulting from
 * the use of the software.
 *
 * Contact license@getabine.com with any license-related questions.
 *
 * https://www.abine.com
 * @license
 *
 */

;

// Chrome content script for iframes

(function() {

  // Mark the DOM so we know that we loaded this already
  var htmlEl = document.documentElement;
  if (htmlEl.attributes.idmeScript == "iframe_content.js" || htmlEl.attributes.idmeScript == "content.js") {
    return; // this script has already been injected
  }
  htmlEl.attributes.idmeScript = "iframe_content.js";

  // To start the application, we have to inject the real content script into every iframe.
  // Chrome provides no way to inject the script into a single iframe.
  // WTF CHROME?

  function startApplication() {
    htmlEl.attributes.injectedOnFocus = true;
    chrome.extension.sendRequest({
      eventName: "injectContentScript",
      payload: {
        file: 'content.js',
        allFrames: true,
        runAt: 'document_idle'
      }
    });
  }

  function isAnInput(el) {
    if (el.tagName === "INPUT") {
      var type = (el.getAttribute('type') || 'text').toLowerCase();
      if (type in {text:1,password:1,email:1,tel:1}) {
        return true
      }
    } else if (el.tagName === "SELECT") {
      return true;
    }
    return false;
  }

  function fieldFocusHandler(e) {
    if (isAnInput(e.target || e.srcElement)) {
      document.removeEventListener('focus', fieldFocusHandler, true);
      window.lastFieldFocused = e.target || e.srcElement;
      startApplication();
    }
  }

  function hasForms() {
    // If there is an active element (an input field with focus) then start the application immediately.
    if (document.activeElement && document.activeElement.tagName != "BODY")
      return true;
    // inject content script if there are more than 3 input fields
    var nodeList = document.querySelectorAll('input[type=text], input[type=password], input[type=number], select');
    if (nodeList.length >= 3) {
      return true;
    }
    return false;
  }

  // Are we in an iframe?
  if (document.defaultView != document.defaultView.top) {
    // check if iframe is cross-origin
    try {
      var frameElement = window.frameElement;
      var parentDoc = frameElement.ownerDocument;
      var topHost = parentDoc.location.host;

      if (window.top != window.parent) {
        throw "nested iframe cannot be process in TOP content script";
      }

      // prevent loading of content.js inside this same-origin frame
      htmlEl.attributes.idmeScript = "content.js";

      if (!frameElement.getAttribute('id')) {
        frameElement.setAttribute('id', 'mmFrameId_'+Math.random());
      }

      // process any forms within this frame (as this could be a dynamically inserted frame with forms
      chrome.extension.sendRequest({
        eventName: "processFormsInNewFrame",
        frame_id: frameElement.getAttribute('id')
      });

    } catch(e) {
      if (hasForms()) {
        startApplication();
      } else {
        // wait for a click on an input element.
        document.addEventListener('focus', fieldFocusHandler, true);
      }
    }
  }
})();
