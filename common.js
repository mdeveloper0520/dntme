/**
 * Blur Copyright (c) 2008-2015 by Abine, Inc. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Abine, Inc. ("Confidential Information"), subject
 * to the Non-Disclosure Agreement and/or License Agreement you entered
 * into with Abine. You shall use such Confidential Information only
 * in accordance with the terms of said Agreement(s). Abine makes
 * no representations or warranties about the suitability of the
 * software. The software is provided with ABSOLUTELY NO WARRANTY
 * and Abine will NOT BE LIABLE for ANY DAMAGES resulting from
 * the use of the software.
 *
 * Contact license@getabine.com with any license-related questions.
 *
 * https://www.abine.com
 * @license
 *
 */

;

// pre-processing directives
if (typeof LOG_FINEST === 'undefined') LOG_FINEST = true;
if (typeof LOG_DEBUG  === 'undefined') LOG_DEBUG = true;
if (typeof LOG_INFO   === 'undefined') LOG_INFO = true;
if (typeof LOG_WARN   === 'undefined') LOG_WARN = true;
if (typeof LOG_ERROR  === 'undefined') LOG_ERROR = true;
var ABINE_DNTME = ABINE_DNTME || {};

// For profiling startup performance :)
ABINE_DNTME.startTime = Date.now();

window.ABINE_DNTME = window.ABINE_DNTME || ABINE_DNTME;

define.unordered = true;

ABINE_DNTME.require = require;
ABINE_DNTME.define = define;

// stub for satisfying static jquery dependency.
// real unmodified jquery will be loaded in sandbox on Firefox and directly as content-script or background-script in chrome


ABINE_DNTME.define('jquery', ['abine/timer'], function (timer) {
  return $;
});


ABINE_DNTME.define('abine/baseConfig', [], function() {

  var Config = ABINE_DNTME.config = {
    version: "7.8.2448",
    buildNum: "2448.bd36dd2c5e1744dfe5b0004f60aef666aab2fd96",
    appName: "Blur",
    environment: "production",
    tags: ['chromestore'],
    crypt: true,
    silent: false,
    payments: true,
    browser: null,  // defined in browser specific config file
    serverHost: "mm.abine.com",
    webHost: "www.abine.com",
    webAppHost: "dnt.abine.com",
    mappingServerHost: "mapping.abine.com",
    phoneServerHost: "phone.abine.com",
    licenseServerHost: "license.abine.com",
    emailServerHost: "emails.abine.com",
    pushNotificationUrl: "https://push.abine.com",
    disposableDomain: "opayq.com",
    paymentsServerHost: "payments.abine.com",
    logLevel: "ERROR",
    pfVersion: ".790 Chrome development",
    mmVersion: "0.0.1 development",
    features: 'accounts,wallet,abinestore,shareWebsocket',
    buildTag: 'chromestore',
    stripe_key: 'pk_live_J13Da0TuELwopBJeF4HlBwU9',
    overriddenKeys: {}
  };

  Config.protocol = (ABINE_DNTME.config.environment === "development")?'http://':'https://';

  Config.tellWebapp = function(doc, tagChanged) {
    if (doc.documentElement.hasAttribute("abine_webapp")) {
      doc.documentElement.setAttribute("dnt", Config.version);
      doc.documentElement.setAttribute("dnt-features", Config.features);
      doc.documentElement.setAttribute("dnt-tag", Config.tags[0]);
      doc.documentElement.setAttribute("dnt-build-tag", Config.buildTag);

      if (tagChanged) {
        var event = doc.createEvent("Event");
        event.initEvent("data:tag:changed", true, true);
        doc.dispatchEvent(event);
      }
    }
  };

  Config.updateTag = function(tag, doc) {
    if (Config.tags[0] != tag) {
      Config.tags[0] = tag;
      if (doc) {
        Config.tellWebapp(doc, true);
      }
    }
  };

  return Config;

});




ABINE_DNTME.define('abine/config', ['abine/baseConfig'], function(Config) {

  Config.browser = 'Chrome';

  // without this login in webapp fails randomly
  Config.tellWebapp(document);

  return Config;
});

ABINE_DNTME.require('abine/config');
ABINE_DNTME.define("abine/cobranding", ['abine/config'], function (config) {

  var blur = {
    "rules.update.url": "http://www.abine.com/rules/update.php",

    // images
    "panel.logo.small": "/pages/images/2015panels/panel_b_logo.png",
    "panel.logo.big": "/pages/images/blur_logo_ext_window.png"
  };
  var zonealarm = {
    "rules.update.url": "http://zonealarm.abine.com/update/rules/update.php",

    // images
    "panel.logo.small": "/pages/images/zonealarm/logo-small.png",
    "panel.logo.big": "/pages/images/zonealarm/logo-big.png"

  };
  var samsung = {
    // images
    "panel.logo.small": "/pages/images/samsung/logo-small.png",
    "panel.logo.big": "/pages/images/samsung/logo-big.png"

  };
  var civic = {
    // images
    "panel.logo.small": "/pages/images/civic/logo-small.png",
    "panel.logo.big": "/pages/images/civic/logo-big.png"

  };


  var brands = {
    zonealarm: zonealarm,
    zonealarm_webapp: zonealarm,
    samsung: samsung,
    samsung_webapp: samsung,
    civic: civic,
    civic_webapp: civic
  };

  if (config.environment != 'production') {
    blur["rules.update.url"] =  "https://rules.abine.com/test-rules.json";
  }

  return {
    get : function(key) {
      if (config.tags[0] in brands && key in brands[config.tags[0]]) {
        return brands[config.tags[0]][key];
      }
      return blur[key];
    }
  };

});


// Firefox does not have global timer available,
// so we must have our own implementation,
// and it must work both in a plain html window, and chrome.

ABINE_DNTME.define("abine/timer",
    [],
    function() {

  var abineTimerId = 1, abineTimerMap = {};

  var ffSetTimeout = function(func, time) {
    var id = abineTimerId++;
    var timer = Cc["@mozilla.org/timer;1"].createInstance(Ci.nsITimer);
    abineTimerMap[id] = timer;
    timer.initWithCallback(function(){
        delete abineTimerMap[id];
        func();
      }, time, Ci.nsITimer.TYPE_ONE_SHOT);
    return id;
  };

  var ffClearTimer = function(id) {
    try {
      if (id in abineTimerMap){
        abineTimerMap[id].cancel();
        delete abineTimerMap[id];
      }
    } catch(e) {}
  };

  var ffSetInterval = function(func, time) {
    var id = abineTimerId++;
    var timer = Cc["@mozilla.org/timer;1"].createInstance(Ci.nsITimer);
    abineTimerMap[id] = timer;
    timer.initWithCallback(func, time, Ci.nsITimer.TYPE_REPEATING_SLACK);
    return id;
  };

  var timerAPI = {};

  if (typeof(window) != 'undefined' && typeof window.setTimeout == "function" ) {
    timerAPI.setTimeout = function() {
      if(ABINE_DNTME.log) LOG_FINEST && ABINE_DNTME.log.finest("setting timeout of " + arguments[1]);
      return window.setTimeout.apply(window, arguments);
    }
  } else {
    timerAPI.setTimeout = ffSetTimeout;
  }

  if (typeof(window) != 'undefined' && typeof window.clearTimeout == "function" ) {
    timerAPI.clearTimeout = function() {
      window.clearTimeout.apply(window, arguments);
    }
  } else {
    timerAPI.clearTimeout = ffClearTimer;
  }

  if (typeof(window) != 'undefined' && typeof window.setInterval == "function" ) {
    timerAPI.setInterval = function() {
      return window.setInterval.apply(window, arguments);
    }
  } else {
    timerAPI.setInterval = ffSetInterval;
  }

  if (typeof(window) != 'undefined' && typeof window.clearInterval == "function" ) {
    timerAPI.clearInterval = function() {
      window.clearInterval.apply(window, arguments);
    }
  } else {
    timerAPI.clearInterval = ffClearTimer;
  }

  return timerAPI;

  // wot a pain

});




ABINE_DNTME.define('documentcloud/underscore', function(){return _;});
// async module for underscore.

ABINE_DNTME.define('documentcloud/underscore/async',
    ['documentcloud/underscore', 'abine/timer'],
    function(_, abineTimer) {

  // Fix for firefox
  var setTimeout = abineTimer.setTimeout;

  // START ASYNC MODULE

  var async = {};

  // global on the server, window in the browser
  var asyncRoot = _,
      previous_async = _.async;


      asyncRoot.async = async;

  async.noConflict = function () {
      asyncRoot.async = previous_async;
      return async;
  };

  //// cross-browser compatiblity functions ////

  var _forEach = function (arr, iterator) {
      if (arr.forEach) {
          return arr.forEach(iterator);
      }
      for (var i = 0; i < arr.length; i += 1) {
          iterator(arr[i], i, arr);
      }
  };

  var _map = function (arr, iterator) {
      if (arr.map) {
          return arr.map(iterator);
      }
      var results = [];
      _forEach(arr, function (x, i, a) {
          results.push(iterator(x, i, a));
      });
      return results;
  };

  var _reduce = function (arr, iterator, memo) {
      if (arr.reduce) {
          return arr.reduce(iterator, memo);
      }
      _forEach(arr, function (x, i, a) {
          memo = iterator(memo, x, i, a);
      });
      return memo;
  };

  var _keys = function (obj) {
      if (Object.keys) {
          return Object.keys(obj);
      }
      var keys = [];
      for (var k in obj) {
          if (obj.hasOwnProperty(k)) {
              keys.push(k);
          }
      }
      return keys;
  };

  //// exported async module functions ////

  //// nextTick implementation with browser-compatible fallback ////
  if (typeof process === 'undefined' || !(process.nextTick)) {
      async.nextTick = function (fn) {
          setTimeout(fn, 0);
      };
  }
  else {
      async.nextTick = process.nextTick;
  }

  async.forEach = function (arr, iterator, callback) {
      callback = callback || function () {};
      if (!arr.length) {
          return callback();
      }
      var completed = 0;
      _forEach(arr, function (x) {
          iterator(x, function (err) {
              if (err) {
                  callback(err);
                  callback = function () {};
              }
              else {
                  completed += 1;
                  if (completed === arr.length) {
                      callback(null);
                  }
              }
          });
      });
  };

  async.forEachSeries = function (arr, iterator, callback) {
      callback = callback || function () {};
      if (!arr.length) {
          return callback();
      }
      var completed = 0;
      var iterate = function () {
          iterator(arr[completed], function (err) {
              if (err) {
                  callback(err);
                  callback = function () {};
              }
              else {
                  completed += 1;
                  if (completed === arr.length) {
                      callback(null);
                  }
                  else {
                      iterate();
                  }
              }
          });
      };
      iterate();
  };

  async.forEachLimit = function (arr, limit, iterator, callback) {
      callback = callback || function () {};
      if (!arr.length || limit <= 0) {
          return callback();
      }
      var completed = 0;
      var started = 0;
      var running = 0;

      (function replenish () {
          if (completed === arr.length) {
              return callback();
          }

          while (running < limit && started < arr.length) {
              started += 1;
              running += 1;
              iterator(arr[started - 1], function (err) {
                  if (err) {
                      callback(err);
                      callback = function () {};
                  }
                  else {
                      completed += 1;
                      running -= 1;
                      if (completed === arr.length) {
                          callback();
                      }
                      else {
                          replenish();
                      }
                  }
              });
          }
      })();
  };


  var doParallel = function (fn) {
      return function () {
          var args = Array.prototype.slice.call(arguments);
          return fn.apply(null, [async.forEach].concat(args));
      };
  };
  var doSeries = function (fn) {
      return function () {
          var args = Array.prototype.slice.call(arguments);
          return fn.apply(null, [async.forEachSeries].concat(args));
      };
  };


  var _asyncMap = function (eachfn, arr, iterator, callback) {
      var results = [];
      arr = _map(arr, function (x, i) {
          return {index: i, value: x};
      });
      eachfn(arr, function (x, callback) {
          iterator(x.value, function (err, v) {
              results[x.index] = v;
              callback(err);
          });
      }, function (err) {
          callback(err, results);
      });
  };
  async.map = doParallel(_asyncMap);
  async.mapSeries = doSeries(_asyncMap);


  // reduce only has a series version, as doing reduce in parallel won't
  // work in many situations.
  async.reduce = function (arr, memo, iterator, callback) {
      async.forEachSeries(arr, function (x, callback) {
          iterator(memo, x, function (err, v) {
              memo = v;
              callback(err);
          });
      }, function (err) {
          callback(err, memo);
      });
  };
  // inject alias
  async.inject = async.reduce;
  // foldl alias
  async.foldl = async.reduce;

  async.reduceRight = function (arr, memo, iterator, callback) {
      var reversed = _map(arr, function (x) {
          return x;
      }).reverse();
      async.reduce(reversed, memo, iterator, callback);
  };
  // foldr alias
  async.foldr = async.reduceRight;

  var _filter = function (eachfn, arr, iterator, callback) {
      var results = [];
      arr = _map(arr, function (x, i) {
          return {index: i, value: x};
      });
      eachfn(arr, function (x, callback) {
          iterator(x.value, function (v) {
              if (v) {
                  results.push(x);
              }
              callback();
          });
      }, function (err) {
          callback(_map(results.sort(function (a, b) {
              return a.index - b.index;
          }), function (x) {
              return x.value;
          }));
      });
  };
  async.filter = doParallel(_filter);
  async.filterSeries = doSeries(_filter);
  // select alias
  async.select = async.filter;
  async.selectSeries = async.filterSeries;

  var _reject = function (eachfn, arr, iterator, callback) {
      var results = [];
      arr = _map(arr, function (x, i) {
          return {index: i, value: x};
      });
      eachfn(arr, function (x, callback) {
          iterator(x.value, function (v) {
              if (!v) {
                  results.push(x);
              }
              callback();
          });
      }, function (err) {
          callback(_map(results.sort(function (a, b) {
              return a.index - b.index;
          }), function (x) {
              return x.value;
          }));
      });
  };
  async.reject = doParallel(_reject);
  async.rejectSeries = doSeries(_reject);

  var _detect = function (eachfn, arr, iterator, main_callback) {
      eachfn(arr, function (x, callback) {
          iterator(x, function (result) {
              if (result) {
                  main_callback(x);
                  main_callback = function () {};
              }
              else {
                  callback();
              }
          });
      }, function (err) {
          main_callback();
      });
  };
  async.detect = doParallel(_detect);
  async.detectSeries = doSeries(_detect);

  async.some = function (arr, iterator, main_callback) {
      async.forEach(arr, function (x, callback) {
          iterator(x, function (v) {
              if (v) {
                  main_callback(true);
                  main_callback = function () {};
              }
              callback();
          });
      }, function (err) {
          main_callback(false);
      });
  };
  // any alias
  async.any = async.some;

  async.every = function (arr, iterator, main_callback) {
      async.forEach(arr, function (x, callback) {
          iterator(x, function (v) {
              if (!v) {
                  main_callback(false);
                  main_callback = function () {};
              }
              callback();
          });
      }, function (err) {
          main_callback(true);
      });
  };
  // all alias
  async.all = async.every;

  async.sortBy = function (arr, iterator, callback) {
      async.map(arr, function (x, callback) {
          iterator(x, function (err, criteria) {
              if (err) {
                  callback(err);
              }
              else {
                  callback(null, {value: x, criteria: criteria});
              }
          });
      }, function (err, results) {
          if (err) {
              return callback(err);
          }
          else {
              var fn = function (left, right) {
                  var a = left.criteria, b = right.criteria;
                  return a < b ? -1 : a > b ? 1 : 0;
              };
              callback(null, _map(results.sort(fn), function (x) {
                  return x.value;
              }));
          }
      });
  };

  async.auto = function (tasks, callback) {
      callback = callback || function () {};
      var keys = _keys(tasks);
      if (!keys.length) {
          return callback(null);
      }

      var results = {};

      var listeners = [];
      var addListener = function (fn) {
          listeners.unshift(fn);
      };
      var removeListener = function (fn) {
          for (var i = 0; i < listeners.length; i += 1) {
              if (listeners[i] === fn) {
                  listeners.splice(i, 1);
                  return;
              }
          }
      };
      var taskComplete = function () {
          _forEach(listeners.slice(0), function (fn) {
              fn();
          });
      };

      addListener(function () {
          if (_keys(results).length === keys.length) {
              callback(null, results);
              callback = function () {};
          }
      });

      _forEach(keys, function (k) {
          var task = (tasks[k] instanceof Function) ? [tasks[k]]: tasks[k];
          var taskCallback = function (err) {
              if (err) {
                  callback(err);
                  // stop subsequent errors hitting callback multiple times
                  callback = function () {};
              }
              else {
                  var args = Array.prototype.slice.call(arguments, 1);
                  if (args.length <= 1) {
                      args = args[0];
                  }
                  results[k] = args;
                  taskComplete();
              }
          };
          var requires = task.slice(0, Math.abs(task.length - 1)) || [];
          var ready = function () {
              return _reduce(requires, function (a, x) {
                  return (a && results.hasOwnProperty(x));
              }, true) && !results.hasOwnProperty(k);
          };
          if (ready()) {
              task[task.length - 1](taskCallback, results);
          }
          else {
              var listener = function () {
                  if (ready()) {
                      removeListener(listener);
                      task[task.length - 1](taskCallback, results);
                  }
              };
              addListener(listener);
          }
      });
  };

  async.waterfall = function (tasks, callback) {
      callback = callback || function () {};
      if (!tasks.length) {
          return callback();
      }
      var wrapIterator = function (iterator) {
          return function (err) {
              if (err) {
                  callback(err);
                  callback = function () {};
              }
              else {
                  var args = Array.prototype.slice.call(arguments, 1);
                  var next = iterator.next();
                  if (next) {
                      args.push(wrapIterator(next));
                  }
                  else {
                      args.push(callback);
                  }
                  async.nextTick(function () {
                      iterator.apply(null, args);
                  });
              }
          };
      };
      wrapIterator(async.iterator(tasks))();
  };

  async.parallel = function (tasks, callback) {
      callback = callback || function () {};
      if (tasks.constructor === Array) {
          async.map(tasks, function (fn, callback) {
              if (fn) {
                  fn(function (err) {
                      var args = Array.prototype.slice.call(arguments, 1);
                      if (args.length <= 1) {
                          args = args[0];
                      }
                      callback.call(null, err, args);
                  });
              }
          }, callback);
      }
      else {
          var results = {};
          async.forEach(_keys(tasks), function (k, callback) {
              tasks[k](function (err) {
                  var args = Array.prototype.slice.call(arguments, 1);
                  if (args.length <= 1) {
                      args = args[0];
                  }
                  results[k] = args;
                  callback(err);
              });
          }, function (err) {
              callback(err, results);
          });
      }
  };

  async.series = function (tasks, callback) {
      callback = callback || function () {};
      if (tasks.constructor === Array) {
          async.mapSeries(tasks, function (fn, callback) {
              if (fn) {
                  fn(function (err) {
                      var args = Array.prototype.slice.call(arguments, 1);
                      if (args.length <= 1) {
                          args = args[0];
                      }
                      callback.call(null, err, args);
                  });
              }
          }, callback);
      }
      else {
          var results = {};
          async.forEachSeries(_keys(tasks), function (k, callback) {
              tasks[k](function (err) {
                  var args = Array.prototype.slice.call(arguments, 1);
                  if (args.length <= 1) {
                      args = args[0];
                  }
                  results[k] = args;
                  callback(err);
              });
          }, function (err) {
              callback(err, results);
          });
      }
  };

  async.iterator = function (tasks) {
      var makeCallback = function (index) {
          var fn = function () {
              if (tasks.length) {
                  tasks[index].apply(null, arguments);
              }
              return fn.next();
          };
          fn.next = function () {
              return (index < tasks.length - 1) ? makeCallback(index + 1): null;
          };
          return fn;
      };
      return makeCallback(0);
  };

  async.apply = function (fn) {
      var args = Array.prototype.slice.call(arguments, 1);
      return function () {
          return fn.apply(
              null, args.concat(Array.prototype.slice.call(arguments))
          );
      };
  };

  var _concat = function (eachfn, arr, fn, callback) {
      var r = [];
      eachfn(arr, function (x, cb) {
          fn(x, function (err, y) {
              r = r.concat(y || []);
              cb(err);
          });
      }, function (err) {
          callback(err, r);
      });
  };
  async.concat = doParallel(_concat);
  async.concatSeries = doSeries(_concat);

  async.whilst = function (test, iterator, callback) {
      if (test()) {
          iterator(function (err) {
              if (err) {
                  return callback(err);
              }
              async.whilst(test, iterator, callback);
          });
      }
      else {
          callback();
      }
  };

  async.until = function (test, iterator, callback) {
      if (!test()) {
          iterator(function (err) {
              if (err) {
                  return callback(err);
              }
              async.until(test, iterator, callback);
          });
      }
      else {
          callback();
      }
  };

  async.queue = function (worker, concurrency) {
      var workers = 0;
      var q = {
          tasks: [],
          concurrency: concurrency,
          saturated: null,
          empty: null,
          drain: null,
          push: function (data, callback) {
              if(data.constructor !== Array) {
                  data = [data];
              }
              _forEach(data, function(task) {
                  q.tasks.push({
                      data: task,
                      callback: typeof callback === 'function' ? callback : null
                  });
                  if (q.saturated && q.tasks.length == concurrency) {
                      q.saturated();
                  }
                  async.nextTick(q.process);
              });
          },
          process: function () {
              if (workers < q.concurrency && q.tasks.length) {
                  var task = q.tasks.shift();
                  if(q.empty && q.tasks.length == 0) q.empty();
                  workers += 1;
                  worker(task.data, function () {
                      workers -= 1;
                      if (task.callback) {
                          task.callback.apply(task, arguments);
                      }
                      if(q.drain && q.tasks.length + workers == 0) q.drain();
                      q.process();
                  });
              }
          },
          length: function () {
              return q.tasks.length;
          },
          running: function () {
              return workers;
          }
      };
      return q;
  };

  var _console_fn = function (name) {
      return function (fn) {
          var args = Array.prototype.slice.call(arguments, 1);
          fn.apply(null, args.concat([function (err) {
              var args = Array.prototype.slice.call(arguments, 1);
              if (typeof console !== 'undefined') {
                  if (err) {
                      if (console.error) {
                          console.error(err);
                      }
                  }
                  else if (console[name]) {
                      _forEach(args, function (x) {
                          console[name](x);
                      });
                  }
              }
          }]));
      };
  };
  async.log = _console_fn('log');
  async.dir = _console_fn('dir');
  /*async.info = _console_fn('info');
  async.warn = _console_fn('warn');
  async.error = _console_fn('error');*/

  async.memoize = function (fn, hasher) {
      var memo = {};
      var queues = {};
      hasher = hasher || function (x) {
          return x;
      };
      var memoized = function () {
          var args = Array.prototype.slice.call(arguments);
          var callback = args.pop();
          var key = hasher.apply(null, args);
          if (key in memo) {
              callback.apply(null, memo[key]);
          }
          else if (key in queues) {
              queues[key].push(callback);
          }
          else {
              queues[key] = [callback];
              fn.apply(null, args.concat([function () {
                  memo[key] = arguments;
                  var q = queues[key];
                  delete queues[key];
                  for (var i = 0, l = q.length; i < l; i++) {
                    q[i].apply(null, arguments);
                  }
              }]));
          }
      };
      memoized.unmemoized = fn;
      return memoized;
  };

  async.unmemoize = function (fn) {
    return function () {
      return (fn.unmemoized || fn).apply(null, arguments);
    };
  };
  // END ASYNC MODULE

  // Return the defined module
  return _

});

ABINE_DNTME.require(['documentcloud/underscore/async'], function(){});
ABINE_DNTME.define("abine/eventLogger", ['abine/config'], function (config) {

  var FINEST = 0
  var DEBUG = 1;
  var INFO = 2;
  var WARN = 3;
  var ERROR = 4;
  var NONE = 5;

  var noOp = function(){};

  function errorStringToJSON(errorString){
    var error = {}; // JSON representation

    var errorArray = errorString && errorString.split? errorString.split("#"):[];

    // parse the error string [ERROR HANDLER]#[abine/models/user]#[loadShieldedPhones]# error event in this.shieldedPhones.fetchAll
    if(errorArray.length == 4){
      error.error_tag = errorArray[0];
      error.module = errorArray[1];
      error.method = errorArray[2];
      error.message = errorArray[3];
    } else {
      error.error_tag = error.module = error.method = "not well-formed";
      // convert input to string. without this you will get infinite recursion of buildParams
      // inside jquery.ajax when input is a DOM object
      error.message = errorString+"";
    }

    // get information from static config options
    error.browser = config.browser;
    error.build = config.buildNum;
    error.environment = config.environment;
    error.build_tag = config.tags[0];
    error.server_host = config.serverHost;
    error.mapping_server_host = config.mappingServerHost;
    error.license_server_host = config.licenseServerHost;
    error.phone_server_host = config.phoneServerHost;
    error.crypt = config.crypt;
    error.version = config.version;

    // get the user agent string to ID the browser version
    error.user_agent = ABINE_DNTME.userAgent;

    return error;
  }

  function getConsoleService() {
    if (typeof console == "undefined" || typeof(console.debug) == 'undefined' || ABINE_DNTME.IE_BG) {
      function ConsoleWrap(msg) {
        try{Console(msg);}catch(e){}
      }
      return {
        finest: function(msg) { ConsoleWrap(ABINE_DNTME.context+" [FINEST][" + new Date() + "]: " + msg); },
        debug:  function(msg) { ConsoleWrap(ABINE_DNTME.context+" [DEBUG][" + new Date() + "]: "  + msg);   },
        info:   function(msg) { ConsoleWrap(ABINE_DNTME.context+" [INFO][" + new Date() + "]: "  + msg);    },
        warn:   function(msg) { ConsoleWrap(ABINE_DNTME.context+" [WARNING][" + new Date() + "]: "  + msg); },
        error:  function(msg) { ConsoleWrap(ABINE_DNTME.context+" [ERROR][" + new Date() + "]: "  + msg); }
      }
    } else if (typeof console != "undefined") {
      return {
        finest: function(msg) { console.debug(ABINE_DNTME.context+" [FINEST][" + new Date() + "]: " + msg); },
        debug:  function(msg) { console.debug(ABINE_DNTME.context+" [DEBUG][" + new Date() + "]: "  + msg);   },
        info:   function(msg) { console.info(ABINE_DNTME.context+" [INFO][" + new Date() + "]: "  + msg);    },
        warn:   function(msg) { console.warn(ABINE_DNTME.context+" [WARNING][" + new Date() + "]: "  + msg); },
        error:  function(msg) { console.error(ABINE_DNTME.context+" [ERROR][" + new Date() + "]: "  + msg); }
      }
    }
  }

  var EventLogger = getConsoleService();

  EventLogger.dumpObj = function(obj){ // http://www.sitepoint.com/javascript-json-serialization/ - thanks, bro.
    var t = typeof (obj);
    if (t != "object" || obj === null) {
      if (t == "string") obj = '"'+obj+'"';
      return String(obj);
    }
    else {
      var n, v, json = [], arr = (obj && obj.constructor == Array);
      for (n in obj) {
        v = obj[n]; t = typeof(v);
        if (t == "string") v = '"'+v+'"';
        else if (t == "object" && v !== null) v = EventLogger.dumpObj(v);
        json.push((arr ? "" : '"' + n + '":') + String(v));
      }
      return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
    }
  };

  // intentionally not using BREAK in case statement to let it disable all logging below given level.
  switch (config.logLevel) {
    case "NONE": EventLogger.error = noOp;
    case "ERROR": EventLogger.warn = noOp;
    case "WARN": EventLogger.info = noOp;
    case "INFO": EventLogger.debug = noOp;
    case "DEBUG":EventLogger.finest = noOp;
  }

  EventLogger.stackTrace = function(msg) {
    msg = msg || '';
    try {does.not=exist}catch(e) {
      var stack = (e.stack+"").split('\n');
      if (stack.length > 3) {
        stack.shift();
        stack.shift();
        stack.shift();
      }
      msg += "\n" + stack.join('\n');
    }
    this.debug(msg);
  };

  // for easy access in all places
  ABINE_DNTME.log = ABINE_DNTME.log || EventLogger;

  return EventLogger;

});










