/**
 * Blur Copyright (c) 2008-2015 by Abine, Inc. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Abine, Inc. ("Confidential Information"), subject
 * to the Non-Disclosure Agreement and/or License Agreement you entered
 * into with Abine. You shall use such Confidential Information only
 * in accordance with the terms of said Agreement(s). Abine makes
 * no representations or warranties about the suitability of the
 * software. The software is provided with ABSOLUTELY NO WARRANTY
 * and Abine will NOT BE LIABLE for ANY DAMAGES resulting from
 * the use of the software.
 *
 * Contact license@getabine.com with any license-related questions.
 *
 * https://www.abine.com
 * @license
 *
 */

;
(function(){
  var pageUrl = window.location.href;
  if (!(window.top == window) || !pageUrl.match(/dntmeFullTour=|dntmeTour=/)) {
    return;
  }
  var frameId = 'mmPostInstallFlow';
  var iframe = document.createElement('iframe');
  iframe.style.opacity = '0';

  var overlayDiv = document.createElement('div');
  overlayDiv.className = 'dntmDimPostInstall';

  function closeTour(){
    iframe.className = 'mmFadeOut';
    if (closeDiv) {
      closeDiv.style.display = 'none';
    }
    if (arrowDiv) {
      arrowDiv.style.display = 'none';
    }
    overlayDiv.className = '';
    var nodesToRemove = [iframe, closeDiv, arrowDiv, overlayDiv];
    setTimeout(function(){
      for (var i=0;i<nodesToRemove.length;i++) {
        var node = nodesToRemove[i];
        if (node && node.parentNode) {
          node.parentNode.removeChild(node);
        }
      }
    }, 2000);
  }

  var arrowDiv = null, closeDiv = null;

  var frameAttrs = {id: frameId, scrolling:'no', allowTransparency:'true', frameborder:'0'};

  var clientHeight = document.documentElement.clientHeight;
  var clientWidth = document.documentElement.clientWidth;

  if (pageUrl.indexOf('dntmeFullTour') != -1) {
    var idx = pageUrl.indexOf('dntmeFullTour')+14;
    var len = pageUrl.length-idx;
    if (pageUrl.indexOf('&', idx) != -1) {
      len = pageUrl.indexOf('&', idx)-idx;
    }
    frameAttrs['src'] = pageUrl.substr(idx, len)+'?domain='+window.location.host+'&w='+ clientWidth+'&h='+ clientHeight;
    frameAttrs['top'] = (-clientHeight)+'px';
    frameAttrs['height'] = (clientHeight)+'px';
    frameAttrs['id'] = frameId+'Full';
  } else {
    frameAttrs['src'] = pageUrl.substr(pageUrl.indexOf('dntmeTour')+10)+'?domain='+window.location.host;
    arrowDiv = document.createElement('div');
    arrowDiv.id = 'mmPostInstallArrow';
    arrowDiv.style.display = 'none';

    closeDiv = document.createElement('div');
    closeDiv.innerHTML = 'CLOSE &nbsp;x';
    closeDiv.id = 'mmPostInstallClose';
    closeDiv.style.opacity = '0';
    closeDiv.onclick = closeTour;
  }

  for (var i in frameAttrs) {
    iframe.setAttribute(i, frameAttrs[i]);
  }

  iframe.style.width = (clientWidth)+'px';

  if (arrowDiv) {
    document.documentElement.insertBefore(arrowDiv, document.documentElement.firstChild);
  }
  document.documentElement.insertBefore(overlayDiv, document.documentElement.firstChild);
  document.documentElement.insertBefore(iframe, overlayDiv);
  if (closeDiv) {
    closeDiv.style.left = (clientWidth-80)+'px';
    document.documentElement.insertBefore(closeDiv, iframe);
  }

  setTimeout(function(){
    iframe.className = 'mmFadeIn';
    if (arrowDiv) {
      setTimeout(function(){arrowDiv.style.display = '';}, 1000);
    }
    if (closeDiv) {
      setTimeout(function(){closeDiv.className = 'show';}, 3000);
    }
  }, 1000);

  window.addEventListener("message", function(event){
    if (frameAttrs['src'].indexOf(event.origin) == 0) {
      if (event.data == 'close') {
        closeTour();
      }
    }
  }, false);
})();
