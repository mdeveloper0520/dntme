/**
 * Blur Copyright (c) 2008-2015 by Abine, Inc. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Abine, Inc. ("Confidential Information"), subject
 * to the Non-Disclosure Agreement and/or License Agreement you entered
 * into with Abine. You shall use such Confidential Information only
 * in accordance with the terms of said Agreement(s). Abine makes
 * no representations or warranties about the suitability of the
 * software. The software is provided with ABSOLUTELY NO WARRANTY
 * and Abine will NOT BE LIABLE for ANY DAMAGES resulting from
 * the use of the software.
 *
 * Contact license@getabine.com with any license-related questions.
 *
 * https://www.abine.com
 * @license
 *
 */

;
(function addPayButton(retry) {
    // var host = window.location.host, insertBefore, button, amount, mapping, merchantId, affiliate;
    // if (host.match(/^(www\.)?1800flowers.com$/) && window.location.protocol == 'https:') {
    //   insertBefore = $('#FDCreditCardDisplayPaypal');
    //   if ($('.BP-payment-details').length > 0 && insertBefore.length > 0) {
    //     affiliate = '1800flowers';
    //     merchantId = 'ZJfYTWbZ18IidiN';
    //     button = "<img src='https://s3.amazonaws.com/abine-static/images/icon-samsungpay.png' style='width: 200px;padding: 10px;'/>";
    //     amount = ($('.BP-total-amount .BP-col2').first().text() + '').replace(/[^0-9\.]+/g, '');
    //     mapping = {
    //       "address-first-name": "firstName",
    //       "address-last-name": "lastName",
    //       "address1": "address1",
    //       "address2": "address2",
    //       "city": "city",
    //       "state": "select[name=state]",
    //       "zipcode": "zipCode",
    //
    //       "card-number": "account",
    //       "expiry-month": "expire_month",
    //       "expiry-year": "expire_year",
    //       "card-name": "cc_nameoncard",
    //       "cvc": "securitycode"
    //     };
    //   }
    // } else if (host.match(/^(www-ssl\.)?bestbuy.com$/) && window.location.protocol == 'https:') {
    //   insertBefore = $('.place-order');
    //   if ($('#creditCard').length > 0 && insertBefore.length > 0) {
    //     affiliate = 'bestbuy';
    //     merchantId = 'mMxoqq3pBrXP8OW';
    //     button = "<img src='https://s3.amazonaws.com/abine-static/images/icon-samsungpay.png' style='width: 200px;padding:10px;'/>";
    //     amount = ($('.total .pull-right').text() + '').replace(/[^0-9\.]+/g, '');
    //     mapping = {
    //       "address-first-name": "firstName",
    //       "address-last-name": "lastName",
    //       "address1": "street",
    //       "address2": "street2",
    //       "city": "city",
    //       "state": "state",
    //       "zipcode": "zipcode",
    //
    //       "card-number": "creditCard",
    //       "expiry-month": "expirationMonth",
    //       "expiry-year": "expirationYear",
    //       "cvc": "securityCode"
    //     };
    //   }
    // } else if (host.match(/^www.walmart.com$/) && window.location.protocol == 'https:') {
    //   insertBefore = $('#COAC3PayReviewOrderBtn');
    //   if ($('#COAC3PayCardNumber').length > 0 && insertBefore.length > 0) {
    //     affiliate = 'walmart';
    //     merchantId = '6BDBdZVlPcZ4Eqd';
    //     button = "<img src='https://s3.amazonaws.com/abine-static/images/icon-samsungpay.png' style='width: 200px;padding:0px;'/>";
    //     amount = ($('.pos-grand-total-price').first().text() + '').replace(/[^0-9\.]+/g, '');
    //     mapping = {
    //       "address1": "COAC3PayBillAddrLine1",
    //       "address2": "COAC3PayBillAddrLine2",
    //       "city": "COAC3PayBillAddrCity",
    //       "state": "COAC3PayBillAddrState",
    //       "zipcode": "COAC3PayBillAddrZip",
    //
    //       "card-first-name": "COAC3PayCardFirstName",
    //       "card-last-name": "COAC3PayCardLastName",
    //       "card-number": "COAC3PayCardNumber",
    //       "expiry-month": "COAC3PayCardExpMnth",
    //       "expiry-year": "COAC3PayCardYear",
    //       "cvc": "COAC3PayCardSecCode"
    //     };
    //   }
    // } else if (host.match(/^www.kohls.com$/) && window.location.protocol == 'https:') {
    //   $('.button_continueToPayment').off('click.blur').on('click.blur', function(){
    //     clearTimeout(window.abinePayButtonTimer);
    //     retry = 5;
    //     window.abinePayButtonTimer = setTimeout(function(){addPayButton(retry-1)}, 1000);
    //   });
    //   insertBefore = $('.button_continueto_review_order');
    //   if ($('#payment_information').length > 0 && insertBefore.length > 0) {
    //     affiliate = 'kohls';
    //     merchantId = 'EEKCdZuNXbwo0tM';
    //     button = "<img src='https://s3.amazonaws.com/abine-static/images/icon-samsungpay.png' style='width: 200px;padding:0px;'/>";
    //     amount = ($('#totalcharges').first().text() + '').replace(/[^0-9\.]+/g, '');
    //     if (!amount) {
    //       merchantId = null;
    //     }
    //     mapping = {
    //       "address-first-name": "bill_fname",
    //       "address-last-name": "bill_lname",
    //       "address1": "bill_addr1",
    //       "address2": "bill_addr2",
    //       "city": "bill_city",
    //       "state": "billingGuestContinentalUSState",
    //       "zipcode": "bill_postal",
    //
    //       "card-type": "select.paymentCCType",
    //       "card-number": "payment_information",
    //       "expiry-month": "creditcard-month-select",
    //       "expiry-year": "creditcard-year-select",
    //       "cvc": "payment_information_ccv_hidden"
    //     };
    //   }
    // } else if (host.match(/^www-secure.target.com$/) && window.location.protocol == 'https:') {
    //   insertBefore = $('.cartOffers');
    //   if ($('#ccNumber').length > 0 && insertBefore.length > 0) {
    //     affiliate = 'walmart';
    //     merchantId = 'gZgtJ6fyAiryIG6';
    //     button = "<img src='https://s3.amazonaws.com/abine-static/images/icon-samsungpay.png' onclick='var x=document.getElementById(\"payWithCreditcard\");if (!x.checked) x.click()' style='width: 200px;padding:0px;'/>";
    //     amount = $('#grandTotal').val().replace(/[^0-9\.]+/g, '');
    //     mapping = {
    //       "card-name": "ccName",
    //       "card-number": "ccNumber",
    //       "expiry-month": "ccExpMonth",
    //       "expiry-year": "ccExpYear",
    //       "cvc": "cvv"
    //     };
    //   }
    // }
    //
    // if (!merchantId) {
    //   if (retry > 0) {
    //     clearTimeout(window.abinePayButtonTimer);
    //     window.abinePayButtonTimer = setTimeout(function(){addPayButton(retry-1)}, 1000);
    //   }
    //   if (!window.abineHashChangeHooked) {
    //     window.abineHashChangeHooked = true;
    //     window.addEventListener("hashchange", function(){
    //       clearTimeout(window.abinePayButtonTimer);
    //       retry = 5;
    //       window.abinePayButtonTimer = setTimeout(function(){addPayButton(retry-1)}, 1000);
    //     }, false);
    //   }
    //   return;
    // }
    //
    // var oldBtn = document.getElementById('abineWidgetTarget');
    // if (oldBtn) {
    //   oldBtn.parentNode.removeChild(oldBtn);
    // } else {
    //   ABINE_DNTME.require(['abine/url', 'abine/proxies'], function(url, proxies){
    //     proxies.licenseModule.logCheckout({method: 'button', merchant: url.getTLD(host), shown: 1, amount: amount});
    //   });
    // }
    // var html = '<a id="abineWidgetTarget" href="#" onclick="window.abineDNTMeWidget.go(this, \'' + affiliate + '\');return false;" style="display:none;">' + button + '</a>';
    // button = $(html)[0];
    // insertBefore[0].parentNode.insertBefore(button, insertBefore[0]);
    // var attrs = _.extend({
    //   "base-url": "https://dnt.abine.com/",
    //   "amount": amount,
    //   "merchant": merchantId
    // }, mapping);
    // _.each(attrs, function (value, name) {
    //   $(button).attr('data-' + name, value);
    // });
    // var me = document.getElementById("abineWidgetTarget"), id = 'abineDNTMeWidgetScript';
    // if (!document.getElementById(id)) {
    //   var s = document.createElement('script');
    //   s.setAttribute('id', id);
    //   s.setAttribute('src', attrs['base-url'] + 'assets/autofill-card-widget.js');
    //   s.addEventListener("load", function () {
    //     me.style.display = '';
    //   }, false);
    //   document.documentElement.appendChild(s);
    // } else {
    //   me.style.display = '';
    // }
  })(10);
