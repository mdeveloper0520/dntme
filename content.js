/**
 * Blur Copyright (c) 2008-2015 by Abine, Inc. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Abine, Inc. ("Confidential Information"), subject
 * to the Non-Disclosure Agreement and/or License Agreement you entered
 * into with Abine. You shall use such Confidential Information only
 * in accordance with the terms of said Agreement(s). Abine makes
 * no representations or warranties about the suitability of the
 * software. The software is provided with ABSOLUTELY NO WARRANTY
 * and Abine will NOT BE LIABLE for ANY DAMAGES resulting from
 * the use of the software.
 *
 * Contact license@getabine.com with any license-related questions.
 *
 * https://www.abine.com
 * @license
 *
 */

;
ABINE_DNTME.define("abine/contentManager",
  ["documentcloud/underscore", "jquery", "abine/core", "abine/contentMessenger", "abine/window",
    "abine/timer", "abine/formAnalyzer", "abine/formStrategy", "models", "abine/config",
    "abine/url", "abine/proxies", "abine/formSubmitContentHelper", "abine/filler"],
  function(_, $, abineCore, abineContentMessenger, abineWindow,
    abineTimer, abineFormAnalyzer, abineFormStrategy, models, Config,
    abineUrl, proxies, FormSubmitContentHelper, AbineFiller) {

    // Default CSS properties for new floating panels
    var PANEL_CSS_DEFAULTS = {
      zIndex: "2147483647",
      position: "absolute",
      overflow: "hidden",
      borderWidth: "0px",
      visibility: "visible",
      background: "transparent",
      backgroundColor: "transparent"
    };


    var CACHE_PANEL_FRAME = ABINE_DNTME.config.browser != 'Edge' && ABINE_DNTME.config.browser != 'Safari';

    // allow special inline form mappings for license platform / abine pages
    // currently only used to ignore reset password form on license platform
    if (Config.environment == 'development') {
      var allowedHostsRegex = /(^(.+\.)?abine\.com$)|(^(.+\.)?idme\.dev$)|(^(.+\.)?license-platform\.dev$)|(^(.+\.)?idme-platform\.dev$)|(^localhost(:[0-9]+)?$)/i;
    } else {
      var allowedHostsRegex = /^(.+\.)?abine\.com$/i;
    }

    function emptyCallback(){}


    var ContentManager = abineCore.BaseClass.extend({

      // ### function initialize(options)
      // - `document` - the DOM document to manage
      // - `force` - force content manager to init event if it is not the top document
      // - `noHelp` - do not create form helpers
      //
      // Instantiates a content manager for the given document.
      // Will abandon init if it's not the top document (ie iframe content) unless `force` is passed
      initialize: function(options) {
        this._locked = null;
        this.document = options.document;
        if (this.document.URL)
          this.href = this.document.URL;
        else
          this.href = (this.document.location) ? this.document.location.href : null;

        this.host = this.href.replace(/http[s]?:\/\//i,'').replace(/\/.*/i,'');
        this.domain = options.domain || ((this.href) ? abineUrl.getTLD(this.href) : null);

        Config.tellWebapp(this.document);

        this.topWindow = true;
        if (this.document.defaultView && this.document.defaultView != this.document.defaultView.top) {
          this.topWindow = false;
        }

        if (!options.force) {
          if (!this.topWindow) {
            LOG_FINEST && ABINE_DNTME.log.finest("Abandoning content manager init for: " + this.href);
            this.messenger = {send: function(){}};
            return; // abandon initialization if this is a child iframe
          }
        }

        this.messenger = ABINE_DNTME.contentMessenger || new abineContentMessenger.ContentMessenger({
          document: options.document
        });
        ABINE_DNTME.contentMessenger = this.messenger;

        this.hookToProxyContentManager();

        if (this.topWindow) {
          this.messenger.on('background:dntme:show', _.bind(this.showToolbarPanel, this));
          if (!this.document.defaultView) {
            // firefox about:* pages have defaultView null
            return;
          }
        }

        // ignore form submit events on our webapp
        if(!(this.document.documentElement && this.document.documentElement.hasAttribute('abine_webapp'))){
          this.formSubmitDetectorHelper = new FormSubmitContentHelper(this.messenger, this.document.defaultView);
        }

        proxies.connect(this.messenger, _.bind(function(tabId){this.tabId = tabId;}, this));

        proxies.securityManager.isMaskmeInstalled(_.bind(function(installed){

          // load any in-memory panel setting for this domain
          proxies.memoryStore.get('disabled.'+this.href.replace(/\?.*/, ''), _.bind(function(disabledOnUrl){
            proxies.memoryStore.get('disabled.'+this.domain, _.bind(function(disabledOnDomain){
              proxies.memoryStore.get('fieldIcons.'+this.tabId+'.'+this.domain, _.bind(function(showFieldIcons){
                this._disablePanels = disabledOnDomain || disabledOnUrl;
                this._showFieldIcons = showFieldIcons;
              }, this));
            }, this));
          }, this));

          if (installed) {
            this._disableFormHelpers = true;
            this.maskme_installed = true;
            this._locked = true;
            this.loadUserPreferences();
            this.trigger("ready");
          } else {
            this.messenger.on('get:form:ids', _.bind(function(panelId){this.getFormIds(panelId);}, this));
            if (this.topWindow) {
              this.messenger.on('disable:form:helpers', _.bind(this.disableFormHelpers, this));
              this.messenger.on('background:saveForm:notification', _.bind(this.showNotification, this));
              this.messenger.on('background:show:notification', _.bind(this.showNotification, this));
              this.messenger.on('background:show:notification:'+this.domain, _.bind(this.showNotification, this));
              this.messenger.on('background:create:panel', _.bind(this.createPanel, this));
              this.messenger.on('panel-as-growl', _.bind(this.createPanel, this));
              this.messenger.on('close-growl-panel', _.bind(this.closeCurrentPanel, this));

              this.messenger.on('update:tag', _.bind(function(tag){
                ABINE_DNTME.config.updateTag(tag, this.document);
              }, this));

              // this event is triggered only in chrome when a new same domain iframe is added to current page
              this.messenger.on('contentManager:processNewFrame', _.bind(function(frameId){
                var iframeEl = this.document.getElementById(frameId);
                if (iframeEl) {
                  var frameDoc = (iframeEl.contentWindow || iframeEl.contentDocument);
                  if (frameDoc) {
                    if(frameDoc.document) frameDoc = frameDoc.document;
                    this.setupFormsInFrame(frameDoc, iframeEl);
                  }
                }
              }, this));
            }

            // required as hooking to event handler too early will not generate mousedown/keydown events
            // which are required to detect userIntent.
            $(this.document).ready(_.bind(this.hookDocumentEventHandlers, this, this.document));

            this.lockedCB = _.bind(this.locked, this);
            this.unlockedCB = _.bind(this.unlocked, this);
            this.messenger.on('background:securityManager:locked', this.lockedCB);
            this.messenger.on('background:securityManager:unlocked', this.unlockedCB);

            proxies.securityManager.isLocked(_.bind(function(locked){
              this._locked = locked;

              if (this.domain) {
                this.messenger.send("contentscript:contextmenu:refresh",{domain:this.domain});
              }

              $(this.document).bind('contextmenu', _.bind(this.trackLastElementClicked, this));
              this.messenger.on("background:contextmenu:fill", _.bind(this.fillField,this));
              this.messenger.on("background:contextmenu:maskcard", _.bind(this.maskCardContextMenu, this));
              this.messenger.on("background:contextmenu:fillcard", _.bind(this.autofillCardContextMenu, this));
              this.messenger.on("background:contextmenu:mapform", _.bind(this.mapFormContextMenu, this));
              this.messenger.on("broadcast:preference:reload", _.bind(function(){this.loadUserPreferences()}, this));
              this.messenger.on("process:form", _.bind(this.processForm, this));

              ABINE_DNTME.contentuser = {
                hasRegEmail:_.bind(function(){
                  return this.preferences?this.preferences.hasPref("regEmail") : false
                },this)
              };

              this.trigger("ready");

              if (this.topWindow && this.href && this.href.match(/^http[s]?/)) {
                this.sendPageViewData();
              }
            }, this));
          }
        }, this));

        this.notifyInit();
      },

      getFormIds: function (panelId, retry) {
        var helpers = _.filter(this.formHelpers, function(helper){
          return abineFormAnalyzer.isElementInViewport(helper.formEl);
        });
        if (helpers.length > 0) {
          var ids = [];
          _.each(helpers, function(helper){ids.push(helper.id);});
          this.messenger.sendToCS(panelId, "form:ids:response", ids);
        } else if (!retry) {
          var self = this;
          this.startFormHelpers(function(){
            self.getFormIds(panelId, true)
          });
        } else {
          this.messenger.sendToCS(panelId, "form:ids:response", []);
        }
      },
      getPageText: function(doc) {
        var documentElement = doc.documentElement;
        if (documentElement && 'innerText' in documentElement) {
          return documentElement.innerText;
        }
        return $(doc).text() || "";
      },

      getAmount: function (doc) {
        try {
          doc = doc || this.document;
          var pageText = this.getPageText(doc);
          var amountRegex = /(total|amount[\s]+due|balance[\s]+due|price|you[\s]*will[\s]*be[\s]*charged|you'll pay|total to pay now|you will pay|total.*puchase[^\$]+|billed)[=:usd\s]*\$[\s]*([0-9\,\.]+([\s]+[0-9]{2})?)/ig;
          var matches = pageText.match(amountRegex);
          var amount = 0;
          amountRegex = new RegExp(amountRegex.source, "i");
          _.each(matches, function (match) {
            var match = (match + "").match(amountRegex);
            var amt = match[2].replace(/[\s]+/, '.');
            amt = parseFloat(amt.replace(/[^0-9\.]+/g, ''));
            if (!isNaN(amt) && amt > amount) {
              amount = amt;
            }
          });
          if (amount == 0) return null;
          return amount + "";
        } catch(e){}
        return null;
      },

      sendFormFillData: function (formStrategy) {
        // var pageDataType = null;
        // if (formStrategy.formType == abineFormAnalyzer.LOGIN_FORM) {
        //   pageDataType = 'login_fill';
        // } else if (formStrategy.formType == abineFormAnalyzer.REGISTRATION_FORM) {
        //   pageDataType = 'register_fill';
        // } else if (formStrategy.formType == abineFormAnalyzer.CHECKOUT_FORM) {
        //   pageDataType = 'checkout_fill';
        // }
        // this._formFillDataSent = this._formFillDataSent || {};
        // if (!this._formFillDataSent[pageDataType]) {
        //   this._formFillDataSent[pageDataType] = true;
        //   var pageData = new models.PageData();
        //   var data = {domain: this.domain};
        //   data[pageDataType] = true;
        //   pageData.save(data);
        // }
      },

      sendFormViewData: function (type) {
        // this._formFillDataSent = this._formFillDataSent || {};
        // if (!this._formFillDataSent[type]) {
        //   this._formFillDataSent[type] = true;
        //   var pageData = new models.PageData();
        //   var data = {domain: this.domain};
        //   data[type] = true;
        //   pageData.save(data);
        // }
      },

      sendPageViewData: function() {
        // var pageData = new models.PageData();
        // pageData.save({domain: this.domain, page_view: true});
      },

      hookToProxyContentManager: function() {
        var self = this;
        this.messenger.on('autoResizePanels', function(payload, sender){
          self.autoResizePanels(payload.className, payload.extraWidth, payload.extraHeight, payload.documentHeight, sender);
        });
        this.messenger.on('removePanels', function(payload){
          if (payload.strategyId) {
            var formStrategy = self.getFormStrategy(payload.strategyId);
            if (formStrategy && payload.fieldId) {
              var field = formStrategy.getField(payload.fieldId);
              if (field) {
                formStrategy.cleanupPanel(field.element);
              }
            }
          }
          var iframeId = payload ? payload.id : null;
          if (iframeId) {
            if (self._fieldPanelIframe && self._fieldPanelIframe.id == iframeId &&
              self._currentPanelField && self._currentPanelField.signature != payload.fieldId) {
              // do not remove panel if user moved to another field and we have to show panel for that field.
              return;
            }
            if (self._currentPanelIframe == iframeId){
              self._currentPanelField = null;
              self._currentPanelIframe = null;
            }
          }
          self.removePanels(iframeId);
        });
        this.messenger.on('cleanUpAfterPanelClosed', function(payload){
          var formStrategy = self.getFormStrategy(payload.strategyId);
          if (formStrategy) {
            var field = formStrategy.getField(payload.fieldId);
            formStrategy.cleanupPanel(field.element);
          }
        });
        this.messenger.on('ignoreBlur', function(payload){
          var formStrategy = self.getFormStrategy(payload.strategyId);
          if (formStrategy) {
            formStrategy.ignoreBlur(payload.fieldId, payload.value);
          }
        });
        this.messenger.on('showFieldIcon', function(payload){
          var formStrategy = self.getFormStrategy(payload.strategyId);
          if (formStrategy) {
            formStrategy.showFieldIcon(payload.fieldId, payload.value);
          }
        });
        this.messenger.on('focusField', function(payload){
          var formStrategy = self.getFormStrategy(payload.strategyId);
          if (formStrategy) {
            var field = formStrategy.getField(payload.fieldId);
            if (field) {
              LOG_FINEST && ABINE_DNTME.log.finest("show panel on "+payload.fieldId);
              abineTimer.setTimeout(function () {
                $(field.element).trigger('showpanel');
              }, 100);
            }
          }
        });
        this.messenger.on('addFieldBorder', function(payload){
          var formStrategy = self.getFormStrategy(payload.strategyId);
          if (formStrategy) {
            var field = formStrategy.getField(payload.fieldId);
            if (field) {
              if (!field.oldBorder) {
                field.oldBorder = field.element.style.border;
              }
              field.element.style.border = '2px solid red';
              field.element.scrollIntoView(false);
            }
          }
        });
        this.messenger.on('removeFieldBorder', function(payload){
          var formStrategy = self.getFormStrategy(payload.strategyId);
          if (formStrategy) {
            var field = formStrategy.getField(payload.fieldId);
            if (field) {
              field.element.style.border = field.oldBorder;
            }
          }
        });

        this.messenger.on('focusFieldWithIntent', function(payload){
          var formStrategy = self.getFormStrategy(payload.strategyId);
          if (formStrategy) {
            var field = formStrategy.getField(payload.fieldId);
            if (field) {
              LOG_FINEST && ABINE_DNTME.log.finest("focus field "+payload.fieldId);
              self.userIntent = true;
              field.element.scrollIntoView();
              field.element.blur();
              if (payload.params) {
                payload.params.frameId = payload.frameId;
                $(field.element).data('abine-params', payload.params);
              }
              abineTimer.setTimeout(function () {
                $(field.element).trigger('showpanel');
              }, 100);
            }
          }
        });

        this.messenger.on('showToolbarPanel', function(payload){
          self.showToolbarPanel(payload);
        });
        this.messenger.on('disablePanelOnThisPage', function(){
          self.disablePanelOnThisPage();
        });
        this.messenger.on('disablePanelOnThisSite', function(){
          self.disablePanelOnThisSite();
        });
        this.messenger.on('reloadPreference', function(){
          self.reloadPreference();
        });
        this.messenger.on('showFieldIconsOnThisSite', function(){
          self.showFieldIconsOnThisSite();
        });
        this.messenger.on('fillFields', function(payload, sender){
          var formStrategy = self.getFormStrategy(payload.strategyId);
          if (formStrategy) {
            self.sendFormFillData(formStrategy);
            formStrategy.ignoreBlur(payload.fieldId, true);
            if (payload.type == 'mapField') {
              self.mapField(formStrategy, payload.fieldId, payload.values);
            } else if (payload.type == 'mapForm') {
              self.mapForm(formStrategy, payload.fieldId, payload.values);
            } else {
              if (self._fieldPanelIframe) {
                try {
                  if (self._fieldPanelIframe.style.opacity != 1 || self._fieldPanelIframe.parentNode.style.opacity != 1) {
                    // do not fill as the site is trying to clickjack.
                    return;
                  }
                } catch (e){}
              }
              self.filler.fillFields(formStrategy, payload.type, payload.fieldId, payload.values, self.domain, payload.origin, function(){
                if (payload.type == 'login' && payload.origin != 'changepassword' &&
                  self.preferences.getPref("submitAccount") && // auto-submit not turned off globally
                  !(self.blacklist && !!self.blacklist.get('submit_account')) // auto-submit not turned off on this site
                ) {
                  var loginUrls = new models.LoginUrlCollection();
                  loginUrls.fetchAllByDomain(self.domain, function () {
                    var delay = 0;
                    if (payload.delay) {
                      // 3 second delay for biometric login approved panel
                      delay = 3000;
                    }
                    abineTimer.setTimeout(function(){self.filler.autoLogin(formStrategy.formEl, loginUrls);}, delay);
                  });
                }
              });
            }
            abineTimer.setTimeout(function(){formStrategy.ignoreBlur(payload.fieldId, false);}, 500);
          }
        });
        this.messenger.on('sendFeedbackEmail', function(payload, sender){
          self.sendFeedbackEmail(payload);
        });
        this.messenger.on('closePanelFromHeader', function(payload, sender){
          var formStrategy = self.getFormStrategy(payload.strategyId);
          if (formStrategy) {
            formStrategy.closePanelFromHeader(payload.fieldId);
          }
        });
        this.messenger.on('closeNotification', function(frameId, sender){
          self.closeNotification(frameId);
        });
        this.messenger.on('bindKeyEvents', function(payload, sender){
          self.bindKeyEvents(payload.frameId, payload.strategyId, payload.fieldId, payload.doNotCloseOnTyping);
        });
        this.messenger.on('getValue', function(payload, sender){
          self.getValue(payload);
        });
        this.messenger.on('panel:shown', function(payload, sender){
          if (self._panelName) {
            LOG_FINEST && ABINE_DNTME.log.finest("Time to show "+self._panelName+" is " + (Date.now()-self._panelStartTime) + "ms");
            delete self._panelName;
          }
        });
        this.messenger.on('showAutoFillWith', function(payload, sender){
          // delay required to let the regular panel close before showing auto-fill panel.
          abineTimer.setTimeout(function(){self.showAutoFillWith(payload);}, 1000);
        });
        this.messenger.on('showMapForm', function(payload, sender){
          self.showMapForm(payload);
        });
      },

      bindKeyEvents: function(frameId, strategyId, fieldId, doNotCloseOnTyping) {
        var formStrategy = this.getFormStrategy(strategyId);
        if (formStrategy) {
          var self = this;
          formStrategy.bindKeyEvents(fieldId, doNotCloseOnTyping, function(type){
            self.messenger.sendToCS(frameId, 'keyEvent-'+fieldId, type);
          });
        }
      },

      getValue: function(payload) {
        var response = null;
        if (payload.name == 'current.field.value') {
          var formStrategy = this.getFormStrategy(payload.strategyId);
          if (formStrategy) {
            response = formStrategy.getFieldValue(payload.fieldId);
          }
        } else if (payload.name == 'autofill.data') {
          var formStrategy = this.getFormStrategy(payload.strategyId);
          if (formStrategy) {
            response = formStrategy.getAutofillData();
          }
        }
        this.messenger.sendToCS(payload.frameId, payload.responseKey, response);
      },

      getFormStrategy: function(strategyId) {
        return _.find(this.formHelpers, function(helper){return helper.id == strategyId;});
      },

      sendFeedbackEmail: function(payload) {
        var formStrategy = this.getFormStrategy(payload.strategyId);
        if (formStrategy) {

          var fields = formStrategy.getFields();
          var field = formStrategy.getField(payload.fieldId);
          var thisField = field;
          var thisForm = formStrategy.formEl;
          var contentManager = this;

          var formData = {
            domain: contentManager.domain,
            signature: $(thisForm).data('mmSignature'),
            currentField: payload.fieldId
          };

          formData.mmFormType = $(thisForm).data('mmFormType');
          formData.elementType = thisForm.nodeName;
          formData.elementId = thisForm.getAttribute('name');
          formData.name = thisForm.getAttribute('name');
          formData.action = thisForm.getAttribute('action');
          formData.url = contentManager.href;
          formData.fields = [];
          formData.detected_locales =  abineFormAnalyzer.getDetectedLocales(thisForm).join(',');
          formData.formless_depth = $(thisForm).data('abineMaxDepth');
          formData.formType = formData.mmFormType;
          _.each(fields, function(field){
            formData.fields.push({
              signature: field.signature,
              dataType: field.dataType,
              label: field.label,
              elementId: field.id,
              fieldType: field.fieldType,
              rule_locale: field.matchedLocale,
              mmDataType: $(field.element).data('mmDetectType')
            });
          });

          var data = payload.data;
          data.extensionErrors = formData;
          proxies.licenseModule.sendFeedbackEmail(data, function(){});
        }
      },

      getCheckoutButtons: function() {
        if (!this._checkoutButtons) {
          var doc = this.document;
          var fillElement = $('#abineFillElement', doc);
          this._checkoutButtons = {
            amex: !!doc.querySelector("amex\\:init"),
            visa: !!doc.querySelector(".v-button"),
            chase: !!doc.querySelector(".jpmc-pwc-button"),
            amazon: !!fillElement.data("amazon"),
            paypal: !!fillElement.data("paypal"),
            master: !!fillElement.data("masterpass")
          };
        }
        return this._checkoutButtons;
      },

      mapField: function(formStrategy, currentFieldId, data, callback) {
        callback = callback || emptyCallback;
        var fields = formStrategy.getFields();
        var dataType = data.dataType;
        var formType = data.formType;

        var field = formStrategy.getField(currentFieldId);
        var element = field.element;
        var changed = true;

        // if field type matches old type OR detected type AND there was no change in form type
        if (!data.right_click && (dataType == field.dataType || dataType == $(element).data('mmDetectType')) &&
          (!formType || formType == this.formType)) {
          // no change in mapping
          changed = false;
        }

        var thisField = field;
        var thisForm = formStrategy.formEl;
        var contentManager = this;

        var newMapping = {
          domain: contentManager.domain,
          signature: $(thisForm).data('mmSignature'),
          right_click: !!data.right_click
        };

        if (changed) {
          newMapping.mmFormType = $(thisForm).data('mmFormType');
          newMapping.elementType = thisForm.nodeName;
          newMapping.elementId = thisForm.getAttribute('name');
          newMapping.name = thisForm.getAttribute('name');
          newMapping.action = thisForm.getAttribute('action');
          newMapping.detected_locales =  abineFormAnalyzer.getDetectedLocales(thisForm).join(',');
          newMapping.formless_depth = $(thisForm).data('abineMaxDepth');
          newMapping.url = contentManager.href;
          newMapping.fields = [];
          newMapping.formType = formType ? formType :  newMapping.mmFormType;
          _.each(fields, function(field){
            newMapping.fields.push({
              signature: field.signature,
              dataType: field.signature == thisField.signature && dataType?dataType:field.dataType,
              right_click: field.signature == thisField.signature?data.right_click:false,
              label: field.label,
              elementId: field.id,
              fieldType: field.fieldType,
              rule_locale: field.matchedLocale,
              mmDataType: $(field.element).data('mmDetectType')
            });
          });
        }

        var mappedForm = new models.MappedForm();
        mappedForm.save({strData: JSON.stringify(newMapping), changed: changed}, {
          success: _.bind(function(){
            contentManager.reloadFormHelper(thisForm, _.bind(function(){
              if (!data.noFocus) {
                abineTimer.setTimeout(function(){$(element).trigger('showpanel');}, 500);
              }
              var mapperMode = !!contentManager.preferences.getPref('mapping:mapper');
              if (!contentManager.isLocked()) {
                mapperMode = false;
              }
              if (mapperMode) {
                var self = this;
                self.filler.fillFields(formStrategy, 'address', currentFieldId, {address1: '280 Summer Street', address2: '#256', city: 'Boston', state: 'MA', country: 'US', zip: '02210'}, self.domain, null, function(){
                  self.filler.fillFields(formStrategy, 'payment', currentFieldId, {number: '4532185676188657', type: 'visa', first_name: 'John',  last_name: 'Smith', expiry_month: '10', expiry_year: '2020', cvc: '332'}, self.domain, null, function(){
                    self.filler.fillFields(formStrategy, 'phone', currentFieldId, {country_code: '1', number: '4445556666'}, self.domain, null, function(){
                      self.filler.fillFields(formStrategy, 'password', currentFieldId, 'somePassword', self.domain, null, function(){
                        self.filler.fillFields(formStrategy, 'email', currentFieldId, 'myemail@example.com', self.domain, null, function(){

                        });
                      });
                    });
                  });
                });
              }
              callback();
            }, this));
          }, this)
        });
      },

      mapForm: function(formStrategy, currentFieldId, data) {
        var fields = formStrategy.getFields();
        var formType = data.formType;
        var fieldTypes = data.fieldTypes;

        var thisField = formStrategy.getField(currentFieldId);
        if (!thisField) {
          thisField = _.find(fields, function(fld){return fld.fieldType == 'text';});
        }
        var thisForm = formStrategy.formEl;
        var contentManager = this;

        var mappingChanged = data.mappingChanged || formStrategy.formType != formType;

        var newMapping = {
          domain: contentManager.domain,
          signature: $(thisForm).data('mmSignature'),
          mmFormType: $(thisForm).data('mmFormType'),
          elementType: thisForm.nodeName,
          elementId: thisForm.getAttribute('name'),
          name: thisForm.getAttribute('name'),
          mapper: contentManager.preferences.getPref('mapping:mapper'),
          batch: contentManager.preferences.getPref('mapping:batch'),
          external_id: contentManager.preferences.getPref('mapping:form:id'),
          action: thisForm.getAttribute('action'),
          url: contentManager.href,
          fields: [],
          detected_locales:  abineFormAnalyzer.getDetectedLocales(thisForm).join(','),
          formless_depth: $(thisForm).data('abineMaxDepth'),
          formType: formType,
          verified: !!data.verified
        };

        _.each(fieldTypes, _.bind(function(dataType, signature){
          var field = fields[signature];
          var newField = {
            signature: signature,
            dataType: dataType,
            label: field.label,
            elementId: field.id,
            fieldType: field.fieldType,
            rule_locale: field.matchedLocale,
            mmDataType: $(field.element).data('mmDetectType')
          };
          newMapping.fields.push(newField);
          if (!mappingChanged) {
            mappingChanged = newField.dataType != field.dataType && newField.dataType != newField.mmDataType;
          }
        }, this));

        _.each(fields, function(field){
          if ('oldBorder' in field) {
            field.element.style.border = field.oldBorder;
            delete field.oldBorder;
          }
        });

        if (thisField) {
          var element = thisField.element;
          element.blur();
        }

        // always push mapping when its a mapper (even when there is no change)
        if (newMapping.mapper || contentManager.preferences.getPref('mapping:reviewer')) {
          mappingChanged = true;
        }
        var mappedForm = new models.MappedForm();
        var formHtml = null;
        if ('outerHTML' in thisForm) {
          formHtml = thisForm.outerHTML;
        }
        var formData = {
          strData: JSON.stringify(newMapping),
          changed: mappingChanged,
          formHtml: formHtml,
          formSignature: newMapping.signature
        };
        mappedForm.save(formData, {
          success: _.bind(function(){
            contentManager.reloadFormHelper(thisForm, function(){
              if (thisField) {
                abineTimer.setTimeout(function () {
                  $(element).trigger('showpanel');
                }, 500);
              }
            });
          }, this)
        });
      },

      processForm: function(data) {
        var parser = new DOMParser();
        var doc = parser.parseFromString('<html><body id="abFormBody">'+data.html+'</body></html>', "text/html");
        var form = $(doc.getElementById('abFormBody').firstChild);
        if (form[0].parentNode.childNodes.length > 1 && form[0].nodeName.toLowerCase() != 'form') {
          form = $(form[0].parentNode);
        }
        form.attr('page-url', data.mapping.url || 'http://'+data.mapping.host);

        if (form.length > 0) {
          this.formMappings = new models.MappedFormCollection({messenger: this.messenger});
          this.domain = data.mapping.domain;
          this.formsData = [];
          this.formHelpers = [];
          this.formMappings.freshen([new models.MappedForm(data.mapping)]);
          this.setupFormHelperForFormEl(form[0], this.formsData, _.bind(function(formType, fields){
            var newMapping = {
              mmFormType: form.data('mmFormType'),
              formType: formType?formType:form.data('mmFormType'),
              fields: []
            };
            _.each(fields, function(field){
              newMapping.fields.push({signature: field.signature, id: field.id, label: field.label, dataType: field.dataType, mmDataType: $(field.element).data('mmDetectType')});
            });
            delete form;
            delete doc;
            data.callback(newMapping);
          }, this), true);
          return;
        }
        data.callback(null);
      },

      maskCardContextMenu: function(fieldInfo){
        if (this.lastElementClicked) {
          var lastElement = this.lastElementClicked, field, strategyId = 'right-click';
          var formStrategy = _.find(this.formHelpers, function(helper){
            return !!helper.getField(lastElement);
          });

          if (formStrategy) {
            field = formStrategy.getField(lastElement);
            strategyId = formStrategy.id;
            if (field) {
              // save local mapping and push to server
              this.mapField(formStrategy, field.signature, {formType: 'checkoutForm', noFocus: true, right_click: true});
            }
          }
          // without this it will close panel on its own
          try{lastElement.blur();}catch(e){}

          if (!field) {
            field = {
              element: lastElement,
              dataType: "/financial/creditcard/number",
              signature: 'active-right-click'
            };
          }

          var blacklist = this.blacklist;
          var maskedCardsDisabled = !this.preferences.isPrefTrue("suggestProtectedCard") || (blacklist && !!blacklist.get("card"));
          var autoFillCardsDisabled = !this.preferences.isPrefTrue("suggestRealCard") || (blacklist && !!blacklist.get("real_card"));


          abineTimer.setTimeout(_.bind(function(){
            this.createPanel({
              style: {},
              secured: true,
              panelWidth: 450,
              field: field,
              view: 'payment',
              payload: {
                maskedCardsDisabled: maskedCardsDisabled,
                autoFillCardsDisabled: autoFillCardsDisabled,
                fieldType: field.dataType,
                paymentsr2: this.shouldShowCardAlertPanel(true),
                fromContextMenu: true,
                fieldId: field.signature,
                strategyId: strategyId
              }
            });
          }, this), 1000);
        }
      },

      autofillCardContextMenu: function(card){
        if (this.lastElementClicked) {
          var lastElement = this.lastElementClicked;
          var formStrategy = _.find(this.formHelpers, function(helper){
            return !!helper.getField(lastElement);
          });

          this.showNotification({
            msg: "",
            targetId: "",
            model: {maskedCard: card},
            height: 260,
            neverFade: true,
            type: 'real_card'
          });
        }
      },

      mapFormContextMenu: function(){
        var lastElementClicked = this.lastElementClicked;
        if (lastElementClicked) {
          var field;
          var formStrategy = _.find(this.formHelpers, function(helper){
            return !!helper.getField(lastElementClicked);
          });

          if (formStrategy) {
            field = formStrategy.getField(lastElementClicked);
          }
          if (field) {
            formStrategy.onUnknownFieldActivity({field: field});
          }
        }
      },

      showAutoFillWith: function(options) {
        if (this.preferences.getPref('mapping:mapper')) {
          this.showMapForm(options);
          return;
        }
        if (this.preferences.getPref('autofill:mapper')) {
          this.showAutoFill(options);
          return;
        }
        var strategy = this.getFormStrategy(options.strategyId);
        if (strategy) {
          var field = strategy.getField(options.fieldId);
          options.formType = strategy.formType;
          options.fieldType = field.dataType;
          this.createPanel({
            style: {},
            field: field,
            panelWidth: 450,
            view: 'autoFillWith',
            payload: options
          });
        }
      },

      getHelperForFormlessForm: function (el) {
        var formHelper = _(this.formHelpers).find(function (formHelper) {
          var node = el;
          while (node && node != formHelper.formEl) {
            node = node.parentNode;
          }
          if (formHelper.formEl == node) {
            return true;
          }
        });
        return formHelper;
      },

      getHelperForForm: function (form) {
        return _(this.formHelpers).find(function (formHelper) {
          if (formHelper.formEl == form) {
            return true;
          }
        });
      },

      removeFormHelper: function (formHelperOrForm) {
        var formHelper = _(this.formHelpers).find(function (fh) {
          return fh == formHelperOrForm || fh.formEl == formHelperOrForm
        });
        if (formHelper) {
          formHelper.cleanup();
          var id = $(formHelper.formEl).attr('abineGuid');
          this.formsData = _(this.formsData).filter(function (data) {
            return data.id != id;
          });
          this.formHelpers = _(this.formHelpers).filter(function (fh) {
            return fh != formHelper;
          });
          abineFormAnalyzer.clearFormCache(formHelper.formEl);
        }
      },

      fixOverlappingFormHelpers: function(formHelper, callback) {
        var self = this, removed = false;
        _(this.formHelpers).each(function (fh) {
          if (fh != formHelper && $.contains(formHelper.formEl, fh.formEl)) {
            // form element overlaps new formHelper
            self.removeFormHelper(fh);
            removed = true;
          }
        });
        if (removed) {
          // recreate current form helper
          self.removeFormHelper(formHelper);
          var formEl = formHelper.formEl;
          self.setupFormHelperForFormEl(formEl, self.formsData, function(){
            formHelper = self.getHelperForFormlessForm(formEl);
            callback(formHelper);
          });
        } else {
          callback(formHelper);
        }
      },

      removeHelperForMissingForms: function () {
        var self = this, removed = false;
        _(this.formHelpers).each(function (fh) {
          if (!$.contains(fh.formEl.ownerDocument, fh.formEl)) {
            // form element removed
            self.removeFormHelper(fh);
            removed = true;
          }
        });
        return removed;
      },

      showMapForm: function(options) {
        if (options.formId) {
          var strategy = _.find(this.formHelpers, function(helper){
            return helper.id == options.formId;
          });
          if (!strategy) return;
          options = {strategyId: strategy.id, forms: options.forms, formId: options.formId};
          options.formType = strategy.formType;
        }
        if (options.mapFirstForm) {
          var strategy = _.find(this.formHelpers, function(helper){
            return abineFormAnalyzer.isElementInViewport(helper.formEl);
          });
          if (!strategy) {
            if (!options.retry) {
              var self = this;
              this.startFormHelpers(function(){
                options.retry = true;
                self.showMapForm(options)
              });
              return;
            }
            return;
          }
          options = {strategyId: strategy.id};
          options.formType = strategy.formType;
        } else {
          var strategy = this.getFormStrategy(options.strategyId);
        }
        if (!strategy) {
          return;
        }
        var form = strategy.formEl;

        function showView() {
          var strategy = _(this.formHelpers).find(function(fh){return fh.formEl == form});
          options.strategyId = strategy.id;
          var reviewMode = this.preferences.getPref('mapping:reviewer');
          function showImpl() {
            this.closeCurrentPanel();
            this.createPanel({
              className: "abineDNTMePanel",
              panelWidth: 400,
              panelHeight: 600,
              draggable: true,
              style:{
                borderRadius: '0px',
                height: 600 + "px",
                position: 'fixed',
                top: "20px",
                right: "20px"
              },
              view: 'mapForm',
              payload: options
            });
          }
          var fields = [];

          _.each(strategy.formObserver.getFields(), function(field){
            var label = field.label;
            if (!label || label.length <= 3) {
              label = field.signature;
            }
            var shortLabel = label;
            if (label.length > 26) {
              shortLabel = label.substr(0, 13) + "..." + label.substr(label.length-13);
            }
            fields.push({label: label, shortLabel: shortLabel, signature: field.signature, dataType: field.dataType});
          });

          options.reviewMode = reviewMode;
          options.fields = fields;
          options.formType = strategy.formType;
          options.formSignature = $(form).data('mmSignature');

          if (reviewMode) {
            proxies.mappingsModule.getOneFormMappings({id: $(form).data('mmSignature')}, _.bind(function(mappings){
              options.mappings = _.values(mappings);
              showImpl.call(this);
            }, this));
          } else{
            showImpl.call(this);
          }
        }

        if (abineFormAnalyzer.hasFormChanged(form)) {
          // recreate form helper.
          var formHelper = this.getHelperForForm(form);
          this.removeFormHelper(formHelper);
          formHelper = null;
          abineFormAnalyzer.clearFormCache(form);
          this.setupFormHelperForFormEl(form, this.formsData, _.bind(showView, this));
        } else {
          showView.call(this);
        }
      },

      showAutoFill: function(options) {
        var strategy = this.getFormStrategy(options.strategyId);
        if (!strategy) return;

        var form = strategy.formEl;

        function showView() {
          var strategy = this.getHelperForForm(form);
          options.strategyId = strategy.id;
          function showImpl() {
            this.closeCurrentPanel();
            this.createPanel({
              draggable: true,
              className: "abineDNTMePanel",
              panelWidth: 400,
              panelHeight: 500,
              style:{
                borderRadius: '0px',
                height: 500 + "px",
                position: 'fixed',
                top: "20px",
                right: "20px"
              },
              view: 'autoFill',
              payload: options
            });
          }
          var fields = [];

          _.each(strategy.formObserver.getFields(), function(field){
            var label = field.label;
            if (!label || label.length <= 3) {
              label = field.signature;
            }
            var shortLabel = label;
            if (label.length > 26) {
              shortLabel = label.substr(0, 13) + "..." + label.substr(label.length-13);
            }
            var visible = $(field.element).is(":visible");
            fields.push({label: label, shortLabel: shortLabel, signature: field.signature, dataType: field.dataType, visible: visible});
          });

          options.fields = fields;
          options.formType = strategy.formType;
          options.formSignature = $(form).data('mmSignature');
          options.url = this.href;
          options.domain = this.domain;

          showImpl.call(this);
        }

        if (abineFormAnalyzer.hasFormChanged(form)) {
          // recreate form helper.
          var formHelper = this.getHelperForForm(form);
          this.removeFormHelper(formHelper);
          formHelper = null;
          abineFormAnalyzer.clearFormCache(form);
          this.setupFormHelperForFormEl(form, this.formsData, _.bind(showView, this));
        } else {
          showView.call(this);
        }
      },

      disablePanelOnThisPage: function() {
        this._disablePanels = true;
        proxies.memoryStore.set('disabled.'+this.href.replace(/\?.*/, ''), true);
      },

      disablePanelOnThisSite: function() {
        this._disablePanels = true;
//        abineProxies.memoryStore.set('disabled.'+this.domain, true);
      },

      showFieldIconsOnThisSite: function() {
        this._showFieldIcons = true;
        proxies.memoryStore.set('fieldIcons.'+this.tabId+'.'+this.domain, true);
        this.formHelpers.forEach(function(helper){
          helper.cleanup();
          helper.setupForm();
        });
      },

      canShowPanelHere: function() {
        return !this._disablePanels;
      },

      fillField: function(fieldInfo){
        // fill the last element that triggered a 'contextmenu' event
        //   (firefox sends element in fieldInfo)
        var field = fieldInfo.element || this.lastElementClicked;
        if (field) {

          // want to re-use the same password across multiple context menu fills on same form :-/
          // first time we fill one, save the value for later
          if (fieldInfo.dataType && fieldInfo.dataType.match("/password")) {
            if (!this.generatedPassword && fieldInfo.generatedNow) {
              this.generatedPassword = fieldInfo.value;
            } else if (this.generatedPassword && fieldInfo.generatedNow) {
              fieldInfo.value = this.generatedPassword;
            }
          }

          // want to re-use the same masked email across multiple context menu fills on same form :-/
          // first time we generate one, save the value for later
          if (fieldInfo.generate && fieldInfo.dataType && fieldInfo.dataType.match("/contact/email")) {
            if (!this.generatedEmail) {
              var shieldedEmails = new models.ShieldedEmailCollection();
              shieldedEmails.fetchAll({
                success: _.bind(function () {
                  if (shieldedEmails.length > 0) {
                    var primaryEmail = shieldedEmails.at(0);
                    var disposable = primaryEmail.generateDisposable(this.domain, 'right-click');
                    disposable.save({}, {
                      success: _.bind(function () {
                        this.generatedEmail = disposable;
                        this.fillField(fieldInfo);
                      }, this)
                    });
                  }
                }, this)
              });

              return;
            } else {
              fieldInfo.value = this.generatedEmail.getAddress();
              fieldInfo.disposableGUID = this.generatedEmail.get('id');
            }
          }

          var triggerPanelFillEvent = false;
          if (fieldInfo.value) {
            $(field).val(fieldInfo.value);

            fieldInfo.name = field.getAttribute("name");
            fieldInfo.id = field.getAttribute("id");

            // keep and build an in-memory form of the fields filled using the context menu to submit to background for processing
            if(!this.contextForm){
              this.contextForm = $("<form maskMeFilled='yes'></form>", this.document);
            }
            var clonedField = $(field).clone();
            clonedField.attr("abinedatatype", fieldInfo.dataType);
            this.contextForm.append(clonedField);
            $(this.contextForm.get(0)).data("mmFields", null);
            triggerPanelFillEvent = true;
          }

          var formStrategy = _.find(this.formHelpers, function(helper){
            return !!helper.getField(field);
          });

          if (formStrategy) {
            // save local mapping and push to server
            var fieldData = formStrategy.getField(field);
            var self = this;
            this.mapField(formStrategy, fieldData.signature, {dataType: fieldInfo.dataType, formType: fieldInfo.formType, right_click: true}, function(){
              // trigger panel fill event only after reloading form helper with new data type changes
              if (triggerPanelFillEvent) {
                self.triggerPanelFillEvent(self.contextForm.get(0), fieldInfo);
              }
            });
          }
        }
      },

      triggerPanelFillEvent: function(formEl, fieldData) {
        this.messenger.send("panel:fill",{fieldData:fieldData, formData: abineFormAnalyzer.getFormData(formEl, this.href)});
      },

      trackLastElementClicked: function(e){
        if(e.target.localName == "input" || e.target.localName == "textarea"){
          this.lastElementClicked = e.target;
        }
      },

      isLocked: function() {
        return this._locked;
      },

      locked: function() {
        this._locked = true;
        this.trigger('locked');
        if(this.domain){
          this.messenger.send("contentscript:contextmenu:refresh",{domain:this.domain}); //BL: state can change within the same domain -> within MM
        }
      },

      unlocked: function() {
        this._locked = false;
        this.trigger('unlocked');
        if (this.preferences) {
          this.preferences.fetchAll();
        }
        if(this.domain){
          this.messenger.send("contentscript:contextmenu:refresh",{domain:this.domain});
        }
      },

      // ### function notifyInit
      // Notify the background that it is initialized
      // Triggers the _content\_manager_:init event
      notifyInit: function() {
        // There is no location in test documents?
        if (this.href) {
          var initDetails = {
            url: this.href,
            domain: this.domain
          };
          this.messenger.send("contentManager:init", initDetails);
          this.trigger("contentManager:init",initDetails);
        }
      },

      // Create helpers for any forms present on this page
      // First we need to pull the preferences, and then we can do this
      start: function() {
        if (!this.document.defaultView) {
          // firefox about:* pages have defaultView null
          return;
        }
        if(this._locked === null){
          this.once("ready", _.bind(this.start, this));
          return;
        }

        // intrusive panel
        function showTrackersOnLoad() {
          // show only for top most frame
          if (!this.topWindow)
            return;
          if (this.preferences.getPref('showTrackersOnload') &&
                ABINE_DNTME.config.webAppHost.indexOf(this.domain) == -1 || this.preferences.getPref('showBlurGrowl')) {
            var key = this.domain;
            proxies.memoryStore.get(key, _.bind(function(value){
              if (value && !this.shouldShowCardAlertPanel()){ // show the card intrusive window anytime checkout form is present even if this is a domain we've visited before
                return;
              }
              $(this.document).ready(_.bind(function(){
                var tabData = new models.TabData({messenger: this.messenger});
                tabData.fetch({success: _.bind(function(){
                  if (!this.lastDNTMePanel && // do not show if user has already clicked on toolbar button
                    !tabData.get('reloadRequired') && // do not show for page-not-scanned tabs
                    !this.href.match(/dntmeFullTour=|dntmeTour=/i) && // do not auto show for post install tour page
                    (!this.preferences.getPref('blurGrowlTab') || this.preferences.getPref('blurGrowlTab') == this.tabId) // only show for first tab
                    ) {
                    this.preferences.setPref('blurGrowlTab', this.tabId);
                    if(!this.preferences.getPref('showBlurGrowl')){
                        proxies.memoryStore.set(key, true);
                    }

                    this.showToolbarPanel({tabId: this.tabId, compact: true, cardAlertPanel: this.shouldShowCardAlertPanel(), showBlurGrowl: this.preferences.getPref('showBlurGrowl')});
                  }
                }, this)});
              }, this));
            }, this));
          }
        }

        function showDNTMeGrowls() {
          if (!this.maskme_installed && this.preferences.getPref('showInTheKnowGrowls')) {
            proxies.panelsModule.getDNTMeGrowl(this.tabId, _.bind(function (dynamicPanel) {
              if (!dynamicPanel) {
                showTrackersOnLoad.call(this);
              } else {
                $(this.document).ready(_.bind(function(){
                  this.showNotification({type: 'growl', dynamicPanel: dynamicPanel, neverFade: true});
                }, this));
              }
            }, this));
          } else {
            showTrackersOnLoad.call(this);
          }
        }

        function afterLoadingPreference() {
          this.startFormHelpers(_.bind(function(){
            showDNTMeGrowls.apply(this);
            if (this.document.readyState == 'complete') {
              this.pageLoadDone('skipFormHelpers');
            } else {
              $(this.document.defaultView).on('load', _.bind(function(){this.startFormHelpers();}, this));
              $(this.document).ready(_.bind(this.pageLoadDone, this));
            }
            if (this.document.documentElement && this.document.documentElement.attributes.injectedOnFocus) {
              // this does not work :(
              $(this.document.activeElement).trigger('showpanel');
            }
          }, this));
        }

        proxies.mappingsModule.getFormRules(_.bind(function(rules){
          if (rules) {
            LOG_FINEST && ABINE_DNTME.log.debug("** using form rules version "+rules.Version);
            abineFormAnalyzer.updateRules(rules);
          }
          this.filler = new AbineFiller(this.document, this.domain, abineFormAnalyzer.getKeySimulationRegex(), this);

          if (!this.blacklist) {
            this.blacklist = new models.Blacklist({domain: this.domain, messenger: this.messenger});
            try {
              this.blacklist.fetchByDomain(false, _.bind(function() {
                // BL: 1/5/2015 no longer turning off all panels/features if tracker blocking is off
                // this.blacklisted = this.blacklist.get('tracker');
                this.loadUserPreferences(_.bind(afterLoadingPreference, this));
              }, this));
            } catch(e) {
              this.loadUserPreferences(_.bind(afterLoadingPreference, this));
            }
          } else {
            this.loadUserPreferences(_.bind(afterLoadingPreference, this));
          }
        }, this));
      },

      getVisibleFormsData: function() {
        var formsData = [];
        var self = this;
        _.each(this.formsData, function(data){
          var formHelper = _.find(self.formHelpers, function(fh){return fh.formEl.getAttribute('abineGuid') == data.id;})
          if (formHelper && abineFormAnalyzer.isVisible(formHelper.formEl)) {
            formsData.push(data);
          }
        });
        return formsData;
      },

      pageLoadDone: function(param) {
        var self = this;
        function formInitialized(){
          abineTimer.setTimeout(function(){
            var pageText = self.getPageText(self.document);

            // look for logout text in page.
            var hasLogout = !!pageText.match(/signout|logout|sign out|log out/i);

            // look for login text in the top 1000 chars of page
            var hasLogin = !hasLogout && !!pageText.substr(0, 1000).match(/signin|login|signup|sign in|log in/i);

            var data = {
              forms: self.getVisibleFormsData(),
              domain: self.domain,
              hasLogin: hasLogin,
              hasLogout: hasLogout
            };

            self.updateLastActivity();

            self.messenger.send('contentManager:formsInitialized', data);
            self.trigger('contentManager:formsInitialized', data);
          }, 2000);
        }
        if (param == 'skipFormHelpers') {
          formInitialized();
        } else {
          // recheck for forms after page load
          this.startFormHelpers(formInitialized);
        }
      },

      updateLastActivity: function() {
          if (!this.isLocked() && this.preferences) {
            this.preferences.setPref("lastActivity",Date.now())
          }
      },

      addBlurIconToField: function() {
//        if (ABINE_DNTME.config.fieldIcons && field.fieldType != 'select' && element$.css('background-image') in {'':1,'none':1}) {
//          // add field icon
//          element$.css({
//            'background-image': 'url('+assets.assetUrl('/pages/images/dnt-field-icon.png')+')',
//            'background-repeat': 'no-repeat',
//            'background-position': 'calc(100% - 2px) center',
//            'background-size': '15px 15px'
//          });
//          // cursor for out icon
//          element$.attr('old-cursor', element$.css('cursor'));
//          element$.bind('mousemove.mmfoicon', _.bind(function(e){
//            var offsetX = typeof(e.offsetX)!='undefined'?e.offsetX:(e.clientX-$(e.target).offset().left);
//            if (offsetX > (e.target.offsetWidth-18)) {
//              element$.css('cursor', 'pointer');
//            } else {
//              element$.css('cursor', element$.attr('old-cursor'));
//            }
//          }, this));
//          // click on field icon
//          element$.bind('mousedown.mmfoicon', _.bind(function(e){
//            var offsetX = typeof(e.offsetX)!='undefined'?e.offsetX:(e.clientX-$(e.target).offset().left);
//            if (offsetX > (e.target.offsetWidth-18)) {
//              element$.attr('dntmeIconClick', 'yes');
//              var eventName = eventNameFor(getHookableDataType(field.dataType), 'iconclick');
//              timer.setTimeout(_.bind(function(){
//                this.trigger(eventName, {
//                  formObserver: this,
//                  field: field,
//                  type: 'iconclick',
//                  keyCode: 0
//                });
//              }, this), 0);
//            }
//          }, this));
//        }
      },

      reloadPreference: function() {
        this.loadUserPreferences();
        this.blacklist.fetchByDomain(false, _.bind(function() {
        }, this));
      },

      loadUserPreferences: function(callback) {
        callback = callback || function(){};

        function afterLoad() {
          var tag = this.preferences.getPref('userTag') || this.preferences.getPref('dynamicTag');
          if (tag) {
            ABINE_DNTME.config.updateTag(tag, this.document);
          }
          callback.call(this);
        }

        this.preferences = new models.PreferenceCollection({});
        this.preferences.bind('sync:success', _.once(_.bind(function(){afterLoad.call(this);},this)));
        this.preferences.bind('sync:error', _.bind(function(){afterLoad.call(this);}, this));
        this.preferences.fetchAll();
      },

      arrayFromNodeList: function(nodeList){
        return Array.prototype.slice.call(nodeList);
      },

      hookForUserIntent: function(e){
        this.userIntent = true;
      },

      disableFormHelpers: function() {
        LOG_FINEST && ABINE_DNTME.log.finest("contentManager:disableFormHelpers");
        $(this.document).off('mousedown', this.hookNewFormsOnClickHandler);
        this.stopFormHelpers();
        this.blacklisted = true;
        this._disableFormHelpers = true;
      },

      reloadFormHelper: function(form, cb) {
        var formHelper = _(this.formHelpers).find(function(formHelper){
          if(formHelper.formEl == form){
            return true;
          }
        });

        var oldId = null;
        if (formHelper) {
          oldId = formHelper.id;
          this.removeFormHelper(formHelper);
          abineFormAnalyzer.clearFormCache(form);
        }

        this.formMappings = new models.MappedFormCollection({});
        this.formMappings.fetchAllByDomain(this.domain, _.bind(function(){
          this.setupFormHelperForFormEl(form, [], _.bind(function(){
            var formHelper = this.getHelperForForm(form);
            if (formHelper && oldId) {
              formHelper.id = oldId;
            }
            cb();
          }, this), false, true);
        }, this));
      },

      hookNewFormsOnClick: function(e) {
        LOG_FINEST && ABINE_DNTME.log.finest("contentManager:hookNewFormsOnClick enter");
        if (this.blacklisted) {
          LOG_FINEST && ABINE_DNTME.log.finest("contentManager:hookNewFormsOnClick blacklisted return");
          return;
        }

        // ignore clicks in our panels
        try {
          if (e.target.ownerDocument.getElementsByTagName('body')[0].getAttribute('abine-panel')) {
            LOG_FINEST && ABINE_DNTME.log.finest("contentManager:hookNewFormsOnClick blur panel clicked. return");
            return;
          }
        } catch(e){}

        this.formHelpers = this.formHelpers || [];

        var el = e.target;

        // ignore cross-origin form clicks
        try {
          el.ownerDocument.getElementById('dummy');
          el.ownerDocument.location.href;
        } catch(e){
          LOG_FINEST && ABINE_DNTME.log.finest("contentManager:hookNewFormsOnClick return cross-origin");
          return;
        }

        var self = this;
        function triggerOurPanel(form, newFormHelper) {
          $(el).trigger('showpanel');
          if ($(form).data('ab-skipped')) {
            $(form).removeData('ab-skipped');
            self.showCheckoutGrowl();
          }
          if (typeof newFormHelper != 'undefined') {
            // new form or old form changed, check if we need to show SAVE growl.
            self.removeHelperForMissingForms();
            self.messenger.send("contentManager:reinit", {
              url: self.href,
              domain: self.domain
            });
          }
        }

        var nodeName = el.nodeName.toLowerCase();
          var form = el.form;
        if ((nodeName == "input" && !((el.type||"text").toLowerCase() in abineFormAnalyzer.UNPROCESSABLE_TAGS)) || nodeName == "label"){
          var formHelper = null;
          self.removeHelperForMissingForms();
          if (form) {
            if (form.elements.length > 100) {
              LOG_FINEST && ABINE_DNTME.log.finest("+++ skipping form with " + form.elements.length + ' fields');
              return;
            }
            formHelper = this.getHelperForForm(form);

            if (!formHelper && el.ownerDocument.getElementsByTagName("form").length == 1 && this.formHelpers.length > 0) {
              // .net forms, we detected groups of fields as forms earlier.  clear everything.
              LOG_FINEST && ABINE_DNTME.log.finest("contentManager:hookNewFormsOnClick clearing old detected forms as there is a single form now.");
              _(this.formHelpers).find(function(formHelper){
                abineFormAnalyzer.clearFormCache(formHelper.formEl);
                formHelper.cleanup();
              });
              this.formHelpers = [];
            }

          } else {
            formHelper = this.getHelperForFormlessForm(el);
            if (formHelper) {
              form = formHelper.formEl;
            }
          }
          var oldFormHelper = formHelper;
          if (formHelper && abineFormAnalyzer.hasFormChanged(form, oldFormHelper)) {
            // form was already processed, but it was changed dynamically.
            // recreate form helper.
            formHelper.cleanup();
            this.formHelpers = _(this.formHelpers).filter(function(fh){return fh != formHelper});
            formHelper = null;

            abineFormAnalyzer.clearFormCache(form);
          }

          this.formsData = this.formData || [];

          if (!formHelper) {
            if (form) {
              $(form).data('ab-skipped', 'yes');
              this.setupFormHelperForFormEl(form, this.formsData, function(){
                triggerOurPanel(form, !!oldFormHelper);
              });
            } else {
              // no FORM, so rerun startFormHelpers to detect new fields without forms.
              this.startFormHelpers(function(){
                formHelper = self.getHelperForFormlessForm(el);
                if (formHelper) {
                  formHelper = self.fixOverlappingFormHelpers(formHelper, function(formHelper){
                    $(formHelper.formEl).data('ab-skipped', 'yes');
                    triggerOurPanel(form, true);
                  });
                }
              });
            }
          } else {
            if ($(form).data('ab-skipped')) {
              $(form).removeData('ab-skipped');
              self.showCheckoutGrowl();
            }
          }
        } else {
          if (self.removeHelperForMissingForms()) {
            self.messenger.send("contentManager:reinit", {
              url: self.href,
              domain: self.domain
            });
          }
        }
      },

      pageHasCheckoutForm: function(){
        var found = false;
        if (this.formHelpers) {
          this.formHelpers.forEach(function(fh){
            if (found) return;
            if (fh.formType == abineFormAnalyzer.CHECKOUT_FORM) {
              var fields = abineFormAnalyzer.getFields(fh.formEl);
              var field = _.find(fields, function(field){
                return (field.dataType && field.dataType.indexOf('/financial') != -1 && abineFormAnalyzer.isElementInViewport(field.element));
              });
              if (field) {
                $(fh.formEl).removeData('ab-skipped');
                found = true;
              } else {
                $(fh.formEl).data('ab-skipped', 'yes');
              }
            }
          });
        }
        return found;
      },

      getFirstCheckoutFormHelper: function(){
        var firstCheckoutFormHelper = null;
        this.formHelpers.forEach(function(fh){
          if(firstCheckoutFormHelper == null && fh.formType == abineFormAnalyzer.CHECKOUT_FORM){
            firstCheckoutFormHelper = fh;
          }
        });
        return firstCheckoutFormHelper;
      },

      getPanelPayloadForFormHelper: function(formHelper){
        var creditCardField = formHelper.getFieldForDataType("/financial/creditcard/number");

        if(creditCardField){
          return {
            fieldType: creditCardField.dataType,
            fieldId: creditCardField.signature,
            strategyId: formHelper.id
          }
        } else {
          return {}
        }
      },

      shouldShowCardAlertPanel: function(isPaymentFieldActivity){ // if true, ignore whether we initially detected forms or not because of dynamic forms/etc.
        if (isPaymentFieldActivity || this.pageHasCheckoutForm()){
          return !!this.preferences.getPref('suggestProtectedCard');
        }
        return false;
      },

      setupFormHelperForFormEl: function(formEl, formsData, cb, doNotStartHelper, doNotAutoFill){
        LOG_FINEST && ABINE_DNTME.log.finest("contentManager:setupFormHelperForFormEl enter");
        if (this._disableFormHelpers) {
          if (cb) cb();
          return;
        }
        var helperClass = null;
        var formType = null;

        if (this.domain && this.domain.match(allowedHostsRegex) && formEl.hasAttribute("datatype-ignore")) {
          cb();
          return;
        }

        var formSignature = abineFormAnalyzer.getFormSignature(this.domain, formEl);

        var mappedForm = null;
        if(!this.formMappings){
          LOG_WARN && ABINE_DNTME.log.warn("No form mappings set up yet.")
        }else{
          mappedForm = this.formMappings.find(function(form){
            if(form.get("signature") == formSignature){
              return true;
            } else {
              return false;
            }
          });

          if (!mappedForm) {
            // form-signature didnt match because of change in field name or field state.  so find best matching mapping by looking at field names.

            var formFields = abineFormAnalyzer.getFields(formEl, {});
            var formFieldMap = {}, numFormFields = 0;
            _.each(formFields, function(field){formFieldMap[field.signature]=true;numFormFields++});
            mappedForm = null;
            var bestScore = 0;
            this.formMappings.each(function(form){
              // skip if form on page has more fields than the mapping
              if (numFormFields > form.fields.length) return;

              var numMatches = 0, numMismatches = 0;
              _.each(form.fields.models, function(field){
                if (field.get('signature') in formFieldMap) {
                  numMatches++;
                } else {
                  numMismatches++;
                }
              });
              var score = numMatches / form.fields.length;
              // > 90% match
              if (score > 0.9) {
                if (score > bestScore) {
                  LOG_FINEST && ABINE_DNTME.log.finest("matched mapping by comparing field signature. score = "+score+", matches="+numMatches+", mismatches="+numMismatches);
                  bestScore = score;
                  mappedForm = form;
                }
              }
            });
          }

        }

        if (mappedForm) {
          $(formEl).removeData('mmFields'); // remove cache as we have to rerun this with mappings
          if (mappedForm.get('local'))
            $(formEl).data('mmLocal', 1);
          else
            $(formEl).removeData('mmLocal');

          LOG_FINEST && ABINE_DNTME.log.finest("form type set from mapping "+mappedForm.get("formType"));
          formType = abineFormAnalyzer.getFormType(formEl, null, mappedForm.get("formType"));
        } else {
          formType = abineFormAnalyzer.getFormType(formEl);
        }

        var pageDataType = null, preLoadPanelFrame = false;
        // Determine helper class for form type
        if (formType == abineFormAnalyzer.LOGIN_FORM) {
          helperClass = abineFormStrategy.LoginHelper;
          pageDataType = 'login';
          preLoadPanelFrame = true;
        } else if (formType == abineFormAnalyzer.REGISTRATION_FORM) {
          helperClass = abineFormStrategy.RegistrationHelper;
          pageDataType = 'register';
          preLoadPanelFrame = true;
        } else if (formType == abineFormAnalyzer.OTHER_FORM) {
          helperClass = abineFormStrategy.OtherHelper;
        } else if (formType == abineFormAnalyzer.ADDRESS_FORM) {
          helperClass = abineFormStrategy.OtherHelper;
        } else if (formType == abineFormAnalyzer.LOGIN_REGISTRATION_FORM){
          helperClass = "loginRegisterHelper";
        } else if (formType == abineFormAnalyzer.CHECKOUT_FORM){
          helperClass = abineFormStrategy.CheckoutHelper;
          pageDataType = 'checkout';
          preLoadPanelFrame = true;
        } else if (formType == abineFormAnalyzer.CHANGEPASSWORD_FORM){
          helperClass = abineFormStrategy.ChangePasswordHelper;
        } else {
          helperClass = abineFormStrategy.IgnoreHelper;
        }

        // Instantiate and store form helper
        if (helperClass) {
          // formsData is used to detect form submit failures
          var hc, fieldMap;
          var fieldNames = {};

          if(mappedForm && mappedForm.fields && mappedForm.fields.models){
            fieldMap = {};
            _.each(mappedForm.fields.models, function(field){
              fieldMap[field.get("signature")] = field.get("dataType");
            });
          }

          var formFields = abineFormAnalyzer.getFields(formEl, fieldMap);

          LOG_FINEST && abineFormAnalyzer.dumpForm(formEl, this.domain);

          // used in processForm for https://mapping.abine.com/#verify page
          if (doNotStartHelper) {
            cb(formType, formFields);
            abineFormAnalyzer.clearFormCache(formEl);
            return;
          }

          _.each(formFields, function(field){
            fieldNames[field.dataType] = field.signature;
          });

          var actionUrl = formEl.hasAttribute("action")?formEl.getAttribute("action"):"";
          formsData.push({
            id: $(formEl).attr('abineGuid'),
            url: actionUrl,
            type: formType,
            fields: fieldNames,
            formSignature: formSignature,
            visible: abineFormAnalyzer.isVisible(formEl)
          });

          if (helperClass == "loginRegisterHelper" || helperClass == abineFormStrategy.LoginHelper || helperClass == abineFormStrategy.ChangePasswordHelper) {
            var accounts = new models.AccountCollection();
            var afterAccount = _.bind(function(err){
              if(err){
                LOG_ERROR && ABINE_DNTME.log.error("[ERROR CALLBACK]#[abine/common/content_manager]#[setupFormHelperForFormEl]# error while loading accounts for site");
              }
              var tmpFormType = formType;
              if (err || !accounts || accounts.length == 0) {
                if (helperClass == "loginRegisterHelper") {
                  helperClass = abineFormStrategy.RegistrationHelper;
                  tmpFormType = abineFormAnalyzer.REGISTRATION_FORM;
              } else {
                  helperClass = helperClass;
                }
              } else if (helperClass != abineFormStrategy.ChangePasswordHelper) {
                helperClass = abineFormStrategy.LoginHelper;
                tmpFormType = abineFormAnalyzer.LOGIN_FORM;
              }

              hc = new helperClass({
                el: formEl,
                mappedFields: fieldMap,
                contentManager: this,
                preferences: this.preferences,
                doNotAutoFill: doNotAutoFill,
                accounts: accounts
              });
              hc.formType = tmpFormType;
              this.formHelpers.push(hc);

              LOG_FINEST && ABINE_DNTME.log.finest("contentManager:setupFormHelperForFormEl callback");
              cb();
            },this);
            if (this.domain) {
              accounts.fetchLoginsByDomain(this.domain, afterAccount);
            } else {
              afterAccount();
            }
          } else {
            hc = new helperClass({
              el: formEl,
              mappedFields: fieldMap,
              contentManager: this,
              doNotAutoFill: doNotAutoFill,
              preferences: this.preferences
            });
            hc.formType = formType;
            this.formHelpers.push(hc);
            LOG_FINEST && ABINE_DNTME.log.finest("contentManager:setupFormHelperForFormEl callback");
            cb();
          }

          if (pageDataType) {
            this.sendFormViewData(pageDataType);
            if (pageDataType == 'checkout') {
               var url = (this.href || '').replace(/[\?].+$/, '');
              proxies.licenseModule.logCheckout({method: 'visit', merchant: this.domain, url: url, amount: this.getAmount()});
            }
          }

          if (preLoadPanelFrame && CACHE_PANEL_FRAME && !this._fieldPanelIframe && !this._loadingFieldPanel) {
            // load panel iframe for faster display
            this.createPanel({
              style: {},
              secured: false,
              panelWidth: 450,
              panelHeight: 0,
              field: {element: this.document.createElement('input')},
              view: 'none',
              payload: {}
            });
          }
        }
      },

      hookDocumentEventHandlers: function (document) {
        if (!document.userIntentHandler) {
          document.userIntentHandler = _.bind(this.hookForUserIntent, this);
          document.hookNewFormsOnClickHandler = _.bind(this.hookNewFormsOnClick, this);

          $(document).on('keydown', document.userIntentHandler);
          $(document).on('mousedown', document.userIntentHandler);

          // add click handler only after preferences are loaded.
          // otherwise helpers will fail with javacript errors
          $(document).on('mousedown', document.hookNewFormsOnClickHandler);
        }
      },

      // ### function startFormHelpers
      // Create helpers for any forms present on this page
      startFormHelpers: function(callback) {
        callback = callback || emptyCallback;
        LOG_FINEST && ABINE_DNTME.log.finest("contentManager:startFormHelpers enter");
        if (this._disableFormHelpers) {
          callback();
          return;
        }
        if (this.blacklisted) {
          LOG_DEBUG && ABINE_DNTME.log.debug("skipping form processing for blacklisted site");
          callback();
          return;
        }

        var startTime = Date.now();
        this.formHelpers = this.formHelpers || [];
        this.innerFrames = this.innerFrames || {};

        var formElements = abineFormAnalyzer.getAllForms(this.document, this.domain);

        // Get forms inside of iframes (works for same origin forms in chrome and all frames in firefox)
        var iframeElements = this.arrayFromNodeList(this.document.getElementsByTagName('iframe'));
        _(iframeElements).each(function(iframeEl){
          try {
            var frameDoc = (iframeEl.contentWindow || iframeEl.contentDocument);
            if (frameDoc) {
              if(frameDoc.document) frameDoc = frameDoc.document;
              // required to reject cross origin
              frameDoc.getElementById('dummy');
              frameDoc.location.href;

              // require for userIntent recording inside frames.
              try{this.hookDocumentEventHandlers(frameDoc);}catch(e){}

              frameDoc.documentElement.mmFormsProcessed = true;
              var forms = abineFormAnalyzer.getAllForms(frameDoc, this.domain);
              if (forms.length > 0) {
                // store frame element with some unique id and set that id on all forms in that frame
                // frame element is required to compute location for showing our helpers.
                var id = 'iframe.'+Math.random();
                this.innerFrames[id] = iframeEl;
                _(forms).each(function(frm){frm.setAttribute('abFrameId', id);});

                formElements = formElements.concat(forms);
              }
            }
          } catch(e) {
            // skip errors as they will be cross-origin frames
          }
        },this);

        // remove already processed forms
        formElements = _(formElements).filter(function(form){
          return !$(form).data('mmFormType');
        });

        this.formsData = this.formsData || [];

        if (formElements.length == 0) {
          LOG_FINEST && ABINE_DNTME.log.finest("contentManager:startFormHelpers leave. no forms.");
          if (!this.formMappings && this.domain) {
            this.formMappings = new models.MappedFormCollection({messenger: this.messenger});
            this.formMappings.fetchAllByDomain(this.domain, function(){});
          }

          this.showCheckoutGrowl();

          function retry() {
            if (this.preferences.getPref('autoLoginHost') == this.domain && (!this._retriedForForm || this._retriedForForm < 5)) {
              LOG_FINEST && ABINE_DNTME.log.finest("contentManager:startFormHelpers retry for forms in 1 sec for auto-login.");
              this._retriedForForm = (this._retriedForForm || 0)+1;
              abineTimer.setTimeout(_.bind(this.startFormHelpers, this), 1000);
            }
          }
          this.executeLoginPageEvents(retry, 3);
          callback();
          return;
        }

        LOG_FINEST && ABINE_DNTME.log.finest("processing "+formElements.length+" forms");

        var postMappings = _.bind(function(){
          this.executeLoginPageEvents(_.bind(function(){
            _.async.forEachSeries(formElements, _.bind(function(formEl,cb) {
              this.setupFormHelperForFormEl(formEl, this.formsData, cb);
            }, this), _.bind(function(err){
              if(err){
                LOG_ERROR && ABINE_DNTME.log.error("[ERROR CALLBACK]#[abine/common/content_manager]#[startFormHandlers]# error event in setupFormHelperForFormEl while doing formElements async.forEach");
              }

              // code to focus into input field that will also pop login panel on page load if there are logins
              //if(this.document.activeElement){
              //  var jqActiveElement = $(this.document.activeElement);
              //  if(jqActiveElement.is("input")){
              //    jqActiveElement.focus();
              //  }
              //}

              this.showCheckoutGrowl();

              var processTime = Date.now() - startTime;
              LOG_DEBUG && ABINE_DNTME.log.debug("Analyzed and processed all forms in " + processTime + " ms.");
              LOG_FINEST && ABINE_DNTME.log.finest("contentManager:startFormHelpers callback");

              if (window.lastFieldFocused){
                // lastFieldFocused is set when an input inside the iframe is focused (iframe_content.js).
                // Here, this logic makes sure that the panel is shown after the first click inside the iframe.
                this.userIntent = true;
                $(window.lastFieldFocused).trigger('showpanel');
                delete window.lastFieldFocused;
              }

              callback();
            },this));
          }, this), 3);
				}, this);

        if(!this.formMappings && this.domain){
          this.formMappings = new models.MappedFormCollection({messenger: this.messenger});
          this.formMappings.fetchAllByDomain(this.domain, _.bind(function(err){
            postMappings();
          }, this));
        } else {
          postMappings();
        }
      },

      showCheckoutGrowl: function() {
        if(this.preferences.getPref('showCheckoutPanelGrowl') &&  this.pageHasCheckoutForm()){
          if (this.checkoutGrowlShown) return;
          this.checkoutGrowlShown = true;
          var realCards = new models.RealCardCollection();
          realCards.fetchAllForDomain(this.domain, _.bind(function(err) {
            var payload = this.getPanelPayloadForFormHelper(this.getFirstCheckoutFormHelper());
            var disposableCards = new models.DisposableCardCollection();
            function done() {
              this.showNotification({
                msg: "",
                targetUrl: ABINE_DNTME.config.protocol + ABINE_DNTME.config.webAppHost + "/#/cards",
                targetId: "",
                model: {maskedCard: {}},
                domain: this.domain,
                amount: this.getAmount(this.document),
                collection: realCards.toJSON(),
                maskedCards: disposableCards.toJSON(),
                height: 350,
                neverFade: true,
                type: 'checkout_growl',
                panelWidth: 400,
                payload: payload
              });
            }
            var shieldedCard = new models.ShieldedCardCollection();
            // if logged in, see if user has shielded card
            shieldedCard.fetchAll({
              success: _.bind(function () {
                if (shieldedCard.length > 0) {
                  var primaryCard = shieldedCard.at(0);
                }
                // logged in case with existing shielded card (billing info on file)
                if (primaryCard) {
                  //              this.panelId = this.options.paymentsr2?8000006:hasEnabledPayments?4000001:9000001;
                  disposableCards.fetchAllByDomain(this.domain, _.bind(done, this));
                } else {
                  done.call(this);
                }
              }, this)
            });
          }, this));
        } else if (false && !this.offerGrowlShown && this.href && this.href.match(/^http[s]?:\/\/[^\/\?]+[\/]?(\?.+)?$/i)) {
          // home page, so show offer cards
          this.offerGrowlShown = true;
          var realCards = new models.RealCardCollection();
          realCards.fetchOfferCardsForDomain(this.domain, _.bind(function(err) {
            if (realCards.length == 0) {
              return;
            }
            this.showNotification({
              msg: "",
              targetUrl: ABINE_DNTME.config.protocol + ABINE_DNTME.config.webAppHost + "/#/cards",
              targetId: "",
              model: {maskedCard: {}},
              domain: this.domain,
              offerOnly: true,
              collection: realCards.toJSON(),
              height: 350,
              neverFade: true,
              type: 'checkout_growl',
              panelWidth: 400,
              payload: {}
            });
          }, this));
        }
      },

      executeLoginPageEvents: function(callback, retry) {
        try {
          if (this.preferences.getPref('autoLoginHost') == this.domain) {
            var events = this.preferences.getPref('autoLoginEvents') || "";
            if (events.length > 0) {
              events = events.split('\n');
              while(events.length > 0) {
                var command = events.shift();
                LOG_FINEST && ABINE_DNTME.log.finest("*** processing auto-login event " + command);
                var spaceIdx = command.indexOf(' ');
                var event = command.substr(0, spaceIdx);
                var selector = command.substr(spaceIdx + 1);
                if (event.length > 0 && selector.length > 0 && event.match(/click|focus/)) {
                  var element = $(selector, this.document);
                  if (element.length > 0) {
                    this.userIntent = true;
                    if (event == 'click') {
                      element[0][event]();
                    } else {
                      $(element).trigger('mousedown');
                      element.trigger(event);
                    }
                    continue;
                  } else {
                    events.unshift(command);
                    break;
                  }
                }
                break;
              }
              this.preferences.setPref('autoLoginEvents', events.join("\n"));
              if (events.length > 0 && retry > 0) {
                abineTimer.setTimeout(_.bind(function(){this.executeLoginPageEvents(callback, retry-1);}, this), 1000);
                return;
              }
            }
          }
        } catch(e) {
          LOG_FINEST && ABINE_DNTME.log.finest("error processing auto-login event " + e);
        }
        callback.apply(this, []);
      },

      stopFormHelpers: function() {
        _(this.formHelpers).each(function(helper){
          helper.cleanup();
        });
        this.formHelpers = [];
        this.innerFrames = [];
      },

      setupFormsInFrame: function(frameDoc, frameElement) {
        LOG_FINEST && ABINE_DNTME.log.finest("contentManager:setupFormsInFrame enter");
        if (!frameDoc.documentElement || frameDoc.documentElement.mmFormsProcessed) {
          // already processed or not valid frameDoc.
          LOG_FINEST && ABINE_DNTME.log.finest("contentManager:startFormHelpers return");
          return;
        }

        if (this.blacklisted) {
          LOG_DEBUG && ABINE_DNTME.log.debug("skipping form processing for blacklisted site");
          LOG_FINEST && ABINE_DNTME.log.finest("contentManager:setupFormsInFrame return");
          return;
        }

        // require for userIntent recording inside frames.
        this.hookDocumentEventHandlers(frameDoc);

        this.formHelpers = this.formHelpers || [];
        this.innerFrames = this.innerFrames || {};
        this.formsData = this.formsData || [];

        var iframeElId = null;
        if (!frameElement && frameDoc.defaultView && frameDoc.defaultView.frameElement) {
          frameElement = frameDoc.defaultView.frameElement;
        }
        if (frameElement) {
          iframeElId = 'iframe.'+Math.random();
          this.innerFrames[iframeElId] = frameElement;
        }

        frameDoc.documentElement.mmFormsProcessed = true;

        // add click handler only after preferences are loaded.
        // otherwise helpers will fail with javacript errors
        $(frameDoc).bind('click', _.bind(this.hookNewFormsOnClick, this));

        var formElements = abineFormAnalyzer.getAllForms(frameDoc, this.domain);

        var newFormsData = [];

        _.async.forEachSeries(formElements, _.bind(function(formEl,cb) {
          if (iframeElId) {
            formEl.setAttribute('abFrameId', iframeElId);
          }
          this.setupFormHelperForFormEl(formEl, newFormsData, cb);
        }, this), _.bind(function(err){
          if(err){
            LOG_ERROR && ABINE_DNTME.log.error("[ERROR CALLBACK]#[abine/common/content_manager]#[setupFormsInFrame]# error in setupFormHelperForFormEl while doing formElements async.forEach");
          }
          this.formsData = this.formsData.concat(newFormsData);

          LOG_FINEST && ABINE_DNTME.log.finest("contentManager:setupFormsInFrame return");
        },this));

      },

      // ### function triggerFocusOnElement
      // Manually trigger the focus hander for the activeElement
      triggerFocusOnElement: function(activeElement) {
        LOG_FINEST && ABINE_DNTME.log.finest("contentManager:triggerFocusOnElement enter");
        $(activeElement).trigger('focus');
        LOG_FINEST && ABINE_DNTME.log.finest("contentManager:triggerFocusOnElement return");
      },

      getPanelForDataType: function(panels, dataType){
        var panel = _.find(panels,function(panel){return panel.field_type == dataType});
        return panel? panel:false;
      },

      // ### function createPanel(payload)
      // - `payload.localUrl` - the page to load into the panel
      // - `payload.style` - css to apply to the iframe
      // - returns the added iframe element
      //
      // Create an iframe panel in the content document.
      createPanel: function(options) {
        if (options.className == 'abineDNTMePanel' && !this.topWindow) {
          this.messenger.send("background:create:panel:forward", options);
          return;
        }

        var doc = this.document;
        if (!this.topWindow && options.field) {
          try {
            var documentElement = doc.documentElement;
            var iframeWidth = Math.max(documentElement.clientWidth, doc.defaultView.innerWidth || 0);
            var iframeHeight = Math.max(documentElement.clientHeight, doc.defaultView.innerHeight || 0);
            if (iframeWidth < (options.panelWidth || 400) || iframeHeight < (options.panelHeight || 400)) {
              this._currentPanelField = options.field;
              var options = _.clone(options);
              if (options.payload) {
                options.payload.domain = this.domain;
              }
              options.field = _.clone(options.field);
              delete options.field.element;
              options.panelAsGrowl = true;
              this.messenger.sendToCS('CS', 'panel-as-growl', options);
              return;
            }
          } catch(e){}
        }
        this.updateLastActivity();

        if (options.panelAsGrowl && this.typeFrames && this.typeFrames['card']) {
          this.closeNotification(this.typeFrames['card'])
        }

        options.style = _.extend({}, PANEL_CSS_DEFAULTS, options.style);
        options.document = doc;
        if (!options.ready) {
          options.ready = _.bind(function(iframe){
            if (options.draggable) {
              this.makeDraggable(iframe);
            }
            this.showView(iframe.id, options.view, options.payload);
          }, this)
        }
        var self = this;
        if (options.field) {
          // do not show any field panel on our webapp.
          if (this.domain == ABINE_DNTME.config.webAppHost) {
            return;
          }

          var iframeEls = this.innerFrames;
          options.ready = _.wrap(options.ready, function(ready, iframe){
            delete self._loadingFieldPanel;
            if (CACHE_PANEL_FRAME && options.field && !self._fieldPanelIframe) {
              self._fieldPanelIframe = iframe;
            }
            if (self._currentPanelField != options.field) {
              // user switched to another field.  no need to show panel for this field.
              self.removePanels(iframe.id);
              return;
            }
            if (!options.field.element || $(options.field.element).is(':visible')) {
              self._currentPanelIframe = iframe.id;
              positionPanelToField(self.document, iframe.parentNode, options.field.element, iframeEls);
              ready(iframe);
            } else {
              // field not visible now, hide our panel.
              self._currentPanelField = null;
              self.removePanels(iframe.id);
            }
          });
          this.closeCurrentPanel();
          this._currentPanelField = options.field;
        }
        if(!options.panelWidth){
          options.panelWidth = 400;
        }
        function createPanelImpl(options, retry) {
          self._panelName = options.view;
          self._panelStartTime = Date.now();
          var iframe = null;
          options.parentMessenger = self.messenger;
          if (CACHE_PANEL_FRAME && options.field) {
            if (self._fieldPanelIframe) {
              LOG_FINEST && ABINE_DNTME.log.finest("** reusing panel frame");
              iframe = self._fieldPanelIframe;
              iframe.style.width = options.panelWidth+'px';

              try {
                if (iframe.style.opacity != 1 || iframe.parentNode.style.opacity != 1) {
                  // do not show panel as the site is trying to clickjack.
                  LOG_FINEST && ABINE_DNTME.log.finest("** clickjack attempt, not showing panels");
                  return;
                }
              } catch (e){}

              if (options.panelAsGrowl) {
                iframe.parentNode.style.position = "fixed";
              } else {
                iframe.parentNode.style.position = "absolute";
              }

              $(iframe.parentNode).show();
              abineTimer.setTimeout(function(){options.ready(iframe);}, 0);
              return iframe;
            } else if (self._loadingFieldPanel) {
              if (retry > 10) {
                // we didnt get load event for more than 2 secs.  kill frame and add a new one
                try {
                  var frame = this.document.getElementById(this._oldFrameId);
                  if (frame) {
                    frame.parentNode.parentNode.removeChild(frame.parentNode);
                  }
                } catch(e) {}
              } else {
                // wait for cache panel to load
                abineTimer.setTimeout(function(){createPanelImpl(options, retry+1);}, 20);
                return;
              }
            } else {
              self._loadingFieldPanel = true;
            }
            options.id = 'abine'+Math.floor(Math.random()*100000000)+'doNotRemove';
            this._oldFrameId = options.id;
          }
          var panelReady = function(){
            iframe.parentNode.style.display = '';
            self.messenger.off('panel-ready-'+iframe.id, panelReady);
            abineTimer.setTimeout(function(){options.ready(iframe);}, 0);
          };
          iframe = abineWindow.createPanel(options);
          self.messenger.on('panel-ready-'+iframe.id, panelReady);
          return iframe;
        }
        if (options.secured) {
          if (this.isLocked() && !options.hasDynamicPanel && this.preferences.isPrefTrue(options.secured)) {
            if (!this.canShowPanelHere()) {
              return;
            }
            proxies.memoryStore.get('login-panel-closed', _.bind(function(value) {
              // if closed, do not show login panel for 12 hrs or till browser restart
              if (!value || (parseInt(value)+12*60*60*1000) < Date.now()) {
                if (value) {
                  proxies.memoryStore.set('login-panel-closed', null);
                }
                options.view = 'unlock';
                createPanelImpl(options, 0);
              }
            }, this));
            return;
          }
        }

        return createPanelImpl(options, 0);
      },

      makeDraggable: function(iframe) {
        var container = iframe.parentNode;
        container.style.cursor = 'move';
        container.style.paddingTop = '10px';
        container.style.backgroundColor = '#eee';
        iframe.style.cursor = 'auto';

        container.style.userSelect = 'none';
        var offset = 0;
        function mouseUp() {
          container.ownerDocument.removeEventListener('mousemove', divMove, true);
        }

        function mouseDown(e){
          container.style.left = $(container).position().left + 'px';
          container.style.right = 'auto';
          offset = e.clientX - parseInt(container.style.left);
          container.ownerDocument.addEventListener('mousemove', divMove, true);
        }
        function divMove(e) {
          container.style.top = e.clientY + 'px';
          container.style.left = (e.clientX-offset) + 'px';
        }
        container.addEventListener('mousedown', mouseDown, false);
        container.addEventListener('mouseup', mouseUp, false);
      },

      processDynamicPanelForField: function(field_type){
        var dpString = this.preferences.getPref("dynamicPanels");
        var dynamicPanelsJSON = dpString?JSON.parse(dpString):"";
        var dynamicPanel = false;

        if (dynamicPanelsJSON && dynamicPanelsJSON.panels) {
          dynamicPanel = this.getPanelForDataType(dynamicPanelsJSON.panels, field_type);

          if (dynamicPanel) {
            if(parseInt(dynamicPanel.masked_emails) > 0) {
              dynamicPanel.dontShow = true;
            }

            var showRate = parseFloat(dynamicPanel.show_rate);
            var r = Math.random();
            if (r > showRate) {
              dynamicPanel.dontShow = true;
            }
          }
        }

        return {
          dynamicPanel: dynamicPanel
        }
      },

      closeToolbarPanel: function() {
        if (this.lastDNTMePanelFrame) {
          try {
            this.messenger.sendToCS(this.lastDNTMePanelFrame, 'closeToolbarPanel');
            this.lastDNTMePanelFrame = null;
          } catch(e) {}
        }
      },

      showView: function (frameId, name, payload) {
        this.messenger.sendToCS(frameId, 'init-'+frameId, {
          href: this.href,
          domain: this.domain,
          locked: this._locked,
          message_id: ''+Math.random(),
          view: {name: name, payload: payload}
        });
      },

      showToolbarPanel: function(payload) {
        LOG_FINEST && ABINE_DNTME.log.finest("contentManager:showToolbarPanel enter");

        this.closeToolbarPanel();

        payload = payload || {tabId: this.tabId};

        var postInstallFlow = this.document.getElementById('mmPostInstallClose');
        if (postInstallFlow) {
          $(postInstallFlow).click();
        }

        //if we might want to hide the panel instead, check preferences
        if(payload.hideAfter){
          if (this.preferences.hasPref("clickedClose")){
            var numClicked = this.preferences.getPref("clickedClose");
            this.preferences.setPref("clickedClose", numClicked + 1);
            if(numClicked > payload.hideAfter){
              return
            }
          }else{
            this.preferences.setPref("clickedClose", 1);
          }
        }

        var height = 800;
        var width = payload.showBlurGrowl?350:200;
        var frame = this.createPanel({
          className: "abineDNTMePanel",
          panelWidth: payload.compact && payload.page != 'medals'?width:311,
          panelHeight: height,
          style:{
            borderRadius: '0px',
            height: height + "px",
            position: 'fixed',
            top: "20px",
            right: "20px"
          },
          view: 'blur',
          payload: payload
        });

        this.lastDNTMePanelFrame = frame.id;
      },

      showNotification: function(notification){
        if (!this.topWindow) {
          this.messenger.send("background:show:notification:forward", notification);
          return;
        }
        var self = this;
        LOG_FINEST && ABINE_DNTME.log.finest("contentManager:showNotifications enter");

        if(this.preferences.getPref("showNotifications") && notification && this.domain != ABINE_DNTME.config.webAppHost){
          var countNotifications = parseInt(this.preferences.getPref('countNotifications')) + 1;
          this.preferences.setPref('countNotifications', countNotifications);
          var timeout = countNotifications > 10 ? 3000 : 10000;
          notification.height = notification.height?notification.height:35;

          // close any dntme panel to avoid overlap of notification with dntme panel.
          this.closeToolbarPanel();

          var closeOtherNotifications = false;
          if (notification.type == 'card') {
            var cardNotifications = 1;
            if (this.preferences.hasPref('countCardNotifications')) {
              cardNotifications = parseInt(this.preferences.getPref('countCardNotifications')) + 1;
            }
            if (cardNotifications <= 5) {
              closeOtherNotifications = true;
            }
          } else if (notification.type == 'email' || notification.type == 'post-register') {
            var emailNotifications = 1;
            if (this.preferences.hasPref('countEmailNotifications'))
              emailNotifications = parseInt(this.preferences.getPref('countEmailNotifications')) + 1;
            if (emailNotifications <= 1) {
              closeOtherNotifications = true;
            }
          }
          var frame = this.createPanel({
            className: "abineNotificationPanel",
            panelWidth: notification.panelWidth,
            style:{
              position: 'fixed',
              top: "20px",
              right: "20px"
            },
            view: 'notification',
            payload: {notification: notification, timeout: timeout}
          });

          var type = notification.type;
          if (type.match(/checkout_growl|real_card/)) {
            type = 'card';
          }
          // Detect and close existing growls of same notification type
          if (!this.typeFrames){
            this.typeFrames = {}
          } else if (this.typeFrames[type]) {
            this.closeNotification(this.typeFrames[type]);
          }
          this.typeFrames[type] = frame.id;

          if (!this.notificationFrames) {
            this.notificationFrames = [];
          }

          if (closeOtherNotifications) {
            _.each(this.notificationFrames, function(frameId){
              self.removePanels(frameId);
            });
            this.notificationFrames = [];
          }
          this.notificationFrames.unshift(frame.id);

          var sumHeight = 10;
          _.each(this.notificationFrames, function(frameId){
            if (frameId != frame.id) {
              var node = this.document.getElementById(frameId);
              sumHeight += node.parentElement.offsetHeight+5;
            }
          });

          frame.parentElement.style.top = sumHeight + "px";
        }
      },

      closeNotification: function(frameId) {
        if (!this.notificationFrames) {
          this.removePanels(frameId);
          return;
        }
        var toRemoveIndex = 0;
        for (var i=0;i<this.notificationFrames.length;i++) {
          if(this.notificationFrames[i] == frameId){
            toRemoveIndex = i;
          }
        }
        this.notificationFrames.splice(toRemoveIndex, 1);
        if (this.notificationFrames.length > 0) {
          // update positions
          var top = 10;
          for(var i=this.notificationFrames.length-1;i>=0;i--){
            var frame = this.document.getElementById(this.notificationFrames[i]);
            frame.parentElement.style.top = top + "px";
            top += frame.parentElement.offsetHeight + 5;
          }
        }
        this.removePanels(frameId);
      },

      isPanelShown: function(field) {
        return this._currentPanelField && this._currentPanelField.element == field.element;
      },

      closeCurrentPanel: function(frameId) {
        if (frameId == -1) {
          // message from child iframe
          frameId = this._currentPanelIframe;
        }
        if (frameId && frameId != this._currentPanelIframe) {
          // frame already closed.  so ignore.
          return;
        }
        if (this._currentPanelIframe) {
          this.messenger.sendToCS(this._currentPanelIframe, 'closePanel');
          this._currentPanelIframe = null;
        } else if (!this.topWindow) {
          // panel was shown on top window, forward close call to top window
          this.messenger.sendToCS('CS', 'close-growl-panel', -1);
        }
        this._currentPanelField = null;
      },

      autoResizePanels: function(className, extraWidth, extraHeight, documentHeight, frameId){
        LOG_FINEST && ABINE_DNTME.log.finest("contentManager:autoResizePanels enter");
        abineWindow.autoResizePanels({
          document:this.document,
          className: className,
          extraWidth: extraWidth,
          extraHeight: extraHeight,
          documentHeight: documentHeight,
          frameId: frameId
        });
        LOG_FINEST && ABINE_DNTME.log.finest("contentManager:autoResizePanels exit");
      },

      // ### function removePanels()
      // Remove all iframe panels from the document
      removePanels: function(iframeId) {
        LOG_FINEST && ABINE_DNTME.log.finest("contentManager:removePanels enter "+iframeId);
        if (iframeId) {
          var frame = this.document.getElementById(iframeId);
          if (frame) {
            abineWindow.removePanels({
              document: this.document,
              iframe: frame
            });
          }
        } else {
          abineWindow.removePanels({
            document: this.document
          });
        }
        LOG_FINEST && ABINE_DNTME.log.finest("contentManager:removePanels exit");
      },

      destroy: function() {
        LOG_FINEST && ABINE_DNTME.log.finest("contentManager:destroy enter");

        if (this._fieldPanelIframe) {
          // remove cached field panel
          try {
            this._fieldPanelIframe.parentNode.parentNode.removeChild(this._fieldPanelIframe.parentNode);
          } catch(e) {}
          this._fieldPanelIframe = null;
          this._loadingFieldPanel = false;
        }

        if (this.decorator) {
          this.decorator.cleanup();
        }

        try{this.messenger.off('background:securityManager:locked',this.lockedCB);}catch(e){}
        try{this.messenger.off('background:securityManager:unlocked',this.unlockedCB);}catch(e){}

        try{proxies.disconnect();}catch(e){}

        try{this.disableFormHelpers();}catch(e){}

        if (this.messenger) {
          try{this.messenger.send('contentManager:destroy');}catch(e){}
          this.messenger.off();
          this.messenger.destroy();
          delete this.messenger;
        }
        LOG_FINEST && ABINE_DNTME.log.finest("contentManager:destroy exit");
      }
    });

    function findPos(obj) {
    	var curleft = 0, curtop = 0;
      if (obj.offsetParent) {
        do {
          curleft += obj.offsetLeft;
          curtop += obj.offsetTop;
        } while (obj = obj.offsetParent);
      }
      return {left: curleft, top: curtop};
    }

    // Helper to position the opened panel near the relevant field
    var positionPanelToField = function(doc, panelEl, fieldEl, iframeEls) {
      LOG_FINEST && ABINE_DNTME.log.finest("contentManager:positionPanelToField enter");
      var elOffset;

      if (!fieldEl) {
        panelEl.style.top = '20px';
        panelEl.style.left = 'auto';
        panelEl.style.right = '20px';
        return;
      }

      function addFrameOffset(win) {
        var iframeEl = win.frameElement;
        if (iframeEl) {
          var frameOffset;
          try {
            frameOffset = $(iframeEl).offset();
          } catch (e) {
            frameOffset = findPos(iframeEl);
          }
          elOffset.top += frameOffset.top + 1 - fieldEl.ownerDocument.body.scrollTop;
          elOffset.left += frameOffset.left - fieldEl.ownerDocument.body.scrollLeft;
          if (iframeEl.ownerDocument.defaultView != win.top) {
            addFrameOffset(win.parent);
          }
        }
      }

      try {
        try {
          elOffset = $(fieldEl).offset();
          try {
            // add frame offset when the form element is inside a frame to properly position helper window
            if (fieldEl.ownerDocument != panelEl.ownerDocument) {
              addFrameOffset(fieldEl.ownerDocument.defaultView);
            }
          } catch(e){}
        } catch(e) {
          // failed because it is an inner frame element in chrome
          elOffset = findPos(fieldEl);
          addFrameOffset();
        }
        elOffset.top += $(fieldEl, doc).outerHeight();
      } catch (e) { // This fails in unit tests, unfortunately
        elOffset = {top: 0, left: 0};
      }
      panelEl.style.top = elOffset.top + "px";
      panelEl.style.left = elOffset.left + "px";
      LOG_FINEST && ABINE_DNTME.log.finest("contentManager:positionPanelToField exit");
    };

    return {
      ContentManager: ContentManager
    };

});

ABINE_DNTME.define("abine/formAnalyzer",
    ["documentcloud/underscore", "jquery", 'cryptojs', 'abine/phoneHeuristics', 'abine/paymentHeuristics', 'abine/addressHeuristics', 'abine/url'],
    function(_, $, cryptojs, AbinePhoneHeuristics, AbinePaymentHeuristics, AbineAddressHeuristics, AbineURL) {

  var CryptoJS_SHA1 = cryptojs.SHA1;

  var AbineFormRules = {"Version":60,"Date":"2018-06-14 10:56:37 +0000","FieldRules":{"en-us":{"text":[{"name":".*(captcha|image|word.*verify).*","data_type":"/captcha"},{"name":"^q$|find|search|terms|query","data_type":"/search"},{"name":"hint|password.*reminder","data_type":"/auth/hint"},{"name":"(security|secret)?.*answer","data_type":"/auth/answer"},{"name":"(security|secret)?.*question","data_type":"/auth/question"},{"name":"email|e\\-mail","label":"windowsliveid|email|e\\-mail","negative":"search|friend|reference|recipient|reference|receiver|compose","data_type":"/contact/email"},{"name":".*(?:co|comp|company|business|emp)(?:name)|(?:customer).*(?:company)|(?:org|company).*name|(ship|bill).*company","label":"(?:company|business|organization|emp)(?:\\s+name)?","negative":"card|credit|addr|phone|empty","any_negative":true,"data_type":"/company/name"},{"name":"user|userid|alias|login|uname|yahooid|onlineid|session_key|signon|(?:(?:nick|screen|member|usr|user|account|login|display).*(name|id))","label":"alias|shortname|login|handle|display.*name|user[\\s]*(name|id)|apple[\\s]*id|microsoft[\\s]*account","negative":"credit|password|full|first|last|phone|city|street|ship|bill|country|state|postal|addr|pho|mobile|contact|house|zip|postal|exp|holder|pwd","data_type":"/nameperson/friendly"},{"name":"(?:title|salutation)","data_type":"/nameperson/prefix"},{"name":"(?:lname|last|sur).*(?:name)?","label":"last.*name","negative":"full|number|digit|card|line|street|state|cc(?!ount)|holder","any_negative":true,"data_type":"/nameperson/last"},{"name":"(?:fname|first).*(?:name)?","label":"first.*name","negative":"full|number|digit|card|line|street|state|cc(?!ount)|holder","any_negative":true,"data_type":"/nameperson/first"},{"name":"(?:middle.*name|mname|middle.*initial)","negative":"full|card|cc(?!ount)","data_type":"/nameperson/middle"},{"name":"(?:.*(?:full|invoice|real|bill|(?:ship.*to)).*name)|(?:^name$)","negative":"holder|credit|card|house|street|card|phone|addr|state","data_type":"/nameperson/full"},{"label":"(?:suffix)","negative":"phone","data_type":"/nameperson/suffix"},{"name":"(?:(?:confirm|new|create|re\\-enter)[\\s]*)?password(?:(?:confirm|new|create|re\\-enter)[\\s]*)?","label":"(?:(?:confirm|new|create|choose|re\\-enter)[\\s]*)?password","negative":"hint|old|current","data_type":"/password"},{"name":"(?:old|current)[\\s]*(?:pass|pwd)","label":"[\\s]*(?:(?:current|old)[\\s]*)password","data_type":"/passwordold"},{"label":".*twitter.*","data_type":"/contact/im/twitter"},{"label":".*linkedin.*","data_type":"/contact/web/linkedin"},{"label":".*blog.*","data_type":"/contact/web/blog"},{"label":".*aim","data_type":"/contact/IM/aim"},{"label":".*icq.*","data_type":"/contact/IM/icq"},{"label":".*msn(?:m)?","data_type":"/contact/IM/msn"},{"label":".*jabber","data_type":"/contact/IM/jabber"},{"label":".*skypq","data_type":"/contact/IM/skype"},{"label":".*(?:yim|yahoo)","data_type":"/contact/IM/yahoo"},{"name":"(day|work|business).*phone.*(ext|extension)","negative":"Text|text|addr|cc|card|year|date|dt|mon|qty|quantity","data_type":"/contact/phone/extension/work"},{"name":"(night|home).*phone.*(ext|extension)","negative":"Text|text|addr|cc|card|year|date|dt|mon|qty|quantity","data_type":"/contact/phone/extension/home"},{"name":"phone.*(ext|extension)","label":"^(ext|extension)$","negative":"Text|text|addr|cc|card|year|date|dt|mon|qty|quantity","data_type":"/contact/phone/extension"},{"name":"(day|work|business).*phone.*(area.*code|area).*","negative":"zip|addr|country|qty|quantity","data_type":"/contact/phone/areacode/work"},{"name":"(night|home).*phone.*(area.*code|area).*","negative":"zip|addr|country|qty|quantity","data_type":"/contact/phone/areacode/home"},{"name":"phone.*(area.*code|area).*","negative":"zip|addr|country|qty|quantity","data_type":"/contact/phone/areacode"},{"name":"(day|work|business).*(exch|exchange|prefix|first).*","negative":"addr|qty|quantity","data_type":"/contact/phone/exchange/work"},{"name":"(night|home).*(exch|exchange|prefix|first).*","negative":"addr|qty|quantity","data_type":"/contact/phone/exchange/home"},{"name":"phone.*(exch|exchange|prefix|first).*","negative":"addr|qty|quantity","data_type":"/contact/phone/exchange"},{"name":"(day|work|business).*(suffix|end|last|local).*","negative":"zip|addr|qty|quantity","data_type":"/contact/phone/local/work"},{"name":"(night|home).*(suffix|end|last|local).*","negative":"zip|addr|qty|quantity","data_type":"/contact/phone/local/home"},{"name":"phone.*(suffix|end|last|local).*","negative":"zip|addr|qty|quantity","data_type":"/contact/phone/local"},{"name":"(day|work|business).*(country|countrycode|prefix).*","negative":"addr|qty|quantity","data_type":"/contact/phone/countrycode/work"},{"name":"(night|home).*(country|countrycode|prefix).*","negative":"addr|qty|quantity","data_type":"/contact/phone/countrycode/home"},{"name":"phone.*(country|countrycode|prefix).*","label":"\\+1","negative":"addr|qty|quantity","data_type":"/contact/phone/countrycode"},{"name":".*(cell|mob(ile)?).*","negative":"cancel|date|landline|qty|quantity|secret|otp|street|addr|city|postal|state|[^t]ext","data_type":"/contact/phone/mobile"},{"name":".*fax.*","data_type":"/contact/phone/fax"},{"name":"(day|work|business).*phone","label":"(day|daytime|work|business).*phone","negative":"cancel|date|landline|qty|quantity","data_type":"/contact/phone/work"},{"name":"(night|home).*phone","label":"(night|nighttime|home).*phone","negative":"cancel|date|landline|qty|quantity","data_type":"/contact/phone/home"},{"name":"phone","label":"phone","negative":"ext|qty|quantity","any_negative":true,"data_type":"/contact/phone"},{"name":"(?:ship|shp).*((?:zip|postal|post|pincode)(?:.*code)?)","negative":"slug|tag|city","data_type":"/contact/postalcode/shipping"},{"name":"(cc(?!ount)|bill|pay(?!pal)|card).*((?:zip|postal|post|pincode)(?:.*code)?)|((?:zip|postal|post|pincode)(?:.*code)?).*(cc(?!ount)|bill|pay(?!pal)|card)","negative":"slug|tag|city","data_type":"/contact/postalcode/billing"},{"name":"(?:business).*((?:zip|postal|post|pincode)(?:.*code)?)","negative":"slug|tag|city","data_type":"/contact/postalcode/business"},{"name":"(?:home).*((?:zip|postal|post|pincode)(?:.*code)?)","negative":"slug|tag|city","data_type":"/contact/postalcode/home"},{"name":".*(?:zip|postal|post|pincode)(?:.*code)?","negative":"slug|tag|city","data_type":"/contact/postalcode"},{"name":"(ship|shp).*(city|suburb|locality)","data_type":"/contact/city/shipping"},{"name":"(cc(?!ount)|bill|pay(?!pal)|card).*(city|suburb|locality)|(city|suburb|locality).*(cc(?!ount)|bill|pay(?!pal)|card)","data_type":"/contact/city/billing"},{"name":"business.*(city|suburb|locality)","data_type":"/contact/city/business"},{"name":"home.*(city|suburb|locality)","data_type":"/contact/city/home"},{"name":"(city|suburb|locality)","negative":"team","data_type":"/contact/city"},{"name":"(ship|shp).*country","negative":"card","data_type":"/contact/country/shipping"},{"name":"(cc(?!ount)|bill|pay(?!pal)|card).*(country)|(country).*(cc(?!ount)|bill|pay(?!pal)|card)","data_type":"/contact/country/billing"},{"name":"business.*country","negative":"card","data_type":"/contact/country/business"},{"name":"home.*country","negative":"card","data_type":"/contact/country/home"},{"name":"country","negative":"card","data_type":"/contact/country"},{"name":"(ship|shp).*(state|province|zone|region)","data_type":"/contact/state/shipping"},{"name":"(cc(?!ount)|bill|pay(?!pal)|card).*(state|province|zone|region)|(state|province|zone|region).*(cc(?!ount)|bill|pay(?!pal)|card)","data_type":"/contact/state/billing"},{"name":"business.*(state|province|zone|region)","data_type":"/contact/state/business"},{"name":"home.*(state|province|zone|region)","data_type":"/contact/state/home"},{"name":"state|province|zone|region","negative":"united|statement|country","data_type":"/contact/state"},{"name":"(ship|shp).*((?:addr.*(2|two|apart|apt|second))|apartment|suite|(?:(?:ad.*)?line.*(2|two|aprt|apt|second))|street.*(2|cont|opt))","data_type":"/contact/postaladdressAdditional/shipping"},{"name":"(cc(?!ount)|bill|pay(?!pal)|card).*((?:addr.*(2|two|apart|apt|second))|apartment|suite|(?:(?:ad.*)?line.*(2|two|aprt|apt|second))|street.*(2|cont|opt))","data_type":"/contact/postaladdressAdditional/billing"},{"name":"business.*((?:addr.*(2|two|apart|apt|second))|apartment|suite|(?:(?:ad.*)?line.*(2|two|aprt|apt|second))|street.*(2|cont|opt))","data_type":"/contact/postaladdressAdditional/business"},{"name":"home.*((?:addr.*(2|two|apart|apt|second))|apartment|suite|(?:(?:ad.*)?line.*(2|two|aprt|apt|second))|street.*(2|cont|opt))","data_type":"/contact/postaladdressAdditional/home"},{"name":"(?:addr.*(2|two|apart|apt|second))|apartment|suite|(?:(?:ad.*)?line.*(2|two|aprt|apt|second))|street.*(2|cont|opt)","label":".*address.*(?:line)?.*2.*","negative":"1|message","any_negative":true,"data_type":"/contact/postaladdressAdditional"},{"name":"(ship|shp).*((?:addr.*(3|three))|(?:(?:ad.*)?line.*(3|three)))","data_type":"/contact/postaladdress3/shipping"},{"name":"bill.*((?:addr.*(3|three))|(?:(?:ad.*)?line.*(3|three)))","data_type":"/contact/postaladdress3/billing"},{"name":"business.*((?:addr.*(3|three))|(?:(?:ad.*)?line.*(3|three)))","data_type":"/contact/postaladdress3/business"},{"name":"home.*((?:addr.*(3|three))|(?:(?:ad.*)?line.*(3|three)))","data_type":"/contact/postaladdress3/home"},{"name":"((?:addr.*(3|three))|(?:(?:ad.*)?line.*(3|three)))","label":".*address.*(?:line)?.*3.*","negative":"message","any_negative":true,"data_type":"/contact/postaladdress3"},{"name":"(ship|shp).*((?:addr.*(1|one|first)?)|(?:(?:ad.*)?line.*(1|one|first)?)|street(.*1)?)","negative":"apt|apart|(^(?=street.*))name","data_type":"/contact/postaladdress/shipping"},{"name":"(cc(?!ount)|bill|pay(?!pal)|card).*((?:addr.*(1|one|first)?)|(?:(?:ad.*)?line.*(1|one|first)?)|street(.*1)?)","negative":"apt|apart|(^(?=street.*))name","data_type":"/contact/postaladdress/billing"},{"name":"business.*((?:addr.*(1|one|first)?)|(?:(?:ad.*)?line.*(1|one|first)?)|street(.*1)?)","negative":"apt|apart|(^(?=street.*))name","data_type":"/contact/postaladdress/business"},{"name":"home.*((?:addr.*(1|one|first)?)|(?:(?:ad.*)?line.*(1|one|first)?)|street(.*1)?)","negative":"apt|apart|(^(?=street.*))name","data_type":"/contact/postaladdress/home"},{"name":"(?:addr.*(1|one|first)?)|(?:(?:ad.*)?line.*(1|one|first)?)|street(.*1)?","label":".*address.*(?:line.*1?)?.*","negative":"type|apt|apart|extension|(^(?=street.*))name|tag|recipient|nick|message","any_negative":true,"data_type":"/contact/postaladdress"},{"name":".*(?:comment|message|body).*","data_type":"/message"},{"name":".*(?:subject).*","data_type":"/subject"},{"name":"((cc|card).*first.*name)","label":"first.*name.*card","negative":"gift|placeholder|account|acct|num|cvv|note|sec.*code|occa|street|suite","data_type":"/financial/creditcard/issuedto/first"},{"name":"((cc|card).*last.*name)","label":"last.*name.*card","negative":"gift|placeholder|account|acct|num|cvv|note|sec.*code|occa|street|suite","data_type":"/financial/creditcard/issuedto/last"},{"name":".*(?:(?:(?:cc|card)?.*(?:(?:holder|owner).*)))|(?:name.*on.*card)|(?:issued.*to)|(?:(?:card|cc|payment).*name)","label":"(?:issued.*to)|(?:name.*on.*card)|(?:card.*name)|(?:card.*holder)","negative":"gift|placeholder|account|acct|first|last|num|cvv|note|sec.*code|occa|street|suite","data_type":"/financial/creditcard/issuedto"},{"name":"cc.*(cvc|code|cvv|ccv)|.*(?:cc|card|credit.*(card)?)?(?:(?:cvv|cvc|cvn|ccv|csc|cv|cid|verify|verif.*(?:code|num)|verification|sec.*code|card.*sec.*num)|cvv|csc|cvc|cvn|ccv).*","negative":"gift|pin|discount|promo|job|redeem|validation|area|access|pass|exp|occa|ssh","data_type":"/financial/creditcard/verification"},{"name":"card.*last","negative":"gift|account|reward|acct|issue|address|note|occa|phone|license","data_type":"/financial/creditcard/number/part4"},{"name":".*(?:cc|card|credit.*card|payment).*(?:num|number|no)|ccard","label":"card.*number","negative":"gift|account|reward|acct|issue|address|note|occa|last|verif|phone|house|street|suite|license","data_type":"/financial/creditcard/number"},{"name":".*(?:cc|card|credit.*card).*(?:type|issuer|brand)","negative":"gift|occa","data_type":"/financial/creditcard/type"},{"name":".*(?:(?:(?:cc|card|billing)?.*(?:exp.*))|(?:(?:cc|card).*(?:exp.*)?)).*(?:date.*)?(?:[m]{2}|mon|month|mn)","negative":"gift|start|valid|birth|amount|occa|year|yy|express","data_type":"/financial/creditcard/expirymonth"},{"name":".*(?:(?:(?:cc|card|billing)?.*(?:exp.*))|(?:(?:cc|card).*(?:exp.*)?)).*(?:date.*)?(?:[y]{2,4}|yr|year)","negative":"gift|start|valid|birth|amount|occa|mm|mon|express","data_type":"/financial/creditcard/expiryyear"},{"label":"expir.*mm.*yy\\b|(mm/yy$)","negative":"gift|year|yr|mon|start|valid|birth","data_type":"/financial/creditcard/expiry2"},{"name":".*(?:(?:(?:cc|card|billing)?.*(?:exp.*))|(?:(?:cc|card).*(?:exp.*))|expiration)","label":"expir.*date","negative":"gift|year|yr|mon|mm(^(?=.*yy))|(^(?=mm.*))yy|start|valid|birth|occa|street|express","data_type":"/financial/creditcard/expiry"},{"name":"(?:birth.*year)|(?:dob[y]+)|(?:(?:dob|date).*year.*)|(?:year.*(?:born|birth))|(?:birthday[y]+r?)","negative":"exp","data_type":"/birthdate/birthyear"},{"name":"(?:birth.*mon(?:th)?)|(?:dob[m]+)|(?:(?:dob|date).*mon(?:th)?.*)|(?:month.*(?:born|birth))|(?:birthday[m]+)","negative":"exp","data_type":"/birthdate/birthmonth"},{"name":"(?:birth.*day)|(?:dob[d]+)|(?:(?:dob|date).*(?:day|date).*)|(?:day.*(?:born|birth))|(?:birthday[d]+)","data_type":"/birthdate/birthday"},{"name":"(?:b|birth|dob)(?:day|date)","data_type":"/birthdate"},{"name":"^pin$","label":"^pin$","data_type":"/pin"}],"password":[{"name":".*(captcha|word.*verify).*","data_type":"/captcha"},{"name":"cc.*(cvc|code|cvv|ccv)|.*(?:cc|card|credit.*card)?(?:(?:cvv|cvc|ccv|csc|cv|cid|verify|verif.*(?:code|num)|verification|sec.*code|card.*sec.*num)|cvv|csc|cvc|ccv).*","negative":"gift|password|pass|pin|discount|ssn","data_type":"/financial/creditcard/verification"},{"name":"pin","label":"pin","data_type":"/pin"},{"name":"(security|secret)?.*answer","data_type":"/auth/answer"},{"name":"(?:(?:confirm|new|create|re\\-enter|verify)[\\s]*)?password(?:(?:confirm|new|create|re\\-enter)[\\s]*)?","label":"(?:(?:confirm|new|create|choose|re\\-enter)[\\s]*)?password","negative":"hint|old|current","data_type":"/password"},{"name":"(?:old|current)[\\s]*(?:pass|pwd)","label":"[\\s]*(?:(?:current|old)[\\s]*)password","data_type":"/passwordold"}],"select":[{"name":"(security|secret)?.*question","data_type":"/auth/question"},{"name":"(?:title|salutation)","data_type":"/nameperson/prefix"},{"name":"(cc(?!ount)|bill|pay(?!pal)|card).*(state|province|zone|region)|(state|province|zone|region).*(cc(?!ount)|bill|pay(?!pal)|card)","data_type":"/contact/state/billing"},{"name":"(ship|shp).*(state|province|zone|region)","data_type":"/contact/state/shipping"},{"name":"busi.*(state|province|zone|region)","data_type":"/contact/state/business"},{"name":"home.*(state|province|zone|regioon)","data_type":"/contact/state/home"},{"name":"state|province|zone|region","negative":"usa|canada|italy|united|country|statement","options":"AK|FL|Arizona","negative_options":"usa|canada|italy|statement","data_type":"/contact/state"},{"name":"(cc(?!ount)|bill|pay(?!pal)|card).*(country)|(country).*(cc(?!ount)|bill|pay(?!pal)|card)","negative":"phone","data_type":"/contact/country/billing"},{"name":"(ship|shp).*country","negative":"phone","data_type":"/contact/country/shipping"},{"name":"busi.*country","negative":"phone","data_type":"/contact/country/business"},{"name":"home.*country","negative":"phone","data_type":"/contact/country/home"},{"name":"country|united","negative":"phone|language|airport","any_negative":true,"options":"united states|italy|usa|(\\bus\\b)","negative_options":"Arizona","data_type":"/contact/country"},{"name":".*(?:cc|card|credit.*card|payment).*(?:type|issuer|brand)","negative":"gift","options":"visa|diner|amex|american[^\u003c]*express","data_type":"/financial/creditcard/type"},{"name":".*(?:(?:(?:cc|card|billing|payment)?.*(?:exp.*))|(?:(?:cc|card).*(?:exp.*)?)|exp.*)(?:date.*)?(?:[m]{2}|mon|month|mn)","negative":"gift|start|valid|birth|amount|occa|express","data_type":"/financial/creditcard/expirymonth"},{"name":".*(?:(?:(?:cc|card|billing|payment)?.*(?:exp.*))|(?:(?:cc|card).*(?:exp.*)?)|exp.*)(?:date.*)?(?:[y]{2,4}|yr|year)","negative":"gift|start|valid|birth|amount|occa|express","data_type":"/financial/creditcard/expiryyear"},{"name":"(?:birth.*year)|(?:dob[y]+)|(?:(?:dob|date).*year.*)|(?:year.*(?:born|birth))|(?:birthday[y]+r?)","negative":"exp","data_type":"/birthdate/birthyear"},{"name":"(?:birth.*mon(?:th)?)|(?:dob[m]+)|(?:(?:dob|date).*mon(?:th)?.*)|(?:month.*(?:born|birth))|(?:birthday[m]+)","negative":"exp","data_type":"/birthdate/birthmonth"},{"name":"(?:birth.*day)|(?:dob[d]+)|(?:(?:dob|date).*(?:day|date).*)|(?:day.*(?:born|birth))|(?:birthday[d]+)","data_type":"/birthdate/birthday"},{"name":"(?:b|birth|dob)(?:day|date)","data_type":"/birthdate"},{"name":"gender|sex","data_type":"/person/gender"},{"name":"(day|work|business).*(country|countrycode).*","data_type":"/contact/phone/countrycode/work"},{"name":"(night|home).*(country|countrycode).*","data_type":"/contact/phone/countrycode/home"},{"name":"phone.*(country|countrycode).*","data_type":"/contact/phone/countrycode"}],"checkbox":[{"name":"(?:accept|tos|agree|terms|(?:read)?rules|conditions|privacypolicy).*","data_type":"/agreetos"},{"name":".*(?:remember|persistent).*","label":"keepmesignedin","data_type":"/rememberme"}],"radio":[{"name":"gender|sex","label":"^(?:male|female)$","data_type":"/person/gender"}]},"de-de":{"text":[{"label":"captcha","data_type":"/captcha"},{"label":"suche","data_type":"/search"},{"label":"hinweis","data_type":"/auth/hint"},{"label":"antworten","data_type":"/auth/answer"},{"label":"frage|herausforderung","data_type":"/auth/question"},{"label":"email","data_type":"/contact/email"},{"label":"unternehmen|name der firma","data_type":"/company/name"},{"label":"freundlich|benutzername|anmeldung","data_type":"/nameperson/friendly"},{"label":"präfix","data_type":"/nameperson/prefix"},{"label":"letzte|familienname|nachname","negative":"zuerst","data_type":"/nameperson/last"},{"label":"zuerst","negative":"letzte","data_type":"/nameperson/first"},{"label":"mitte|zweiter vorname","negative":"letzte|zuerst","data_type":"/nameperson/middle"},{"label":"vollständiger name","data_type":"/nameperson/full"},{"label":"suffix","data_type":"/nameperson/suffix"},{"label":"passwort","data_type":"/password"},{"label":"kennwort alt","data_type":"/passwordold"},{"label":"twitter","data_type":"/contact/im/twitter"},{"label":"linkedin|verknüpft","data_type":"/contact/web/linkedin"},{"label":"blog","data_type":"/contact/web/blog"},{"label":"ziel|aol instant messenger","data_type":"/contact/IM/aim"},{"label":"icq","data_type":"/contact/IM/icq"},{"label":"msn","data_type":"/contact/IM/msn"},{"label":"quasseln","data_type":"/contact/IM/jabber"},{"label":"skype","data_type":"/contact/IM/skype"},{"label":"yahoo","data_type":"/contact/IM/yahoo"},{"label":"arbeitshandy|geschäftstelefon","data_type":"/contact/phone/extension/work"},{"label":"haupttelefonerweiterung","data_type":"/contact/phone/extension/home"},{"label":"erweiterung","data_type":"/contact/phone/extension"},{"label":"vorwahl","data_type":"/contact/phone/areacode/work"},{"label":"vorwahl","data_type":"/contact/phone/areacode"},{"label":"telefon","data_type":"/contact/phone/local/work"},{"label":"telefon","data_type":"/contact/phone/local/home"},{"label":"telefon","data_type":"/contact/phone/local"},{"label":"arbeits-ländercode","data_type":"/contact/phone/countrycode/work"},{"label":"landesvorwahl","data_type":"/contact/phone/countrycode/home"},{"label":"landesvorwahl","data_type":"/contact/phone/countrycode"},{"label":"mobiltelefon|zelle","data_type":"/contact/phone/mobile"},{"label":"fax","data_type":"/contact/phone/fax"},{"label":"arbeitshandy|geschäftstelefon|unternehmenstelefon|geschäftslinie","data_type":"/contact/phone/work"},{"label":"haustelefon","data_type":"/contact/phone/home"},{"label":"telefon","data_type":"/contact/phone"},{"label":"versand postleitzahl","data_type":"/contact/postalcode/shipping"},{"label":"postleitzahl","data_type":"/contact/postalcode/billing"},{"label":"geschäft postleitzahl|arbeiten postleitzahl","data_type":"/contact/postalcode/business"},{"label":"haus-postleitzahl|home postleitzahl","data_type":"/contact/postalcode/home"},{"label":"postleitzahl","data_type":"/contact/postalcode"},{"label":"versand stadt","data_type":"/contact/city/shipping"},{"label":"rechnungsstadt","data_type":"/contact/city/billing"},{"label":"geschäftsstadt","data_type":"/contact/city/business"},{"label":"heimatstadt","data_type":"/contact/city/home"},{"label":"stadt","data_type":"/contact/city"},{"label":"versandland","data_type":"/contact/country/shipping"},{"label":"land der rechnungsadresse","data_type":"/contact/country/billing"},{"label":"wirtschaftsland","data_type":"/contact/country/business"},{"label":"heimatland","data_type":"/contact/country/home"},{"label":"land","data_type":"/contact/country"},{"label":"versandzustand","data_type":"/contact/state/shipping"},{"label":"abrechnungsstatus","data_type":"/contact/state/billing"},{"label":"betriebswirtschaft","data_type":"/contact/state/business"},{"label":"heimatstaat","data_type":"/contact/state/home"},{"label":"bundesland","data_type":"/contact/state"},{"label":"zzgl. versandkosten|adresszeile 2|geeignet|suite","data_type":"/contact/postaladdressAdditional/shipping"},{"label":"abrechnung plus 4|abrechnungslinie 2|geeignet|suite","data_type":"/contact/postaladdressAdditional/billing"},{"label":"geschäft plus 4","data_type":"/contact/postaladdressAdditional/business"},{"label":"haus plus 4","data_type":"/contact/postaladdressAdditional/home"},{"label":"plus 4","data_type":"/contact/postaladdressAdditional"},{"label":"lieferanschrift","data_type":"/contact/postaladdress/shipping"},{"label":"rechnungsadresse","data_type":"/contact/postaladdress/billing"},{"label":"geschäftsadresse","data_type":"/contact/postaladdress/business"},{"label":"heimatadresse","data_type":"/contact/postaladdress/home"},{"label":"adresse","data_type":"/contact/postaladdress"},{"label":"nachricht","data_type":"/message"},{"label":"fach","data_type":"/subject"},{"label":"vorname auf der kreditkarte","data_type":"/financial/creditcard/issuedto/first"},{"label":"nachname auf der kreditkarte","data_type":"/financial/creditcard/issuedto/last"},{"label":"kreditkartenaussteller","data_type":"/financial/creditcard/issuedto"},{"label":"kreditkarte cvv","data_type":"/financial/creditcard/verification"},{"label":"kreditkartennummer teil 1","data_type":"/financial/creditcard/number/part1"},{"label":"kreditkartennummer teil 2","data_type":"/financial/creditcard/number/part2"},{"label":"kreditkartennummer teil 3","data_type":"/financial/creditcard/number/part3"},{"label":"kreditkartennummer teil 4","data_type":"/financial/creditcard/number/part4"},{"label":"kreditkartennummer","data_type":"/financial/creditcard/number"},{"label":"kreditkartentyp","data_type":"/financial/creditcard/type"},{"label":"kreditkartenablaufmonat|karte läuft ab (monat)","data_type":"/financial/creditcard/expirymonth"},{"label":"ablaufjahr","data_type":"/financial/creditcard/expiryyear"},{"label":"monat und jahr","data_type":"/financial/creditcard/expiry2"},{"label":"ablauf","data_type":"/financial/creditcard/expiry"},{"label":"geburtsjahr","data_type":"/birthdate/birthyear"},{"label":"geburtsmonat","data_type":"/birthdate/birthmonth"},{"label":"geburtstag","data_type":"/birthdate/birthday"},{"label":"geburtsdatum|geburtstag","data_type":"/birthdate"},{"label":"stift","data_type":"/pin"},{"label":"geschlecht","data_type":"/person/gender"},{"label":"zustimmen","data_type":"/agreetos"},{"label":"erinnere dich an mich","data_type":"/rememberme"}],"password":[{"label":"passwort","data_type":"/password"},{"label":"kennwort alt","data_type":"/passwordold"},{"label":"kreditkarte cvv","data_type":"/financial/creditcard/verification"},{"label":"stift","data_type":"/pin"}],"select":[{"label":"arbeits-ländercode","data_type":"/contact/phone/countrycode/work"},{"label":"landesvorwahl","data_type":"/contact/phone/countrycode/home"},{"label":"landesvorwahl","data_type":"/contact/phone/countrycode"},{"label":"versandland","data_type":"/contact/country/shipping"},{"label":"land der rechnungsadresse","data_type":"/contact/country/billing"},{"label":"wirtschaftsland","data_type":"/contact/country/business"},{"label":"heimatland","data_type":"/contact/country/home"},{"label":"land","data_type":"/contact/country"},{"label":"versandzustand","data_type":"/contact/state/shipping"},{"label":"abrechnungsstatus","data_type":"/contact/state/billing"},{"label":"betriebswirtschaft","data_type":"/contact/state/business"},{"label":"heimatstaat","data_type":"/contact/state/home"},{"label":"bundesland","data_type":"/contact/state"},{"label":"kreditkartentyp","data_type":"/financial/creditcard/type"},{"label":"kreditkartenablaufmonat|karte läuft ab (monat)","data_type":"/financial/creditcard/expirymonth"},{"label":"ablaufjahr","data_type":"/financial/creditcard/expiryyear"},{"label":"monat und jahr","data_type":"/financial/creditcard/expiry2"},{"label":"ablauf","data_type":"/financial/creditcard/expiry"},{"label":"geburtsjahr","data_type":"/birthdate/birthyear"},{"label":"geburtsmonat","data_type":"/birthdate/birthmonth"},{"label":"geburtstag","data_type":"/birthdate/birthday"},{"label":"geburtsdatum|geburtstag","data_type":"/birthdate"}]},"es-es":{"text":[{"label":"captcha","data_type":"/captcha"},{"label":"buscar","data_type":"/search"},{"label":"insinuación","data_type":"/auth/hint"},{"label":"responder","data_type":"/auth/answer"},{"label":"pregunta|reto","data_type":"/auth/question"},{"label":"correo electrónico","data_type":"/contact/email"},{"label":"empresa|nombre de empresa","data_type":"/company/name"},{"label":"amistoso|nombre de usuario|iniciar sesión","data_type":"/nameperson/friendly"},{"label":"prefijo","data_type":"/nameperson/prefix"},{"label":"último|apellido","negative":"primero","data_type":"/nameperson/last"},{"label":"primero","negative":"último","data_type":"/nameperson/first"},{"label":"medio|segundo nombre","negative":"último|primero","data_type":"/nameperson/middle"},{"label":"nombre completo","data_type":"/nameperson/full"},{"label":"sufijo","data_type":"/nameperson/suffix"},{"label":"contraseña","data_type":"/password"},{"label":"contraseña antigua","data_type":"/passwordold"},{"label":"gorjeo","data_type":"/contact/im/twitter"},{"label":"vinculado|vinculado en","data_type":"/contact/web/linkedin"},{"label":"blog","data_type":"/contact/web/blog"},{"label":"objetivo|mensajero instantáneo aol","data_type":"/contact/IM/aim"},{"label":"icq","data_type":"/contact/IM/icq"},{"label":"msn","data_type":"/contact/IM/msn"},{"label":"farfullar","data_type":"/contact/IM/jabber"},{"label":"skype","data_type":"/contact/IM/skype"},{"label":"yahoo","data_type":"/contact/IM/yahoo"},{"label":"teléfono del trabajo|teléfono de negocios","data_type":"/contact/phone/extension/work"},{"label":"extensión del teléfono casero","data_type":"/contact/phone/extension/home"},{"label":"extensión","data_type":"/contact/phone/extension"},{"label":"codigo de area","data_type":"/contact/phone/areacode/work"},{"label":"codigo de area","data_type":"/contact/phone/areacode"},{"label":"teléfono","data_type":"/contact/phone/local/work"},{"label":"teléfono","data_type":"/contact/phone/local/home"},{"label":"teléfono","data_type":"/contact/phone/local"},{"label":"código de país de trabajo","data_type":"/contact/phone/countrycode/work"},{"label":"código del país de origen","data_type":"/contact/phone/countrycode/home"},{"label":"código de país","data_type":"/contact/phone/countrycode"},{"label":"teléfono móvil|celda","data_type":"/contact/phone/mobile"},{"label":"fax","data_type":"/contact/phone/fax"},{"label":"teléfono del trabajo|teléfono de negocios|teléfono corporativo|línea de negocios","data_type":"/contact/phone/work"},{"label":"teléfono de casa","data_type":"/contact/phone/home"},{"label":"teléfono","data_type":"/contact/phone"},{"label":"código postal de envío","data_type":"/contact/postalcode/shipping"},{"label":"código postal de facturación","data_type":"/contact/postalcode/billing"},{"label":"código postal comercial|código postal trabajo","data_type":"/contact/postalcode/business"},{"label":"código postal de origen|código postal de su casa","data_type":"/contact/postalcode/home"},{"label":"código postal","data_type":"/contact/postalcode"},{"label":"ciudad de envío","data_type":"/contact/city/shipping"},{"label":"ciudad de facturación","data_type":"/contact/city/billing"},{"label":"ciudad de negocios","data_type":"/contact/city/business"},{"label":"ciudad de origen","data_type":"/contact/city/home"},{"label":"ciudad","data_type":"/contact/city"},{"label":"país de envío","data_type":"/contact/country/shipping"},{"label":"país de facturación","data_type":"/contact/country/billing"},{"label":"país de negocios","data_type":"/contact/country/business"},{"label":"país de origen","data_type":"/contact/country/home"},{"label":"país","data_type":"/contact/country"},{"label":"estado de envío","data_type":"/contact/state/shipping"},{"label":"estado de cuenta","data_type":"/contact/state/billing"},{"label":"estado de negocios","data_type":"/contact/state/business"},{"label":"estado natal","data_type":"/contact/state/home"},{"label":"estado","data_type":"/contact/state"},{"label":"envío más 4|dirección línea 2|apto|suite","data_type":"/contact/postaladdressAdditional/shipping"},{"label":"facturación más 4|línea de facturación 2|apto|suite","data_type":"/contact/postaladdressAdditional/billing"},{"label":"business plus 4","data_type":"/contact/postaladdressAdditional/business"},{"label":"casa más 4","data_type":"/contact/postaladdressAdditional/home"},{"label":"más 4","data_type":"/contact/postaladdressAdditional"},{"label":"dirección de envío","data_type":"/contact/postaladdress/shipping"},{"label":"dirección de envio","data_type":"/contact/postaladdress/billing"},{"label":"dirección de negocios","data_type":"/contact/postaladdress/business"},{"label":"direccion de casa","data_type":"/contact/postaladdress/home"},{"label":"dirección","data_type":"/contact/postaladdress"},{"label":"mensaje","data_type":"/message"},{"label":"tema","data_type":"/subject"},{"label":"nombre en la tarjeta de crédito","data_type":"/financial/creditcard/issuedto/first"},{"label":"apellido en tarjeta de crédito","data_type":"/financial/creditcard/issuedto/last"},{"label":"emisor de tarjeta de crédito","data_type":"/financial/creditcard/issuedto"},{"label":"tarjeta de crédito cvv","data_type":"/financial/creditcard/verification"},{"label":"número de tarjeta de crédito parte 1","data_type":"/financial/creditcard/number/part1"},{"label":"número de tarjeta de crédito parte 2","data_type":"/financial/creditcard/number/part2"},{"label":"número de tarjeta de crédito parte 3","data_type":"/financial/creditcard/number/part3"},{"label":"número de tarjeta de crédito parte 4","data_type":"/financial/creditcard/number/part4"},{"label":"número de tarjeta de crédito","data_type":"/financial/creditcard/number"},{"label":"tipo de tarjeta de crédito","data_type":"/financial/creditcard/type"},{"label":"mes de vencimiento de la tarjeta de crédito|tarjeta expira (mes)","data_type":"/financial/creditcard/expirymonth"},{"label":"año de vencimiento","data_type":"/financial/creditcard/expiryyear"},{"label":"mes y año","data_type":"/financial/creditcard/expiry2"},{"label":"vencimiento","data_type":"/financial/creditcard/expiry"},{"label":"año de nacimiento","data_type":"/birthdate/birthyear"},{"label":"mes de nacimiento","data_type":"/birthdate/birthmonth"},{"label":"día de nacimiento","data_type":"/birthdate/birthday"},{"label":"fecha de nacimiento|cumpleaños","data_type":"/birthdate"},{"label":"alfiler","data_type":"/pin"},{"label":"género","data_type":"/person/gender"},{"label":"de acuerdo","data_type":"/agreetos"},{"label":"recuérdame","data_type":"/rememberme"}],"password":[{"label":"contraseña","data_type":"/password"},{"label":"contraseña antigua","data_type":"/passwordold"},{"label":"tarjeta de crédito cvv","data_type":"/financial/creditcard/verification"},{"label":"alfiler","data_type":"/pin"}],"select":[{"label":"código de país de trabajo","data_type":"/contact/phone/countrycode/work"},{"label":"código del país de origen","data_type":"/contact/phone/countrycode/home"},{"label":"código de país","data_type":"/contact/phone/countrycode"},{"label":"país de envío","data_type":"/contact/country/shipping"},{"label":"país de facturación","data_type":"/contact/country/billing"},{"label":"país de negocios","data_type":"/contact/country/business"},{"label":"país de origen","data_type":"/contact/country/home"},{"label":"país","data_type":"/contact/country"},{"label":"estado de envío","data_type":"/contact/state/shipping"},{"label":"estado de cuenta","data_type":"/contact/state/billing"},{"label":"estado de negocios","data_type":"/contact/state/business"},{"label":"estado natal","data_type":"/contact/state/home"},{"label":"estado","data_type":"/contact/state"},{"label":"tipo de tarjeta de crédito","data_type":"/financial/creditcard/type"},{"label":"mes de vencimiento de la tarjeta de crédito|tarjeta expira (mes)","data_type":"/financial/creditcard/expirymonth"},{"label":"año de vencimiento","data_type":"/financial/creditcard/expiryyear"},{"label":"mes y año","data_type":"/financial/creditcard/expiry2"},{"label":"vencimiento","data_type":"/financial/creditcard/expiry"},{"label":"año de nacimiento","data_type":"/birthdate/birthyear"},{"label":"mes de nacimiento","data_type":"/birthdate/birthmonth"},{"label":"día de nacimiento","data_type":"/birthdate/birthday"},{"label":"fecha de nacimiento|cumpleaños","data_type":"/birthdate"}]},"fr-fr":{"text":[{"label":"captcha","data_type":"/captcha"},{"label":"chercher","data_type":"/search"},{"label":"conseil","data_type":"/auth/hint"},{"label":"répondre","data_type":"/auth/answer"},{"label":"question|défi","data_type":"/auth/question"},{"label":"email","data_type":"/contact/email"},{"label":"compagnie|nom de la compagnie","data_type":"/company/name"},{"label":"amicale|nom d'utilisateur|s'identifier","data_type":"/nameperson/friendly"},{"label":"préfixe","data_type":"/nameperson/prefix"},{"label":"dernier|nom de famille","negative":"premier","data_type":"/nameperson/last"},{"label":"premier","negative":"dernier","data_type":"/nameperson/first"},{"label":"milieu|deuxième nom","negative":"dernier|premier","data_type":"/nameperson/middle"},{"label":"nom complet","data_type":"/nameperson/full"},{"label":"suffixe","data_type":"/nameperson/suffix"},{"label":"mot de passe","data_type":"/password"},{"label":"mot de passe ancien","data_type":"/passwordold"},{"label":"gazouillement","data_type":"/contact/im/twitter"},{"label":"lié|lié dans|lié à","data_type":"/contact/web/linkedin"},{"label":"blog","data_type":"/contact/web/blog"},{"label":"avoir pour but|messager instantané aol","data_type":"/contact/IM/aim"},{"label":"icq","data_type":"/contact/IM/icq"},{"label":"msn","data_type":"/contact/IM/msn"},{"label":"jacasser","data_type":"/contact/IM/jabber"},{"label":"skype","data_type":"/contact/IM/skype"},{"label":"yahoo","data_type":"/contact/IM/yahoo"},{"label":"téléphone de travail","data_type":"/contact/phone/extension/work"},{"label":"extension de téléphone à la maison","data_type":"/contact/phone/extension/home"},{"label":"extension","data_type":"/contact/phone/extension"},{"label":"indicatif régional","data_type":"/contact/phone/areacode/work"},{"label":"indicatif régional","data_type":"/contact/phone/areacode"},{"label":"téléphone","data_type":"/contact/phone/local/work"},{"label":"téléphone","data_type":"/contact/phone/local/home"},{"label":"téléphone","data_type":"/contact/phone/local"},{"label":"code du pays de travail","data_type":"/contact/phone/countrycode/work"},{"label":"code du pays de résidence","data_type":"/contact/phone/countrycode/home"},{"label":"code postal","data_type":"/contact/phone/countrycode"},{"label":"téléphone portable|cellule","data_type":"/contact/phone/mobile"},{"label":"fax","data_type":"/contact/phone/fax"},{"label":"téléphone de travail|téléphone d'entreprise|secteur d'activité","data_type":"/contact/phone/work"},{"label":"téléphone fixe","data_type":"/contact/phone/home"},{"label":"téléphone","data_type":"/contact/phone"},{"label":"code postal de livraison|code postal d'expédition","data_type":"/contact/postalcode/shipping"},{"label":"code postal de facturation","data_type":"/contact/postalcode/billing"},{"label":"code postal professionnel|code postal de travail","data_type":"/contact/postalcode/business"},{"label":"code postal de la maison|code postal domicile","data_type":"/contact/postalcode/home"},{"label":"code postal","data_type":"/contact/postalcode"},{"label":"ville d'expédition","data_type":"/contact/city/shipping"},{"label":"ville de facturation","data_type":"/contact/city/billing"},{"label":"ville d'affaires","data_type":"/contact/city/business"},{"label":"ville natale","data_type":"/contact/city/home"},{"label":"ville","data_type":"/contact/city"},{"label":"pays de livraison","data_type":"/contact/country/shipping"},{"label":"pays de facturation","data_type":"/contact/country/billing"},{"label":"pays d'affaires","data_type":"/contact/country/business"},{"label":"pays d'origine","data_type":"/contact/country/home"},{"label":"pays","data_type":"/contact/country"},{"label":"État de livraison","data_type":"/contact/state/shipping"},{"label":"État de facturation","data_type":"/contact/state/billing"},{"label":"État des affaires","data_type":"/contact/state/business"},{"label":"État de résidence","data_type":"/contact/state/home"},{"label":"etat","data_type":"/contact/state"},{"label":"expédition plus 4|adresse ligne 2|apte|suite","data_type":"/contact/postaladdressAdditional/shipping"},{"label":"facturation plus 4|ligne de facturation 2|apte|suite","data_type":"/contact/postaladdressAdditional/billing"},{"label":"affaires plus 4","data_type":"/contact/postaladdressAdditional/business"},{"label":"maison plus 4","data_type":"/contact/postaladdressAdditional/home"},{"label":"plus 4","data_type":"/contact/postaladdressAdditional"},{"label":"adresse de livraison","data_type":"/contact/postaladdress/shipping"},{"label":"adresse de facturation","data_type":"/contact/postaladdress/billing"},{"label":"adresse d'affaires","data_type":"/contact/postaladdress/business"},{"label":"adresse du domicile","data_type":"/contact/postaladdress/home"},{"label":"adresse","data_type":"/contact/postaladdress"},{"label":"message","data_type":"/message"},{"label":"assujettir","data_type":"/subject"},{"label":"prénom sur carte de crédit","data_type":"/financial/creditcard/issuedto/first"},{"label":"nom de famille sur la carte de crédit","data_type":"/financial/creditcard/issuedto/last"},{"label":"Émetteur de carte de crédit","data_type":"/financial/creditcard/issuedto"},{"label":"carte de crédit cvv","data_type":"/financial/creditcard/verification"},{"label":"numéro de carte de crédit partie 1","data_type":"/financial/creditcard/number/part1"},{"label":"numéro de carte de crédit partie 2","data_type":"/financial/creditcard/number/part2"},{"label":"numéro de carte de crédit, partie 3","data_type":"/financial/creditcard/number/part3"},{"label":"numéro de carte de crédit partie 4","data_type":"/financial/creditcard/number/part4"},{"label":"numéro de carte de crédit","data_type":"/financial/creditcard/number"},{"label":"type de carte de crédit","data_type":"/financial/creditcard/type"},{"label":"mois d'expiration de carte de crédit|carte expire (mois)","data_type":"/financial/creditcard/expirymonth"},{"label":"année d'expiration","data_type":"/financial/creditcard/expiryyear"},{"label":"mois et année","data_type":"/financial/creditcard/expiry2"},{"label":"expiration","data_type":"/financial/creditcard/expiry"},{"label":"année de naissance","data_type":"/birthdate/birthyear"},{"label":"mois de naissance|le mois de naissance","data_type":"/birthdate/birthmonth"},{"label":"anniversaire|date de naissance","data_type":"/birthdate/birthday"},{"label":"date de naissance|anniversaire","data_type":"/birthdate"},{"label":"épingle","data_type":"/pin"},{"label":"le genre","data_type":"/person/gender"},{"label":"se mettre d'accord","data_type":"/agreetos"},{"label":"souviens-toi de moi","data_type":"/rememberme"}],"password":[{"label":"mot de passe","data_type":"/password"},{"label":"mot de passe ancien","data_type":"/passwordold"},{"label":"carte de crédit cvv","data_type":"/financial/creditcard/verification"},{"label":"épingle","data_type":"/pin"}],"select":[{"label":"code du pays de travail","data_type":"/contact/phone/countrycode/work"},{"label":"code du pays de résidence","data_type":"/contact/phone/countrycode/home"},{"label":"code postal","data_type":"/contact/phone/countrycode"},{"label":"pays de livraison","data_type":"/contact/country/shipping"},{"label":"pays de facturation","data_type":"/contact/country/billing"},{"label":"pays d'affaires","data_type":"/contact/country/business"},{"label":"pays d'origine","data_type":"/contact/country/home"},{"label":"pays","data_type":"/contact/country"},{"label":"État de livraison","data_type":"/contact/state/shipping"},{"label":"État de facturation","data_type":"/contact/state/billing"},{"label":"État des affaires","data_type":"/contact/state/business"},{"label":"État de résidence","data_type":"/contact/state/home"},{"label":"etat","data_type":"/contact/state"},{"label":"type de carte de crédit","data_type":"/financial/creditcard/type"},{"label":"mois d'expiration de carte de crédit|carte expire (mois)","data_type":"/financial/creditcard/expirymonth"},{"label":"année d'expiration","data_type":"/financial/creditcard/expiryyear"},{"label":"mois et année","data_type":"/financial/creditcard/expiry2"},{"label":"expiration","data_type":"/financial/creditcard/expiry"},{"label":"année de naissance","data_type":"/birthdate/birthyear"},{"label":"mois de naissance|le mois de naissance","data_type":"/birthdate/birthmonth"},{"label":"anniversaire|date de naissance","data_type":"/birthdate/birthday"},{"label":"date de naissance|anniversaire","data_type":"/birthdate"}]},"it-it":{"text":[{"label":"captcha","data_type":"/captcha"},{"label":"ricerca","data_type":"/search"},{"label":"suggerimento","data_type":"/auth/hint"},{"label":"risposta","data_type":"/auth/answer"},{"label":"domanda|sfida","data_type":"/auth/question"},{"label":"e-mail","data_type":"/contact/email"},{"label":"società|nome della ditta","data_type":"/company/name"},{"label":"amichevole|nome utente|accesso","data_type":"/nameperson/friendly"},{"label":"prefisso","data_type":"/nameperson/prefix"},{"label":"ultimo|cognome","negative":"primo","data_type":"/nameperson/last"},{"label":"primo","negative":"ultimo","data_type":"/nameperson/first"},{"label":"in mezzo|secondo nome","negative":"ultimo|primo","data_type":"/nameperson/middle"},{"label":"nome e cognome","data_type":"/nameperson/full"},{"label":"suffisso","data_type":"/nameperson/suffix"},{"label":"parola d'ordine","data_type":"/password"},{"label":"vecchia password","data_type":"/passwordold"},{"label":"twitter","data_type":"/contact/im/twitter"},{"label":"linkedin|linked in|linked-in","data_type":"/contact/web/linkedin"},{"label":"blog","data_type":"/contact/web/blog"},{"label":"aim|aol instant messenger","data_type":"/contact/IM/aim"},{"label":"icq","data_type":"/contact/IM/icq"},{"label":"msn","data_type":"/contact/IM/msn"},{"label":"blaterare","data_type":"/contact/IM/jabber"},{"label":"skype","data_type":"/contact/IM/skype"},{"label":"yahoo","data_type":"/contact/IM/yahoo"},{"label":"telefono del lavoro|telefono aziendale","data_type":"/contact/phone/extension/work"},{"label":"estensione telefono di casa","data_type":"/contact/phone/extension/home"},{"label":"estensione","data_type":"/contact/phone/extension"},{"label":"prefisso teleselettivo","data_type":"/contact/phone/areacode/work"},{"label":"prefisso teleselettivo","data_type":"/contact/phone/areacode"},{"label":"telefono","data_type":"/contact/phone/local/work"},{"label":"telefono","data_type":"/contact/phone/local/home"},{"label":"telefono","data_type":"/contact/phone/local"},{"label":"lavoro codice del paese","data_type":"/contact/phone/countrycode/work"},{"label":"codice paese di origine","data_type":"/contact/phone/countrycode/home"},{"label":"prefisso internazionale","data_type":"/contact/phone/countrycode"},{"label":"cellulare|cella","data_type":"/contact/phone/mobile"},{"label":"fax","data_type":"/contact/phone/fax"},{"label":"telefono del lavoro|telefono aziendale|linea di business","data_type":"/contact/phone/work"},{"label":"telefono di casa","data_type":"/contact/phone/home"},{"label":"telefono","data_type":"/contact/phone"},{"label":"codice di avviamento postale di spedizione|spedizione codice postale","data_type":"/contact/postalcode/shipping"},{"label":"fatturazione codice postale","data_type":"/contact/postalcode/billing"},{"label":"codice di avviamento postale affari|cap lavoro|codice postale","data_type":"/contact/postalcode/business"},{"label":"codice di avviamento postale casa|casa codice postale","data_type":"/contact/postalcode/home"},{"label":"cap|codice postale","data_type":"/contact/postalcode"},{"label":"il trasporto della città","data_type":"/contact/city/shipping"},{"label":"fatturazione città","data_type":"/contact/city/billing"},{"label":"città d'affari","data_type":"/contact/city/business"},{"label":"città natale","data_type":"/contact/city/home"},{"label":"città","data_type":"/contact/city"},{"label":"paese di spedizione","data_type":"/contact/country/shipping"},{"label":"paese di fatturazione","data_type":"/contact/country/billing"},{"label":"paese di business","data_type":"/contact/country/business"},{"label":"paese natale","data_type":"/contact/country/home"},{"label":"nazione","data_type":"/contact/country"},{"label":"stato di spedizione","data_type":"/contact/state/shipping"},{"label":"stato di fatturazione","data_type":"/contact/state/billing"},{"label":"stato di business","data_type":"/contact/state/business"},{"label":"stato di residenza","data_type":"/contact/state/home"},{"label":"stato","data_type":"/contact/state"},{"label":"il trasporto più 4|indirizzo 2|adatto|suite","data_type":"/contact/postaladdressAdditional/shipping"},{"label":"fatturazione più 4|la linea di fatturazione 2|adatto|suite","data_type":"/contact/postaladdressAdditional/billing"},{"label":"business plus 4","data_type":"/contact/postaladdressAdditional/business"},{"label":"casa più 4","data_type":"/contact/postaladdressAdditional/home"},{"label":"più 4","data_type":"/contact/postaladdressAdditional"},{"label":"indirizzo di spedizione","data_type":"/contact/postaladdress/shipping"},{"label":"indirizzo di fatturazione","data_type":"/contact/postaladdress/billing"},{"label":"recapito di lavoro","data_type":"/contact/postaladdress/business"},{"label":"indirizzo di casa","data_type":"/contact/postaladdress/home"},{"label":"indirizzo","data_type":"/contact/postaladdress"},{"label":"messaggio","data_type":"/message"},{"label":"soggetto","data_type":"/subject"},{"label":"primo nome sulla carta di credito","data_type":"/financial/creditcard/issuedto/first"},{"label":"cognome sulla carta di credito","data_type":"/financial/creditcard/issuedto/last"},{"label":"emittente della carta di credito","data_type":"/financial/creditcard/issuedto"},{"label":"cvv carta di credito","data_type":"/financial/creditcard/verification"},{"label":"numero di carta di credito parte 1","data_type":"/financial/creditcard/number/part1"},{"label":"numero di carta di credito parte 2","data_type":"/financial/creditcard/number/part2"},{"label":"numero di carta di credito parte 3","data_type":"/financial/creditcard/number/part3"},{"label":"numero di carta di credito parte 4","data_type":"/financial/creditcard/number/part4"},{"label":"numero di carta di credito","data_type":"/financial/creditcard/number"},{"label":"tipo di carta di credito","data_type":"/financial/creditcard/type"},{"label":"mese di credito di scadenza della carta|scadenza della carta (mese)","data_type":"/financial/creditcard/expirymonth"},{"label":"anno di scadenza","data_type":"/financial/creditcard/expiryyear"},{"label":"mese e anno","data_type":"/financial/creditcard/expiry2"},{"label":"scadenza","data_type":"/financial/creditcard/expiry"},{"label":"anno di nascita","data_type":"/birthdate/birthyear"},{"label":"mese di nascita","data_type":"/birthdate/birthmonth"},{"label":"compleanno|giorno di nascita","data_type":"/birthdate/birthday"},{"label":"data di nascita|compleanno","data_type":"/birthdate"},{"label":"perno","data_type":"/pin"},{"label":"genere","data_type":"/person/gender"},{"label":"concordare","data_type":"/agreetos"},{"label":"ricordati di me","data_type":"/rememberme"}],"password":[{"label":"parola d'ordine","data_type":"/password"},{"label":"vecchia password","data_type":"/passwordold"},{"label":"cvv carta di credito","data_type":"/financial/creditcard/verification"},{"label":"perno","data_type":"/pin"}],"select":[{"label":"lavoro codice del paese","data_type":"/contact/phone/countrycode/work"},{"label":"codice paese di origine","data_type":"/contact/phone/countrycode/home"},{"label":"prefisso internazionale","data_type":"/contact/phone/countrycode"},{"label":"paese di spedizione","data_type":"/contact/country/shipping"},{"label":"paese di fatturazione","data_type":"/contact/country/billing"},{"label":"paese di business","data_type":"/contact/country/business"},{"label":"paese natale","data_type":"/contact/country/home"},{"label":"nazione","data_type":"/contact/country"},{"label":"stato di spedizione","data_type":"/contact/state/shipping"},{"label":"stato di fatturazione","data_type":"/contact/state/billing"},{"label":"stato di business","data_type":"/contact/state/business"},{"label":"stato di residenza","data_type":"/contact/state/home"},{"label":"stato","data_type":"/contact/state"},{"label":"tipo di carta di credito","data_type":"/financial/creditcard/type"},{"label":"mese di credito di scadenza della carta|scadenza della carta (mese)","data_type":"/financial/creditcard/expirymonth"},{"label":"anno di scadenza","data_type":"/financial/creditcard/expiryyear"},{"label":"mese e anno","data_type":"/financial/creditcard/expiry2"},{"label":"scadenza","data_type":"/financial/creditcard/expiry"},{"label":"anno di nascita","data_type":"/birthdate/birthyear"},{"label":"mese di nascita","data_type":"/birthdate/birthmonth"},{"label":"compleanno|giorno di nascita","data_type":"/birthdate/birthday"},{"label":"data di nascita|compleanno","data_type":"/birthdate"}]},"nl-nl":{"text":[{"label":"captcha","data_type":"/captcha"},{"label":"sök","data_type":"/search"},{"label":"antydan","data_type":"/auth/hint"},{"label":"svar","data_type":"/auth/answer"},{"label":"fråga|utmaning","data_type":"/auth/question"},{"label":"e-post","data_type":"/contact/email"},{"label":"företag|företagsnamn","data_type":"/company/name"},{"label":"vänlig|användarnamn|logga in|användare","data_type":"/nameperson/friendly"},{"label":"prefix","data_type":"/nameperson/prefix"},{"label":"sista|efternamn","negative":"först","data_type":"/nameperson/last"},{"label":"först","negative":"sista","data_type":"/nameperson/first"},{"label":"mitten|mellannamn","negative":"sista|först","data_type":"/nameperson/middle"},{"label":"fullständiga namn","data_type":"/nameperson/full"},{"label":"ändelse","data_type":"/nameperson/suffix"},{"label":"lösenord","data_type":"/password"},{"label":"lösenord gamla","data_type":"/passwordold"},{"label":"twitter","data_type":"/contact/im/twitter"},{"label":"edin|länkade i|linked-in","data_type":"/contact/web/linkedin"},{"label":"blogg","data_type":"/contact/web/blog"},{"label":"syfte|aol instant messenger","data_type":"/contact/IM/aim"},{"label":"icq","data_type":"/contact/IM/icq"},{"label":"msn","data_type":"/contact/IM/msn"},{"label":"pladder","data_type":"/contact/IM/jabber"},{"label":"skype","data_type":"/contact/IM/skype"},{"label":"yahoo","data_type":"/contact/IM/yahoo"},{"label":"jobbtelefon","data_type":"/contact/phone/extension/work"},{"label":"hemtelefon förlängning","data_type":"/contact/phone/extension/home"},{"label":"förlängning","data_type":"/contact/phone/extension"},{"label":"riktnummer","data_type":"/contact/phone/areacode/work"},{"label":"riktnummer","data_type":"/contact/phone/areacode"},{"label":"telefon","data_type":"/contact/phone/local/work"},{"label":"telefon","data_type":"/contact/phone/local/home"},{"label":"telefon","data_type":"/contact/phone/local"},{"label":"arbete landskod","data_type":"/contact/phone/countrycode/work"},{"label":"hem landskod","data_type":"/contact/phone/countrycode/home"},{"label":"landskod","data_type":"/contact/phone/countrycode"},{"label":"mobiltelefon|cell","data_type":"/contact/phone/mobile"},{"label":"fax","data_type":"/contact/phone/fax"},{"label":"jobbtelefon|företags telefon|affärsområde","data_type":"/contact/phone/work"},{"label":"hemtelefon","data_type":"/contact/phone/home"},{"label":"telefon","data_type":"/contact/phone"},{"label":"frakt postnummer|leverans postnummer","data_type":"/contact/postalcode/shipping"},{"label":"fakturering postnummer","data_type":"/contact/postalcode/billing"},{"label":"affärs postnummer|arbete postnummer","data_type":"/contact/postalcode/business"},{"label":"hem postnummer","data_type":"/contact/postalcode/home"},{"label":"postnummer","data_type":"/contact/postalcode"},{"label":"sändnings ort","data_type":"/contact/city/shipping"},{"label":"fakturering stad","data_type":"/contact/city/billing"},{"label":"affärsstad","data_type":"/contact/city/business"},{"label":"hemstad","data_type":"/contact/city/home"},{"label":"stad","data_type":"/contact/city"},{"label":"leveransland","data_type":"/contact/country/shipping"},{"label":"faktureringsland","data_type":"/contact/country/billing"},{"label":"affärs land","data_type":"/contact/country/business"},{"label":"hemland","data_type":"/contact/country/home"},{"label":"land","data_type":"/contact/country"},{"label":"shipping state","data_type":"/contact/state/shipping"},{"label":"faktureringstillstånd","data_type":"/contact/state/billing"},{"label":"affärs tillstånd","data_type":"/contact/state/business"},{"label":"hemstat","data_type":"/contact/state/home"},{"label":"ange","data_type":"/contact/state"},{"label":"frakt plus 4|adress linje 2|benägen|svit","data_type":"/contact/postaladdressAdditional/shipping"},{"label":"fakturering plus 4|fakturering linje 2|benägen|svit","data_type":"/contact/postaladdressAdditional/billing"},{"label":"företag plus 4","data_type":"/contact/postaladdressAdditional/business"},{"label":"hem plus 4","data_type":"/contact/postaladdressAdditional/home"},{"label":"plus 4","data_type":"/contact/postaladdressAdditional"},{"label":"leveransadress","data_type":"/contact/postaladdress/shipping"},{"label":"fakturaadress","data_type":"/contact/postaladdress/billing"},{"label":"företagsadress","data_type":"/contact/postaladdress/business"},{"label":"hemadress","data_type":"/contact/postaladdress/home"},{"label":"adress","data_type":"/contact/postaladdress"},{"label":"meddelande","data_type":"/message"},{"label":"ämne","data_type":"/subject"},{"label":"förnamn på kreditkort","data_type":"/financial/creditcard/issuedto/first"},{"label":"efternamn på kreditkort","data_type":"/financial/creditcard/issuedto/last"},{"label":"kreditkortsutgivare","data_type":"/financial/creditcard/issuedto"},{"label":"kreditkort cvv","data_type":"/financial/creditcard/verification"},{"label":"kreditkortsnummer del 1","data_type":"/financial/creditcard/number/part1"},{"label":"kreditkortsnummer del 2","data_type":"/financial/creditcard/number/part2"},{"label":"kreditkortsnummer del 3","data_type":"/financial/creditcard/number/part3"},{"label":"kreditkortsnummer del 4","data_type":"/financial/creditcard/number/part4"},{"label":"kreditkortsnummer","data_type":"/financial/creditcard/number"},{"label":"typ av kreditkort","data_type":"/financial/creditcard/type"},{"label":"kreditkortsutgångs månad|kort går ut (månad)","data_type":"/financial/creditcard/expirymonth"},{"label":"utgångsåret","data_type":"/financial/creditcard/expiryyear"},{"label":"månad och år","data_type":"/financial/creditcard/expiry2"},{"label":"utgång","data_type":"/financial/creditcard/expiry"},{"label":"födelseår","data_type":"/birthdate/birthyear"},{"label":"födelsemånad","data_type":"/birthdate/birthmonth"},{"label":"födelsedag","data_type":"/birthdate/birthday"},{"label":"födelsedatum|födelsedag","data_type":"/birthdate"},{"label":"stift","data_type":"/pin"},{"label":"kön","data_type":"/person/gender"},{"label":"hålla med","data_type":"/agreetos"},{"label":"kom ihåg mig","data_type":"/rememberme"}],"password":[{"label":"lösenord","data_type":"/password"},{"label":"lösenord gamla","data_type":"/passwordold"},{"label":"kreditkort cvv","data_type":"/financial/creditcard/verification"},{"label":"stift","data_type":"/pin"}],"select":[{"label":"arbete landskod","data_type":"/contact/phone/countrycode/work"},{"label":"hem landskod","data_type":"/contact/phone/countrycode/home"},{"label":"landskod","data_type":"/contact/phone/countrycode"},{"label":"leveransland","data_type":"/contact/country/shipping"},{"label":"faktureringsland","data_type":"/contact/country/billing"},{"label":"affärs land","data_type":"/contact/country/business"},{"label":"hemland","data_type":"/contact/country/home"},{"label":"land","data_type":"/contact/country"},{"label":"shipping state","data_type":"/contact/state/shipping"},{"label":"faktureringstillstånd","data_type":"/contact/state/billing"},{"label":"affärs tillstånd","data_type":"/contact/state/business"},{"label":"hemstat","data_type":"/contact/state/home"},{"label":"ange","data_type":"/contact/state"},{"label":"typ av kreditkort","data_type":"/financial/creditcard/type"},{"label":"kreditkortsutgångs månad|kort går ut (månad)","data_type":"/financial/creditcard/expirymonth"},{"label":"utgångsåret","data_type":"/financial/creditcard/expiryyear"},{"label":"månad och år","data_type":"/financial/creditcard/expiry2"},{"label":"utgång","data_type":"/financial/creditcard/expiry"},{"label":"födelseår","data_type":"/birthdate/birthyear"},{"label":"födelsemånad","data_type":"/birthdate/birthmonth"},{"label":"födelsedag","data_type":"/birthdate/birthday"},{"label":"födelsedatum|födelsedag","data_type":"/birthdate"}]},"pl-pl":{"text":[{"label":"captcha","data_type":"/captcha"},{"label":"szukanie","data_type":"/search"},{"label":"wskazówka","data_type":"/auth/hint"},{"label":"odpowiedź","data_type":"/auth/answer"},{"label":"pytanie|wyzwanie","data_type":"/auth/question"},{"label":"e-mail","data_type":"/contact/email"},{"label":"firma|nazwa firmy","data_type":"/company/name"},{"label":"przyjazny|nazwa użytkownika|zaloguj się","data_type":"/nameperson/friendly"},{"label":"prefiks","data_type":"/nameperson/prefix"},{"label":"trwać|nazwisko","negative":"pierwszy","data_type":"/nameperson/last"},{"label":"pierwszy","negative":"trwać","data_type":"/nameperson/first"},{"label":"środkowy|drugie imię","negative":"trwać|pierwszy","data_type":"/nameperson/middle"},{"label":"pełne imię i nazwisko","data_type":"/nameperson/full"},{"label":"przyrostek","data_type":"/nameperson/suffix"},{"label":"hasło","data_type":"/password"},{"label":"stare hasło","data_type":"/passwordold"},{"label":"świergot","data_type":"/contact/im/twitter"},{"label":"linkedin|połączone w","data_type":"/contact/web/linkedin"},{"label":"blog","data_type":"/contact/web/blog"},{"label":"cel|aol instant messenger","data_type":"/contact/IM/aim"},{"label":"icq","data_type":"/contact/IM/icq"},{"label":"msn","data_type":"/contact/IM/msn"},{"label":"paplanie","data_type":"/contact/IM/jabber"},{"label":"skype","data_type":"/contact/IM/skype"},{"label":"wieśniak","data_type":"/contact/IM/yahoo"},{"label":"telefon służbowy","data_type":"/contact/phone/extension/work"},{"label":"rozszerzenie telefon domowy","data_type":"/contact/phone/extension/home"},{"label":"rozbudowa","data_type":"/contact/phone/extension"},{"label":"numer kierunkowy","data_type":"/contact/phone/areacode/work"},{"label":"numer kierunkowy","data_type":"/contact/phone/areacode"},{"label":"telefon","data_type":"/contact/phone/local/work"},{"label":"telefon","data_type":"/contact/phone/local/home"},{"label":"telefon","data_type":"/contact/phone/local"},{"label":"prace kod kraju","data_type":"/contact/phone/countrycode/work"},{"label":"kod kraju pochodzenia","data_type":"/contact/phone/countrycode/home"},{"label":"kod pocztowy","data_type":"/contact/phone/countrycode"},{"label":"telefon komórkowy|komórka","data_type":"/contact/phone/mobile"},{"label":"faks","data_type":"/contact/phone/fax"},{"label":"telefon służbowy|corporate telefonu|linia biznesowa","data_type":"/contact/phone/work"},{"label":"telefon domowy","data_type":"/contact/phone/home"},{"label":"telefon","data_type":"/contact/phone"},{"label":"wysyłka kod pocztowy|wysyłać kod pocztowy","data_type":"/contact/postalcode/shipping"},{"label":"rozliczeniowy kod pocztowy","data_type":"/contact/postalcode/billing"},{"label":"kod biznesu zip|prace kod pocztowy","data_type":"/contact/postalcode/business"},{"label":"kod domu zip|domu kod pocztowy","data_type":"/contact/postalcode/home"},{"label":"kod pocztowy","data_type":"/contact/postalcode"},{"label":"wysyłka miasta","data_type":"/contact/city/shipping"},{"label":"rozliczeniowy miasta","data_type":"/contact/city/billing"},{"label":"miasto biznesu","data_type":"/contact/city/business"},{"label":"miasto rodzinne","data_type":"/contact/city/home"},{"label":"miasto","data_type":"/contact/city"},{"label":"wysyłka kraju","data_type":"/contact/country/shipping"},{"label":"kraj rozliczenia","data_type":"/contact/country/billing"},{"label":"branza kraj","data_type":"/contact/country/business"},{"label":"ojczyzna","data_type":"/contact/country/home"},{"label":"kraj","data_type":"/contact/country"},{"label":"stan shipping","data_type":"/contact/state/shipping"},{"label":"stan rozliczeń","data_type":"/contact/state/billing"},{"label":"stan biznesu","data_type":"/contact/state/business"},{"label":"stan domu","data_type":"/contact/state/home"},{"label":"stan","data_type":"/contact/state"},{"label":"wysyłka plus 4|wiersz adresu 2|trafny|zestaw","data_type":"/contact/postaladdressAdditional/shipping"},{"label":"rozliczeniowych oraz 4|linia rozliczeniowy 2|trafny|zestaw","data_type":"/contact/postaladdressAdditional/billing"},{"label":"business plus 4","data_type":"/contact/postaladdressAdditional/business"},{"label":"home plus 4","data_type":"/contact/postaladdressAdditional/home"},{"label":"plus 4","data_type":"/contact/postaladdressAdditional"},{"label":"adres wysyłki","data_type":"/contact/postaladdress/shipping"},{"label":"adres rozliczeniowy","data_type":"/contact/postaladdress/billing"},{"label":"adres biznesowy","data_type":"/contact/postaladdress/business"},{"label":"adres domowy","data_type":"/contact/postaladdress/home"},{"label":"adres","data_type":"/contact/postaladdress"},{"label":"wiadomość","data_type":"/message"},{"label":"przedmiot","data_type":"/subject"},{"label":"nazwisko na karcie kredytowej","data_type":"/financial/creditcard/issuedto/first"},{"label":"nazwisko na karcie kredytowej","data_type":"/financial/creditcard/issuedto/last"},{"label":"wystawca karty kredytowej","data_type":"/financial/creditcard/issuedto"},{"label":"karty kredytowej cvv","data_type":"/financial/creditcard/verification"},{"label":"numer karty kredytowej część 1","data_type":"/financial/creditcard/number/part1"},{"label":"numer karty kredytowej część 2","data_type":"/financial/creditcard/number/part2"},{"label":"numer karty kredytowej część 3","data_type":"/financial/creditcard/number/part3"},{"label":"numer karty kredytowej część 4","data_type":"/financial/creditcard/number/part4"},{"label":"numer karty kredytowej","data_type":"/financial/creditcard/number"},{"label":"typ karty kredytowej","data_type":"/financial/creditcard/type"},{"label":"ważności karty kredytowej miesiąc|wygasa karty (miesiąc)","data_type":"/financial/creditcard/expirymonth"},{"label":"rok ważności","data_type":"/financial/creditcard/expiryyear"},{"label":"miesiąc i rok","data_type":"/financial/creditcard/expiry2"},{"label":"wygaśnięcie","data_type":"/financial/creditcard/expiry"},{"label":"rok urodzenia","data_type":"/birthdate/birthyear"},{"label":"miesiąc urodzenia","data_type":"/birthdate/birthmonth"},{"label":"urodziny|dzień narodzin","data_type":"/birthdate/birthday"},{"label":"data urodzenia|urodziny","data_type":"/birthdate"},{"label":"kołek","data_type":"/pin"},{"label":"płeć","data_type":"/person/gender"},{"label":"zgodzić się","data_type":"/agreetos"},{"label":"zapamiętaj mnie","data_type":"/rememberme"}],"password":[{"label":"hasło","data_type":"/password"},{"label":"stare hasło","data_type":"/passwordold"},{"label":"karty kredytowej cvv","data_type":"/financial/creditcard/verification"},{"label":"kołek","data_type":"/pin"}],"select":[{"label":"prace kod kraju","data_type":"/contact/phone/countrycode/work"},{"label":"kod kraju pochodzenia","data_type":"/contact/phone/countrycode/home"},{"label":"kod pocztowy","data_type":"/contact/phone/countrycode"},{"label":"wysyłka kraju","data_type":"/contact/country/shipping"},{"label":"kraj rozliczenia","data_type":"/contact/country/billing"},{"label":"branza kraj","data_type":"/contact/country/business"},{"label":"ojczyzna","data_type":"/contact/country/home"},{"label":"kraj","data_type":"/contact/country"},{"label":"stan shipping","data_type":"/contact/state/shipping"},{"label":"stan rozliczeń","data_type":"/contact/state/billing"},{"label":"stan biznesu","data_type":"/contact/state/business"},{"label":"stan domu","data_type":"/contact/state/home"},{"label":"stan","data_type":"/contact/state"},{"label":"typ karty kredytowej","data_type":"/financial/creditcard/type"},{"label":"ważności karty kredytowej miesiąc|wygasa karty (miesiąc)","data_type":"/financial/creditcard/expirymonth"},{"label":"rok ważności","data_type":"/financial/creditcard/expiryyear"},{"label":"miesiąc i rok","data_type":"/financial/creditcard/expiry2"},{"label":"wygaśnięcie","data_type":"/financial/creditcard/expiry"},{"label":"rok urodzenia","data_type":"/birthdate/birthyear"},{"label":"miesiąc urodzenia","data_type":"/birthdate/birthmonth"},{"label":"urodziny|dzień narodzin","data_type":"/birthdate/birthday"},{"label":"data urodzenia|urodziny","data_type":"/birthdate"}]},"pt-pt":{"text":[{"label":"captcha","data_type":"/captcha"},{"label":"pesquisa","data_type":"/search"},{"label":"dica","data_type":"/auth/hint"},{"label":"responda","data_type":"/auth/answer"},{"label":"questão|desafio","data_type":"/auth/question"},{"label":"o email","data_type":"/contact/email"},{"label":"companhia|nome da empresa","data_type":"/company/name"},{"label":"amigáveis|nome de usuário|entrar|do utilizador","data_type":"/nameperson/friendly"},{"label":"prefixo","data_type":"/nameperson/prefix"},{"label":"último|último nome","negative":"primeiro","data_type":"/nameperson/last"},{"label":"primeiro","negative":"último","data_type":"/nameperson/first"},{"label":"meio|nome do meio","negative":"último|primeiro","data_type":"/nameperson/middle"},{"label":"nome completo","data_type":"/nameperson/full"},{"label":"sufixo","data_type":"/nameperson/suffix"},{"label":"senha","data_type":"/password"},{"label":"senha antiga","data_type":"/passwordold"},{"label":"twitter","data_type":"/contact/im/twitter"},{"label":"ligado|ligado em","data_type":"/contact/web/linkedin"},{"label":"blog","data_type":"/contact/web/blog"},{"label":"alvo|aol mensageiro instantâneo","data_type":"/contact/IM/aim"},{"label":"icq","data_type":"/contact/IM/icq"},{"label":"msn","data_type":"/contact/IM/msn"},{"label":"jabber","data_type":"/contact/IM/jabber"},{"label":"skype","data_type":"/contact/IM/skype"},{"label":"yahoo","data_type":"/contact/IM/yahoo"},{"label":"telefone de trabalho|telefone comercial","data_type":"/contact/phone/extension/work"},{"label":"extensão de telefone residencial","data_type":"/contact/phone/extension/home"},{"label":"extensão","data_type":"/contact/phone/extension"},{"label":"código de área","data_type":"/contact/phone/areacode/work"},{"label":"código de área","data_type":"/contact/phone/areacode"},{"label":"telefone","data_type":"/contact/phone/local/work"},{"label":"telefone","data_type":"/contact/phone/local/home"},{"label":"telefone","data_type":"/contact/phone/local"},{"label":"código do país de trabalho","data_type":"/contact/phone/countrycode/work"},{"label":"código do país de origem","data_type":"/contact/phone/countrycode/home"},{"label":"código do país","data_type":"/contact/phone/countrycode"},{"label":"celular|célula","data_type":"/contact/phone/mobile"},{"label":"fax","data_type":"/contact/phone/fax"},{"label":"telefone de trabalho|telefone comercial|telefone corporativo|linha de negócio","data_type":"/contact/phone/work"},{"label":"telefone fixo","data_type":"/contact/phone/home"},{"label":"telefone","data_type":"/contact/phone"},{"label":"código postal de envio|envio de código postal","data_type":"/contact/postalcode/shipping"},{"label":"cep para cobrança","data_type":"/contact/postalcode/billing"},{"label":"código postal comercial|código postal do trabalho","data_type":"/contact/postalcode/business"},{"label":"código postal residencial","data_type":"/contact/postalcode/home"},{"label":"cep|código postal","data_type":"/contact/postalcode"},{"label":"cidade de remessa","data_type":"/contact/city/shipping"},{"label":"cidade de faturamento","data_type":"/contact/city/billing"},{"label":"cidade de negócios","data_type":"/contact/city/business"},{"label":"cidade natal","data_type":"/contact/city/home"},{"label":"cidade","data_type":"/contact/city"},{"label":"país de remessa","data_type":"/contact/country/shipping"},{"label":"país de faturamento","data_type":"/contact/country/billing"},{"label":"país de negócios","data_type":"/contact/country/business"},{"label":"país natal","data_type":"/contact/country/home"},{"label":"país","data_type":"/contact/country"},{"label":"estado de envio","data_type":"/contact/state/shipping"},{"label":"estado de faturamento","data_type":"/contact/state/billing"},{"label":"estado de negócios","data_type":"/contact/state/business"},{"label":"estado de origem","data_type":"/contact/state/home"},{"label":"estado","data_type":"/contact/state"},{"label":"envio mais 4|linha de endereço 2|apto|suíte","data_type":"/contact/postaladdressAdditional/shipping"},{"label":"faturamento mais 4|linha de faturamento 2|apto|suíte","data_type":"/contact/postaladdressAdditional/billing"},{"label":"negócio mais 4","data_type":"/contact/postaladdressAdditional/business"},{"label":"casa mais 4","data_type":"/contact/postaladdressAdditional/home"},{"label":"mais 4","data_type":"/contact/postaladdressAdditional"},{"label":"endereço de entrega","data_type":"/contact/postaladdress/shipping"},{"label":"endereço de cobrança","data_type":"/contact/postaladdress/billing"},{"label":"endereço profissional","data_type":"/contact/postaladdress/business"},{"label":"endereço residencial","data_type":"/contact/postaladdress/home"},{"label":"endereço","data_type":"/contact/postaladdress"},{"label":"mensagem","data_type":"/message"},{"label":"sujeito","data_type":"/subject"},{"label":"primeiro nome no cartão de crédito","data_type":"/financial/creditcard/issuedto/first"},{"label":"sobrenome no cartão de crédito","data_type":"/financial/creditcard/issuedto/last"},{"label":"emissor de cartão de crédito","data_type":"/financial/creditcard/issuedto"},{"label":"cartão de crédito cvv","data_type":"/financial/creditcard/verification"},{"label":"número do cartão de crédito parte 1","data_type":"/financial/creditcard/number/part1"},{"label":"número de cartão de crédito parte 2","data_type":"/financial/creditcard/number/part2"},{"label":"número de cartão de crédito parte 3","data_type":"/financial/creditcard/number/part3"},{"label":"número de cartão de crédito parte 4","data_type":"/financial/creditcard/number/part4"},{"label":"número do cartão de crédito","data_type":"/financial/creditcard/number"},{"label":"tipo de cartão de crédito","data_type":"/financial/creditcard/type"},{"label":"mês de expiração do cartão de crédito|cartão expira (mês)","data_type":"/financial/creditcard/expirymonth"},{"label":"ano de expiração","data_type":"/financial/creditcard/expiryyear"},{"label":"mês e ano","data_type":"/financial/creditcard/expiry2"},{"label":"expiração","data_type":"/financial/creditcard/expiry"},{"label":"ano de nascimento","data_type":"/birthdate/birthyear"},{"label":"mês de nascimento","data_type":"/birthdate/birthmonth"},{"label":"dia de nascimento","data_type":"/birthdate/birthday"},{"label":"data de nascimento|aniversário","data_type":"/birthdate"},{"label":"pino","data_type":"/pin"},{"label":"gênero","data_type":"/person/gender"},{"label":"concordar","data_type":"/agreetos"},{"label":"lembre de mim","data_type":"/rememberme"}],"password":[{"label":"senha","data_type":"/password"},{"label":"senha antiga","data_type":"/passwordold"},{"label":"cartão de crédito cvv","data_type":"/financial/creditcard/verification"},{"label":"pino","data_type":"/pin"}],"select":[{"label":"código do país de trabalho","data_type":"/contact/phone/countrycode/work"},{"label":"código do país de origem","data_type":"/contact/phone/countrycode/home"},{"label":"código do país","data_type":"/contact/phone/countrycode"},{"label":"país de remessa","data_type":"/contact/country/shipping"},{"label":"país de faturamento","data_type":"/contact/country/billing"},{"label":"país de negócios","data_type":"/contact/country/business"},{"label":"país natal","data_type":"/contact/country/home"},{"label":"país","data_type":"/contact/country"},{"label":"estado de envio","data_type":"/contact/state/shipping"},{"label":"estado de faturamento","data_type":"/contact/state/billing"},{"label":"estado de negócios","data_type":"/contact/state/business"},{"label":"estado de origem","data_type":"/contact/state/home"},{"label":"estado","data_type":"/contact/state"},{"label":"tipo de cartão de crédito","data_type":"/financial/creditcard/type"},{"label":"mês de expiração do cartão de crédito|cartão expira (mês)","data_type":"/financial/creditcard/expirymonth"},{"label":"ano de expiração","data_type":"/financial/creditcard/expiryyear"},{"label":"mês e ano","data_type":"/financial/creditcard/expiry2"},{"label":"expiração","data_type":"/financial/creditcard/expiry"},{"label":"ano de nascimento","data_type":"/birthdate/birthyear"},{"label":"mês de nascimento","data_type":"/birthdate/birthmonth"},{"label":"dia de nascimento","data_type":"/birthdate/birthday"},{"label":"data de nascimento|aniversário","data_type":"/birthdate"}]},"zh-cn":{"text":[{"label":"验证码","data_type":"/captcha"},{"label":"搜索","data_type":"/search"},{"label":"暗示","data_type":"/auth/hint"},{"label":"回答","data_type":"/auth/answer"},{"label":"题|挑战","data_type":"/auth/question"},{"label":"电子邮件","data_type":"/contact/email"},{"label":"公司|公司名","data_type":"/company/name"},{"label":"友善|用户名|登录","data_type":"/nameperson/friendly"},{"label":"字首","data_type":"/nameperson/prefix"},{"label":"持续|姓","negative":"第一","data_type":"/nameperson/last"},{"label":"第一","negative":"持续","data_type":"/nameperson/first"},{"label":"中间|中间名字","negative":"持续|第一","data_type":"/nameperson/middle"},{"label":"全名","data_type":"/nameperson/full"},{"label":"后缀","data_type":"/nameperson/suffix"},{"label":"密码","data_type":"/password"},{"label":"密码老","data_type":"/passwordold"},{"label":"推特","data_type":"/contact/im/twitter"},{"label":"连接|链接","data_type":"/contact/web/linkedin"},{"label":"博客","data_type":"/contact/web/blog"},{"label":"目标|aol即时通讯","data_type":"/contact/IM/aim"},{"label":"icq","data_type":"/contact/IM/icq"},{"label":"msn","data_type":"/contact/IM/msn"},{"label":"jabber","data_type":"/contact/IM/jabber"},{"label":"skype","data_type":"/contact/IM/skype"},{"label":"雅虎","data_type":"/contact/IM/yahoo"},{"label":"工作电话|商务电话","data_type":"/contact/phone/extension/work"},{"label":"家庭电话分机","data_type":"/contact/phone/extension/home"},{"label":"延期","data_type":"/contact/phone/extension"},{"label":"区号","data_type":"/contact/phone/areacode/work"},{"label":"区号","data_type":"/contact/phone/areacode"},{"label":"电话","data_type":"/contact/phone/local/work"},{"label":"电话","data_type":"/contact/phone/local/home"},{"label":"电话","data_type":"/contact/phone/local"},{"label":"工作国家代码","data_type":"/contact/phone/countrycode/work"},{"label":"家庭国家代码","data_type":"/contact/phone/countrycode/home"},{"label":"国家代码","data_type":"/contact/phone/countrycode"},{"label":"移动电话|细胞","data_type":"/contact/phone/mobile"},{"label":"传真","data_type":"/contact/phone/fax"},{"label":"工作电话|商务电话|公司电话|业务线","data_type":"/contact/phone/work"},{"label":"家庭电话","data_type":"/contact/phone/home"},{"label":"电话","data_type":"/contact/phone"},{"label":"邮政编码","data_type":"/contact/postalcode/shipping"},{"label":"计费邮编","data_type":"/contact/postalcode/billing"},{"label":"商业邮政编码|工作邮政编码","data_type":"/contact/postalcode/business"},{"label":"家邮政编码|家庭邮政编码","data_type":"/contact/postalcode/home"},{"label":"邮政编码","data_type":"/contact/postalcode"},{"label":"航运城市","data_type":"/contact/city/shipping"},{"label":"计费城市","data_type":"/contact/city/billing"},{"label":"商业城","data_type":"/contact/city/business"},{"label":"家乡城市","data_type":"/contact/city/home"},{"label":"市","data_type":"/contact/city"},{"label":"航运国家","data_type":"/contact/country/shipping"},{"label":"帐单国家/地区","data_type":"/contact/country/billing"},{"label":"商业国家","data_type":"/contact/country/business"},{"label":"母国","data_type":"/contact/country/home"},{"label":"国家","data_type":"/contact/country"},{"label":"运输状态","data_type":"/contact/state/shipping"},{"label":"计费状态","data_type":"/contact/state/billing"},{"label":"业务状态","data_type":"/contact/state/business"},{"label":"家庭状态","data_type":"/contact/state/home"},{"label":"州","data_type":"/contact/state"},{"label":"运费加4|地址线2|易于|套房","data_type":"/contact/postaladdressAdditional/shipping"},{"label":"结算加4|结算行2|易于|套房","data_type":"/contact/postaladdressAdditional/billing"},{"label":"业务加4","data_type":"/contact/postaladdressAdditional/business"},{"label":"家加4","data_type":"/contact/postaladdressAdditional/home"},{"label":"加4","data_type":"/contact/postaladdressAdditional"},{"label":"邮寄地址","data_type":"/contact/postaladdress/shipping"},{"label":"帐单地址","data_type":"/contact/postaladdress/billing"},{"label":"商业地址","data_type":"/contact/postaladdress/business"},{"label":"家庭地址","data_type":"/contact/postaladdress/home"},{"label":"地址","data_type":"/contact/postaladdress"},{"label":"信息","data_type":"/message"},{"label":"学科","data_type":"/subject"},{"label":"信用卡上的名字","data_type":"/financial/creditcard/issuedto/first"},{"label":"姓氏在信用卡","data_type":"/financial/creditcard/issuedto/last"},{"label":"信用卡发卡机构","data_type":"/financial/creditcard/issuedto"},{"label":"信用卡cvv","data_type":"/financial/creditcard/verification"},{"label":"信用卡号码第1部分","data_type":"/financial/creditcard/number/part1"},{"label":"信用卡号码第2部分","data_type":"/financial/creditcard/number/part2"},{"label":"信用卡号码第3部分","data_type":"/financial/creditcard/number/part3"},{"label":"信用卡号码第4部分","data_type":"/financial/creditcard/number/part4"},{"label":"信用卡号码","data_type":"/financial/creditcard/number"},{"label":"信用卡类型","data_type":"/financial/creditcard/type"},{"label":"信用卡到期月份|卡过期（月）","data_type":"/financial/creditcard/expirymonth"},{"label":"到期年","data_type":"/financial/creditcard/expiryyear"},{"label":"月和年","data_type":"/financial/creditcard/expiry2"},{"label":"到期","data_type":"/financial/creditcard/expiry"},{"label":"出生年|出生年份","data_type":"/birthdate/birthyear"},{"label":"出生月|出生月份","data_type":"/birthdate/birthmonth"},{"label":"出生日","data_type":"/birthdate/birthday"},{"label":"生日","data_type":"/birthdate"},{"label":"销","data_type":"/pin"},{"label":"性别","data_type":"/person/gender"},{"label":"同意","data_type":"/agreetos"},{"label":"记住我","data_type":"/rememberme"}],"password":[{"label":"密码","data_type":"/password"},{"label":"密码老","data_type":"/passwordold"},{"label":"信用卡cvv","data_type":"/financial/creditcard/verification"},{"label":"销","data_type":"/pin"}],"select":[{"label":"工作国家代码","data_type":"/contact/phone/countrycode/work"},{"label":"家庭国家代码","data_type":"/contact/phone/countrycode/home"},{"label":"国家代码","data_type":"/contact/phone/countrycode"},{"label":"航运国家","data_type":"/contact/country/shipping"},{"label":"帐单国家/地区","data_type":"/contact/country/billing"},{"label":"商业国家","data_type":"/contact/country/business"},{"label":"母国","data_type":"/contact/country/home"},{"label":"国家","data_type":"/contact/country"},{"label":"运输状态","data_type":"/contact/state/shipping"},{"label":"计费状态","data_type":"/contact/state/billing"},{"label":"业务状态","data_type":"/contact/state/business"},{"label":"家庭状态","data_type":"/contact/state/home"},{"label":"州","data_type":"/contact/state"},{"label":"信用卡类型","data_type":"/financial/creditcard/type"},{"label":"信用卡到期月份|卡过期（月）","data_type":"/financial/creditcard/expirymonth"},{"label":"到期年","data_type":"/financial/creditcard/expiryyear"},{"label":"月和年","data_type":"/financial/creditcard/expiry2"},{"label":"到期","data_type":"/financial/creditcard/expiry"},{"label":"出生年|出生年份","data_type":"/birthdate/birthyear"},{"label":"出生月|出生月份","data_type":"/birthdate/birthmonth"},{"label":"出生日","data_type":"/birthdate/birthday"},{"label":"生日","data_type":"/birthdate"}]},"zh-tw":{"text":[{"label":"驗證碼","data_type":"/captcha"},{"label":"搜索","data_type":"/search"},{"label":"暗示","data_type":"/auth/hint"},{"label":"回答","data_type":"/auth/answer"},{"label":"題|挑戰","data_type":"/auth/question"},{"label":"電子郵件","data_type":"/contact/email"},{"label":"公司|公司名","data_type":"/company/name"},{"label":"友善|用戶名|登錄","data_type":"/nameperson/friendly"},{"label":"字首","data_type":"/nameperson/prefix"},{"label":"持續|姓","negative":"第一","data_type":"/nameperson/last"},{"label":"第一","negative":"持續","data_type":"/nameperson/first"},{"label":"中間|中間名字","negative":"持續|第一","data_type":"/nameperson/middle"},{"label":"全名","data_type":"/nameperson/full"},{"label":"後綴","data_type":"/nameperson/suffix"},{"label":"密碼","data_type":"/password"},{"label":"密碼老","data_type":"/passwordold"},{"label":"推特","data_type":"/contact/im/twitter"},{"label":"連接|鏈接","data_type":"/contact/web/linkedin"},{"label":"博客","data_type":"/contact/web/blog"},{"label":"目標|aol即時通訊","data_type":"/contact/IM/aim"},{"label":"icq","data_type":"/contact/IM/icq"},{"label":"msn","data_type":"/contact/IM/msn"},{"label":"jabber","data_type":"/contact/IM/jabber"},{"label":"skype","data_type":"/contact/IM/skype"},{"label":"雅虎","data_type":"/contact/IM/yahoo"},{"label":"工作電話|商務電話","data_type":"/contact/phone/extension/work"},{"label":"家庭電話分機","data_type":"/contact/phone/extension/home"},{"label":"延期","data_type":"/contact/phone/extension"},{"label":"區號","data_type":"/contact/phone/areacode/work"},{"label":"區號","data_type":"/contact/phone/areacode"},{"label":"電話","data_type":"/contact/phone/local/work"},{"label":"電話","data_type":"/contact/phone/local/home"},{"label":"電話","data_type":"/contact/phone/local"},{"label":"工作國家代碼","data_type":"/contact/phone/countrycode/work"},{"label":"家庭國家代碼","data_type":"/contact/phone/countrycode/home"},{"label":"國家代碼","data_type":"/contact/phone/countrycode"},{"label":"移動電話|細胞","data_type":"/contact/phone/mobile"},{"label":"傳真","data_type":"/contact/phone/fax"},{"label":"工作電話|商務電話|公司電話|業務線","data_type":"/contact/phone/work"},{"label":"家庭電話","data_type":"/contact/phone/home"},{"label":"電話","data_type":"/contact/phone"},{"label":"郵政編碼","data_type":"/contact/postalcode/shipping"},{"label":"計費郵編","data_type":"/contact/postalcode/billing"},{"label":"商業郵政編碼|工作郵政編碼","data_type":"/contact/postalcode/business"},{"label":"家郵政編碼|家庭郵政編碼","data_type":"/contact/postalcode/home"},{"label":"郵政編碼","data_type":"/contact/postalcode"},{"label":"航運城市","data_type":"/contact/city/shipping"},{"label":"計費城市","data_type":"/contact/city/billing"},{"label":"商業城","data_type":"/contact/city/business"},{"label":"家鄉城市","data_type":"/contact/city/home"},{"label":"市","data_type":"/contact/city"},{"label":"航運國家","data_type":"/contact/country/shipping"},{"label":"帳單國家/地區","data_type":"/contact/country/billing"},{"label":"商業國家","data_type":"/contact/country/business"},{"label":"母國","data_type":"/contact/country/home"},{"label":"國家","data_type":"/contact/country"},{"label":"運輸狀態","data_type":"/contact/state/shipping"},{"label":"計費狀態","data_type":"/contact/state/billing"},{"label":"業務狀態","data_type":"/contact/state/business"},{"label":"家庭狀態","data_type":"/contact/state/home"},{"label":"州","data_type":"/contact/state"},{"label":"運費加4|地址線2|易於|套房","data_type":"/contact/postaladdressAdditional/shipping"},{"label":"結算加4|結算行2|易於|套房","data_type":"/contact/postaladdressAdditional/billing"},{"label":"業務加4","data_type":"/contact/postaladdressAdditional/business"},{"label":"家加4","data_type":"/contact/postaladdressAdditional/home"},{"label":"加4","data_type":"/contact/postaladdressAdditional"},{"label":"郵寄地址","data_type":"/contact/postaladdress/shipping"},{"label":"帳單地址","data_type":"/contact/postaladdress/billing"},{"label":"商業地址","data_type":"/contact/postaladdress/business"},{"label":"家庭地址","data_type":"/contact/postaladdress/home"},{"label":"地址","data_type":"/contact/postaladdress"},{"label":"信息","data_type":"/message"},{"label":"學科","data_type":"/subject"},{"label":"信用卡上的名字","data_type":"/financial/creditcard/issuedto/first"},{"label":"姓氏在信用卡","data_type":"/financial/creditcard/issuedto/last"},{"label":"信用卡發卡機構","data_type":"/financial/creditcard/issuedto"},{"label":"信用卡cvv","data_type":"/financial/creditcard/verification"},{"label":"信用卡號碼第1部分","data_type":"/financial/creditcard/number/part1"},{"label":"信用卡號碼第2部分","data_type":"/financial/creditcard/number/part2"},{"label":"信用卡號碼第3部分","data_type":"/financial/creditcard/number/part3"},{"label":"信用卡號碼第4部分","data_type":"/financial/creditcard/number/part4"},{"label":"信用卡號碼","data_type":"/financial/creditcard/number"},{"label":"信用卡類型","data_type":"/financial/creditcard/type"},{"label":"信用卡到期月份|卡過期（月）","data_type":"/financial/creditcard/expirymonth"},{"label":"到期年","data_type":"/financial/creditcard/expiryyear"},{"label":"月和年","data_type":"/financial/creditcard/expiry2"},{"label":"到期","data_type":"/financial/creditcard/expiry"},{"label":"出生年|出生年份","data_type":"/birthdate/birthyear"},{"label":"出生月|出生月份","data_type":"/birthdate/birthmonth"},{"label":"出生日","data_type":"/birthdate/birthday"},{"label":"生日","data_type":"/birthdate"},{"label":"銷","data_type":"/pin"},{"label":"性別","data_type":"/person/gender"},{"label":"同意","data_type":"/agreetos"},{"label":"記住我","data_type":"/rememberme"}],"password":[{"label":"密碼","data_type":"/password"},{"label":"密碼老","data_type":"/passwordold"},{"label":"信用卡cvv","data_type":"/financial/creditcard/verification"},{"label":"銷","data_type":"/pin"}],"select":[{"label":"工作國家代碼","data_type":"/contact/phone/countrycode/work"},{"label":"家庭國家代碼","data_type":"/contact/phone/countrycode/home"},{"label":"國家代碼","data_type":"/contact/phone/countrycode"},{"label":"航運國家","data_type":"/contact/country/shipping"},{"label":"帳單國家/地區","data_type":"/contact/country/billing"},{"label":"商業國家","data_type":"/contact/country/business"},{"label":"母國","data_type":"/contact/country/home"},{"label":"國家","data_type":"/contact/country"},{"label":"運輸狀態","data_type":"/contact/state/shipping"},{"label":"計費狀態","data_type":"/contact/state/billing"},{"label":"業務狀態","data_type":"/contact/state/business"},{"label":"家庭狀態","data_type":"/contact/state/home"},{"label":"州","data_type":"/contact/state"},{"label":"信用卡類型","data_type":"/financial/creditcard/type"},{"label":"信用卡到期月份|卡過期（月）","data_type":"/financial/creditcard/expirymonth"},{"label":"到期年","data_type":"/financial/creditcard/expiryyear"},{"label":"月和年","data_type":"/financial/creditcard/expiry2"},{"label":"到期","data_type":"/financial/creditcard/expiry"},{"label":"出生年|出生年份","data_type":"/birthdate/birthyear"},{"label":"出生月|出生月份","data_type":"/birthdate/birthmonth"},{"label":"出生日","data_type":"/birthdate/birthday"},{"label":"生日","data_type":"/birthdate"}]},"ja":{"text":[{"label":"キャプチャ","data_type":"/captcha"},{"label":"サーチ","data_type":"/search"},{"label":"ヒント","data_type":"/auth/hint"},{"label":"回答","data_type":"/auth/answer"},{"label":"質問|チャレンジ","data_type":"/auth/question"},{"label":"eメール","data_type":"/contact/email"},{"label":"会社|会社名","data_type":"/company/name"},{"label":"フレンドリーな|ユーザー名|ログイン","data_type":"/nameperson/friendly"},{"label":"プレフィックス","data_type":"/nameperson/prefix"},{"label":"最終|苗字","negative":"最初","data_type":"/nameperson/last"},{"label":"最初","negative":"最終","data_type":"/nameperson/first"},{"label":"中間|ミドルネーム","negative":"最終|最初","data_type":"/nameperson/middle"},{"label":"フルネーム","data_type":"/nameperson/full"},{"label":"サフィックス","data_type":"/nameperson/suffix"},{"label":"パスワード","data_type":"/password"},{"label":"古いパスワード","data_type":"/passwordold"},{"label":"ツイッター","data_type":"/contact/im/twitter"},{"label":"リンクス|リンク先|リンクされた","data_type":"/contact/web/linkedin"},{"label":"ブログ","data_type":"/contact/web/blog"},{"label":"目的|インスタントメッセンジャー","data_type":"/contact/IM/aim"},{"label":"icq","data_type":"/contact/IM/icq"},{"label":"msn","data_type":"/contact/IM/msn"},{"label":"ジャバー","data_type":"/contact/IM/jabber"},{"label":"スカイプ","data_type":"/contact/IM/skype"},{"label":"ヤフー","data_type":"/contact/IM/yahoo"},{"label":"職場の電話|ビジネス用電話機","data_type":"/contact/phone/extension/work"},{"label":"自宅の内線番号","data_type":"/contact/phone/extension/home"},{"label":"拡張","data_type":"/contact/phone/extension"},{"label":"市外局番","data_type":"/contact/phone/areacode/work"},{"label":"市外局番","data_type":"/contact/phone/areacode"},{"label":"電話","data_type":"/contact/phone/local/work"},{"label":"電話","data_type":"/contact/phone/local/home"},{"label":"電話","data_type":"/contact/phone/local"},{"label":"作業国コード","data_type":"/contact/phone/countrycode/work"},{"label":"自国コード","data_type":"/contact/phone/countrycode/home"},{"label":"国コード","data_type":"/contact/phone/countrycode"},{"label":"携帯電話|細胞","data_type":"/contact/phone/mobile"},{"label":"ファックス","data_type":"/contact/phone/fax"},{"label":"職場の電話|ビジネス用電話機|企業の電話|ビジネスライン","data_type":"/contact/phone/work"},{"label":"自宅の電話","data_type":"/contact/phone/home"},{"label":"電話","data_type":"/contact/phone"},{"label":"発送郵便番号","data_type":"/contact/postalcode/shipping"},{"label":"請求先郵便番号","data_type":"/contact/postalcode/billing"},{"label":"ビジネス郵便番号|仕事の郵便番号","data_type":"/contact/postalcode/business"},{"label":"自宅の郵便番号","data_type":"/contact/postalcode/home"},{"label":"郵便番号","data_type":"/contact/postalcode"},{"label":"出荷都市","data_type":"/contact/city/shipping"},{"label":"請求都市","data_type":"/contact/city/billing"},{"label":"ビジネス街","data_type":"/contact/city/business"},{"label":"ホームシティ","data_type":"/contact/city/home"},{"label":"シティ","data_type":"/contact/city"},{"label":"出荷国","data_type":"/contact/country/shipping"},{"label":"課金国","data_type":"/contact/country/billing"},{"label":"ビジネスカントリー","data_type":"/contact/country/business"},{"label":"本国","data_type":"/contact/country/home"},{"label":"国","data_type":"/contact/country"},{"label":"出荷状態","data_type":"/contact/state/shipping"},{"label":"請求状態","data_type":"/contact/state/billing"},{"label":"ビジネス状態","data_type":"/contact/state/business"},{"label":"家の状態","data_type":"/contact/state/home"},{"label":"状態","data_type":"/contact/state"},{"label":"出荷+4|住所2|適切|スイート","data_type":"/contact/postaladdressAdditional/shipping"},{"label":"請求とプラス4|請求行2|適切|スイート","data_type":"/contact/postaladdressAdditional/billing"},{"label":"ビジネスプラス4","data_type":"/contact/postaladdressAdditional/business"},{"label":"ホームプラス4","data_type":"/contact/postaladdressAdditional/home"},{"label":"プラス4","data_type":"/contact/postaladdressAdditional"},{"label":"お届け先の住所","data_type":"/contact/postaladdress/shipping"},{"label":"請求先住所","data_type":"/contact/postaladdress/billing"},{"label":"職場の住所","data_type":"/contact/postaladdress/business"},{"label":"自宅住所","data_type":"/contact/postaladdress/home"},{"label":"住所","data_type":"/contact/postaladdress"},{"label":"メッセージ","data_type":"/message"},{"label":"主題","data_type":"/subject"},{"label":"クレジットカードのファーストネーム","data_type":"/financial/creditcard/issuedto/first"},{"label":"クレジットカードの名字","data_type":"/financial/creditcard/issuedto/last"},{"label":"クレジットカード発行者","data_type":"/financial/creditcard/issuedto"},{"label":"クレジットカードcvv","data_type":"/financial/creditcard/verification"},{"label":"クレジットカード番号パート1","data_type":"/financial/creditcard/number/part1"},{"label":"クレジットカード番号パート2","data_type":"/financial/creditcard/number/part2"},{"label":"クレジットカード番号パート3","data_type":"/financial/creditcard/number/part3"},{"label":"クレジットカード番号パート4","data_type":"/financial/creditcard/number/part4"},{"label":"クレジットカード番号","data_type":"/financial/creditcard/number"},{"label":"クレジットカードの種類","data_type":"/financial/creditcard/type"},{"label":"クレジットカード有効期限|カードの有効期限（月）","data_type":"/financial/creditcard/expirymonth"},{"label":"有効期限","data_type":"/financial/creditcard/expiryyear"},{"label":"月と年","data_type":"/financial/creditcard/expiry2"},{"label":"失効","data_type":"/financial/creditcard/expiry"},{"label":"生年","data_type":"/birthdate/birthyear"},{"label":"誕生月|生年月日","data_type":"/birthdate/birthmonth"},{"label":"お誕生日","data_type":"/birthdate/birthday"},{"label":"誕生日|お誕生日","data_type":"/birthdate"},{"label":"ピン","data_type":"/pin"},{"label":"性別","data_type":"/person/gender"},{"label":"同意する","data_type":"/agreetos"},{"label":"私を覚えてますか","data_type":"/rememberme"}],"password":[{"label":"パスワード","data_type":"/password"},{"label":"古いパスワード","data_type":"/passwordold"},{"label":"クレジットカードcvv","data_type":"/financial/creditcard/verification"},{"label":"ピン","data_type":"/pin"}],"select":[{"label":"作業国コード","data_type":"/contact/phone/countrycode/work"},{"label":"自国コード","data_type":"/contact/phone/countrycode/home"},{"label":"国コード","data_type":"/contact/phone/countrycode"},{"label":"出荷国","data_type":"/contact/country/shipping"},{"label":"課金国","data_type":"/contact/country/billing"},{"label":"ビジネスカントリー","data_type":"/contact/country/business"},{"label":"本国","data_type":"/contact/country/home"},{"label":"国","data_type":"/contact/country"},{"label":"出荷状態","data_type":"/contact/state/shipping"},{"label":"請求状態","data_type":"/contact/state/billing"},{"label":"ビジネス状態","data_type":"/contact/state/business"},{"label":"家の状態","data_type":"/contact/state/home"},{"label":"状態","data_type":"/contact/state"},{"label":"クレジットカードの種類","data_type":"/financial/creditcard/type"},{"label":"クレジットカード有効期限|カードの有効期限（月）","data_type":"/financial/creditcard/expirymonth"},{"label":"有効期限","data_type":"/financial/creditcard/expiryyear"},{"label":"月と年","data_type":"/financial/creditcard/expiry2"},{"label":"失効","data_type":"/financial/creditcard/expiry"},{"label":"生年","data_type":"/birthdate/birthyear"},{"label":"誕生月|生年月日","data_type":"/birthdate/birthmonth"},{"label":"お誕生日","data_type":"/birthdate/birthday"},{"label":"誕生日|お誕生日","data_type":"/birthdate"}]},"ko":{"text":[{"label":"보안 문자","data_type":"/captcha"},{"label":"수색","data_type":"/search"},{"label":"힌트","data_type":"/auth/hint"},{"label":"대답","data_type":"/auth/answer"},{"label":"문제|도전","data_type":"/auth/question"},{"label":"이메일","data_type":"/contact/email"},{"label":"회사|회사 이름","data_type":"/company/name"},{"label":"친한|사용자 이름|로그인","data_type":"/nameperson/friendly"},{"label":"접두사","data_type":"/nameperson/prefix"},{"label":"마지막|성","negative":"먼저","data_type":"/nameperson/last"},{"label":"먼저","negative":"마지막","data_type":"/nameperson/first"},{"label":"중간|중간 이름","negative":"마지막|먼저","data_type":"/nameperson/middle"},{"label":"성명","data_type":"/nameperson/full"},{"label":"접미사","data_type":"/nameperson/suffix"},{"label":"암호","data_type":"/password"},{"label":"오래된 암호","data_type":"/passwordold"},{"label":"지저귀다","data_type":"/contact/im/twitter"},{"label":"링크드 인|연결된|연계 된","data_type":"/contact/web/linkedin"},{"label":"블로그","data_type":"/contact/web/blog"},{"label":"목표|에일 메신저","data_type":"/contact/IM/aim"},{"label":"icq","data_type":"/contact/IM/icq"},{"label":"msn","data_type":"/contact/IM/msn"},{"label":"재잘 거림","data_type":"/contact/IM/jabber"},{"label":"스카이프","data_type":"/contact/IM/skype"},{"label":"야후","data_type":"/contact/IM/yahoo"},{"label":"직장 전화|회사 전화 번호","data_type":"/contact/phone/extension/work"},{"label":"집 전화 연장","data_type":"/contact/phone/extension/home"},{"label":"신장","data_type":"/contact/phone/extension"},{"label":"지역 코드","data_type":"/contact/phone/areacode/work"},{"label":"지역 코드","data_type":"/contact/phone/areacode"},{"label":"전화","data_type":"/contact/phone/local/work"},{"label":"전화","data_type":"/contact/phone/local/home"},{"label":"전화","data_type":"/contact/phone/local"},{"label":"근무 국가 코드","data_type":"/contact/phone/countrycode/work"},{"label":"자국 국가 코드","data_type":"/contact/phone/countrycode/home"},{"label":"국가 코드","data_type":"/contact/phone/countrycode"},{"label":"휴대 전화|세포","data_type":"/contact/phone/mobile"},{"label":"팩스","data_type":"/contact/phone/fax"},{"label":"직장 전화|회사 전화 번호|기업 전화|비즈니스 라인","data_type":"/contact/phone/work"},{"label":"집 전화","data_type":"/contact/phone/home"},{"label":"전화","data_type":"/contact/phone"},{"label":"배송 우편 번호","data_type":"/contact/postalcode/shipping"},{"label":"청구 우편 번호","data_type":"/contact/postalcode/billing"},{"label":"비즈니스 우편 번호|직장 우편 번호","data_type":"/contact/postalcode/business"},{"label":"집 우편 번호","data_type":"/contact/postalcode/home"},{"label":"우편 번호","data_type":"/contact/postalcode"},{"label":"선적 도시","data_type":"/contact/city/shipping"},{"label":"과금 도시","data_type":"/contact/city/billing"},{"label":"사업 도시","data_type":"/contact/city/business"},{"label":"본국","data_type":"/contact/city/home"},{"label":"시티","data_type":"/contact/city"},{"label":"선적 국가","data_type":"/contact/country/shipping"},{"label":"청구서 수신 국가","data_type":"/contact/country/billing"},{"label":"사업 국가","data_type":"/contact/country/business"},{"label":"본국","data_type":"/contact/country/home"},{"label":"국가","data_type":"/contact/country"},{"label":"선적 상태","data_type":"/contact/state/shipping"},{"label":"청구서 수신 상태","data_type":"/contact/state/billing"},{"label":"사업 상태","data_type":"/contact/state/business"},{"label":"가정 상태","data_type":"/contact/state/home"},{"label":"상태","data_type":"/contact/state"},{"label":"배송 + 4|주소 2|적절한|모음곡","data_type":"/contact/postaladdressAdditional/shipping"},{"label":"결제 + 4|과금 라인 2|적절한|모음곡","data_type":"/contact/postaladdressAdditional/billing"},{"label":"비즈니스 플러스 4","data_type":"/contact/postaladdressAdditional/business"},{"label":"집에 4 더하기","data_type":"/contact/postaladdressAdditional/home"},{"label":"4 더하기","data_type":"/contact/postaladdressAdditional"},{"label":"배송 주소","data_type":"/contact/postaladdress/shipping"},{"label":"청구 지 주소","data_type":"/contact/postaladdress/billing"},{"label":"사업장 주소","data_type":"/contact/postaladdress/business"},{"label":"집 주소","data_type":"/contact/postaladdress/home"},{"label":"주소","data_type":"/contact/postaladdress"},{"label":"메시지","data_type":"/message"},{"label":"제목","data_type":"/subject"},{"label":"신용 카드의 이름","data_type":"/financial/creditcard/issuedto/first"},{"label":"신용 카드의 성","data_type":"/financial/creditcard/issuedto/last"},{"label":"신용 카드 발급 기관","data_type":"/financial/creditcard/issuedto"},{"label":"신용 카드 cvv","data_type":"/financial/creditcard/verification"},{"label":"신용 카드 번호 부 1","data_type":"/financial/creditcard/number/part1"},{"label":"신용 카드 번호 부 2","data_type":"/financial/creditcard/number/part2"},{"label":"신용 카드 번호 부 3","data_type":"/financial/creditcard/number/part3"},{"label":"신용 카드 번호 부 4","data_type":"/financial/creditcard/number/part4"},{"label":"신용 카드 번호","data_type":"/financial/creditcard/number"},{"label":"신용 카드 종류","data_type":"/financial/creditcard/type"},{"label":"신용 카드 유효 기간|카드 만료 (월)","data_type":"/financial/creditcard/expirymonth"},{"label":"만료 연도","data_type":"/financial/creditcard/expiryyear"},{"label":"월 및 연도","data_type":"/financial/creditcard/expiry2"},{"label":"만료","data_type":"/financial/creditcard/expiry"},{"label":"생년","data_type":"/birthdate/birthyear"},{"label":"탄생 월|생년월일","data_type":"/birthdate/birthmonth"},{"label":"탄생일|생년월일","data_type":"/birthdate/birthday"},{"label":"생일","data_type":"/birthdate"},{"label":"핀","data_type":"/pin"},{"label":"성별","data_type":"/person/gender"},{"label":"동의하다","data_type":"/agreetos"},{"label":"날 기억해","data_type":"/rememberme"}],"password":[{"label":"암호","data_type":"/password"},{"label":"오래된 암호","data_type":"/passwordold"},{"label":"신용 카드 cvv","data_type":"/financial/creditcard/verification"},{"label":"핀","data_type":"/pin"}],"select":[{"label":"근무 국가 코드","data_type":"/contact/phone/countrycode/work"},{"label":"자국 국가 코드","data_type":"/contact/phone/countrycode/home"},{"label":"국가 코드","data_type":"/contact/phone/countrycode"},{"label":"선적 국가","data_type":"/contact/country/shipping"},{"label":"청구서 수신 국가","data_type":"/contact/country/billing"},{"label":"사업 국가","data_type":"/contact/country/business"},{"label":"본국","data_type":"/contact/country/home"},{"label":"국가","data_type":"/contact/country"},{"label":"선적 상태","data_type":"/contact/state/shipping"},{"label":"청구서 수신 상태","data_type":"/contact/state/billing"},{"label":"사업 상태","data_type":"/contact/state/business"},{"label":"가정 상태","data_type":"/contact/state/home"},{"label":"상태","data_type":"/contact/state"},{"label":"신용 카드 종류","data_type":"/financial/creditcard/type"},{"label":"신용 카드 유효 기간|카드 만료 (월)","data_type":"/financial/creditcard/expirymonth"},{"label":"만료 연도","data_type":"/financial/creditcard/expiryyear"},{"label":"월 및 연도","data_type":"/financial/creditcard/expiry2"},{"label":"만료","data_type":"/financial/creditcard/expiry"},{"label":"생년","data_type":"/birthdate/birthyear"},{"label":"탄생 월|생년월일","data_type":"/birthdate/birthmonth"},{"label":"탄생일|생년월일","data_type":"/birthdate/birthday"},{"label":"생일","data_type":"/birthdate"}]},"ru":{"text":[{"label":"captcha","data_type":"/captcha"},{"label":"поиск","data_type":"/search"},{"label":"намек","data_type":"/auth/hint"},{"label":"ответ","data_type":"/auth/answer"},{"label":"вопрос|вызов","data_type":"/auth/question"},{"label":"Эл. адрес|Эл. почта","data_type":"/contact/email"},{"label":"Компания|название компании","data_type":"/company/name"},{"label":"дружелюбный|имя пользователя|авторизоваться|пользователь","data_type":"/nameperson/friendly"},{"label":"префикс","data_type":"/nameperson/prefix"},{"label":"последний|Фамилия","negative":"первый","data_type":"/nameperson/last"},{"label":"первый","negative":"последний","data_type":"/nameperson/first"},{"label":"средний|второе имя","negative":"последний|первый","data_type":"/nameperson/middle"},{"label":"полное имя","data_type":"/nameperson/full"},{"label":"суффикс","data_type":"/nameperson/suffix"},{"label":"пароль","data_type":"/password"},{"label":"пароль старый","data_type":"/passwordold"},{"label":"щебетать","data_type":"/contact/im/twitter"},{"label":"linkedin|связаны|связаны в","data_type":"/contact/web/linkedin"},{"label":"блог","data_type":"/contact/web/blog"},{"label":"цель|aol instant messenger","data_type":"/contact/IM/aim"},{"label":"icq","data_type":"/contact/IM/icq"},{"label":"msn","data_type":"/contact/IM/msn"},{"label":"трескотня","data_type":"/contact/IM/jabber"},{"label":"скайп","data_type":"/contact/IM/skype"},{"label":"yahoo","data_type":"/contact/IM/yahoo"},{"label":"Рабочий телефон|бизнес-телефон","data_type":"/contact/phone/extension/work"},{"label":"расширение домашнего телефона","data_type":"/contact/phone/extension/home"},{"label":"расширение","data_type":"/contact/phone/extension"},{"label":"код зоны","data_type":"/contact/phone/areacode/work"},{"label":"код зоны","data_type":"/contact/phone/areacode"},{"label":"телефон","data_type":"/contact/phone/local/work"},{"label":"телефон","data_type":"/contact/phone/local/home"},{"label":"телефон","data_type":"/contact/phone/local"},{"label":"работа код страны","data_type":"/contact/phone/countrycode/work"},{"label":"код страны происхождения","data_type":"/contact/phone/countrycode/home"},{"label":"код страны","data_type":"/contact/phone/countrycode"},{"label":"мобильный телефон|клетка","data_type":"/contact/phone/mobile"},{"label":"факс","data_type":"/contact/phone/fax"},{"label":"Рабочий телефон|бизнес-телефон|корпоративный телефон|бизнес линия","data_type":"/contact/phone/work"},{"label":"домашний телефон","data_type":"/contact/phone/home"},{"label":"Телефон","data_type":"/contact/phone"},{"label":"отправка почтовый индекс","data_type":"/contact/postalcode/shipping"},{"label":"индекс по банковскому переводу, по счету","data_type":"/contact/postalcode/billing"},{"label":"бизнес почтовый индекс|работа почтовый индекс|Почтовый индекс","data_type":"/contact/postalcode/business"},{"label":"домашний почтовый индекс","data_type":"/contact/postalcode/home"},{"label":"почтовый индекс|Почтовый индекс","data_type":"/contact/postalcode"},{"label":"отправка город","data_type":"/contact/city/shipping"},{"label":"биллинг город","data_type":"/contact/city/billing"},{"label":"деловой части города","data_type":"/contact/city/business"},{"label":"родной город","data_type":"/contact/city/home"},{"label":"город","data_type":"/contact/city"},{"label":"отправка страна","data_type":"/contact/country/shipping"},{"label":"страной плательщика","data_type":"/contact/country/billing"},{"label":"бизнес-страна","data_type":"/contact/country/business"},{"label":"Родина","data_type":"/contact/country/home"},{"label":"страна","data_type":"/contact/country"},{"label":"отправка состояние","data_type":"/contact/state/shipping"},{"label":"состояние счетов","data_type":"/contact/state/billing"},{"label":"бизнес-состояние","data_type":"/contact/state/business"},{"label":"родной штат","data_type":"/contact/state/home"},{"label":"государство","data_type":"/contact/state"},{"label":"доставка плюс 4|Адресная строка 2|склонный|набор","data_type":"/contact/postaladdressAdditional/shipping"},{"label":"биллинг плюс 4|биллинг линия 2|склонный|набор","data_type":"/contact/postaladdressAdditional/billing"},{"label":"бизнес плюс 4","data_type":"/contact/postaladdressAdditional/business"},{"label":"домой плюс 4","data_type":"/contact/postaladdressAdditional/home"},{"label":"плюс 4","data_type":"/contact/postaladdressAdditional"},{"label":"Адрес доставки","data_type":"/contact/postaladdress/shipping"},{"label":"Платежный адрес","data_type":"/contact/postaladdress/billing"},{"label":"служебный адрес","data_type":"/contact/postaladdress/business"},{"label":"домашний адрес","data_type":"/contact/postaladdress/home"},{"label":"адрес","data_type":"/contact/postaladdress"},{"label":"сообщение","data_type":"/message"},{"label":"предмет","data_type":"/subject"},{"label":"первое имя на кредитной карте","data_type":"/financial/creditcard/issuedto/first"},{"label":"фамилия на кредитной карте","data_type":"/financial/creditcard/issuedto/last"},{"label":"эмитент кредитных карточек","data_type":"/financial/creditcard/issuedto"},{"label":"кредитной карты cvv","data_type":"/financial/creditcard/verification"},{"label":"номер кредитной карты, часть 1","data_type":"/financial/creditcard/number/part1"},{"label":"номер кредитной карты, часть 2","data_type":"/financial/creditcard/number/part2"},{"label":"номер кредитной карты, часть 3","data_type":"/financial/creditcard/number/part3"},{"label":"номер кредитной карты часть 4","data_type":"/financial/creditcard/number/part4"},{"label":"Номер кредитной карты","data_type":"/financial/creditcard/number"},{"label":"Тип кредитной карты","data_type":"/financial/creditcard/type"},{"label":"истечения срока действия кредитной карты месяц|карты истекает (месяц)","data_type":"/financial/creditcard/expirymonth"},{"label":"год окончания срока действия","data_type":"/financial/creditcard/expiryyear"},{"label":"месяц и год","data_type":"/financial/creditcard/expiry2"},{"label":"истечение","data_type":"/financial/creditcard/expiry"},{"label":"год рождения","data_type":"/birthdate/birthyear"},{"label":"Месяц рождения|месяц рождения","data_type":"/birthdate/birthmonth"},{"label":"день рождения","data_type":"/birthdate/birthday"},{"label":"Дата рождения|день рождения","data_type":"/birthdate"},{"label":"штырь","data_type":"/pin"},{"label":"Пол","data_type":"/person/gender"},{"label":"дать согласие","data_type":"/agreetos"},{"label":"Запомни меня","data_type":"/rememberme"}],"password":[{"label":"пароль","data_type":"/password"},{"label":"пароль старый","data_type":"/passwordold"},{"label":"кредитной карты cvv","data_type":"/financial/creditcard/verification"},{"label":"штырь","data_type":"/pin"}],"select":[{"label":"работа код страны","data_type":"/contact/phone/countrycode/work"},{"label":"код страны происхождения","data_type":"/contact/phone/countrycode/home"},{"label":"код страны","data_type":"/contact/phone/countrycode"},{"label":"отправка страна","data_type":"/contact/country/shipping"},{"label":"страной плательщика","data_type":"/contact/country/billing"},{"label":"бизнес-страна","data_type":"/contact/country/business"},{"label":"Родина","data_type":"/contact/country/home"},{"label":"страна","data_type":"/contact/country"},{"label":"отправка состояние","data_type":"/contact/state/shipping"},{"label":"состояние счетов","data_type":"/contact/state/billing"},{"label":"бизнес-состояние","data_type":"/contact/state/business"},{"label":"родной штат","data_type":"/contact/state/home"},{"label":"государство","data_type":"/contact/state"},{"label":"Тип кредитной карты","data_type":"/financial/creditcard/type"},{"label":"истечения срока действия кредитной карты месяц|карты истекает (месяц)","data_type":"/financial/creditcard/expirymonth"},{"label":"год окончания срока действия","data_type":"/financial/creditcard/expiryyear"},{"label":"месяц и год","data_type":"/financial/creditcard/expiry2"},{"label":"истечение","data_type":"/financial/creditcard/expiry"},{"label":"год рождения","data_type":"/birthdate/birthyear"},{"label":"Месяц рождения|месяц рождения","data_type":"/birthdate/birthmonth"},{"label":"день рождения","data_type":"/birthdate/birthday"},{"label":"Дата рождения|день рождения","data_type":"/birthdate"}]},"he":{"text":[{"label":"captcha","data_type":"/captcha"},{"label":"לחפש","data_type":"/search"},{"label":"רֶמֶז","data_type":"/auth/hint"},{"label":"תשובה","data_type":"/auth/answer"},{"label":"שְׁאֵלָה|אתגר","data_type":"/auth/question"},{"label":"אֶלֶקטרוֹנִי","data_type":"/contact/email"},{"label":"חֶברָה|שם החברה","data_type":"/company/name"},{"label":"יְדִידוּתִי|שם משתמש|התחבר|מִשׁתַמֵשׁ","data_type":"/nameperson/friendly"},{"label":"קידומת","data_type":"/nameperson/prefix"},{"label":"אחרון|שם משפחה","negative":"ראשון","data_type":"/nameperson/last"},{"label":"ראשון","negative":"אחרון","data_type":"/nameperson/first"},{"label":"אֶמצַע|שם אמצעי","negative":"אחרון|ראשון","data_type":"/nameperson/middle"},{"label":"שם מלא","data_type":"/nameperson/full"},{"label":"סִיוֹמֶת","data_type":"/nameperson/suffix"},{"label":"סיסמה","data_type":"/password"},{"label":"סיסמא ישנה","data_type":"/passwordold"},{"label":"טוויטר","data_type":"/contact/im/twitter"},{"label":"linkedin|מקושר|צמוד ב","data_type":"/contact/web/linkedin"},{"label":"בלוג","data_type":"/contact/web/blog"},{"label":"מַטָרָה|מיידית aol","data_type":"/contact/IM/aim"},{"label":"icq","data_type":"/contact/IM/icq"},{"label":"msn","data_type":"/contact/IM/msn"},{"label":"לְקַשְׁקֵשׁ","data_type":"/contact/IM/jabber"},{"label":"סקייפ","data_type":"/contact/IM/skype"},{"label":"yahoo","data_type":"/contact/IM/yahoo"},{"label":"טלפון עבודה|טלפון עסקי","data_type":"/contact/phone/extension/work"},{"label":"סיומת טלפון בבית","data_type":"/contact/phone/extension/home"},{"label":"סיומת","data_type":"/contact/phone/extension"},{"label":"מיקוד","data_type":"/contact/phone/areacode/work"},{"label":"מיקוד","data_type":"/contact/phone/areacode"},{"label":"טֵלֵפוֹן","data_type":"/contact/phone/local/work"},{"label":"טֵלֵפוֹן","data_type":"/contact/phone/local/home"},{"label":"טֵלֵפוֹן","data_type":"/contact/phone/local"},{"label":"קידומת מדינת עבודה","data_type":"/contact/phone/countrycode/work"},{"label":"קידומת מדינה הביתה","data_type":"/contact/phone/countrycode/home"},{"label":"קוד מדינה","data_type":"/contact/phone/countrycode"},{"label":"טלפון נייד|תָא","data_type":"/contact/phone/mobile"},{"label":"פַקס","data_type":"/contact/phone/fax"},{"label":"טלפון עבודה|טלפון עסקי|בטלפון של החברה|קו עסקי","data_type":"/contact/phone/work"},{"label":"טלפון בבית","data_type":"/contact/phone/home"},{"label":"טלפון","data_type":"/contact/phone"},{"label":"מיקוד משלוח|משלוח מיקוד","data_type":"/contact/postalcode/shipping"},{"label":"מיקוד לחיוב","data_type":"/contact/postalcode/billing"},{"label":"מיקוד עסקי|מיקוד עבודה","data_type":"/contact/postalcode/business"},{"label":"מיקוד בבית|מיקוד הבית","data_type":"/contact/postalcode/home"},{"label":"מיקוד","data_type":"/contact/postalcode"},{"label":"עיר משלוח","data_type":"/contact/city/shipping"},{"label":"עיר חיוב","data_type":"/contact/city/billing"},{"label":"עיר עסקים","data_type":"/contact/city/business"},{"label":"עיר מגורים","data_type":"/contact/city/home"},{"label":"עִיר","data_type":"/contact/city"},{"label":"ארץ משלוח","data_type":"/contact/country/shipping"},{"label":"ארץ חיוב","data_type":"/contact/country/billing"},{"label":"מדינת עסק","data_type":"/contact/country/business"},{"label":"מולדת","data_type":"/contact/country/home"},{"label":"מדינה","data_type":"/contact/country"},{"label":"מדינת משלוח","data_type":"/contact/state/shipping"},{"label":"מדינת חיוב","data_type":"/contact/state/billing"},{"label":"מדינת עסק","data_type":"/contact/state/business"},{"label":"מדינה הביתה","data_type":"/contact/state/home"},{"label":"מדינה","data_type":"/contact/state"},{"label":"משלוח בתוספת 4|שורת כתובת 2|מַתְאִים|סְוִיטָה","data_type":"/contact/postaladdressAdditional/shipping"},{"label":"החיוב בתוספת 4|חיוב קו 2|מַתְאִים|סְוִיטָה","data_type":"/contact/postaladdressAdditional/billing"},{"label":"עסקים פלוס 4","data_type":"/contact/postaladdressAdditional/business"},{"label":"הבית בתוספת 4","data_type":"/contact/postaladdressAdditional/home"},{"label":"בתוספת 4","data_type":"/contact/postaladdressAdditional"},{"label":"כתובת למשלוח","data_type":"/contact/postaladdress/shipping"},{"label":"כתובת לחיוב","data_type":"/contact/postaladdress/billing"},{"label":"כתובת עסקית","data_type":"/contact/postaladdress/business"},{"label":"כתובת בית","data_type":"/contact/postaladdress/home"},{"label":"כתובת","data_type":"/contact/postaladdress"},{"label":"הוֹדָעָה","data_type":"/message"},{"label":"נושא","data_type":"/subject"},{"label":"שם פרטי בכרטיס אשראי","data_type":"/financial/creditcard/issuedto/first"},{"label":"שם המשפחה על כרטיס אשראי","data_type":"/financial/creditcard/issuedto/last"},{"label":"חברת כרטיס אשראי","data_type":"/financial/creditcard/issuedto"},{"label":"כרטיס האשראי cvv","data_type":"/financial/creditcard/verification"},{"label":"מספר כרטיס אשראי חלק 1","data_type":"/financial/creditcard/number/part1"},{"label":"מספר כרטיס אשראי חלק 2","data_type":"/financial/creditcard/number/part2"},{"label":"מספר כרטיס אשראי חלק 3","data_type":"/financial/creditcard/number/part3"},{"label":"מספר כרטיס אשראי חלק 4","data_type":"/financial/creditcard/number/part4"},{"label":"מספר כרטיס אשראי","data_type":"/financial/creditcard/number"},{"label":"סוג כרטיס אשראי","data_type":"/financial/creditcard/type"},{"label":"חודש תוקף של כרטיס אשראי|כרטיס יפוג (חודש)","data_type":"/financial/creditcard/expirymonth"},{"label":"שנת פקיעת תוקף","data_type":"/financial/creditcard/expiryyear"},{"label":"חודש ושנה","data_type":"/financial/creditcard/expiry2"},{"label":"תפוגה","data_type":"/financial/creditcard/expiry"},{"label":"שנת לידה","data_type":"/birthdate/birthyear"},{"label":"חודש לידה|חודש הלידה","data_type":"/birthdate/birthmonth"},{"label":"יום הולדת|יום ההולדת","data_type":"/birthdate/birthday"},{"label":"תאריך לידה|יום הולדת","data_type":"/birthdate"},{"label":"פִּין","data_type":"/pin"},{"label":"מִין","data_type":"/person/gender"},{"label":"לְהַסכִּים","data_type":"/agreetos"},{"label":"זכור אותי","data_type":"/rememberme"}],"password":[{"label":"סיסמה","data_type":"/password"},{"label":"סיסמא ישנה","data_type":"/passwordold"},{"label":"כרטיס האשראי cvv","data_type":"/financial/creditcard/verification"},{"label":"פִּין","data_type":"/pin"}],"select":[{"label":"קידומת מדינת עבודה","data_type":"/contact/phone/countrycode/work"},{"label":"קידומת מדינה הביתה","data_type":"/contact/phone/countrycode/home"},{"label":"קוד מדינה","data_type":"/contact/phone/countrycode"},{"label":"ארץ משלוח","data_type":"/contact/country/shipping"},{"label":"ארץ חיוב","data_type":"/contact/country/billing"},{"label":"מדינת עסק","data_type":"/contact/country/business"},{"label":"מולדת","data_type":"/contact/country/home"},{"label":"מדינה","data_type":"/contact/country"},{"label":"מדינת משלוח","data_type":"/contact/state/shipping"},{"label":"מדינת חיוב","data_type":"/contact/state/billing"},{"label":"מדינת עסק","data_type":"/contact/state/business"},{"label":"מדינה הביתה","data_type":"/contact/state/home"},{"label":"מדינה","data_type":"/contact/state"},{"label":"סוג כרטיס אשראי","data_type":"/financial/creditcard/type"},{"label":"חודש תוקף של כרטיס אשראי|כרטיס יפוג (חודש)","data_type":"/financial/creditcard/expirymonth"},{"label":"שנת פקיעת תוקף","data_type":"/financial/creditcard/expiryyear"},{"label":"חודש ושנה","data_type":"/financial/creditcard/expiry2"},{"label":"תפוגה","data_type":"/financial/creditcard/expiry"},{"label":"שנת לידה","data_type":"/birthdate/birthyear"},{"label":"חודש לידה|חודש הלידה","data_type":"/birthdate/birthmonth"},{"label":"יום הולדת|יום ההולדת","data_type":"/birthdate/birthday"},{"label":"תאריך לידה|יום הולדת","data_type":"/birthdate"}]},"ar":{"text":[{"label":"كلمة التحقق","data_type":"/captcha"},{"label":"بحث","data_type":"/search"},{"label":"تلميح","data_type":"/auth/hint"},{"label":"إجابة","data_type":"/auth/answer"},{"label":"سؤال|التحدي","data_type":"/auth/question"},{"label":"البريد الإلكتروني","data_type":"/contact/email"},{"label":"شركة|اسم الشركة","data_type":"/company/name"},{"label":"ودود|اسم المستخدم|تسجيل الدخول","data_type":"/nameperson/friendly"},{"label":"بادئة","data_type":"/nameperson/prefix"},{"label":"آخر|الكنية","negative":"الأول","data_type":"/nameperson/last"},{"label":"الأول","negative":"آخر","data_type":"/nameperson/first"},{"label":"وسط|الاسم الأوسط","negative":"آخر|الأول","data_type":"/nameperson/middle"},{"label":"الاسم الكامل","data_type":"/nameperson/full"},{"label":"لاحقة","data_type":"/nameperson/suffix"},{"label":"كلمه السر","data_type":"/password"},{"label":"كلمة المرور القديمة","data_type":"/passwordold"},{"label":"تغريد","data_type":"/contact/im/twitter"},{"label":"ينكدين|ترتبط في","data_type":"/contact/web/linkedin"},{"label":"مدونة","data_type":"/contact/web/blog"},{"label":"هدف|أمريكا أون لاين لحظة رسول","data_type":"/contact/IM/aim"},{"label":"icq","data_type":"/contact/IM/icq"},{"label":"ام اس ان","data_type":"/contact/IM/msn"},{"label":"بربر","data_type":"/contact/IM/jabber"},{"label":"سكايب","data_type":"/contact/IM/skype"},{"label":"ياهو","data_type":"/contact/IM/yahoo"},{"label":"هاتف عمل|هاتف العمل","data_type":"/contact/phone/extension/work"},{"label":"تمديد هاتف المنزل","data_type":"/contact/phone/extension/home"},{"label":"تمديد","data_type":"/contact/phone/extension"},{"label":"رمز المنطقة","data_type":"/contact/phone/areacode/work"},{"label":"رمز المنطقة","data_type":"/contact/phone/areacode"},{"label":"هاتف","data_type":"/contact/phone/local/work"},{"label":"هاتف","data_type":"/contact/phone/local/home"},{"label":"هاتف","data_type":"/contact/phone/local"},{"label":"رمز البلد العمل","data_type":"/contact/phone/countrycode/work"},{"label":"رمز البلد المنزل","data_type":"/contact/phone/countrycode/home"},{"label":"رقم البلد","data_type":"/contact/phone/countrycode"},{"label":"تليفون محمول|زنزانة","data_type":"/contact/phone/mobile"},{"label":"الفاكس","data_type":"/contact/phone/fax"},{"label":"هاتف عمل|هاتف العمل|الهاتف الشركات|خط الأعمال","data_type":"/contact/phone/work"},{"label":"هاتف المنزل","data_type":"/contact/phone/home"},{"label":"هاتف","data_type":"/contact/phone"},{"label":"الرمز البريدي الشحن|الشحن الرمز البريدي","data_type":"/contact/postalcode/shipping"},{"label":"الفواتير الرمز البريدي","data_type":"/contact/postalcode/billing"},{"label":"كود الأعمال البريدي|الرمز البريدي العمل","data_type":"/contact/postalcode/business"},{"label":"كود المنزل البريدي|الرمز البريدي المنزل","data_type":"/contact/postalcode/home"},{"label":"الرمز البريدي","data_type":"/contact/postalcode"},{"label":"مدينة الشحن","data_type":"/contact/city/shipping"},{"label":"مدينة الفواتير","data_type":"/contact/city/billing"},{"label":"مدينة الأعمال","data_type":"/contact/city/business"},{"label":"مسقط","data_type":"/contact/city/home"},{"label":"مدينة","data_type":"/contact/city"},{"label":"بلد الشحن","data_type":"/contact/country/shipping"},{"label":"بلد إرسال الفواتير","data_type":"/contact/country/billing"},{"label":"دولة العمل","data_type":"/contact/country/business"},{"label":"الوطن","data_type":"/contact/country/home"},{"label":"بلد","data_type":"/contact/country"},{"label":"الدولة الشحن","data_type":"/contact/state/shipping"},{"label":"الدولة الفواتير","data_type":"/contact/state/billing"},{"label":"دولة العمل","data_type":"/contact/state/business"},{"label":"الوطن","data_type":"/contact/state/home"},{"label":"حالة","data_type":"/contact/state"},{"label":"الشحن بالإضافة إلى 4|سطر العنوان 2|عرضة|جناح","data_type":"/contact/postaladdressAdditional/shipping"},{"label":"الفواتير بالإضافة إلى 4|خط الفواتير 2|عرضة|جناح","data_type":"/contact/postaladdressAdditional/billing"},{"label":"الأعمال بالإضافة إلى 4","data_type":"/contact/postaladdressAdditional/business"},{"label":"المنزل بالإضافة إلى 4","data_type":"/contact/postaladdressAdditional/home"},{"label":"بالإضافة إلى 4","data_type":"/contact/postaladdressAdditional"},{"label":"عنوان الشحن","data_type":"/contact/postaladdress/shipping"},{"label":"عنوان وصول الفواتير","data_type":"/contact/postaladdress/billing"},{"label":"عنوان العمل","data_type":"/contact/postaladdress/business"},{"label":"عنوان المنزل","data_type":"/contact/postaladdress/home"},{"label":"عنوان","data_type":"/contact/postaladdress"},{"label":"الرسالة","data_type":"/message"},{"label":"موضوع","data_type":"/subject"},{"label":"الاسم الأول على بطاقة الائتمان","data_type":"/financial/creditcard/issuedto/first"},{"label":"الاسم الأخير على بطاقة الائتمان","data_type":"/financial/creditcard/issuedto/last"},{"label":"بطاقة الائتمان المصدر","data_type":"/financial/creditcard/issuedto"},{"label":"بطاقة الائتمان cvv","data_type":"/financial/creditcard/verification"},{"label":"رقم بطاقة الائتمان جزء 1","data_type":"/financial/creditcard/number/part1"},{"label":"رقم بطاقة الائتمان جزء 2","data_type":"/financial/creditcard/number/part2"},{"label":"رقم بطاقة الائتمان جزء 3","data_type":"/financial/creditcard/number/part3"},{"label":"رقم بطاقة الائتمان جزء 4","data_type":"/financial/creditcard/number/part4"},{"label":"رقم بطاقة الائتمان","data_type":"/financial/creditcard/number"},{"label":"نوع بطاقة الائتمان","data_type":"/financial/creditcard/type"},{"label":"انتهاء صلاحية بطاقة الائتمان الشهر|انتهاء بطاقة (شهر)","data_type":"/financial/creditcard/expirymonth"},{"label":"عام انتهاء","data_type":"/financial/creditcard/expiryyear"},{"label":"الشهر والسنة","data_type":"/financial/creditcard/expiry2"},{"label":"انتهاء","data_type":"/financial/creditcard/expiry"},{"label":"سنة الميلاد","data_type":"/birthdate/birthyear"},{"label":"شهر الميلاد|شهر الولادة","data_type":"/birthdate/birthmonth"},{"label":"تاريخ الميلاد|يوم الميلاد","data_type":"/birthdate/birthday"},{"label":"تاريخ الميلاد","data_type":"/birthdate"},{"label":"دبوس","data_type":"/pin"},{"label":"جنس","data_type":"/person/gender"},{"label":"توافق","data_type":"/agreetos"},{"label":"تذكرنى","data_type":"/rememberme"}],"password":[{"label":"كلمه السر","data_type":"/password"},{"label":"كلمة المرور القديمة","data_type":"/passwordold"},{"label":"بطاقة الائتمان cvv","data_type":"/financial/creditcard/verification"},{"label":"دبوس","data_type":"/pin"}],"select":[{"label":"رمز البلد العمل","data_type":"/contact/phone/countrycode/work"},{"label":"رمز البلد المنزل","data_type":"/contact/phone/countrycode/home"},{"label":"رقم البلد","data_type":"/contact/phone/countrycode"},{"label":"بلد الشحن","data_type":"/contact/country/shipping"},{"label":"بلد إرسال الفواتير","data_type":"/contact/country/billing"},{"label":"دولة العمل","data_type":"/contact/country/business"},{"label":"الوطن","data_type":"/contact/country/home"},{"label":"بلد","data_type":"/contact/country"},{"label":"الدولة الشحن","data_type":"/contact/state/shipping"},{"label":"الدولة الفواتير","data_type":"/contact/state/billing"},{"label":"دولة العمل","data_type":"/contact/state/business"},{"label":"الوطن","data_type":"/contact/state/home"},{"label":"حالة","data_type":"/contact/state"},{"label":"نوع بطاقة الائتمان","data_type":"/financial/creditcard/type"},{"label":"انتهاء صلاحية بطاقة الائتمان الشهر|انتهاء بطاقة (شهر)","data_type":"/financial/creditcard/expirymonth"},{"label":"عام انتهاء","data_type":"/financial/creditcard/expiryyear"},{"label":"الشهر والسنة","data_type":"/financial/creditcard/expiry2"},{"label":"انتهاء","data_type":"/financial/creditcard/expiry"},{"label":"سنة الميلاد","data_type":"/birthdate/birthyear"},{"label":"شهر الميلاد|شهر الولادة","data_type":"/birthdate/birthmonth"},{"label":"تاريخ الميلاد|يوم الميلاد","data_type":"/birthdate/birthday"},{"label":"تاريخ الميلاد","data_type":"/birthdate"}]},"bn":{"text":[{"label":"ক্যাপচা","data_type":"/captcha"},{"label":"অনুসন্ধান","data_type":"/search"},{"label":"ইঙ্গিত","data_type":"/auth/hint"},{"label":"উত্তর","data_type":"/auth/answer"},{"label":"প্রশ্ন|চ্যালেঞ্জ","data_type":"/auth/question"},{"label":"ই-মেইল","data_type":"/contact/email"},{"label":"কোম্পানির|কোমপানির নাম","data_type":"/company/name"},{"label":"বন্ধুত্বপূর্ণ|ব্যবহারকারীর নাম|লগইন","data_type":"/nameperson/friendly"},{"label":"উপসর্গ","data_type":"/nameperson/prefix"},{"label":"গত|নামের শেষাংশ","negative":"প্রথম","data_type":"/nameperson/last"},{"label":"প্রথম","negative":"গত","data_type":"/nameperson/first"},{"label":"মধ্যম|নামের মধ্যাংশ","negative":"গত|প্রথম","data_type":"/nameperson/middle"},{"label":"পূর্ণ নাম","data_type":"/nameperson/full"},{"label":"প্রত্যয়","data_type":"/nameperson/suffix"},{"label":"পাসওয়ার্ড","data_type":"/password"},{"label":"পাসওয়ার্ড পুরাতন","data_type":"/passwordold"},{"label":"টুইটার","data_type":"/contact/im/twitter"},{"label":"লিঙ্কডইন|লিঙ্ক-ইন","data_type":"/contact/web/linkedin"},{"label":"ব্লগ","data_type":"/contact/web/blog"},{"label":"লক্ষ্য|এওএল ইনস্ট্যান্ট মেসেঞ্জার","data_type":"/contact/IM/aim"},{"label":"icq","data_type":"/contact/IM/icq"},{"label":"এমএসএন","data_type":"/contact/IM/msn"},{"label":"বক্বক্","data_type":"/contact/IM/jabber"},{"label":"স্কাইপ","data_type":"/contact/IM/skype"},{"label":"নরপশু","data_type":"/contact/IM/yahoo"},{"label":"কাজের ফোন|ব্যবসায়িক ফোন","data_type":"/contact/phone/extension/work"},{"label":"বাড়িতে ফোন এক্সটেনশন","data_type":"/contact/phone/extension/home"},{"label":"প্রসার","data_type":"/contact/phone/extension"},{"label":"এরিয়া কোড","data_type":"/contact/phone/areacode/work"},{"label":"এরিয়া কোড","data_type":"/contact/phone/areacode"},{"label":"টেলিফোন","data_type":"/contact/phone/local/work"},{"label":"টেলিফোন","data_type":"/contact/phone/local/home"},{"label":"টেলিফোন","data_type":"/contact/phone/local"},{"label":"কাজ কান্ট্রি কোড","data_type":"/contact/phone/countrycode/work"},{"label":"হোম কান্ট্রি কোড","data_type":"/contact/phone/countrycode/home"},{"label":"কান্ট্রি কোড","data_type":"/contact/phone/countrycode"},{"label":"মোবাইল ফোন|কোষ","data_type":"/contact/phone/mobile"},{"label":"ফ্যাক্স","data_type":"/contact/phone/fax"},{"label":"কাজের ফোন|ব্যবসায়িক ফোন|কর্পোরেট ফোন|ব্যবসার লাইন","data_type":"/contact/phone/work"},{"label":"বাসার ফোন","data_type":"/contact/phone/home"},{"label":"ফোন","data_type":"/contact/phone"},{"label":"শিপিং জিপ কোড|শিপিং পোস্টাল কোড","data_type":"/contact/postalcode/shipping"},{"label":"বিল করার জন্য জিপ কোড","data_type":"/contact/postalcode/billing"},{"label":"ব্যবসা জিপ কোড|কাজ জিপ কোড|পোস্ট অফিসের নাম্বার","data_type":"/contact/postalcode/business"},{"label":"হোম জিপ কোড|হোম পোস্টাল কোড","data_type":"/contact/postalcode/home"},{"label":"জিপ কোড|পিনকোড|পোস্ট অফিসের নাম্বার","data_type":"/contact/postalcode"},{"label":"শিপিং শহর","data_type":"/contact/city/shipping"},{"label":"বিলিং শহর","data_type":"/contact/city/billing"},{"label":"শহর","data_type":"/contact/city/business"},{"label":"হোম শহর","data_type":"/contact/city/home"},{"label":"শহর","data_type":"/contact/city"},{"label":"শিপিং দেশ","data_type":"/contact/country/shipping"},{"label":"বিলিং দেশ","data_type":"/contact/country/billing"},{"label":"ব্যবসার দেশ","data_type":"/contact/country/business"},{"label":"নিজের দেশ","data_type":"/contact/country/home"},{"label":"দেশ","data_type":"/contact/country"},{"label":"শিপিং রাষ্ট্র","data_type":"/contact/state/shipping"},{"label":"বিলিং রাষ্ট্র","data_type":"/contact/state/billing"},{"label":"ব্যবসা রাষ্ট্র","data_type":"/contact/state/business"},{"label":"জন্মস্থান","data_type":"/contact/state/home"},{"label":"অবস্থা","data_type":"/contact/state"},{"label":"শিপিং প্লাস 4|ঠিকানা লাইন 2|চটপটে|স্যুট","data_type":"/contact/postaladdressAdditional/shipping"},{"label":"বিলিং প্লাস 4|বিলিং লাইন 2|চটপটে|স্যুট","data_type":"/contact/postaladdressAdditional/billing"},{"label":"ব্যবসা প্লাস 4","data_type":"/contact/postaladdressAdditional/business"},{"label":"হোম প্লাস 4","data_type":"/contact/postaladdressAdditional/home"},{"label":"প্লাস 4","data_type":"/contact/postaladdressAdditional"},{"label":"প্রেরণের ঠিকানা","data_type":"/contact/postaladdress/shipping"},{"label":"বিলিং ঠিকানা","data_type":"/contact/postaladdress/billing"},{"label":"ব্যবসা ঠিকানা","data_type":"/contact/postaladdress/business"},{"label":"বাসার ঠিকানা","data_type":"/contact/postaladdress/home"},{"label":"ঠিকানা","data_type":"/contact/postaladdress"},{"label":"বার্তা","data_type":"/message"},{"label":"বিষয়","data_type":"/subject"},{"label":"ক্রেডিট কার্ডে নামের প্রথম অংশ","data_type":"/financial/creditcard/issuedto/first"},{"label":"ক্রেডিট কার্ডে নামের শেষাংশ","data_type":"/financial/creditcard/issuedto/last"},{"label":"ক্রেডিট কার্ড ইস্যুকারী","data_type":"/financial/creditcard/issuedto"},{"label":"ক্রেডিট কার্ডের cvv","data_type":"/financial/creditcard/verification"},{"label":"ক্রেডিট কার্ড নম্বর অংশ 1","data_type":"/financial/creditcard/number/part1"},{"label":"ক্রেডিট কার্ড নম্বর অংশ 2","data_type":"/financial/creditcard/number/part2"},{"label":"ক্রেডিট কার্ড নম্বর অংশ 3","data_type":"/financial/creditcard/number/part3"},{"label":"ক্রেডিট কার্ড নম্বর অংশ 4","data_type":"/financial/creditcard/number/part4"},{"label":"ক্রেডিট কার্ড নম্বর","data_type":"/financial/creditcard/number"},{"label":"ক্রেডিট কার্ড টাইপ","data_type":"/financial/creditcard/type"},{"label":"ক্রেডিট কার্ডের মেয়াদ উত্তীর্ণের মাস|কার্ড মেয়াদ শেষ (মাস)","data_type":"/financial/creditcard/expirymonth"},{"label":"মেয়াদ শেষের বছর","data_type":"/financial/creditcard/expiryyear"},{"label":"মাস এবং বছর","data_type":"/financial/creditcard/expiry2"},{"label":"শ্বাসত্যাগ","data_type":"/financial/creditcard/expiry"},{"label":"জন্ম সাল|জন্মসাল","data_type":"/birthdate/birthyear"},{"label":"জন্ম মাস|জন্মের মাস","data_type":"/birthdate/birthmonth"},{"label":"জন্ম দিন","data_type":"/birthdate/birthday"},{"label":"জন্ম তারিখ|জন্মদিন","data_type":"/birthdate"},{"label":"পিন","data_type":"/pin"},{"label":"লিঙ্গ","data_type":"/person/gender"},{"label":"একমত","data_type":"/agreetos"},{"label":"আমাকে মনে কর","data_type":"/rememberme"}],"password":[{"label":"পাসওয়ার্ড","data_type":"/password"},{"label":"পাসওয়ার্ড পুরাতন","data_type":"/passwordold"},{"label":"ক্রেডিট কার্ডের cvv","data_type":"/financial/creditcard/verification"},{"label":"পিন","data_type":"/pin"}],"select":[{"label":"কাজ কান্ট্রি কোড","data_type":"/contact/phone/countrycode/work"},{"label":"হোম কান্ট্রি কোড","data_type":"/contact/phone/countrycode/home"},{"label":"কান্ট্রি কোড","data_type":"/contact/phone/countrycode"},{"label":"শিপিং দেশ","data_type":"/contact/country/shipping"},{"label":"বিলিং দেশ","data_type":"/contact/country/billing"},{"label":"ব্যবসার দেশ","data_type":"/contact/country/business"},{"label":"নিজের দেশ","data_type":"/contact/country/home"},{"label":"দেশ","data_type":"/contact/country"},{"label":"শিপিং রাষ্ট্র","data_type":"/contact/state/shipping"},{"label":"বিলিং রাষ্ট্র","data_type":"/contact/state/billing"},{"label":"ব্যবসা রাষ্ট্র","data_type":"/contact/state/business"},{"label":"জন্মস্থান","data_type":"/contact/state/home"},{"label":"অবস্থা","data_type":"/contact/state"},{"label":"ক্রেডিট কার্ড টাইপ","data_type":"/financial/creditcard/type"},{"label":"ক্রেডিট কার্ডের মেয়াদ উত্তীর্ণের মাস|কার্ড মেয়াদ শেষ (মাস)","data_type":"/financial/creditcard/expirymonth"},{"label":"মেয়াদ শেষের বছর","data_type":"/financial/creditcard/expiryyear"},{"label":"মাস এবং বছর","data_type":"/financial/creditcard/expiry2"},{"label":"শ্বাসত্যাগ","data_type":"/financial/creditcard/expiry"},{"label":"জন্ম সাল|জন্মসাল","data_type":"/birthdate/birthyear"},{"label":"জন্ম মাস|জন্মের মাস","data_type":"/birthdate/birthmonth"},{"label":"জন্ম দিন","data_type":"/birthdate/birthday"},{"label":"জন্ম তারিখ|জন্মদিন","data_type":"/birthdate"}]},"el":{"text":[{"label":"captcha","data_type":"/captcha"},{"label":"έρευνα","data_type":"/search"},{"label":"στοιχείο","data_type":"/auth/hint"},{"label":"απάντηση","data_type":"/auth/answer"},{"label":"ερώτηση|πρόκληση","data_type":"/auth/question"},{"label":"ΗΛΕΚΤΡΟΝΙΚΗ ΔΙΕΥΘΥΝΣΗ","data_type":"/contact/email"},{"label":"εταιρεία|Όνομα Εταιρίας","data_type":"/company/name"},{"label":"φιλικός|όνομα χρήστη|Σύνδεση|μεταχειριζόμενος","data_type":"/nameperson/friendly"},{"label":"πρόθεμα","data_type":"/nameperson/prefix"},{"label":"τελευταίο|επίθετο","negative":"πρώτα","data_type":"/nameperson/last"},{"label":"πρώτα","negative":"τελευταίο","data_type":"/nameperson/first"},{"label":"Μέσης|μεσαίο όνομα","negative":"τελευταίο|πρώτα","data_type":"/nameperson/middle"},{"label":"πλήρες όνομα","data_type":"/nameperson/full"},{"label":"κατάληξη","data_type":"/nameperson/suffix"},{"label":"σύνθημα","data_type":"/password"},{"label":"κωδικό παλιά","data_type":"/passwordold"},{"label":"Τουίτερ","data_type":"/contact/im/twitter"},{"label":"linkedin|συνδέονται σε|συνδεδεμένη στο","data_type":"/contact/web/linkedin"},{"label":"blog","data_type":"/contact/web/blog"},{"label":"σκοπός|aol instant messenger","data_type":"/contact/IM/aim"},{"label":"icq","data_type":"/contact/IM/icq"},{"label":"msn","data_type":"/contact/IM/msn"},{"label":"κουβεντολόι","data_type":"/contact/IM/jabber"},{"label":"skype","data_type":"/contact/IM/skype"},{"label":"yahoo","data_type":"/contact/IM/yahoo"},{"label":"τηλέφωνο εργασίας","data_type":"/contact/phone/extension/work"},{"label":"επέκταση το τηλέφωνο του σπιτιού","data_type":"/contact/phone/extension/home"},{"label":"επέκταση","data_type":"/contact/phone/extension"},{"label":"ταχυδρομικός κώδικας","data_type":"/contact/phone/areacode/work"},{"label":"ταχυδρομικός κώδικας","data_type":"/contact/phone/areacode"},{"label":"Τηλέφωνο","data_type":"/contact/phone/local/work"},{"label":"Τηλέφωνο","data_type":"/contact/phone/local/home"},{"label":"Τηλέφωνο","data_type":"/contact/phone/local"},{"label":"κωδικό χώρας εργασία","data_type":"/contact/phone/countrycode/work"},{"label":"Κωδικός χώρας καταγωγής","data_type":"/contact/phone/countrycode/home"},{"label":"κωδικός χώρας","data_type":"/contact/phone/countrycode"},{"label":"κινητό τηλέφωνο|κύτταρο","data_type":"/contact/phone/mobile"},{"label":"φαξ","data_type":"/contact/phone/fax"},{"label":"τηλέφωνο εργασίας|εταιρικό τηλέφωνο|επιχειρηματικό","data_type":"/contact/phone/work"},{"label":"το τηλέφωνο του σπιτιού","data_type":"/contact/phone/home"},{"label":"τηλέφωνο","data_type":"/contact/phone"},{"label":"ναυτιλία ταχυδρομικό κώδικα","data_type":"/contact/postalcode/shipping"},{"label":"χρέωσης των ΗΠΑ ταχυδρομικός κώδικας","data_type":"/contact/postalcode/billing"},{"label":"των επιχειρήσεων των ΗΠΑ ταχυδρομικός κώδικας|εργασία ταχυδρομικό κώδικα|Ταχυδρομικός Κώδικας","data_type":"/contact/postalcode/business"},{"label":"σπίτι ταχυδρομικό κώδικα","data_type":"/contact/postalcode/home"},{"label":"ταχυδρομικός κώδικας|Ταχυδρομικός Κώδικας","data_type":"/contact/postalcode"},{"label":"ναυτιλία πόλη","data_type":"/contact/city/shipping"},{"label":"πόλη χρέωσης","data_type":"/contact/city/billing"},{"label":"πόλη των επιχειρήσεων","data_type":"/contact/city/business"},{"label":"πόλη καταγωγής","data_type":"/contact/city/home"},{"label":"πόλη","data_type":"/contact/city"},{"label":"χώρα αποστολής","data_type":"/contact/country/shipping"},{"label":"χώρα χρέωσης","data_type":"/contact/country/billing"},{"label":"χώρα των επιχειρήσεων","data_type":"/contact/country/business"},{"label":"πατρίδα","data_type":"/contact/country/home"},{"label":"χώρα","data_type":"/contact/country"},{"label":"κρατική ναυτιλιακή","data_type":"/contact/state/shipping"},{"label":"κατάσταση χρέωσης","data_type":"/contact/state/billing"},{"label":"κατάσταση των επιχειρήσεων","data_type":"/contact/state/business"},{"label":"κράτος καταγωγής","data_type":"/contact/state/home"},{"label":"κατάσταση","data_type":"/contact/state"},{"label":"ναυτιλία συν 4|γραμμή διεύθυνσης 2|κατάλληλος|Σουίτα","data_type":"/contact/postaladdressAdditional/shipping"},{"label":"χρέωσης συν 4|γραμμή χρέωσης 2|κατάλληλος|Σουίτα","data_type":"/contact/postaladdressAdditional/billing"},{"label":"business plus 4","data_type":"/contact/postaladdressAdditional/business"},{"label":"σπίτι συν 4","data_type":"/contact/postaladdressAdditional/home"},{"label":"συν 4","data_type":"/contact/postaladdressAdditional"},{"label":"διεύθυνση αποστολής","data_type":"/contact/postaladdress/shipping"},{"label":"διεύθυνση χρέωσης","data_type":"/contact/postaladdress/billing"},{"label":"διεύθυνση επιχείρησης","data_type":"/contact/postaladdress/business"},{"label":"διεύθυνση σπιτιού","data_type":"/contact/postaladdress/home"},{"label":"διεύθυνση","data_type":"/contact/postaladdress"},{"label":"μήνυμα","data_type":"/message"},{"label":"θέμα","data_type":"/subject"},{"label":"πρώτο όνομα στην πιστωτική κάρτα","data_type":"/financial/creditcard/issuedto/first"},{"label":"τελευταίο όνομα στην πιστωτική κάρτα","data_type":"/financial/creditcard/issuedto/last"},{"label":"εκδότη της πιστωτικής κάρτας","data_type":"/financial/creditcard/issuedto"},{"label":"cvv πιστωτική κάρτα","data_type":"/financial/creditcard/verification"},{"label":"τον αριθμό της πιστωτικής κάρτας μέρος 1","data_type":"/financial/creditcard/number/part1"},{"label":"τον αριθμό της πιστωτικής κάρτας μέρος 2","data_type":"/financial/creditcard/number/part2"},{"label":"τον αριθμό της πιστωτικής κάρτας μέρος 3","data_type":"/financial/creditcard/number/part3"},{"label":"τον αριθμό της πιστωτικής κάρτας μέρος 4","data_type":"/financial/creditcard/number/part4"},{"label":"αριθμός πιστωτικής κάρτας","data_type":"/financial/creditcard/number"},{"label":"Τύπος πιστωτικής κάρτας","data_type":"/financial/creditcard/type"},{"label":"πιστωτική κάρτα μήνα λήξης|κάρτα λήγει (μήνας)","data_type":"/financial/creditcard/expirymonth"},{"label":"έτος λήξης","data_type":"/financial/creditcard/expiryyear"},{"label":"μήνα και έτος","data_type":"/financial/creditcard/expiry2"},{"label":"λήξη","data_type":"/financial/creditcard/expiry"},{"label":"έτος γέννησης","data_type":"/birthdate/birthyear"},{"label":"μήνα γέννησης","data_type":"/birthdate/birthmonth"},{"label":"ημερομηνία γέννησης|ημέρα της γέννησης","data_type":"/birthdate/birthday"},{"label":"ημερομηνία γέννησης|γενέθλια","data_type":"/birthdate"},{"label":"καρφίτσα","data_type":"/pin"},{"label":"γένος","data_type":"/person/gender"},{"label":"συμφωνώ","data_type":"/agreetos"},{"label":"Θυμήσου με","data_type":"/rememberme"}],"password":[{"label":"σύνθημα","data_type":"/password"},{"label":"κωδικό παλιά","data_type":"/passwordold"},{"label":"cvv πιστωτική κάρτα","data_type":"/financial/creditcard/verification"},{"label":"καρφίτσα","data_type":"/pin"}],"select":[{"label":"κωδικό χώρας εργασία","data_type":"/contact/phone/countrycode/work"},{"label":"Κωδικός χώρας καταγωγής","data_type":"/contact/phone/countrycode/home"},{"label":"κωδικός χώρας","data_type":"/contact/phone/countrycode"},{"label":"χώρα αποστολής","data_type":"/contact/country/shipping"},{"label":"χώρα χρέωσης","data_type":"/contact/country/billing"},{"label":"χώρα των επιχειρήσεων","data_type":"/contact/country/business"},{"label":"πατρίδα","data_type":"/contact/country/home"},{"label":"χώρα","data_type":"/contact/country"},{"label":"κρατική ναυτιλιακή","data_type":"/contact/state/shipping"},{"label":"κατάσταση χρέωσης","data_type":"/contact/state/billing"},{"label":"κατάσταση των επιχειρήσεων","data_type":"/contact/state/business"},{"label":"κράτος καταγωγής","data_type":"/contact/state/home"},{"label":"κατάσταση","data_type":"/contact/state"},{"label":"Τύπος πιστωτικής κάρτας","data_type":"/financial/creditcard/type"},{"label":"πιστωτική κάρτα μήνα λήξης|κάρτα λήγει (μήνας)","data_type":"/financial/creditcard/expirymonth"},{"label":"έτος λήξης","data_type":"/financial/creditcard/expiryyear"},{"label":"μήνα και έτος","data_type":"/financial/creditcard/expiry2"},{"label":"λήξη","data_type":"/financial/creditcard/expiry"},{"label":"έτος γέννησης","data_type":"/birthdate/birthyear"},{"label":"μήνα γέννησης","data_type":"/birthdate/birthmonth"},{"label":"ημερομηνία γέννησης|ημέρα της γέννησης","data_type":"/birthdate/birthday"},{"label":"ημερομηνία γέννησης|γενέθλια","data_type":"/birthdate"}]},"cs":{"text":[{"label":"captcha","data_type":"/captcha"},{"label":"vyhledávání","data_type":"/search"},{"label":"náznak","data_type":"/auth/hint"},{"label":"odpověď","data_type":"/auth/answer"},{"label":"otázka|výzva","data_type":"/auth/question"},{"label":"e-mail","data_type":"/contact/email"},{"label":"společnost|jméno společnosti","data_type":"/company/name"},{"label":"přátelský|uživatelské jméno|přihlásit se","data_type":"/nameperson/friendly"},{"label":"předpona","data_type":"/nameperson/prefix"},{"label":"poslední|příjmení","negative":"první","data_type":"/nameperson/last"},{"label":"první","negative":"poslední","data_type":"/nameperson/first"},{"label":"střední|prostřední jméno","negative":"poslední|první","data_type":"/nameperson/middle"},{"label":"celé jméno","data_type":"/nameperson/full"},{"label":"přípona","data_type":"/nameperson/suffix"},{"label":"heslo","data_type":"/password"},{"label":"heslo starý","data_type":"/passwordold"},{"label":"cvrlikání","data_type":"/contact/im/twitter"},{"label":"linkedin|spojeny|spojený-in","data_type":"/contact/web/linkedin"},{"label":"blog","data_type":"/contact/web/blog"},{"label":"cíl|aol instant messenger","data_type":"/contact/IM/aim"},{"label":"icq","data_type":"/contact/IM/icq"},{"label":"msn","data_type":"/contact/IM/msn"},{"label":"brebentit","data_type":"/contact/IM/jabber"},{"label":"skype","data_type":"/contact/IM/skype"},{"label":"yahoo","data_type":"/contact/IM/yahoo"},{"label":"pracovní telefon","data_type":"/contact/phone/extension/work"},{"label":"rozšíření domácí telefon","data_type":"/contact/phone/extension/home"},{"label":"rozšíření","data_type":"/contact/phone/extension"},{"label":"kód oblasti","data_type":"/contact/phone/areacode/work"},{"label":"kód oblasti","data_type":"/contact/phone/areacode"},{"label":"telefon","data_type":"/contact/phone/local/work"},{"label":"telefon","data_type":"/contact/phone/local/home"},{"label":"telefon","data_type":"/contact/phone/local"},{"label":"kód fungovat země","data_type":"/contact/phone/countrycode/work"},{"label":"kód domovské země","data_type":"/contact/phone/countrycode/home"},{"label":"kód země","data_type":"/contact/phone/countrycode"},{"label":"mobilní telefon|buňka","data_type":"/contact/phone/mobile"},{"label":"fax","data_type":"/contact/phone/fax"},{"label":"pracovní telefon|firemní telefon|obchodní linii","data_type":"/contact/phone/work"},{"label":"domácí telefon","data_type":"/contact/phone/home"},{"label":"telefon","data_type":"/contact/phone"},{"label":"lodní poštovní směrovací číslo|odeslání poštovní směrovací číslo","data_type":"/contact/postalcode/shipping"},{"label":"fakturační psČ","data_type":"/contact/postalcode/billing"},{"label":"obchodní poštovní směrovací číslo|práce poštovní směrovací číslo","data_type":"/contact/postalcode/business"},{"label":"home poštovní směrovací číslo","data_type":"/contact/postalcode/home"},{"label":"psČ|poštovní směrovací číslo","data_type":"/contact/postalcode"},{"label":"přeprava město","data_type":"/contact/city/shipping"},{"label":"fakturace město","data_type":"/contact/city/billing"},{"label":"obchodní město","data_type":"/contact/city/business"},{"label":"rodné město","data_type":"/contact/city/home"},{"label":"město","data_type":"/contact/city"},{"label":"lodní země","data_type":"/contact/country/shipping"},{"label":"země fakturace","data_type":"/contact/country/billing"},{"label":"obchod země","data_type":"/contact/country/business"},{"label":"domovské země","data_type":"/contact/country/home"},{"label":"země","data_type":"/contact/country"},{"label":"státní plavební","data_type":"/contact/state/shipping"},{"label":"fakturace stav","data_type":"/contact/state/billing"},{"label":"business state","data_type":"/contact/state/business"},{"label":"domovský stát","data_type":"/contact/state/home"},{"label":"stát","data_type":"/contact/state"},{"label":"poštovné plus 4|2. řádek adresy|nakloněný|souprava","data_type":"/contact/postaladdressAdditional/shipping"},{"label":"fakturace plus 4|fakturace řádek 2|nakloněný|souprava","data_type":"/contact/postaladdressAdditional/billing"},{"label":"business plus 4","data_type":"/contact/postaladdressAdditional/business"},{"label":"home plus 4","data_type":"/contact/postaladdressAdditional/home"},{"label":"plus 4","data_type":"/contact/postaladdressAdditional"},{"label":"doručovací adresa","data_type":"/contact/postaladdress/shipping"},{"label":"fakturační adresa","data_type":"/contact/postaladdress/billing"},{"label":"obchodní adresa","data_type":"/contact/postaladdress/business"},{"label":"domácí adresa","data_type":"/contact/postaladdress/home"},{"label":"adresa","data_type":"/contact/postaladdress"},{"label":"zpráva","data_type":"/message"},{"label":"předmět","data_type":"/subject"},{"label":"křestní jméno na kreditní kartě","data_type":"/financial/creditcard/issuedto/first"},{"label":"příjmení na kreditní kartě","data_type":"/financial/creditcard/issuedto/last"},{"label":"kreditní karty emitenta","data_type":"/financial/creditcard/issuedto"},{"label":"kreditní karta cvv","data_type":"/financial/creditcard/verification"},{"label":"číslo kreditní karty část 1","data_type":"/financial/creditcard/number/part1"},{"label":"číslo kreditní karty část 2","data_type":"/financial/creditcard/number/part2"},{"label":"číslo kreditní karty část 3","data_type":"/financial/creditcard/number/part3"},{"label":"číslo kreditní karty část 4","data_type":"/financial/creditcard/number/part4"},{"label":"číslo kreditní karty","data_type":"/financial/creditcard/number"},{"label":"typ kreditní karty","data_type":"/financial/creditcard/type"},{"label":"měsíc vypršení platnosti kreditní karty|vyprší karty (měsíc)","data_type":"/financial/creditcard/expirymonth"},{"label":"rok vypršení platnosti","data_type":"/financial/creditcard/expiryyear"},{"label":"měsíc a rok","data_type":"/financial/creditcard/expiry2"},{"label":"vypršení","data_type":"/financial/creditcard/expiry"},{"label":"rok narození","data_type":"/birthdate/birthyear"},{"label":"měsíc narození","data_type":"/birthdate/birthmonth"},{"label":"narozeniny|den narození","data_type":"/birthdate/birthday"},{"label":"datum narození|narozeniny","data_type":"/birthdate"},{"label":"kolík","data_type":"/pin"},{"label":"rod","data_type":"/person/gender"},{"label":"souhlasit","data_type":"/agreetos"},{"label":"zapamatuj si mě","data_type":"/rememberme"}],"password":[{"label":"heslo","data_type":"/password"},{"label":"heslo starý","data_type":"/passwordold"},{"label":"kreditní karta cvv","data_type":"/financial/creditcard/verification"},{"label":"kolík","data_type":"/pin"}],"select":[{"label":"kód fungovat země","data_type":"/contact/phone/countrycode/work"},{"label":"kód domovské země","data_type":"/contact/phone/countrycode/home"},{"label":"kód země","data_type":"/contact/phone/countrycode"},{"label":"lodní země","data_type":"/contact/country/shipping"},{"label":"země fakturace","data_type":"/contact/country/billing"},{"label":"obchod země","data_type":"/contact/country/business"},{"label":"domovské země","data_type":"/contact/country/home"},{"label":"země","data_type":"/contact/country"},{"label":"státní plavební","data_type":"/contact/state/shipping"},{"label":"fakturace stav","data_type":"/contact/state/billing"},{"label":"business state","data_type":"/contact/state/business"},{"label":"domovský stát","data_type":"/contact/state/home"},{"label":"stát","data_type":"/contact/state"},{"label":"typ kreditní karty","data_type":"/financial/creditcard/type"},{"label":"měsíc vypršení platnosti kreditní karty|vyprší karty (měsíc)","data_type":"/financial/creditcard/expirymonth"},{"label":"rok vypršení platnosti","data_type":"/financial/creditcard/expiryyear"},{"label":"měsíc a rok","data_type":"/financial/creditcard/expiry2"},{"label":"vypršení","data_type":"/financial/creditcard/expiry"},{"label":"rok narození","data_type":"/birthdate/birthyear"},{"label":"měsíc narození","data_type":"/birthdate/birthmonth"},{"label":"narozeniny|den narození","data_type":"/birthdate/birthday"},{"label":"datum narození|narozeniny","data_type":"/birthdate"}]},"vi":{"text":[{"label":"captcha","data_type":"/captcha"},{"label":"tìm kiếm","data_type":"/search"},{"label":"dấu","data_type":"/auth/hint"},{"label":"câu trả lời","data_type":"/auth/answer"},{"label":"câu hỏi|thử thách","data_type":"/auth/question"},{"label":"e-mail","data_type":"/contact/email"},{"label":"công ty|tên công ty","data_type":"/company/name"},{"label":"thân thiện|tên truy nhập|đăng nhập|người sử dụng","data_type":"/nameperson/friendly"},{"label":"tiếp đầu ngữ","data_type":"/nameperson/prefix"},{"label":"cuối cùng|tên họ","negative":"Đầu tiên","data_type":"/nameperson/last"},{"label":"Đầu tiên","negative":"cuối cùng","data_type":"/nameperson/first"},{"label":"ở giữa|tên đệm","negative":"cuối cùng|Đầu tiên","data_type":"/nameperson/middle"},{"label":"họ và tên","data_type":"/nameperson/full"},{"label":"hậu tố","data_type":"/nameperson/suffix"},{"label":"mật khẩu","data_type":"/password"},{"label":"mật khẩu cũ","data_type":"/passwordold"},{"label":"twitter","data_type":"/contact/im/twitter"},{"label":"linkedin|liên kết trong","data_type":"/contact/web/linkedin"},{"label":"blog","data_type":"/contact/web/blog"},{"label":"mục đích|aol instant messenger","data_type":"/contact/IM/aim"},{"label":"icq","data_type":"/contact/IM/icq"},{"label":"msn","data_type":"/contact/IM/msn"},{"label":"lời nói xàm","data_type":"/contact/IM/jabber"},{"label":"skype","data_type":"/contact/IM/skype"},{"label":"yahoo","data_type":"/contact/IM/yahoo"},{"label":"điện thoại công việc|kinh doanh điện thoại","data_type":"/contact/phone/extension/work"},{"label":"mở rộng điện thoại nhà","data_type":"/contact/phone/extension/home"},{"label":"sự mở rộng","data_type":"/contact/phone/extension"},{"label":"mã vùng","data_type":"/contact/phone/areacode/work"},{"label":"mã vùng","data_type":"/contact/phone/areacode"},{"label":"Điện thoại","data_type":"/contact/phone/local/work"},{"label":"Điện thoại","data_type":"/contact/phone/local/home"},{"label":"Điện thoại","data_type":"/contact/phone/local"},{"label":"mã quốc gia làm việc","data_type":"/contact/phone/countrycode/work"},{"label":"mã quốc gia về nhà","data_type":"/contact/phone/countrycode/home"},{"label":"mã quốc gia","data_type":"/contact/phone/countrycode"},{"label":"điện thoại di động|tế bào","data_type":"/contact/phone/mobile"},{"label":"số fax","data_type":"/contact/phone/fax"},{"label":"điện thoại công việc|kinh doanh điện thoại|điện thoại của công ty|ngành nghề kinh doanh","data_type":"/contact/phone/work"},{"label":"điện thoại nhà","data_type":"/contact/phone/home"},{"label":"điện thoại","data_type":"/contact/phone"},{"label":"zip code vận chuyển|vận chuyển mã bưu chính","data_type":"/contact/postalcode/shipping"},{"label":"thanh toán zip code","data_type":"/contact/postalcode/billing"},{"label":"mã số kinh doanh zip|zip code làm việc|mã bưu điện","data_type":"/contact/postalcode/business"},{"label":"mã nhà zip|mã bưu chính nhà","data_type":"/contact/postalcode/home"},{"label":"mã bưu chính|mã bưu điện","data_type":"/contact/postalcode"},{"label":"thành phố vận chuyển","data_type":"/contact/city/shipping"},{"label":"thành phố thanh toán","data_type":"/contact/city/billing"},{"label":"thành phố kinh doanh","data_type":"/contact/city/business"},{"label":"thành phố quê hương","data_type":"/contact/city/home"},{"label":"thành phố","data_type":"/contact/city"},{"label":"nước vận chuyển","data_type":"/contact/country/shipping"},{"label":"đất nước thanh toán","data_type":"/contact/country/billing"},{"label":"nước kinh doanh","data_type":"/contact/country/business"},{"label":"nươc nha","data_type":"/contact/country/home"},{"label":"quốc gia","data_type":"/contact/country"},{"label":"nhà nước vận chuyển","data_type":"/contact/state/shipping"},{"label":"nhà nước thanh toán","data_type":"/contact/state/billing"},{"label":"nhà nước kinh doanh","data_type":"/contact/state/business"},{"label":"bang nhà","data_type":"/contact/state/home"},{"label":"tiểu bang","data_type":"/contact/state"},{"label":"vận chuyển cộng với 4|Địa chỉ 2|apt|bộ","data_type":"/contact/postaladdressAdditional/shipping"},{"label":"thanh toán cộng với 4|dòng thanh toán 2|apt|bộ","data_type":"/contact/postaladdressAdditional/billing"},{"label":"kinh doanh cộng với 4","data_type":"/contact/postaladdressAdditional/business"},{"label":"nhà cộng với 4","data_type":"/contact/postaladdressAdditional/home"},{"label":"cộng với 4","data_type":"/contact/postaladdressAdditional"},{"label":"Địa chỉ giao hàng","data_type":"/contact/postaladdress/shipping"},{"label":"địa chỉ thanh toán","data_type":"/contact/postaladdress/billing"},{"label":"địa chỉ doanh nghiệp","data_type":"/contact/postaladdress/business"},{"label":"địa chỉ nhà","data_type":"/contact/postaladdress/home"},{"label":"địa chỉ nhà","data_type":"/contact/postaladdress"},{"label":"thông điệp","data_type":"/message"},{"label":"môn học","data_type":"/subject"},{"label":"tên trên thẻ tín dụng","data_type":"/financial/creditcard/issuedto/first"},{"label":"tên trên thẻ tín dụng","data_type":"/financial/creditcard/issuedto/last"},{"label":"tổ chức phát hành thẻ tín dụng","data_type":"/financial/creditcard/issuedto"},{"label":"cvv thẻ tín dụng","data_type":"/financial/creditcard/verification"},{"label":"số thẻ tín dụng part 1","data_type":"/financial/creditcard/number/part1"},{"label":"số thẻ tín dụng part 2","data_type":"/financial/creditcard/number/part2"},{"label":"số thẻ tín dụng part 3","data_type":"/financial/creditcard/number/part3"},{"label":"số thẻ tín dụng part 4","data_type":"/financial/creditcard/number/part4"},{"label":"số thẻ tín dụng","data_type":"/financial/creditcard/number"},{"label":"loại thẻ tín dụng","data_type":"/financial/creditcard/type"},{"label":"thẻ tín dụng hết hạn tháng|thẻ hết hạn (tháng)","data_type":"/financial/creditcard/expirymonth"},{"label":"năm hết hạn","data_type":"/financial/creditcard/expiryyear"},{"label":"tháng, năm","data_type":"/financial/creditcard/expiry2"},{"label":"hết hạn","data_type":"/financial/creditcard/expiry"},{"label":"năm sinh","data_type":"/birthdate/birthyear"},{"label":"tháng sinh","data_type":"/birthdate/birthmonth"},{"label":"sinh nhật|ngày sinh","data_type":"/birthdate/birthday"},{"label":"ngày sinh|sinh nhật","data_type":"/birthdate"},{"label":"ghim","data_type":"/pin"},{"label":"tính","data_type":"/person/gender"},{"label":"đồng ý","data_type":"/agreetos"},{"label":"nhớ tôi","data_type":"/rememberme"}],"password":[{"label":"mật khẩu","data_type":"/password"},{"label":"mật khẩu cũ","data_type":"/passwordold"},{"label":"cvv thẻ tín dụng","data_type":"/financial/creditcard/verification"},{"label":"ghim","data_type":"/pin"}],"select":[{"label":"mã quốc gia làm việc","data_type":"/contact/phone/countrycode/work"},{"label":"mã quốc gia về nhà","data_type":"/contact/phone/countrycode/home"},{"label":"mã quốc gia","data_type":"/contact/phone/countrycode"},{"label":"nước vận chuyển","data_type":"/contact/country/shipping"},{"label":"đất nước thanh toán","data_type":"/contact/country/billing"},{"label":"nước kinh doanh","data_type":"/contact/country/business"},{"label":"nươc nha","data_type":"/contact/country/home"},{"label":"quốc gia","data_type":"/contact/country"},{"label":"nhà nước vận chuyển","data_type":"/contact/state/shipping"},{"label":"nhà nước thanh toán","data_type":"/contact/state/billing"},{"label":"nhà nước kinh doanh","data_type":"/contact/state/business"},{"label":"bang nhà","data_type":"/contact/state/home"},{"label":"tiểu bang","data_type":"/contact/state"},{"label":"loại thẻ tín dụng","data_type":"/financial/creditcard/type"},{"label":"thẻ tín dụng hết hạn tháng|thẻ hết hạn (tháng)","data_type":"/financial/creditcard/expirymonth"},{"label":"năm hết hạn","data_type":"/financial/creditcard/expiryyear"},{"label":"tháng, năm","data_type":"/financial/creditcard/expiry2"},{"label":"hết hạn","data_type":"/financial/creditcard/expiry"},{"label":"năm sinh","data_type":"/birthdate/birthyear"},{"label":"tháng sinh","data_type":"/birthdate/birthmonth"},{"label":"sinh nhật|ngày sinh","data_type":"/birthdate/birthday"},{"label":"ngày sinh|sinh nhật","data_type":"/birthdate"}]},"uk":{"text":[{"label":"captcha","data_type":"/captcha"},{"label":"пошук","data_type":"/search"},{"label":"натяк","data_type":"/auth/hint"},{"label":"відповідь","data_type":"/auth/answer"},{"label":"питання|виклик","data_type":"/auth/question"},{"label":"електронна пошта","data_type":"/contact/email"},{"label":"компанія|Назва компанії","data_type":"/company/name"},{"label":"доброзичливий|ім'я користувача|Ввійти","data_type":"/nameperson/friendly"},{"label":"префікс","data_type":"/nameperson/prefix"},{"label":"останній|прізвище","negative":"перший","data_type":"/nameperson/last"},{"label":"перший","negative":"останній","data_type":"/nameperson/first"},{"label":"середній|батькові","negative":"останній|перший","data_type":"/nameperson/middle"},{"label":"повне ім'я","data_type":"/nameperson/full"},{"label":"суфікс","data_type":"/nameperson/suffix"},{"label":"пароль","data_type":"/password"},{"label":"пароль старий","data_type":"/passwordold"},{"label":"щебетати","data_type":"/contact/im/twitter"},{"label":"linkedin|пов'язані|пов'язані в","data_type":"/contact/web/linkedin"},{"label":"блог","data_type":"/contact/web/blog"},{"label":"мета|aol instant messenger","data_type":"/contact/IM/aim"},{"label":"icq","data_type":"/contact/IM/icq"},{"label":"msn","data_type":"/contact/IM/msn"},{"label":"тріскотня","data_type":"/contact/IM/jabber"},{"label":"скайп","data_type":"/contact/IM/skype"},{"label":"yahoo","data_type":"/contact/IM/yahoo"},{"label":"робочий телефон|бізнес-телефон","data_type":"/contact/phone/extension/work"},{"label":"розширення домашнього телефону","data_type":"/contact/phone/extension/home"},{"label":"розширення","data_type":"/contact/phone/extension"},{"label":"код зони","data_type":"/contact/phone/areacode/work"},{"label":"код зони","data_type":"/contact/phone/areacode"},{"label":"телефон","data_type":"/contact/phone/local/work"},{"label":"телефон","data_type":"/contact/phone/local/home"},{"label":"телефон","data_type":"/contact/phone/local"},{"label":"робота код країни","data_type":"/contact/phone/countrycode/work"},{"label":"код країни походження","data_type":"/contact/phone/countrycode/home"},{"label":"код країни","data_type":"/contact/phone/countrycode"},{"label":"мобільний телефон|клітина","data_type":"/contact/phone/mobile"},{"label":"факс","data_type":"/contact/phone/fax"},{"label":"робочий телефон|бізнес-телефон|корпоративний телефон|бізнес-лінія","data_type":"/contact/phone/work"},{"label":"домашній телефон","data_type":"/contact/phone/home"},{"label":"телефон","data_type":"/contact/phone"},{"label":"відправка поштовий індекс","data_type":"/contact/postalcode/shipping"},{"label":"платіжний код поштового індексу","data_type":"/contact/postalcode/billing"},{"label":"бізнес поштовий індекс|робота поштовий індекс|Поштовий індекс","data_type":"/contact/postalcode/business"},{"label":"домашній поштовий індекс","data_type":"/contact/postalcode/home"},{"label":"ЗІП код|Поштовий індекс","data_type":"/contact/postalcode"},{"label":"відправка місто","data_type":"/contact/city/shipping"},{"label":"білінг місто","data_type":"/contact/city/billing"},{"label":"діловій частині міста","data_type":"/contact/city/business"},{"label":"рідне місто","data_type":"/contact/city/home"},{"label":"місто","data_type":"/contact/city"},{"label":"відправка країна","data_type":"/contact/country/shipping"},{"label":"білінг країна","data_type":"/contact/country/billing"},{"label":"бізнес-країна","data_type":"/contact/country/business"},{"label":"заміський будинок","data_type":"/contact/country/home"},{"label":"країна","data_type":"/contact/country"},{"label":"відправка стан","data_type":"/contact/state/shipping"},{"label":"стан рахунків","data_type":"/contact/state/billing"},{"label":"бізнес-стан","data_type":"/contact/state/business"},{"label":"рідний штат","data_type":"/contact/state/home"},{"label":"стан","data_type":"/contact/state"},{"label":"доставка плюс 4|Адресний рядок 2|схильний|набір","data_type":"/contact/postaladdressAdditional/shipping"},{"label":"білінг плюс 4|білінг лінія 2|схильний|набір","data_type":"/contact/postaladdressAdditional/billing"},{"label":"бізнес плюс 4","data_type":"/contact/postaladdressAdditional/business"},{"label":"додому плюс 4","data_type":"/contact/postaladdressAdditional/home"},{"label":"плюс 4","data_type":"/contact/postaladdressAdditional"},{"label":"адреса доставки","data_type":"/contact/postaladdress/shipping"},{"label":"платіжний адреса","data_type":"/contact/postaladdress/billing"},{"label":"службову адресу","data_type":"/contact/postaladdress/business"},{"label":"Домашня адреса","data_type":"/contact/postaladdress/home"},{"label":"адреса","data_type":"/contact/postaladdress"},{"label":"повідомлення","data_type":"/message"},{"label":"суб'єкт","data_type":"/subject"},{"label":"перше ім'я на кредитній карті","data_type":"/financial/creditcard/issuedto/first"},{"label":"прізвище на кредитній карті","data_type":"/financial/creditcard/issuedto/last"},{"label":"емітент кредитних карток","data_type":"/financial/creditcard/issuedto"},{"label":"кредитної картки cvv","data_type":"/financial/creditcard/verification"},{"label":"номер кредитної картки, частина 1","data_type":"/financial/creditcard/number/part1"},{"label":"номер кредитної картки, частина 2","data_type":"/financial/creditcard/number/part2"},{"label":"номер кредитної картки, частина 3","data_type":"/financial/creditcard/number/part3"},{"label":"номер кредитної картки частину 4","data_type":"/financial/creditcard/number/part4"},{"label":"Номер кредитної карти","data_type":"/financial/creditcard/number"},{"label":"Тип кредитної картки","data_type":"/financial/creditcard/type"},{"label":"закінчення терміну дії кредитної картки місяць|картки закінчується (місяць)","data_type":"/financial/creditcard/expirymonth"},{"label":"рік закінчення терміну дії","data_type":"/financial/creditcard/expiryyear"},{"label":"місяць і рік","data_type":"/financial/creditcard/expiry2"},{"label":"витікання","data_type":"/financial/creditcard/expiry"},{"label":"рік народження","data_type":"/birthdate/birthyear"},{"label":"місяць народження","data_type":"/birthdate/birthmonth"},{"label":"день народження","data_type":"/birthdate/birthday"},{"label":"дата народження|день народження","data_type":"/birthdate"},{"label":"шпилька","data_type":"/pin"},{"label":"Стать","data_type":"/person/gender"},{"label":"згодні","data_type":"/agreetos"},{"label":"Пам'ятай мене","data_type":"/rememberme"}],"password":[{"label":"пароль","data_type":"/password"},{"label":"пароль старий","data_type":"/passwordold"},{"label":"кредитної картки cvv","data_type":"/financial/creditcard/verification"},{"label":"шпилька","data_type":"/pin"}],"select":[{"label":"робота код країни","data_type":"/contact/phone/countrycode/work"},{"label":"код країни походження","data_type":"/contact/phone/countrycode/home"},{"label":"код країни","data_type":"/contact/phone/countrycode"},{"label":"відправка країна","data_type":"/contact/country/shipping"},{"label":"білінг країна","data_type":"/contact/country/billing"},{"label":"бізнес-країна","data_type":"/contact/country/business"},{"label":"заміський будинок","data_type":"/contact/country/home"},{"label":"країна","data_type":"/contact/country"},{"label":"відправка стан","data_type":"/contact/state/shipping"},{"label":"стан рахунків","data_type":"/contact/state/billing"},{"label":"бізнес-стан","data_type":"/contact/state/business"},{"label":"рідний штат","data_type":"/contact/state/home"},{"label":"стан","data_type":"/contact/state"},{"label":"Тип кредитної картки","data_type":"/financial/creditcard/type"},{"label":"закінчення терміну дії кредитної картки місяць|картки закінчується (місяць)","data_type":"/financial/creditcard/expirymonth"},{"label":"рік закінчення терміну дії","data_type":"/financial/creditcard/expiryyear"},{"label":"місяць і рік","data_type":"/financial/creditcard/expiry2"},{"label":"витікання","data_type":"/financial/creditcard/expiry"},{"label":"рік народження","data_type":"/birthdate/birthyear"},{"label":"місяць народження","data_type":"/birthdate/birthmonth"},{"label":"день народження","data_type":"/birthdate/birthday"},{"label":"дата народження|день народження","data_type":"/birthdate"}]},"tr":{"text":[{"label":"captcha","data_type":"/captcha"},{"label":"arama","data_type":"/search"},{"label":"ipucu","data_type":"/auth/hint"},{"label":"cevap","data_type":"/auth/answer"},{"label":"soru|meydan okuma","data_type":"/auth/question"},{"label":"e-posta","data_type":"/contact/email"},{"label":"Şirket|Şirket adı","data_type":"/company/name"},{"label":"arkadaş canlısı|kullanıcı adı|oturum aç","data_type":"/nameperson/friendly"},{"label":"önek","data_type":"/nameperson/prefix"},{"label":"son|soyadı","negative":"ilk","data_type":"/nameperson/last"},{"label":"ilk","negative":"son","data_type":"/nameperson/first"},{"label":"orta|ikinci ad","negative":"son|ilk","data_type":"/nameperson/middle"},{"label":"ad soyad","data_type":"/nameperson/full"},{"label":"son ek","data_type":"/nameperson/suffix"},{"label":"parola","data_type":"/password"},{"label":"eski şifre","data_type":"/passwordold"},{"label":"heyecan","data_type":"/contact/im/twitter"},{"label":"bağlı|bağlantılı","data_type":"/contact/web/linkedin"},{"label":"blog","data_type":"/contact/web/blog"},{"label":"amaç|anlık mesajlaşma sistemi","data_type":"/contact/IM/aim"},{"label":"icq","data_type":"/contact/IM/icq"},{"label":"msn","data_type":"/contact/IM/msn"},{"label":"cıvıl cıvıl","data_type":"/contact/IM/jabber"},{"label":"skype","data_type":"/contact/IM/skype"},{"label":"yahoo","data_type":"/contact/IM/yahoo"},{"label":"iş telefonu","data_type":"/contact/phone/extension/work"},{"label":"evdeki telefon uzantısı","data_type":"/contact/phone/extension/home"},{"label":"uzantı","data_type":"/contact/phone/extension"},{"label":"alan kodu","data_type":"/contact/phone/areacode/work"},{"label":"alan kodu","data_type":"/contact/phone/areacode"},{"label":"telefon","data_type":"/contact/phone/local/work"},{"label":"telefon","data_type":"/contact/phone/local/home"},{"label":"telefon","data_type":"/contact/phone/local"},{"label":"iş ülke kodu","data_type":"/contact/phone/countrycode/work"},{"label":"Ülke ülke kodu","data_type":"/contact/phone/countrycode/home"},{"label":"ülke kodu","data_type":"/contact/phone/countrycode"},{"label":"cep telefonu|hücre","data_type":"/contact/phone/mobile"},{"label":"faks","data_type":"/contact/phone/fax"},{"label":"iş telefonu|kurumsal telefon|iş hattı","data_type":"/contact/phone/work"},{"label":"ev telefonu","data_type":"/contact/phone/home"},{"label":"telefon","data_type":"/contact/phone"},{"label":"posta kodu gönderme|nakliye posta kodu","data_type":"/contact/postalcode/shipping"},{"label":"fatura posta kodu","data_type":"/contact/postalcode/billing"},{"label":"işletme posta kodu|posta kodunu işle","data_type":"/contact/postalcode/business"},{"label":"ev posta kodu","data_type":"/contact/postalcode/home"},{"label":"posta kodu","data_type":"/contact/postalcode"},{"label":"nakliye şehri","data_type":"/contact/city/shipping"},{"label":"faturalandırma şehri","data_type":"/contact/city/billing"},{"label":"iş şehri","data_type":"/contact/city/business"},{"label":"memleket","data_type":"/contact/city/home"},{"label":"Şehir","data_type":"/contact/city"},{"label":"nakliye ülkesi","data_type":"/contact/country/shipping"},{"label":"faturalandırma ülkesi","data_type":"/contact/country/billing"},{"label":"iş ülkesi","data_type":"/contact/country/business"},{"label":"anavatan","data_type":"/contact/country/home"},{"label":"Ülke","data_type":"/contact/country"},{"label":"nakliye durumu","data_type":"/contact/state/shipping"},{"label":"faturalandırma durumu","data_type":"/contact/state/billing"},{"label":"iş durumu","data_type":"/contact/state/business"},{"label":"ev devleti","data_type":"/contact/state/home"},{"label":"belirtmek, bildirmek","data_type":"/contact/state"},{"label":"gönderim artı 4|adres satırı 2|uygun|süit","data_type":"/contact/postaladdressAdditional/shipping"},{"label":"faturalandırma artı 4|faturalandırma satırı 2|uygun|süit","data_type":"/contact/postaladdressAdditional/billing"},{"label":"iş artı 4","data_type":"/contact/postaladdressAdditional/business"},{"label":"ev artı 4","data_type":"/contact/postaladdressAdditional/home"},{"label":"artı 4","data_type":"/contact/postaladdressAdditional"},{"label":"teslimat adresi","data_type":"/contact/postaladdress/shipping"},{"label":"fatura adresi","data_type":"/contact/postaladdress/billing"},{"label":"iş adresi","data_type":"/contact/postaladdress/business"},{"label":"ev adresi","data_type":"/contact/postaladdress/home"},{"label":"adres","data_type":"/contact/postaladdress"},{"label":"mesaj","data_type":"/message"},{"label":"konu","data_type":"/subject"},{"label":"kredi kartı üzerindeki ilk adı","data_type":"/financial/creditcard/issuedto/first"},{"label":"kredi kartındaki soyad","data_type":"/financial/creditcard/issuedto/last"},{"label":"kredi kartı düzenleyicisi","data_type":"/financial/creditcard/issuedto"},{"label":"kredi kartı cvv","data_type":"/financial/creditcard/verification"},{"label":"kredi kartı numarası bölüm 1","data_type":"/financial/creditcard/number/part1"},{"label":"kredi kartı numarası bölüm 2","data_type":"/financial/creditcard/number/part2"},{"label":"kredi kartı numarası bölüm 3","data_type":"/financial/creditcard/number/part3"},{"label":"kredi kartı numarası bölüm 4","data_type":"/financial/creditcard/number/part4"},{"label":"kredi kartı numarası","data_type":"/financial/creditcard/number"},{"label":"kredi kart tipi","data_type":"/financial/creditcard/type"},{"label":"kredi kartı son kullanma ayı|kartın süresi doldu (ay)","data_type":"/financial/creditcard/expirymonth"},{"label":"son kullanma yılı","data_type":"/financial/creditcard/expiryyear"},{"label":"ay ve yıl","data_type":"/financial/creditcard/expiry2"},{"label":"son","data_type":"/financial/creditcard/expiry"},{"label":"doğum yılı","data_type":"/birthdate/birthyear"},{"label":"doğum ayı","data_type":"/birthdate/birthmonth"},{"label":"doğum günü","data_type":"/birthdate/birthday"},{"label":"doğum günü","data_type":"/birthdate"},{"label":"toplu iğne","data_type":"/pin"},{"label":"cinsiyet","data_type":"/person/gender"},{"label":"katılıyorum","data_type":"/agreetos"},{"label":"beni hatırla","data_type":"/rememberme"}],"password":[{"label":"parola","data_type":"/password"},{"label":"eski şifre","data_type":"/passwordold"},{"label":"kredi kartı cvv","data_type":"/financial/creditcard/verification"},{"label":"toplu iğne","data_type":"/pin"}],"select":[{"label":"iş ülke kodu","data_type":"/contact/phone/countrycode/work"},{"label":"Ülke ülke kodu","data_type":"/contact/phone/countrycode/home"},{"label":"ülke kodu","data_type":"/contact/phone/countrycode"},{"label":"nakliye ülkesi","data_type":"/contact/country/shipping"},{"label":"faturalandırma ülkesi","data_type":"/contact/country/billing"},{"label":"iş ülkesi","data_type":"/contact/country/business"},{"label":"anavatan","data_type":"/contact/country/home"},{"label":"Ülke","data_type":"/contact/country"},{"label":"nakliye durumu","data_type":"/contact/state/shipping"},{"label":"faturalandırma durumu","data_type":"/contact/state/billing"},{"label":"iş durumu","data_type":"/contact/state/business"},{"label":"ev devleti","data_type":"/contact/state/home"},{"label":"belirtmek, bildirmek","data_type":"/contact/state"},{"label":"kredi kart tipi","data_type":"/financial/creditcard/type"},{"label":"kredi kartı son kullanma ayı|kartın süresi doldu (ay)","data_type":"/financial/creditcard/expirymonth"},{"label":"son kullanma yılı","data_type":"/financial/creditcard/expiryyear"},{"label":"ay ve yıl","data_type":"/financial/creditcard/expiry2"},{"label":"son","data_type":"/financial/creditcard/expiry"},{"label":"doğum yılı","data_type":"/birthdate/birthyear"},{"label":"doğum ayı","data_type":"/birthdate/birthmonth"},{"label":"doğum günü","data_type":"/birthdate/birthday"},{"label":"doğum günü","data_type":"/birthdate"}]},"da":{"text":[{"label":"captcha","data_type":"/captcha"},{"label":"søge","data_type":"/search"},{"label":"antydning","data_type":"/auth/hint"},{"label":"svar","data_type":"/auth/answer"},{"label":"spørgsmål|udfordring","data_type":"/auth/question"},{"label":"e-mail","data_type":"/contact/email"},{"label":"selskab|firmanavn","data_type":"/company/name"},{"label":"venlige|brugernavn|log på","data_type":"/nameperson/friendly"},{"label":"præfiks","data_type":"/nameperson/prefix"},{"label":"sidste|efternavn","negative":"først","data_type":"/nameperson/last"},{"label":"først","negative":"sidste","data_type":"/nameperson/first"},{"label":"midten|mellemnavn","negative":"sidste|først","data_type":"/nameperson/middle"},{"label":"fulde navn","data_type":"/nameperson/full"},{"label":"endelse","data_type":"/nameperson/suffix"},{"label":"adgangskode","data_type":"/password"},{"label":"adgangskode gamle","data_type":"/passwordold"},{"label":"twitter","data_type":"/contact/im/twitter"},{"label":"linkedin|forbundet i|linked-in","data_type":"/contact/web/linkedin"},{"label":"blog","data_type":"/contact/web/blog"},{"label":"sigte|aol instant messenger","data_type":"/contact/IM/aim"},{"label":"icq","data_type":"/contact/IM/icq"},{"label":"msn","data_type":"/contact/IM/msn"},{"label":"jabber","data_type":"/contact/IM/jabber"},{"label":"skype","data_type":"/contact/IM/skype"},{"label":"yahoo","data_type":"/contact/IM/yahoo"},{"label":"arbejdstelefon|firmatelefon","data_type":"/contact/phone/extension/work"},{"label":"hjemmetelefon udvidelse","data_type":"/contact/phone/extension/home"},{"label":"udvidelse","data_type":"/contact/phone/extension"},{"label":"områdenummer","data_type":"/contact/phone/areacode/work"},{"label":"områdenummer","data_type":"/contact/phone/areacode"},{"label":"telefon","data_type":"/contact/phone/local/work"},{"label":"telefon","data_type":"/contact/phone/local/home"},{"label":"telefon","data_type":"/contact/phone/local"},{"label":"arbejde landekode","data_type":"/contact/phone/countrycode/work"},{"label":"hjem landekode","data_type":"/contact/phone/countrycode/home"},{"label":"landekode","data_type":"/contact/phone/countrycode"},{"label":"mobiltelefon|celle","data_type":"/contact/phone/mobile"},{"label":"fax","data_type":"/contact/phone/fax"},{"label":"arbejdstelefon|firmatelefon|virksomhedernes telefon|forretningsområde","data_type":"/contact/phone/work"},{"label":"hjemmetelefon","data_type":"/contact/phone/home"},{"label":"telefon","data_type":"/contact/phone"},{"label":"forsendelse postnummer","data_type":"/contact/postalcode/shipping"},{"label":"fakturering postnummer","data_type":"/contact/postalcode/billing"},{"label":"business postnummer|arbejde postnummer","data_type":"/contact/postalcode/business"},{"label":"hjem postnummer","data_type":"/contact/postalcode/home"},{"label":"postnummer","data_type":"/contact/postalcode"},{"label":"forsendelse byen","data_type":"/contact/city/shipping"},{"label":"fakturering byen","data_type":"/contact/city/billing"},{"label":"business by","data_type":"/contact/city/business"},{"label":"hjemby","data_type":"/contact/city/home"},{"label":"by","data_type":"/contact/city"},{"label":"skibsfart land","data_type":"/contact/country/shipping"},{"label":"fakturering land","data_type":"/contact/country/billing"},{"label":"business land","data_type":"/contact/country/business"},{"label":"hjemland","data_type":"/contact/country/home"},{"label":"land","data_type":"/contact/country"},{"label":"forsendelse tilstand","data_type":"/contact/state/shipping"},{"label":"fakturering tilstand","data_type":"/contact/state/billing"},{"label":"business tilstand","data_type":"/contact/state/business"},{"label":"hjemstat","data_type":"/contact/state/home"},{"label":"tilstand","data_type":"/contact/state"},{"label":"forsendelse plus 4|adresselinje 2|apt|suite","data_type":"/contact/postaladdressAdditional/shipping"},{"label":"fakturering plus 4|fakturering linje 2|apt|suite","data_type":"/contact/postaladdressAdditional/billing"},{"label":"business plus 4","data_type":"/contact/postaladdressAdditional/business"},{"label":"hjem plus 4","data_type":"/contact/postaladdressAdditional/home"},{"label":"plus 4","data_type":"/contact/postaladdressAdditional"},{"label":"leveringsadresse","data_type":"/contact/postaladdress/shipping"},{"label":"betalingsadresse","data_type":"/contact/postaladdress/billing"},{"label":"forretningsadresse","data_type":"/contact/postaladdress/business"},{"label":"hjemme adresse","data_type":"/contact/postaladdress/home"},{"label":"adresse","data_type":"/contact/postaladdress"},{"label":"besked","data_type":"/message"},{"label":"emne","data_type":"/subject"},{"label":"fornavn på kreditkort","data_type":"/financial/creditcard/issuedto/first"},{"label":"efternavn på kreditkort","data_type":"/financial/creditcard/issuedto/last"},{"label":"kreditkort udstederen","data_type":"/financial/creditcard/issuedto"},{"label":"kreditkort cvv","data_type":"/financial/creditcard/verification"},{"label":"kreditkortnummer del 1","data_type":"/financial/creditcard/number/part1"},{"label":"kreditkortnummer del 2","data_type":"/financial/creditcard/number/part2"},{"label":"kreditkortnummer del 3","data_type":"/financial/creditcard/number/part3"},{"label":"kreditkortnummer del 4","data_type":"/financial/creditcard/number/part4"},{"label":"kreditkortnummer","data_type":"/financial/creditcard/number"},{"label":"kreditkort typen","data_type":"/financial/creditcard/type"},{"label":"udløb kreditkort måned|kort udløber (måned)","data_type":"/financial/creditcard/expirymonth"},{"label":"udløb år","data_type":"/financial/creditcard/expiryyear"},{"label":"måned og år","data_type":"/financial/creditcard/expiry2"},{"label":"udløb","data_type":"/financial/creditcard/expiry"},{"label":"fødselsår|fødsels år","data_type":"/birthdate/birthyear"},{"label":"fødsel måned|fødselsmåned","data_type":"/birthdate/birthmonth"},{"label":"fødsel dag|dagen for fødslen","data_type":"/birthdate/birthday"},{"label":"fødselsdato|fødselsdag","data_type":"/birthdate"},{"label":"pin","data_type":"/pin"},{"label":"køn","data_type":"/person/gender"},{"label":"enig","data_type":"/agreetos"},{"label":"husk mig","data_type":"/rememberme"}],"password":[{"label":"adgangskode","data_type":"/password"},{"label":"adgangskode gamle","data_type":"/passwordold"},{"label":"kreditkort cvv","data_type":"/financial/creditcard/verification"},{"label":"pin","data_type":"/pin"}],"select":[{"label":"arbejde landekode","data_type":"/contact/phone/countrycode/work"},{"label":"hjem landekode","data_type":"/contact/phone/countrycode/home"},{"label":"landekode","data_type":"/contact/phone/countrycode"},{"label":"skibsfart land","data_type":"/contact/country/shipping"},{"label":"fakturering land","data_type":"/contact/country/billing"},{"label":"business land","data_type":"/contact/country/business"},{"label":"hjemland","data_type":"/contact/country/home"},{"label":"land","data_type":"/contact/country"},{"label":"forsendelse tilstand","data_type":"/contact/state/shipping"},{"label":"fakturering tilstand","data_type":"/contact/state/billing"},{"label":"business tilstand","data_type":"/contact/state/business"},{"label":"hjemstat","data_type":"/contact/state/home"},{"label":"tilstand","data_type":"/contact/state"},{"label":"kreditkort typen","data_type":"/financial/creditcard/type"},{"label":"udløb kreditkort måned|kort udløber (måned)","data_type":"/financial/creditcard/expirymonth"},{"label":"udløb år","data_type":"/financial/creditcard/expiryyear"},{"label":"måned og år","data_type":"/financial/creditcard/expiry2"},{"label":"udløb","data_type":"/financial/creditcard/expiry"},{"label":"fødselsår|fødsels år","data_type":"/birthdate/birthyear"},{"label":"fødsel måned|fødselsmåned","data_type":"/birthdate/birthmonth"},{"label":"fødsel dag|dagen for fødslen","data_type":"/birthdate/birthday"},{"label":"fødselsdato|fødselsdag","data_type":"/birthdate"}]},"fi":{"text":[{"label":"captcha","data_type":"/captcha"},{"label":"haku","data_type":"/search"},{"label":"vihje","data_type":"/auth/hint"},{"label":"vastaus","data_type":"/auth/answer"},{"label":"kysymys|haaste","data_type":"/auth/question"},{"label":"email|sähköposti","data_type":"/contact/email"},{"label":"yhtiö|yrityksen nimi","data_type":"/company/name"},{"label":"ystävällinen|käyttäjätunnus|kirjaudu sisään","data_type":"/nameperson/friendly"},{"label":"etuliite","data_type":"/nameperson/prefix"},{"label":"kestää|sukunimi","negative":"ensimmäinen","data_type":"/nameperson/last"},{"label":"ensimmäinen","negative":"kestää","data_type":"/nameperson/first"},{"label":"keskimmäinen|toinen nimi","negative":"kestää|ensimmäinen","data_type":"/nameperson/middle"},{"label":"koko nimi","data_type":"/nameperson/full"},{"label":"jälkiliite","data_type":"/nameperson/suffix"},{"label":"salasana","data_type":"/password"},{"label":"salasana vanha","data_type":"/passwordold"},{"label":"viserrys","data_type":"/contact/im/twitter"},{"label":"linkedin|linkitetty|sidoksissa-in","data_type":"/contact/web/linkedin"},{"label":"blogi","data_type":"/contact/web/blog"},{"label":"tavoite|aol instant messenger","data_type":"/contact/IM/aim"},{"label":"icq","data_type":"/contact/IM/icq"},{"label":"msn","data_type":"/contact/IM/msn"},{"label":"pälpättää","data_type":"/contact/IM/jabber"},{"label":"skype","data_type":"/contact/IM/skype"},{"label":"yahoo","data_type":"/contact/IM/yahoo"},{"label":"työpuhelin","data_type":"/contact/phone/extension/work"},{"label":"kotipuhelin laajennus","data_type":"/contact/phone/extension/home"},{"label":"laajentaminen","data_type":"/contact/phone/extension"},{"label":"aluekoodi","data_type":"/contact/phone/areacode/work"},{"label":"aluekoodi","data_type":"/contact/phone/areacode"},{"label":"puhelin","data_type":"/contact/phone/local/work"},{"label":"puhelin","data_type":"/contact/phone/local/home"},{"label":"puhelin","data_type":"/contact/phone/local"},{"label":"työ maatunnus","data_type":"/contact/phone/countrycode/work"},{"label":"koti maatunnus","data_type":"/contact/phone/countrycode/home"},{"label":"maatunnus","data_type":"/contact/phone/countrycode"},{"label":"kännykkä|solu","data_type":"/contact/phone/mobile"},{"label":"faksi","data_type":"/contact/phone/fax"},{"label":"työpuhelin|yritysten puhelin|liiketoimintalinja","data_type":"/contact/phone/work"},{"label":"kotipuhelin","data_type":"/contact/phone/home"},{"label":"puhelin","data_type":"/contact/phone"},{"label":"toimitus postinumero|merenkulun postinumeron","data_type":"/contact/postalcode/shipping"},{"label":"laskutuksen postinumero","data_type":"/contact/postalcode/billing"},{"label":"liike postinumeron|työ postinumeron","data_type":"/contact/postalcode/business"},{"label":"koti postinumeron","data_type":"/contact/postalcode/home"},{"label":"postinumero","data_type":"/contact/postalcode"},{"label":"merenkulun kaupunki","data_type":"/contact/city/shipping"},{"label":"laskutuskaupunki","data_type":"/contact/city/billing"},{"label":"business-kaupunki","data_type":"/contact/city/business"},{"label":"kotikaupunki","data_type":"/contact/city/home"},{"label":"kaupunki","data_type":"/contact/city"},{"label":"toimitusmaa","data_type":"/contact/country/shipping"},{"label":"laskutusmaa","data_type":"/contact/country/billing"},{"label":"liike maa","data_type":"/contact/country/business"},{"label":"kotimaa","data_type":"/contact/country/home"},{"label":"maa","data_type":"/contact/country"},{"label":"merenkulku tila","data_type":"/contact/state/shipping"},{"label":"laskutusosavaltio","data_type":"/contact/state/billing"},{"label":"liike tila","data_type":"/contact/state/business"},{"label":"kotivaltion","data_type":"/contact/state/home"},{"label":"osavaltio","data_type":"/contact/state"},{"label":"toimitus plus 4|osoiterivi 2|osuva|sviitti","data_type":"/contact/postaladdressAdditional/shipping"},{"label":"laskutus plus 4|laskutus rivi 2|osuva|sviitti","data_type":"/contact/postaladdressAdditional/billing"},{"label":"liike plus 4","data_type":"/contact/postaladdressAdditional/business"},{"label":"koti plus 4","data_type":"/contact/postaladdressAdditional/home"},{"label":"plus 4","data_type":"/contact/postaladdressAdditional"},{"label":"toimitusosoite","data_type":"/contact/postaladdress/shipping"},{"label":"laskutusosoite","data_type":"/contact/postaladdress/billing"},{"label":"työosoite","data_type":"/contact/postaladdress/business"},{"label":"kotiosoite","data_type":"/contact/postaladdress/home"},{"label":"osoite","data_type":"/contact/postaladdress"},{"label":"viesti","data_type":"/message"},{"label":"aihe","data_type":"/subject"},{"label":"etunimi luottokortilla","data_type":"/financial/creditcard/issuedto/first"},{"label":"sukunimi luottokortilla","data_type":"/financial/creditcard/issuedto/last"},{"label":"luottokortin myöntäjä","data_type":"/financial/creditcard/issuedto"},{"label":"luottokortti cvv","data_type":"/financial/creditcard/verification"},{"label":"luottokortin numero osa 1","data_type":"/financial/creditcard/number/part1"},{"label":"luottokortin numero osa 2","data_type":"/financial/creditcard/number/part2"},{"label":"luottokortin numero osa 3","data_type":"/financial/creditcard/number/part3"},{"label":"luottokortin numero osa 4","data_type":"/financial/creditcard/number/part4"},{"label":"luottokortin numero","data_type":"/financial/creditcard/number"},{"label":"luottokortin tyyppi","data_type":"/financial/creditcard/type"},{"label":"luottokortin vanhentumiskuukausi|kortti vanhenee (kk)","data_type":"/financial/creditcard/expirymonth"},{"label":"vanhentumisvuosi","data_type":"/financial/creditcard/expiryyear"},{"label":"kuukausi ja vuosi","data_type":"/financial/creditcard/expiry2"},{"label":"vanheneminen","data_type":"/financial/creditcard/expiry"},{"label":"syntymävuosi","data_type":"/birthdate/birthyear"},{"label":"syntymä kuukausi|syntymäkuukausi","data_type":"/birthdate/birthmonth"},{"label":"syntymäpäivä|syntymästä","data_type":"/birthdate/birthday"},{"label":"syntymäpäivä","data_type":"/birthdate"},{"label":"tappi","data_type":"/pin"},{"label":"sukupuoli","data_type":"/person/gender"},{"label":"olla samaa mieltä","data_type":"/agreetos"},{"label":"muista minut","data_type":"/rememberme"}],"password":[{"label":"salasana","data_type":"/password"},{"label":"salasana vanha","data_type":"/passwordold"},{"label":"luottokortti cvv","data_type":"/financial/creditcard/verification"},{"label":"tappi","data_type":"/pin"}],"select":[{"label":"työ maatunnus","data_type":"/contact/phone/countrycode/work"},{"label":"koti maatunnus","data_type":"/contact/phone/countrycode/home"},{"label":"maatunnus","data_type":"/contact/phone/countrycode"},{"label":"toimitusmaa","data_type":"/contact/country/shipping"},{"label":"laskutusmaa","data_type":"/contact/country/billing"},{"label":"liike maa","data_type":"/contact/country/business"},{"label":"kotimaa","data_type":"/contact/country/home"},{"label":"maa","data_type":"/contact/country"},{"label":"merenkulku tila","data_type":"/contact/state/shipping"},{"label":"laskutusosavaltio","data_type":"/contact/state/billing"},{"label":"liike tila","data_type":"/contact/state/business"},{"label":"kotivaltion","data_type":"/contact/state/home"},{"label":"osavaltio","data_type":"/contact/state"},{"label":"luottokortin tyyppi","data_type":"/financial/creditcard/type"},{"label":"luottokortin vanhentumiskuukausi|kortti vanhenee (kk)","data_type":"/financial/creditcard/expirymonth"},{"label":"vanhentumisvuosi","data_type":"/financial/creditcard/expiryyear"},{"label":"kuukausi ja vuosi","data_type":"/financial/creditcard/expiry2"},{"label":"vanheneminen","data_type":"/financial/creditcard/expiry"},{"label":"syntymävuosi","data_type":"/birthdate/birthyear"},{"label":"syntymä kuukausi|syntymäkuukausi","data_type":"/birthdate/birthmonth"},{"label":"syntymäpäivä|syntymästä","data_type":"/birthdate/birthday"},{"label":"syntymäpäivä","data_type":"/birthdate"}]},"th":{"text":[{"label":"แจ้งลบความคิดเห็น","data_type":"/captcha"},{"label":"ค้นหา","data_type":"/search"},{"label":"เปรย","data_type":"/auth/hint"},{"label":"ตอบ","data_type":"/auth/answer"},{"label":"คำถาม|ท้าทาย","data_type":"/auth/question"},{"label":"อีเมล|e-mail","data_type":"/contact/email"},{"label":"บริษัท|ชื่อ บริษัท","data_type":"/company/name"},{"label":"เป็นมิตร|ชื่อผู้ใช้|เข้าสู่ระบบ|ผู้ใช้งาน","data_type":"/nameperson/friendly"},{"label":"อุปสรรค","data_type":"/nameperson/prefix"},{"label":"สุดท้าย|นามสกุล","negative":"เป็นครั้งแรก","data_type":"/nameperson/last"},{"label":"เป็นครั้งแรก","negative":"สุดท้าย","data_type":"/nameperson/first"},{"label":"กลาง|ชื่อกลาง","negative":"สุดท้าย|เป็นครั้งแรก","data_type":"/nameperson/middle"},{"label":"ชื่อเต็ม","data_type":"/nameperson/full"},{"label":"วิภัตติ","data_type":"/nameperson/suffix"},{"label":"รหัสผ่าน","data_type":"/password"},{"label":"รหัสผ่านเดิม","data_type":"/passwordold"},{"label":"พูดเบาและรวดเร็ว","data_type":"/contact/im/twitter"},{"label":"linkedin|เชื่อมโยงใน","data_type":"/contact/web/linkedin"},{"label":"บล็อก","data_type":"/contact/web/blog"},{"label":"จุดมุ่งหมาย|aol messenger ทันที","data_type":"/contact/IM/aim"},{"label":"ไอซีคิว","data_type":"/contact/IM/icq"},{"label":"msn","data_type":"/contact/IM/msn"},{"label":"ส่งเสียงเจี๊ยวจ๊าว","data_type":"/contact/IM/jabber"},{"label":"skype","data_type":"/contact/IM/skype"},{"label":"yahoo","data_type":"/contact/IM/yahoo"},{"label":"โทรศัพท์ที่ทำงาน|เบอร์โทรสำหรับติดต่อเรื่องธุรกิจ","data_type":"/contact/phone/extension/work"},{"label":"ขยายโทรศัพท์บ้าน","data_type":"/contact/phone/extension/home"},{"label":"ส่วนขยาย","data_type":"/contact/phone/extension"},{"label":"รหัสพื้นที่","data_type":"/contact/phone/areacode/work"},{"label":"รหัสพื้นที่","data_type":"/contact/phone/areacode"},{"label":"โทรศัพท์","data_type":"/contact/phone/local/work"},{"label":"โทรศัพท์","data_type":"/contact/phone/local/home"},{"label":"โทรศัพท์","data_type":"/contact/phone/local"},{"label":"ทำงานรหัสประเทศ","data_type":"/contact/phone/countrycode/work"},{"label":"รหัสประเทศบ้านเกิด","data_type":"/contact/phone/countrycode/home"},{"label":"รหัสประเทศ","data_type":"/contact/phone/countrycode"},{"label":"โทรศัพท์มือถือ|เซลล์","data_type":"/contact/phone/mobile"},{"label":"แฟกซ์","data_type":"/contact/phone/fax"},{"label":"โทรศัพท์ที่ทำงาน|เบอร์โทรสำหรับติดต่อเรื่องธุรกิจ|โทรศัพท์ขององค์กร|สายธุรกิจ","data_type":"/contact/phone/work"},{"label":"โทรศัพท์บ้าน","data_type":"/contact/phone/home"},{"label":"โทรศัพท์","data_type":"/contact/phone"},{"label":"รหัสไปรษณีย์การจัดส่งสินค้า|การจัดส่งรหัสไปรษณีย์","data_type":"/contact/postalcode/shipping"},{"label":"รหัสไปรษณีย์เรียกเก็บเงิน","data_type":"/contact/postalcode/billing"},{"label":"รหัสไปรษณีย์ธุรกิจ|รหัสไปรษณีย์ทำงาน","data_type":"/contact/postalcode/business"},{"label":"รหัสไปรษณีย์บ้าน","data_type":"/contact/postalcode/home"},{"label":"รหัสไปรษณีย์","data_type":"/contact/postalcode"},{"label":"เมืองการจัดส่งสินค้า","data_type":"/contact/city/shipping"},{"label":"เมืองที่เรียกเก็บเงิน","data_type":"/contact/city/billing"},{"label":"เมืองธุรกิจ","data_type":"/contact/city/business"},{"label":"บ้านเกิด","data_type":"/contact/city/home"},{"label":"เมือง","data_type":"/contact/city"},{"label":"การจัดส่งสินค้าในประเทศ","data_type":"/contact/country/shipping"},{"label":"การเรียกเก็บเงินในประเทศ","data_type":"/contact/country/billing"},{"label":"ประเทศธุรกิจ","data_type":"/contact/country/business"},{"label":"ประเทศบ้านเกิด","data_type":"/contact/country/home"},{"label":"ประเทศ","data_type":"/contact/country"},{"label":"รัฐจัดส่ง","data_type":"/contact/state/shipping"},{"label":"รัฐเรียกเก็บเงิน","data_type":"/contact/state/billing"},{"label":"รัฐธุรกิจ","data_type":"/contact/state/business"},{"label":"รัฐบ้าน","data_type":"/contact/state/home"},{"label":"สถานะ","data_type":"/contact/state"},{"label":"การจัดส่งสินค้าบวก 4|บรรทัดที่ 2|ฉลาด|ชุด","data_type":"/contact/postaladdressAdditional/shipping"},{"label":"การเรียกเก็บเงินบวก 4|สายการเรียกเก็บเงิน 2|ฉลาด|ชุด","data_type":"/contact/postaladdressAdditional/billing"},{"label":"ธุรกิจบวก 4","data_type":"/contact/postaladdressAdditional/business"},{"label":"บ้านบวก 4","data_type":"/contact/postaladdressAdditional/home"},{"label":"บวก 4","data_type":"/contact/postaladdressAdditional"},{"label":"ที่อยู่จัดส่ง","data_type":"/contact/postaladdress/shipping"},{"label":"ที่อยู่เรียกเก็บเงิน","data_type":"/contact/postaladdress/billing"},{"label":"ที่อยู่ บริษัท","data_type":"/contact/postaladdress/business"},{"label":"ที่อยู่บ้าน","data_type":"/contact/postaladdress/home"},{"label":"ที่อยู่","data_type":"/contact/postaladdress"},{"label":"ข่าวสาร","data_type":"/message"},{"label":"เรื่อง","data_type":"/subject"},{"label":"ชื่อบนบัตรเครดิต","data_type":"/financial/creditcard/issuedto/first"},{"label":"นามสกุลในบัตรเครดิต","data_type":"/financial/creditcard/issuedto/last"},{"label":"ผู้ออกบัตรเครดิต","data_type":"/financial/creditcard/issuedto"},{"label":"cvv บัตรเครดิต","data_type":"/financial/creditcard/verification"},{"label":"หมายเลขบัตรเครดิตส่วน 1","data_type":"/financial/creditcard/number/part1"},{"label":"หมายเลขบัตรเครดิตส่วน 2","data_type":"/financial/creditcard/number/part2"},{"label":"หมายเลขบัตรเครดิตส่วน 3","data_type":"/financial/creditcard/number/part3"},{"label":"หมายเลขบัตรเครดิตส่วน 4","data_type":"/financial/creditcard/number/part4"},{"label":"หมายเลขบัตรเครดิต","data_type":"/financial/creditcard/number"},{"label":"ประเภทบัตรเครดิต","data_type":"/financial/creditcard/type"},{"label":"เดือนหมดอายุของบัตรเครดิต|บัตรหมดอายุ (เดือน)","data_type":"/financial/creditcard/expirymonth"},{"label":"ปีที่หมดอายุ","data_type":"/financial/creditcard/expiryyear"},{"label":"เดือนและปี","data_type":"/financial/creditcard/expiry2"},{"label":"การหมดอายุ","data_type":"/financial/creditcard/expiry"},{"label":"ปีเกิด","data_type":"/birthdate/birthyear"},{"label":"เดือนเกิด","data_type":"/birthdate/birthmonth"},{"label":"วันเดือนปีเกิด|วันเกิด","data_type":"/birthdate/birthday"},{"label":"วันที่เกิด|วันเกิด","data_type":"/birthdate"},{"label":"หมุด","data_type":"/pin"},{"label":"เพศ","data_type":"/person/gender"},{"label":"ตกลง","data_type":"/agreetos"},{"label":"จดจำฉัน","data_type":"/rememberme"}],"password":[{"label":"รหัสผ่าน","data_type":"/password"},{"label":"รหัสผ่านเดิม","data_type":"/passwordold"},{"label":"cvv บัตรเครดิต","data_type":"/financial/creditcard/verification"},{"label":"หมุด","data_type":"/pin"}],"select":[{"label":"ทำงานรหัสประเทศ","data_type":"/contact/phone/countrycode/work"},{"label":"รหัสประเทศบ้านเกิด","data_type":"/contact/phone/countrycode/home"},{"label":"รหัสประเทศ","data_type":"/contact/phone/countrycode"},{"label":"การจัดส่งสินค้าในประเทศ","data_type":"/contact/country/shipping"},{"label":"การเรียกเก็บเงินในประเทศ","data_type":"/contact/country/billing"},{"label":"ประเทศธุรกิจ","data_type":"/contact/country/business"},{"label":"ประเทศบ้านเกิด","data_type":"/contact/country/home"},{"label":"ประเทศ","data_type":"/contact/country"},{"label":"รัฐจัดส่ง","data_type":"/contact/state/shipping"},{"label":"รัฐเรียกเก็บเงิน","data_type":"/contact/state/billing"},{"label":"รัฐธุรกิจ","data_type":"/contact/state/business"},{"label":"รัฐบ้าน","data_type":"/contact/state/home"},{"label":"สถานะ","data_type":"/contact/state"},{"label":"ประเภทบัตรเครดิต","data_type":"/financial/creditcard/type"},{"label":"เดือนหมดอายุของบัตรเครดิต|บัตรหมดอายุ (เดือน)","data_type":"/financial/creditcard/expirymonth"},{"label":"ปีที่หมดอายุ","data_type":"/financial/creditcard/expiryyear"},{"label":"เดือนและปี","data_type":"/financial/creditcard/expiry2"},{"label":"การหมดอายุ","data_type":"/financial/creditcard/expiry"},{"label":"ปีเกิด","data_type":"/birthdate/birthyear"},{"label":"เดือนเกิด","data_type":"/birthdate/birthmonth"},{"label":"วันเดือนปีเกิด|วันเกิด","data_type":"/birthdate/birthday"},{"label":"วันที่เกิด|วันเกิด","data_type":"/birthdate"}]},"hu":{"text":[{"label":"captcha","data_type":"/captcha"},{"label":"keresés","data_type":"/search"},{"label":"célzás","data_type":"/auth/hint"},{"label":"válasz","data_type":"/auth/answer"},{"label":"kérdés|kihívás","data_type":"/auth/question"},{"label":"email","data_type":"/contact/email"},{"label":"vállalat|cégnév","data_type":"/company/name"},{"label":"barátságos|felhasználónév|belépek","data_type":"/nameperson/friendly"},{"label":"előtag","data_type":"/nameperson/prefix"},{"label":"utolsó|vezetéknév","negative":"első","data_type":"/nameperson/last"},{"label":"első","negative":"utolsó","data_type":"/nameperson/first"},{"label":"középső|középső név","negative":"utolsó|első","data_type":"/nameperson/middle"},{"label":"teljes név","data_type":"/nameperson/full"},{"label":"képző","data_type":"/nameperson/suffix"},{"label":"jelszó","data_type":"/password"},{"label":"jelszó a régi","data_type":"/passwordold"},{"label":"twitter","data_type":"/contact/im/twitter"},{"label":"linkedin|összekapcsolódik|linked-in","data_type":"/contact/web/linkedin"},{"label":"blog","data_type":"/contact/web/blog"},{"label":"cél|aol instant messenger","data_type":"/contact/IM/aim"},{"label":"icq","data_type":"/contact/IM/icq"},{"label":"msn","data_type":"/contact/IM/msn"},{"label":"fecseg","data_type":"/contact/IM/jabber"},{"label":"skype","data_type":"/contact/IM/skype"},{"label":"jehu","data_type":"/contact/IM/yahoo"},{"label":"munkahelyi telefon|céges telefon","data_type":"/contact/phone/extension/work"},{"label":"otthoni telefon mellék","data_type":"/contact/phone/extension/home"},{"label":"kiterjesztés","data_type":"/contact/phone/extension"},{"label":"körzetszám","data_type":"/contact/phone/areacode/work"},{"label":"körzetszám","data_type":"/contact/phone/areacode"},{"label":"telefon","data_type":"/contact/phone/local/work"},{"label":"telefon","data_type":"/contact/phone/local/home"},{"label":"telefon","data_type":"/contact/phone/local"},{"label":"munka országkód","data_type":"/contact/phone/countrycode/work"},{"label":"otthon országkód","data_type":"/contact/phone/countrycode/home"},{"label":"ország kód","data_type":"/contact/phone/countrycode"},{"label":"mobiltelefon|sejt","data_type":"/contact/phone/mobile"},{"label":"fax","data_type":"/contact/phone/fax"},{"label":"munkahelyi telefon|céges telefon|vállalati telefon|Üzleti vonal","data_type":"/contact/phone/work"},{"label":"otthoni telefon","data_type":"/contact/phone/home"},{"label":"telefon","data_type":"/contact/phone"},{"label":"hajózás irányítószám|hajózás irányítószámot","data_type":"/contact/postalcode/shipping"},{"label":"számlázási irányítószámot","data_type":"/contact/postalcode/billing"},{"label":"üzleti irányítószám|munka irányítószám","data_type":"/contact/postalcode/business"},{"label":"otthon irányítószám|otthon irányítószámot","data_type":"/contact/postalcode/home"},{"label":"irányítószám","data_type":"/contact/postalcode"},{"label":"hajózás város","data_type":"/contact/city/shipping"},{"label":"számlázási város","data_type":"/contact/city/billing"},{"label":"üzleti város","data_type":"/contact/city/business"},{"label":"szülőváros","data_type":"/contact/city/home"},{"label":"város","data_type":"/contact/city"},{"label":"szállítási ország","data_type":"/contact/country/shipping"},{"label":"számlázási ország","data_type":"/contact/country/billing"},{"label":"üzleti ország","data_type":"/contact/country/business"},{"label":"hazájában","data_type":"/contact/country/home"},{"label":"ország","data_type":"/contact/country"},{"label":"szállítási állapotban","data_type":"/contact/state/shipping"},{"label":"számlázási állam","data_type":"/contact/state/billing"},{"label":"üzleti állam","data_type":"/contact/state/business"},{"label":"székhely szerinti állam","data_type":"/contact/state/home"},{"label":"állami","data_type":"/contact/state"},{"label":"szállítás plusz 4|cím sor 2|hajlamos|lakosztály","data_type":"/contact/postaladdressAdditional/shipping"},{"label":"számlázási plusz 4|számlázás 2. sor|hajlamos|lakosztály","data_type":"/contact/postaladdressAdditional/billing"},{"label":"business plus 4","data_type":"/contact/postaladdressAdditional/business"},{"label":"otthoni plusz 4","data_type":"/contact/postaladdressAdditional/home"},{"label":"plusz 4","data_type":"/contact/postaladdressAdditional"},{"label":"szállítási cím","data_type":"/contact/postaladdress/shipping"},{"label":"számlázási cím","data_type":"/contact/postaladdress/billing"},{"label":"üzleti cím","data_type":"/contact/postaladdress/business"},{"label":"lakcím","data_type":"/contact/postaladdress/home"},{"label":"cím","data_type":"/contact/postaladdress"},{"label":"üzenet","data_type":"/message"},{"label":"tantárgy","data_type":"/subject"},{"label":"utónév hitelkártya","data_type":"/financial/creditcard/issuedto/first"},{"label":"vezetéknevét hitelkártya","data_type":"/financial/creditcard/issuedto/last"},{"label":"hitelkártya kibocsátója","data_type":"/financial/creditcard/issuedto"},{"label":"hitelkártya cvv","data_type":"/financial/creditcard/verification"},{"label":"hitelkártya száma, 1. rész","data_type":"/financial/creditcard/number/part1"},{"label":"hitelkártya száma, 2. rész","data_type":"/financial/creditcard/number/part2"},{"label":"hitelkártya száma 3. rész","data_type":"/financial/creditcard/number/part3"},{"label":"hitelkártya száma, 4. rész","data_type":"/financial/creditcard/number/part4"},{"label":"bankkártya száma","data_type":"/financial/creditcard/number"},{"label":"hitelkártya típus","data_type":"/financial/creditcard/type"},{"label":"hitelkártya lejárati hónap|kártya lejár (hónap)","data_type":"/financial/creditcard/expirymonth"},{"label":"lejáratának éve","data_type":"/financial/creditcard/expiryyear"},{"label":"hónap, év","data_type":"/financial/creditcard/expiry2"},{"label":"lejárat","data_type":"/financial/creditcard/expiry"},{"label":"születési év","data_type":"/birthdate/birthyear"},{"label":"születési hónap","data_type":"/birthdate/birthmonth"},{"label":"születési idő|születési nap","data_type":"/birthdate/birthday"},{"label":"születési dátum|születésnap","data_type":"/birthdate"},{"label":"tű","data_type":"/pin"},{"label":"nem","data_type":"/person/gender"},{"label":"egyetért","data_type":"/agreetos"},{"label":"emlékezz rám","data_type":"/rememberme"}],"password":[{"label":"jelszó","data_type":"/password"},{"label":"jelszó a régi","data_type":"/passwordold"},{"label":"hitelkártya cvv","data_type":"/financial/creditcard/verification"},{"label":"tű","data_type":"/pin"}],"select":[{"label":"munka országkód","data_type":"/contact/phone/countrycode/work"},{"label":"otthon országkód","data_type":"/contact/phone/countrycode/home"},{"label":"ország kód","data_type":"/contact/phone/countrycode"},{"label":"szállítási ország","data_type":"/contact/country/shipping"},{"label":"számlázási ország","data_type":"/contact/country/billing"},{"label":"üzleti ország","data_type":"/contact/country/business"},{"label":"hazájában","data_type":"/contact/country/home"},{"label":"ország","data_type":"/contact/country"},{"label":"szállítási állapotban","data_type":"/contact/state/shipping"},{"label":"számlázási állam","data_type":"/contact/state/billing"},{"label":"üzleti állam","data_type":"/contact/state/business"},{"label":"székhely szerinti állam","data_type":"/contact/state/home"},{"label":"állami","data_type":"/contact/state"},{"label":"hitelkártya típus","data_type":"/financial/creditcard/type"},{"label":"hitelkártya lejárati hónap|kártya lejár (hónap)","data_type":"/financial/creditcard/expirymonth"},{"label":"lejáratának éve","data_type":"/financial/creditcard/expiryyear"},{"label":"hónap, év","data_type":"/financial/creditcard/expiry2"},{"label":"lejárat","data_type":"/financial/creditcard/expiry"},{"label":"születési év","data_type":"/birthdate/birthyear"},{"label":"születési hónap","data_type":"/birthdate/birthmonth"},{"label":"születési idő|születési nap","data_type":"/birthdate/birthday"},{"label":"születési dátum|születésnap","data_type":"/birthdate"}]},"hi":{"text":[{"label":"कैप्चा","data_type":"/captcha"},{"label":"खोज","data_type":"/search"},{"label":"संकेत","data_type":"/auth/hint"},{"label":"उत्तर","data_type":"/auth/answer"},{"label":"सवाल|चुनौती","data_type":"/auth/question"},{"label":"ईमेल","data_type":"/contact/email"},{"label":"कंपनी|कंपनी का नाम","data_type":"/company/name"},{"label":"अनुकूल|उपयोगकर्ता नाम|लॉग इन करें","data_type":"/nameperson/friendly"},{"label":"उपसर्ग","data_type":"/nameperson/prefix"},{"label":"पिछली बार|अंतिम नाम","negative":"प्रथम","data_type":"/nameperson/last"},{"label":"प्रथम","negative":"पिछली बार","data_type":"/nameperson/first"},{"label":"मध्य|मध्य नाम","negative":"पिछली बार|प्रथम","data_type":"/nameperson/middle"},{"label":"पूरा नाम","data_type":"/nameperson/full"},{"label":"प्रत्यय","data_type":"/nameperson/suffix"},{"label":"पासवर्ड","data_type":"/password"},{"label":"पासवर्ड वर्ष","data_type":"/passwordold"},{"label":"ट्विटर","data_type":"/contact/im/twitter"},{"label":"लिंक्डइन|में जुड़े हुए|लिंक्ड-इन","data_type":"/contact/web/linkedin"},{"label":"ब्लॉग","data_type":"/contact/web/blog"},{"label":"लक्ष्य|aol इन्स्टैंट मैसेन्जर","data_type":"/contact/IM/aim"},{"label":"आईसीक्यू","data_type":"/contact/IM/icq"},{"label":"एमएसएन","data_type":"/contact/IM/msn"},{"label":"गपशप","data_type":"/contact/IM/jabber"},{"label":"स्काइप","data_type":"/contact/IM/skype"},{"label":"याहू","data_type":"/contact/IM/yahoo"},{"label":"कार्य फ़ोन|व्यवसायिक दूरभाष","data_type":"/contact/phone/extension/work"},{"label":"घर फोन विस्तार","data_type":"/contact/phone/extension/home"},{"label":"विस्तार","data_type":"/contact/phone/extension"},{"label":"एरिया कोड","data_type":"/contact/phone/areacode/work"},{"label":"एरिया कोड","data_type":"/contact/phone/areacode"},{"label":"टेलीफोन","data_type":"/contact/phone/local/work"},{"label":"टेलीफोन","data_type":"/contact/phone/local/home"},{"label":"टेलीफोन","data_type":"/contact/phone/local"},{"label":"काम देश कोड","data_type":"/contact/phone/countrycode/work"},{"label":"घर देश कोड","data_type":"/contact/phone/countrycode/home"},{"label":"देश कोड","data_type":"/contact/phone/countrycode"},{"label":"मोबाइल फोन|सेल","data_type":"/contact/phone/mobile"},{"label":"फैक्स","data_type":"/contact/phone/fax"},{"label":"कार्य फ़ोन|व्यवसायिक दूरभाष|कॉर्पोरेट फोन|व्यपार","data_type":"/contact/phone/work"},{"label":"घर का फ़ोन","data_type":"/contact/phone/home"},{"label":"फ़ोन","data_type":"/contact/phone"},{"label":"शिपिंग ज़िप कोड|शिपिंग डाक कोड","data_type":"/contact/postalcode/shipping"},{"label":"बिलिंग ज़िप कोड","data_type":"/contact/postalcode/billing"},{"label":"व्यापार ज़िप कोड|काम ज़िप कोड|डाक कोड","data_type":"/contact/postalcode/business"},{"label":"घर ज़िप कोड|घर डाक कोड","data_type":"/contact/postalcode/home"},{"label":"पिन कोड|डाक कोड","data_type":"/contact/postalcode"},{"label":"शिपिंग शहर","data_type":"/contact/city/shipping"},{"label":"बिलिंग शहर","data_type":"/contact/city/billing"},{"label":"व्यावसायिक शहर","data_type":"/contact/city/business"},{"label":"गृह शहर","data_type":"/contact/city/home"},{"label":"शहर","data_type":"/contact/city"},{"label":"शिपिंग देश","data_type":"/contact/country/shipping"},{"label":"बिलिंग देश","data_type":"/contact/country/billing"},{"label":"व्यापार देश","data_type":"/contact/country/business"},{"label":"स्वदेश","data_type":"/contact/country/home"},{"label":"देश","data_type":"/contact/country"},{"label":"शिपिंग राज्य","data_type":"/contact/state/shipping"},{"label":"बिलिंग राज्य","data_type":"/contact/state/billing"},{"label":"व्यापार राज्य","data_type":"/contact/state/business"},{"label":"गृह राज्य","data_type":"/contact/state/home"},{"label":"राज्य","data_type":"/contact/state"},{"label":"शिपिंग प्लस 4|पता पंक्ति नं। 2|उपयुक्त|सुइट","data_type":"/contact/postaladdressAdditional/shipping"},{"label":"बिलिंग प्लस 4|बिलिंग लाइन 2|उपयुक्त|सुइट","data_type":"/contact/postaladdressAdditional/billing"},{"label":"बिजनेस प्लस 4","data_type":"/contact/postaladdressAdditional/business"},{"label":"घर प्लस 4","data_type":"/contact/postaladdressAdditional/home"},{"label":"प्लस 4","data_type":"/contact/postaladdressAdditional"},{"label":"शिपिंग पता","data_type":"/contact/postaladdress/shipping"},{"label":"बिल भेजने का पता","data_type":"/contact/postaladdress/billing"},{"label":"व्यावसायिक पता","data_type":"/contact/postaladdress/business"},{"label":"घर का पता","data_type":"/contact/postaladdress/home"},{"label":"पता","data_type":"/contact/postaladdress"},{"label":"संदेश","data_type":"/message"},{"label":"विषय","data_type":"/subject"},{"label":"क्रेडिट कार्ड पर पहला नाम","data_type":"/financial/creditcard/issuedto/first"},{"label":"क्रेडिट कार्ड पर आखिरी नाम","data_type":"/financial/creditcard/issuedto/last"},{"label":"क्रेडिट कार्ड जारीकर्ता","data_type":"/financial/creditcard/issuedto"},{"label":"क्रेडिट कार्ड के सीवीवी","data_type":"/financial/creditcard/verification"},{"label":"क्रेडिट कार्ड नंबर भाग 1","data_type":"/financial/creditcard/number/part1"},{"label":"क्रेडिट कार्ड नंबर भाग 2","data_type":"/financial/creditcard/number/part2"},{"label":"क्रेडिट कार्ड नंबर भाग 3","data_type":"/financial/creditcard/number/part3"},{"label":"क्रेडिट कार्ड नंबर भाग 4","data_type":"/financial/creditcard/number/part4"},{"label":"क्रेडिट कार्ड नंबर","data_type":"/financial/creditcard/number"},{"label":"क्रेडिट कार्ड के प्रकार","data_type":"/financial/creditcard/type"},{"label":"क्रेडिट कार्ड की समाप्ति महीने|कार्ड समाप्त हो रहा है (महीना)","data_type":"/financial/creditcard/expirymonth"},{"label":"समाप्ति वर्ष","data_type":"/financial/creditcard/expiryyear"},{"label":"महीने और साल","data_type":"/financial/creditcard/expiry2"},{"label":"समाप्ति","data_type":"/financial/creditcard/expiry"},{"label":"जन्म वर्ष|जन्म का साल","data_type":"/birthdate/birthyear"},{"label":"जन्म महीना|जन्म का माह","data_type":"/birthdate/birthmonth"},{"label":"जन्म दिन|जन्म का दिन","data_type":"/birthdate/birthday"},{"label":"जन्म दिन|जन्मदिन","data_type":"/birthdate"},{"label":"पिन","data_type":"/pin"},{"label":"लिंग","data_type":"/person/gender"},{"label":"इस बात से सहमत","data_type":"/agreetos"},{"label":"मुझे याद रखना","data_type":"/rememberme"}],"password":[{"label":"पासवर्ड","data_type":"/password"},{"label":"पासवर्ड वर्ष","data_type":"/passwordold"},{"label":"क्रेडिट कार्ड के सीवीवी","data_type":"/financial/creditcard/verification"},{"label":"पिन","data_type":"/pin"}],"select":[{"label":"काम देश कोड","data_type":"/contact/phone/countrycode/work"},{"label":"घर देश कोड","data_type":"/contact/phone/countrycode/home"},{"label":"देश कोड","data_type":"/contact/phone/countrycode"},{"label":"शिपिंग देश","data_type":"/contact/country/shipping"},{"label":"बिलिंग देश","data_type":"/contact/country/billing"},{"label":"व्यापार देश","data_type":"/contact/country/business"},{"label":"स्वदेश","data_type":"/contact/country/home"},{"label":"देश","data_type":"/contact/country"},{"label":"शिपिंग राज्य","data_type":"/contact/state/shipping"},{"label":"बिलिंग राज्य","data_type":"/contact/state/billing"},{"label":"व्यापार राज्य","data_type":"/contact/state/business"},{"label":"गृह राज्य","data_type":"/contact/state/home"},{"label":"राज्य","data_type":"/contact/state"},{"label":"क्रेडिट कार्ड के प्रकार","data_type":"/financial/creditcard/type"},{"label":"क्रेडिट कार्ड की समाप्ति महीने|कार्ड समाप्त हो रहा है (महीना)","data_type":"/financial/creditcard/expirymonth"},{"label":"समाप्ति वर्ष","data_type":"/financial/creditcard/expiryyear"},{"label":"महीने और साल","data_type":"/financial/creditcard/expiry2"},{"label":"समाप्ति","data_type":"/financial/creditcard/expiry"},{"label":"जन्म वर्ष|जन्म का साल","data_type":"/birthdate/birthyear"},{"label":"जन्म महीना|जन्म का माह","data_type":"/birthdate/birthmonth"},{"label":"जन्म दिन|जन्म का दिन","data_type":"/birthdate/birthday"},{"label":"जन्म दिन|जन्मदिन","data_type":"/birthdate"}]},"id":{"text":[{"label":"captcha","data_type":"/captcha"},{"label":"pencarian","data_type":"/search"},{"label":"petunjuk","data_type":"/auth/hint"},{"label":"menjawab","data_type":"/auth/answer"},{"label":"pertanyaan|tantangan","data_type":"/auth/question"},{"label":"e-mail","data_type":"/contact/email"},{"label":"perusahaan|nama perusahaan","data_type":"/company/name"},{"label":"ramah|nama pengguna|masuk|pemakai","data_type":"/nameperson/friendly"},{"label":"awalan","data_type":"/nameperson/prefix"},{"label":"terakhir|nama keluarga","negative":"pertama","data_type":"/nameperson/last"},{"label":"pertama","negative":"terakhir","data_type":"/nameperson/first"},{"label":"tengah|nama tengah","negative":"terakhir|pertama","data_type":"/nameperson/middle"},{"label":"nama lengkap","data_type":"/nameperson/full"},{"label":"akhiran","data_type":"/nameperson/suffix"},{"label":"kata sandi","data_type":"/password"},{"label":"password lama","data_type":"/passwordold"},{"label":"kericau","data_type":"/contact/im/twitter"},{"label":"linkedin|terkait dalam|terkait-in","data_type":"/contact/web/linkedin"},{"label":"blog","data_type":"/contact/web/blog"},{"label":"tujuan|aol instant messenger","data_type":"/contact/IM/aim"},{"label":"icq","data_type":"/contact/IM/icq"},{"label":"msn","data_type":"/contact/IM/msn"},{"label":"mengoceh","data_type":"/contact/IM/jabber"},{"label":"skype","data_type":"/contact/IM/skype"},{"label":"yahoo","data_type":"/contact/IM/yahoo"},{"label":"telepon kantor|telepon bisnis","data_type":"/contact/phone/extension/work"},{"label":"ekstensi telepon rumah","data_type":"/contact/phone/extension/home"},{"label":"perpanjangan","data_type":"/contact/phone/extension"},{"label":"kode area","data_type":"/contact/phone/areacode/work"},{"label":"kode area","data_type":"/contact/phone/areacode"},{"label":"telepon","data_type":"/contact/phone/local/work"},{"label":"telepon","data_type":"/contact/phone/local/home"},{"label":"telepon","data_type":"/contact/phone/local"},{"label":"kode negara kerja","data_type":"/contact/phone/countrycode/work"},{"label":"kode negara asal","data_type":"/contact/phone/countrycode/home"},{"label":"kode negara","data_type":"/contact/phone/countrycode"},{"label":"telepon genggam|sel","data_type":"/contact/phone/mobile"},{"label":"fax","data_type":"/contact/phone/fax"},{"label":"telepon kantor|telepon bisnis|telepon perusahaan|lini bisnis","data_type":"/contact/phone/work"},{"label":"telepon rumah","data_type":"/contact/phone/home"},{"label":"telepon","data_type":"/contact/phone"},{"label":"kode pos pengiriman|pengiriman kode pos","data_type":"/contact/postalcode/shipping"},{"label":"penagihan kode pos","data_type":"/contact/postalcode/billing"},{"label":"kode bisnis pos|kode pos kerja","data_type":"/contact/postalcode/business"},{"label":"kode rumah pos|kode pos rumah","data_type":"/contact/postalcode/home"},{"label":"kode pos","data_type":"/contact/postalcode"},{"label":"kota pengiriman","data_type":"/contact/city/shipping"},{"label":"kota penagihan","data_type":"/contact/city/billing"},{"label":"kota bisnis","data_type":"/contact/city/business"},{"label":"kota asal","data_type":"/contact/city/home"},{"label":"kota","data_type":"/contact/city"},{"label":"negara pengiriman","data_type":"/contact/country/shipping"},{"label":"negara penagihan","data_type":"/contact/country/billing"},{"label":"negara bisnis","data_type":"/contact/country/business"},{"label":"tanah air","data_type":"/contact/country/home"},{"label":"negara","data_type":"/contact/country"},{"label":"negara pengiriman","data_type":"/contact/state/shipping"},{"label":"negara penagihan","data_type":"/contact/state/billing"},{"label":"negara bisnis","data_type":"/contact/state/business"},{"label":"keadaan rumah","data_type":"/contact/state/home"},{"label":"negara","data_type":"/contact/state"},{"label":"pengiriman ditambah 4|alamat baris 2|tepat|rangkaian","data_type":"/contact/postaladdressAdditional/shipping"},{"label":"penagihan ditambah 4|baris penagihan 2|tepat|rangkaian","data_type":"/contact/postaladdressAdditional/billing"},{"label":"bisnis ditambah 4","data_type":"/contact/postaladdressAdditional/business"},{"label":"rumah ditambah 4","data_type":"/contact/postaladdressAdditional/home"},{"label":"ditambah 4","data_type":"/contact/postaladdressAdditional"},{"label":"alamat pengiriman","data_type":"/contact/postaladdress/shipping"},{"label":"alamat tagihan","data_type":"/contact/postaladdress/billing"},{"label":"alamat bisnis","data_type":"/contact/postaladdress/business"},{"label":"alamat rumah","data_type":"/contact/postaladdress/home"},{"label":"alamat","data_type":"/contact/postaladdress"},{"label":"pesan","data_type":"/message"},{"label":"subyek","data_type":"/subject"},{"label":"nama pertama pada kartu kredit","data_type":"/financial/creditcard/issuedto/first"},{"label":"nama terakhir pada kartu kredit","data_type":"/financial/creditcard/issuedto/last"},{"label":"penerbit kartu kredit","data_type":"/financial/creditcard/issuedto"},{"label":"cvv kartu kredit","data_type":"/financial/creditcard/verification"},{"label":"nomor kartu kredit bagian 1","data_type":"/financial/creditcard/number/part1"},{"label":"nomor kartu kredit bagian 2","data_type":"/financial/creditcard/number/part2"},{"label":"nomor kartu kredit bagian 3","data_type":"/financial/creditcard/number/part3"},{"label":"nomor kartu kredit bagian 4","data_type":"/financial/creditcard/number/part4"},{"label":"nomor kartu kredit","data_type":"/financial/creditcard/number"},{"label":"tipe kartu kredit","data_type":"/financial/creditcard/type"},{"label":"kartu kredit kedaluwarsa bulan|kartu berakhir (bulan)","data_type":"/financial/creditcard/expirymonth"},{"label":"tahun kedaluwarsa","data_type":"/financial/creditcard/expiryyear"},{"label":"bulan dan tahun","data_type":"/financial/creditcard/expiry2"},{"label":"waktu berakhir","data_type":"/financial/creditcard/expiry"},{"label":"tahun lahir|tahun kelahiran","data_type":"/birthdate/birthyear"},{"label":"bulan lahir|bulan kelahiran","data_type":"/birthdate/birthmonth"},{"label":"hari lahir|tanggal lahir","data_type":"/birthdate/birthday"},{"label":"tanggal lahir|ulang tahun","data_type":"/birthdate"},{"label":"pin","data_type":"/pin"},{"label":"jenis kelamin","data_type":"/person/gender"},{"label":"setuju","data_type":"/agreetos"},{"label":"ingat saya","data_type":"/rememberme"}],"password":[{"label":"kata sandi","data_type":"/password"},{"label":"password lama","data_type":"/passwordold"},{"label":"cvv kartu kredit","data_type":"/financial/creditcard/verification"},{"label":"pin","data_type":"/pin"}],"select":[{"label":"kode negara kerja","data_type":"/contact/phone/countrycode/work"},{"label":"kode negara asal","data_type":"/contact/phone/countrycode/home"},{"label":"kode negara","data_type":"/contact/phone/countrycode"},{"label":"negara pengiriman","data_type":"/contact/country/shipping"},{"label":"negara penagihan","data_type":"/contact/country/billing"},{"label":"negara bisnis","data_type":"/contact/country/business"},{"label":"tanah air","data_type":"/contact/country/home"},{"label":"negara","data_type":"/contact/country"},{"label":"negara pengiriman","data_type":"/contact/state/shipping"},{"label":"negara penagihan","data_type":"/contact/state/billing"},{"label":"negara bisnis","data_type":"/contact/state/business"},{"label":"keadaan rumah","data_type":"/contact/state/home"},{"label":"negara","data_type":"/contact/state"},{"label":"tipe kartu kredit","data_type":"/financial/creditcard/type"},{"label":"kartu kredit kedaluwarsa bulan|kartu berakhir (bulan)","data_type":"/financial/creditcard/expirymonth"},{"label":"tahun kedaluwarsa","data_type":"/financial/creditcard/expiryyear"},{"label":"bulan dan tahun","data_type":"/financial/creditcard/expiry2"},{"label":"waktu berakhir","data_type":"/financial/creditcard/expiry"},{"label":"tahun lahir|tahun kelahiran","data_type":"/birthdate/birthyear"},{"label":"bulan lahir|bulan kelahiran","data_type":"/birthdate/birthmonth"},{"label":"hari lahir|tanggal lahir","data_type":"/birthdate/birthday"},{"label":"tanggal lahir|ulang tahun","data_type":"/birthdate"}]},"sw":{"text":[{"label":"kamata","data_type":"/captcha"},{"label":"search","data_type":"/search"},{"label":"hint","data_type":"/auth/hint"},{"label":"jibu","data_type":"/auth/answer"},{"label":"swali|changamoto","data_type":"/auth/question"},{"label":"email|barua pepe","data_type":"/contact/email"},{"label":"kampuni|jina la kampuni","data_type":"/company/name"},{"label":"kirafiki|username|ingia","data_type":"/nameperson/friendly"},{"label":"kiambishi awali","data_type":"/nameperson/prefix"},{"label":"mwisho|jina la familia","negative":"kwanza","data_type":"/nameperson/last"},{"label":"kwanza","negative":"mwisho","data_type":"/nameperson/first"},{"label":"katikati|jina la kati","negative":"mwisho|kwanza","data_type":"/nameperson/middle"},{"label":"jina kamili","data_type":"/nameperson/full"},{"label":"suffix","data_type":"/nameperson/suffix"},{"label":"nywila","data_type":"/password"},{"label":"nywila umri","data_type":"/passwordold"},{"label":"twitter","data_type":"/contact/im/twitter"},{"label":"linkedin|wanaohusishwa katika|wanaohusishwa-katika","data_type":"/contact/web/linkedin"},{"label":"blog","data_type":"/contact/web/blog"},{"label":"lengo|aol papo mjumbe","data_type":"/contact/IM/aim"},{"label":"icq","data_type":"/contact/IM/icq"},{"label":"msn","data_type":"/contact/IM/msn"},{"label":"jabber","data_type":"/contact/IM/jabber"},{"label":"skype","data_type":"/contact/IM/skype"},{"label":"yahoo","data_type":"/contact/IM/yahoo"},{"label":"kazi ya simu|biashara ya simu","data_type":"/contact/phone/extension/work"},{"label":"ugani simu nyumbani","data_type":"/contact/phone/extension/home"},{"label":"ugani","data_type":"/contact/phone/extension"},{"label":"eneo","data_type":"/contact/phone/areacode/work"},{"label":"eneo","data_type":"/contact/phone/areacode"},{"label":"namba","data_type":"/contact/phone/local/work"},{"label":"namba","data_type":"/contact/phone/local/home"},{"label":"namba","data_type":"/contact/phone/local"},{"label":"kazi nchi kificho","data_type":"/contact/phone/countrycode/work"},{"label":"nyumbani nchi kificho","data_type":"/contact/phone/countrycode/home"},{"label":"nchi kificho","data_type":"/contact/phone/countrycode"},{"label":"simu ya rununu|kiini","data_type":"/contact/phone/mobile"},{"label":"faksi","data_type":"/contact/phone/fax"},{"label":"kazi ya simu|biashara ya simu|kampuni ya simu|mstari wa biashara","data_type":"/contact/phone/work"},{"label":"simu nyumbani","data_type":"/contact/phone/home"},{"label":"simu","data_type":"/contact/phone"},{"label":"meli zip code|meli posta","data_type":"/contact/postalcode/shipping"},{"label":"bili zip code","data_type":"/contact/postalcode/billing"},{"label":"biashara zip code|kazi zip code|kanuni ya posta","data_type":"/contact/postalcode/business"},{"label":"nyumbani zip code|nyumbani code posta","data_type":"/contact/postalcode/home"},{"label":"namba ya posta|kanuni ya posta","data_type":"/contact/postalcode"},{"label":"meli mji","data_type":"/contact/city/shipping"},{"label":"bili mji","data_type":"/contact/city/billing"},{"label":"biashara ya mji","data_type":"/contact/city/business"},{"label":"nyumbani mji","data_type":"/contact/city/home"},{"label":"mji","data_type":"/contact/city"},{"label":"nchi meli","data_type":"/contact/country/shipping"},{"label":"nchi bili","data_type":"/contact/country/billing"},{"label":"nchi ya biashara","data_type":"/contact/country/business"},{"label":"nchi nyumbani","data_type":"/contact/country/home"},{"label":"nchi","data_type":"/contact/country"},{"label":"hali ya meli","data_type":"/contact/state/shipping"},{"label":"hali ya bili","data_type":"/contact/state/billing"},{"label":"hali ya biashara","data_type":"/contact/state/business"},{"label":"hali ya nyumbani","data_type":"/contact/state/home"},{"label":"hali","data_type":"/contact/state"},{"label":"meli plus 4|anuani mstari wa 2|anayeweza|suite","data_type":"/contact/postaladdressAdditional/shipping"},{"label":"bili plus 4|line bili 2|anayeweza|suite","data_type":"/contact/postaladdressAdditional/billing"},{"label":"biashara pamoja na 4","data_type":"/contact/postaladdressAdditional/business"},{"label":"nyumbani plus 4","data_type":"/contact/postaladdressAdditional/home"},{"label":"plus 4","data_type":"/contact/postaladdressAdditional"},{"label":"anwani ya kusafirishia","data_type":"/contact/postaladdress/shipping"},{"label":"mahali deni litakapotumwa","data_type":"/contact/postaladdress/billing"},{"label":"biashara mahali","data_type":"/contact/postaladdress/business"},{"label":"anwani ya nyumbani","data_type":"/contact/postaladdress/home"},{"label":"anuani","data_type":"/contact/postaladdress"},{"label":"ujumbe","data_type":"/message"},{"label":"somo","data_type":"/subject"},{"label":"jina la kwanza kwenye kadi ya mikopo","data_type":"/financial/creditcard/issuedto/first"},{"label":"jina la mwisho juu ya kadi","data_type":"/financial/creditcard/issuedto/last"},{"label":"kadi issuer","data_type":"/financial/creditcard/issuedto"},{"label":"kadi cvv","data_type":"/financial/creditcard/verification"},{"label":"kadi namba sehemu 1","data_type":"/financial/creditcard/number/part1"},{"label":"kadi namba sehemu 2","data_type":"/financial/creditcard/number/part2"},{"label":"kadi namba sehemu 3","data_type":"/financial/creditcard/number/part3"},{"label":"kadi namba sehemu 4","data_type":"/financial/creditcard/number/part4"},{"label":"idadi ya kadi","data_type":"/financial/creditcard/number"},{"label":"aina kadi","data_type":"/financial/creditcard/type"},{"label":"mwezi kadi ya kumalizika muda|kadi muda wake (mwezi)","data_type":"/financial/creditcard/expirymonth"},{"label":"mwaka kumalizika muda","data_type":"/financial/creditcard/expiryyear"},{"label":"mwezi na mwaka","data_type":"/financial/creditcard/expiry2"},{"label":"kumalizika muda","data_type":"/financial/creditcard/expiry"},{"label":"mwaka wa kuzaliwa","data_type":"/birthdate/birthyear"},{"label":"mwezi kuzaliwa|mwezi wa kuzaliwa","data_type":"/birthdate/birthmonth"},{"label":"siku ya kuzaliwa","data_type":"/birthdate/birthday"},{"label":"tarehe ya kuzaliwa|siku ya kuzaliwa","data_type":"/birthdate"},{"label":"siri","data_type":"/pin"},{"label":"jinsia","data_type":"/person/gender"},{"label":"kukubaliana","data_type":"/agreetos"},{"label":"nikumbuke","data_type":"/rememberme"}],"password":[{"label":"nywila","data_type":"/password"},{"label":"nywila umri","data_type":"/passwordold"},{"label":"kadi cvv","data_type":"/financial/creditcard/verification"},{"label":"siri","data_type":"/pin"}],"select":[{"label":"kazi nchi kificho","data_type":"/contact/phone/countrycode/work"},{"label":"nyumbani nchi kificho","data_type":"/contact/phone/countrycode/home"},{"label":"nchi kificho","data_type":"/contact/phone/countrycode"},{"label":"nchi meli","data_type":"/contact/country/shipping"},{"label":"nchi bili","data_type":"/contact/country/billing"},{"label":"nchi ya biashara","data_type":"/contact/country/business"},{"label":"nchi nyumbani","data_type":"/contact/country/home"},{"label":"nchi","data_type":"/contact/country"},{"label":"hali ya meli","data_type":"/contact/state/shipping"},{"label":"hali ya bili","data_type":"/contact/state/billing"},{"label":"hali ya biashara","data_type":"/contact/state/business"},{"label":"hali ya nyumbani","data_type":"/contact/state/home"},{"label":"hali","data_type":"/contact/state"},{"label":"aina kadi","data_type":"/financial/creditcard/type"},{"label":"mwezi kadi ya kumalizika muda|kadi muda wake (mwezi)","data_type":"/financial/creditcard/expirymonth"},{"label":"mwaka kumalizika muda","data_type":"/financial/creditcard/expiryyear"},{"label":"mwezi na mwaka","data_type":"/financial/creditcard/expiry2"},{"label":"kumalizika muda","data_type":"/financial/creditcard/expiry"},{"label":"mwaka wa kuzaliwa","data_type":"/birthdate/birthyear"},{"label":"mwezi kuzaliwa|mwezi wa kuzaliwa","data_type":"/birthdate/birthmonth"},{"label":"siku ya kuzaliwa","data_type":"/birthdate/birthday"},{"label":"tarehe ya kuzaliwa|siku ya kuzaliwa","data_type":"/birthdate"}]},"is":{"text":[{"label":"captcha","data_type":"/captcha"},{"label":"leita","data_type":"/search"},{"label":"vísbending","data_type":"/auth/hint"},{"label":"svarið","data_type":"/auth/answer"},{"label":"spurning|áskorun","data_type":"/auth/question"},{"label":"tölvupóstur|e-mail","data_type":"/contact/email"},{"label":"félagið|nafn fyrirtækis","data_type":"/company/name"},{"label":"vingjarnlegur|notandanafn|skrá inn|notandi","data_type":"/nameperson/friendly"},{"label":"forskeyti","data_type":"/nameperson/prefix"},{"label":"síðasta|eftirnafn","negative":"fyrsta","data_type":"/nameperson/last"},{"label":"fyrsta","negative":"síðasta","data_type":"/nameperson/first"},{"label":"miðja|millinafn","negative":"síðasta|fyrsta","data_type":"/nameperson/middle"},{"label":"fullt nafn","data_type":"/nameperson/full"},{"label":"viðskeyti","data_type":"/nameperson/suffix"},{"label":"lykilorð","data_type":"/password"},{"label":"lykilorð gamall","data_type":"/passwordold"},{"label":"kvak","data_type":"/contact/im/twitter"},{"label":"linkedin|tengd í|linked-í","data_type":"/contact/web/linkedin"},{"label":"blogg","data_type":"/contact/web/blog"},{"label":"markmið|aol instant messenger","data_type":"/contact/IM/aim"},{"label":"icq","data_type":"/contact/IM/icq"},{"label":"msn","data_type":"/contact/IM/msn"},{"label":"jabber","data_type":"/contact/IM/jabber"},{"label":"skype","data_type":"/contact/IM/skype"},{"label":"yahoo","data_type":"/contact/IM/yahoo"},{"label":"vinna síma|sími fyrirtæki","data_type":"/contact/phone/extension/work"},{"label":"heimasími eftirnafn","data_type":"/contact/phone/extension/home"},{"label":"framlenging","data_type":"/contact/phone/extension"},{"label":"svæðisnúmer","data_type":"/contact/phone/areacode/work"},{"label":"svæðisnúmer","data_type":"/contact/phone/areacode"},{"label":"sími","data_type":"/contact/phone/local/work"},{"label":"sími","data_type":"/contact/phone/local/home"},{"label":"sími","data_type":"/contact/phone/local"},{"label":"vinna landsnúmer","data_type":"/contact/phone/countrycode/work"},{"label":"heim landsnúmer","data_type":"/contact/phone/countrycode/home"},{"label":"landsnúmer","data_type":"/contact/phone/countrycode"},{"label":"farsími|klefi","data_type":"/contact/phone/mobile"},{"label":"fax","data_type":"/contact/phone/fax"},{"label":"vinna síma|sími fyrirtæki|sameiginlegur sími|afkomusviðið","data_type":"/contact/phone/work"},{"label":"heimasími","data_type":"/contact/phone/home"},{"label":"síminn","data_type":"/contact/phone"},{"label":"skipum póstnúmer","data_type":"/contact/postalcode/shipping"},{"label":"innheimtu póstnúmer","data_type":"/contact/postalcode/billing"},{"label":"viðskipti póstnúmer|vinna póstnúmer","data_type":"/contact/postalcode/business"},{"label":"heimili póstnúmer","data_type":"/contact/postalcode/home"},{"label":"póstnúmer","data_type":"/contact/postalcode"},{"label":"skipum borg","data_type":"/contact/city/shipping"},{"label":"innheimtu borg","data_type":"/contact/city/billing"},{"label":"fyrirtæki borgarinnar","data_type":"/contact/city/business"},{"label":"forsíða city","data_type":"/contact/city/home"},{"label":"borg","data_type":"/contact/city"},{"label":"shipping land","data_type":"/contact/country/shipping"},{"label":"innheimtulandið","data_type":"/contact/country/billing"},{"label":"viðskipti land","data_type":"/contact/country/business"},{"label":"heimaland","data_type":"/contact/country/home"},{"label":"landið","data_type":"/contact/country"},{"label":"skipum ástand","data_type":"/contact/state/shipping"},{"label":"innheimtu ástand","data_type":"/contact/state/billing"},{"label":"viðskipti ástand","data_type":"/contact/state/business"},{"label":"heimaríki","data_type":"/contact/state/home"},{"label":"ástand","data_type":"/contact/state"},{"label":"skipum plús 4|heimilisfang lína 2|íbúð|suite","data_type":"/contact/postaladdressAdditional/shipping"},{"label":"innheimtu plús 4|innheimtu lína 2|íbúð|suite","data_type":"/contact/postaladdressAdditional/billing"},{"label":"viðskipti plús 4","data_type":"/contact/postaladdressAdditional/business"},{"label":"heimili plús 4","data_type":"/contact/postaladdressAdditional/home"},{"label":"plús 4","data_type":"/contact/postaladdressAdditional"},{"label":"sendingarheimilisfang","data_type":"/contact/postaladdress/shipping"},{"label":"greiðslufang","data_type":"/contact/postaladdress/billing"},{"label":"viðskipti netfang","data_type":"/contact/postaladdress/business"},{"label":"heimilisfangið","data_type":"/contact/postaladdress/home"},{"label":"netfang","data_type":"/contact/postaladdress"},{"label":"skilaboðin","data_type":"/message"},{"label":"efni","data_type":"/subject"},{"label":"fyrsta nafn á kreditkortinu","data_type":"/financial/creditcard/issuedto/first"},{"label":"eftirnafnið á kreditkortinu","data_type":"/financial/creditcard/issuedto/last"},{"label":"útgefandi kortsins","data_type":"/financial/creditcard/issuedto"},{"label":"greiðslukort cvv","data_type":"/financial/creditcard/verification"},{"label":"kreditkortanúmer hluti 1","data_type":"/financial/creditcard/number/part1"},{"label":"kreditkortanúmer hluti 2","data_type":"/financial/creditcard/number/part2"},{"label":"kreditkortanúmer hluti 3","data_type":"/financial/creditcard/number/part3"},{"label":"kreditkortanúmer hluti 4","data_type":"/financial/creditcard/number/part4"},{"label":"kreditkortanúmer","data_type":"/financial/creditcard/number"},{"label":"greiðslukort tegund","data_type":"/financial/creditcard/type"},{"label":"greiðslukort fyrningarmánuður|kort rennur út (mánuður)","data_type":"/financial/creditcard/expirymonth"},{"label":"fyrningarár","data_type":"/financial/creditcard/expiryyear"},{"label":"mánuði og ár","data_type":"/financial/creditcard/expiry2"},{"label":"gildistíma","data_type":"/financial/creditcard/expiry"},{"label":"fæðingarár","data_type":"/birthdate/birthyear"},{"label":"fæðingu mánuði|fæðingarmánuð","data_type":"/birthdate/birthmonth"},{"label":"fæðing dag|dagur fæðingu","data_type":"/birthdate/birthday"},{"label":"fæðingardagur|afmæli","data_type":"/birthdate"},{"label":"pinna","data_type":"/pin"},{"label":"kyn","data_type":"/person/gender"},{"label":"sammála","data_type":"/agreetos"},{"label":"mundu eftir mér","data_type":"/rememberme"}],"password":[{"label":"lykilorð","data_type":"/password"},{"label":"lykilorð gamall","data_type":"/passwordold"},{"label":"greiðslukort cvv","data_type":"/financial/creditcard/verification"},{"label":"pinna","data_type":"/pin"}],"select":[{"label":"vinna landsnúmer","data_type":"/contact/phone/countrycode/work"},{"label":"heim landsnúmer","data_type":"/contact/phone/countrycode/home"},{"label":"landsnúmer","data_type":"/contact/phone/countrycode"},{"label":"shipping land","data_type":"/contact/country/shipping"},{"label":"innheimtulandið","data_type":"/contact/country/billing"},{"label":"viðskipti land","data_type":"/contact/country/business"},{"label":"heimaland","data_type":"/contact/country/home"},{"label":"landið","data_type":"/contact/country"},{"label":"skipum ástand","data_type":"/contact/state/shipping"},{"label":"innheimtu ástand","data_type":"/contact/state/billing"},{"label":"viðskipti ástand","data_type":"/contact/state/business"},{"label":"heimaríki","data_type":"/contact/state/home"},{"label":"ástand","data_type":"/contact/state"},{"label":"greiðslukort tegund","data_type":"/financial/creditcard/type"},{"label":"greiðslukort fyrningarmánuður|kort rennur út (mánuður)","data_type":"/financial/creditcard/expirymonth"},{"label":"fyrningarár","data_type":"/financial/creditcard/expiryyear"},{"label":"mánuði og ár","data_type":"/financial/creditcard/expiry2"},{"label":"gildistíma","data_type":"/financial/creditcard/expiry"},{"label":"fæðingarár","data_type":"/birthdate/birthyear"},{"label":"fæðingu mánuði|fæðingarmánuð","data_type":"/birthdate/birthmonth"},{"label":"fæðing dag|dagur fæðingu","data_type":"/birthdate/birthday"},{"label":"fæðingardagur|afmæli","data_type":"/birthdate"}]},"ga":{"text":[{"label":"captcha","data_type":"/captcha"},{"label":"cuardaigh","data_type":"/search"},{"label":"leid","data_type":"/auth/hint"},{"label":"freagra","data_type":"/auth/answer"},{"label":"ceist|dúshlán","data_type":"/auth/question"},{"label":"ríomhphost","data_type":"/contact/email"},{"label":"cuideachta|ainm na cuideachta","data_type":"/company/name"},{"label":"cairdiúil|ainm úsáideora|logáil isteach|faoi ​​úsáideoir","data_type":"/nameperson/friendly"},{"label":"réimír","data_type":"/nameperson/prefix"},{"label":"deireanach|sloinne","negative":"chéad","data_type":"/nameperson/last"},{"label":"chéad","negative":"deireanach","data_type":"/nameperson/first"},{"label":"lár|dara ainm","negative":"deireanach|chéad","data_type":"/nameperson/middle"},{"label":"ainm iomlán","data_type":"/nameperson/full"},{"label":"iarmhír","data_type":"/nameperson/suffix"},{"label":"focal faire","data_type":"/password"},{"label":"focal faire d'aois","data_type":"/passwordold"},{"label":"twitter","data_type":"/contact/im/twitter"},{"label":"linkedin|nasctha i|nasctha-in","data_type":"/contact/web/linkedin"},{"label":"blog","data_type":"/contact/web/blog"},{"label":"aidhm|aol toirt teachtaire","data_type":"/contact/IM/aim"},{"label":"icq","data_type":"/contact/IM/icq"},{"label":"msn","data_type":"/contact/IM/msn"},{"label":"jabber","data_type":"/contact/IM/jabber"},{"label":"skype","data_type":"/contact/IM/skype"},{"label":"yahoo","data_type":"/contact/IM/yahoo"},{"label":"fón oibre|fón gnó","data_type":"/contact/phone/extension/work"},{"label":"síneadh fón baile","data_type":"/contact/phone/extension/home"},{"label":"síneadh","data_type":"/contact/phone/extension"},{"label":"cód cheantar","data_type":"/contact/phone/areacode/work"},{"label":"cód cheantar","data_type":"/contact/phone/areacode"},{"label":"teileafón","data_type":"/contact/phone/local/work"},{"label":"teileafón","data_type":"/contact/phone/local/home"},{"label":"teileafón","data_type":"/contact/phone/local"},{"label":"cód tíre oibre","data_type":"/contact/phone/countrycode/work"},{"label":"cód thír dhúchais","data_type":"/contact/phone/countrycode/home"},{"label":"cód tíre","data_type":"/contact/phone/countrycode"},{"label":"fón póca|cill","data_type":"/contact/phone/mobile"},{"label":"facs","data_type":"/contact/phone/fax"},{"label":"fón oibre|fón gnó|fón corparáideach|líne gnó","data_type":"/contact/phone/work"},{"label":"fón baile","data_type":"/contact/phone/home"},{"label":"fón","data_type":"/contact/phone"},{"label":"loingseoireachta cód zip|loingseoireachta cód poist","data_type":"/contact/postalcode/shipping"},{"label":"billeála cód zip","data_type":"/contact/postalcode/billing"},{"label":"cód zip gnó|cód zip oibre|cód poist","data_type":"/contact/postalcode/business"},{"label":"cód zip baile|baile cód poist","data_type":"/contact/postalcode/home"},{"label":"cód zip|cód poist","data_type":"/contact/postalcode"},{"label":"chathair loingseoireachta","data_type":"/contact/city/shipping"},{"label":"chathair billeála","data_type":"/contact/city/billing"},{"label":"chathair gnó","data_type":"/contact/city/business"},{"label":"chathair bhaile","data_type":"/contact/city/home"},{"label":"cathrach","data_type":"/contact/city"},{"label":"tír loingseoireachta","data_type":"/contact/country/shipping"},{"label":"tír billeála","data_type":"/contact/country/billing"},{"label":"tír gnó","data_type":"/contact/country/business"},{"label":"thír dhúchais","data_type":"/contact/country/home"},{"label":"tír","data_type":"/contact/country"},{"label":"stáit loingseoireachta","data_type":"/contact/state/shipping"},{"label":"stáit billeála","data_type":"/contact/state/billing"},{"label":"stáit gnó","data_type":"/contact/state/business"},{"label":"stát baile","data_type":"/contact/state/home"},{"label":"stáit","data_type":"/contact/state"},{"label":"loingseoireachta móide 4|seoladh líne 2|apt|sraith","data_type":"/contact/postaladdressAdditional/shipping"},{"label":"billeála móide 4|líne billeála 2|apt|sraith","data_type":"/contact/postaladdressAdditional/billing"},{"label":"gnó móide 4","data_type":"/contact/postaladdressAdditional/business"},{"label":"baile móide 4","data_type":"/contact/postaladdressAdditional/home"},{"label":"móide 4","data_type":"/contact/postaladdressAdditional"},{"label":"seoladh loingseoireacht","data_type":"/contact/postaladdress/shipping"},{"label":"seoladh billeála","data_type":"/contact/postaladdress/billing"},{"label":"seoladh gnó","data_type":"/contact/postaladdress/business"},{"label":"seoladh baile","data_type":"/contact/postaladdress/home"},{"label":"seoladh","data_type":"/contact/postaladdress"},{"label":"teachtaireacht","data_type":"/message"},{"label":"ábhar","data_type":"/subject"},{"label":"céadainm ar chárta creidmheasa","data_type":"/financial/creditcard/issuedto/first"},{"label":"sloinne ar chárta creidmheasa","data_type":"/financial/creditcard/issuedto/last"},{"label":"eisitheoir cárta creidmheasa","data_type":"/financial/creditcard/issuedto"},{"label":"cvv cárta creidmheasa","data_type":"/financial/creditcard/verification"},{"label":"uimhir an chárta creidmheasa cuid 1","data_type":"/financial/creditcard/number/part1"},{"label":"uimhir an chárta creidmheasa cuid 2","data_type":"/financial/creditcard/number/part2"},{"label":"uimhir an chárta creidmheasa cuid 3","data_type":"/financial/creditcard/number/part3"},{"label":"uimhir an chárta creidmheasa cuid 4","data_type":"/financial/creditcard/number/part4"},{"label":"uimhir an chárta creidmheasa","data_type":"/financial/creditcard/number"},{"label":"chineál cárta creidmheasa","data_type":"/financial/creditcard/type"},{"label":"creidmheasa cárta dhul in éag mí|éag cárta (mí)","data_type":"/financial/creditcard/expirymonth"},{"label":"glanadh ndeireadh bliana","data_type":"/financial/creditcard/expiryyear"},{"label":"mí agus bliain","data_type":"/financial/creditcard/expiry2"},{"label":"bheith caite","data_type":"/financial/creditcard/expiry"},{"label":"bliain bhreithe|bliain breithe","data_type":"/birthdate/birthyear"},{"label":"mí breithe","data_type":"/birthdate/birthmonth"},{"label":"lá breithe","data_type":"/birthdate/birthday"},{"label":"dáta breithe|lá breithe","data_type":"/birthdate"},{"label":"bioráin","data_type":"/pin"},{"label":"inscne","data_type":"/person/gender"},{"label":"aontú","data_type":"/agreetos"},{"label":"ná déan dearmad orm","data_type":"/rememberme"}],"password":[{"label":"focal faire","data_type":"/password"},{"label":"focal faire d'aois","data_type":"/passwordold"},{"label":"cvv cárta creidmheasa","data_type":"/financial/creditcard/verification"},{"label":"bioráin","data_type":"/pin"}],"select":[{"label":"cód tíre oibre","data_type":"/contact/phone/countrycode/work"},{"label":"cód thír dhúchais","data_type":"/contact/phone/countrycode/home"},{"label":"cód tíre","data_type":"/contact/phone/countrycode"},{"label":"tír loingseoireachta","data_type":"/contact/country/shipping"},{"label":"tír billeála","data_type":"/contact/country/billing"},{"label":"tír gnó","data_type":"/contact/country/business"},{"label":"thír dhúchais","data_type":"/contact/country/home"},{"label":"tír","data_type":"/contact/country"},{"label":"stáit loingseoireachta","data_type":"/contact/state/shipping"},{"label":"stáit billeála","data_type":"/contact/state/billing"},{"label":"stáit gnó","data_type":"/contact/state/business"},{"label":"stát baile","data_type":"/contact/state/home"},{"label":"stáit","data_type":"/contact/state"},{"label":"chineál cárta creidmheasa","data_type":"/financial/creditcard/type"},{"label":"creidmheasa cárta dhul in éag mí|éag cárta (mí)","data_type":"/financial/creditcard/expirymonth"},{"label":"glanadh ndeireadh bliana","data_type":"/financial/creditcard/expiryyear"},{"label":"mí agus bliain","data_type":"/financial/creditcard/expiry2"},{"label":"bheith caite","data_type":"/financial/creditcard/expiry"},{"label":"bliain bhreithe|bliain breithe","data_type":"/birthdate/birthyear"},{"label":"mí breithe","data_type":"/birthdate/birthmonth"},{"label":"lá breithe","data_type":"/birthdate/birthday"},{"label":"dáta breithe|lá breithe","data_type":"/birthdate"}]},"lt":{"text":[{"label":"captcha","data_type":"/captcha"},{"label":"paieška","data_type":"/search"},{"label":"užuomina","data_type":"/auth/hint"},{"label":"atsakymas","data_type":"/auth/answer"},{"label":"klausimas|iššūkis","data_type":"/auth/question"},{"label":"siųsti|elektroninis paštas","data_type":"/contact/email"},{"label":"bendrovė|Įmonės pavadinimas","data_type":"/company/name"},{"label":"draugiškas|vartotojo vardas|prisijungti|vartotojas","data_type":"/nameperson/friendly"},{"label":"priešdėlis","data_type":"/nameperson/prefix"},{"label":"paskutinis|pavardė","negative":"pirmas","data_type":"/nameperson/last"},{"label":"pirmas","negative":"paskutinis","data_type":"/nameperson/first"},{"label":"vidurys|antras vardas","negative":"paskutinis|pirmas","data_type":"/nameperson/middle"},{"label":"pilnas vardas","data_type":"/nameperson/full"},{"label":"priesaga","data_type":"/nameperson/suffix"},{"label":"slaptažodis","data_type":"/password"},{"label":"slaptažodis metai","data_type":"/passwordold"},{"label":"twitter","data_type":"/contact/im/twitter"},{"label":"linkedin|susietos|susiję-in","data_type":"/contact/web/linkedin"},{"label":"dienoraštis","data_type":"/contact/web/blog"},{"label":"tikslas|aol instant messenger","data_type":"/contact/IM/aim"},{"label":"icq","data_type":"/contact/IM/icq"},{"label":"msn","data_type":"/contact/IM/msn"},{"label":"plerpti","data_type":"/contact/IM/jabber"},{"label":"skype","data_type":"/contact/IM/skype"},{"label":"yahoo","data_type":"/contact/IM/yahoo"},{"label":"darbo telefonas|verslo telefonas","data_type":"/contact/phone/extension/work"},{"label":"namų telefono pratęsimo","data_type":"/contact/phone/extension/home"},{"label":"pratęsimas","data_type":"/contact/phone/extension"},{"label":"vietovės kodas","data_type":"/contact/phone/areacode/work"},{"label":"vietovės kodas","data_type":"/contact/phone/areacode"},{"label":"telefonas","data_type":"/contact/phone/local/work"},{"label":"telefonas","data_type":"/contact/phone/local/home"},{"label":"telefonas","data_type":"/contact/phone/local"},{"label":"darbas šalies kodas","data_type":"/contact/phone/countrycode/work"},{"label":"pagrindinis šalies kodas","data_type":"/contact/phone/countrycode/home"},{"label":"šalies kodas","data_type":"/contact/phone/countrycode"},{"label":"mobilusis telefonas|ląstelė","data_type":"/contact/phone/mobile"},{"label":"faksas","data_type":"/contact/phone/fax"},{"label":"darbo telefonas|verslo telefonas|firmos telefonas|verslo linija","data_type":"/contact/phone/work"},{"label":"namų telefono","data_type":"/contact/phone/home"},{"label":"telefonas","data_type":"/contact/phone"},{"label":"pristatymas pašto kodas|laivybos pašto kodą","data_type":"/contact/postalcode/shipping"},{"label":"sąskaitos siuntimo pašto indeksas","data_type":"/contact/postalcode/billing"},{"label":"verslo pašto kodas|darbas pašto kodas","data_type":"/contact/postalcode/business"},{"label":"pagrindinis pašto kodas|namo pašto kodas","data_type":"/contact/postalcode/home"},{"label":"pašto kodas","data_type":"/contact/postalcode"},{"label":"pristatymas mieste","data_type":"/contact/city/shipping"},{"label":"atsiskaitymo miesto","data_type":"/contact/city/billing"},{"label":"verslo miestas","data_type":"/contact/city/business"},{"label":"gimtasis miestas","data_type":"/contact/city/home"},{"label":"miestas","data_type":"/contact/city"},{"label":"pristatymas šalis","data_type":"/contact/country/shipping"},{"label":"atsiskaitymo šalis","data_type":"/contact/country/billing"},{"label":"Šalys pagal veiklas","data_type":"/contact/country/business"},{"label":"gimtoji šalis","data_type":"/contact/country/home"},{"label":"Šalis","data_type":"/contact/country"},{"label":"siunčiančioji valstybė","data_type":"/contact/state/shipping"},{"label":"atsiskaitymo būklė","data_type":"/contact/state/billing"},{"label":"verslo būklė","data_type":"/contact/state/business"},{"label":"namų būklė","data_type":"/contact/state/home"},{"label":"valstybės","data_type":"/contact/state"},{"label":"pristatymas plius 4|adreso eilutė 2|linkęs|rinkinys","data_type":"/contact/postaladdressAdditional/shipping"},{"label":"atsiskaitymo plius 4|atsiskaitymo linija 2|linkęs|rinkinys","data_type":"/contact/postaladdressAdditional/billing"},{"label":"verslo plius 4","data_type":"/contact/postaladdressAdditional/business"},{"label":"namo plius 4","data_type":"/contact/postaladdressAdditional/home"},{"label":"plius 4","data_type":"/contact/postaladdressAdditional"},{"label":"pristatymo adresas","data_type":"/contact/postaladdress/shipping"},{"label":"atsiskaitymo adresas","data_type":"/contact/postaladdress/billing"},{"label":"verslo adresas","data_type":"/contact/postaladdress/business"},{"label":"namų adresas","data_type":"/contact/postaladdress/home"},{"label":"adresas","data_type":"/contact/postaladdress"},{"label":"žinutė","data_type":"/message"},{"label":"tema","data_type":"/subject"},{"label":"vardas ant kredito kortelės","data_type":"/financial/creditcard/issuedto/first"},{"label":"pavardė ant kredito kortelės","data_type":"/financial/creditcard/issuedto/last"},{"label":"kredito kortelės išdavėjas","data_type":"/financial/creditcard/issuedto"},{"label":"kreditinės kortelės cvv","data_type":"/financial/creditcard/verification"},{"label":"kredito kortelės numeris dalis 1","data_type":"/financial/creditcard/number/part1"},{"label":"kredito kortelės numeris dalis 2","data_type":"/financial/creditcard/number/part2"},{"label":"kredito kortelės numeris dalis 3","data_type":"/financial/creditcard/number/part3"},{"label":"kredito kortelės numeris dalis 4","data_type":"/financial/creditcard/number/part4"},{"label":"kreditinės kortelės numeris","data_type":"/financial/creditcard/number"},{"label":"kredito kortelės tipas","data_type":"/financial/creditcard/type"},{"label":"kreditinės kortelės galiojimo mėnesį|kortelės galiojimo laikas (mėnuo)","data_type":"/financial/creditcard/expirymonth"},{"label":"galiojimo metus","data_type":"/financial/creditcard/expiryyear"},{"label":"mėnesį ir metus","data_type":"/financial/creditcard/expiry2"},{"label":"pasibaigimas","data_type":"/financial/creditcard/expiry"},{"label":"gimimo metai","data_type":"/birthdate/birthyear"},{"label":"gimimo mėnesį|mėnesį gimimo","data_type":"/birthdate/birthmonth"},{"label":"gimtadienis|diena gimimo","data_type":"/birthdate/birthday"},{"label":"gimimo data|gimtadienis","data_type":"/birthdate"},{"label":"kaištis","data_type":"/pin"},{"label":"lytis","data_type":"/person/gender"},{"label":"susitarti","data_type":"/agreetos"},{"label":"prisimink mane","data_type":"/rememberme"}],"password":[{"label":"slaptažodis","data_type":"/password"},{"label":"slaptažodis metai","data_type":"/passwordold"},{"label":"kreditinės kortelės cvv","data_type":"/financial/creditcard/verification"},{"label":"kaištis","data_type":"/pin"}],"select":[{"label":"darbas šalies kodas","data_type":"/contact/phone/countrycode/work"},{"label":"pagrindinis šalies kodas","data_type":"/contact/phone/countrycode/home"},{"label":"šalies kodas","data_type":"/contact/phone/countrycode"},{"label":"pristatymas šalis","data_type":"/contact/country/shipping"},{"label":"atsiskaitymo šalis","data_type":"/contact/country/billing"},{"label":"Šalys pagal veiklas","data_type":"/contact/country/business"},{"label":"gimtoji šalis","data_type":"/contact/country/home"},{"label":"Šalis","data_type":"/contact/country"},{"label":"siunčiančioji valstybė","data_type":"/contact/state/shipping"},{"label":"atsiskaitymo būklė","data_type":"/contact/state/billing"},{"label":"verslo būklė","data_type":"/contact/state/business"},{"label":"namų būklė","data_type":"/contact/state/home"},{"label":"valstybės","data_type":"/contact/state"},{"label":"kredito kortelės tipas","data_type":"/financial/creditcard/type"},{"label":"kreditinės kortelės galiojimo mėnesį|kortelės galiojimo laikas (mėnuo)","data_type":"/financial/creditcard/expirymonth"},{"label":"galiojimo metus","data_type":"/financial/creditcard/expiryyear"},{"label":"mėnesį ir metus","data_type":"/financial/creditcard/expiry2"},{"label":"pasibaigimas","data_type":"/financial/creditcard/expiry"},{"label":"gimimo metai","data_type":"/birthdate/birthyear"},{"label":"gimimo mėnesį|mėnesį gimimo","data_type":"/birthdate/birthmonth"},{"label":"gimtadienis|diena gimimo","data_type":"/birthdate/birthday"},{"label":"gimimo data|gimtadienis","data_type":"/birthdate"}]}},"FormRules":{"registrationForm":{"min_fields":3,"min_rank":30,"action":"regist|signup|sign_up|sign-up|join","action_rank":40,"text":[{"match":"forgot","rank":-40}],"button":[{"match":"forgot","rank":-40},{"match":"sign.*up|register|join|create","rank":20}],"checkbox":[{"match":"email.*promotions|subscribe.*news","rank":40}],"fields":[{"type":"/agreetos","rank":10,"min_field":-1,"max_field":-1},{"type":"/birthdate","rank":10,"min_field":-1,"max_field":-1},{"type":"^/contact/","rank":10,"min_field":-1,"max_field":-1},{"type":"/nameperson/","rank":10,"min_field":-1,"max_field":-1},{"type":"^/password","rank":10,"min_field":-1,"max_field":2},{"type":"/person/gender","rank":10,"min_field":-1,"max_field":-1},{"type":"/financial","rank":-10,"min_field":-1,"max_field":0},{"type":"/message","rank":-50,"min_field":-1,"max_field":-1},{"type":"/subject","rank":-50,"min_field":-1,"max_field":-1}]},"commentForm":{"min_fields":2,"min_rank":20,"fields":[{"type":"/contact/email","rank":10,"min_field":-1,"max_field":-1},{"type":"/message","rank":10,"min_field":1,"max_field":-1},{"type":"/nameperson/friendly","rank":10,"min_field":-1,"max_field":-1},{"type":"/subject","rank":10,"min_field":-1,"max_field":-1},{"type":"/financial","rank":-50,"min_field":0,"max_field":-1}]},"loginForm":{"min_fields":1,"min_rank":20,"action":"login|signin|sign_in|sign-in","action_rank":40,"text":[{"match":"forgot|password.*help","rank":40},{"match":"remember.*me|keep.*me.*(signed|logged)|stay.*(signed|logged).*in","rank":10},{"match":"already.*account","rank":10}],"button":[{"match":"(?\u003c!trouble)(log.*in|sign.*in)","rank":20},{"match":"(trouble.*sign.*in)|(forgot.*password)","rank":40},{"match":"sign.*up|register|join|create","rank":-20}],"fields":[{"type":"/contact/email","rank":10,"min_field":-1,"max_field":1},{"type":"/nameperson/friendly","rank":10,"min_field":-1,"max_field":1},{"type":"^/password$","rank":10,"min_field":-1,"max_field":1},{"type":"/rememberme","rank":10,"min_field":-1,"max_field":-1},{"type":"/birthdate","rank":-10,"min_field":-1,"max_field":-1},{"type":"/(first|middle|last)","rank":-10,"min_field":-1,"max_field":-1},{"type":"/gender","rank":-10,"min_field":-1,"max_field":-1},{"type":"/city","rank":-10,"min_field":-1,"max_field":-1},{"type":"(country|postaladdress|state)","rank":-20,"min_field":-1,"max_field":-1},{"type":"/passwordold","rank":-20,"min_field":-1,"max_field":-1}]},"checkoutForm":{"min_fields":4,"min_rank":30,"fields":[{"type":"^/financial/","rank":10,"min_field":-1,"max_field":1}]},"changePasswordForm":{"min_fields":3,"min_rank":30,"fields":[{"type":"^/password$","rank":10,"min_field":2,"max_field":-1},{"type":"/nameperson/","rank":-10,"min_field":-1,"max_field":1},{"type":"^/contact","rank":-10,"min_field":-1,"max_field":1},{"type":"/passwordold","rank":100,"min_field":-1,"max_field":1}]},"addressForm":{"min_fields":3,"min_rank":30,"fields":[{"type":"/nameperson","rank":10,"min_field":-1,"max_field":-1},{"type":"/contact/email","rank":-10,"min_field":-1,"max_field":0},{"type":"/password","rank":-10,"min_field":-1,"max_field":0},{"type":"/financial","rank":-10,"min_field":-1,"max_field":0},{"type":"/contact/postal","rank":10,"min_field":1,"max_field":-1},{"type":"/contact/(city|country|state)","rank":10,"min_field":-1,"max_field":-1}]}},"KeySimulations":"^in\\.com$|123rf\\.com|1800mattress\\.com|23andme\\.com|aa\\.com|abelandcole\\.co\\.uk|abine\\.com|actblue\\.com|adplexmedia\\.ply2c\\.com|agoda\\.com|airbnb\\.co\\.in|airbnb\\.com|airbnbpayments\\.com|alibaba\\.com|aliexpress\\.com|ally\\.com|amctheatres\\.com|anker\\.com|aol\\.com|apple\\.com|arcteryx\\.com|asana\\.com|auth0\\.com|avira\\.com|bbc\\.com|bestbuy\\.com|bet9ja\\.com|booking\\.com|bookmyshow\\.com|bostonunchained\\.com|box\\.com|breitbart\\.com|bt\\.com|buildabear\\.com|cakespy\\.com|canadapost\\.ca|carmax\\.com|ccavenue\\.com|civic\\.com|codecademy\\.com|connection\\.com|conrad\\.nl|coupons\\.com|crateandbarrel\\.com|crmls\\.org|custommade\\.com|dell\\.com|directvnow\\.com|discuss\\.com\\.hk|dji\\.com|drillspot\\.com|dsw\\.com|duolingo\\.com|ebay\\.com|ebay\\.in|ebharathgas\\.com|eisnersafety\\.com|euromini\\.bike|eventbrite\\.com|everlane\\.com|evernote\\.com|everydayhealth\\.com|facebook\\.com|fakeapp\\.org|fastspring\\.com|fedex\\.com|findingrover\\.com|fiverr\\.com|focuscamera\\.com|footlocker\\.com|foxnews\\.com|freepeople\\.com|g2a\\.com|gamersky\\.com|gap\\.com|garmin\\.com|geappliances\\.io|gemini\\.com|geoguessr\\.com|getjobber\\.com|getquip\\.com|gitcoin\\.co|glassdoor\\.com|goal\\.com|godaddy\\.com|gopro\\.com|grammarly\\.com|gsntv\\.com|hallmark\\.com|healthcare\\.gov|heroku\\.com|hertz\\.com|hertz\\.nl|homechef\\.com|hootsuite\\.com|hover\\.com|hubspot\\.com|hulu\\.com|ibm\\.com|icloud\\.com|ifit\\.com|iheart\\.com|ikea\\.com|indeed\\.com|independent\\.co\\.uk|inet\\.se|ing\\.nl|instagram\\.com|itex\\.com|jcpenney\\.com|jcrew\\.com|jimdo\\.com|joinhoney\\.com|khanacademy\\.org|kickstarter\\.com|klarna\\.com|kohls\\.com|kuvo\\.com|lakeside\\.com|landofnod\\.com|laredoute\\.com|latimes\\.com|le\\.com|lifecell\\.ua|live\\.com|llbean\\.com|mailchimp\\.com|mardigras\\.com|mbc\\.net|mcafee\\.com|meijer\\.com|microcenter\\.com|miniinthebox\\.com|monster\\.com|musiciansfriend\\.com|myclubwyndham\\.com|navyfederal\\.org|netflix\\.com|newatlas\\.com|nextdoor\\.com|nhl\\.com|nordstrom\\.com|northerntool\\.com|nsenmf\\.com|nytimes\\.com|okcupid\\.com|opendns\\.com|oracle\\.com|orientaltrading\\.com|overstock\\.com|pandora\\.com|paypal\\.com|pch\\.com|pgatour\\.com|pinterest\\.com|pizzahut\\.com|portmone\\.com|projectalpha\\.com|publicreputation\\.com|quizlet\\.com|ralphlauren\\.com|rambler\\.ru|realtor\\.ca|redfin\\.com|renttherunway\\.com|repsol\\.com|ringcentral\\.com|saksfifthavenue\\.com|satmetrix\\.com|sbilife\\.co\\.in|sciencedirect\\.com|scribd\\.com|sharefile\\.com|shopdisney\\.com|signnow\\.com|sky\\.com|slack\\.com|softonic\\.com|soundcloud\\.com|squarespace\\.com|squareup\\.com|staples\\.com|steemit\\.com|stripe\\.com|stubhub\\.com|surveymonkey\\.com|target\\.com|telegraph\\.co\\.uk|thekitchn\\.com|thetileapp\\.com|ticketmaster\\.com|tinyhomebuilders\\.com|tokenfoundry\\.com|tokopedia\\.com|tomshardware\\.com|trello\\.com|tripadvisor\\.com|uber\\.com|universe\\.com|usatoday\\.com|venmo\\.com|visa\\.com|walmart\\.com|warbyparker\\.com|wargamevault\\.com|wayfair\\.com|wayfair\\.de|webcruiter\\.com|wetransfer\\.com|wiley\\.com|wordpress\\.com|worldmarket\\.com|wowair\\.com|wsj\\.com|wunderground\\.com|xda-developers\\.com|xsolla\\.com|yours\\.org|yummly\\.co|yummly\\.com|zazzle\\.com|zillow\\.com|zomato\\.com","FormlessDomains":{"mturk.com":4,"dsw.com":4,"sephora.com":4,"jared.com":4,"nordstrom.com":5,"officedepot.com":5,"fingerhut.com":5,"asana.com":5,"sonyentertainmentnetwork.com":5,"ae.com":8,"okcupid.com":8,"quora.com":8,"uptodown.com":8,"nba.com":8,"clickadu.com":8,"walgreens.com":10,"bestbuy.com":10,"indiegogo.com":10,"shutterfly.com":10,"google.com":10,"squarespace.com":10,"vrbo.com":10,"xiaomi.com":10,"llbean.com":10,"hertz.com":10,"amazon.com":10,"cnbc.com":10,"tinypass.com":10,"focuscamera.com":10,"hertz.nl":10,"apple.com":12,"target.com":12,"aliexpress.com":5,"quikr.com":11,"booking.com":6,"trollandtoad.com":7,"homeaway.com":5,"fedex.com":4,"dell.com":5,"ifit.com":9,"hp.com":6,"homedepot.com":6,"donateconsumers.org":18,"udemy.com":7,"thelotter.com":10,"netgear.com":8,"adp.com":8,"revolve.com":8,"ninewest.com":5,"fastcompany.com":12,"ancestry.com":15,"espn.com":4,"pinterest.com":9,"norton.com":10,"ups.com":7,"etsy.com":9,"earn.com":5,"academia.edu":5,"airbnb.com":12,"force.com":9,"dji.com":12,"pbs.org":6,"getjobber.com":10},"AutoCompleteRules":{"off":{"category":"off"},"on":{"category":"automatic"},"name":{"type":"/nameperson/full"},"honorific-prefix":{"type":"/nameperson/prefix"},"given-name":{"type":"/nameperson/first"},"additional-name":{"type":"/nameperson/middle"},"family-name":{"type":"/nameperson/last"},"honorific-suffix":{"type":"/nameperson/suffix"},"nickname":{"type":"/nameperson/friendly"},"organization-title":{"type":"/company/position"},"username":{"type":"/nameperson/friendly"},"new-password":{"type":"/password"},"current-password":{"type":"/password"},"organization":{"type":"/company/name"},"street-address":{"type":"/contact/postaladdress/full"},"address-line1":{"type":"/contact/postaladdress"},"address-line2":{"type":"/contact/postaladdressAdditional"},"address-line3":{"type":"/contact/postaladdress3"},"address-level1":{"type":"/contact/state"},"region":{"type":"/contact/state"},"address-level2":{"type":"/contact/city"},"locality":{"type":"/contact/city"},"address-level3":{"type":"/contact/district"},"address-level4":{"type":null},"country":{"type":"/contact/countrycode"},"country-name":{"type":"/contact/country"},"postal-code":{"type":"/contact/postalcode"},"cc-name":{"type":"/financial/creditcard/issuedto"},"cc-given-name":{"type":"/financial/creditcard/issuedto/first"},"cc-additional-name":{"type":"/financial/creditcard/issuedto/middle"},"cc-family-name":{"type":"/financial/creditcard/issuedto/last"},"cc-number":{"type":"/financial/creditcard/number"},"cc-exp":{"type":"/financial/creditcard/expiry"},"cc-exp-month":{"type":"/financial/creditcard/expirymonth"},"cc-exp-year":{"type":"/financial/creditcard/expiryyear"},"cc-csc":{"type":"/financial/creditcard/verification"},"cc-type":{"type":"/financial/creditcard/type"},"transaction-currency":{"type":"/transaction/currency"},"transaction-amount":{"type":"/transaction/amount"},"language":{"type":"/language"},"bday":{"type":"/birthdate"},"bday-day":{"type":"/birthdate/birthday"},"bday-month":{"type":"/birthdate/birthmonth"},"bday-year":{"type":"/birthdate/birthyear"},"sex":{"type":"/person/gender"},"url":{"type":"/url"},"photo":{"type":"/photo"},"tel":{"category":"contact","type":"/contact/phone/full"},"tel-country-code":{"category":"contact","type":"/contact/phone/countrycode"},"tel-national":{"category":"contact","type":"/contact/phone"},"tel-area-code":{"category":"contact","type":"/contact/phone/areacode"},"tel-local":{"category":"contact","type":"/contact/phone/number"},"tel-local-prefix":{"category":"contact","type":"/contact/phone/exchange"},"tel-local-suffix":{"category":"contact","type":"/contact/phone/local"},"tel-extension":{"category":"contact","type":"/contact/phone/extension"},"email":{"category":"contact","type":"/contact/email"},"impp":{"category":"contact","type":"/contact/im"}}};

  var FormAnalyzer = (function() {

  // dependency: _, $, CryptoJS_SHA1, AbinePhoneHeuristics, AbinePaymentHeuristics, AbineAddressHeuristics, AbineURL, AbineFormRules;


  var LOGIN_FORM = "loginForm";
  var REGISTRATION_FORM = "registrationForm";
  var CHECKOUT_FORM = "checkoutForm";
  var CHANGEPASSWORD_FORM = "changePasswordForm";
  var OTHER_FORM = "otherForm";
  var ADDRESS_FORM = "addressForm";
  var LOGIN_REGISTRATION_FORM = "loginRegistrationForm";

  var EMAIL_FIELD = "emailField";
  var PASSWORD_FIELD = "passwordField";
  var OLDPASSWORD_FIELD = "oldPasswordField";
  var PHONE_FIELD = "phoneField";
  var USERNAME_FIELD = "usernameField";
  var IDENTITY_FIELD = "identityField";
  var PAYMENT_FIELD = "paymentField";
  var ADDRESS_FIELD = "addressField";
  var LOGIN_FIELD = "loginField";

  var LabelStopWordsRegex = /\b(please|fill|your|required|the)\b/ig;

  /**
   * Generates a UUID according to http://www.ietf.org/rfc/rfc4122.txt
   */
  function createUUID() {
    var s = [];
    var hexDigits = "0123456789ABCDEF";
    for ( var i = 0; i < 32; i++) {
      s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
    }
    s[12] = "4";
    s[16] = hexDigits.substr((s[16] & 0x3) | 0x8, 1);

    var uuid = s.join("");
    return uuid;
  }

  function strip(str) {
    if (str) {
      return str.replace(/[\s]+/g, ' ').replace(/^[\s]+|[\s]+$/g, '');
    }
    return str;
  }

  function setupRegex(rule, attributes) {
    if ("_done" in rule) return;
    rule["_done"] = true;
    _.each(attributes, function(name){
      if (name in rule && !rule[name].source) {
        try {
          rule[name] = new RegExp(rule[name], "i");
        } catch(e) {
          // get rid of negative lookbehind rule in regex.
          rule[name] = rule[name].replace(/\(\?\<\![^\)]+\)/, '');
          rule[name] = new RegExp(rule[name], "i");
        }
      }
    });
  }

  function getCommonPrefix(ids) {
    if (!ids || ids.length <= 1) return null;
    // ignore if fields do not have a '$' in name
    if (ids[0].indexOf('$') == -1) return null;
    ids.sort();
    var first = ids[0];
    var last = ids[ids.length-1];
    var len = first.length;
    if (len > last.length) len = last.length;
    var idx = 0;
    while (idx < len && first.charAt(idx) == last.charAt(idx)) idx++;
    return first.substr(0, idx);
  }

  var ccTLDMapping = {
    se: 'nl',
    pl: 'pl',
    gf: 'fr',
    pf: 'fr',
    tf: 'fr',
    jp: 'ja',
    kr: 'ko',
    cn: 'zh-cn',
    tw: 'zh-tw',
    taipei: 'zh-tw',
    gr: 'el',
    cz: 'cs',
    dk: 'da',
    uk: 'en',
    irish: 'ga',
    ua: 'uk',
    vn: 'vi'
  };

  function getLocaleFromUrl(url) {
    var locale = null;
    var domain = AbineURL.getTLD(url);
    if (!domain.match(/^[0-9.:]+$/)) {
      var parts = domain.replace(/:[0-9]+$/, '').split('.');
      var tld = parts[parts.length-1];
      if (tld.length <= 2 || tld in ccTLDMapping) {
        locale = tld;
        if (tld in ccTLDMapping) {
          locale = ccTLDMapping[tld];
        }
      }
    }
    if (!locale) {
      var match = url.match(/^http[s]?:\/\/[^\/]+\/([a-z]{2})\//i);
      if (match) {
        locale = match[1];
      }
    }
    return locale;
  }

  function detectFormLanguage(formEl) {
    var doc = formEl.ownerDocument;
    if (doc._abLangs) return doc._abLangs;
    var lang;
    while (formEl && formEl.getAttribute && !(lang = formEl.getAttribute('lang'))) formEl = formEl.parentNode;
    if (!lang) {
      lang = doc.documentElement.lang;
    }
    if (!lang) {
      lang = $('meta[http-equiv=content-language]', doc.documentElement).attr('content');
    }
    if (!lang) {
      lang = $('meta[name=language]', doc.documentElement).attr('content');
    }
    if (lang) {
      lang = lang.toLowerCase().replace(/\s+/g, '');
    }
    var href = doc.URL || (doc.location && doc.location.href) || null;
    if (href) {
      var langFromUrl = getLocaleFromUrl(href);
      if (langFromUrl) {
        if (lang) {
          if (!lang.match(new RegExp('(^'+langFromUrl+')|([^\-]+'+langFromUrl+')'))) {
            lang = langFromUrl+','+lang;
          }
        } else {
          lang = langFromUrl;
        }
      }
    }
    if (!lang) {
      try {
        lang = doc.defaultView.navigator.language.toLowerCase();
      } catch(e) {}
    }
    if (!lang) {
      lang = 'en-us';
    }
    if (!lang.match(/(^en$)|(,en)|(en[,-_])/)) {
      lang = lang+',en-us';
    }
    lang = lang.replace(/[,]+/g, ',');
    LOG_FINEST && ABINE_DNTME.log.finest("** detected languages " + lang);
    doc._abLangs = lang.split(',');
    return doc._abLangs;
  }

  function getRulesForLanguage(lang, fieldType) {
    if (lang in AbineFormRules.FieldRules) {
      return AbineFormRules.FieldRules[lang][fieldType];
    }
    if (lang.match(/[-_]/)) {
      lang = lang.split(/[-_]/)[0];
    }
    for (var availableLang in AbineFormRules.FieldRules) {
      if (availableLang.indexOf(lang) == 0) {
        return AbineFormRules.FieldRules[availableLang][fieldType];
      }
    }

    return null;
  }

  var UNPROCESSABLE_TAGS = {
    'checkbox': 1,
    'radio': 1,
    'hidden': 1,
    'file' : 1,
    'button': 1,
    'submit': 1,
    'reset': 1,
    'image': 1,
    'time': 1,
    'color': 1,
    'date': 1,
    'datetime': 1,
    'datetime-local': 1,
    'month': 1,
    'range': 1,
    'url': 1,
    'week': 1
  };

  function getFormType(fields, formEl, pageUrl, actionUrl) {
    LOG_FINEST && ABINE_DNTME.log.finest("formAnalyzer:getFormType enter");
    var hasUsernameOrEmail = false, hasDetectedFields = false;
    var dataTypesCount = {};
    _.each(fields, function(field){
      var dataType = field.dataType;
      if (!dataType || dataType == '/ignore') return;
      if (dataType.match(/.+\/(email|friendly|first|last)/i)) hasUsernameOrEmail = true;
      dataTypesCount[dataType] = dataTypesCount[dataType]?dataTypesCount[dataType]+1:1;
      hasDetectedFields = true;
    });
    var buttons = $(':button, :submit, button', formEl);
    var buttonText = [];
    buttons.each(function(idx, button) {
      buttonText.push(button.nodeName.toLowerCase() == 'input' ? button.value : $(button).text());
    });
    var formText = $(formEl).text();
    var msg = '';
    var bestRank = 0, bestFormType = OTHER_FORM;
    _.each(AbineFormRules.FormRules, function(formTypeRule, name){
      var rank = 0;
      if (name == 'registrationForm' && !hasUsernameOrEmail) {
        if (LOG_FINEST) msg += "register = skipped as no username/email\n";
        return;
      }
      setupRegex(formTypeRule, ["action"]);
      var fieldRules = formTypeRule.fields, formTypeNotPossible = false;
      // skip if any min/max required fields condition not satisfied
      _.each(fieldRules, function(rule){
        if (formTypeNotPossible) return;
        setupRegex(rule, ["type"]);
        var numFields = 0;
        var type = rule.type;
        for (var dataType in dataTypesCount) {
          if (dataType.match(type)) {
            numFields += dataTypesCount[dataType];
          }
        }
        if ((rule.min_field >= 0 && numFields < rule.min_field) ||
          (rule.max_field >= 0 && numFields > rule.max_field)) {
          // violated min/max field count for a data_type
          if (LOG_FINEST) msg += name+" = min/max failed "+type+" "+numFields+"\n";
          formTypeNotPossible = true;
        } else {
          rank += (numFields * rule.rank);
        }
      });
      if (formTypeNotPossible) return;
      if (rank > 0) {
        if (formTypeRule.action && ((pageUrl && pageUrl.match(formTypeRule.action)) || (actionUrl && actionUrl.match(formTypeRule.action)))) {
          if (LOG_FINEST) msg += 'action/page url pattern matched, rank increased by '+formTypeRule.action_rank+' for '+name+'\n';
          rank += formTypeRule.action_rank;
        }
        if (formTypeRule.text) {
          _.each(formTypeRule.text, function(textRule){
            setupRegex(textRule, ["match"]);
            if (formText.match(textRule.match)) {
              rank += textRule.rank;
              if (LOG_FINEST) msg += textRule.match+' text matched, rank increased by '+textRule.rank+' for '+name+'\n';
            }
          });
        }
        if (formTypeRule.button) {
          _.each(formTypeRule.button, function(buttonRule){
            setupRegex(buttonRule, ["match"]);
            _.each(buttonText, function(text){
              if (text.match(buttonRule.match)) {
                rank += buttonRule.rank;
                if (LOG_FINEST) msg += buttonRule.match+' button matched, rank increased by '+buttonRule.rank+' for '+name+'\n';
              }
            });
          });
        }
      }
      if (LOG_FINEST) msg += name+" = "+rank+"\n";
      if (rank < formTypeRule.min_rank || rank <= 0) {
        return;
      }
      if (rank > bestRank) {
        bestRank = rank;
        bestFormType = name;
      }
    });

    LOG_FINEST && ABINE_DNTME.log.finest("formAnalyzer:getFormType\n"+msg);

    return bestFormType;
  }

  function getExpiryMonth(fld) {
    if (fld.type == 'select') {
      var options = fld.element.options;
      var month = fld.element.selectedIndex;
      if (options.length > 12) {
        month--;
      }
      return (month-1).toString();
    } else {
      var month = parseInt($(fld.element).val());
      if (!isNaN(month)) {
        return month.toString();
      }
    }
    return "";
  }

  function getExpiryYear(fld) {
    var year = parseInt($(fld.element).val());
    if (isNaN(year)) return "";
    if (year < 100) {
      year = 2000 + year;
    }
    return year.toString();
  }

  function getExpiryMonthYear(fld) {
    var val = $(fld.element).val(), month = '', year = '';
    var match = val.match(/([0-9]+)[\-\/]([0-9]+)/);
    if (match) {
      if (match[1] > 12) {
        year = parseInt(match[1]);
        month = parseInt(match[2]);
      } else {
        year = parseInt(match[2]);
        month = parseInt(match[1]);
      }
      if (year < 100) {
        year = 2000 + year;
      }
    }
    return {month: month.toString(), year:year.toString()};
  }

  // Transform common rails-style user form field ids
  // ie. "user[email]" => "email"
  var unwrapRailsField = function(fieldId) {
    var userFieldMatch = fieldId.match(/^user\[(.*)\]$/);
    return (userFieldMatch) ? userFieldMatch[1] : fieldId;
  };

  var regExps = {
    nonAlphanumeric: /[^a-z0-9]+/gim,
    nonAlpha: /[^a-z]+/gim,
    spaces: /[\s]+/gim,
    startsWithDot: /^\./,
    trimSepartor: /(^\|\|)|(\|\|$)/g,
    textOrSelect: /text|select/i,
    // as per RFC 2822
    email: /^(?:[a-z0-9!#$%&'*+\/=?^_`{|}~\-]+(?:\.[a-z0-9!#$%&'*+\/=?^_`{|}~\-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9\-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9\-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9\-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/i
  };

  var AbineFormAnalyzer = function() {
  };

  AbineFormAnalyzer.prototype = {

    LOGIN_FORM: LOGIN_FORM,
    REGISTRATION_FORM: REGISTRATION_FORM,
    CHECKOUT_FORM: CHECKOUT_FORM,
    CHANGEPASSWORD_FORM: CHANGEPASSWORD_FORM,
    OTHER_FORM: OTHER_FORM,
    ADDRESS_FORM: ADDRESS_FORM,
    LOGIN_REGISTRATION_FORM: LOGIN_REGISTRATION_FORM,

    EMAIL_FIELD: EMAIL_FIELD,
    PASSWORD_FIELD: PASSWORD_FIELD,
    OLDPASSWORD_FIELD: OLDPASSWORD_FIELD,
    PHONE_FIELD: PHONE_FIELD,
    USERNAME_FIELD: USERNAME_FIELD,
    IDENTITY_FIELD: IDENTITY_FIELD,
    PAYMENT_FIELD: PAYMENT_FIELD,
    ADDRESS_FIELD: ADDRESS_FIELD,
    LOGIN_FIELD: LOGIN_FIELD,

    UNPROCESSABLE_TAGS: UNPROCESSABLE_TAGS,

    isVisible: function(node) {
      var visible = true;
      try {
        visible = $(node).is(":visible") && $(node).css("visibility") != 'hidden';
        if (visible) {
          // check inherited style
          var window = node.ownerDocument.defaultView;
          var styles = window.getComputedStyle(node);
          visible = styles['visibility'] != 'hidden';
        }
      } catch(e){}
      return visible;
    },

    isElementInViewport: function(el) {
      try {
        var document = el.ownerDocument;
        var window = document.defaultView;
        var rect = el.getBoundingClientRect();

        return rect.bottom > 0 &&
          rect.right > 0 &&
          rect.left < (window.innerWidth || document.documentElement.clientWidth) /*or $(window).width() */ &&
          rect.top < (window.innerHeight || document.documentElement.clientHeight) /*or $(window).height() */;
      } catch(e) {}
      return true;
    },

    // ### function getFormType(el)
    // - formEl - the DOM parent element of the form to analyze,
    //   usually a form tag, but not strictly
    // - returns - the type of form
    //
    // Determine the type of form, ie login, registration, etc.
    // Trivial implementation to get started:
    // 1 password field => login form.
    // 2 password fields or more than 2 text fields => registration form.
    // This needs to be improved.
    getFormType: function(formEl, mappedFields, mappedFormType) {
      LOG_FINEST && ABINE_DNTME.log.finest("formAnalyzer:getFormType enter");
      if (!formEl.getAttribute('abineGuid')) {
        formEl.setAttribute('abineGuid', createUUID());
      }

      if(mappedFormType){
        $(formEl).data("mmMappedFormType", mappedFormType);
      } else {
        mappedFormType = $(formEl).data("mmMappedFormType");
      }

      // exclude url parameters and host in url
      var pageUrl = ($(formEl).attr('page-url') || formEl.ownerDocument.URL || formEl.ownerDocument.location.href || "").replace(/\?.*/, '').replace(/^http[s]?:\/\/[^\/]*/, '');
      var actionUrl = (formEl.getAttribute('action') || "");

      // saw few javascript: urls in logs
      if (actionUrl.indexOf('javascript:') == 0) {
        actionUrl = "";
      } else {
        // retain only path in url
        actionUrl = actionUrl.replace(/\?.*/, '').replace(/^http[s]?:\/\/[^\/]*/, '')
      }

      var formType = null;
      if (!mappedFormType) {
        formType = $(formEl).data('mmFormType');
        if (!formType) {
          var fields = this.getFields(formEl, mappedFields);
          formType = getFormType(fields, formEl, pageUrl, actionUrl);
          $(formEl).data('mmFormType', formType);
        }
      } else {
        $(formEl).data('mmFormType', mappedFormType);
        formType = mappedFormType;
      }

      // cache signature for future getFormType calls
      $(formEl).data('mmFormType', formType);

      LOG_FINEST && ABINE_DNTME.log.finest("formAnalyzer:getFormType return: "+formType);
      return formType;
    },

    dumpForm: function(formEl, domain) {
      var fields = this.getFields(formEl);
      var formFields = '';
      _.each(fields, function(field) {
        var dataType = field.dataType || '-unknown-';
        formFields += '** '+field.signature+' ('+field.label+') => '+dataType+' ('+field.matchedLocale+')\n';
      });

      ABINE_DNTME.log.finest("\nForm Type: "+this.getFormType(formEl)+', '+domain+' ('+detectFormLanguage(formEl).join(',')+")\nFields:\n"+formFields);
    },

    _elementInDocument: function(element) {
      var doc = element.ownerDocument;
      while (element = element.parentNode) {
        if (element == doc) {
          return true;
        }
      }
      return false;
    },

    hasFormChanged: function(formEl, formStrategy) {
      if (!this._elementInDocument(formEl)) {
        return true;
      }
      var elements = [];
      var formNode = false;
      if (formEl.nodeName.toLowerCase() == 'form') {
        elements = formEl.elements;
        formNode = true;
      }
      if (elements.length == 0 && !formNode && formStrategy){
        // formless form, so search for nodes at given depth
        var fields = formStrategy.getFields();
        var fieldNames = _.keys(fields);
        if (fieldNames.length > 0) {
          var depth = $(formEl).data('abineMaxDepth') || 3;
          if (depth) {
            depth = parseInt(depth);
            var node = fields[fieldNames[0]].element;
            while (depth > 0 && node) {
              node = node.parentNode;
              depth--;
            }
            if (node) {
              elements = $(node).find('input, textarea, select').toArray();
              formStrategy.newRoot = node;
            }
          }
        }
      }
      if (elements.length == 0) {
        elements = $(formEl).find('input, textarea, select').toArray();
      }
      elements = _(elements).filter(function(el){
        return (el.nodeName||'').match(/input|textarea|select/i) && (formNode || !el.form);
      }, this);
      for (var i=0; i<elements.length; i++) {
        var el = elements[i];
        if (el.hasAttribute("type") && el.getAttribute("type").toLowerCase() in UNPROCESSABLE_TAGS) {
          continue; // do not process checkboxes, radios, or hidden fields
        }
        if (el.hasAttribute("readonly")) {
          continue; // ignore readonly fields
        }

        if (((el.id || el.name) && !$(el).data('mmProcessed')) || ($(el).data('mmWasReadOnly') && !el.hasAttribute("readonly"))) {
          return true;
        }
      }
      // check for any deleted fields
      var fields = this.getFields(formEl);
      for (var i in fields) {
        if (!this._elementInDocument(fields[i].element)) {
          return true;
        }
      }
      return false;
    },

    clearFormCache: function(formEl) {
      $(formEl).removeData('mmFormType');
      $(formEl).removeData('mmSignature');
      $(formEl).removeData('mmFields');
      $(formEl).removeData('mmFormType');
    },

    getFormSignature: function(domain, formEl){
      LOG_FINEST && ABINE_DNTME.log.finest("formAnalyzer:getFormSignature enter");
      var id, tag, element, fields = {}, position= 0, elements;

      var elements;
      var formNode = false;
      if (formEl.nodeName == 'FORM') {
        elements = formEl.elements;
        formNode = true;
      }
      if (!elements || elements.length == 0) {
        // not sure why "select" was not added here.  but changing it now will cause signature mismatch :(
        elements = $(formEl).find('input, textarea').toArray();
      }
      elements = _(elements).filter(function(el){
        return (el.nodeName||'').match(/input|textarea/i) && (formNode || !el.form);
      }, this);

      // check if this signature was already computed for this form
      var signature = $(formEl).data('mmSignature');
      if (signature) {
        // return from cache
        return signature;
      } else {
        signature = "";
      }

      signature += domain;

      var el_ids = [];

      for (var i=0; i<elements.length; i++) {
        element = elements[i];
        if (element.hasAttribute("type") && element.getAttribute("type").toLowerCase() in UNPROCESSABLE_TAGS) {
          continue; // do not process checkboxes, radios, or hidden fields
        }

        if(element.hasAttribute("readonly")){
          continue; // do not process READONLY elements
        }
        tag = element.nodeName.toLowerCase();
        id = element.name || element.id || element.getAttribute('data-id') || element.getAttribute('data-ng-model') || element.getAttribute('ng-model') || tag;

        if (fields[id]) {
          var uniqueId = 2;
          while(fields[id+uniqueId]){
            uniqueId++;
          }
          id += uniqueId;
        }

        el_ids.push(id);

        // Save the element to the return object
        fields[id] = 1;
      }

      el_ids.sort();

      for(var i=0;i<el_ids.length;i++){
        signature += el_ids[i] + "_";
      }

      signature = CryptoJS_SHA1(signature);

      // cache signature for future getFormSignature calls
      $(formEl).data('mmSignature', signature);

      LOG_FINEST && ABINE_DNTME.log.finest("formAnalyzer:getFormSignature return");
      return signature;

    },

    getLabel: function (labelMap, id, element, formEl) {
      var lbl = labelMap[id];
      if (!lbl && element.id) lbl = labelMap[element.id];
      if (!lbl && element.abineGuid) lbl = labelMap[element.abineGuid];
      if (!lbl && element.className) {
        // some sites are class-name of input field as "for" attribute of label
        _.find(element.className.split(' '), function (name) {
          if (labelMap[name]) {
            lbl = labelMap[name];
            return true;
          }
          return false;
        });
      }
      var labelTxt = lbl ? lbl.text : null;
      if (!labelTxt && element.hasAttribute("placeholder")) {
        labelTxt = strip(element.getAttribute("placeholder")); // use placeholders as alternate labels
      }
      if (!labelTxt && element.hasAttribute("title")) {
        try {
          labelTxt = strip(element.getAttribute("title"));
        } catch (e) {
        }
      }
      if (!labelTxt && element.hasAttribute("label")) { // worldtravelguide.net
        try {
          labelTxt = strip(element.getAttribute("label"));
        } catch (e) {
        }
      }
      // use accessibility label attributes
      if (!labelTxt && element.hasAttribute("aria-label")) {
        labelTxt = strip(element.getAttribute("aria-label"));
        if (labelTxt && LOG_FINEST) {
          ABINE_DNTME.log.finest("Got label using aria-label for " + id + " " + labelTxt);
        }
      }
      if (!labelTxt && element.hasAttribute("aria-labelledby")) {
        try {
          labelTxt = strip($(element.getAttribute("aria-labelledby"), this.document).text());
          if (labelTxt && LOG_FINEST) {
            ABINE_DNTME.log.finest("Got label using aria-labelledby for " + id + " " + labelTxt);
          }
        } catch (e) {
        }
      }

      // try to get preceding node text
      if (!labelTxt) {
        labelTxt = this.getPrecedingText(element, formEl);
        if (labelTxt && LOG_FINEST) {
          ABINE_DNTME.log.finest("Got label using preceding text for " + id + " " + labelTxt);
        }
      }

      if (labelTxt == "") {
        labelTxt = null;
      } // missing label should be null not empty string

      if (labelTxt) {
        // strip long words
        labelTxt = labelTxt.replace(/[^\s]{15,}/, '');
        // remove stop words
        labelTxt = strip(labelTxt.replace(LabelStopWordsRegex, ''));
        // retain first 6 words
        var labelParts = labelTxt.split(/[\s]+/);
        if (labelParts.length > 6) {
          labelTxt = labelParts.splice(0, 6).join(' ');
        }
        // truncate long strings
        labelTxt = labelTxt.substr(0, 200);
      }
      return labelTxt;
    },

    // ### function getFields(formEl)
    // - formEl - the DOM element of the form
    // - returns - an object describing each input and select field found in the form.
    //   the keys are the ids of each form field.
    //
    // This is a performance-critical method, as it is called for every form the user sees.
    // Take care in adding to and modifying it. Minimize function calls, especially DOM access.
    getFields: function(formEl, mappedFields) {
      LOG_FINEST && ABINE_DNTME.log.finest("formAnalyzer:getFields enter");
      var id, tag, firstPasswordFieldId, dataType, element, fieldType, logicalName,
        orderedFields=[],
        position= 0, passwordFields = 0, visiblePasswordFields = 0;

      var languages = detectFormLanguage(formEl);

      // check if this form was already processed
      var fields = $(formEl).data('mmFields');
      if (fields) {
        // return from cache
        LOG_FINEST && ABINE_DNTME.log.finest("formAnalyzer:getFields returning from cache return");
        return fields;
      } else {
        fields = {};
        // cache fields for future getFields calls
        $(formEl).data('mmFields', fields);
      }

      var elements;
      var formNode = false;
      if (formEl.nodeName.toLowerCase() == 'form') {
        elements = formEl.elements;
        formNode = true;
      }
      if (!formNode && formEl.getAttribute('abineForceFormless')) {
        formNode = true;
      }
      if (!elements || elements.length == 0) {
        elements = $(formEl).find('input, textarea, select').toArray();
      }

      var done = {}, ids = [];
      elements = _(elements).filter(function(el){
        if (!(el.nodeName||'').match(/input|textarea|select/i)) {
          return false;
        }
        if (!formNode && el.form) {
          // formEl is not FORM and element has a FORM.  so ignore it as it is part of another form inside a form-less form group.
          return false;
        }
        if (el.hasAttribute("type") && el.getAttribute("type").toLowerCase() in UNPROCESSABLE_TAGS) {
          return false; // do not process checkboxes, radios, or hidden fields
        }

        if (el.name == 'not-the-password') {
          // ignore fake password field on https://webbranch.uwcu.org/auth/login
          return false;
        }

        if (el.hasAttribute("readonly")) {
          $(el).data('mmWasReadOnly', 1);
          return false; // do not process READONLY elements
        }

        tag = el.nodeName.toLowerCase();
        id = el.name || el.id || el.getAttribute('data-id') || el.getAttribute('data-ng-model') || el.getAttribute('ng-model');
        $(el).data('abine-logical-name', id || el.className || "");
        if (!id) id = tag;

        if (done[id]) {
          var uniqueId = 2;
          while(done[id+uniqueId]){
            uniqueId++;
          }
          id +=uniqueId;
        }
        done[id]=1;
        $(el).data('abine-id', id);
        ids.push(id);
        return true;
      });

      var commonPrefix = getCommonPrefix(ids);
      if (commonPrefix) {
        LOG_FINEST && ABINE_DNTME.log.finest("** common prefix for this form "+commonPrefix);
      }

      var labelMap = this.getLabelMapFor(formEl); // builds a fast lookup for all element labels

      // Main processing loop iterates over the form's field elements
      for (var i=0; i<elements.length; i++) {
        element = elements[i];

        // mark the field as processed to detect any dynamically added fields.
        $(element).data('mmProcessed', 1);

        position += 1; // each field is given a logical position
        tag = element.nodeName.toLowerCase();
        id = $(element).data('abine-id');

        var labelTxt = this.getLabel(labelMap, id, element, formEl);

        LOG_DEBUG && ABINE_DNTME.log.debug("** processing id="+id+" lbl="+labelTxt+" tag="+tag);

        // Derive field type from tag
        if (tag == "select") {
          fieldType = "select";
        } else if (tag == "textarea") {
          fieldType = "textarea";
        } else if (tag == "input") {
          fieldType =(element.getAttribute('type')) ? element.getAttribute('type').toLowerCase() : "text";
          // some sites use invalid field type,  default to "text" in such cases
          if (!(fieldType in {password:1,text:1,tel:1,email:1})) {
            fieldType = 'text';
          }
        }
        // Run all of the regexp rules to derive dataType
        var elementId = element.id;
        logicalName = $(element).data('abine-logical-name');
        var phoneTypeField = false, emailTypeField = false;
        if (fieldType == 'tel') {
          phoneTypeField = true;
          fieldType = "text";
        } else if (fieldType == "email") {
          emailTypeField = true;
        }


        var matched_locale = null;
        dataType = this.getDataTypeByAutocompleteAttribute(element);
        if (dataType == '/nameperson/friendly') {
          // data type detected by autocomplete attribute is username, make sure regex rules do not say that it is email field.
          // sites like target.com use bad autocomplete attribute.
          var regexDataType = this.getDataTypeByRegExRules(languages, logicalName, labelTxt, fieldType, element.innerHTML);
          if (regexDataType == '/contact/email') {
            dataType = regexDataType;
          }
        }
        if (!dataType) {
          if (commonPrefix) {
            dataType = this.getDataTypeByRegExRules(languages, logicalName.replace(commonPrefix, ''), labelTxt, fieldType, element.innerHTML);
            dataType && LOG_FINEST && ABINE_DNTME.log.finest("** data type found without common prefix "+commonPrefix);
          }
          if (!dataType) {
            dataType = this.getDataTypeByRegExRules(languages, logicalName, labelTxt, fieldType, element.innerHTML);
          }
          if (!dataType && element.name && elementId && elementId != logicalName) {
            elementId = elementId?unwrapRailsField(elementId):null;
            dataType = this.getDataTypeByRegExRules(languages, elementId, "", fieldType, "");
          }

          // max 2 password fields by using fieldType.
          // otherwise, usps.com register form detected with 6 password fields (includes security answer)
          if (fieldType == "password" && dataType != "/passwordold" && dataType != "/password" &&
            (!dataType || dataType.indexOf('/financial') == -1) && visiblePasswordFields < 2) {
            dataType = "/password";
            LOG_FINEST && ABINE_DNTME.log.finest("set type for "+logicalName+" to "+dataType);
          }

          if (dataType != "/passwordold" && dataType && dataType.match("/password")) {
            passwordFields++;
            if (this.isVisible(element)) {
              visiblePasswordFields++;
            }
            if(!firstPasswordFieldId){
              firstPasswordFieldId = id;
            }
          }

          // we cant do this as most sites use "TEL" for any numeric input like Quantity
          // if (phoneTypeField && !dataType) {
          //   dataType = '/contact/phone';
          //   LOG_FINEST && ABINE_DNTME.log.finest("set type for "+logicalName+" to "+dataType);
          // }

          if (!dataType && emailTypeField) {
            dataType = '/contact/email';
            LOG_FINEST && ABINE_DNTME.log.finest("set type for "+logicalName+" to "+dataType);
          } else {
            matched_locale = this.ruleMatchedLocale;
            this.ruleMatchedLocale = null;
          }
        }

        // Save the element to the return object
        fields[id] = {
          position: position,
          element: element,
          fieldType: fieldType,
          id: element.id,
          className: element.className,
          name: element.name,
          dataType: dataType,
          label: labelTxt,
          signature: id,
          matchedLocale: matched_locale,
          maxlength: element.hasAttribute("maxlength") ? parseInt(element.getAttribute("maxlength")) : null,
          size: element.size? element.size:null
        };
        orderedFields[i] = fields[id];
        orderedFields[i].key = id;
      } // end main loop

      if (passwordFields == 3) {
        fields[firstPasswordFieldId].dataType = "/passwordold";
        LOG_FINEST && ABINE_DNTME.log.finest("set type for "+fields[firstPasswordFieldId].signature+" to /passwordold");
      }

      AbinePhoneHeuristics.apply(fields, orderedFields);
      AbinePaymentHeuristics.apply(fields, orderedFields);
      AbineAddressHeuristics.apply(fields, orderedFields);

      _.each(fields, function(field, id){
        var element = field.element;
        $(element).data('mmDetectType', field.dataType);

        if (mappedFields && id in mappedFields) {
          LOG_FINEST && ABINE_DNTME.log.finest("changed type for "+id+" to "+mappedFields[id]);
          field.dataType = mappedFields[id];
          $(element).data('mmMappedType', field.dataType);
        }

        // override datatype because user used context menu to fill
        if(element.hasAttribute("abinedatatype")){
          field.dataType = element.getAttribute("abinedatatype");
        }
      });

      AbineAddressHeuristics.groupAddressFields(fields, orderedFields);

      LOG_FINEST && ABINE_DNTME.log.finest("formAnalyzer:getFields return");
      return fields;
    },

    getProtocol: function(formEl, url) {
      var actionUrl = formEl.getAttribute('action');
      if (!actionUrl || actionUrl.indexOf('http') !== 0) {
        actionUrl = url;
      }
      return actionUrl.replace(/:.*/, '');
    },

    getPageDomain: function(url) {
      return AbineURL.getHostname(url);
    },

    getFormDomain: function(formEl, pageUrl) {
      var actionUrl = formEl.getAttribute('action');
      if (actionUrl)
        if (actionUrl.match(/(http[s]?:)?\/\//i))
          return AbineURL.getHostname(actionUrl);
        else
          return AbineURL.getHostname(pageUrl);
      return '';
    },

    getDetectedLocales: function(formEl) {
      return detectFormLanguage(formEl);
    },

    // ### function getFormData(formEl)
    // - formEl - the DOM element of the form
    // - pageUrl - url of the current page in case form doesn't have action url
    // - returns - an object describing data that would go out on submit.
    getFormData: function(formEl, pageUrl) {
      LOG_FINEST && ABINE_DNTME.log.finest("formAnalyzer:getFormData enter");
      var actionUrl = formEl.hasAttribute("action")?formEl.getAttribute("action"):pageUrl;
      var fields = this.getFields(formEl);
      var formType = this.getFormType(formEl);

      var formSignature = this.getFormSignature(pageUrl, formEl);
      var submitData = {url: actionUrl, type: formType, fields: {}, pageUrl: pageUrl, formSignature: formSignature, allFields: []};
      if($(formEl).attr("maskMeFilled") === "yes"){
        submitData.origin = "MaskMe";
      }

      submitData.id = formEl.getAttribute('abineGuid');
      submitData.accountId = formEl.getAttribute('abineAccountId');
      submitData.cardId = formEl.getAttribute('abineCardId');
      submitData.protocol = this.getProtocol(formEl, pageUrl);
      submitData.domain = AbineURL.getTLD(pageUrl);
      submitData.page_host = this.getPageDomain(pageUrl);
      submitData.form_host = this.getFormDomain(formEl, pageUrl);
      submitData.mappedFormType = $(formEl).data("mmMappedFormType");
      submitData.local_mapping = !!$(formEl).data("mmLocal");
      submitData.detectedLocales = detectFormLanguage(formEl).join(',');
      submitData.tagName = formEl.nodeName.toLowerCase();
      submitData.formlessDepth = $(formEl).data('abineMaxDepth');

      submitData.language = null;
      try {
        submitData.language = formEl.ownerDocument.defaultView.navigator.language
      } catch(e) {}

      var self = this;

      _.each(fields, function(field) {
        var el$ = $(field.element);
        submitData.allFields.push({
          name: field.name,
          className: field.className,
          fieldType: field.fieldType,
          id: field.id,
          detectedType: el$.data("mmDetectType"),
          mappedType: el$.data("mmMappedType"),
          key: field.key,
          label: field.label,
          maxlength: field.maxlength,
          matchedLocale: field.matchedLocale,
          position: field.position,
          signature: field.signature,
          size: field.size,
          hidden: !self.isVisible(field.element),
          dataType: field.dataType,
          value: el$.val()
        });
        if (!field.dataType) return;
        submitData.fields[field.dataType] = field.signature;
        // do not capture values of these buttons
        if ('submit.image.button'.indexOf(field.fieldType) != -1) { return; }
        // consider only checked checkbox/radio
        if ('checkbox.radio'.indexOf(field.fieldType) != -1) {
          if (!field.element.checked) { return; }
        }
        var fieldValueOrigin = el$.attr('abineOrigin');
        if (field.dataType.indexOf('email') != -1) {
          // email not already set OR it is a generated email
          if (!submitData.email || submitData.email.length == 0 || fieldValueOrigin == 'disposable') {
            submitData.email = el$.val() || "";
            if (fieldValueOrigin == 'disposable') {
              submitData.generated_email = true;
            }
          }
        } else if (field.dataType.indexOf('passwordold') != -1) {
          // password not already set OR it is a generated password
          if (!submitData.oldpassword || submitData.oldpassword.length == 0) {
            submitData.oldpassword = el$.val() || "";
          }
        } else if (field.dataType.indexOf('password') != -1) {
          // password not already set OR it is a generated password
          if (!submitData.password || submitData.password.length == 0 || fieldValueOrigin == 'strong') {
            submitData.password = el$.val() || "";
            if (fieldValueOrigin == 'strong') {
              submitData.generated_password = true;
            }
          }
        } else if (field.dataType.indexOf('friendly') != -1) {
          if (!submitData.username || submitData.username.length == 0)
            submitData.username = el$.val() || "";
        } else if (field.dataType == '/financial/creditcard/number') {
          if (!submitData.number || submitData.number.length == 0)
            submitData.number = (el$.val() || "").replace(/[^0-9]+/g, '');
        } else if (field.dataType == '/financial/creditcard/number/part1') {
          if (!submitData.number || submitData.number.length == 0)
            submitData.number = el$.val() || "";
        } else if (field.dataType == '/financial/creditcard/number/part2') {
          if (submitData.number && submitData.number.length == 4)
            submitData.number += (el$.val() || "");
        } else if (field.dataType == '/financial/creditcard/number/part3') {
          if (submitData.number && submitData.number.length == 8)
            submitData.number += (el$.val() || "");
        } else if (field.dataType == '/financial/creditcard/number/part4') {
          if (submitData.number && submitData.number.length == 12)
            submitData.number += (el$.val() || "");
        } else if (field.dataType == '/financial/creditcard/issuedto') {
          if (!submitData.issuedto || submitData.issuedto.length == 0)
            submitData.issuedto = el$.val();
        } else if (field.dataType == '/financial/creditcard/verification') {
          if (!submitData.cvc || submitData.cvc.length == 0)
            submitData.cvc = el$.val();
        } else if (field.dataType == '/financial/creditcard/expirymonth') {
          if (!submitData.expiry_month || submitData.expiry_month.length == 0)
            submitData.expiry_month = getExpiryMonth(field);
        } else if (field.dataType.indexOf('/financial/creditcard/expiryyear') != -1) {
          if (!submitData.expiry_year || submitData.expiry_year.length == 0)
            submitData.expiry_year = getExpiryYear(field);
        } else if (field.dataType.indexOf('/financial/creditcard/expiry') != -1) {
          if (!submitData.expiry_month && !submitData.expiry_year) {
            var dt = getExpiryMonthYear(field);
            submitData.expiry_month = dt.month;
            submitData.expiry_year = dt.year;
          }
        }
      });
      try{LOG_FINEST && ABINE_DNTME.log.finest("formAnalyzer:getFormData submit data: " + JSON.stringify(submitData));}catch(e){LOG_FINEST && ABINE_DNTME.log.finest('could not stringify submitData');}
      LOG_FINEST && ABINE_DNTME.log.finest("formAnalyzer:getFormData return");
      return submitData;
    },

    // ### function getFieldType(fieldEl)
    // - fieldEl - the DOM element of a form field
    // - returns - the logical type of the field,
    //
    // Returns one of: [select, textarea, text, password, email, checkbox, radio, integer]
    getFieldType: function(fieldEl) {
      LOG_FINEST && ABINE_DNTME.log.finest("formAnalyzer:getFieldType enter");
      var tag = fieldEl.nodeName.toLowerCase();
      if (tag == "select") {
        LOG_FINEST && ABINE_DNTME.log.finest("formAnalyzer:getFieldType return");
        return "select";
      }
      if (tag == "textarea") {
        LOG_FINEST && ABINE_DNTME.log.finest("formAnalyzer:getFieldType return");
        return "textarea";
      }
      if (tag == "input") {
        var type = fieldEl.getAttribute('type');
        LOG_FINEST && ABINE_DNTME.log.finest("formAnalyzer:getFieldType return");
        return (type) ? type.toLowerCase() : "text";
      }
    },

    // ### function getPrecedingText(element, formEl)
    // - returns text preceding given element
    getPrecedingText: function(element, formEl) {
      var text = null, depth = 4, node = element;
      function getNextNode(node, type) {
        node = node[type];
        if (node) {
          if ($(node).data('abine-id')) {
            // we have reached another field. stop.
            return null;
          }
          var els = $(element.nodeName+', input, select, textarea', node);
          if (els.length > 0 && els[0] != element) {
            // we have reached another field. stop.
            return null;
          }
        }
        return node;
      }
      while (node && !text && node != formEl && depth > 0) {
        while (!text && node && node.previousSibling) {
          node = getNextNode(node, 'previousSibling');
          if (node && !(node.nodeName||"").match(/script|style/i)) {
            text = strip($(node).text());
          }
        }
        if (!text && node) {
          node = getNextNode(node, 'parentNode');
        }
        depth--;
      }
      return text;
    },

    // ### function getLabelMapFor(formEl)
    // - returns a map of labels whos keys are the label for ids
    getLabelMapFor: function(formEl) {
      LOG_FINEST && ABINE_DNTME.log.finest("formAnalyzer:getLabelMapFor enter");
      var formField = /input|textarea|select/i;
      var map = {}, labelFor, field;
      $(formEl).find('label').each(function(i, label) {
        labelFor = strip(label.getAttribute('for'));
        var labelText = strip($(label).text() || label.name || label.id || "");
        if (labelFor) {
          if (!map[labelFor]) {
            map[labelFor] = {
              element: label,
              text: labelText
            };
          }
          // check if that element is present
          try {
            if ($('[name=\"'+labelFor+'\"], [id=\"'+labelFor+'\"], .'+labelFor, formEl).length > 0) {
              return;
            }
          } catch(e) {}
        }
        // check if input field is inside <label> tag
        field = $('input', label);
        if (field.length > 0) {
          field = field.get(0);
          map[$(field).data('abine-id') || field.id || field.name] = {
            element: label,
            text: labelText
          };
          return;
        }

        // check if next sibling is an form field.  many sites simply use LABEL without FOR attribute
        // majority of the cases these labels had form field as next sibling.
        var sibling = label.nextSibling;
        if (!sibling) sibling = label.parentNode.nextSibling;
        while (sibling && (sibling.nodeType != 1 || sibling.nodeName.match(/br/i))) {
          sibling = sibling.nextSibling;
        }
        // use first child of next sibling if its form field
        if (sibling && !sibling.nodeName.match(formField) && sibling.firstChild) {
          sibling = sibling.firstChild;
          while (sibling && sibling.nodeType != 1) {
            sibling = sibling.nextSibling;
          }
        }
        if (sibling && !sibling.nodeName.match(formField)) {
          // check if previous sibling is a form field.  (eg: proflowers.com)
          sibling = label.previousSibling;
          if (!sibling) sibling = label.parentNode.previousSibling;
          while (sibling && sibling.nodeType != 1) {
            sibling = sibling.previousSibling;
          }
        }
        if (sibling && sibling.nodeName.match(formField)) {
          var id = ($(sibling).data('abine-id') || sibling.id || sibling.name);
          if (!id) {
            id = sibling.abineGuid = Math.random()+'';
          }
          map[id] = {
            element: label,
            text: labelText
          };
        }
      });
      LOG_FINEST && ABINE_DNTME.log.finest("formAnalyzer:getLabelMapFor return");
      return map;
    },

    // ### function getLabelFor(fieldEl, formEl)
    // - fieldEl - the DOM element of the field
    // - formEl - the DOM element of the parent form
    // - returns - the DOM element of the label for the given fieldEl, or null
    getLabelFor: function(fieldEl, formEl) {
      LOG_FINEST && ABINE_DNTME.log.finest("formAnalyzer:getLabel enter");
      var id = fieldEl.id || fieldEl.name;
      if (!id) { return null; }
      LOG_FINEST && ABINE_DNTME.log.finest("formAnalyzer:getLabel return");
      return $(formEl).find('label[for="'+id+'"]').get(0);
    },


    getDataTypeByAutocompleteAttribute: function(element) {
      LOG_FINEST && ABINE_DNTME.log.finest("formAnalyzer:getDataTypeByAutocompleteAttribute enter");
      var autoComplete = element.getAttribute('oldautocomplete') || element.getAttribute('autocomplete') || element.getAttribute("x-autocomplete");
      if (!autoComplete) {
        LOG_FINEST && ABINE_DNTME.log.finest("formAnalyzer:getDataTypeByAutocompleteAttribute return (no autocomplete attribute)");
        return null;
      }
      var tokens = autoComplete.toLowerCase().split(' ');
      var index = tokens.length - 1, categoryContact = false, contact = null, mode = null, section = null;
      var token = tokens[index];
      var rule = AbineFormRules.AutoCompleteRules[token];
      if (!rule || rule.category == 'off' || rule.category == 'automatic') return null;

      var dataType = rule.type;

      if (rule.category == 'contact') {
        categoryContact = true;
      }

      // contact category cannot have more than 4 tokens
      if (categoryContact && tokens.length > 4) {
        LOG_FINEST && ABINE_DNTME.log.finest("formAnalyzer:getDataTypeByAutocompleteAttribute return (contact category cannot have more than 4 tokens)");
        return null;
      }
      // normal category cannot have more than 3 tokens
      if (!categoryContact && tokens.length > 3) {
        LOG_FINEST && ABINE_DNTME.log.finest("formAnalyzer:getDataTypeByAutocompleteAttribute return (normal category cannot have more than 3 tokens)");
        return null;
      }
      index--;

      if (index >= 0) {
        token = tokens[index];
        if (token in {home:1, work:1, mobile:1, fax:1, pager:1}) {
          contact = token;
          index--;
        }
      }

      if (index >= 0) {
        token = tokens[index];
        if (token in {shipping:1, billing:1}) {
          mode = token;
          index--;
        }
      }

      // section not used now
      if (index >= 0) {
        token = tokens[index];
        if (token.indexOf('section-') == 0) {
          section = token;
          index--;
        }
      }

      // we use shipping/billing only for address
      if (mode && dataType.match(/postal|city|state|country/i)) {
        dataType += '/'+mode;
      }

      // we have contact type only for phone
      if (contact && dataType.indexOf('phone') != -1) {
        dataType += '/'+contact;
      }

      LOG_FINEST && ABINE_DNTME.log.finest("** type detected using autocomplete attribute for "+(element.name||element.id)+" is "+dataType);
      return dataType;
    },

    //
    // Reduce/simplify/clarify this method? It scares me...
    //

    // ### function getDataTypeByRegExRules(name, label, fieldType)
    // - name - the logical field name (name or id)
    // - label - the logical label (text or id)
    // - fieldType - the logical field type (select, text, etc.)
    //
    // Returns the most likely data type for the given field qualities
    getDataTypeByRegExRules: function(languages, name, label, fieldType, innerHTML) {
      LOG_FINEST && ABINE_DNTME.log.finest("formAnalyzer:getDataTypeByRegExRules enter");
      var nameNotEmpty = (name && name.length > 0);
      var labelNotEmpty = (label && label.length > 0);
      if (fieldType == 'textarea' || fieldType == 'email') {
        fieldType = 'text';
      }
      var filteredTypes = [];
      var matchReason = [];
      var self = this;
      _.find(languages, _.bind(function(lang){
        filteredTypes = [];
        matchReason = [];
        var rules = getRulesForLanguage(lang, fieldType);
        if (!rules) {
          LOG_FINEST && ABINE_DNTME.log.finest("formAnalyzer:getDataTypeByRegExRules return (no rules) - "+lang+" "+fieldType);
          return false;
        } else {
          LOG_FINEST && ABINE_DNTME.log.finest("** formAnalyzer:getDataTypeByRegExRules checking with '"+lang+"' rules");
        }
        self.ruleMatchedLocale = lang;
        for (var i = 0; i<rules.length; i++) {
          var rule = rules[i];
          setupRegex(rule, ["name", "label", "negative", "options", "negative_options"]);
          var hasLabelRule = typeof(rule['label']) != 'undefined';
          var hasNameRule = typeof(rule['name']) != 'undefined';

          var reason = null, labelMatched = false, nameMatched = false;
          if (labelNotEmpty && hasLabelRule && label.match(rule.label)) {
            if (LOG_FINEST) {
              reason = label+" label matched label rule: "+rule.label;
            }
            labelMatched = true;
          } else if (labelNotEmpty && !hasLabelRule && hasNameRule && label.match(rule.name)) {
            if (LOG_FINEST) {
              reason = label+" label matched name rule: "+rule.name;
            }
            labelMatched = true;
          } else if (nameNotEmpty && hasNameRule && name.match(rule.name)) {
            if (LOG_FINEST) {
              reason = name+" name matched name rule: "+rule.name;
            }
            nameMatched = true;
          } else {
            var alternateName = nameNotEmpty ? name.replace(regExps.nonAlphanumeric, '') : null;
            var alternateLabel = labelNotEmpty ? label.replace(regExps.nonAlphanumeric, '') : null;
            if (alternateName == name) alternateName = null;
            if (alternateLabel == label) alternateLabel = null;
            if (alternateLabel && !hasLabelRule && hasNameRule && alternateLabel.match(rule.name)) {
              if (LOG_FINEST) {
                reason = alternateLabel+" alternate label matched name rule: "+rule.name;
              }
              labelMatched = true;
            } else if (alternateLabel && hasLabelRule && alternateLabel.match(rule.label)) {
              if (LOG_FINEST) {
                reason = alternateLabel+" alternate label matched label rule: "+rule.label;
              }
              labelMatched = true;
            } else if (alternateName && hasNameRule && alternateName.match(rule.name)) {
              if (LOG_FINEST) {
                reason = alternateName+" alternate name matched name rule: "+rule.name;
              }
              nameMatched = true;
            }
          }
          if (rule.negative) {
            if (nameMatched && (name && name.match(rule.negative))) {
              continue;
            }
            if (labelMatched && (label && label.match(rule.negative))) {
              continue;
            }
            if (rule.any_negative && (nameMatched || labelMatched) && ((name && name.match(rule.negative) || (label && label.match(rule.negative))))) {
              continue;
            }
          }
          if (nameMatched || labelMatched) {
            if (rule.options && innerHTML && !innerHTML.match(rule.options)) {
              continue;
            }
            if (LOG_FINEST) {
              matchReason.push(reason);
            }
            filteredTypes.push(rule.data_type);
            break;
          }
          // for credit card type, we can even match the data type using options inside select box.
          if (rule.options && innerHTML && innerHTML.match(rule.options) && (!rule.negative_options || !innerHTML.match(rule.negative_options))) {
            if (LOG_FINEST) {
              matchReason.push("matched by options rule "+rule.options);
            }
            filteredTypes.push(rule.data_type);
            break;
          }
        }
        return (filteredTypes.length > 0);
      }, this));

      // find best option (for now use first)
      if (filteredTypes.length == 1){
        if (LOG_FINEST) {
          ABINE_DNTME.log.finest("** "+filteredTypes[0]+"  => "+matchReason[0]);
        }
        return filteredTypes[0];
      } else if(filteredTypes.length > 1) {

        var emailType = _(filteredTypes).find(function(type){
          if(type == "/contact/email"){
            if (LOG_FINEST) {
              ABINE_DNTME.log.finest("** "+filteredTypes[0]+"  => "+matchReason[0]);
            }
            return true;
          }
        });

        if (emailType) {
          return emailType;
        } else {
          if (LOG_FINEST) {
            ABINE_DNTME.log.finest("** "+filteredTypes[0]+"  => "+matchReason[0]);
          }
          return filteredTypes[0];
        }
      }
      self.ruleMatchedLocale = null;
      LOG_FINEST && ABINE_DNTME.log.finest("formAnalyzer:getDataTypeByRegExRules return (no match for "+name+")");
      return null;
    },

    findReplacementForm: function(formEl, domain) {
      var forms = this.getAllForms(formEl.ownerDocument, domain);
      var elements = $('input,select', formEl);
      var form = _.find(forms, function(frm){
        var els = $('input,select', frm);
        // find form that has same elements as old form
        if (els.length != elements.length) {
          return false;
        }
        for (var i=0;i<els.length;i++) {
          if (elements[i].name) {
            if (els[i].name != elements[i].name) {
              return false;
            }
          }
          if (elements[i].id) {
           if (els[i].id != elements[i].id) {
             return false;
           }
          }
        }
        return true;
      });
      return form;
    },

    getAllForms: function(doc, domain) {
      var allForms = doc.getElementsByTagName('form');
      var formElements = [];
      // consider only forms with at least one text field or one password field.
      _.each(allForms, function(form){
        // skip already processed forms
        if ($(form).data('mmFormType')) return;

        if (form.elements.length > 100) {
          LOG_FINEST && ABINE_DNTME.log.finest("+++ skipping form with " + form.elements.length + ' fields');
          return;
        }
        var numFields = $(':text, :password, input[type="tel"], input[type="number"], input[type="email"]', form).length;
        if (numFields > 0) {
          if (numFields >= 2) {
            // more text/password fields to the beginning of array
            formElements.unshift(form);
          } else {
            formElements.push(form);
          }
        }
      });

      // process only first 10 forms to avoid crashing on pages like http://www.landmarkonthenet.com/
      formElements = formElements.slice(0, 10);

      var treatFormAsFieldGroups = ["nordstrom.com","proflowers.com"];

      if (treatFormAsFieldGroups.indexOf(domain) != -1) {
        formElements = this.getFieldGroupsWithoutForm(doc, domain, true);
        _.each(formElements, function(el){el.setAttribute('abineForceFormless', true);})
      } else {
        formElements = formElements.concat(this.getFieldGroupsWithoutForm(doc, domain));
      }

      // remove already processed forms
      formElements = _(formElements).filter(function(form){
        return !$(form).data('mmFormType');
      });

      return formElements;
    },

    // returns groups of fields without a enclosing FORM tag
    getFieldGroupsWithoutForm: function(doc, domain, ignoreFormElsHere) {
      var desiredTypes = {text: 1, password: 1, email: 1, tel: 1};
      var inputElements = doc.getElementsByTagName('input');

      var maxDepth = 3;
      if (domain in AbineFormRules.FormlessDomains) {
        maxDepth = AbineFormRules.FormlessDomains[domain];
      }

      // get fields without forms
      var fields = _(inputElements).filter(function(field){
        if (field.form && !ignoreFormElsHere) return false;
        var type = !field.type?'text':field.type.toLowerCase();
        return (type in desiredTypes);
      }, this);

      var forms = [];

      var fieldParents = [];
      var fieldGroups = [1], groupLCA = {};

      // get parents of all input fields to max depth of 3
      // also compute least-common-ancestor of adjacent fields.
      for (var i=0;i<fields.length;i++) {
        var field = fields[i].parentNode;
        var parents = {}, depth= 0, matchedGroup = false;
        while (field && depth < maxDepth) {
          field.id = field.id || 'abId'+Math.random();
          if (i > 0 && field.id in fieldParents[i-1]) {
            fieldGroups[i] = fieldGroups[i-1];
            parents = fieldParents[i-1];
            if (!(fieldGroups[i-1] in groupLCA) || !$.contains(groupLCA[fieldGroups[i-1]].lca, field)) {
              groupLCA[fieldGroups[i-1]] = {lca: field, depth: depth};
            }
            matchedGroup = true;
            break;
          }
          parents[field.id] = 1;
          field = field.parentNode;
          depth++;
        }
        fieldParents.push(parents);
        if (!matchedGroup) {
          if (i > 0 && $.contains(groupLCA[fieldGroups[i - 1]].lca, fields[i])) {
            fieldGroups[i] = fieldGroups[i - 1];
          } else {
            fieldGroups[i] = i == 0 ? 1 : fieldGroups[i - 1] + 1;
            groupLCA[fieldGroups[i]] = {lca: fields[i].parentNode, depth: 0};
          }
        }
      }
      var doneForms = {};

      for (var group in groupLCA) {
        if (groupLCA[group].lca.id in doneForms) continue;
        doneForms[groupLCA[group].lca.id] = 1;

        // if(groupLCA[group].lca.getElementsByTagName("form").length == 0){
        $(groupLCA[group].lca).data('abineMaxDepth', maxDepth);
          forms.push(groupLCA[group].lca);
        // }
      }

      if (LOG_DEBUG && forms.length > 0) {
        ABINE_DNTME.log.debug("** detected "+forms.length+" groups of fields (max-depth "+maxDepth+") without forms for domain "+domain);
      }

      return forms;
    }

  };

  return AbineFormAnalyzer;

}).apply(this, []);
;

  FormAnalyzer.prototype.updateRules = function(rules) {
    AbineFormRules = rules;
  };

  FormAnalyzer.prototype.getKeySimulationRegex = function() {
    if (AbineFormRules.KeySimulations && AbineFormRules.KeySimulations.source) {
      return AbineFormRules.KeySimulations;
    }
    if (AbineFormRules.KeySimulations) {
      AbineFormRules.KeySimulations = new RegExp(AbineFormRules.KeySimulations, "i");
    } else {
      AbineFormRules.KeySimulations = /^$/i
    }
    return AbineFormRules.KeySimulations;
  };

  return new FormAnalyzer();

});
ABINE_DNTME.define("abine/formObserver",
    ["documentcloud/underscore", "jquery", "abine/core", "abine/contentMessenger",
      "abine/formAnalyzer", 'abine/timer', "abine/assets", "abine/responseCodes"],
    function(_, $, abineCore, abineContentMessenger, abineFormAnalyzer, timer, assets, responseCodes) {

    var NO_FIELD_ICON = 1;
    var STATIC_FIELD_ICON = 2;
    var DYNAMIC_FIELD_ICON = 3;

  // These are the field types that the observer cares about
  // map from dataType to field type
  // there can be more than one dataType per field type
  var hookableDataTypes = {
      "/contact/email": abineFormAnalyzer.EMAIL_FIELD,
      "/contact/email/login": abineFormAnalyzer.LOGIN_FIELD,

      "/nameperson/full": abineFormAnalyzer.IDENTITY_FIELD,
      "/nameperson/first": abineFormAnalyzer.IDENTITY_FIELD,
      "/nameperson/middle": abineFormAnalyzer.IDENTITY_FIELD,
      "/nameperson/last": abineFormAnalyzer.IDENTITY_FIELD,

      "/password": abineFormAnalyzer.PASSWORD_FIELD,
      "/passwordold": abineFormAnalyzer.OLDPASSWORD_FIELD,
      "/password/login": abineFormAnalyzer.LOGIN_FIELD,

      "/contact/phone": abineFormAnalyzer.PHONE_FIELD,
      "/contact/phone/countrycode": abineFormAnalyzer.PHONE_FIELD,
      "/contact/phone/areacode": abineFormAnalyzer.PHONE_FIELD,
      "/contact/phone/exchange": abineFormAnalyzer.PHONE_FIELD,
      "/contact/phone/local": abineFormAnalyzer.PHONE_FIELD,
      "/contact/phone/number": abineFormAnalyzer.PHONE_FIELD,
      "/contact/phone/extension": abineFormAnalyzer.PHONE_FIELD,

      "/nameperson/friendly": abineFormAnalyzer.USERNAME_FIELD,

      "/financial/creditcard/number": abineFormAnalyzer.PAYMENT_FIELD,
      "/financial/creditcard/number/part1": abineFormAnalyzer.PAYMENT_FIELD,
      "/financial/creditcard/number/part2": abineFormAnalyzer.PAYMENT_FIELD,
      "/financial/creditcard/number/part3": abineFormAnalyzer.PAYMENT_FIELD,
      "/financial/creditcard/number/part4": abineFormAnalyzer.PAYMENT_FIELD,
      "/financial/creditcard/number/amex/part1": abineFormAnalyzer.PAYMENT_FIELD,
      "/financial/creditcard/number/amex/part2": abineFormAnalyzer.PAYMENT_FIELD,
      "/financial/creditcard/number/amex/part3": abineFormAnalyzer.PAYMENT_FIELD,

      "/financial/creditcard/type": abineFormAnalyzer.PAYMENT_FIELD,
      "/financial/creditcard/issuedto": abineFormAnalyzer.PAYMENT_FIELD,
      "/financial/creditcard/expiry": abineFormAnalyzer.PAYMENT_FIELD,
      "/financial/creditcard/expiry2": abineFormAnalyzer.PAYMENT_FIELD,
      "/financial/creditcard/expiryyear": abineFormAnalyzer.PAYMENT_FIELD,
      "/financial/creditcard/expiryyear2": abineFormAnalyzer.PAYMENT_FIELD,
      "/financial/creditcard/expirymonth": abineFormAnalyzer.PAYMENT_FIELD,
      "/financial/creditcard/verification": abineFormAnalyzer.PAYMENT_FIELD,

      "/contact/postaladdress": abineFormAnalyzer.ADDRESS_FIELD,
      "/contact/postaladdressAdditional": abineFormAnalyzer.ADDRESS_FIELD,
      "/contact/state": abineFormAnalyzer.ADDRESS_FIELD,
      "/contact/state/code": abineFormAnalyzer.ADDRESS_FIELD,
      "/contact/city": abineFormAnalyzer.ADDRESS_FIELD,
      "/contact/country": abineFormAnalyzer.ADDRESS_FIELD,
      "/contact/postalcode": abineFormAnalyzer.ADDRESS_FIELD
  };

  // These are the event types that the observer cares about
  var hookableFieldEvents = ['focus.mmfo', 'click.mmfo', 'blur.mmfo', 'change.mmfo', 'showpanel.mmfo'];

  function getHookableDataType(type) {
    if (!type) return null;
    if (type in hookableDataTypes) return hookableDataTypes[type];
    type = type.substr(0, type.lastIndexOf('/'));
    if (type in hookableDataTypes) return hookableDataTypes[type];
    return null;
  }

  function isHookableField(field) {
    var dataType = field.dataType;
    if (!dataType) return false;
    if (dataType in hookableDataTypes)
      return true;
    dataType = dataType.substr(0, dataType.lastIndexOf('/'));
    return dataType in hookableDataTypes;
  }

  var FormObserver = abineCore.BaseClass.extend({

    // ### function initialize(options)
    // - el - the form element to observe
    // - messenger - a content messenger to use for events, otherwise sends from self
    // - returns - a FormObserver
    //
    // Observes a form, deduces its type, and reports all user activity
    // Triggers events:
    // - loginFormPresent
    // - registrationFormPresent
    // - emailFieldChanged, phoneFieldChanged, passwordFieldChanged
    // - formSubmit (unimplemented)
    initialize: function(options) {
      LOG_FINEST && ABINE_DNTME.log.finest("formObserver:initialize enter");
      this.id = _.uniqueId();
      this.el = options.el;
      this.mappedFields = options.mappedFields;
      this.messenger = options.messenger || this;
      this.contentManager = options.contentManager;
      this.strategy = options.strategy;
      if (!this.el) {
        throw("Error: FormObserver initialized without a form element.");
      }
      this.mapperMode = !!this.contentManager.preferences.getPref('mapping:mapper');
      this.autofillMode = !!this.contentManager.preferences.getPref('autofill:mapper');
      this.showIconsForUnhandled = this.mapperMode || this.autofillMode;
      if (!this.contentManager.isLocked()) {
        this.mapperMode = false;
        this.autofillMode = false;
      }
      this.triggerFormPresent();
      this.bindToFormEvents();
      LOG_FINEST && ABINE_DNTME.log.finest("formObserver:initialize return");
    },

    getType: function() {
      if (this.formType) {
        return this.formType;
      } else {
        this.formType = abineFormAnalyzer.getFormType(this.el, this.mappedFields);
        return this.formType;
      }
    },

    getFields: function(overwrite) {
      if (this.fields && !overwrite) {
        return this.fields;
      } else {
        this.fields = abineFormAnalyzer.getFields(this.el, this.mappedFields);
        return this.fields;
      }
    },

    // ### function triggerFormPresent
    // Trigger an event indicating the presence of this form type
    triggerFormPresent: function() {
      LOG_FINEST && ABINE_DNTME.log.finest("formObserver:triggerFormPresent enter");
      var formType = this.getType();
      if (formType == abineFormAnalyzer.LOGIN_FORM) {
          LOG_INFO && ABINE_DNTME.log.info("Triggering loginFormPresent");
        this.trigger('loginFormPresent', {
          formObserver: this,
          fields: this.getFields()
        });
      } else if (formType == abineFormAnalyzer.REGISTRATION_FORM) {
          LOG_INFO && ABINE_DNTME.log.info("Triggering registrationFormPresent");
        this.trigger('registrationFormPresent', {
          formObserver: this,
          fields: this.getFields()
        });
      } else if (formType == abineFormAnalyzer.CHECKOUT_FORM){
        LOG_INFO && ABINE_DNTME.log.info("Triggering checkoutFormPresent");
        this.trigger('checkoutFormPresent', {
          formObserver: this,
          fields: this.getFields()
        });
      }

      LOG_FINEST && ABINE_DNTME.log.finest("formObserver:triggerFormPresent return");
    },

    ignoreFieldEvent: function(eventType) {
      if (eventType != 'iconclick') {
        if (this.isDynamicFieldIcon()) {
          return true;
        }
        if (this.strategy.contentManager.preferences.getPref("useIconPanelMode") == responseCodes.SPLIT_ICON_PANELS_CLICK_ICON_FOR_PANEL) {
          return true;
        }
      }
      return false;
    },

    isStaticFieldIcon: function() {
      return this.mode == STATIC_FIELD_ICON;
    },

    isDynamicFieldIcon: function() {
      return this.mode == DYNAMIC_FIELD_ICON;
    },

    // ### function bindToFormChange
    // Bind to DOM change events on all the form elements' change events.
    // Triggers an event like "emailFieldFocused", "passwordFieldChanged", etc.
    // Binds to all hookableFieldEvents on all fields that are in hookableDataTypes
    bindToFormEvents: function() {
      LOG_FINEST && ABINE_DNTME.log.finest("formObserver:bindToFormEvents enter");
      var events = hookableFieldEvents.join(' ');
      var fields = this.getFields();
      var formType = this.getType();
      this.mode = NO_FIELD_ICON;
      if (this.strategy.contentManager.preferences.getPref("useIconPanelMode")) {
        this.mode = STATIC_FIELD_ICON;
      } else if (this.strategy.contentManager._showFieldIcons) { // TODO: condition to check when we show dynamic field icon
        this.mode = DYNAMIC_FIELD_ICON;
      }

      _(fields).each(function(field) {
        // if(userHasFeedbackEnabled) {
        var element$ = $(field.element);
        element$.bind("change.mmfo", _.bind(function(e) {
          var blurFilled = !!element$.attr('blurFilled');
          element$.removeAttr('blurFilled');
          timer.setTimeout(_.bind(function(){
            this.trigger("formFieldChanged", {
              formObserver: this,
              field: field,
              type: e.type,
              blurFilled: blurFilled,
              keyCode: e.type === "keydown"?e.keyCode:0,
              realEvent: e
            });
          }, this), 0);
        }, this));
        // } endif

        var hookable = !(this.mapperMode || this.autofillMode) && isHookableField(field);
        if (hookable) {
          var element$ = $(field.element);

          if (this.strategy.fieldTypeDisabled(field.dataType, formType)) {
            // do not attach any events as the helper is disabled for this type
            return;
          }

          if (this.isStaticFieldIcon() && field.fieldType != 'select') {
            this.addFieldIcon(element$, field);
          } else if (this.isDynamicFieldIcon()) {
            this.addDynamicFieldIcon(element$, field);
          }

          element$.bind(events, _.bind(function(e) {
            if(this.contentManager && !this.contentManager.userIntent){
              return;
            }

            if (e.type == 'focus') {
              if (!element$.attr('dntmeHasFocus')) {
                element$.attr('dntmeHasFocus', 'yes');
              } else {
                // already has focus, so ignore focus event
                return;
              }
            } else if (e.type == 'blur') {
              element$.removeAttr('dntmeIconClick');
              element$.removeAttr('dntmeHasFocus');
              element$.removeAttr('dntmeHasFocusRemoved');
            }
            if (e.type == 'click' && element$.attr('dntmeHasFocus')) {
              return;
            }
            if (element$.attr('mmautofilling') || // nothing to do as its an event generated by autofiller
                element$.attr('dntmeIgnoreBlur') // nothing to do as its an event generated by click on panel
              ) {
              return;
            }

            // re-enable panels when user clears a field
            if (element$.val() == '' && element$.data('abineManualTyped')) {
              element$.removeData('abineManualTyped');
              var grp = element$.attr('mmclosedfield');
              _(fields).each(function(f){
                if ($(f.element).attr('mmclosedfield') == grp) {
                  $(f.element).removeAttr('mmclosedfield');
                }
              });
            }

            if (e.type == 'click' || e.type == 'focus') {
              // prevent multiple clicks/focus in short span of time
              if (element$.attr('dntmeIgnoreClickFocus')) {
                return;
              }
              element$.attr('dntmeIgnoreClickFocus', 'yes');
              timer.setTimeout(function(){element$.removeAttr('dntmeIgnoreClickFocus');element$.removeAttr('dntmeIconClick');}, 1000);
            }

            if (element$.attr('dntmeIconClick')) {
              return;
            }

            var fieldType = getHookableDataType(field.dataType);

            if (fieldType) {
              var eventName = eventNameFor(fieldType, e.type);
              if (eventName) {
                if (this.hasListeners(eventName)) {
                  LOG_FINEST && ABINE_DNTME.log.finest("formObserver:generating "+eventName);
                  // run our handler with 0ms delay to avoid lockup of browser on big forms in firefox
                  timer.setTimeout(_.bind(function () {
                    this.trigger(eventName, {
                      formObserver: this,
                      field: field,
                      type: e.type,
                      keyCode: e.type === "keydown" ? e.keyCode : 0,
                      realEvent: e
                    });
                  }, this), 0);
                }
              }
            }
          }, this)); // end (field.element).bind
        }
      }, this); // end _(fields).each

      LOG_FINEST && ABINE_DNTME.log.finest("formObserver:bindToFormEvents return");
    },

    bindUnhandledFields: function() {
      var fields = this.getFields();
      var unhandledEvents = {};
      // show field icon for unhandled fields only if in mapping or auto-fill mode.
      if (this.showIconsForUnhandled) {
        _(fields).each(function(field) {
          var hookable = !(this.mapperMode || this.autofillMode) && isHookableField(field);
          if (hookable) {
            var fieldType = getHookableDataType(field.dataType);
            if (fieldType) {
              var eventName = eventNameFor(fieldType, 'focus');
              if (eventName) {
                if (!this.hasListeners(eventName)) {
                  unhandledEvents[eventName.replace('Focused', 'IconClicked')] = 1;
                  this.bindUnknownField(field, $(field.element));
                }
              }
            }
          } else {
            unhandledEvents['unknownFieldIconClicked'] = 1;
            this.bindUnknownField(field, $(field.element));
          }
        }, this);
      }
      return unhandledEvents;
    },

    bindUnknownField: function (field, element$) {
      if (field.fieldType != 'select' && element$.css('background-image') in {'': 1, 'none': 1} && !element$.data('unknown-hooked')) {
        element$.data('unknown-hooked', 'true');
        element$.bind('focus.mmfounknown', _.bind(function () {
          this.addFieldIcon(element$, field);
        }, this));
        element$.bind('blur.mmfounknown', _.bind(function () {
          this.removeFieldIcon(element$, field);
        }, this));
      }
    },

    switchToDynamicFieldIcons: function() {
      // can be done only when there is no field icon
      if (this.mode != NO_FIELD_ICON) {
        return;
      }
      this.mode = DYNAMIC_FIELD_ICON;
      var fields = this.getFields();
      var formType = this.getType();

      _(fields).each(function(field) {
        if (isHookableField(field)) {
          if (this.strategy.fieldTypeDisabled(field.dataType, formType)) {
            // do not attach any events as the helper is disabled for this type
            return;
          }
          this.addDynamicFieldIcon($(field.element), field);
        }
      }, this);
    },

    addDynamicFieldIcon: function(element$, field) {
      var entitledToPhone = this.contentManager.preferences.isPrefTrue("entitledToPhone");
      var isPhoneField = field.dataType.indexOf("/contact/phone") == 0

      // Don't Show Blur Icon for Phone Masking On Non-Premium Users: SERV-2162
      var hasAccess = (isPhoneField? entitledToPhone:true)

      if (hasAccess && field.fieldType != 'select' && element$.css('background-image') in {'': 1, 'none': 1}) {
        element$.bind('focus.mmfodynamic', _.bind(function () {
          if (!this.strategy.blurIconDisabled(field.dataType, this.getType())) {
            this.addFieldIcon(element$, field);
          }
          element$.data('abine-ignore-mouseout', 'true');
        }, this));
        element$.bind('blur.mmfodynamic', _.bind(function () {
          this.removeFieldIcon(element$, field);
          element$.removeData('abine-ignore-mouseout');
        }, this));
        element$.bind('mouseenter.mmfodynamic', _.bind(function () {
          if (!this.strategy.blurIconDisabled(field.dataType, this.getType())) {
            this.addFieldIcon(element$, field);
          }
        }, this));
        element$.bind('mouseleave.mmfodynamic', _.bind(function () {
          if (!element$.data('abine-ignore-mouseout')) {
            this.removeFieldIcon(element$, field);
          }
        }, this));
      }
    },

    addFieldIcon: function(element$, field) {
      element$.data('old-background', element$[0].style.background);
      _.each({
        'background-image': 'url(' + assets.assetUrl('/pages/images/dnt-field-icon.png') + ')',
        'background-repeat': 'no-repeat',
        'background-position': 'calc(100% - 2px) center',
        'background-size': '15px 15px'
      }, function(val, key){
        element$[0].style.setProperty(key, val, 'important');
      });
      // cursor for our icon
      element$.data('old-cursor', element$.css('cursor'));
      element$.unbind('mousemove.mmfoicon').bind('mousemove.mmfoicon', _.bind(function (e) {
        var offsetX = typeof(e.offsetX) != 'undefined' ? e.offsetX : (e.clientX - $(e.target).offset().left);
        if (offsetX > (e.target.offsetWidth - 26)) {
          element$.css('cursor', 'pointer');
        } else {
          element$.css('cursor', element$.data('old-cursor'));
        }
      }, this));
      // click on field icon
      element$.unbind('mousedown.mmfoicon').bind('mousedown.mmfoicon', _.bind(function (e) {
        var offsetX = typeof(e.offsetX) != 'undefined' ? e.offsetX : (e.clientX - $(e.target).offset().left);
        if (offsetX > (e.target.offsetWidth - 18)) {
          element$.attr('dntmeIconClick', 'yes');
          var eventName = eventNameFor(getHookableDataType(field.dataType), 'iconclick');
          if (this.mapperMode || this.autofillMode) {
            timer.setTimeout(_.bind(function () {
              this.trigger('unknownFieldIconClicked', {
                formObserver: this,
                field: field,
                type: 'iconclick',
                keyCode: 0
              });
            }, this), 0);
          } else {
            timer.setTimeout(_.bind(function () {
              this.trigger(eventName, {
                formObserver: this,
                field: field,
                type: 'iconclick',
                keyCode: 0
              });
            }, this), 0);
          }
        }
      }, this));
    },

    removeFieldIcon: function(element$, field) {
      _.each({
        'background-image': 'url(' + assets.assetUrl('/pages/images/dnt-field-icon.png') + ')',
        'background-repeat': 'no-repeat',
        'background-position': 'calc(100% - 2px) center',
        'background-size': '15px 15px'
      }, function(val, key){
        element$[0].style.removeProperty(key);
      });
      element$[0].style.background = element$.data('old-background');
      element$.unbind('.mmfoicon');
      element$.removeData('old-background');
      element$.removeData('old-cursor');
      element$.removeData('abine-ignore-mouseout');
    },

    unbindFormEvents: function() {
      var fields = this.getFields();
      _(fields).each(function(field) {
        var element$ = $(field.element);
        element$.unbind('.mmfo').unbind('.mmfoicon').unbind('.mmfounknown');
        element$.removeData('unknown-hooked', 'true');
        $(field.element).unbind('.mmfo');
        $(field.element).unbind('.mmfoicon');
        $(field.element).unbind('.mmfodynamic');
        $(field.element).unbind('.mmfounknown');
      });
    }

  });

  // Maps a field type and event type to an event name
  var eventNameFor = function(fieldType, eventType) {
    var eventName;
    if (fieldType == abineFormAnalyzer.EMAIL_FIELD) {
      eventName = 'emailField';
    } else if (fieldType == abineFormAnalyzer.PHONE_FIELD) {
      eventName = 'phoneField';
    } else if (fieldType == abineFormAnalyzer.PASSWORD_FIELD) {
      eventName = 'passwordField';
    } else if (fieldType == abineFormAnalyzer.USERNAME_FIELD){
      eventName = 'usernameField';
    } else if (fieldType == abineFormAnalyzer.PAYMENT_FIELD){
      eventName = 'paymentField';
    } else if (fieldType == abineFormAnalyzer.ADDRESS_FIELD){
      eventName = 'addressField';
    } else if (fieldType == abineFormAnalyzer.OLDPASSWORD_FIELD) {
      eventName = 'oldPasswordField';
    } else if (fieldType == abineFormAnalyzer.LOGIN_FIELD) {
      eventName = 'loginField';
    } else if (fieldType == abineFormAnalyzer.IDENTITY_FIELD) {
      eventName = 'identityField';
    } else {
      eventName = 'unknownField';
    }

    if (eventType == 'change') {
      eventName += 'Changed';
    } else if (eventType == 'focus') {
      eventName += 'Focused';
    } else if (eventType == 'blur') {
      eventName += 'Blurred';
    } else if (eventType == 'click'){
      eventName += 'Clicked';
    } else if (eventType == 'iconclick' || eventType == 'showpanel'){
      eventName += 'IconClicked';
    } else if (eventType == 'keydown'){
      eventName += 'Keydown';
    }
    return eventName;
  };

  return {
    FormObserver: FormObserver,

    LOGIN_FORM: abineFormAnalyzer.LOGIN_FORM,
    REGISTRATION_FORM: abineFormAnalyzer.REGISTRATION_FORM,
    CHECKOUT_FORM: abineFormAnalyzer.CHECKOUT_FORM,
    CHANGEPASSWORD_FORM: abineFormAnalyzer.CHANGEPASSWORD_FORM,
    OTHER_FORM: abineFormAnalyzer.OTHER_FORM
  };

});
ABINE_DNTME.define('abine/formStrategy',
    ["jquery", "documentcloud/underscore", "abine/core", "abine/formObserver","abine/timer", "abine/formAnalyzer",
      "models", "abine/config", "abine/url", "abine/responseCodes", 'abine/simulateEvents'],
    function($, _, abineCore, abineFormObserver, abineTimer, abineFormAnalyzer,
      models, config, abineUrl, responseCodes, simulateEvents) {

  var ENABLE_BROWSER_AUTOCOMPLETE = false;

  var KEYCODE_ENTER = 13;
  var KEYCODE_UP = 38;
  var KEYCODE_DOWN = 40;
  var KEYCODE_BACKSPACE = 8;
  var KEYCODE_DELETE = 46;

  var BaseHelper = abineCore.BaseClass.extend({
    constructor: function() {
      this.id = 'strategy'+Math.floor(Math.random()*10000000);
      this.initialize.apply(this, arguments);

    },

    setupForm: function() {
      this.initFormObserver();
      this.bindFormObserver();
      this.bindFieldChangeObserver();
    },

    cleanup: function(){

    },

    getFields: function(overwrite){
      return this.formObserver.getFields(overwrite);
    },

    getField: function(field) {
      if (!field) return null;
      var fields = this.formObserver.getFields();
      return _(fields).find(function(f) { return (f.signature == field || f.element == field || (field.id && f.element.id && f.element.id == field.id)); });
    },

    getFieldValue: function(fieldId) {
      var field = this.getField(fieldId);
      if (field) {
        return $(field.element).val();
      }
      return null;
    },

    getFieldForDataType: function(dataType){
      var fields = this.formObserver.getFields();
      return _(fields).find(function(f) { return f.dataType == dataType;});
    },

    getAutofillData: function() {
      var fields = this.formObserver.getFields();
      var response = {};
      _(fields).each(function(f) {
        var el = $(f.element);
        response[f.signature] = {value: el.val(), fillType: el.data('mmFillType')}
      });
      return response;
    },

    bindKeyEvents: function(fieldId, doNotCloseOnTyping, callback) {
      var field = this.getField(fieldId);
      if (!field) return;
      $(field.element).unbind('keydown.dntme').bind('keydown.dntme', _.bind(function(e) {
        if (e.keyCode === KEYCODE_ENTER) {
          e.preventDefault();
          e.stopPropagation();
          return false;
        }
      }));

      $(field.element).unbind('keyup.dntme').bind('keyup.dntme', _.bind(function(e) {
        if (e.keyCode === KEYCODE_UP) {
          callback('up');
        } else if (e.keyCode === KEYCODE_DOWN) {
          callback('down');
        } else if (e.keyCode === KEYCODE_ENTER){
          callback('enter');
          return false;
        } else if (String.fromCharCode(e.keyCode).match(/\w/)) { // only when printable character
          $(field.element).data('abineManualTyped', true);
          if (e.currentTarget.value.length >= 5 && !doNotCloseOnTyping){
            this.groupClose(this.formObserver.getFields(), field.dataType);
            this.contentManager.closeCurrentPanel();
          }
        }
      }, this));
    },

    refreshOnFormChange: function(fields) {
      var hasFormChanged = abineFormAnalyzer.hasFormChanged(this.formEl, this);
      if (hasFormChanged){
        if (this.formEl.nodeName.toLowerCase() == 'form' || !this.newRoot) {
          abineFormAnalyzer.clearFormCache(this.formEl);
          //if (!abineFormAnalyzer._elementInDocument(this.formEl)) {
          //  // form was replaced.  find the new form.
          //  var newForm = abineFormAnalyzer.findReplacementForm(this.formEl, this.contentManager.domain);
          //  if (newForm) {
          //    LOG_FINEST && ABINE_DNTME.log.finest("** found replacement form");
          //    this.cleanup();
          //    this.formEl = newForm;
          //    abineFormAnalyzer.clearFormCache(this.formEl);
          //    this.setupForm();
          //  }
          //}
          return this.getFields(true);
        } else {
          var forms = abineFormAnalyzer.getFieldGroupsWithoutForm(this.formEl.ownerDocument, this.contentManager.domain, true);
          // find new form that has one of the fields in current form
          var newForm = _.find(forms, function(form){
            if (form.nodeName.toLowerCase() == 'form') return false;
            var field = _.find(fields, function(field){
              var node = field.element;
              while (node && node != form) {
                node = node.parentNode;
              }
              return node;
            });
            if (field) {
              return true;
            }
          });
          if (newForm) {
            // setup form again
            abineFormAnalyzer.clearFormCache(this.formEl);
            this.cleanup();
            this.formEl = newForm;
            this.setupForm();
            return this.getFields(true);
          }
        }
      }
      return fields;
    },

    triggerPanelFillEvent: function(options) {
      var fieldData = {
        disposableGUID: options.field.disposableGUID,
        name: options.field.name,
        origin: options.origin,
        dataType: options.field.dataType,
        accountId: options.accountId,
        value: options.field.value
      };

      options.field.element.setAttribute('abineOrigin', options.origin);

      //mark form as having been filled
      $(this.formEl).attr("maskMeFilled", "yes");
      if(fieldData.accountId) $(this.formEl).attr('abineAccountId', fieldData.accountId);

      this.contentManager.triggerPanelFillEvent(this.formEl, fieldData);
    },

    closePanelFromHeader: function(fieldId) {
      var field = this.getField(fieldId);
      if (field) {
        $(field.element).attr("mmclosedfield", true); // modify the dom to remember that for this field, the user explicitly closed the panel
        var fields = this.formObserver.getFields();
        this.groupClose(fields, field.dataType);
      }
    },

    groupClose: function(fields, dataType) {
      if (!dataType) return;

      var groups = [
        '/financial/creditcard',
        '/contact/phone',
        /nameperson\/(first|last|middle|full)/i,
        '/contact/email',
        '/nameperson/friendly',
        '/password',
        /contact\/(postal|country|state|city|zip)/i
      ];

      function matches(group, dataType) {
        if (group.source) {
          return group.test(dataType);
        } else {
          return dataType.indexOf(group) != -1;
        }
      }

      var grp = "grp-"+Math.random();

      _.each(groups, _.bind(function(group){
        if (!matches(group, dataType)) return;
        _.each(fields, function(field){
          if (field.dataType && matches(group, field.dataType)) {
            field.element.setAttribute("mmclosedfield", grp);
          }
        });
      }, this));
    },

    showFieldIcon: function(fieldId, value) {
        this.formObserver.switchToDynamicFieldIcons();
    },

    ignoreBlur: function(fieldId, value) {
      var field = this.getField(fieldId);
      if (field) {
        LOG_FINEST && ABINE_DNTME.log.finest("ignore blur "+fieldId+" "+value);
        if (value === true) {
          if (!field._blurTimer) {
            field.element.setAttribute('dntmeIgnoreBlur', 'true');
          }
          abineTimer.clearTimeout(field._blurTimer);
          delete field._blurTimer;
        } else {
          field.element.removeAttribute('dntmeIgnoreBlur');
        }
      }
    },

    fieldsToFieldDataTypeMap: function(fieldsToFill) {
      var fieldDataTypeMap = {};
      _.each(fieldsToFill, _.bind(function(fieldToFill) {
        if (fieldToFill.dataType && abineFormAnalyzer.isVisible(fieldToFill.element)) {
          fieldDataTypeMap[fieldToFill.dataType] = fieldToFill;
        }
      }, this));

      return fieldDataTypeMap;
    }
  });


  var fieldChangeEventMixins = {
    fieldTypeDisabled: function(fieldType, formType) {
      if (fieldType == '/contact/email') {
        if (formType != abineFormAnalyzer.LOGIN_FORM)
          return this.emailDisabledHere();
        else
          return this.loginDisabledHere();
      }

      if (fieldType && fieldType.match('/password')) {
        if (formType != abineFormAnalyzer.LOGIN_FORM)
          return this.passwordDisabledHere();
        else
          return this.loginDisabledHere();
      }

      if (fieldType == '/nameperson/friendly') {
        return this.loginDisabledHere();
      }

      if (fieldType.match(/\/nameperson\/(first|last|middle)/i)) {
        return this.identityDisabledHere();
      }

      if (fieldType == '/contact/phone')
        return this.phoneDisabledHere();

      if (fieldType && fieldType.match(/address|state|city|postal|country/))
        return this.addressDisabledHere();

      if (fieldType && fieldType.match(/creditcard/))
        return this.cardDisabledHere();

      return false;
    },
    emailDisabledHere: function() {
      var contentManager = this.contentManager;
      var blacklist = contentManager.blacklist;
      return !contentManager.preferences.getPref("suggestProtectedEmail") || (blacklist && blacklist.get('email')) ||
                !contentManager.canShowPanelHere();
    },

    phoneDisabledHere: function() {
      var contentManager = this.contentManager;
      var blacklist = contentManager.blacklist;
      return !contentManager.preferences.getPref("suggestProtectedPhone") || (blacklist && blacklist.get('phone')) ||
                !contentManager.canShowPanelHere();
    },

    cardDisabledHere: function() {
      var contentManager = this.contentManager;
      var blacklist = contentManager.blacklist;
      var globalRealCard = contentManager.preferences.getPref("suggestRealCard");
      var globalCard = contentManager.preferences.getPref("suggestProtectedCard");
      if(blacklist){
        return ((blacklist.get("card") || !globalCard) && (blacklist.get("real_card") || !globalRealCard)) || !contentManager.canShowPanelHere() ;
      } else {
        return (!globalRealCard && !globalCard) || !contentManager.canShowPanelHere();
      }
    },

    loginDisabledHere: function() {
      var contentManager = this.contentManager;
      var blacklist = contentManager.blacklist;
      var preferences = contentManager.preferences;
      return !preferences.getPref("helpMeLogin") ||
                (blacklist && blacklist.get('account'))  || !contentManager.canShowPanelHere();
    },

    passwordDisabledHere: function() {
      var contentManager = this.contentManager;
      var blacklist = contentManager.blacklist;
      var preferences = contentManager.preferences;
      return !preferences.getPref("suggestStrongPassword") || !preferences.getPref("rememberAccounts") ||
        (blacklist && blacklist.get('password')) || (blacklist && blacklist.get('save_account')) || !contentManager.canShowPanelHere();
    },

    addressDisabledHere: function() {
      var contentManager = this.contentManager;
      var blacklist = contentManager.blacklist;
      var preferences = contentManager.preferences;
      return !preferences.getPref('entitledToAccounts') || !preferences.getPref("suggestAddresses") ||
                (blacklist && blacklist.get('address')) || !contentManager.canShowPanelHere() || this.contentManager.preferences.getSplitTest("wallet01") == responseCodes.SPLIT_WALLET_OFF;
    },

    identityDisabledHere: function() {
      var contentManager = this.contentManager;
      var blacklist = contentManager.blacklist;
      var preferences = contentManager.preferences;
      return !preferences.getPref('entitledToAccounts') || !preferences.getPref("suggestIdentities") ||
                (blacklist && blacklist.get('identity')) || !contentManager.canShowPanelHere();
    },

    disableAutoComplete: function(element){
      if (element.hasAttribute("autocomplete") && !element.hasAttribute("oldautocomplete")) {
        element.setAttribute("oldautocomplete", element.getAttribute("autocomplete"));
      } else if(!element.hasAttribute("autocomplete")) {
        element.setAttribute("oldautocomplete", "remove");
      }
      element.setAttribute("autocomplete","off");
    },

    enableAutoComplete: function(element){
      if (element.hasAttribute("oldautocomplete")) {
        if (element.getAttribute("oldautocomplete") == "remove") {
          element.removeAttribute("autocomplete");
        } else {
          element.setAttribute("autocomplete", element.getAttribute("oldautocomplete"));
        }
        element.removeAttribute("oldautocomplete");
      }
    },

    blurIconDisabled: function(fieldType, formType) {
      var blurIconDisabledHere = _.bind(function(fieldType, formType) {
        if (fieldType == '/nameperson/friendly') {
          if (formType == abineFormAnalyzer.LOGIN_FORM) {
            return this.accountsLength == 0;
          } else {
            return true;
          }
        }

        if (fieldType && fieldType.match('/password')) {
          if (formType == abineFormAnalyzer.LOGIN_FORM) {
            // handler to show panel only gets added to password fields if there wasn't a username/email
            if (this.fieldDataTypeMap && ('/contact/email' in this.fieldDataTypeMap || '/nameperson/friendly' in this.fieldDataTypeMap)){
              return true;
            } else {
              return this.accountsLength == 0;
            }
          }
        }

        if (fieldType == '/contact/email') {
          if (formType == abineFormAnalyzer.LOGIN_FORM) {
            return this.accountsLength == 0;
          }
        }

        return false
      }, this);

      return this.fieldTypeDisabled(fieldType, formType) || blurIconDisabledHere(fieldType, formType);
    },

    cleanupPanel: function(element, enableAutoComplete) {
      if (enableAutoComplete) {
        this.enableAutoComplete(element);
      }
      $(element).unbind('keydown.dntme').unbind('keyup.dntme');
      if (element.hasAttribute('dntmeHasFocusRemoved')){
        element.removeAttribute('dntmeHasFocusRemoved');
        element.setAttribute('dntmeHasFocus', 'yes');
      } else {
        element.removeAttribute('dntmeIgnoreBlur');
      }
    },

    canShowDNTMeHelper: function(e, formType) {
      var element = e.field.element;
      var type = e.type;
      if (type == 'click' && formType == 'register' && ENABLE_BROWSER_AUTOCOMPLETE) {
        // to prevent both browser autosuggest and our panel from appearing together
        if (element.hasAttribute('dntmeHasFocus')) {
          if (this.contentManager.isPanelShown(e.field)) {
            return true;
          }
          element.removeAttribute('dntmeHasFocus');
          element.setAttribute('dntmeHasFocusRemoved', 'yes');
          this.disableAutoComplete(element);
          return false;
        } else {
          element.removeAttribute('dntmeHasFocusRemoved');
          element.setAttribute('dntmeHasFocus', 'yes');
        }
      }
      return true;
    },

    initFormObserver: function () {
      this.formObserver = this.formObserver || new abineFormObserver.FormObserver({
          el: this.formEl,
          mappedFields: this.mappedFields,
          contentManager: this.contentManager,
          strategy: this
        });
    },

    bindFieldChangeObserver: function(options){
      LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:bindFieldChangeObserver enter");
      var accountHandler = _.bind(this.onAccountFieldActivity, this);
      this.formObserver.on('emailFieldChanged', accountHandler);
      this.formObserver.on('passwordFieldChanged', accountHandler);
      this.formObserver.on('oldPasswordFieldChanged', accountHandler);
      this.formObserver.on('usernameFieldChanged', accountHandler);
      this.formObserver.on('loginFieldChanged', accountHandler);

      var paymentHandler = _.bind(this.onCardFieldActivity, this);
      this.formObserver.on('paymentFieldChanged', paymentHandler);

      var fieldChangeHandler = _.bind(this.onFieldChanged, this);
      this.formObserver.on('formFieldChanged', fieldChangeHandler);

      LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:bindFieldChangeObserver return");
    },

    onFieldChanged: function(e) {
      var formData = abineFormAnalyzer.getFormData(this.formEl, this.contentManager.href);
      var fieldData = _.find(formData.allFields, function(field){
        return field.signature == e.field.signature;
      });
      if (fieldData && !fieldData.hidden) {
        fieldData.origin = "change";
        fieldData.blurFilled = e.blurFilled;
        fieldData.value = e.field.value || $(e.field.element).val();

        this.contentManager.messenger.send('contentManager:formFieldChanged:processMapping', {fieldData: fieldData, formData: formData, checkoutButtons: this.contentManager.getCheckoutButtons()});
      }
    },

    bindUnhandledFields: function() {
      this.formObserver = this.formObserver || new abineFormObserver.FormObserver({
        el: this.formEl,
        mappedFields: this.mappedFields,
        contentManager: this.contentManager,
        strategy: this
      });
      var events = this.formObserver.bindUnhandledFields();
      _.each(events, _.bind(function(val, event){
        this.formObserver.on(event, _.bind(this.onUnknownFieldActivity, this));
      }, this));
    },

    onAccountFieldActivity: function(e) {
      // assume any change means user changed data from possibly filled data to something else
      // on testing, this event does not get triggered on autologin via panel
      // if user edits password for example after autofill, this will remove maskMeFilled
      // but if user then proceeds to use panel again to login with stored account, maskMeFilled will be set and this event won't be fired again, leaving maskMeFilled = yes
      $(this.formEl).removeAttr("maskMeFilled");
      var field = e.field;
      field.origin = "change";
      var fieldData = {
        name: field.name,
        origin: field.origin,
        dataType: field.dataType,
        value: field.value
      };
      this.contentManager.messenger.send('contentManager:formFieldChanged', {fieldData:fieldData, formData:abineFormAnalyzer.getFormData(this.formEl,this.contentManager.href)});
    },

    onCardFieldActivity: function(e) {
      $(this.formEl).removeAttr("maskMeFilled");
      var field = e.field;
      field.origin = "change";
      var fieldData = {
        name: field.name,
        origin: field.origin,
        dataType: field.dataType,
        value: field.value
      };
      this.contentManager.messenger.send('contentManager:formPaymentFieldChanged', {fieldData:fieldData, formData:abineFormAnalyzer.getFormData(this.formEl,this.contentManager.href)});
    },

    onFieldBlurred: function(e) {
      if (this.contentManager.isPanelShown(e.field)) {
        if (e.field.element.getAttribute('dntmeIgnoreBlur')) {
          e.field.element.removeAttribute('dntmeIgnoreBlur');
          return;
        }
        abineTimer.clearTimeout(e.field._blurTimer);
        var panelId = this.contentManager._currentPanelIframe;
        LOG_FINEST && ABINE_DNTME.log.finest("field blur "+e.field.signature);
        e.field._blurTimer = abineTimer.setTimeout(_.bind(function(){
          delete e.field._blurTimer;
          if (this.contentManager.isPanelShown(e.field)) {
            if (e.field.element.getAttribute('dntmeIgnoreBlur')) {
              e.field.element.removeAttribute('dntmeIgnoreBlur');
              return;
            }
            LOG_FINEST && ABINE_DNTME.log.finest("closing on field blur "+e.field.signature);
            this.contentManager.closeCurrentPanel(panelId);
          }
        }, this), 500);
      }
    },

    onUnknownFieldActivity: function(e) {
      var element = e.field.element;
      this.disableAutoComplete(element);

      // don't show panel if it's autofilling or if someone has already closed the panel  (overridden by click on field icon)
      if(!element.hasAttribute("mmautofilling") && (!element.hasAttribute("mmclosedfield") || e.type == 'iconclick')){
        this.contentManager.showAutoFillWith({
          fieldType: e.field.dataType,
          formType: this.formType,
          fieldId: e.field.signature,
          strategyId: this.id,
          category: 'unknown',
          iconClick: e.type=='iconclick'
        });
      }
    }
  };


  var commonFormStrategyMixins = {

    // ### function bindformObserver
    // Creates a FormObserver and binds handlers for all relevant events
    bindFormObserver: function(options){
      LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:bindFormObserver enter");
      var emailHandler = _.bind(this.onEmailFieldActivity, this);
      this.formObserver.on('emailFieldFocused', emailHandler);
      this.formObserver.on('emailFieldClicked', emailHandler);
      this.formObserver.on('emailFieldIconClicked', emailHandler);

      var loginHandler = _.bind(this.onLoginFieldActivity, this);
      this.formObserver.on('loginFieldFocused', loginHandler);
      this.formObserver.on('loginFieldClicked', loginHandler);
      this.formObserver.on('loginFieldIconClicked', loginHandler);

      var phoneHandler = _.bind(this.onPhoneFieldActivity, this);
      this.formObserver.on('phoneFieldFocused', phoneHandler);
      this.formObserver.on('phoneFieldClicked', phoneHandler);
      this.formObserver.on('phoneFieldIconClicked', phoneHandler);

      var cardHandler = _.bind(this.onPaymentFieldActivity, this);
      this.formObserver.on('paymentFieldFocused', cardHandler);
      this.formObserver.on('paymentFieldClicked', cardHandler);
      this.formObserver.on('paymentFieldIconClicked', cardHandler);

      var passwordHandler = _.bind(this.onPasswordFieldActivity, this);
      this.formObserver.on('passwordFieldFocused', passwordHandler);
      this.formObserver.on('passwordFieldClicked', passwordHandler);
      this.formObserver.on('passwordFieldIconClicked', passwordHandler);

      var addressHandler = _.bind(this.onAddressFieldActivity, this);
      this.formObserver.on('addressFieldFocused', addressHandler);
      this.formObserver.on('addressFieldClicked', addressHandler);
      this.formObserver.on('addressFieldIconClicked', addressHandler);

      var identityHandler = _.bind(this.onIdentityFieldActivity, this);
      this.formObserver.on('identityFieldFocused', identityHandler);
      this.formObserver.on('identityFieldClicked', identityHandler);
      this.formObserver.on('identityFieldIconClicked', identityHandler);

      var accountHandler = _.bind(this.onAccountFieldActivity, this);
      this.formObserver.on('emailFieldChanged', accountHandler);
      this.formObserver.on('passwordFieldChanged', accountHandler);
      this.formObserver.on('usernameFieldChanged', accountHandler);
      this.formObserver.on('loginFieldChanged', accountHandler);

      var fieldBlurHandler = _.bind(this.onFieldBlurred, this);
      this.formObserver.on('emailFieldBlurred', fieldBlurHandler);
      this.formObserver.on('usernameFieldBlurred', fieldBlurHandler);
      this.formObserver.on('passwordFieldBlurred', fieldBlurHandler);
      this.formObserver.on('paymentFieldBlurred', fieldBlurHandler);
      this.formObserver.on('addressFieldBlurred', fieldBlurHandler);
      this.formObserver.on('phoneFieldBlurred', fieldBlurHandler);
      this.formObserver.on('loginFieldBlurred', fieldBlurHandler);
      this.formObserver.on('identityFieldBlurred', fieldBlurHandler);

      this.bindUnhandledFields();

      LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:bindFormObserver return");
    },

    cleanup: function() {
      if (this.formObserver) {
        this.formObserver.unbindFormEvents();
        this.formObserver.off();
        this.formObserver = null;
      }
    },

    // ### function onEmailFieldActivity(event)
    // Creates an email helper panel in response to an event on an email field
    onEmailFieldActivity: function(e) {
      LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:onEmailFieldActivity enter");

      if ((!this.canShowDNTMeHelper(e, 'register') || this.emailDisabledHere()) && e.type != 'iconclick') {
        LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:onEmailFieldActivity return");
        return; // Return if this preference is deactivated (overridden by click on field icon)
      }

      if (this.contentManager.isPanelShown(e.field)) {
        LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:onEmailFieldActivity return");
        if (e.field.lastShownReason != 'showpanel') {
          this.contentManager.closeCurrentPanel();
        }
        return; // Panel for this field already open
      }
      if (this.contentManager.preferences.getPref("hasRegistered") && !this.contentManager.preferences.getPref("authToken")) {
        LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:onEmailFieldActivity return");
        return; // Return if there's no auth token because user got booted
      }

      if (this.formObserver.ignoreFieldEvent(e.type)) {
        return;
      }

      var element = e.field.element;
      this.disableAutoComplete(element);

        //don't show panel if it's autofilling or if someone has already closed the panel (overridden by click on field icon)
      if(!element.hasAttribute("mmautofilling") && (!element.hasAttribute("mmclosedfield") || e.type == 'iconclick')){

        var dynamicPanelObj = this.contentManager.processDynamicPanelForField("/contact/email");

        if(dynamicPanelObj.dynamicPanel && dynamicPanelObj.dynamicPanel.dontShow && !this.contentManager.preferences.isPrefTrue("hasRegistered")){
          return false;
        }

        e.field.lastShownReason = e.type;

        var fields = this.formObserver.getFields();
        var hasPassword = !!_.find(fields, function(f){return f.dataType != null && f.dataType.indexOf("password") != -1 && abineFormAnalyzer.isVisible(f.element)});

        this.contentManager.createPanel({
          secured: 'hadEmail',
          style: {},
          field: e.field,
          view: 'email',
          payload: {
            oldValue: e.field.element.value,
            iconClick: e.type=='iconclick',
            dynamicPanel: dynamicPanelObj.dynamicPanel,
            hasPassword: hasPassword,
            fieldType: e.field.dataType,
            formType: this.formType,
            fieldId: e.field.signature,
            category: 'email',
            strategyId: this.id
          }
        });
      }
      LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:onEmailFieldActivity return");
    },

    // ### function onLoginFieldActivity(event)
    // Creates a login helper panel in response to an event on an email field
    onLoginFieldActivity: function(e) {
      LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:loginHelper:onLoginFieldActivity enter");

      if (e.type != 'iconclick') {
        if (e.type != 'showpanel' && e.realEvent && e.realEvent.originalEvent === undefined) { // not triggered by human
          LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:loginHelper:onLoginFieldActivity return");
          return;
        }

        if (!this.canShowDNTMeHelper(e, 'login') || this.loginDisabledHere()) {
          LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:loginHelper:onLoginFieldActivity return");
          return;
        }
      }

      if (this.contentManager.isPanelShown(e.field)) {
        LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:loginHelper:onLoginFieldActivity return");
        if (e.field.lastShownReason != 'showpanel') {
          this.contentManager.closeCurrentPanel();
        }
        return; // Panel for this field already open
      }

      if (this.formObserver.ignoreFieldEvent(e.type)) {
        return;
      }

      var element = e.field.element;
      this.disableAutoComplete(element);

      if(!element.hasAttribute("mmautofilling") && (!element.hasAttribute("mmclosedfield") || e.type == 'iconclick')){

        var dynamicPanelObj = this.contentManager.processDynamicPanelForField("/contact/email");

        e.field.lastShownReason = e.type;

        this.contentManager.createPanel({
          style: {},
          secured: 'hadEmail',
          field: e.field,
          view: "login",
          payload: {
            iconClick: e.type=='iconclick',
            dynamicPanel: dynamicPanelObj.dynamicPanel,
            fieldType: e.field.dataType,
            formType: this.formType,
            fieldId: e.field.signature,
            category: 'account',
            strategyId: this.id
          }
        });
      }
      LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:loginHelper:onLoginFieldActivity return");
    },

    // ### function onPasswordFieldActivity(event)
    // Creates a password helper panel in response to an event on a password field
    onPasswordFieldActivity: function(e) {
      LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:onPasswordFieldActivity enter");
      if (this.passwordDisabledHere() || !this.canShowDNTMeHelper(e, 'register')) {
        LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:onPasswordFieldActivity return");
        return; // Return if this preference is deactivated
      }
      if (this.contentManager.isPanelShown(e.field)) {
        LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:onPasswordFieldActivity return");
        if (e.field.lastShownReason != 'showpanel') {
          this.contentManager.closeCurrentPanel();
        }
        return; // Panel for this field already open
      }
      if (!this.contentManager.preferences.getPref("authToken")) {
        LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:onPasswordFieldActivity return");
        return; // Return if there's no auth token because user got booted
      }
      // BL: 12/11/14 add check for split test for iconpanels. if we are in "click_icon_for_panel" split, then only show the panel on an icon click
      if((this.contentManager.preferences.getPref("useIconPanelMode") == responseCodes.SPLIT_ICON_PANELS_CLICK_ICON_FOR_PANEL) && e.type != 'iconclick') {
        return;
      }

        if (this.formObserver.ignoreFieldEvent(e.type)) {
            return;
        }

      var element = e.field.element;
      if(element.getAttribute("type") == "text"){
        LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:onPasswordFieldActivity return");
        return; // don't open password panels for text fields :/
        //maybe we should trigger this event again in 50ms to detect type="password" switch?
      }

      this.disableAutoComplete(element);

      if(!element.hasAttribute("mmautofilling") && !element.hasAttribute("mmclosedfield") || e.type == 'iconclick'){

        var dynamicPanelObj = this.contentManager.processDynamicPanelForField("/contact/email");

        if(dynamicPanelObj.dynamicPanel && dynamicPanelObj.dynamicPanel.dontShow){
          return false;
        }

        e.field.lastShownReason = e.type;

        this.contentManager.createPanel({
          style: {},
          secured: 'hadEmail',
          field: e.field,
          view: 'password',
          payload: {
            iconClick: e.type=='iconclick',
            dynamicPanel: dynamicPanelObj.dynamicPanel,
            dynamicPanelsJSON: dynamicPanelObj.json,
            fieldType: e.field.dataType,
            formType: this.formType,
            fieldId: e.field.signature,
            category: 'password',
            strategyId: this.id
          }
        });
      }
      LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:onPasswordFieldActivity return");
    },


    onAddressFieldActivity: function(e) {
      LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:onAddressFieldActivity enter");
      if (this.addressDisabledHere() || !this.canShowDNTMeHelper(e, 'register')) {
        LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:onAddressFieldActivity return");
        return; // Return if this preference is deactivated
      }
      if (this.contentManager.isPanelShown(e.field)) {
        LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:onAddressFieldActivity return");
        if (e.field.lastShownReason != 'showpanel') {
          this.contentManager.closeCurrentPanel();
        }
        return; // Panel for this field already open
      }
      if (this.contentManager.preferences.getPref("hasRegistered") && !this.contentManager.preferences.getPref("authToken")) {
        LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:onAddressFieldActivity return");
        return; // Return if there's no auth token because user got booted
      }

      if (this.formObserver.ignoreFieldEvent(e.type)) {
        return;
      }

      var element = e.field.element;

      this.disableAutoComplete(element);

      if (!element.hasAttribute("mmautofilling") && !element.hasAttribute("mmclosedfield") || e.type == 'iconclick') {

        var dynamicPanelObj = this.contentManager.processDynamicPanelForField("/contact/email");

        if (dynamicPanelObj.dynamicPanel && dynamicPanelObj.dynamicPanel.dontShow && !this.contentManager.preferences.isPrefTrue("hasRegistered")) {
          return false;
        }

        e.field.lastShownReason = e.type;

        this.contentManager.createPanel({
          style: {},
          secured: 'hadEmail',
          field: e.field,
          view: 'address',
          payload: {
            iconClick: e.type=='iconclick',
            dynamicPanel: dynamicPanelObj.dynamicPanel,
            dynamicPanelsJSON: dynamicPanelObj.json,
            fieldType: e.field.dataType,
            formType: this.formType,
            category: 'address',
            fieldId: e.field.signature,
            strategyId: this.id
          }
        });
      }
      LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:onAddressFieldActivity return");
    },

    onIdentityFieldActivity: function(e) {
      LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:onIdentityFieldActivity enter");
      if (this.identityDisabledHere() || !this.canShowDNTMeHelper(e, 'register')) {
        LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:onIdentityFieldActivity return");
        return; // Return if this preference is deactivated
      }
      if (this.contentManager.isPanelShown(e.field)) {
        LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:onIdentityFieldActivity return");
        if (e.field.lastShownReason != 'showpanel') {
          this.contentManager.closeCurrentPanel();
        }
        return; // Panel for this field already open
      }
      if (this.contentManager.preferences.getPref("hasRegistered") && !this.contentManager.preferences.getPref("authToken")) {
        LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:onIdentityFieldActivity return");
        return; // Return if there's no auth token because user got booted
      }

      if (this.formObserver.ignoreFieldEvent(e.type)) {
        return;
      }

      var element = e.field.element;

      this.disableAutoComplete(element);

      if (!element.hasAttribute("mmautofilling") && !element.hasAttribute("mmclosedfield") || e.type == 'iconclick') {

        var allDataTypes = {};
        _.each(this.formObserver.getFields(), function(fld){
          if (fld.dataType) {
            allDataTypes[fld.dataType] = 1;
          }
        });

        e.field.lastShownReason = e.type;

        this.contentManager.createPanel({
          style: {},
          secured: 'hadEmail',
          field: e.field,
          view: 'identity',
          payload: {
            iconClick: e.type=='iconclick',
            fieldId: e.field.signature,
            fieldType: e.field.dataType,
            formType: this.formType,
            category: 'identity',
            strategyId: this.id,
            allDataTypes: _.keys(allDataTypes)
          }
        });
      }
      LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:onIdentityFieldActivity return");
    },

    // ### function onPhoneFieldActivity(event)
    // Creates a phone helper panel in response to an event on a phone field
    onPhoneFieldActivity: function(e) {
      LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:onPhoneFieldActivity enter");
      if ((!this.canShowDNTMeHelper(e, 'register') || this.phoneDisabledHere()) && e.type != 'iconclick') {
        LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:onPhoneFieldActivity return");
        return; // Return if this preference is deactivated  (overridden by click on field icon)
      }
      if (this.contentManager.isPanelShown(e.field)) {
        LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:onPhoneFieldActivity return");
        if (e.field.lastShownReason != 'showpanel') {
          this.contentManager.closeCurrentPanel();
        }
        return; // Panel for this field already open
      }
      if (this.contentManager.preferences.getPref("hasRegistered") && !this.contentManager.preferences.getPref("authToken")) {
        LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:onPhoneFieldActivity return");
        return; // Return if there's no auth token because user got booted
      }

      if (this.formObserver.ignoreFieldEvent(e.type)) {
        return;
      }

      var element = e.field.element;

      function showPhonePanel() {
        this.disableAutoComplete(element);

        e.field.lastShownReason = e.type;

        this.contentManager.createPanel({
          secured: 'hadPhone',
          style: {},
          hasDynamicPanel: !!dynamicPanelObj.dynamicPanel,
          field: e.field,
          view: "phone",
          payload: {
            iconClick: e.type=='iconclick',
            dynamicPanel: dynamicPanelObj.dynamicPanel,
            fieldType: e.field.dataType,
            formType: this.formType,
            category: 'phone',
            fieldId: e.field.signature,
            strategyId: this.id
          }
        });
      }
      // don't show panel if it's autofilling or if someone has already closed the panel  (overridden by click on field icon)
      if (!element.hasAttribute("mmautofilling") && (!element.hasAttribute("mmclosedfield") || e.type == 'iconclick')) {

        var dynamicPanelObj = this.contentManager.processDynamicPanelForField("/contact/phone");

        showPhonePanel.call(this);
      }
      LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:onPhoneFieldActivity return");
    },

    // ### function onPaymentFieldActivity(event)
    // Creates a payment helper panel in response to an event on a payment field
    onPaymentFieldActivity: function(e) {
      LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:onPaymentFieldActivity enter");
      if ((!this.canShowDNTMeHelper(e, 'register') || this.cardDisabledHere() || e.field.fieldType == 'select') && e.type != 'iconclick') {
        LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:onPaymentFieldActivity return");
        return;
      }
      var contentManager = this.contentManager;
      if (contentManager.isPanelShown(e.field)) { // do not show helper for select boxes
        LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:onPaymentFieldActivity return");
        if (e.field.lastShownReason != 'showpanel') {
          this.contentManager.closeCurrentPanel();
        }
        return; // Panel for this field already open
      }
      var preferences = contentManager.preferences;
      if (preferences.getPref("hasRegistered") && !preferences.getPref("authToken")) {
        LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:onPaymentFieldActivity return");
        return; // Return if there's no auth token because user got booted
      }

      // do not ignore event if it was generated from checkout growl
      if (this.formObserver.ignoreFieldEvent(e.type) && !$(e.field.element).data('abine-params')) {
        return;
      }

      var element = e.field.element;

      function showCardPanel() {
        this.disableAutoComplete(element);

        var blacklist = contentManager.blacklist;
        var maskedCardsDisabled = !preferences.isPrefTrue("suggestProtectedCard") || (blacklist && !!blacklist.get("card"));
        var autoFillCardsDisabled = !preferences.isPrefTrue("suggestRealCard") || (blacklist && !!blacklist.get("real_card"));

        e.field.lastShownReason = e.type;

        contentManager.createPanel({
          secured: 'hadCard',
          style: {},
          panelWidth: 400,
          hasDynamicPanel: !!dynamicPanelObj.dynamicPanel,
          field: e.field,
          view: "payment",
          payload: {
            iconClick: e.type=='iconclick',
            maskedCardsDisabled: maskedCardsDisabled,
            autoFillCardsDisabled: autoFillCardsDisabled,
            amount: contentManager.getAmount(element.ownerDocument),
            fieldType: e.field.dataType,
            formType: this.formType,
            paymentsr2: contentManager.shouldShowCardAlertPanel(true),
            dynamicPanel: dynamicPanelObj.dynamicPanel,
            fieldId: e.field.signature,
            category: 'card',
            strategyId: this.id,
            params: $(e.field.element).data('abine-params')
          }
        });
        $(e.field.element).removeData('abine-params')
      }

      if (!element.hasAttribute("mmautofilling") && (!element.hasAttribute("mmclosedfield") || e.type == 'iconclick')){

        var dynamicPanelObj = contentManager.processDynamicPanelForField("/creditcard");

        if (dynamicPanelObj.dynamicPanel && dynamicPanelObj.dynamicPanel.dontShow) {
          return;
        }
//
//        if (!dynamicPanelObj.dynamicPanel) {
//          // no marketing panel
//          if (e.type != 'iconclick' && !contentManager.preferences.getPref("entitledToPhone")) {
//            // not premium user & not clicked on "field icon"
//            return;
//          }
//        }
        showCardPanel.call(this);
      }
      LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:onPaymentFieldActivity return");
    }

  };

  // Takes a content manager, and a form observer
  // Observes events and takes the appropriate action

  var RegistrationHelper = BaseHelper.extend({

    // ### function initialize(options)
    // - `el` - the form element
    // - `contentManager` - the content manager
    // - `preferences` - an array of the user's preferences
    initialize: function(options) {
      LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:initializeRegistrationHelper enter "+this.id);
      if (!options.el) { throw("RegistrationFormHelper expects a form element"); }
      this.formEl = options.el;
      this.contentManager = options.contentManager;
      this.preferences = options.preferences;
      this.mappedFields = options.mappedFields;

      this.initFormObserver();
      this.bindFieldChangeObserver();
      this.bindFormObserver();
    }
  });

  _.extend(RegistrationHelper.prototype, commonFormStrategyMixins, fieldChangeEventMixins);

  var LoginHelper = BaseHelper.extend({


    // ### function initialize(options)
    // - `el` - the form element
    // - `contentManager` - the content manager
    // - `preferences` - an array of the user's preferences
    initialize: function(options) {
      LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:initializeLoginHelper enter "+this.id);
      if (!options.el) { throw("LoginHelper expects a form element"); }
      this.formEl = options.el;
      this.contentManager = options.contentManager;
      this.preferences = options.preferences;
      this.mappedFields = options.mappedFields;
      this.fieldDataTypeMap = {};
      this.accounts = options.accounts;

      this.initFormObserver();
      this.bindFieldChangeObserver();


      if (!this.loginDisabledHere()) {
        this.tryToAutoFillLogins(options.doNotAutoFill || this.formObserver.ignoreFieldEvent('click'));
      }
    },

    cleanup: function() {
      if (this.formObserver) {
        this.formObserver.unbindFormEvents();
        this.formObserver.off();
        this.formObserver = null;
      }
    },


    // Looks for visible, not-readonly and not-disabled identity fields.
    // If can't find any, call usual loginField handler.
    onPasswordFieldActivity: function(fieldDataTypeMap, e){

      var hookPasswordEvents = !_.find(['/contact/email', '/nameperson/friendly'], function(type){
        if (type in fieldDataTypeMap) {
          var element = fieldDataTypeMap[type].element;
          if (abineFormAnalyzer.isVisible(element)
             && !element.hasAttribute('readonly')
             && !element.hasAttribute('disabled')) {
            return true;
          }
        }
        return false

      });

      if (hookPasswordEvents){
        this.onLoginFieldActivity(e);
      }

    },

    hookEvents: function(fieldDataTypeMap) {
      var emailHandler = _.bind(this.onLoginFieldActivity, this);
      var passwordHandler = _.bind(this.onPasswordFieldActivity, this, fieldDataTypeMap);

      this.formObserver.on('emailFieldFocused', emailHandler);
      this.formObserver.on('emailFieldClicked', emailHandler);
      this.formObserver.on('emailFieldIconClicked', emailHandler);

      this.formObserver.on('loginFieldFocused', emailHandler);
      this.formObserver.on('loginFieldClicked', emailHandler);
      this.formObserver.on('loginFieldIconClicked', emailHandler);

      this.formObserver.on('usernameFieldClicked', emailHandler);
      this.formObserver.on('usernameFieldFocused', emailHandler);
      this.formObserver.on('usernameFieldIconClicked', emailHandler);

      var fieldBlurHandler = _.bind(this.onFieldBlurred, this);
      this.formObserver.on('emailFieldBlurred', fieldBlurHandler);
      this.formObserver.on('usernameFieldBlurred', fieldBlurHandler);
      this.formObserver.on('loginFieldBlurred', fieldBlurHandler);

      this.formObserver.on('passwordFieldFocused', passwordHandler);
      this.formObserver.on('passwordFieldClicked', passwordHandler);
      this.formObserver.on('passwordFieldIconClicked', passwordHandler);
      this.formObserver.on('passwordFieldBlurred', fieldBlurHandler);

      this.bindUnhandledFields();
    },

    // ### function tryToAutoFillLogins
    // Look for a valid account on this site, and fill it with the first one we find
    // Eventually, improve this to use the most recently used account
    tryToAutoFillLogins: function(doNotAutoFill) {
      LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:loginHelper:tryToAutoFillLogins enter");
      var fieldsToFill = this.formObserver.getFields();

      if (!this.contentManager.domain) {
        LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:loginHelper:tryToAutoFillLogins return");
        return; // we don't know where we are
      }

      var fieldDataTypeMap = this.fieldsToFieldDataTypeMap(fieldsToFill);
      this.fieldDataTypeMap = fieldDataTypeMap;

      this.hookEvents(fieldDataTypeMap);

      var pageHost = this.contentManager.host;

      var accounts = this.accounts;
      accounts.comparator = function(a){
        var modifiedAt = a.get('modifiedAt');
        if (modifiedAt) {
          modifiedAt = Date.parse(modifiedAt);
        } else {
          modifiedAt = 0;
        }
        var userModifiedAt = a.get('userModifiedAt');
        if (userModifiedAt) {
          userModifiedAt = Date.parse(userModifiedAt);
        } else {
          userModifiedAt = 0;
        }
        var recentChange = Math.max(modifiedAt, userModifiedAt);
        return -recentChange;
      };
      this.accountsLength = accounts.length;
      accounts.sort();

      if (this.contentManager.isLocked() || (!accounts || accounts.length == 0)) {
        LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:loginHelper:tryToAutoFillLogins return");
        return; // no accounts or we're locked so don't fill
      }

      if ('/contact/email' in fieldDataTypeMap) {
        this.disableAutoComplete(fieldDataTypeMap['/contact/email'].element);
      }
      if ('/nameperson/friendly' in fieldDataTypeMap) {
        this.disableAutoComplete(fieldDataTypeMap['/nameperson/friendly'].element);
      }

      if (doNotAutoFill) {
        return;
      }

      if ('/contact/email' in fieldDataTypeMap || '/nameperson/friendly' in fieldDataTypeMap) {
        // check if browser has already autofilled
        var currentValue = (fieldDataTypeMap['/contact/email'] || fieldDataTypeMap['/nameperson/friendly']).element.value;
        if (currentValue != '') {
          var filledByDisposable = false;
          _.each(accounts, function(disposable){
            if (disposable.getAddress() == currentValue) {
              filledByDisposable = true;
            }
          });
          if (!filledByDisposable) {
            // browser has already filled email/username field
            if ('/password' in fieldDataTypeMap && fieldDataTypeMap['/password'].element.value != '') {
              // password is also filled, so we should not autofill with disposable
              LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:loginHelper:tryToAutoFillLogins browser autofilled return");
              return;
            }
          }
        }
      }

      var account = null;
      var formProtocol = abineFormAnalyzer.getProtocol(this.formEl, this.contentManager.href);
      var form_host = abineFormAnalyzer.getFormDomain(this.formEl, this.contentManager.href);
      var page_host = abineFormAnalyzer.getPageDomain(this.contentManager.href);
      var pageTld = abineUrl.getTLD(page_host);
      var formTLDMatchesPageTLD = form_host == page_host || abineUrl.getTLD(form_host) == pageTld || form_host == '';

      var autoLogin = false, autoLoginSubmitBtn = null;
      if (this.contentManager.preferences.getPref('autoLoginHost') == pageTld) {
        autoLogin = true;
        var autoLoginId = this.contentManager.preferences.getPref('autoLoginAccount');
        autoLoginSubmitBtn = this.contentManager.preferences.getPref('autoLoginSubmitBtn');
        this.contentManager.preferences.removePref('autoLoginHost');
        this.contentManager.preferences.removePref('autoLoginEvents');
        this.contentManager.preferences.removePref('autoLoginAccount');
        account = accounts.find(function(acc) {
          return (acc.get('id') == autoLoginId)
        });
      } else {
        var isPageHttp = this.contentManager.href.indexOf('https') != 0;
        // filter accounts that can be auto-filled
        var filteredAccounts = [];
        accounts.find(_.bind(function(acc){
          if (!acc.get('protocol') || acc.get('protocol') == '') {
            // protocol not set, so fill only if its http and page/form domain matches
            if (!formTLDMatchesPageTLD || isPageHttp || formProtocol != 'https') {
              return;
            }
          } else if (acc.get('protocol') == 'https' && 'https' != formProtocol || // cannot fill https account in http form
            (form_host && ((acc.get('form_host') && abineUrl.getTLD(acc.get('form_host')) != abineUrl.getTLD(form_host)) || acc.get('domain') != abineUrl.getTLD(form_host))) || // form host exists and does not match, so do not autofill
            abineUrl.getTLD(acc.get('page_host')) != abineUrl.getTLD(page_host) // page domain does not match, so do not autofill
            ) {
            return;
          }
          filteredAccounts.push(acc);
        }, this));
        // preference to the first account with matching page_host (eg: jira.abine.com)
        account = _.find(filteredAccounts, function(acc){
          return pageHost == acc.get('page_host');
        });
        if (!account) {
          // default to most recently used account
          account = filteredAccounts[0];
        }
      }

      if (!account || account.get('mfa')) {
        // do not autofill 2fa enabled accounts
        return;
      }

      var accountDataTypeMap = {};
      accountDataTypeMap['/contact/email'] = account.get('email');
      accountDataTypeMap['/nameperson/friendly'] = account.get('username');
      accountDataTypeMap['/password'] = account.get('password');
      var numFieldsFilled = 0;

      _.each(_.keys(fieldDataTypeMap), _.bind(function(fieldTypeToFill){
        var element = fieldDataTypeMap[fieldTypeToFill].element;
        if (fieldTypeToFill in accountDataTypeMap && accountDataTypeMap[fieldTypeToFill].length > 0) {
          if(fieldTypeToFill === "/password" && element.type === "text"){
            $(element).focus();
            abineTimer.setTimeout(_.bind(function(){
              $("[type='password']",this.formEl).val(accountDataTypeMap[fieldTypeToFill]).blur();
            },this),100);
            fieldTypeToFill = null;
          }
        } else if (fieldTypeToFill == "/nameperson/friendly" && "/contact/email" in accountDataTypeMap) {
          fieldTypeToFill = "/contact/email";
        } else if (fieldTypeToFill == "/contact/email" && "/nameperson/friendly" in accountDataTypeMap) {
          fieldTypeToFill = "/nameperson/friendly";
        }

        if (fieldTypeToFill && fieldTypeToFill in accountDataTypeMap) {
          this.disableAutoComplete(element);
          var value = accountDataTypeMap[fieldTypeToFill];
          this.contentManager.filler.fillTextField(element, value);
          numFieldsFilled++;
        }
      },this));

      // required to fix sites that need manual typing
      var self = this;
      this.contentManager.filler.fillFields(this, 'login', null, {email: accountDataTypeMap['/contact/email'], username: accountDataTypeMap['/nameperson/friendly'], password: accountDataTypeMap['/password']}, this.contentManager.domain, null, function(){
        if (autoLogin &&
          self.contentManager.preferences.getPref("submitAccount") && // auto-submit not turned off globally
          !(self.contentManager.blacklist && !!self.contentManager.blacklist.get('submit_account')) // auto-submit not turned off on this site
        ) {
          var loginUrls = new models.LoginUrlCollection();
          loginUrls.fetchAllByDomain(self.contentManager.domain, function () {
            self.contentManager.filler.autoLogin(self.formEl, loginUrls);
          });
        }
      });

      if (numFieldsFilled >= 2) {
        this.contentManager.sendFormFillData(this);
        $(this.formEl).attr("maskMeFilled", "yes");

        // var times_used = account.get("times_used") || 0;
        // account.set("times_used", times_used + 1);

        account.set('pushed', 0);
        account.set('ignoreUserModifiedAt', true); // don't update usermodified at for this automatic action

        var url = account.get('login_url');
        if (!url || url.length == 0) {
          account.set('login_url', this.contentManager.href);
          account.save();
        } else {
          // do not save account for every auto fill
        }

        // BL: save PWM engagement as panel data
        // save autofill as impression
        this.contentManager.messenger.send('update:panelActivity:impression', {panelId: 7000004});
      }

      LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:loginHelper:tryToAutoFillLogins return");
    }
  });

  _.extend(LoginHelper.prototype, fieldChangeEventMixins);
  _.extend(LoginHelper.prototype, {onLoginFieldActivity: commonFormStrategyMixins.onLoginFieldActivity});


  var ChangePasswordHelper = BaseHelper.extend({

    initialize: function(options) {
      LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:initializeChangePasswordHelper enter "+this.id);
      if (!options.el) { throw("ChangePasswordHelper expects a form element"); }
      this.formEl = options.el;
      this.contentManager = options.contentManager;
      this.preferences = options.preferences;
      this.mappedFields = options.mappedFields;
      this.fieldDataTypeMap = {};
      this.accounts = options.accounts;

      this.initFormObserver();
      this.bindFieldChangeObserver();

      var fieldsToFill = this.formObserver.getFields();

      if (!this.contentManager.domain) {
        return; // we don't know where we are
      }

      var fieldDataTypeMap = this.fieldsToFieldDataTypeMap(fieldsToFill);
      this.fieldDataTypeMap = fieldDataTypeMap;

      this.hookEvents(fieldDataTypeMap);
    },

    hookEvents: function(fieldDataTypeMap) {
      var loginHandler = _.bind(this.onLoginFieldActivity, this);
      var passwordHandler = _.bind(this.onPasswordFieldActivity, this);


      this.formObserver.on('emailFieldFocused', loginHandler);
      this.formObserver.on('emailFieldClicked', loginHandler);
      this.formObserver.on('emailFieldIconClicked', loginHandler);

      this.formObserver.on('loginFieldFocused', loginHandler);
      this.formObserver.on('loginFieldClicked', loginHandler);
      this.formObserver.on('loginFieldIconClicked', loginHandler);

      this.formObserver.on('usernameFieldClicked', loginHandler);
      this.formObserver.on('usernameFieldFocused', loginHandler);
      this.formObserver.on('usernameFieldIconClicked', loginHandler);

      this.formObserver.on('oldPasswordFieldClicked', loginHandler);
      this.formObserver.on('oldPasswordFieldFocused', loginHandler);
      this.formObserver.on('oldPasswordFieldIconClicked', loginHandler);

      this.formObserver.on('passwordFieldClicked', passwordHandler);
      this.formObserver.on('passwordFieldFocused', passwordHandler);
      this.formObserver.on('passwordFieldIconClicked', passwordHandler);

      var fieldBlurHandler = _.bind(this.onFieldBlurred, this);
      this.formObserver.on('emailFieldBlurred', fieldBlurHandler);
      this.formObserver.on('usernameFieldBlurred', fieldBlurHandler);
      this.formObserver.on('passwordFieldBlurred', fieldBlurHandler);
      this.formObserver.on('oldPasswordFieldBlurred', fieldBlurHandler);
      this.formObserver.on('loginFieldBlurred', fieldBlurHandler);
    },

    cleanup: function() {
      if (this.formObserver) {
        this.formObserver.unbindFormEvents();
        this.formObserver.off();
        this.formObserver = null;
      }
    },

     // ### function onLoginFieldActivity(event)
    // Creates a change password login helper panel in response to an event on an email field or old password
    onLoginFieldActivity: function(e) {
      LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:changePasswordHelper:onLoginFieldActivity enter");

      if ((!this.canShowDNTMeHelper(e, 'login') || this.loginDisabledHere()) && e.type != 'iconclick') {
        LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:changePasswordHelper:onLoginFieldActivity return");
        return;
      }

      if (this.contentManager.isPanelShown(e.field)) {
        LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:changePasswordHelper:onLoginFieldActivity return");
        if (e.field.lastShownReason != 'showpanel') {
          this.contentManager.closeCurrentPanel();
        }
        return; // Panel for this field already open
      }

      // BL: 12/11/14 add check for split test for iconpanels. if we are in "click_icon_for_panel" split, then only show the panel on an icon click
      if((this.contentManager.preferences.getPref("useIconPanelMode") == responseCodes.SPLIT_ICON_PANELS_CLICK_ICON_FOR_PANEL) && e.type != 'iconclick'){
        LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:changePasswordHelper:onLoginFieldActivity return");
        return;
      }

      if (this.formObserver.ignoreFieldEvent(e.type)) {
        LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:changePasswordHelper:onLoginFieldActivity return");
        return;
      }

      var element = e.field.element;
      this.disableAutoComplete(element);

      if(!element.hasAttribute("mmautofilling") && (!element.hasAttribute("mmclosedfield") || e.type == 'iconclick')){

        e.field.lastShownReason = e.type;

        this.contentManager.createPanel({
          style: {},
          secured: 'hadEmail',
          field: e.field,
          view: "change-password-login",
          payload: {
            iconClick: e.type=='iconclick',
            fieldType: e.field.dataType,
            formType: this.formType,
            fieldId: e.field.signature,
            category: 'account',
            strategyId: this.id
          }
        });
      }
      LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:changePasswordHelper:onLoginFieldActivity return");
    },

    onPasswordFieldActivity: function(e) {
      LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:onPasswordFieldActivity enter");
      if (this.passwordDisabledHere() || !this.canShowDNTMeHelper(e, 'register')) {
        LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:onPasswordFieldActivity return");
        return; // Return if this preference is deactivated
      }
      if (this.contentManager.isPanelShown(e.field)) {
        LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:onPasswordFieldActivity return");
        if (e.field.lastShownReason != 'showpanel') {
          this.contentManager.closeCurrentPanel();
        }
        return; // Panel for this field already open
      }
      if (this.contentManager.preferences.getPref("hasRegistered") && !this.contentManager.preferences.getPref("authToken")) {
        LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:onPasswordFieldActivity return");
        return; // Return if there's no auth token because user got booted
      }

      if (this.formObserver.ignoreFieldEvent(e.type)) {
        LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:onPasswordFieldActivity return");
        return;
      }

      var element = e.field.element;
      if(element.getAttribute("type") == "text"){
        LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:onPasswordFieldActivity return");
        return; // don't open password panels for text fields :/
        //maybe we should trigger this event again in 50ms to detect type="password" switch?
      }

      this.disableAutoComplete(element);

      if(!element.hasAttribute("mmautofilling") && !element.hasAttribute("mmclosedfield") || e.type == 'iconclick'){

        var dynamicPanelObj = this.contentManager.processDynamicPanelForField("/contact/email");

        if(dynamicPanelObj.dynamicPanel && dynamicPanelObj.dynamicPanel.dontShow){
          return false;
        }

        e.field.lastShownReason = e.type;

        this.contentManager.createPanel({
          style: {},
          secured: 'hadEmail',
          field: e.field,
          view: "change-password",
          payload: {
            fieldType: e.field.dataType,
            formType: this.formType,
            iconClick: e.type=='iconclick',
            fieldId: e.field.signature,
            category: 'password',
            strategyId: this.id
          }
        });
      }
      LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:onPasswordFieldActivity return");
    }

  });

  _.extend(ChangePasswordHelper.prototype, fieldChangeEventMixins);

  var OtherHelper = BaseHelper.extend({

    // ### function initialize(options)
    // - `el` - the form element
    // - `contentManager` - the content manager
    // - `preferences` - an array of the user's preferences
    initialize: function(options) {
      LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:initializeOtherHelper enter "+this.id);
      if (!options.el) { throw("OtherHelper expects a form element"); }
      this.formEl = options.el;
      this.contentManager = options.contentManager;
      this.preferences = options.preferences;
      this.mappedFields = options.mappedFields;

      this.initFormObserver();
      this.bindFieldChangeObserver();
      this.bindFormObserver();
    }
  });

  _.extend(OtherHelper.prototype, commonFormStrategyMixins, fieldChangeEventMixins);


  var CheckoutHelper = BaseHelper.extend({

    // ### function initialize(options)
    // - `el` - the form element
    // - `contentManager` - the content manager
    // - `preferences` - an array of the user's preferences
    initialize: function(options) {
      LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:initializeCheckoutHelper enter "+this.id);
      if (!options.el) { throw("CheckoutHelper expects a form element"); }
      this.formEl = options.el;
      this.contentManager = options.contentManager;
      this.preferences = options.preferences;
      this.mappedFields = options.mappedFields;

      this.initFormObserver();
      this.bindFieldChangeObserver();
      this.bindFormObserver();
    }
  });

  _.extend(CheckoutHelper.prototype, commonFormStrategyMixins, fieldChangeEventMixins);


  var IgnoreHelper = BaseHelper.extend({

    // ### function initialize(options)
    // - `el` - the form element
    // - `contentManager` - the content manager
    // - `preferences` - an array of the user's preferences
    initialize: function(options) {
      if (!options.el) { throw("IgnoreHelper expects a form element"); }
      this.formEl = options.el;
      this.contentManager = options.contentManager;
      this.preferences = options.preferences;
      this.mappedFields = options.mappedFields;
      this.bindUnhandledFields();
    }
  });

  _.extend(IgnoreHelper.prototype, commonFormStrategyMixins, fieldChangeEventMixins);

  return {
    RegistrationHelper: RegistrationHelper,
    LoginHelper: LoginHelper,
    ChangePasswordHelper: ChangePasswordHelper,
    OtherHelper: OtherHelper,
    CheckoutHelper: CheckoutHelper,
    IgnoreHelper: IgnoreHelper
  };

});
ABINE_DNTME.define("abine/phoneHeuristics",
    ["documentcloud/underscore", "jquery"],
  function(_, $) {
    return (function () {

  // dependency:  _

  // Linear search through an array, starting at the given index
  function fastFindByProp(array, property, desiredValue, startIndex) {
    startIndex = startIndex || 0;
    var len = array.length;
    for (var i = startIndex; i < len; i++) {
      var obj = array[i];
      if (!obj) continue;
      if (obj[property] === desiredValue) {
        return obj;
      }
    }
    return null;
  }

  var AbinePhoneHeuristics = {
    apply: function (fields, orderedFields) {
      var hasPhoneFields = false;
      for (var i in fields) {
        if (fields[i].dataType && fields[i].dataType.indexOf('contact/phone') > 0) {
          hasPhoneFields = true;  // Only run phone post-processing if necessary
          break;
        }
      }

      // Post process for phone optimizations:
      // - also sending ordered array for position lookup
      // - avoid most post processing of phones if there are none
      this.fixUnknownPhoneFieldsWithMaxLengths(fields, orderedFields);
      this.fixUnknownPhoneFieldsWithSizes(fields, orderedFields);
      if (hasPhoneFields) {
        this.fixUnknownPhoneFieldsWithAreaCode(fields, orderedFields);
        this.fixThreePhoneFieldsInnaRow(fields, orderedFields);
        this.fixOrphanedPhoneFields(fields, orderedFields);
        this.fixExtension(fields, orderedFields);
      }
    },

    // If two unknown fields follow an area code then assign them to exchange and local
    fixUnknownPhoneFieldsWithAreaCode: function (fields, orderedFields, startIndex) {
      var exchangeField, localField, startIndex = startIndex || 1;
      var areaCodeField = fastFindByProp(orderedFields, 'dataType', '/contact/phone/areacode', startIndex);
      if (!areaCodeField) {
        return fields; // no more potential matches
      }
      var exchangeFieldKey = (orderedFields[areaCodeField.position + 1]) ? orderedFields[areaCodeField.position + 1].key : null;
      var localFieldKey = (orderedFields[areaCodeField.position + 2]) ? orderedFields[areaCodeField.position + 2].key : null;
      if (!exchangeFieldKey || !localFieldKey) {
        return fields;
      }
      if (fields[exchangeFieldKey].dataType == null && fields[localFieldKey].dataType == null) {
        fields[exchangeFieldKey].dataType = '/contact/phone/exchange';
        fields[localFieldKey].dataType = '/contact/phone/local';
        LOG_FINEST && ABINE_DNTME.log.finest("phoneHeuristics:fixUnknownPhoneFieldsWithAreaCode set exchange/local field types");
      }
      // Recurse through rest of the fields
      return this.fixUnknownPhoneFieldsWithAreaCode(fields, orderedFields, startIndex + 3);
    },

    // If three unknown fields have maxlengths of 3, 3, 4 then assign them to area, exchange, local
    fixUnknownPhoneFieldsWithMaxLengths: function (fields, orderedFields, startIndex) {
      startIndex = startIndex || 1;
      var areaCodeField = fastFindByProp(orderedFields, 'maxlength', 3, startIndex);
      if (!areaCodeField) {
        return fields; // no more potential matches
      }
      var exchangeFieldKey = (orderedFields[areaCodeField.position + 1]) ? orderedFields[areaCodeField.position + 1].key : null;
      var localFieldKey = (orderedFields[areaCodeField.position + 2]) ? orderedFields[areaCodeField.position + 2].key : null;
      if (!exchangeFieldKey || !localFieldKey) {
        return fields;
      }
      if (fields[exchangeFieldKey].maxlength === 3 && fields[localFieldKey].maxlength === 4) {
        fields[areaCodeField.key].dataType = '/contact/phone/areacode';
        fields[exchangeFieldKey].dataType = '/contact/phone/exchange';
        fields[localFieldKey].dataType = '/contact/phone/local';
        LOG_FINEST && ABINE_DNTME.log.finest("phoneHeuristics:fixUnknownPhoneFieldsWithMaxLengths mark 3,3,4 maxlength fields as phone");
      }
      // Recurse through rest of the fields
      return this.fixUnknownPhoneFieldsWithMaxLengths(fields, orderedFields, startIndex + 3);
    },

    // If three unknown fields have sizes of 3, 3, 4 then assign them to area, exchange, local
    fixUnknownPhoneFieldsWithSizes: function (fields, orderedFields, startIndex) {
      startIndex = startIndex || 1;
      var areaCodeField = fastFindByProp(orderedFields, 'size', 3, startIndex);
      if (!areaCodeField) {
        return fields; // no more potential matches
      }
      var exchangeFieldKey = (orderedFields[areaCodeField.position + 1]) ? orderedFields[areaCodeField.position + 1].key : null;
      var localFieldKey = (orderedFields[areaCodeField.position + 2]) ? orderedFields[areaCodeField.position + 2].key : null;
      if (!exchangeFieldKey || !localFieldKey) {
        return fields;
      }
      if (fields[exchangeFieldKey].size === 3 && fields[localFieldKey].size === 4) {
        fields[areaCodeField.key].dataType = '/contact/phone/areacode';
        fields[exchangeFieldKey].dataType = '/contact/phone/exchange';
        fields[localFieldKey].dataType = '/contact/phone/local';
        LOG_FINEST && ABINE_DNTME.log.finest("phoneHeuristics:fixUnknownPhoneFieldsWithSizes mark 3,3,4 size fields as phone");
      }
      // Recurse through rest of the fields
      return this.fixUnknownPhoneFieldsWithSizes(fields, orderedFields, startIndex + 3);
    },

    // If three similar phone fields are in a row, and the second two don't have labels, assign them to exchange and local
    fixThreePhoneFieldsInnaRow: function (fields, orderedFields) {
      var areaCodeFields = _(orderedFields).filter(function (f) {
        if (!f) return false;
        return (f.dataType && f.dataType.indexOf("contact/phone") > 0);
      });
      _(areaCodeFields).each(function (areaCodeField) {
        if (areaCodeField.dataType && areaCodeField.dataType === '/contact/phone/exchange') {
          return;
        }
        if (areaCodeField.dataType && areaCodeField.dataType === '/contact/phone/local') {
          return;
        }
        var exchangeFieldKey = (orderedFields[areaCodeField.position + 1]) ? orderedFields[areaCodeField.position + 1].key : null;
        var localFieldKey = (orderedFields[areaCodeField.position + 2]) ? orderedFields[areaCodeField.position + 2].key : null;
        if (!exchangeFieldKey || !localFieldKey) {
          return
        }
        var exchangeField = fields[exchangeFieldKey];
        var localField = fields[localFieldKey];
        if (exchangeField.dataType === areaCodeField.dataType &&
          localField.dataType === areaCodeField.dataType &&
          exchangeField.label == null && localField.label == null) {
          fields[areaCodeField.key].dataType = '/contact/phone/areacode';
          fields[exchangeFieldKey].dataType = '/contact/phone/exchange';
          fields[localFieldKey].dataType = '/contact/phone/local';
          LOG_FINEST && ABINE_DNTME.log.finest("phoneHeuristics:fixThreePhoneFieldsInnaRow mark 3 fields in a row with no label for last 2 as phone");
        }
      });
      return fields
    },

    // Don't leave phone exchange or local dangling without all three fields present
    fixOrphanedPhoneFields: function (fields, orderedFields) {
      var areaCodeFields = _(fields).filter(function (f) {
        return (f.dataType && f.dataType.indexOf('/contact/phone/areacode') != -1);
      });
      var exchangeFields = _(fields).filter(function (f) {
        return (f.dataType && f.dataType.indexOf('/contact/phone/exchange') != -1);
      });
      var localFields = _(fields).filter(function (f) {
        return (f.dataType && f.dataType.indexOf('/contact/phone/local') != -1);
      });
      if (areaCodeFields.length === 0 && (exchangeFields.length > 0 || localFields.length > 0)) {
        // Something's not cool. Take all the exchanges and locals and make them full phones
        _(exchangeFields).each(function (f) {
          f.dataType = '/contact/phone';
        });
        _(localFields).each(function (f) {
          f.dataType = '/contact/phone';
        });
        LOG_FINEST && ABINE_DNTME.log.finest("phoneHeuristics:fixOrphanedPhoneFields no area code, change exchange/local fields to full phone");
      }
      var phoneFields = _(fields).filter(function (f) {
        return (f.dataType && f.dataType === "/contact/phone");
      });
      if (areaCodeFields.length > 0 && phoneFields.length > 0) {
        // Since there is an area code, those phones are probably only for the last 7 digits
        _(phoneFields).each(function (f) {
          f.dataType = '/contact/phone/number';
        });
        LOG_FINEST && ABINE_DNTME.log.finest("phoneHeuristics:fixOrphanedPhoneFields has area code, change full phone field to 7 digit phone");
      }
    },

    // field after phone with "ext" in it must be extension
    fixExtension: function (fields, orderedFields) {
      var prevType = null;
      _.each(orderedFields, function (field) {
        if (!field.dataType && prevType && prevType.match(/\/contact\/phone/i)) {
          if ((field.id + ":" + field.name).match(/ext/i)) {
            field.dataType = prevType.replace(/\/(number|local)/g, '').replace(/\/extension/g, '') + '/extension';
            LOG_FINEST && ABINE_DNTME.log.finest("phoneHeuristics:fixExtension set type to " + field.dataType + " for " + field.signature);
          }
        }
        prevType = field.dataType;
      });
    }

  };

  return AbinePhoneHeuristics;

}).apply(this, []);
;
  });
ABINE_DNTME.define("abine/paymentHeuristics",
    ["documentcloud/underscore", "jquery"],
  function(_, $) {
    return (function () {

  // dependency:  _, $

  // Linear search through an array, starting at the given index
  function fastFindByProp(array, property, desiredValue, startIndex) {
    startIndex = startIndex || 0;
    var len = array.length;
    for (var i = startIndex; i < len; i++) {
      var obj = array[i];
      if (!obj) continue;
      if (obj[property] === desiredValue) {
        return i;
      }
    }
    return null;
  }

  var AbinePaymentHeuristics = {
    apply: function (fields, orderedFields) {
      var hasPaymentFields = false;
      for (var i in fields) {
        if (fields[i].dataType && fields[i].dataType.indexOf('/financial') == 0) {
          hasPaymentFields = true;  // Only run payment post-processing if necessary
          break;
        }
      }

      // Post process for payment form optimizations:
      if (hasPaymentFields) {

        this.fixMissingPaymentFields(fields, orderedFields);

        this.fixOrphanedPaymentFields(fields, orderedFields);

        this.fixDuplicatePaymentFields(fields, orderedFields);
      }
    },

    fixMissingPaymentFields: function (fields, orderedFields) {
      var numberFieldType = '/financial/creditcard/number';
      var numberFields = _(fields).filter(function (f) {
        return (f.dataType && f.dataType === numberFieldType);
      });

      if (numberFields.length == 0) {
        // missing card number field, try to auto detect (look for text field with maxlength 16 OR type=tel before expiry month)
        var expiryMonthIdx = fastFindByProp(orderedFields, 'dataType', '/financial/creditcard/expirymonth', 0);

        for (var i = expiryMonthIdx - 1; i >= 0; i--) {
          if (typeof(orderedFields[i]) != 'undefined') {

            if (!orderedFields[i].dataType) {
              var nameOrId = orderedFields[i].name + ":" + orderedFields[i].id;
              if (orderedFields[i].element.getAttribute('maxlength') >= 16 ||
                (nameOrId && nameOrId.match(/num|card/i) && !nameOrId.match(/cvs|cv|code|sec/i))
              ) {
                orderedFields[i].dataType = '/financial/creditcard/number';
                numberFields.push(orderedFields[i]);
                LOG_FINEST && ABINE_DNTME.log.finest("set type to /financial/creditcard/number for " + orderedFields[i].signature);
                break;
              }
            } else if (orderedFields[i].dataType == '/contact/phone') {
              // many sites use "tel" for card number for easy input in mobile
              orderedFields[i].dataType = '/financial/creditcard/number';
              LOG_FINEST && ABINE_DNTME.log.finest("set type to /financial/creditcard/number for " + orderedFields[i].signature);
              break;
            }
          }
        }
      }

      if (numberFields.length == 0) {
        // check if it has 4 consecutive fields with max-length 4 (split card number)
        var startIndex = -1, numFields = 0;
        for (var i = 0; i < orderedFields.length; i++) {
          if (typeof(orderedFields[i]) != 'undefined' && !orderedFields[i].dataType) {
            if (orderedFields[i].element.getAttribute('maxlength') == 4) {
              if (startIndex == -1) {
                startIndex = i;
              }
              numFields++;
              if (numFields == 4) break;
            } else if (numFields < 4) {
              startIndex = -1;
              numFields = 0;
            }
          }
        }

        if (numFields == 4) {
          for (var i = startIndex; i < startIndex + 4; i++) {
            orderedFields[i].dataType = '/financial/creditcard/number/part' + (i - startIndex + 1);
            numberFields.push(orderedFields[i]);
          }
          numberFieldType = '/financial/creditcard/number/part4';
        }
      }

      if (numberFields.length > 0) {
        var issuedToFields = _(fields).filter(function (f) {
          return (f.dataType && f.dataType.indexOf('/financial/creditcard/issued') == 0);
        });

        if (issuedToFields.length == 0) {
          // any full name field before it is issuedTo
          var numIdx = fastFindByProp(orderedFields, 'dataType', numberFieldType, 0);
          var fullNameIdx = fastFindByProp(orderedFields, 'dataType', '/nameperson/full', numIdx - 1);
          if (fullNameIdx != null && fullNameIdx <= numIdx + 1) {
            orderedFields[fullNameIdx].dataType = '/financial/creditcard/issuedto';
            LOG_FINEST && ABINE_DNTME.log.finest("paymentHeuristics: changed fullname to issuedto");
          } else {
            // any nameperson field between number and expiry or close to number and expiry
            var nameIdx = fastFindByProp(orderedFields, 'dataType', '/nameperson/friendly', numIdx - 1);
            var expiryIdx = -1;
            _(orderedFields).each(function(f, idx){
              if (idx >= numIdx -1) {
                if (f.dataType && f.dataType.indexOf('/financial/creditcard/expiry') != -1) {
                  expiryIdx = idx;
                }
              }
            });
            if (nameIdx != null && nameIdx <= expiryIdx+1) {
              orderedFields[nameIdx].dataType = '/financial/creditcard/issuedto';
              LOG_FINEST && ABINE_DNTME.log.finest("paymentHeuristics: changed username to issuedto");
            }
          }
        }

        var expiryFields = _(fields).filter(function (f) {
          return (f.dataType && f.dataType.match(/\/financial\/creditcard\/(expirymonth|expiryyear)/i));
        });

        var expiryDateFields = _(fields).filter(function (f) {
          return (f.dataType && f.dataType.match(/\/financial\/creditcard\/expiry[2]?$/i));
        });

        if (expiryFields.length < 2 && expiryDateFields.length == 0) {

          var expiryMonthFields = _(fields).filter(function (f) {
            return (f.dataType && f.dataType.indexOf('/financial/creditcard/expirymonth') == 0);
          });
          if (expiryMonthFields.length == 0) {
            // missing expiry month field, try to auto detect
            var numberFieldIdx = fastFindByProp(orderedFields, 'dataType', numberFieldType, 0);
            // check for any select box after number field with "month"
            for (var i = numberFieldIdx + 1, numSelectFields = 0; i < orderedFields.length && numSelectFields < 4; i++) {
              if (orderedFields[i].fieldType == 'select') numSelectFields++;
              if (orderedFields[i].element && orderedFields[i].element.innerHTML.match(/jan|feb|dec|\b12\b|\b11\b/i)) {
                orderedFields[i].dataType = '/financial/creditcard/expirymonth';
                expiryMonthFields.push(orderedFields[i]);
                LOG_FINEST && ABINE_DNTME.log.finest("paymentHeuristics: detected expiry month by inner html");
                break;
              }
            }
          }

          var expiryYearFields = _(fields).filter(function (f) {
            return (f.dataType && f.dataType.indexOf('/financial/creditcard/expiryyear') == 0);
          });
          if (expiryYearFields.length == 0) {
            // missing expiry year field, try to auto detect
            var numberFieldIdx = fastFindByProp(orderedFields, 'dataType', numberFieldType, 0);
            // check for any select box after number field with "year"
            for (var i = numberFieldIdx + 1, numSelectFields = 0; i < orderedFields.length && numSelectFields < 5; i++) {
              if (orderedFields[i].fieldType == 'select') numSelectFields++;
              if (orderedFields[i].element && orderedFields[i].element.innerHTML.match(/20[0-9]{2}|year/i)) {
                orderedFields[i].dataType = '/financial/creditcard/expiryyear';
                expiryYearFields.push(orderedFields[i]);
                LOG_FINEST && ABINE_DNTME.log.finest("paymentHeuristics: detected expiry year by inner html");
                break;
              }
            }
          }
        }

        var cvvFields = _(fields).filter(function (f) {
          return (f.dataType && f.dataType.indexOf('/financial/creditcard/verification') == 0);
        });
        if (cvvFields.length == 0) {
          // missing cvv field, try to auto detect
          var numberFieldIdx = fastFindByProp(orderedFields, 'dataType', numberFieldType, 0);
          // check for any select box after number field with "cvv"
          for (var i = numberFieldIdx + 1; i < orderedFields.length && i < numberFieldIdx + 5; i++) {
            if (
              (orderedFields[i].name && orderedFields[i].name.match(/cvv|cid|cvs/i)) ||
              (orderedFields[i].id && orderedFields[i].id.match(/cvv|cid|cvs/i))
            ) {
              orderedFields[i].dataType = '/financial/creditcard/verification';
              break;
            }
          }
        }

      }

    },

    // retain only the first cluster of credit card fields and clear all other card fields.
    fixDuplicatePaymentFields: function (fields, orderedFields) {
      var hasDuplicateCardFields = false, start = 0, seenTypes = {};

      // check if there are duplicate fields
      for (var i in fields) {
        var dataType = fields[i].dataType;
        if (!dataType || dataType.indexOf('/financial') != 0) continue;
        if (dataType in seenTypes) {
          hasDuplicateCardFields = true;
          break;
        }
        seenTypes[dataType] = 1;
      }

      while (hasDuplicateCardFields) {
        hasDuplicateCardFields = false;

        var numberIdx = fastFindByProp(orderedFields, 'dataType', '/financial/creditcard/number', start);
        if (numberIdx == null) break;

        var cvvIdx = fastFindByProp(orderedFields, 'dataType', '/financial/creditcard/verification', numberIdx + 1);
        var monthIdx = fastFindByProp(orderedFields, 'dataType', '/financial/creditcard/expirymonth', numberIdx + 1);
        var yearIdx = fastFindByProp(orderedFields, 'dataType', '/financial/creditcard/expiryyear', numberIdx + 1);

        var nextNumberIdx = fastFindByProp(orderedFields, 'dataType', '/financial/creditcard/number', numberIdx + 1);
        if (nextNumberIdx !== null && $(orderedFields[nextNumberIdx].element).is(':visible')) {
          hasDuplicateCardFields = true;
          if (nextNumberIdx > yearIdx || nextNumberIdx > monthIdx) {
            orderedFields[nextNumberIdx].dataType = null;
          } else {
            // all card fields before this card-number field must be cleared
            // exclude card-holder and card-type as they normally appear before card-number
            for (var i = 1; i < nextNumberIdx; i++) {
              if (orderedFields[i].dataType &&
                orderedFields[i].dataType.indexOf('/financial') == 0 &&
                orderedFields[i].dataType.indexOf('/issuedto') == -1 &&
                orderedFields[i].dataType.indexOf('/type') == -1)
                orderedFields[i].dataType = null;
            }

            start = numberIdx;
            continue;
          }
        }

        var nextCvvIdx = fastFindByProp(orderedFields, 'dataType', '/financial/creditcard/verification', cvvIdx + 1);
        if (nextCvvIdx !== null) {
          hasDuplicateCardFields = true;
          orderedFields[nextCvvIdx].dataType = null;
          LOG_FINEST && ABINE_DNTME.log.finest("paymentHeuristics: cleared duplicate CVV field type");
        }

        var nextMonthIdx = fastFindByProp(orderedFields, 'dataType', '/financial/creditcard/expirymonth', monthIdx + 1);
        if (nextMonthIdx !== null) {
          hasDuplicateCardFields = true;
          orderedFields[nextMonthIdx].dataType = null;
          LOG_FINEST && ABINE_DNTME.log.finest("paymentHeuristics: cleared duplicate expiry-month field type");
        }

        var nextYearIdx = fastFindByProp(orderedFields, 'dataType', '/financial/creditcard/expiryyear', yearIdx + 1);
        if (nextYearIdx !== null) {
          hasDuplicateCardFields = true;
          orderedFields[nextYearIdx].dataType = null;
          LOG_FINEST && ABINE_DNTME.log.finest("paymentHeuristics: cleared duplicate expiry-year field type");
        }

      }
    },

    // Don't leave any payment field dangling without all required payment fields
    fixOrphanedPaymentFields: function (fields, orderedFields) {
      var cvvFields = _(fields).filter(function (f) {
        return (f.dataType && f.dataType === '/financial/creditcard/verification');
      });
      var typeFields = _(fields).filter(function (f) {
        return (f.dataType && f.dataType === '/financial/creditcard/type');
      });
      var numberFields = _(fields).filter(function (f) {
        return (f.dataType && (f.dataType === '/financial/creditcard/number' || f.dataType === '/financial/creditcard/number/part4'));
      });
      var issuedToFields = _(fields).filter(function (f) {
        return (f.dataType && f.dataType === '/financial/creditcard/issuedto');
      });
      var expiryFields = _(fields).filter(function (f) {
        return (f.dataType && f.dataType.indexOf('/financial/creditcard/expiry') == 0);
      });

      // normally captcha or some other verification field in a form is wrongly recognized as CVV
      if (cvvFields.length > 0 && numberFields.length == 0) {
        _(cvvFields).each(function (f) {
          f.dataType = null;
        });
      }

      // ignore issuedto without card number
      if (issuedToFields.length > 0 && numberFields.length == 0) {
        _(issuedToFields).each(function (f) {
          f.dataType = null;
        });
      }

      // ignore type without card number
      if (typeFields.length > 0 && numberFields.length == 0) {
        _(typeFields).each(function (f) {
          f.dataType = null;
        });
      }

      // month/year field without card number is probably some other DATE field
      if (expiryFields.length > 0 && numberFields.length == 0) {
        _(expiryFields).each(function (f) {
          f.dataType = null;
        });
      }

      // ignore card number without expiry, CVV and issuedTo fields
      if (numberFields.length > 0 && expiryFields.length == 0 && cvvFields.length == 0 && issuedToFields.length == 0) {
        _(numberFields).each(function (f) {
          f.dataType = null;
        });
      }
    }

  };

  return AbinePaymentHeuristics;

}).apply(this, []);
;
  }
);
ABINE_DNTME.define("abine/addressHeuristics",
  ["documentcloud/underscore", "jquery"],
  function (_, $) {
    return (function () {

  // dependency:  _

  var AbineAddressHeuristics = {
    apply: function (fields, orderedFields) {
      var billingShippingFound = false;
      var addressFields = [];
      _.each(orderedFields, function(field) {
        if (field.dataType) {
          if (field.dataType.match(/contact\/(state|city|country|postal)/)) {
            addressFields.push(field);
          }
          if (field.dataType.match(/billing|shipping|home|business/i)) {
            billingShippingFound = true;  // Only run address post-processing if necessary
          }
        }
      });

      if (billingShippingFound) {
        this.fixAddressFields('billing', fields, addressFields);
        this.fixAddressFields('shipping', fields, addressFields);
        this.fixAddressFields('home', fields, addressFields);
        this.fixAddressFields('business', fields, addressFields);
      }
    },

    groupAddressFields: function(fields, orderedFields) {
      var addressFields = [];
      _.each(orderedFields, function(field) {
        if (field.dataType) {
          if (field.dataType.match(/contact\/(state|city|country|postal)/)) {
            addressFields.push(field);
          }
        }
      });

      // mark groups of address fields so that we can fill only the group that has focus.
      var group = 0, firstField = null;
      _.each(addressFields, function(fld){
        var dataType = fld.dataType.replace(/\/(billing|shipping|home|business)/i, '');
        if (!firstField) {
          firstField = dataType;
        } else if (firstField == dataType) {
          group++;
        }
        fld.addressGroup = group;
      });
      if (group > 0) {
        _.each(addressFields, function(fld) {
          fld.multipleAddress = true;
        });
      }
    },

    // fix non-billing/shipping address in between other billing-shipping address,
    fixAddressFields: function (suffix, fields, orderedFields) {
      LOG_FINEST && ABINE_DNTME.log.finest("addressHeuristics:fixAddressFields enter " + suffix);
      var startIndex = -1, endIndex = -1, anotherSuffixFound = false, numSameSuffixFields = 0, anotherSuffixIdx = -1;
      _.each(orderedFields, function (field, idx) {
        if (anotherSuffixFound) return;
        if (field.dataType.indexOf(suffix) != -1) {
          numSameSuffixFields++;
          if (startIndex == -1) {
            startIndex = idx;
          } else {
            endIndex = idx;
          }
        } else if (field.dataType.match(/\/(billing|shipping|home|business)/)) {
          anotherSuffixFound = true;
          anotherSuffixIdx = idx;
        }
      });
      if (startIndex >= 0 && (endIndex > 0 || numSameSuffixFields == 1)) {
        _.each(orderedFields, function (field, idx) {
          // ignore fields before startIndex
          if (idx < startIndex) return;

          // ignore fields after endIdex
          if (endIndex > 0 && idx > endIndex) return;

          // only start field has suffix, so change suffix of all address fields till a different suffix is found
          if (endIndex == -1 && (anotherSuffixFound && idx >= anotherSuffixIdx)) return;

          if (field.dataType && field.dataType.indexOf(suffix) == -1) {
            var newType = field.dataType.replace(/\/(billing|shipping|home|business)/, '') + '/' + suffix;
            LOG_FINEST && ABINE_DNTME.log.finest("addressHeuristics:fixAddressFields changed " + field.dataType + " to " + newType);
            field.dataType = newType;
          }
        });
      }
      LOG_FINEST && ABINE_DNTME.log.finest("addressHeuristics:fixAddressFields leave " + suffix);
    }
  };

  return AbineAddressHeuristics;
}).apply(this, []);
;
  });

// implementation of page-event handler used inside content script

ABINE_DNTME.define('abine/pageEventsImpl',
  ['documentcloud/underscore', 'abine/core', 'abine/proxies', 'jquery', 'abine/url', 'abine/timer'],
  function (_, abineCore, proxies, $, abineUrl, abineTimer) {

    var messenger = null;

    var secure = true;

    // listen for events only from *.abine.com
    if (ABINE_DNTME.config.environment == 'development') {
      var allowedHostsRegex = /.*/i;
      var allowedProtocol = /^http[s]?:\/\//;
    } else {
      var allowedHostsRegex = /^(.+\.)?abine\.com$/i;
      var allowedProtocol = /^https:\/\//;
    }

    function handlePageEvent(event) {
      var target = event.target;
      var doc = target.ownerDocument;
      var action = target.getAttribute("action");
      var params = target.getAttribute("params") || target.getAttribute('param');

      var expectsResponse = target.getAttribute("expectsResponse");

      if(!expectsResponse) {
        if(target.parentNode){
          target.parentNode.removeChild(target);
        }
      }

      if (params) {
        try{
          params = JSON.parse(params);
        } catch(e){
          params = {};
        }
      } else {
        params = {};
      }

      if (action.indexOf('AuthToken') != -1) {
        this.trigger('disable:form:helpers', doc);
      }

      if (action == "dnt_ping") {
        if (ABINE_DNTME.maskme_installed) {
          // do not respond to webapp ping message when maskme is installed.
          return;
        }
        var evt = doc.createEvent("Event");
        evt.initEvent("dnt_pong",true,true);
        doc.dispatchEvent(evt);
      } else {
        var response_event = doc.createEvent("Event");
        response_event.initEvent("received:"+action, true, true);
        doc.dispatchEvent(response_event);
      }

      var callback = function(){
        if (action == 'setAuthToken') {
          proxies.pageEventsModule.processAction({action: 'refreshTag', host: host, params: {}, secure: secure}, function(tag){
            if (tag) {
              ABINE_DNTME.config.updateTag(tag, target.ownerDocument);
            }
          });
        }
      };

      if (expectsResponse) {
        callback = function(data){
          var response_event = doc.createEvent("Event");
          response_event.initEvent("data:"+action, true, true);
          target.setAttribute("_data_from_extension", JSON.stringify(data));
          target.dispatchEvent(response_event);
        };
      }

      var host = doc.location.host;

      var hookEvents = target.getAttribute('hookEvent');
      if (hookEvents) {
        bindEvents.call(this, hookEvents, callback);
      } else if (action == 'process:form' && (
        host.indexOf(ABINE_DNTME.config.mappingServerHost.replace(/:[0-9]+/,'')) != -1 ||
        host.match(/^(test-\.|staging-\.)?mapping.abine.com$/i)
        )) {
        messenger.trigger('process:form', {html: params.html, mapping: params.mapping, callback: callback});
      } else {
        proxies.pageEventsModule.processAction({action: action, host: host, params: params, secure: secure}, function(data){
          callback(data);
        });
      }
    }

    function handleOldPageEvent(event) {
      var target = event.target;
      var doc = target.ownerDocument;
      var action = target.getAttribute("action");
      var params = target.getAttribute('param');

      if (params) {
        try{
          params = JSON.parse(params);
        } catch(e){
          params = {};
        }
      } else {
        params = {};
      }

      var host = doc.location.host;

      proxies.pageEventsModule.processAction({action: action, host: host, params: params, secure: secure}, function(data){
        $(target).text(JSON.stringify(data));
        $(target).click();
      });
    }

    function bindEvents(hookEvents, callback) {
      if (hookEvents == 'push') {
        if (!this.abinePushEvent) {
          this.abinePushEvent = _.bind(function (e) {
            LOG_FINEST && ABINE_DNTME.log.finest("sending push event to webapp");
            this.trigger('push', e);
          }, this);
          messenger.on('websocket:push', this.abinePushEvent);
        }
        callback('connected');
      } else if (hookEvents == 'abine:store:sync') {
        if (!this.abineStoreSync) {
          this.abineStoreSync = _.bind(function (e) {
            LOG_FINEST && ABINE_DNTME.log.finest("sending abine:store:sync event to webapp");
            this.trigger('abine:store:sync', e);
          }, this);
          messenger.on('abine:store:sync', this.abineStoreSync);
        }
        callback('connected');
      } else {
        if (!this.entityChanged) {
          this.entityChanged = _.bind(function (entity) {
            LOG_FINEST && ABINE_DNTME.log.finest("sending "+entity+":entity:changed event to webapp");
            this.trigger(entity+':entity:changed', null);
          }, this);
          messenger.on('entity:changed', this.entityChanged);
          
          this.updateEntity = _.bind(function (data) {
            LOG_FINEST && ABINE_DNTME.log.finest("sending "+name+":entity:update event to webapp");
            this.trigger('update:entity', ['updated', data[0], data[1]]);
          }, this);
          messenger.on('update:entity', this.updateEntity);
          
          this.deleteEntity = _.bind(function (data) {
            LOG_FINEST && ABINE_DNTME.log.finest("sending "+name+":entity:delete event to webapp");
            this.trigger('delete:entity', ['deleted', data[0], data[1]]);
          }, this);
          messenger.on('delete:entity', this.deleteEntity);
        }
        callback('connected');
      }
      _.each(hookEvents.split(' '), _.bind(function (evt) {
        this.on(evt, callback);
      }, this));
    }

    function unbindEvents() {
      if (this.updateEntity) {
        messenger.off('update:entity', this.updateEntity);
        this.updateEntity = null;
      }
      if (this.deleteEntity) {
        messenger.off('delete:entity', this.deleteEntity);
        this.deleteEntity = null;
      }
      if (this.entityChanged) {
        messenger.off('entity:changed', this.entityChanged);
        this.entityChanged = null;
      }
      if (this.abineStoreSync) {
        messenger.off('abine:store:sync', this.abineStoreSync);
        this.abineStoreSync = null;
      }
      if (this.abinePushEvent) {
        messenger.off('websocket:push', this.abinePushEvent);
        this.abinePushEvent = null;
      }
    }

    var PageEventImpl = abineCore.BaseClass.extend({
      startup: function(msgr, doc, href){
        messenger = msgr;
        if (!href.match(allowedProtocol) || !abineUrl.getHostname(href).match(allowedHostsRegex)){
          return;
        }
        this.initialized = true;
        this.handlePageEventListener = _.bind(function(){
          var args = arguments;
          // without a zero timeout, stack on firefox grows too big and creates timing issues.
          abineTimer.setTimeout(_.bind(function(){
            handlePageEvent.apply(this, args);
          }, this), 0);
        }, this);
        doc.addEventListener('DntPageEvent', this.handlePageEventListener, true, true);
        /* for old report page which will be used when maskme is installed */
        doc.addEventListener('DNTPPageEvent', handleOldPageEvent, true, true);

        // replay any pending events
        var pendingEvents = doc.getElementsByTagName('dntdataelement');
        for (var i=0;i<pendingEvents.length;i++) {
          this.handlePageEventListener({target: pendingEvents[i]});
        }

      },
      shutdown: function(doc){
        if (!this.initialized) return;
        this.initialized = false;
        doc.removeEventListener('DntPageEvent', this.handlePageEventListener, true, true);
        doc.removeEventListener('DNTPPageEvent', handleOldPageEvent, true, true);
        unbindEvents.call(this);
        this.off();
      }
    });

    return PageEventImpl;

  });
ABINE_DNTME.define('abine/formSubmitContentHelper',
  ['documentcloud/underscore', 'abine/formAnalyzer', 'abine/core', 'abine/eventLogger'],
  function(_, abineFormAnalyzer, abineCore, log) {

    // class for content script
    var FormSubmitContentHelper = abineCore.BaseClass.extend({

      initialize: function(contentMessenger, win) {
        win = win || window;

        function sendFormSubmitMessage(e){
          LOG_FINEST && log.finest("formSubmitContentHelper heard submit");
          var form = e.target;
          if (form.nodeName === 'FORM') {
            LOG_FINEST && log.finest("formSubmitContentHelper triggering contentManager:formSubmit");
            contentMessenger.send('contentManager:formSubmit', abineFormAnalyzer.getFormData(form, win.document.URL || win.location.href));
          }
        }

        // listen for form submit events on window and send it to background script
        win.document.addEventListener('submit', sendFormSubmitMessage, true);

        // listen for clicks on submit buttons send it to background script
        win.document.addEventListener('click', function(e){
          if (e.target.form && e.target.nodeName.match(/input|button/i) && (e.target.type||'').match(/submit|button|image/i)) {
            sendFormSubmitMessage({target: e.target.form});
          }
        }, true);

        LOG_FINEST && log.finest("formSubmitContentHelper clearing contentManager:formSubmit");
      }

    });

    return FormSubmitContentHelper;
  });











// Chrome content script

(function() {

  ABINE_DNTME.context = 'CS';

  // Mark the DOM so we know that we loaded this already
  var htmlEl = document.documentElement;
  if (htmlEl.attributes.idmeScript == "content.js") {
    return; // this script has already been injected
  }
  htmlEl.attributes.idmeScript = "content.js";

  function startApplication() {
    ABINE_DNTME.startTime = Date.now();
    requireApplication(function() {
      initializeContentManager();
    });
  }

  function requireApplication(callback) {
    ABINE_DNTME.requireDuckbone(function() {
      ABINE_DNTME.requireModels(callback);
    });
  }

  function initializeContentManager() {
    ABINE_DNTME.require(["abine/contentManager", "abine/eventLogger", "abine/pageEventsImpl"],
      function(abineContentManager, log, PageEventImpl) {
      var contentManager = new abineContentManager.ContentManager({
        document: document,
        force: true
      });
      contentManager.start();

      var pageEvents = new PageEventImpl();
      pageEvents.startup(contentManager.messenger, document, contentManager.href);

      pageEvents.on('disable:form:helpers', function(){
        contentManager.disableFormHelpers();
      });

      // cleanup content manager to avoid leaks
      var cleanup = function () {
        contentManager.destroy();
        pageEvents.shutdown(document);
      };
      window.addEventListener('unload', cleanup, false);
      window.addEventListener('dntme_unload', cleanup, false);

      document.documentElement.contentManager = contentManager;
    });
  }

  function visibilityChangeHandler(){
    if (!document.webkitHidden) {
      // remove handler to stop re-initializing on future visibility changes.
      document.removeEventListener("webkitvisibilitychange", visibilityChangeHandler, false);
      startApplication();
    }
  }

  if (document.webkitVisibilityState != 'visible' && document.location.host.indexOf(ABINE_DNTME.config.webAppHost) == -1) {
    // wait for content to be visible
    document.addEventListener("webkitvisibilitychange", visibilityChangeHandler, false);
  } else {
    startApplication();
  }

  // Notify background
  chrome.extension.sendRequest({
    eventName: "contentScript:init",
    payload: {}
  });
})();


