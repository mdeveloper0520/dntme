/**
 * Blur Copyright (c) 2008-2015 by Abine, Inc. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Abine, Inc. ("Confidential Information"), subject
 * to the Non-Disclosure Agreement and/or License Agreement you entered
 * into with Abine. You shall use such Confidential Information only
 * in accordance with the terms of said Agreement(s). Abine makes
 * no representations or warranties about the suitability of the
 * software. The software is provided with ABSOLUTELY NO WARRANTY
 * and Abine will NOT BE LIABLE for ANY DAMAGES resulting from
 * the use of the software.
 *
 * Contact license@getabine.com with any license-related questions.
 *
 * https://www.abine.com
 * @license
 *
 */

;

// url helper methods

ABINE_DNTME.define("abine/url", function() {
      var CompoundRealms = /^(((co|ac|go|ed|ga|mn|nj|nc|or|ne|in|gr|ca|on|pa|qc|bc|sh|at|va|us|il|tv|me|lg|at|ny|gv|com|edu|org|net|gov|mil|biz)\...)|(163\.com)|(kiev\.ua)|(gob\.pe)|(gen\.tr)|(waw.pl)|(jus.br))$/;

var HostingSites = /^(((go|typepad|tumblr|blogspot|squarespace|wordpress|myshopify|blogfa|blog|blogs|blinkweb|ning|posterous|over-blog|mihanblog|appspot|webs|weebly|onsugar|ucoz|uk|pch|ecwid)(\.com|\.in))|(nic\.in)|((ucoz|spb|narod)\.ru)|(blog\.163\.com)|((gouv|free)\.fr)|(uol\.com\.br)|(web\.id)|(gob\.mx)|(blogg\.se)|(home.pl))$/;

var urlRegex = /^http(?:s)?\:\/\/([^/\?\#]+)/im;
var ipAddressRegex = /^[0-9\.]+(:[0-9]+)?$/;
var localhostRegex = /^localhost(:[0-9]+)?$/;

// helper method to convert input string to proper case
function toProperCase(str) {
  if (!str) return str;
  return str.toLowerCase().replace(/\w+/g, function (s) {
    return s.charAt(0).toUpperCase() + s.substr(1);
  });
}

// extracts host name from a given url
function getHostname(url) {
  var parts = url.match(urlRegex);
  return parts && parts.length > 1 ? parts[1].toString() : url;
}

// extracts Top Level Domain from a given url
function getTLD(url) {
  var hostname = getHostname(url);

  if (hostname.match(ipAddressRegex) || hostname.match(localhostRegex)) {
    return hostname;
  }

  var parts = hostname.split(".");

  if (parts.length < 2) {
    return hostname;
  }

  var realm = parts.pop();
  var baseDomain = parts.pop();
  var domain = baseDomain + "." + realm;

  if (CompoundRealms.test(domain) && parts.length > 0) {
    realm = domain;
    baseDomain = parts.pop();
    domain = baseDomain + "." + realm;
  }

  // singup.wordpress.com and new.aol.com are common to all users.  they should not be treated as hosting sites
  if (HostingSites.test(domain) && parts.length > 0 && !hostname.match(/signup.wordpress.com|new.aol.com|(oauth|www).squarespace.com|secure.onsugar.com|www.tumblr.com/i)) {
    var subDomain = parts.pop();
    domain = subDomain + "." + domain;
  }
  return domain;
}

// converts a top level domain to a meaningful site name
function getSiteName(domain) {
  domain = getTLD(domain);

  if (domain.match(ipAddressRegex)) {
    return domain;
  }

  var parts = domain.split(".");

  if (parts.length > 2) {
    var realm = parts.pop();
    var baseDomain = parts.pop();
    var domain = baseDomain + "." + realm;

    if (CompoundRealms.test(domain) && parts.length > 0) {
      realm = domain;
      baseDomain = parts.pop();
      domain = baseDomain + "." + realm;
    }

    if (HostingSites.test(domain) && parts.length > 0) {
      var subDomain = parts.pop();
      return toProperCase(baseDomain + ' ' + subDomain);
    }

    return toProperCase(baseDomain);
  }

  return toProperCase(parts[0]);
}

var AbineURL = {
  getHostname: getHostname,
  getTLD: getTLD,
  getSiteName: getSiteName
};

      return AbineURL;
    });
/**
 * Copyright (c) 2010 Zef Hemel <zef@zef.me>
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

// WRAP EVERYTHING IN AN AMD MODULE

ABINE_DNTME.define('persistence', [], function() {

// MANGLED HERE FOR MODULE ...
var persistence = initPersistence((typeof(window) != 'undefined' && window.persistence) || {});

function initPersistence(persistence) {
	if (persistence.isImmutable) // already initialized
		return persistence;

/**
 * Check for immutable fields
 */
persistence.isImmutable = function(fieldName) {
  return (fieldName == "id");
};

/**
 * Default implementation for entity-property
 */
persistence.defineProp = function(scope, field, setterCallback, getterCallback) {
  if (Object.defineProperty) {
    Object.defineProperty(scope, field, {
      get: function() {
        return getterCallback();
      },
      set: function(value) {
        setterCallback(value);
      }
    });
  } else {
    scope.__defineSetter__(field, function (value) {
      setterCallback(value);
    });
    scope.__defineGetter__(field, function () {
      return getterCallback();
    });
  }
};

/**
 * Default implementation for entity-property setter
 */
persistence.set = function(scope, fieldName, value) {
    if (persistence.isImmutable(fieldName)) throw new Error("immutable field: "+fieldName);
    scope[fieldName] = value;
};

/**
 * Default implementation for entity-property getter
 */
persistence.get = function(arg1, arg2) {
  return (arguments.length == 1) ? arg1 : arg1[arg2];
};


(function () {
    var entityMeta = {};
    var entityClassCache = {};
    persistence.getEntityMeta = function() { return entityMeta; }

    // Per-session data
    persistence.trackedObjects = {};
    persistence.trackedObjectsByEntity = {};
    persistence.objectsToRemove = {};
    persistence.objectsRemoved = []; // {id: ..., type: ...}
    persistence.globalPropertyListeners = {}; // EntityType__prop -> QueryColleciton obj
    persistence.queryCollectionCache = {}; // entityName -> uniqueString -> QueryCollection

    persistence.getObjectsToRemove = function() { return this.objectsToRemove; };
    persistence.getTrackedObjects = function() { return this.trackedObjects; };

    // Public Extension hooks
    persistence.entityDecoratorHooks = [];
    persistence.flushHooks = [];
    persistence.schemaSyncHooks = [];

    // Enable debugging (display queries using console.log etc)
    persistence.debug = false;

    persistence.subscribeToGlobalPropertyListener = function(coll, entityName, property) {
      var key = entityName + '__' + property;
      if(key in this.globalPropertyListeners) {
        var listeners = this.globalPropertyListeners[key];
        for(var i = 0; i < listeners.length; i++) {
          if(listeners[i] === coll) {
            return;
          }
        }
        this.globalPropertyListeners[key].push(coll);
      } else {
        this.globalPropertyListeners[key] = [coll];
      }
    }

    persistence.unsubscribeFromGlobalPropertyListener = function(coll, entityName, property) {
      var key = entityName + '__' + property;
      var listeners = this.globalPropertyListeners[key];
      for(var i = 0; i < listeners.length; i++) {
        if(listeners[i] === coll) {
          listeners.splice(i, 1);
          return;
        }
      }
    }

    persistence.propertyChanged = function(obj, property, oldValue, newValue) {
      if(!this.trackedObjects[obj.id]) return; // not yet added, ignore for now

      var entityName = obj._type;
      var key = entityName + '__' + property;
      if(key in this.globalPropertyListeners) {
        var listeners = this.globalPropertyListeners[key];
        for(var i = 0; i < listeners.length; i++) {
          var coll = listeners[i];
          var dummyObj = obj._data;
          dummyObj[property] = oldValue;
          var matchedBefore = coll._filter.match(dummyObj);
          dummyObj[property] = newValue;
          var matchedAfter = coll._filter.match(dummyObj);
          if(matchedBefore != matchedAfter) {
            coll.triggerEvent('change', coll, obj);
          }
        }
      }
    }

    persistence.objectRemoved = function(obj) {
      var entityName = obj._type;
      if(this.queryCollectionCache[entityName]) {
        var colls = this.queryCollectionCache[entityName];
        for(var key in colls) {
          if(colls.hasOwnProperty(key)) {
            var coll = colls[key];
            if(coll._filter.match(obj)) { // matched the filter -> was part of collection
              coll.triggerEvent('change', coll, obj);
            }
          }
        }
      }
    }

    /**
     * Retrieves metadata about entity, mostly for internal use
     */
    function getMeta(entityName) {
      return entityMeta[entityName];
    }

    persistence.getMeta = getMeta;
    

    /**
     * A database session
     */
    function Session(conn) {
      this.trackedObjects = {};
      this.trackedObjectsByEntity = {};
      this.objectsToRemove = {};
      this.objectsRemoved = [];
      this.globalPropertyListeners = {}; // EntityType__prop -> QueryColleciton obj
      this.queryCollectionCache = {}; // entityName -> uniqueString -> QueryCollection
      this.conn = conn;
    }

    Session.prototype = persistence; // Inherit everything from the root persistence object

    persistence.Session = Session;

    /* used in tests to remove unwanted entity classes. eg: SampleEntity */
    persistence.undefine = function(entityName) {
      if (entityClassCache[entityName]) {
        delete entityClassCache[entityName];
      }
      if (entityName in entityMeta) {
        delete entityMeta[entityName];
      }
    };

    /**
     * Define an entity
     *
     * @param entityName
     *            the name of the entity (also the table name in the database)
     * @param fields
     *            an object with property names as keys and SQLite types as
     *            values, e.g. {name: "TEXT", age: "INT"}
     * @return the entity's constructor
     */
    persistence.define = function (entityName, fields) {
      if (entityMeta[entityName]) { // Already defined, ignore
        return getEntity(entityName);
      }
      var meta = {
        name: entityName,
        fields: fields,
        isMixin: false,
        indexes: [],
        hasMany: {},
        hasOne: {}
      };
      entityMeta[entityName] = meta;
      return getEntity(entityName);
    };

    /**
     * Checks whether an entity exists
     *
     * @param entityName
     *            the name of the entity (also the table name in the database)
     * @return `true` if the entity exists, otherwise `false`
     */
    persistence.isDefined = function (entityName) {
        return !!entityMeta[entityName];
    }

    /**
     * Define a mixin
     *
     * @param mixinName
     *            the name of the mixin
     * @param fields
     *            an object with property names as keys and SQLite types as
     *            values, e.g. {name: "TEXT", age: "INT"}
     * @return the entity's constructor
     */
    persistence.defineMixin = function (mixinName, fields) {
      var Entity = this.define(mixinName, fields);
      Entity.meta.isMixin = true;
      return Entity;
    };

    persistence.isTransaction = function(obj) {
      return !obj || (obj && obj.executeSql);
    };

    persistence.isSession = function(obj) {
      return !obj || (obj && obj.schemaSync);
    };

    persistence.addToEntityCache = function(obj) {
      var Entity = entityClassCache[obj._type];
      if (Entity.meta.cached) {
        var alternatePK = Entity.meta.alternatePK || 'id';
        if (alternatePK) {
          var key = obj._type+"."+alternatePK;
          if (!(key in this.trackedObjectsByEntity)) {
            this.trackedObjectsByEntity[key] = {};
          }
          this.trackedObjectsByEntity[key][obj[alternatePK]] = obj.id;
        }
      }
      this.addIndex(obj);
    };

    persistence.clearFromEntityCache = function(obj) {
      if (!obj) return;
      var Entity = entityClassCache[obj._type];
      if (Entity.meta.cached) {
        var alternatePK = Entity.meta.alternatePK || 'id';
        if (alternatePK) {
          var key = obj._type+"."+alternatePK;
          if (key in this.trackedObjectsByEntity) {
            if (obj.id == this.trackedObjectsByEntity[key][obj[alternatePK]]) {
              delete this.trackedObjectsByEntity[key][obj[alternatePK]];
            }
            if (obj.id in this.trackedObjects) {
              delete this.trackedObjects[obj.id];
            }
          }
        }
      }
      this.removeIndex(obj);
    };

    persistence.addIndex = function(obj) {
      if (!obj) return;
      var Entity = entityClassCache[obj._type];
      if (Entity.meta.indexed) {
        var value = (obj[Entity.meta.indexed]||"").toString().toLowerCase();
        var key = obj._type+"."+value;
        if (!(key in this.trackedObjectsByEntity)) {
          this.trackedObjectsByEntity[key] = {};
        }
        this.trackedObjectsByEntity[key][obj.id] = true;
      }
    };

    persistence.removeIndex = function(obj) {
      if (!obj) return;
      var Entity = entityClassCache[obj._type];
      if (Entity.meta.indexed) {
        var value = (obj[Entity.meta.indexed]||"").toString().toLowerCase();
        var key = obj._type+"."+value;
        if (key in this.trackedObjectsByEntity) {
          delete this.trackedObjectsByEntity[key][obj.id];
        }
      }
    };

    persistence.getEntityByAlternatePK = function(Entity, pk) {
      var alternatePK = Entity.meta.alternatePK;
      if (alternatePK) {
        var key = Entity.meta.name+"."+alternatePK;
        if (key in this.trackedObjectsByEntity) {
          if (pk in this.trackedObjectsByEntity[key])
            return this.trackedObjects[this.trackedObjectsByEntity[key][pk]];
        }
      }
      return null;
    };

    persistence.getEntitiesByIndex = function(Entity, value) {
      var indexed = Entity.meta.indexed;
      if (indexed) {
        value = (value||"").toString().toLowerCase();
        var key = Entity.meta.name+"."+value;
        if (key in this.trackedObjectsByEntity) {
          var entities = [];
          for (var id in this.trackedObjectsByEntity[key]) {
            if (this.trackedObjectsByEntity[key].hasOwnProperty(id)) {
              if (id in this.trackedObjects && this.trackedObjects[id][indexed] == value) {
                entities.push(this.trackedObjects[id]);
              }
            }
          }
          return entities;
        }
      }
      return [];
    };

    persistence.getCachedEntities = function(Entity) {
      var entities = [];
      if (Entity.meta.cached) {
        var alternatePK = Entity.meta.alternatePK || 'id';
        if (alternatePK) {
          var key = Entity.meta.name+"."+alternatePK;
          if (key in this.trackedObjectsByEntity) {
            var alternatePKs = this.trackedObjectsByEntity[key];
            for (var pk in alternatePKs) {
              if (alternatePKs[pk] in this.trackedObjects)
                entities.push(this.trackedObjects[alternatePKs[pk]]);
              else
                delete alternatePKs[pk];
            }
          }
        }
      }
      return entities;
    };

    /**
     * Adds the object to tracked entities to be persisted
     *
     * @param obj
     *            the object to be tracked
     */
    persistence.add = function (obj) {
      if(!obj) return;
      if (!this.trackedObjects[obj.id]) {
        this.trackedObjects[obj.id] = obj;
        this.addToEntityCache(obj);
        if(obj._new) {
          for(var p in obj._data) {
            if(obj._data.hasOwnProperty(p)) {
              this.propertyChanged(obj, p, undefined, obj._data[p]);
            }
          }
        }
      }
      return this;
    };

    /**
     * Adds the object to tracked entities to be persisted even if its already in tracked list
     *
     * @param obj
     *            the object to be tracked
     */
    persistence.forceAdd = function (obj) {
      if(!obj) return;
      if (!this.trackedObjects[obj.id] || this.trackedObjects[obj.id] != obj) {
        var Entity = entityClassCache[obj._type];
        if (Entity.meta.cached) {
          var alternatePK = Entity.meta.alternatePK;
          if (alternatePK) {
            var otherObj = persistence.getEntityByAlternatePK(Entity, obj[alternatePK]);
            if (otherObj && !(otherObj.id in this.objectsToRemove)) {
              if (otherObj.id != obj.id) {
                // there is a PK mismatch, so make it same as what was in DB
                obj.id = otherObj.id;
              }
            }
          }
        }
        this.trackedObjects[obj.id] = obj;
        this.addToEntityCache(obj);
        if(obj._new) {
          for(var p in obj._data) {
            if(obj._data.hasOwnProperty(p)) {
              this.propertyChanged(obj, p, undefined, obj._data[p]);
            }
          }
        }
      }
      return this;
    };

    /**
     * Marks the object to be removed (on next flush)
     * @param obj object to be removed
     */
    persistence.remove = function(obj) {
      if (!this.objectsToRemove[obj.id]) {
        this.objectsToRemove[obj.id] = obj;
      }
      this.objectsRemoved.push({id: obj.id, entity: obj._type});
      try {delete this.trackedObjects[obj.id];} catch(e){}
      this.clearFromEntityCache(obj);
      this.objectRemoved(obj);
      return this;
    };


    /**
     * Clean the persistence context of cached entities and such.
     */
    persistence.clean = function () {
      this.trackedObjects = {};
      this.objectsToRemove = {};
      this.objectsRemoved = [];
      this.globalPropertyListeners = {};
      this.queryCollectionCache = {};

      this.trackedObjectsByEntity = {};
      persistence.GlobalEvent.triggerEvent("clean");
    };

    /**
     * asynchronous sequential version of Array.prototype.forEach
     * @param array the array to iterate over
     * @param fn the function to apply to each item in the array, function
     *        has two argument, the first is the item value, the second a
     *        callback function
     * @param callback the function to call when the forEach has ended
     */
    persistence.asyncForEach = function(array, fn, callback) {
      array = array.slice(0); // Just to be sure
      function processOne() {
        var item = array.pop();
        fn(item, function(result, err) {
            if(array.length > 0) {
              processOne();
            } else {
              callback(result, err);
            }
          });
      }
      if(array.length > 0) {
        processOne();
      } else {
        callback();
      }
    };

    /**
     * asynchronous parallel version of Array.prototype.forEach
     * @param array the array to iterate over
     * @param fn the function to apply to each item in the array, function
     *        has two argument, the first is the item value, the second a
     *        callback function
     * @param callback the function to call when the forEach has ended
     */
    persistence.asyncParForEach = function(array, fn, callback) {
      var completed = 0;
      var arLength = array.length;
      if(arLength === 0) {
        callback();
      }
      for(var i = 0; i < arLength; i++) {
        fn(array[i], function(result, err) {
            completed++;
            if(completed === arLength) {
              callback(result, err);
            }
          });
      }
    };

    /**
     * Retrieves or creates an entity constructor function for a given
     * entity name
     * @return the entity constructor function to be invoked with `new fn()`
     */
    function getEntity(entityName) {
      if (entityClassCache[entityName]) {
        return entityClassCache[entityName];
      }
      var meta = entityMeta[entityName];

      /**
       * @constructor
       */
      function Entity (session, obj, noEvents) {
        var args = argspec.getArgs(arguments, [
            { name: "session", optional: true, check: persistence.isSession, defaultValue: persistence },
            { name: "obj", optional: true, check: function(obj) { return obj; }, defaultValue: {} }
          ]);
        if (meta.isMixin)
          throw new Error("Cannot instantiate mixin");
        session = args.session;
        obj = args.obj;

        var that = this;
        this.id = obj.id || persistence.createUUID();
        this._new = true;
        this._type = entityName;
        this._dirtyProperties = {};
        this._data = {};
        this._data_obj = {}; // references to objects
        this._session = session || persistence;
        this.subscribers = {}; // observable

        for ( var field in meta.fields) {
          (function () {
              if (meta.fields.hasOwnProperty(field)) {
                var f = field; // Javascript scopes/closures SUCK
                persistence.defineProp(that, f, function(val) {
                    // setterCallback
                    var oldValue = that._data[f];
                    if(oldValue !== val || (oldValue && val && oldValue.getTime && val.getTime)) { // Don't mark properties as dirty and trigger events unnecessarily
                      that._data[f] = val;
                      that._dirtyProperties[f] = oldValue;
                      that.triggerEvent('set', that, f, val);
                      that.triggerEvent('change', that, f, val);
                      session.propertyChanged(that, f, oldValue, val);
                    }
                  }, function() {
                    // getterCallback
                    return that._data[f];
                  });
                that._data[field] = defaultValue(meta.fields[field]);
              }
            }());
        }

        for ( var it in meta.hasOne) {
          if (meta.hasOne.hasOwnProperty(it)) {
            (function () {
                var ref = it;
                var mixinClass = meta.hasOne[it].type.meta.isMixin ? ref + '_class' : null;
                persistence.defineProp(that, ref, function(val) {
                    // setterCallback
                    var oldValue = that._data[ref];
                    var oldValueObj = that._data_obj[ref] || session.trackedObjects[that._data[ref]];
                    if (val == null) {
                      that._data[ref] = null;
                      that._data_obj[ref] = undefined;
                      if (mixinClass)
                        that[mixinClass] = '';
                    } else if (val.id) {
                      that._data[ref] = val.id;
                      that._data_obj[ref] = val;
                      if (mixinClass)
                        that[mixinClass] = val._type;
                      session.add(val);
                      session.add(that);
                    } else { // let's assume it's an id
                      that._data[ref] = val;
                    }
                    that._dirtyProperties[ref] = oldValue;
                    that.triggerEvent('set', that, ref, val);
                    that.triggerEvent('change', that, ref, val);
                    // Inverse
                    if(meta.hasOne[ref].inverseProperty) {
                      var newVal = that[ref];
                      if(newVal) {
                        var inverse = newVal[meta.hasOne[ref].inverseProperty];
                        if(inverse.list && inverse._filter) {
                          inverse.triggerEvent('change', that, ref, val);
                        }
                      }
                      if(oldValueObj) {
                        var inverse = oldValueObj[meta.hasOne[ref].inverseProperty];
                        if(inverse.list && inverse._filter) {
                          inverse.triggerEvent('change', that, ref, val);
                        }
                      }
                    }
                  }, function() {
                    // getterCallback
                    if (!that._data[ref]) {
                      return null;
                    } else if(that._data_obj[ref] !== undefined) {
                      return that._data_obj[ref];
                    } else if(that._data[ref] && session.trackedObjects[that._data[ref]]) {
                      that._data_obj[ref] = session.trackedObjects[that._data[ref]];
                      return that._data_obj[ref];
                    } else {
                      throw new Error("Property '" + ref + "' of '" + meta.name + "' with id: " + that._data[ref] + " not fetched, either prefetch it or fetch it manually.");
                    }
                  });
              }());
          }
        }

        for ( var it in meta.hasMany) {
          if (meta.hasMany.hasOwnProperty(it)) {
            (function () {
                var coll = it;
                if (meta.hasMany[coll].manyToMany) {
                  persistence.defineProp(that, coll, function(val) {
                      // setterCallback
                      if(val && val._items) {
                        // Local query collection, just add each item
                        // TODO: this is technically not correct, should clear out existing items too
                        var items = val._items;
                        for(var i = 0; i < items.length; i++) {
                          persistence.get(that, coll).add(items[i]);
                        }
                      } else {
                        throw new Error("Not yet supported.");
                      }
                    }, function() {
                      // getterCallback
                      if (that._data[coll]) {
                        return that._data[coll];
                      } else {
                        var rel = meta.hasMany[coll];
                        var inverseMeta = rel.type.meta;
                        var inv = inverseMeta.hasMany[rel.inverseProperty];
                        var direct = rel.mixin ? rel.mixin.meta.name : meta.name;
                        var inverse = inv.mixin ? inv.mixin.meta.name : inverseMeta.name;

                        var queryColl = new persistence.ManyToManyDbQueryCollection(session, inverseMeta.name);
                        queryColl.initManyToMany(that, coll);
                        queryColl._manyToManyFetch = {
                            table: rel.tableName,
                            prop: direct + '_' + coll,
                            inverseProp: inverse + '_' + rel.inverseProperty,
                            id: that.id
                          };
                        that._data[coll] = queryColl;
                        return session.uniqueQueryCollection(queryColl);
                      }
                    });
                } else { // one to many
                  persistence.defineProp(that, coll, function(val) {
                      // setterCallback
                      if(val && val._items) {
                        // Local query collection, just add each item
                        // TODO: this is technically not correct, should clear out existing items too
                        var items = val._items;
                        for(var i = 0; i < items.length; i++) {
                          persistence.get(that, coll).add(items[i]);
                        }
                      } else {
                        throw new Error("Not yet supported.");
                      }
                    }, function() {
                      // getterCallback
                      if (that._data[coll]) {
                        return that._data[coll];
                      } else {
                        var queryColl = session.uniqueQueryCollection(new persistence.DbQueryCollection(session, meta.hasMany[coll].type.meta.name).filter(meta.hasMany[coll].inverseProperty, '=', that));
                        that._data[coll] = queryColl;
                        return queryColl;
                      }
                    });
                }
              }());
          }
        }

        if(this.initialize) {
          this.initialize();
        }

        for ( var f in obj) {
          if (obj.hasOwnProperty(f)) {
            if(f !== 'id') {
              persistence.set(that, f, obj[f]);
            }
          }
        }
      } // Entity

      Entity.prototype = new Observable();

      Entity.meta = meta;

      Entity.prototype.equals = function(other) {
        return this.id == other.id;
      };

      Entity.prototype.toJSON = function() {
        var json = {id: this.id};
        for(var p in this._data) {
          if(this._data.hasOwnProperty(p)) {
            if (typeof this._data[p] == "object" && this._data[p] != null) {
              if (this._data[p].toJSON != undefined) {
                json[p] = this._data[p].toJSON();
              }
            } else {
              json[p] = this._data[p];
            }
          }
        }
        return json;
      };


      /**
       * Select a subset of data as a JSON structure (Javascript object)
       *
       * A property specification is passed that selects the
       * properties to be part of the resulting JSON object. Examples:
       *    ['id', 'name'] -> Will return an object with the id and name property of this entity
       *    ['*'] -> Will return an object with all the properties of this entity, not recursive
       *    ['project.name'] -> will return an object with a project property which has a name
       *                        property containing the project name (hasOne relationship)
       *    ['project.[id, name]'] -> will return an object with a project property which has an
       *                              id and name property containing the project name
       *                              (hasOne relationship)
       *    ['tags.name'] -> will return an object with an array `tags` property containing
       *                     objects each with a single property: name
       *
       * @param tx database transaction to use, leave out to start a new one
       * @param props a property specification
       * @param callback(result)
       */
      Entity.prototype.selectJSON = function(tx, props, callback) {
        var that = this;
        var args = argspec.getArgs(arguments, [
            { name: "tx", optional: true, check: persistence.isTransaction, defaultValue: null },
            { name: "props", optional: false },
            { name: "callback", optional: false }
          ]);
        tx = args.tx;
        props = args.props;
        callback = args.callback;

        if(!tx) {
          this._session.transaction(function(tx) {
              that.selectJSON(tx, props, callback);
            });
          return;
        }
        var includeProperties = {};
        props.forEach(function(prop) {
            var current = includeProperties;
            var parts = prop.split('.');
            for(var i = 0; i < parts.length; i++) {
              var part = parts[i];
              if(i === parts.length-1) {
                if(part === '*') {
                  current.id = true;
                  for(var p in meta.fields) {
                    if(meta.fields.hasOwnProperty(p)) {
                      current[p] = true;
                    }
                  }
                  for(var p in meta.hasOne) {
                    if(meta.hasOne.hasOwnProperty(p)) {
                      current[p] = true;
                    }
                  }
                  for(var p in meta.hasMany) {
                    if(meta.hasMany.hasOwnProperty(p)) {
                      current[p] = true;
                    }
                  }
                } else if(part[0] === '[') {
                  part = part.substring(1, part.length-1);
                  var propList = part.split(/,\s*/);
                  propList.forEach(function(prop) {
                      current[prop] = true;
                    });
                } else {
                  current[part] = true;
                }
              } else {
                current[part] = current[part] || {};
                current = current[part];
              }
            }
          });
        buildJSON(this, tx, includeProperties, callback);
      };

      function buildJSON(that, tx, includeProperties, callback) {
        var session = that._session;
        var properties = [];
        var meta = getMeta(that._type);
        var fieldSpec = meta.fields;

        for(var p in includeProperties) {
          if(includeProperties.hasOwnProperty(p)) {
            properties.push(p);
          }
        }

        var cheapProperties = [];
        var expensiveProperties = [];

        properties.forEach(function(p) {
            if(includeProperties[p] === true && !meta.hasMany[p]) { // simple, loaded field
              cheapProperties.push(p);
            } else {
              expensiveProperties.push(p);
            }
          });

        var itemData = that._data;
        var item = {};

        cheapProperties.forEach(function(p) {
            if(p === 'id') {
              item.id = that.id;
            } else if(meta.hasOne[p]) {
              item[p] = itemData[p] ? {id: itemData[p]} : null;
            } else {
              item[p] = persistence.entityValToJson(itemData[p], fieldSpec[p]);
            }
          });
        properties = expensiveProperties.slice();

        persistence.asyncForEach(properties, function(p, callback) {
          if(meta.hasOne[p]) {
            that.fetch(tx, p, function(obj) {
                if(obj) {
                  buildJSON(obj, tx, includeProperties[p], function(result) {
                      item[p] = result;
                      callback();
                    });
                } else {
                  item[p] = null;
                  callback();
                }
              });
          } else if(meta.hasMany[p]) {
            persistence.get(that, p).list(function(objs) {
                item[p] = [];
                persistence.asyncForEach(objs, function(obj, callback) {
                    var obj = objs.pop();
                    if(includeProperties[p] === true) {
                      item[p].push({id: obj.id});
                      callback();
                    } else {
                      buildJSON(obj, tx, includeProperties[p], function(result) {
                          item[p].push(result);
                          callback();
                        });
                    }
                  }, callback);
              });
          }
        }, function() {
          callback(item);
        });
      }; // End of buildJson

      Entity.prototype.fetch = function(tx, rel, callback) {
        var args = argspec.getArgs(arguments, [
            { name: 'tx', optional: true, check: persistence.isTransaction, defaultValue: null },
            { name: 'rel', optional: false, check: argspec.hasType('string') },
            { name: 'callback', optional: false, check: argspec.isCallback() }
          ]);
        tx = args.tx;
        rel = args.rel;
        callback = args.callback;

        var that = this;
        var session = this._session;

        if(!tx) {
          session.transaction(function(tx) {
              that.fetch(tx, rel, callback);
            });
          return;
        }
        if(!this._data[rel]) { // null
          if(callback) {
            callback(null);
          }
        } else if(this._data_obj[rel]) { // already loaded
          if(callback) {
            callback(this._data_obj[rel]);
          }
        } else {
          var type = meta.hasOne[rel].type;
          if (type.meta.isMixin) {
            type = getEntity(this._data[rel + '_class']);
          }
          type.load(session, tx, this._data[rel], function(obj) {
              that._data_obj[rel] = obj;
              if(callback) {
                callback(obj);
              }
            });
        }
      };

      /**
       * Currently this is only required when changing JSON properties
       */
      Entity.prototype.markDirty = function(prop) {
        this._dirtyProperties[prop] = true;
      };

      /**
       * Returns a QueryCollection implementation matching all instances
       * of this entity in the database
       */
      Entity.all = function(session) {
        var args = argspec.getArgs(arguments, [
            { name: 'session', optional: true, check: persistence.isSession, defaultValue: persistence }
          ]);
        session = args.session;
        return session.uniqueQueryCollection(new AllDbQueryCollection(session, entityName));
      };

      Entity.query = function(filter, callback) {
        persistence.transaction(function(t) {
          var where = '';
          if (filter) {
            where = ' where ' + filter;
          }

          t.executeSql('SELECT * FROM ' + entityName + where, null, function(result) {
            callback(result);
          });
        });
      }

      Entity.deleteQuery = function(filter, callback) {
        persistence.transaction(function(t) {
          var where = '';
          if (filter) {
            where = ' where ' + filter;
          }

          t.executeSql('DELETE FROM ' + entityName + where, null, function(result) {
            callback(result);
          });
        });
      }

      Entity.fromSelectJSON = function(session, tx, jsonObj, callback) {
        var args = argspec.getArgs(arguments, [
            { name: 'session', optional: true, check: persistence.isSession, defaultValue: persistence },
            { name: 'tx', optional: true, check: persistence.isTransaction, defaultValue: null },
            { name: 'jsonObj', optional: false },
            { name: 'callback', optional: false, check: argspec.isCallback() }
          ]);
        session = args.session;
        tx = args.tx;
        jsonObj = args.jsonObj;
        callback = args.callback;

        if(!tx) {
          session.transaction(function(tx) {
              Entity.fromSelectJSON(session, tx, jsonObj, callback);
            });
          return;
        }

        if(typeof jsonObj === 'string') {
          jsonObj = JSON.parse(jsonObj);
        }

        if(!jsonObj) {
          callback(null);
          return;
        }

        function loadedObj(obj) {
          if(!obj) {
            obj = new Entity(session);
            if(jsonObj.id) {
              obj.id = jsonObj.id;
            }
          }
          session.add(obj);
          var expensiveProperties = [];
          for(var p in jsonObj) {
            if(jsonObj.hasOwnProperty(p)) {
              if(p === 'id') {
                continue;
              } else if(meta.fields[p]) { // regular field
                persistence.set(obj, p, persistence.jsonToEntityVal(jsonObj[p], meta.fields[p]));
              } else if(meta.hasOne[p] || meta.hasMany[p]){
                expensiveProperties.push(p);
              }
            }
          }
          persistence.asyncForEach(expensiveProperties, function(p, callback) {
              if(meta.hasOne[p]) {
                meta.hasOne[p].type.fromSelectJSON(session, tx, jsonObj[p], function(result) {
                    persistence.set(obj, p, result);
                    callback();
                  });
            } else if(meta.hasMany[p]) {
              var coll = persistence.get(obj, p);
              var ar = jsonObj[p].slice(0);
              var PropertyEntity = meta.hasMany[p].type;
              // get all current items
              coll.list(tx, function(currentItems) {
                  persistence.asyncForEach(ar, function(item, callback) {
                      PropertyEntity.fromSelectJSON(session, tx, item, function(result) {
                          // Check if not already in collection
                          for(var i = 0; i < currentItems.length; i++) {
                            if(currentItems[i].id === result.id) {
                              callback();
                              return;
                            }
                          }
                          coll.add(result);
                          callback();
                        });
                    }, function() {
                      callback();
                    });
                });
            }
          }, function() {
            callback(obj);
          });
        }
        if(jsonObj.id) {
          Entity.load(session, tx, jsonObj.id, loadedObj);
        } else {
          loadedObj(new Entity(session));
        }
      };

      Entity.load = function(session, tx, id, callback) {
        var args = argspec.getArgs(arguments, [
            { name: 'session', optional: true, check: persistence.isSession, defaultValue: persistence },
            { name: 'tx', optional: true, check: persistence.isTransaction, defaultValue: null },
            { name: 'id', optional: false, check: argspec.hasType('string') },
            { name: 'callback', optional: true, check: argspec.isCallback(), defaultValue: function(){} }
          ]);
        Entity.findBy(args.session, args.tx, "id", args.id, args.callback);
      };

      Entity.findBy = function(session, tx, property, value, callback) {
        var args = argspec.getArgs(arguments, [
            { name: 'session', optional: true, check: persistence.isSession, defaultValue: persistence },
            { name: 'tx', optional: true, check: persistence.isTransaction, defaultValue: null },
            { name: 'property', optional: false, check: argspec.hasType('string') },
            { name: 'value', optional: false },
            { name: 'callback', optional: true, check: argspec.isCallback(), defaultValue: function(){} }
          ]);
        session = args.session;
        tx = args.tx;
        property = args.property;
        value = args.value;
        callback = args.callback;

        if(property === 'id' && value in session.trackedObjects) {
          callback(session.trackedObjects[value]);
          return;
        }

        var entityMeta = Entity.meta;
        if (entityMeta.alternatePK && property === entityMeta.alternatePK && session.trackedObjectsByEntity) {
          var key = entityMeta.name+"."+entityMeta.alternatePK;
          if (key in session.trackedObjectsByEntity && value in session.trackedObjectsByEntity[key]) {
            if (session.trackedObjectsByEntity[key][value] in session.trackedObjects) {
              callback(session.trackedObjects[session.trackedObjectsByEntity[key][value]]);
              return;
            } else {
              delete session.trackedObjectsByEntity[key][value];
            }
          } else {
            if (Entity.cached) {
              // not found in cache, no need to hit DB
              callback(null);
              return;
            } else {
              // init cache and then call findBy
              Entity.initCache(function(){Entity.findBy(session, tx, property, value, callback);});
              return;
            }
          }
        }

        if(!tx) {
          session.transaction(function(tx) {
              Entity.findBy(session, tx, property, value, callback);
            });
          return;
        }
        Entity.all(session).filter(property, "=", value).one(tx, function(obj) {
            callback(obj);
          });
      }


      Entity.index = function(cols,options) {
        var opts = options || {};
        if (typeof cols=="string") {
          cols = [cols];
        }
        opts.columns = cols;
        meta.indexes.push(opts);
      };

      /**
       * Declares a one-to-many or many-to-many relationship to another entity
       * Whether 1:N or N:M is chosed depends on the inverse declaration
       * @param collName the name of the collection (becomes a property of
         *   Entity instances
         * @param otherEntity the constructor function of the entity to define
         *   the relation to
         * @param inverseRel the name of the inverse property (to be) defined on otherEntity
         */
      Entity.hasMany = function (collName, otherEntity, invRel) {
        var otherMeta = otherEntity.meta;
        if (otherMeta.hasMany[invRel]) {
          // other side has declared it as a one-to-many relation too -> it's in
          // fact many-to-many
          var tableName = meta.name + "_" + collName + "_" + otherMeta.name;
          var inverseTableName = otherMeta.name + '_' + invRel + '_' + meta.name;

          if (tableName > inverseTableName) {
            // Some arbitrary way to deterministically decide which table to generate
            tableName = inverseTableName;
          }
          meta.hasMany[collName] = {
            type: otherEntity,
            inverseProperty: invRel,
            manyToMany: true,
            tableName: tableName
          };
          otherMeta.hasMany[invRel] = {
            type: Entity,
            inverseProperty: collName,
            manyToMany: true,
            tableName: tableName
          };
          delete meta.hasOne[collName];
          delete meta.fields[collName + "_class"]; // in case it existed
        } else {
          meta.hasMany[collName] = {
            type: otherEntity,
            inverseProperty: invRel
          };
          otherMeta.hasOne[invRel] = {
            type: Entity,
            inverseProperty: collName
          };
          if (meta.isMixin)
            otherMeta.fields[invRel + "_class"] = persistence.typeMapper ? persistence.typeMapper.classNameType : "TEXT";
        }
      }

      Entity.hasOne = function (refName, otherEntity, inverseProperty) {
        meta.hasOne[refName] = {
          type: otherEntity,
          inverseProperty: inverseProperty
        };
        if (otherEntity.meta.isMixin)
          meta.fields[refName + "_class"] = persistence.typeMapper ? persistence.typeMapper.classNameType : "TEXT";
      };

      Entity.is = function(mixin){
        var mixinMeta = mixin.meta;
        if (!mixinMeta.isMixin)
          throw new Error("not a mixin: " + mixin);

        mixin.meta.mixedIns = mixin.meta.mixedIns || [];
        mixin.meta.mixedIns.push(meta);

        for (var field in mixinMeta.fields) {
          if (mixinMeta.fields.hasOwnProperty(field))
            meta.fields[field] = mixinMeta.fields[field];
        }
        for (var it in mixinMeta.hasOne) {
          if (mixinMeta.hasOne.hasOwnProperty(it))
            meta.hasOne[it] = mixinMeta.hasOne[it];
        }
        for (var it in mixinMeta.hasMany) {
          if (mixinMeta.hasMany.hasOwnProperty(it)) {
            mixinMeta.hasMany[it].mixin = mixin;
            meta.hasMany[it] = mixinMeta.hasMany[it];
          }
        }
      }

      // Allow decorator functions to add more stuff
      var fns = persistence.entityDecoratorHooks;
      for(var i = 0; i < fns.length; i++) {
        fns[i](Entity);
      }

      entityClassCache[entityName] = Entity;
      return Entity;
    }

    persistence.jsonToEntityVal = function(value, type) {
      if(type) {
        switch(type) {
        case 'DATE':
          if(typeof value === 'number') {
            if (value > 1000000000000) {
              // it's in milliseconds
              return new Date(value); 
            } else {
              return new Date(value * 1000); 
            }
          } else {
            return null;
          }
          break;
        default:
          return value;
        }
      } else {
        return value;
      }
    };

    persistence.entityValToJson = function(value, type) {
      if(type) {
        switch(type) {
        case 'DATE':
          if(value) {
            value = new Date(value);
            return Math.round(value.getTime() / 1000);
          } else {
            return null;
          }
          break;
        default:
          return value;
        }
      } else {
        return value;
      }
    };

    /**
     * Dumps the entire database into an object (that can be serialized to JSON for instance)
     * @param tx transaction to use, use `null` to start a new one
     * @param entities a list of entity constructor functions to serialize, use `null` for all
     * @param callback (object) the callback function called with the results.
     */
    persistence.dump = function(tx, entities, callback) {
      var args = argspec.getArgs(arguments, [
          { name: 'tx', optional: true, check: persistence.isTransaction, defaultValue: null },
          { name: 'entities', optional: true, check: function(obj) { return !obj || (obj && obj.length && !obj.apply); }, defaultValue: null },
          { name: 'callback', optional: false, check: argspec.isCallback(), defaultValue: function(){} }
        ]);
      tx = args.tx;
      entities = args.entities;
      callback = args.callback;

      if(!entities) { // Default: all entity types
        entities = [];
        for(var e in entityClassCache) {
          if(entityClassCache.hasOwnProperty(e)) {
            entities.push(entityClassCache[e]);
          }
        }
      }

      var result = {};
      persistence.asyncParForEach(entities, function(Entity, callback) {
          Entity.all().list(tx, function(all) {
              var items = [];
              persistence.asyncParForEach(all, function(e, callback) {
                  var rec = {};
                  var fields = Entity.meta.fields;
                  for(var f in fields) {
                    if(fields.hasOwnProperty(f)) {
                      rec[f] = persistence.entityValToJson(e._data[f], fields[f]);
                    }
                  }
                  var refs = Entity.meta.hasOne;
                  for(var r in refs) {
                    if(refs.hasOwnProperty(r)) {
                      rec[r] = e._data[r];
                    }
                  }
                  var colls = Entity.meta.hasMany;
                  var collArray = [];
                  for(var coll in colls) {
                    if(colls.hasOwnProperty(coll)) {
                      collArray.push(coll);
                    }
                  }
                  persistence.asyncParForEach(collArray, function(collP, callback) {
                      var coll = persistence.get(e, collP);
                      coll.list(tx, function(results) {
                          rec[collP] = results.map(function(r) { return r.id; });
                          callback();
                        });
                    }, function() {
                      rec.id = e.id;
                      items.push(rec);
                      callback();
                    });
                }, function() {
                  result[Entity.meta.name] = items;
                  callback();
                });
            });
        }, function() {
          callback(result);
        });
    };

    /**
     * Loads a set of entities from a dump object
     * @param tx transaction to use, use `null` to start a new one
     * @param dump the dump object
     * @param callback the callback function called when done.
     */
    persistence.load = function(tx, dump, callback) {
      var args = argspec.getArgs(arguments, [
          { name: 'tx', optional: true, check: persistence.isTransaction, defaultValue: null },
          { name: 'dump', optional: false },
          { name: 'callback', optional: true, check: argspec.isCallback(), defaultValue: function(){} }
        ]);
      tx = args.tx;
      dump = args.dump;
      callback = args.callback;

      var finishedCount = 0;
      var collItemsToAdd = [];
      var session = this;
      for(var entityName in dump) {
        if(dump.hasOwnProperty(entityName)) {
          var Entity = getEntity(entityName);
          var fields = Entity.meta.fields;
          var instances = dump[entityName];
          for(var i = 0; i < instances.length; i++) {
            var instance = instances[i];
            var ent = new Entity();
            ent.id = instance.id;
            for(var p in instance) {
              if(instance.hasOwnProperty(p)) {
                if (persistence.isImmutable(p)) {
                  ent[p] = instance[p];
                } else if(Entity.meta.hasMany[p]) { // collection
                  var many = Entity.meta.hasMany[p];
                  if(many.manyToMany && Entity.meta.name < many.type.meta.name) { // Arbitrary way to avoid double adding
                    continue;
                  }
                  var coll = persistence.get(ent, p);
                  if(instance[p].length > 0) {
                    instance[p].forEach(function(it) {
                        collItemsToAdd.push({Entity: Entity, coll: coll, id: it});
                      });
                  }
                } else {
                  persistence.set(ent, p, persistence.jsonToEntityVal(instance[p], fields[p]));
                }
              }
            }
            this.add(ent);
          }
        }
      }
      session.flush(tx, function() {
          persistence.asyncForEach(collItemsToAdd, function(collItem, callback) {
              collItem.Entity.load(session, tx, collItem.id, function(obj) {
                  collItem.coll.add(obj);
                  callback();
                });
            }, function() {
              session.flush(tx, callback);
            });
        });
    };

    /**
     * Dumps the entire database to a JSON string
     * @param tx transaction to use, use `null` to start a new one
     * @param entities a list of entity constructor functions to serialize, use `null` for all
     * @param callback (jsonDump) the callback function called with the results.
     */
    persistence.dumpToJson = function(tx, entities, callback) {
      var args = argspec.getArgs(arguments, [
          { name: 'tx', optional: true, check: persistence.isTransaction, defaultValue: null },
          { name: 'entities', optional: true, check: function(obj) { return obj && obj.length && !obj.apply; }, defaultValue: null },
          { name: 'callback', optional: false, check: argspec.isCallback(), defaultValue: function(){} }
        ]);
      tx = args.tx;
      entities = args.entities;
      callback = args.callback;
      this.dump(tx, entities, function(obj) {
          callback(JSON.stringify(obj));
        });
    };

    /**
     * Dumps the raw database to a javascript object.  will preserve encryption
     *
     * @param tx transaction to use, use `null` to start a new one
     * @param entities is an object with where clause for filtering objects in a table
     * @param callback(obj) the callback function called with the results.
     */
    persistence.dumpDatabaseForSync = function(entities, callback) {
      var args = argspec.getArgs(arguments, [
          { name: 'entities', optional: false, check: function(obj) { return obj; }, defaultValue: null },
          { name: 'callback', optional: false, check: argspec.isCallback(), defaultValue: function(){} }
        ]);

      entities = args.entities;
      callback = args.callback;

      ABINE_DNTME.require(['documentcloud/underscore'], function(_){
        var clientData = {};
        _.async.forEach(_.keys(entities), function(entityName, cb) {
          persistence.transaction(function(t) {
            var where = '';
            if (entities[entityName] != 'all') {
              where = ' where '+entities[entityName];
            }
            t.executeSql('SELECT * FROM ' + entityName + where, null, function(result) {
              clientData[entityName] = result;
              cb();
            });
          });
        }, function(err) {
          callback(clientData);
        });
      });
    };


    /**
     * Loads data from a JSON string (as dumped by `dumpToJson`)
     * @param tx transaction to use, use `null` to start a new one
     * @param jsonDump JSON string
     * @param callback the callback function called when done.
     */
    persistence.loadFromJson = function(tx, jsonDump, callback) {
      var args = argspec.getArgs(arguments, [
          { name: 'tx', optional: true, check: persistence.isTransaction, defaultValue: null },
          { name: 'jsonDump', optional: false },
          { name: 'callback', optional: true, check: argspec.isCallback(), defaultValue: function(){} }
        ]);
      tx = args.tx;
      jsonDump = args.jsonDump;
      callback = args.callback;
      this.load(tx, JSON.parse(jsonDump), callback);
    };


    /**
     * Generates a UUID according to http://www.ietf.org/rfc/rfc4122.txt
     */
    function createUUID () {
      if(persistence.typeMapper && persistence.typeMapper.newUuid) {
        return persistence.typeMapper.newUuid();
      }
      var s = [];
      var hexDigits = "0123456789ABCDEF";
      for ( var i = 0; i < 32; i++) {
        s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
      }
      s[12] = "4";
      s[16] = hexDigits.substr((s[16] & 0x3) | 0x8, 1);

      var uuid = s.join("");
      return uuid;
    }

    persistence.createUUID = createUUID;


    function defaultValue(type) {
      if(persistence.typeMapper && persistence.typeMapper.defaultValue) {
        return persistence.typeMapper.defaultValue(type);
      }
      switch(type) {
      case "TEXT": return "";
      case "BOOL": return false;
      default:
        if(type.indexOf("INT") !== -1) {
          return 0;
        } else if(type.indexOf("CHAR") !== -1) {
          return "";
        } else {
          return null;
        }
      }
    }

    function arrayContains(ar, item) {
      var l = ar.length;
      for(var i = 0; i < l; i++) {
        var el = ar[i];
        if(el.equals && el.equals(item)) {
          return true;
        } else if(el === item) {
          return true;
        }
      }
      return false;
    }

    function arrayRemove(ar, item) {
      var l = ar.length;
      for(var i = 0; i < l; i++) {
        var el = ar[i];
        if(el.equals && el.equals(item)) {
          ar.splice(i, 1);
          return;
        } else if(el === item) {
          ar.splice(i, 1);
          return;
        }
      }
    }

    ////////////////// QUERY COLLECTIONS \\\\\\\\\\\\\\\\\\\\\\\

    function Subscription(obj, eventType, fn) {
      this.obj = obj;
      this.eventType = eventType;
      this.fn = fn;
    }

    Subscription.prototype.unsubscribe = function() {
      this.obj.removeEventListener(this.eventType, this.fn);
    };

    /**
     * Simple observable function constructor
     * @constructor
     */
    function Observable() {
      this.subscribers = {};
    }

    Observable.prototype.addEventListener = function (eventType, fn) {
      if (!this.subscribers[eventType]) {
        this.subscribers[eventType] = [];
      }
      this.subscribers[eventType].push(fn);
      return new Subscription(this, eventType, fn);
    };

    Observable.prototype.removeEventListener = function(eventType, fn) {
      var subscribers = this.subscribers[eventType];
      for ( var i = 0; i < subscribers.length; i++) {
        if(subscribers[i] == fn) {
          this.subscribers[eventType].splice(i, 1);
          return true;
        }
      }
      return false;
    };

    Observable.prototype.triggerEvent = function (eventType) {
      if (!this.subscribers[eventType]) { // No subscribers to this event type
        return;
      }
      var subscribers = this.subscribers[eventType].slice(0);
      for(var i = 0; i < subscribers.length; i++) {
        subscribers[i].apply(null, arguments);
      }
    };

    /*
     * Each filter has 4 methods:
     * - sql(prefix, values) -- returns a SQL representation of this filter,
     *     possibly pushing additional query arguments to `values` if ?'s are used
     *     in the query
     * - match(o) -- returns whether the filter matches the object o.
     * - makeFit(o) -- attempts to adapt the object o in such a way that it matches
     *     this filter.
     * - makeNotFit(o) -- the oppositive of makeFit, makes the object o NOT match
     *     this filter
     */

    /**
     * Default filter that does not filter on anything
     * currently it generates a 1=1 SQL query, which is kind of ugly
     */
    function NullFilter () {
    }

    NullFilter.prototype.match = function (o) {
      return true;
    };

    NullFilter.prototype.makeFit = function(o) {
    };

    NullFilter.prototype.makeNotFit = function(o) {
    };

    NullFilter.prototype.toUniqueString = function() {
      return "NULL";
    };

    NullFilter.prototype.subscribeGlobally = function() { };

    NullFilter.prototype.unsubscribeGlobally = function() { };

    /**
     * Filter that makes sure that both its left and right filter match
     * @param left left-hand filter object
     * @param right right-hand filter object
     */
    function AndFilter (left, right) {
      this.left = left;
      this.right = right;
    }

    AndFilter.prototype.match = function (o) {
      return this.left.match(o) && this.right.match(o);
    };

    AndFilter.prototype.makeFit = function(o) {
      this.left.makeFit(o);
      this.right.makeFit(o);
    };

    AndFilter.prototype.makeNotFit = function(o) {
      this.left.makeNotFit(o);
      this.right.makeNotFit(o);
    };

    AndFilter.prototype.toUniqueString = function() {
      return this.left.toUniqueString() + " AND " + this.right.toUniqueString();
    };

    AndFilter.prototype.subscribeGlobally = function(coll, entityName) { 
      this.left.subscribeGlobally(coll, entityName);
      this.right.subscribeGlobally(coll, entityName);
    };

    AndFilter.prototype.unsubscribeGlobally = function(coll, entityName) { 
      this.left.unsubscribeGlobally(coll, entityName);
      this.right.unsubscribeGlobally(coll, entityName);
    };

    /**
     * Filter that makes sure that either its left and right filter match
     * @param left left-hand filter object
     * @param right right-hand filter object
     */
    function OrFilter (left, right) {
      this.left = left;
      this.right = right;
    }

    OrFilter.prototype.match = function (o) {
      return this.left.match(o) || this.right.match(o);
    };

    OrFilter.prototype.makeFit = function(o) {
      this.left.makeFit(o);
      this.right.makeFit(o);
    };

    OrFilter.prototype.makeNotFit = function(o) {
      this.left.makeNotFit(o);
      this.right.makeNotFit(o);
    };

    OrFilter.prototype.toUniqueString = function() {
      return this.left.toUniqueString() + " OR " + this.right.toUniqueString();
    };

    OrFilter.prototype.subscribeGlobally = function(coll, entityName) { 
      this.left.subscribeGlobally(coll, entityName);
      this.right.subscribeGlobally(coll, entityName);
    };

    OrFilter.prototype.unsubscribeGlobally = function(coll, entityName) { 
      this.left.unsubscribeGlobally(coll, entityName);
      this.right.unsubscribeGlobally(coll, entityName);
    };

    /**
     * Filter that checks whether a certain property matches some value, based on an
     * operator. Supported operators are '=', '!=', '<', '<=', '>' and '>='.
     * @param property the property name
     * @param operator the operator to compare with
     * @param value the literal value to compare to
     */
    function PropertyFilter (property, operator, value) {
      this.property = property;
      this.operator = operator.toLowerCase();
      this.value = value;
    }

    PropertyFilter.prototype.match = function (o) {
      var value = this.value;
      var propValue = persistence.get(o, this.property);
      if(value && value.getTime) { // DATE
        // TODO: Deal with arrays of dates for 'in' and 'not in'
        value = Math.round(value.getTime() / 1000) * 1000; // Deal with precision
        if(propValue && propValue.getTime) { // DATE
          propValue = Math.round(propValue.getTime() / 1000) * 1000; // Deal with precision
        }
      }
      switch (this.operator) {
      case '=':
        return propValue === value;
        break;
      case '!=':
        return propValue !== value;
        break;
      case '<':
        return propValue < value;
        break;
      case '<=':
        return propValue <= value;
        break;
      case '>':
        return propValue > value;
        break;
      case '>=':
        return propValue >= value;
        break;
      case 'in':
        return arrayContains(value, propValue);
        break;
      case 'not in':
        return !arrayContains(value, propValue);
        break;
      }
    };

    PropertyFilter.prototype.makeFit = function(o) {
      if(this.operator === '=') {
        persistence.set(o, this.property, this.value);
      } else {
        throw new Error("Sorry, can't perform makeFit for other filters than =");
      }
    };

    PropertyFilter.prototype.makeNotFit = function(o) {
      if(this.operator === '=') {
        persistence.set(o, this.property, null);
      } else {
        throw new Error("Sorry, can't perform makeNotFit for other filters than =");
      }
    };

    PropertyFilter.prototype.subscribeGlobally = function(coll, entityName) {
      persistence.subscribeToGlobalPropertyListener(coll, entityName, this.property);
    };

    PropertyFilter.prototype.unsubscribeGlobally = function(coll, entityName) {
      persistence.unsubscribeFromGlobalPropertyListener(coll, entityName, this.property);
    };

    PropertyFilter.prototype.toUniqueString = function() {
      var val = this.value;
      if(val && val._type) {
        val = val.id;
      }
      return this.property + this.operator + val;
    };

    persistence.NullFilter = NullFilter;
    persistence.AndFilter = AndFilter;
    persistence.OrFilter = OrFilter;
    persistence.PropertyFilter = PropertyFilter;

    /**
     * Ensure global uniqueness of query collection object
     */
    persistence.uniqueQueryCollection = function(coll) {
      var entityName = coll._entityName;
      if(coll._items) { // LocalQueryCollection
        return coll;
      }
      if(!this.queryCollectionCache[entityName]) {
        this.queryCollectionCache[entityName] = {};
      }
      var uniqueString = coll.toUniqueString();
      if(!this.queryCollectionCache[entityName][uniqueString]) {
        this.queryCollectionCache[entityName][uniqueString] = coll;
      }
      return this.queryCollectionCache[entityName][uniqueString];
    }

    /**
     * The constructor function of the _abstract_ QueryCollection
     * DO NOT INSTANTIATE THIS
     * @constructor
     */
    function QueryCollection () {
    }

    QueryCollection.prototype = new Observable();

    QueryCollection.prototype.oldAddEventListener = QueryCollection.prototype.addEventListener;

    QueryCollection.prototype.setupSubscriptions = function() { 
      this._filter.subscribeGlobally(this, this._entityName);
    };

    QueryCollection.prototype.teardownSubscriptions = function() { 
      this._filter.unsubscribeGlobally(this, this._entityName);
    };

    QueryCollection.prototype.addEventListener = function(eventType, fn) {
      var that = this;
      var subscription = this.oldAddEventListener(eventType, fn);
      if(this.subscribers[eventType].length === 1) { // first subscriber
        this.setupSubscriptions();
      }
      subscription.oldUnsubscribe = subscription.unsubscribe;
      subscription.unsubscribe = function() {
        this.oldUnsubscribe();

        if(that.subscribers[eventType].length === 0) { // last subscriber
          that.teardownSubscriptions();
        }
      };
      return subscription;
    };

    /**
     * Function called when session is flushed, returns list of SQL queries to execute
     * (as [query, arg] tuples)
     */
    QueryCollection.prototype.persistQueries = function() { return []; };

    /**
     * Invoked by sub-classes to initialize the query collection
     */
    QueryCollection.prototype.init = function (session, entityName, constructor) {
      this._filter = new NullFilter();
      this._orderColumns = []; // tuples of [column, ascending]
      this._prefetchFields = [];
      this._entityName = entityName;
      this._constructor = constructor;
      this._limit = -1;
      this._skip = 0;
      this._reverse = false;
      this._session = session || persistence;
      // For observable
      this.subscribers = {};
    }

    QueryCollection.prototype.toUniqueString = function() {
      var s = this._constructor.name + ": " + this._entityName;
      s += '|Filter:';
      var values = [];
      s += this._filter.toUniqueString();
      s += '|Values:';
      for(var i = 0; i < values.length; i++) {
        s += values + "|^|";
      }
      s += '|Order:';
      for(var i = 0; i < this._orderColumns.length; i++) {
        var col = this._orderColumns[i];
        s += col[0] + ", " + col[1] + ", " + col[2];
      }
      s += '|Prefetch:';
      for(var i = 0; i < this._prefetchFields.length; i++) {
        s += this._prefetchFields[i];
      }
      s += '|Limit:';
      s += this._limit;
      s += '|Skip:';
      s += this._skip;
      s += '|Reverse:';
      s += this._reverse;
      return s;
    };

    /**
     * Creates a clone of this query collection
     * @return a clone of the collection
     */
    QueryCollection.prototype.clone = function (cloneSubscribers) {
      var c = new (this._constructor)(this._session, this._entityName);
      c._filter = this._filter;
      c._prefetchFields = this._prefetchFields.slice(0); // clone
      c._orderColumns = this._orderColumns.slice(0);
      c._limit = this._limit;
      c._skip = this._skip;
      c._reverse = this._reverse;
      if(cloneSubscribers) {
        var subscribers = {};
        for(var eventType in this.subscribers) {
          if(this.subscribers.hasOwnProperty(eventType)) {
            subscribers[eventType] = this.subscribers[eventType].slice(0);
          }
        }
        c.subscribers = subscribers; //this.subscribers;
      } else {
        c.subscribers = this.subscribers;
      }
      return c;
    };

    /**
     * Returns a new query collection with a property filter condition added
     * @param property the property to filter on
     * @param operator the operator to use
     * @param value the literal value that the property should match
     * @return the query collection with the filter added
     */
    QueryCollection.prototype.filter = function (property, operator, value) {
      var c = this.clone(true);
      c._filter = new AndFilter(this._filter, new PropertyFilter(property,
          operator, value));
      // Add global listener (TODO: memory leak waiting to happen!)
      var session = this._session;
      c = session.uniqueQueryCollection(c);
      //session.subscribeToGlobalPropertyListener(c, this._entityName, property);
      return session.uniqueQueryCollection(c);
    };

    /**
     * Returns a new query collection with an OR condition between the
     * current filter and the filter specified as argument
     * @param filter the other filter
     * @return the new query collection
     */
    QueryCollection.prototype.or = function (filter) {
      var c = this.clone(true);
      c._filter = new OrFilter(this._filter, filter);
      return this._session.uniqueQueryCollection(c);
    };

    /**
     * Returns a new query collection with an AND condition between the
     * current filter and the filter specified as argument
     * @param filter the other filter
     * @return the new query collection
     */
    QueryCollection.prototype.and = function (filter) {
      var c = this.clone(true);
      c._filter = new AndFilter(this._filter, filter);
      return this._session.uniqueQueryCollection(c);
    };

    /**
     * Returns a new query collection with an ordering imposed on the collection
     * @param property the property to sort on
     * @param ascending should the order be ascending (= true) or descending (= false)
     * @param caseSensitive should the order be case sensitive (= true) or case insensitive (= false)
     *        note: using case insensitive ordering for anything other than TEXT fields yields
     *        undefinded behavior
     * @return the query collection with imposed ordering
     */
    QueryCollection.prototype.order = function (property, ascending, caseSensitive) {
      ascending = ascending === undefined ? true : ascending;
      caseSensitive = caseSensitive === undefined ? true : caseSensitive;
      var c = this.clone();
      c._orderColumns.push( [ property, ascending, caseSensitive ]);
      return this._session.uniqueQueryCollection(c);
    };

    /**
     * Returns a new query collection will limit its size to n items
     * @param n the number of items to limit it to
     * @return the limited query collection
     */
    QueryCollection.prototype.limit = function(n) {
      var c = this.clone();
      c._limit = n;
      return this._session.uniqueQueryCollection(c);
    };

    /**
     * Returns a new query collection which will skip the first n results
     * @param n the number of results to skip
     * @return the query collection that will skip n items
     */
    QueryCollection.prototype.skip = function(n) {
      var c = this.clone();
      c._skip = n;
      return this._session.uniqueQueryCollection(c);
    };

    /**
     * Returns a new query collection which reverse the order of the result set
     * @return the query collection that will reverse its items
     */
    QueryCollection.prototype.reverse = function() {
      var c = this.clone();
      c._reverse = true;
      return this._session.uniqueQueryCollection(c);
    };

    /**
     * Returns a new query collection which will prefetch a certain object relationship.
     * Only works with 1:1 and N:1 relations.
     * Relation must target an entity, not a mix-in.
     * @param rel the relation name of the relation to prefetch
     * @return the query collection prefetching `rel`
     */
    QueryCollection.prototype.prefetch = function (rel) {
      var c = this.clone();
      c._prefetchFields.push(rel);
      return this._session.uniqueQueryCollection(c);
    };


    /**
     * Select a subset of data, represented by this query collection as a JSON
     * structure (Javascript object)
     *
     * @param tx database transaction to use, leave out to start a new one
     * @param props a property specification
     * @param callback(result)
     */
    QueryCollection.prototype.selectJSON = function(tx, props, callback) {
      var args = argspec.getArgs(arguments, [
          { name: "tx", optional: true, check: persistence.isTransaction, defaultValue: null },
          { name: "props", optional: false },
          { name: "callback", optional: false }
        ]);
      var session = this._session;
      var that = this;
      tx = args.tx;
      props = args.props;
      callback = args.callback;

      if(!tx) {
        session.transaction(function(tx) {
            that.selectJSON(tx, props, callback);
          });
        return;
      }
      var Entity = getEntity(this._entityName);
      // TODO: This could do some clever prefetching to make it more efficient
      this.list(function(items) {
          var resultArray = [];
          persistence.asyncForEach(items, function(item, callback) {
              item.selectJSON(tx, props, function(obj) {
                  resultArray.push(obj);
                  callback();
                });
            }, function() {
              callback(resultArray);
            });
        });
    };

    /**
     * Adds an object to a collection
     * @param obj the object to add
     */
    QueryCollection.prototype.add = function(obj) {
      if(!obj.id || !obj._type) {
        throw new Error("Cannot add object of non-entity type onto collection.");
      }
      this._session.add(obj);
      this._filter.makeFit(obj);
      this.triggerEvent('add', this, obj);
      this.triggerEvent('change', this, obj);
    }
    
    /**
     * Adds an an array of objects to a collection
     * @param obj the object to add
     */
    QueryCollection.prototype.addAll = function(objs) {
      for(var i = 0; i < objs.length; i++) {
        var obj = objs[i];
        this._session.add(obj);
        this._filter.makeFit(obj);
        this.triggerEvent('add', this, obj);
      }
      this.triggerEvent('change', this);
    }

    /**
     * Removes an object from a collection
     * @param obj the object to remove from the collection
     */
    QueryCollection.prototype.remove = function(obj) {
      if(!obj.id || !obj._type) {
        throw new Error("Cannot remove object of non-entity type from collection.");
      }
      this._filter.makeNotFit(obj);
      this.triggerEvent('remove', this, obj);
      this.triggerEvent('change', this, obj);
    }


    /**
     * A database implementation of the QueryCollection
     * @param entityName the name of the entity to create the collection for
     * @constructor
     */
    function DbQueryCollection (session, entityName) {
      this.init(session, entityName, DbQueryCollection);
    }

    /**
     * Execute a function for each item in the list
     * @param tx the transaction to use (or null to open a new one)
     * @param eachFn (elem) the function to be executed for each item
     */
    QueryCollection.prototype.each = function (tx, eachFn) {
      var args = argspec.getArgs(arguments, [
          { name: 'tx', optional: true, check: persistence.isTransaction, defaultValue: null },
          { name: 'eachFn', optional: true, check: argspec.isCallback() }
        ]);
      tx = args.tx;
      eachFn = args.eachFn;

      this.list(tx, function(results) {
          for(var i = 0; i < results.length; i++) {
            eachFn(results[i]);
          }
        });
    }

    // Alias
    QueryCollection.prototype.forEach = QueryCollection.prototype.each;

    QueryCollection.prototype.one = function (tx, oneFn) {
      var args = argspec.getArgs(arguments, [
          { name: 'tx', optional: true, check: persistence.isTransaction, defaultValue: null },
          { name: 'oneFn', optional: false, check: argspec.isCallback() }
        ]);
      tx = args.tx;
      oneFn = args.oneFn;

      var that = this;

      this.limit(1).list(tx, function(results) {
          if(results.length === 0) {
            oneFn(null);
          } else {
            oneFn(results[0]);
          }
        });
    }

    DbQueryCollection.prototype = new QueryCollection();


    /**
     * An implementation of QueryCollection, that is used
     * to represent all instances of an entity type
     * @constructor
     */
    function AllDbQueryCollection (session, entityName) {
      this.init(session, entityName, AllDbQueryCollection);
    }

    AllDbQueryCollection.prototype = new DbQueryCollection();

    AllDbQueryCollection.prototype.add = function(obj) {
      this._session.add(obj);
      this.triggerEvent('add', this, obj);
      this.triggerEvent('change', this, obj);
    };

    AllDbQueryCollection.prototype.remove = function(obj) {
      this._session.remove(obj);
      this.triggerEvent('remove', this, obj);
      this.triggerEvent('change', this, obj);
    };

    /**
     * A ManyToMany implementation of QueryCollection
     * @constructor
     */
    function ManyToManyDbQueryCollection (session, entityName) {
      this.init(session, entityName, persistence.ManyToManyDbQueryCollection);
      this._localAdded = [];
      this._localRemoved = [];
    }

    ManyToManyDbQueryCollection.prototype = new DbQueryCollection();

    ManyToManyDbQueryCollection.prototype.initManyToMany = function(obj, coll) {
      this._obj = obj;
      this._coll = coll;
    };

    ManyToManyDbQueryCollection.prototype.add = function(obj) {
      if(!arrayContains(this._localAdded, obj)) {
        this._session.add(obj);
        this._localAdded.push(obj);
        this.triggerEvent('add', this, obj);
        this.triggerEvent('change', this, obj);
      }
    };

    ManyToManyDbQueryCollection.prototype.addAll = function(objs) {
      for(var i = 0; i < objs.length; i++) {
        var obj = objs[i];
        if(!arrayContains(this._localAdded, obj)) {
          this._session.add(obj);
          this._localAdded.push(obj);
          this.triggerEvent('add', this, obj);
        }
      }
      this.triggerEvent('change', this);
    }

    ManyToManyDbQueryCollection.prototype.clone = function() {
      var c = DbQueryCollection.prototype.clone.call(this);
      c._localAdded = this._localAdded;
      c._localRemoved = this._localRemoved;
      c._obj = this._obj;
      c._coll = this._coll;
      return c;
    };

    ManyToManyDbQueryCollection.prototype.remove = function(obj) {
      if(arrayContains(this._localAdded, obj)) { // added locally, can just remove it from there
        arrayRemove(this._localAdded, obj);
      } else if(!arrayContains(this._localRemoved, obj)) {
        this._localRemoved.push(obj);
      }
      this.triggerEvent('remove', this, obj);
      this.triggerEvent('change', this, obj);
    };

    ////////// Local implementation of QueryCollection \\\\\\\\\\\\\\\\

    function LocalQueryCollection(initialArray) {
      this.init(persistence, null, LocalQueryCollection);
      this._items = initialArray || [];
    }

    LocalQueryCollection.prototype = new QueryCollection();

    LocalQueryCollection.prototype.clone = function() {
      var c = DbQueryCollection.prototype.clone.call(this);
      c._items = this._items;
      return c;
    };

    LocalQueryCollection.prototype.add = function(obj) {
      if(!arrayContains(this._items, obj)) {
        this._session.add(obj);
        this._items.push(obj);
        this.triggerEvent('add', this, obj);
        this.triggerEvent('change', this, obj);
      }
    };

    LocalQueryCollection.prototype.addAll = function(objs) {
      for(var i = 0; i < objs.length; i++) {
        var obj = objs[i];
        if(!arrayContains(this._items, obj)) {
          this._session.add(obj);
          this._items.push(obj);
          this.triggerEvent('add', this, obj);
        }
      }
      this.triggerEvent('change', this);
    }

    LocalQueryCollection.prototype.remove = function(obj) {
      var items = this._items;
      for(var i = 0; i < items.length; i++) {
        if(items[i] === obj) {
          this._items.splice(i, 1);
          this.triggerEvent('remove', this, obj);
          this.triggerEvent('change', this, obj);
        }
      }
    };

    LocalQueryCollection.prototype.list = function(tx, callback) {
      var args = argspec.getArgs(arguments, [
          { name: 'tx', optional: true, check: persistence.isTransaction, defaultValue: null },
          { name: 'callback', optional: true, check: argspec.isCallback() }
        ]);
      callback = args.callback;

      if(!callback || callback.executeSql) { // first argument is transaction
        callback = arguments[1]; // set to second argument
      }
      var array = this._items.slice(0);
      var that = this;
      var results = [];
      for(var i = 0; i < array.length; i++) {
        if(this._filter.match(array[i])) {
          results.push(array[i]);
        }
      }
      results.sort(function(a, b) {
          for(var i = 0; i < that._orderColumns.length; i++) {
            var col = that._orderColumns[i][0];
            var asc = that._orderColumns[i][1];
            var sens = that._orderColumns[i][2];
            var aVal = persistence.get(a, col);
            var bVal = persistence.get(b, col);
            if (!sens) {
              aVal = aVal.toLowerCase();
              bVal = bVal.toLowerCase();
            }
            if(aVal < bVal) {
              return asc ? -1 : 1;
            } else if(aVal > bVal) {
              return asc ? 1 : -1;
            }
          }
          return 0;
        });
      if(this._skip) {
        results.splice(0, this._skip);
      }
      if(this._limit > -1) {
        results = results.slice(0, this._limit);
      }
      if(this._reverse) {
        results.reverse();
      }
      if(callback) {
        callback(results);
      } else {
        return results;
      }
    };

    LocalQueryCollection.prototype.destroyAll = function(callback) {
      if(!callback || callback.executeSql) { // first argument is transaction
        callback = arguments[1]; // set to second argument
      }
      this._items = [];
      this.triggerEvent('change', this);
      if(callback) callback();
    };

    LocalQueryCollection.prototype.count = function(tx, callback) {
      var args = argspec.getArgs(arguments, [
          { name: 'tx', optional: true, check: persistence.isTransaction, defaultValue: null },
          { name: 'callback', optional: true, check: argspec.isCallback() }
        ]);
      tx = args.tx;
      callback = args.callback;

      var result = this.list();

      if(callback) {
        callback(result.length);
      } else {
        return result.length;
      }
    };

    persistence.QueryCollection             = QueryCollection;
    persistence.DbQueryCollection           = DbQueryCollection;
    persistence.ManyToManyDbQueryCollection = ManyToManyDbQueryCollection;
    persistence.LocalQueryCollection        = LocalQueryCollection;
    persistence.Observable                  = Observable;
    persistence.Subscription                = Subscription;
    persistence.AndFilter                   = AndFilter;
    persistence.OrFilter                    = OrFilter;
    persistence.PropertyFilter              = PropertyFilter;

    persistence.GlobalEvent                 = new Observable();
  }());

// ArgSpec.js library: http://github.com/zefhemel/argspecjs
var argspec = {};

(function() {
    argspec.getArgs = function(args, specs) {
      var argIdx = 0;
      var specIdx = 0;
      var argObj = {};
      while(specIdx < specs.length) {
        var s = specs[specIdx];
        var a = args[argIdx];
        if(s.optional) {
          if(a !== undefined && s.check(a)) {
            argObj[s.name] = a;
            argIdx++;
            specIdx++;
          } else {
            if(s.defaultValue !== undefined) {
              argObj[s.name] = s.defaultValue;
            }
            specIdx++;
          }
        } else {
          if(s.check && !s.check(a)) {
            throw new Error("Invalid value for argument: " + s.name + " Value: " + a);
          }
          argObj[s.name] = a;
          specIdx++;
          argIdx++;
        }
      }
      return argObj;
    }

    argspec.hasProperty = function(name) {
      return function(obj) {
        return obj && obj[name] !== undefined;
      };
    }

    argspec.hasType = function(type) {
      return function(obj) {
        return typeof obj === type;
      };
    }

    argspec.isCallback = function() {
      return function(obj) {
        return obj && obj.apply;
      };
    }
  }());

persistence.argspec = argspec;

  return persistence;
} // end of createPersistence



// JSON2 library, source: http://www.JSON.org/js.html
// Most modern browsers already support this natively, but mobile
// browsers often don't, hence this implementation
// Relevant APIs:
//    JSON.stringify(value, replacer, space)
//    JSON.parse(text, reviver)

if(typeof JSON === 'undefined') {
  JSON = {};
}
//var JSON = typeof JSON === 'undefined' ? window.JSON : {};
if (!JSON.stringify) {
  (function () {
      function f(n) {
        return n < 10 ? '0' + n : n;
      }
      if (typeof Date.prototype.toJSON !== 'function') {

        Date.prototype.toJSON = function (key) {

          return isFinite(this.valueOf()) ?
          this.getUTCFullYear()   + '-' +
            f(this.getUTCMonth() + 1) + '-' +
            f(this.getUTCDate())      + 'T' +
            f(this.getUTCHours())     + ':' +
            f(this.getUTCMinutes())   + ':' +
            f(this.getUTCSeconds())   + 'Z' : null;
        };

        String.prototype.toJSON =
          Number.prototype.toJSON =
          Boolean.prototype.toJSON = function (key) {
            return this.valueOf();
          };
      }

      var cx = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
      escapable = /[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
      gap, indent,
      meta = {
        '\b': '\\b',
        '\t': '\\t',
        '\n': '\\n',
        '\f': '\\f',
        '\r': '\\r',
        '"' : '\\"',
        '\\': '\\\\'
      },
      rep;

      function quote(string) {
        escapable.lastIndex = 0;
        return escapable.test(string) ?
        '"' + string.replace(escapable, function (a) {
            var c = meta[a];
            return typeof c === 'string' ? c :
            '\\u' + ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
          }) + '"' :
        '"' + string + '"';
      }


      function str(key, holder) {
        var i, k, v, length, mind = gap, partial, value = holder[key];

        if (value && typeof value === 'object' &&
          typeof value.toJSON === 'function') {
          value = value.toJSON(key);
        }

        if (typeof rep === 'function') {
          value = rep.call(holder, key, value);
        }

        switch (typeof value) {
        case 'string':
          return quote(value);
        case 'number':
          return isFinite(value) ? String(value) : 'null';
        case 'boolean':
        case 'null':
          return String(value);
        case 'object':
          if (!value) {
            return 'null';
          }

          gap += indent;
          partial = [];

          if (Object.prototype.toString.apply(value) === '[object Array]') {
            length = value.length;
            for (i = 0; i < length; i += 1) {
              partial[i] = str(i, value) || 'null';
            }

            v = partial.length === 0 ? '[]' :
            gap ? '[\n' + gap +
              partial.join(',\n' + gap) + '\n' +
              mind + ']' :
            '[' + partial.join(',') + ']';
            gap = mind;
            return v;
          }

          if (rep && typeof rep === 'object') {
            length = rep.length;
            for (i = 0; i < length; i += 1) {
              k = rep[i];
              if (typeof k === 'string') {
                v = str(k, value);
                if (v) {
                  partial.push(quote(k) + (gap ? ': ' : ':') + v);
                }
              }
            }
          } else {
            for (k in value) {
              if (Object.hasOwnProperty.call(value, k)) {
                v = str(k, value);
                if (v) {
                  partial.push(quote(k) + (gap ? ': ' : ':') + v);
                }
              }
            }
          }

          v = partial.length === 0 ? '{}' :
          gap ? '{\n' + gap + partial.join(',\n' + gap) + '\n' +
            mind + '}' : '{' + partial.join(',') + '}';
          gap = mind;
          return v;
        }
      }

      if (typeof JSON.stringify !== 'function') {
        JSON.stringify = function (value, replacer, space) {
          var i;
          gap = '';
          indent = '';
          if (typeof space === 'number') {
            for (i = 0; i < space; i += 1) {
              indent += ' ';
            }
          } else if (typeof space === 'string') {
            indent = space;
          }

          rep = replacer;
          if (replacer && typeof replacer !== 'function' &&
            (typeof replacer !== 'object' ||
              typeof replacer.length !== 'number')) {
            throw new Error('JSON.stringify');
          }

          return str('', {'': value});
        };
      }

      if (typeof JSON.parse !== 'function') {
        JSON.parse = function (text, reviver) {
          var j;
          function walk(holder, key) {
            var k, v, value = holder[key];
            if (value && typeof value === 'object') {
              for (k in value) {
                if (Object.hasOwnProperty.call(value, k)) {
                  v = walk(value, k);
                  if (v !== undefined) {
                    value[k] = v;
                  } else {
                    delete value[k];
                  }
                }
              }
            }
            return reviver.call(holder, key, value);
          }

          cx.lastIndex = 0;
          if (cx.test(text)) {
            text = text.replace(cx, function (a) {
                return '\\u' +
                  ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
              });
          }

          if (/^[\],:{}\s]*$/.
          test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, '@').
            replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').
            replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {
            j = eval('(' + text + ')');
            return typeof reviver === 'function' ?
            walk({'': j}, '') : j;
          }
          throw new SyntaxError('JSON.parse');
        };
      }
    }());
}

return persistence;

}); // end require wrapper
;
// Wrap in module

ABINE_DNTME.define('persistence/store/memory',
    ['persistence'], 
    function(persistence) {

if(!persistence.store) {
  persistence.store = {};
}

persistence.store.memory = {};

persistence.store.memory.config = function(persistence, dbname) {
  var argspec = persistence.argspec;
  dbname = dbname || 'persistenceData';

  var allObjects = {}; // entityName -> LocalQueryCollection

  persistence.getAllObjects = function() { return allObjects; };

  var defaultAdd = persistence.add;

  persistence.add = function(obj) {
    if(!this.trackedObjects[obj.id]) {
      defaultAdd.call(this, obj);
      var entityName = obj._type;
      if(!allObjects[entityName]) {
        allObjects[entityName] = new persistence.LocalQueryCollection();
        allObjects[entityName]._session = persistence;
      }
      allObjects[entityName].add(obj);
    }
    return this;
  };

  var defaultForceAdd = persistence.forceAdd;

  persistence.forceAdd = function (obj) {
    if(!obj) return;
    if (!this.trackedObjects[obj.id] || this.trackedObjects[obj.id] != obj) {
      defaultForceAdd.call(this, obj);
      var entityName = obj._type;
      if(!allObjects[entityName]) {
        allObjects[entityName] = new persistence.LocalQueryCollection();
        allObjects[entityName]._session = persistence;
      }
      try{allObjects[entityName].remove(this.trackedObjects[obj.id]);}catch(e){}
      allObjects[entityName].add(obj);
    }
    return this;
  };


  var defaultRemove = persistence.remove;

  persistence.remove = function(obj) {
    defaultRemove.call(this, obj);
    var entityName = obj._type;
    allObjects[entityName].remove(obj);
  };

  var defaultClearFromEntityCache = persistence.clearFromEntityCache;

  persistence.clearFromEntityCache = function(obj) {
    if (!obj) return;
    defaultClearFromEntityCache.call(this, obj);
    var entityName = obj._type;
    allObjects[entityName].remove(obj);
  };

  persistence.schemaSync = function (tx, callback, emulate) {
    var args = argspec.getArgs(arguments, [
        { name: "tx", optional: true, check: persistence.isTransaction, defaultValue: null },
        { name: "callback", optional: true, check: argspec.isCallback(), defaultValue: function(){} },
        { name: "emulate", optional: true, check: argspec.hasType('boolean') }
      ]);

    args.callback();
  };

  persistence.flush = function (tx, callback) {
    var args = argspec.getArgs(arguments, [
        { name: "tx", optional: true, check: persistence.isTransaction },
        { name: "callback", optional: true, check: argspec.isCallback(), defaultValue: function(){} }
      ]);

    var fns = persistence.flushHooks;
    var session = this;
    persistence.asyncForEach(fns, function(fn, callback) {
        fn(session, tx, callback);
      }, function() {
        var trackedObjects = persistence.trackedObjects;
        for(var id in trackedObjects) {
          if(trackedObjects.hasOwnProperty(id)) {
            if (persistence.objectsToRemove.hasOwnProperty(id)) {
              delete trackedObjects[id];
            } else {
              trackedObjects[id]._dirtyProperties = {};
            }
          }
        }
        args.callback();
      });
  };

  persistence.transaction = function(callback) {
      callback({executeSql: function() {} });
  };

  persistence.loadFromLocalStorage = function(callback) {
    var dump = window.localStorage.getItem(dbname);
    if(dump) {
      this.loadFromJson(dump, callback);
    } else {
      callback && callback();
    }
  };

  persistence.saveToLocalStorage = function(callback) {
    this.dumpToJson(function(dump) {
        window.localStorage.setItem(dbname, dump);
        if(callback) {
          callback();
        }
      });
  };

  /**
   * Remove all tables in the database (as defined by the model)
   */
  persistence.reset = function (tx, callback) {
    var args = argspec.getArgs(arguments, [
        { name: "tx", optional: true, check: persistence.isTransaction, defaultValue: null },
        { name: "callback", optional: true, check: argspec.isCallback(), defaultValue: function(){} }
      ]);
    tx = args.tx;
    callback = args.callback;

    allObjects = {};
    this.clean();
    callback();
  };

  /**
   * Dummy
   */
  persistence.close = function() {};

  // QueryCollection's list

  function makeLocalClone(otherColl) {
    var coll = allObjects[otherColl._entityName];
    if(!coll) {
      coll = new persistence.LocalQueryCollection();
    }
    coll = coll.clone();
    coll._filter = otherColl._filter;
    coll._prefetchFields = otherColl._prefetchFields;
    coll._orderColumns = otherColl._orderColumns;
    coll._limit = otherColl._limit;
    coll._skip = otherColl._skip;
    coll._reverse = otherColl._reverse;
    return coll;
  }
  /**
   * Asynchronous call to actually fetch the items in the collection
   * @param tx transaction to use
   * @param callback function to be called taking an array with 
   *   result objects as argument
   */
  persistence.DbQueryCollection.prototype.list = function (tx, callback) {
    var args = argspec.getArgs(arguments, [
        { name: 'tx', optional: true, check: persistence.isTransaction, defaultValue: null },
        { name: 'callback', optional: false, check: argspec.isCallback() }
      ]);
    tx = args.tx;
    callback = args.callback;

    var coll = makeLocalClone(this);
    coll.list(null, callback);
  };

  /**
   * Asynchronous call to remove all the items in the collection. 
   * Note: does not only remove the items from the collection, but
   * the items themselves.
   * @param tx transaction to use
   * @param callback function to be called when clearing has completed
   */
  persistence.DbQueryCollection.prototype.destroyAll = function (tx, callback) {
    var args = argspec.getArgs(arguments, [
        { name: 'tx', optional: true, check: persistence.isTransaction, defaultValue: null },
        { name: 'callback', optional: true, check: argspec.isCallback(), defaultValue: function(){} }
      ]);
    tx = args.tx;
    callback = args.callback;

    var coll = makeLocalClone(this);
    coll.destroyAll(null, callback);
  };

  /**
   * Asynchronous call to count the number of items in the collection.
   * @param tx transaction to use
   * @param callback function to be called when clearing has completed
   */
  persistence.DbQueryCollection.prototype.count = function (tx, callback) {
    var args = argspec.getArgs(arguments, [
        { name: 'tx', optional: true, check: persistence.isTransaction, defaultValue: null },
        { name: 'callback', optional: false, check: argspec.isCallback() }
      ]);
    tx = args.tx;
    callback = args.callback;

    var coll = makeLocalClone(this);
    coll.count(null, callback);
  };

  persistence.ManyToManyDbQueryCollection = function(session, entityName) {
    this.init(session, entityName, persistence.ManyToManyDbQueryCollection);
    this._items = [];
  };

  persistence.ManyToManyDbQueryCollection.prototype = new persistence.LocalQueryCollection();

  persistence.ManyToManyDbQueryCollection.prototype.initManyToMany = function(obj, coll) {
    this._obj = obj;
    this._coll = coll; // column name
  };

  persistence.ManyToManyDbQueryCollection.prototype.add = function(item, recursing) {
    persistence.LocalQueryCollection.prototype.add.call(this, item);
    if(!recursing) { // prevent recursively adding to one another
      // Let's find the inverse collection
      var meta = persistence.getMeta(this._obj._type);
      var inverseProperty = meta.hasMany[this._coll].inverseProperty;
      persistence.get(item, inverseProperty).add(this._obj, true);
    }
  };

  persistence.ManyToManyDbQueryCollection.prototype.remove = function(item, recursing) {
    persistence.LocalQueryCollection.prototype.remove.call(this, item);
    if(!recursing) { // prevent recursively adding to one another
      // Let's find the inverse collection
      var meta = persistence.getMeta(this._obj._type);
      var inverseProperty = meta.hasMany[this._coll].inverseProperty;
      persistence.get(item, inverseProperty).remove(this._obj, true); 
    }
  };
};

}); // end module
;
// lets us define alternate primary key, and hence efficient caching

ABINE_DNTME.define('persistence/caching',
    ['documentcloud/underscore', 'abine/core', 'persistence', 'abine/eventLogger', 'abine/timer'],
  function(_, abineCore, persistence) {

  persistence.entityDecoratorHooks.push(function(Entity) {

    /**
     * mark entity for caching.  optional alternate primary key and clearCacheCallback.
     */
    Entity.enableCaching = function(alternatePK, clearCacheCallback) {
      if (typeof alternatePK == 'function') {
        clearCacheCallback = alternatePK;
        alternatePK = null;
      }
      Entity.cached = false;
      Entity.meta.cached = true;
      Entity.meta.alternatePK = alternatePK;

      persistence.GlobalEvent.addEventListener('clean', function(){
        LOG_FINEST && ABINE_DNTME.log.finest("cache cleared for "+Entity.name);
        Entity.cached = false;
        if (clearCacheCallback) {
          clearCacheCallback();
        }
      });

    };

    /**
     * mark entity for indexing.
     */
    Entity.enableIndexing = function(indexField) {
      Entity.meta.indexed = indexField;
    };

  });

  return null;

}); // end module
;




ABINE_DNTME.require('persistence/store/memory', function(memory) { console.log("defined memory: " + memory)});
//ABINE_DNTME.require('persistence/store/sql', function(sql) { console.log("defined sql: " + sql); });
//ABINE_DNTME.require('persistence/store/websql');

ABINE_DNTME.define('abine/ajax',
    ['jquery'], 
    function($) {

  // Use the jquery ajax implementation
  return {
    ajax: $.ajax,
    get: $.get,
    getJSON: $.getJSON,
    post: $.post,
    getScript: $.getScript
  };

});


// # abine/assets
// API for getting resources such as images in the content script

ABINE_DNTME.define("abine/assets",
  ['wycats/handlebars'],
  function(Handlebars) {

  // ## Content Scripts

  // ### function assetUrl(localUrl)
  // Return a url that works in the content-script to access images and css files, etc.
  function assetUrl(localUrl) {
    return chrome.extension.getURL(localUrl);
  }

  function cardAssetUrl(localUrlStart, type, localUrlEnd) {
    var url = localUrlStart + type + localUrlEnd;
    return chrome.extension.getURL(url);
  }

  Handlebars.registerHelper('assetUrl', assetUrl);
  Handlebars.registerHelper('cardAssetUrl', cardAssetUrl);

  // Public API
  return {
    assetUrl: assetUrl,
    cardAssetUrl: cardAssetUrl
  };

});

// Background Messenger
// Object in background scripts that can communicate with content scripts
// Sends messages in the same format as Backbone events.
// Each message has a
// - name
// - payload

// TODO: change sendRequest to sendMessage when chrome 41 is released - (kiran)

ABINE_DNTME.define('abine/backgroundMessenger',
    ['documentcloud/underscore', 'abine/core'], 
    function(_, abineCore) {

  // Create a listener for all simple requests
  // Passes the payload, the sender (a tab)
  var onRequestListener = function onRequestListener(request, sender) {
    if (!sender || !sender.tab) {
      // to avoid errors in console.
      return;
    }
    if (request.eventName == 'FWD') {
      // used for content-script to panel communication. broadcast to content-script and all frames on same tab.
      // we can change this to sending to a single frame when chrome 41 is released.
      chrome.tabs.sendRequest(sender.tab.id, request);
      return;
    }
    this.trigger(request.eventName, request.payload, sender.tab.id);
  };
  
  var destroyed = function() {
    throw("Error. Attempt to call destroyed BackgroundMessenger.");
  };
  
  var BackgroundMessenger = abineCore.BaseClass.extend({

    initialize: function() {
      this.listener = _.bind(onRequestListener, this);
      chrome.extension.onRequest.addListener(this.listener);
    },
    
    // ### function on(eventName, callback, binding)
    // Starts listening to events from the content script
    // Defined on parent
    
    // ### function off(eventName, callback)
    // Stops listening to events from the background script
    // Defined on parent
    
    // ### function trigger(eventName, payload)
    // Triggers all of the handlers
    // Defined on parent
    
    // ### function send()
    // - eventName - name of the event
    // - payload - JSON serializable object
    // - recipient - the tab id to receive the message, defaults to the active tab
    //
    // Triggers the event, and sends a message to the content script
    send: function(eventName, payload, recipient) {
      this.trigger(eventName, payload, recipient);

      if(recipient == "test"){ return;}
      if ((!recipient || recipient == -1) && chrome.tabs) {
        // send to the current tab by default
        chrome.tabs.getSelected(null, function(tab) {
          recipient = tab.id;

          chrome.tabs.sendRequest(recipient, {
            eventName: eventName,
            payload: typeof(payload) === 'undefined'?{}:payload
          });
        });
      } else {
        if (!recipient || recipient == "background" || recipient == -1) { return; }
        chrome.tabs.sendRequest(recipient, {
          eventName: eventName,
          payload: typeof(payload) === 'undefined'?{}:payload
        });
      }
    },
    
    // ### function broadcast(eventName, payload)
    // - eventName - name of the event
    // - payload - JSON serializable object
    //
    // Triggers the event, and broadcasts the message to all maskme tabs
    broadcast: function(eventName, payload) {
      payload = payload || {};
      this.trigger(eventName, payload);
      chrome.tabs.query({
        url: chrome.extension.getURL("pages/index.html")
      }, function (adminTabs) {
        _(adminTabs).each(function(t) {
          // console.log("Broadcasting " + eventName + " to " + t.id);
          chrome.tabs.sendRequest(t.id, {
            eventName: eventName,
            payload: payload
          });
        });
      });
    },
    
    // ### function broadcastToAllTabs(eventName, payload)
    // - eventName - name of the event
    // - payload - JSON serializable object
    //
    // Triggers the event, and broadcasts the message to all tabs
    broadcastToAllTabs: function(eventName, payload) {
      payload = payload || {};
      this.trigger(eventName, payload);
      chrome.tabs.query({}, function (allTabs) {
        _(allTabs).each(function(t) {
          // console.log("Broadcasting " + eventName + " to " + t.id);
          chrome.tabs.sendRequest(t.id, {
            eventName: eventName,
            payload: payload
          });
        });
      });
    },

    // ### function destroy()
    // Destroy this messenger. Cleans up all listeners.
    destroy: function() {
      this.off();
      chrome.extension.onRequest.removeListener(onRequestListener);
      this.on = this.off = this.trigger = destroyed;
    },

    sendToWebapp: function (eventName, payload) {
      ABINE_DNTME.require(['abine/window'], function(window){
        window.getWebappTabs(function(tabIds) {
          _.each(tabIds, function(tabId){
            chrome.tabs.sendRequest(tabId, {
              eventName: eventName,
              payload: payload
            });
          });
        });
      });
    }
  });
  
  // Public
  return {
    
    implementation: "Chrome",
    
    // ### class BackgroundMessenger
    BackgroundMessenger: BackgroundMessenger
  
  };
  
});
ABINE_DNTME.define('abine/browserSyncStorage',
  ["documentcloud/underscore","abine/core"],
  function(_, abineCore){

    var emptyCallback = function(){};

    var BrowserSyncStorage;

    // doesn't work in teamcity because they are run in webkit and not chrome.
    if(!chrome || !chrome.storage || !chrome.storage.sync){
      BrowserSyncStorage = abineCore.BaseClass.extend({
        initialize: function(options){
          this.storage = {};
        },

        set: function(items, callback){
          _.extend(this.storage,items);
          if(callback){callback();}
        },

        remove: function(keys, callback){
          keys = typeof(keys) === "string" ? [keys] : keys;
          _.each(keys, _.bind(function(key){
            delete this.storage[key];
          },this));
          if(callback){callback();}
        },

        get: function(keys, callback){
          keys = typeof(keys) === "string" ? [keys] : keys;
          if(callback){callback(_.pick(this.storage, keys));}
        },

        clear: function(callback){
          this.storage = {};
          if(callback){callback();}
        }
      });
    } else {

      BrowserSyncStorage = abineCore.BaseClass.extend({
        initialize: function(options){
        },

        set: function(items, callback){
          chrome.storage.sync.set(items,callback);
        },

        remove: function(keys, callback){
          chrome.storage.sync.remove(keys,callback);
        },

        get: function(keys, callback){
          chrome.storage.sync.get(keys, callback);
        },

        clear: function(callback){
          chrome.storage.sync.clear(callback);
        }
      });
    }

    var browserSyncStorage = new BrowserSyncStorage({});
    return browserSyncStorage;
  });
ABINE_DNTME.define("abine/clipboard",
    ['abine/eventLogger'],
    function(logger) {
      return {
        copyToClipboard: function(data) {
          var textArea = document.createElement('textarea');
          document.getElementById('navbar').appendChild(textArea);
          textArea.focus();
          textArea.value = data;
          document.execCommand('selectAll');
          document.execCommand('copy');
          document.getElementById('navbar').removeChild(textArea);
        }
      }
    });


ABINE_DNTME.define('abine/baseConfig', [], function() {

  var Config = ABINE_DNTME.config = {
    version: "7.8.2448",
    buildNum: "2448.bd36dd2c5e1744dfe5b0004f60aef666aab2fd96",
    appName: "Blur",
    environment: "production",
    tags: ['chromestore'],
    crypt: true,
    silent: false,
    payments: true,
    browser: null,  // defined in browser specific config file
    serverHost: "mm.abine.com",
    webHost: "www.abine.com",
    webAppHost: "dnt.abine.com",
    mappingServerHost: "mapping.abine.com",
    phoneServerHost: "phone.abine.com",
    licenseServerHost: "license.abine.com",
    emailServerHost: "emails.abine.com",
    pushNotificationUrl: "https://push.abine.com",
    disposableDomain: "opayq.com",
    paymentsServerHost: "payments.abine.com",
    logLevel: "ERROR",
    pfVersion: ".790 Chrome development",
    mmVersion: "0.0.1 development",
    features: 'accounts,wallet,abinestore,shareWebsocket',
    buildTag: 'chromestore',
    stripe_key: 'pk_live_J13Da0TuELwopBJeF4HlBwU9',
    overriddenKeys: {}
  };

  Config.protocol = (ABINE_DNTME.config.environment === "development")?'http://':'https://';

  Config.tellWebapp = function(doc, tagChanged) {
    if (doc.documentElement.hasAttribute("abine_webapp")) {
      doc.documentElement.setAttribute("dnt", Config.version);
      doc.documentElement.setAttribute("dnt-features", Config.features);
      doc.documentElement.setAttribute("dnt-tag", Config.tags[0]);
      doc.documentElement.setAttribute("dnt-build-tag", Config.buildTag);

      if (tagChanged) {
        var event = doc.createEvent("Event");
        event.initEvent("data:tag:changed", true, true);
        doc.dispatchEvent(event);
      }
    }
  };

  Config.updateTag = function(tag, doc) {
    if (Config.tags[0] != tag) {
      Config.tags[0] = tag;
      if (doc) {
        Config.tellWebapp(doc, true);
      }
    }
  };

  return Config;

});




ABINE_DNTME.define('abine/config', ['abine/baseConfig'], function(Config) {

  Config.browser = 'Chrome';

  // without this login in webapp fails randomly
  Config.tellWebapp(document);

  return Config;
});

ABINE_DNTME.require('abine/config');
ABINE_DNTME.define('abine/conflict_handler',['storage', 'persistence', 'abine/localStorage', 'abine/timer'],
  function(storage, persistence, localStorage, timer) {

    var hasMergedData = null;

    function openMaskmeDB() {
      return openDatabase('idme_data', '1.0', "Storage for the idme extension", 5 * 1024 * 1024);
    }

    function getScalarValue(db, query, column, callback) {
      db.transaction(function(tx) {
        tx.executeSql(query, null, function(tx, result) {
          if (result.rows.length > 0) {
            callback(result.rows.item(0)[column]);
          } else {
            callback(null);
          }
        }, function(){callback(null)});
      });
    }

    function checkForMergedAddon(callback) {
      callback = callback || function(){};
      var db = openMaskmeDB();

      getScalarValue(db, "SELECT * FROM sqlite_master WHERE type='table' AND name='Preferences'", 'name', function(name) {
        hasMergedData = (name == 'Preferences');
        ABINE_DNTME.merged_data_available = hasMergedData;
        if (hasMergedData) {
          getScalarValue(db, "select * from Preferences where key = 'regEmail' ", 'value', function(regEmail){
            getScalarValue(db, "select * from Preferences where key = 'entitledToPhone' ", 'value', function(entitledToPhone){
              getScalarValue(db, "select count(*) as cnt from DisposableEmails ", 'cnt', function(numDisposableEmails){
                callback(hasMergedData, !regEmail || numDisposableEmails > 1 || entitledToPhone=='true');
              });
            });
          });
        } else {
          callback(hasMergedData);
        }
      });
    }

    var todayDate = function() {
      var today = new Date();
      today.setHours(0); today.setMinutes(0); today.setSeconds(0); today.setMilliseconds(0); // js dates suck
      return today;
    }

    function installAuthTokenCheck(hasMergedData, callback) {
      callback = callback || function(){};

      if(!hasMergedData){
        callback();
        return;
      }

      var db = openMaskmeDB();

      getScalarValue(db, "select * from Preferences where key = 'installAuthToken' ", 'value', function(installAuthToken){
        if(installAuthToken && installAuthToken != ""){
          storage.Preference.findOrCreate({key:"installAuthToken"}, function(installAuthTokenPref){
            installAuthTokenPref.value = installAuthToken;
            installAuthTokenPref.modifiedAt = todayDate(); 
            persistence.flush(function(){
              callback();
            });
          });
        } else {
          callback();
        }
      });
    }

    return {
      installAuthTokenCheck: installAuthTokenCheck,
      checkForMergedAddon: checkForMergedAddon,

      checkForMaskme: function(callback) {
        if (typeof ABINE_DNTME.maskme_installed != 'undefined') {
          callback(ABINE_DNTME.maskme_installed);
          return;
        }

        function pingMaskme(retry) {
          if (retry <= 0) {
            ABINE_DNTME.maskme_installed = false;
            localStorage.setItem('maskme.installed', '0');
            callback(false);
            return;
          }

          var retryTimer = timer.setTimeout(function(){
            pingMaskme(retry-1);
          }, 200);

          _.each(ABINE_DNTME.MASKME_EXTENSION_ID, function(val, id) {
            chrome.runtime.sendMessage(id, {message: 'ping'},
              function(response) {
                if (response && response['installed']) {
                  timer.clearTimeout(retryTimer);
                  localStorage.setItem('maskme.installed', '1');
                  ABINE_DNTME.maskme_installed = true;
                  ABINE_DNTME.maskme_can_export = false;
                  chrome.runtime.sendMessage(id, {message: 'can_export'}, function(response) {
                    if (response === true) {
                      ABINE_DNTME.maskme_can_export = true;
                    }
                  });
                  callback(true);
                }
              }
            );
          });
        }

        if (localStorage.getItem('maskme.installed') === '0') {
          ABINE_DNTME.maskme_installed = false;
          callback(false);
        }

        // after fresh install, it takes up to 600ms to detect maskme.  we cannot do it earlier
        // as Maskme might take time to respond to our ping.
        pingMaskme(3);
      }
    };

});

// Content Messenger
// Object in content scripts that can communicate with the background
// Sends messages in the same format as Backbone events.
// Each message has a
// - name
// - payload

ABINE_DNTME.define('abine/contentMessenger',
    ['documentcloud/underscore', 'abine/core'], 
    function(_, abineCore) {

  // Create a listener for all requests
  var onRequestListener = function onRequestListener(request, sender) {
    if (request.eventName == 'FWD') {
      // from content script
      if (request.payload.to == ABINE_DNTME.context) {
        this.trigger(request.payload.eventName, request.payload.payload, request.payload.from);
      }
    } else {
      this.trigger(request.eventName, request.payload);
    }
  };
  
  var destroyed = function() {
    throw("Error. Attempt to call destroyed ContentMessenger.");
  };
  
  var ContentMessenger = abineCore.BaseClass.extend({
    
    initialize: function() {
      this.listener = _.bind(onRequestListener, this);
      chrome.extension.onRequest.addListener(this.listener);
    },
    
    sendToCS: function(to, eventName, payload) {
      if (chrome && chrome.extension) {
        chrome.extension.sendRequest({
          eventName: 'FWD',
          payload: {to: to, from: ABINE_DNTME.context, eventName: eventName, payload: typeof(payload) === 'undefined'?{}:payload}
        });
      }
    },

    // ### function on(eventName, callback, binding)
    // Starts listening to events from the background script
    // Defined on parent
    
    // ### function off(eventName, callback)
    // Stops listening to events from the background script
    // Defined on parent
    
    // ### function trigger(eventName, payload)
    // Triggers all of the handlers
    // Defined on parent
    
    // ### function send()
    // - eventName - name of the event
    // - payload - JSON serializable object
    // Sends a message to the background script
    // Returns the response if there is one
    send: function(eventName, payload) {
      this.trigger(eventName, payload);
      if (chrome && chrome.extension) {
        chrome.extension.sendRequest({
          eventName: eventName,
          payload: typeof(payload) === 'undefined'?{}:payload
        });
      }
    },
    
    // ### function destroy()
    // Destroy this messenger. Cleans up all listeners.
    destroy: function() {
      this.off();
      try {chrome.extension.onRequest.removeListener(this.listener);} catch(e){}
      this.on = this.off = this.trigger = destroyed;
    }
  });
  
  // Public
  return {
    
    // ### class ContentMessenger
    ContentMessenger: ContentMessenger
  
  }
  
});
  
ABINE_DNTME.define('abine/contextMenu',['documentcloud/underscore','abine/core', 'abine/contextHelper','storage','abine/securityManager'],
  function(_, abineCore, contextHelper, storage, securityManager){
    
  var contexts = ["editable"];
    
  var ContextMenuHelper = abineCore.BaseClass.extend({

    init: function(backgroundMessenger){
      chrome.contextMenus.onClicked.addListener(_.bind(this.onClick,this));

      backgroundMessenger.on('contentscript:contextmenu:refresh', this.refreshMenu, this);
      backgroundMessenger.on('background:contextmenu:refresh', this.refreshMenu, this);

      securityManager.on('background:securityManager:unlocked', _.bind(function(){
        this.previousDomain = null;
      }, this));
      securityManager.on('background:securityManager:locked', _.bind(function(){
        this.previousDomain = null;
      }, this));

      this.messenger = backgroundMessenger;
    },

    refreshMenu: function(options, tabId){
      if (this.previousDomain == options.domain) return;
      this.previousDomain = options.domain;
      if (!this.alreadyRefreshing) {
        this.domain = options.domain;
        this.alreadyRefreshing = true;

        var done = _.bind(function () {
          this.mappingMenu(_.bind(function () {
            this.alreadyRefreshing = false;
          }, this));
        }, this);

        chrome.contextMenus.removeAll(_.bind(function () {
          if (this.domain == "abine.com") {
            this.alreadyRefreshing = false;
            return;
          }

          if (this.domain.indexOf("chrome-extension://") >= 0 || ABINE_DNTME.maskme_installed) {
            this.alreadyRefreshing = false;
            return;
          }

          storage.Preference.findBy('key', 'hasRegistered', _.bind(function (pref) {
            if (!pref || pref.isFalse()) {
              storage.Preference.batchFindBy([
                {by: 'key', value: 'mapping:mapper'},
                {by: 'key', value: 'autofill:mapper'}
              ], _.bind(function (prefs) {
                if (prefs['mapping:mapper'] || prefs['autofill:mapper']) {
                  chrome.contextMenus.create({
                    title: "Blur",
                    contexts: contexts,
                    id: "abine_dntme_context_root"
                  }, done);
                } else {
                  this.alreadyRefreshing = false;
                }
              }, this));
              return;
            }
            if (securityManager.isLocked()) {
              chrome.contextMenus.create({
                title: "Blur",
                contexts: contexts,
                id: "abine_dntme_context_root"
              }, _.bind(function () {
                chrome.contextMenus.create({
                  title: "locked, click to unlock",
                  contexts: contexts,
                  id: "abine_dntme_unlock",
                  parentId: "abine_dntme_context_root"
                }, done);
              }, this));
            } else {
              this.createStaticMenu(options.domain, _.bind(function () {
                this.createDynamicMenu(options.domain, done);
              }, this));
            }
          }, this));
        }, this));
      }
    },

    onClick: function(e,tab){
      contextHelper.onClick(this.messenger, e.menuItemId, tab.id, this.domain);
    },

    createDynamicMenu: function(domain, callback){
      _.async.series([
        _.bind(function(cb) {
          chrome.contextMenus.create({
            title:"Accounts for " + domain,
            contexts:contexts,
            id:"abine_dntme_logins",
            parentId:"abine_dntme_context_root"
          }, _.bind(function(){
            storage.Account.listCached('domain', domain, _.bind(function(accounts){
              if(accounts.length == 0){
                chrome.contextMenus.create({
                  title:"You have no accounts here",
                  contexts:contexts,
                  id:"abine_dntme_no_accounts",
                  parentId:"abine_dntme_logins"
                }, cb);
              } else {
                _.async.forEachSeries(accounts, _.bind(this.addLoginItem, this), cb);
              }
            }, this));
          }, this))
        }, this),
        _.bind(function(cb){
          storage.Preference.findBy('key', 'masked_card_auth', _.bind(function(pref) {
            if (!pref || pref.value != 'biometric') {
              storage.RealCard.index(null, _.bind(function (cards) {
                if (cards.length == 0) {
                  cb();
                } else {
                  _.async.forEachSeries(cards, _.bind(this.addCardItem, this), cb);
                }
              }, this));
            } else {
              cb();
            }
          }, this));

        }, this),
        _.bind(function(cb){
          storage.Address.index(null, _.bind(function(addresses){
            if (addresses.length == 0) {
              cb();
            } else {
              chrome.contextMenus.create({
                title:"Addresses",
                contexts:contexts,
                id:"abine_dntme_address_parent",
                parentId:"abine_dntme_context_root"
              }, _.bind(function(){
                _.async.forEachSeries(addresses, _.bind(this.addAddressItem, this), cb);
              }, this));
            }
          }, this));

        }, this),
        callback
      ]);
    },

    addLoginItem: function(account, accountCB){
      chrome.contextMenus.create({
        title: account.email || account.username || (account.password ? "- Only Password -" : "- Not Set -"),
        contexts:contexts,
        id:"abine_dntme_accountfield_parent_"+account.id,
        parentId:"abine_dntme_logins"
      }, function(){
        if (account.email) {
          chrome.contextMenus.create({
            title: "Fill email",
            contexts:contexts,
            id:"abine_dntme_accountfield_email_"+account.id,
            parentId:"abine_dntme_accountfield_parent_"+account.id
          }, function(){});
        }
        if (account.username) {
          chrome.contextMenus.create({
            title: "Fill username",
            contexts:contexts,
            id:"abine_dntme_accountfield_username_"+account.id,
            parentId:"abine_dntme_accountfield_parent_"+account.id
          }, function(){});
        }
        if (account.password) {
          chrome.contextMenus.create({
            title: "Fill password",
            contexts:contexts,
            id:"abine_dntme_accountfield_password_"+account.id,
            parentId:"abine_dntme_accountfield_parent_"+account.id
          }, function(){});
        }
        accountCB();
      });
    },

    addCardItem: function(card, cardCB){
      chrome.contextMenus.create({
        title: "Card ending in x"+(card.number.substr(card.number.length-4)),
        contexts:contexts,
        id:"abine_dntme_fillcard_parent_"+card.id,
        parentId:"abine_dntme_card_parent"
      }, function(){
        if (card.number) {
          chrome.contextMenus.create({
            title: "Fill card number",
            contexts:contexts,
            id:"abine_dntme_fillcard_number_"+card.id,
            parentId:"abine_dntme_fillcard_parent_"+card.id
          }, function(){});
        }
        if (card.first_name || card.last_name) {
          chrome.contextMenus.create({
            title: "Fill name - "+card.getFullName(),
            contexts:contexts,
            id:"abine_dntme_fillcard_name_"+card.id,
            parentId:"abine_dntme_fillcard_parent_"+card.id
          }, function(){});
        }
        if (card.cvc) {
          chrome.contextMenus.create({
            title: "Fill CVC/CVV",
            contexts:contexts,
            id:"abine_dntme_fillcard_cvc_"+card.id,
            parentId:"abine_dntme_fillcard_parent_"+card.id
          }, function(){});
        }
        chrome.contextMenus.create({
          title: "Fill expiry month - mm",
          contexts:contexts,
          id:"abine_dntme_fillcard_month_"+card.id,
          parentId:"abine_dntme_fillcard_parent_"+card.id
        }, function(){});
        chrome.contextMenus.create({
          title: "Fill expiry year - yyyy",
          contexts:contexts,
          id:"abine_dntme_fillcard_year_"+card.id,
          parentId:"abine_dntme_fillcard_parent_"+card.id
        }, function(){});
        chrome.contextMenus.create({
          title: "Fill expiry year - yy",
          contexts:contexts,
          id:"abine_dntme_fillcard_yy_"+card.id,
          parentId:"abine_dntme_fillcard_parent_"+card.id
        }, function(){});
        chrome.contextMenus.create({
          title: "Fill expiry - mm/yyyy",
          contexts:contexts,
          id:"abine_dntme_fillcard_mmyyyy_"+card.id,
          parentId:"abine_dntme_fillcard_parent_"+card.id
        }, function(){});
        chrome.contextMenus.create({
          title: "Fill expiry - mm/yy",
          contexts:contexts,
          id:"abine_dntme_fillcard_mmyy_"+card.id,
          parentId:"abine_dntme_fillcard_parent_"+card.id
        }, function(){});
        cardCB();
      });
    },

    addAddressItem: function(address, addressCB){
      chrome.contextMenus.create({
        title: address.label,
        contexts:contexts,
        id:"abine_dntme_address_parent_"+address.id,
        parentId:"abine_dntme_address_parent"
      }, function(){
        if (address.address1) {
          chrome.contextMenus.create({
            title: 'Line 1: '+address.address1,
            contexts:contexts,
            id:"abine_dntme_address_line1_"+address.id,
            parentId:"abine_dntme_address_parent_"+address.id
          }, function(){});
        }
        if (address.address2) {
          chrome.contextMenus.create({
            title: 'Line 2: '+address.address2,
            contexts:contexts,
            id:"abine_dntme_address_line2_"+address.id,
            parentId:"abine_dntme_address_parent_"+address.id
          }, function(){});
        }
        if (address.city) {
          chrome.contextMenus.create({
            title: 'City: '+address.city,
            contexts:contexts,
            id:"abine_dntme_address_city_"+address.id,
            parentId:"abine_dntme_address_parent_"+address.id
          }, function(){});
        }
        if (address.state) {
          chrome.contextMenus.create({
            title: 'State: '+address.state,
            contexts:contexts,
            id:"abine_dntme_address_state_"+address.id,
            parentId:"abine_dntme_address_parent_"+address.id
          }, function(){});
        }
        if (address.country) {
          chrome.contextMenus.create({
            title: 'Country: '+address.country,
            contexts:contexts,
            id:"abine_dntme_address_country_"+address.id,
            parentId:"abine_dntme_address_parent_"+address.id
          }, function(){});
        }
        if (address.zip) {
          chrome.contextMenus.create({
            title: 'Zip: '+address.zip,
            contexts:contexts,
            id:"abine_dntme_address_zip_"+address.id,
            parentId:"abine_dntme_address_parent_"+address.id
          }, function(){});
        }
        addressCB();
      });
    },

    mappingMenu: function(cb) {
      storage.Preference.batchFindBy([
        {by: 'key', value: 'mapping:mapper'},
        {by: 'key', value: 'mapping:reviewer'},
        {by: 'key', value: 'autofill:mapper'}
      ], function(prefs){
        if (prefs['mapping:mapper'] || prefs['mapping:reviewer']) {
          chrome.contextMenus.create({
            title: "Map this form",
            contexts:contexts,
            parentId: "abine_dntme_context_root",
            id:"abine_dntme_map_form"
          }, cb);
        } else if (prefs['autofill:mapper']) {
          chrome.contextMenus.create({
            title: "Autofill this form",
            contexts:contexts,
            parentId: "abine_dntme_context_root",
            id:"abine_dntme_autofill_form"
          }, cb);
        } else {
          chrome.contextMenus.create({
            title: "Ignore this field",
            contexts:contexts,
            parentId: "abine_dntme_context_root",
            id:"abine_dntme_ignore"
          }, cb);
        }
      });
    },

    addPhoneMenu: function (phone, type) {
      var parentId = "abine_dntme_phone_" + type + "_parent";
      var countryCode = phone.country_code;
      var number = phone.number;
      chrome.contextMenus.create({
        title: "Full: " + number,
        contexts: contexts,
        id: "abine_dntme_phone_" + type + "_full",
        parentId: parentId
      }, function () {
      });
      if (countryCode == 1 && number.length == 10) {
        var areaCode = number.substr(0, 3);
        var exchangeCode = number.substr(3, 3);
        var localCode = number.substr(6, 4);
        var last7 = number.substr(3, 7);
        chrome.contextMenus.create({
          title: 'Last 7: ' + last7,
          contexts: contexts,
          id: "abine_dntme_phone_" + type + "_last7",
          parentId: parentId
        }, function () {
        });
        chrome.contextMenus.create({
          title: 'Area: ' + areaCode,
          contexts: contexts,
          id: "abine_dntme_phone_" + type + "_area",
          parentId: parentId
        }, function () {
        });
        chrome.contextMenus.create({
          title: 'Exchange: ' + exchangeCode,
          contexts: contexts,
          id: "abine_dntme_phone_" + type + "_exchange",
          parentId: parentId
        }, function () {
        });
        chrome.contextMenus.create({
          title: 'Local: ' + localCode,
          contexts: contexts,
          id: "abine_dntme_phone_" + type + "_local",
          parentId: parentId
        }, function () {
        });
      }
    },

    createStaticMenu: function(domain, callback){
      var self = this;
      _.async.series([
        function(cb){
          chrome.contextMenus.create({
            title: "Blur",
            contexts:contexts,
            id:"abine_dntme_context_root"
          },cb);
        },
        function(cb){
          chrome.contextMenus.create({
            title: "Email",
            contexts:contexts,
            parentId: "abine_dntme_context_root",
            id:"abine_dntme_email_parent"
          },cb);
        },
        function(cb){
          chrome.contextMenus.create({
            title: "Mask My Email",
            contexts:contexts,
            parentId: "abine_dntme_email_parent",
            id: "abine_dntme_email_mask"
          },cb);
        },
        function(cb){
          chrome.contextMenus.create({
            title: "Use My Email",
            contexts:contexts,
            parentId: "abine_dntme_email_parent",
            id: "abine_dntme_email_disclose"
          },cb);
        },

        function(cb){
          chrome.contextMenus.create({
            title: "Password",
            contexts:contexts,
            parentId: "abine_dntme_context_root",
            id:"abine_dntme_password_parent"
          },cb);
        },

        function(cb){
          chrome.contextMenus.create({
            title: "Generate strong password",
            contexts:contexts,
            parentId: "abine_dntme_password_parent",
            id: "abine_dntme_password_mask"
          },cb);
        },

        function(cb){
          chrome.contextMenus.create({
            title: "Phone",
            contexts:contexts,
            parentId: "abine_dntme_context_root",
            id:"abine_dntme_phone_parent"
          },cb);
        },

        function(cb){
          storage.DisposablePhone.index(null, function(phones){
            if(phones.length > 0){
              chrome.contextMenus.create({
                title: "Mask my Phone",
                contexts:contexts,
                parentId: "abine_dntme_phone_parent",
                id: "abine_dntme_phone_mask_parent"
              }, function() {
                self.addPhoneMenu(phones[0], "mask");
                cb();
              });
            } else {
              cb();
            }
          });
        },

        function(cb){
          storage.ShieldedPhone.index(null, function(phones){
            if(phones.length > 0){
              chrome.contextMenus.create({
                title: "Disclose my phone",
                contexts:contexts,
                parentId: "abine_dntme_phone_parent",
                id: "abine_dntme_phone_disclose_parent"
              }, function(){
                self.addPhoneMenu(phones[0], "disclose");
                cb();
              });
            } else {
              chrome.contextMenus.create({
                title: "Set up your phone",
                contexts:contexts,
                parentId: "abine_dntme_phone_parent",
                id: "abine_dntme_setup_phone"
              },cb);
            }
          });

        },

        function(cb){
          chrome.contextMenus.create({
            title: "Credit Card",
            contexts:contexts,
            parentId: "abine_dntme_context_root",
            id:"abine_dntme_card_parent"
          },function(){
            storage.Preference.findBy('key','hasEnabledPayments', function(enabledPref){
              if(enabledPref && enabledPref.isTrue()){
                chrome.contextMenus.create({
                  title: "Mask my Credit Card",
                  contexts:contexts,
                  parentId: "abine_dntme_card_parent",
                  id: "abine_dntme_card_mask"
                },cb);
              } else {
                chrome.contextMenus.create({
                  title: "Activate Masked Cards",
                  contexts:contexts,
                  parentId: "abine_dntme_card_parent",
                  id: "abine_dntme_activate_card"
                },cb);
              }
            });
          });
        }
      ], function(er){
        callback();
      });
    }
  });

  return {
    contextMenuHelper: new ContextMenuHelper()
  };

});
ABINE_DNTME.define('abine/dntmeUpgrade',['storage', 'persistence', 'abine/localStorage', 'abine/conflict_handler'], function(storage, persistence, localStorage, conflictHandler) {

  var HAS_MASKME_DATA = null;

  var db = null;
  function openDB() {
    var dbSize = 5 * 1024 * 1024; // 5MB
    db = openDatabase('abine2', '1.1', 'abine2', dbSize);
  }

  function getTrackerSettings(data, callback) {
    db.transaction(function(tx) {
      tx.executeSql('SELECT * FROM trackers', [],
        function(tx, results) {
          // success handler
          data.trackerMap = {};
          for (var i = 0; i < results.rows.length; i++) {
            if (!data.trackerMap[results.rows.item(i).domain])
              data.trackerMap[results.rows.item(i).domain] = {};

            data.trackerMap[results.rows.item(i).domain][results.rows.item(i).name] = results.rows.item(i).blocked;
          }
          callback();
        },
        function () {
          // error handler
          callback();
        }
      );
    });
  }

  function getTotalBlocked(data, callback) {
    db.transaction(function(tx) {
      tx.executeSql('SELECT * from blockcount', [],
        function(tx, results) {
          // success handler
          data.totals = {};
          for ( var i = 0; i < results.rows.length; i++) {
            var name = results.rows.item(i).name;
            var value = results.rows.item(i).value;
            if (name != 'totalOptout') {
              if (name == 'totalb') name = 'total';
              data.totals[name] = value;
            }
          }
          callback();
        },
        function() {
          // error handler
          callback();
        }
      );
    });
  }

  function getGenericSettings(data, callback) {
    db.transaction(function(tx) {
      tx.executeSql('SELECT * from settings', [],
        function(tx, results) {
          // success handler
          data.settings = {};
          for ( var i = 0; i < results.rows.length; i++) {
            var name = results.rows.item(i).name;
            if (name.match(/rules|converted|reason/)) {
              continue;
            }
            var value = results.rows.item(i).value;
            data.settings[name.toLowerCase()] = value;
          }
          if ('dntp.useglobalallow' in data.settings) {
            data.settings['dntp.useglobalallow'] = data.settings['dntp.useglobalallow'] != '';
          }
          if ('dntp.showmedals' in data.settings) {
            data.settings['dntp.showmedals'] = data.settings['dntp.showmedals'] != '';
          }
          callback();
        },
        function() {
          // error handler
          callback();
        }
      );
    });
  }

  function getOldLocalStorage(data, callback) {
    if (localStorage.getItem('global.blocked')) {
      if (!data.settings) data.settings = {};
      data.settings.defaultBlocked = localStorage.getItem('global.blocked') != '0';
    }
    if (localStorage.getItem('origin')) {
      if (!data.settings) data.settings = {};
      data.settings.origin = localStorage.getItem('origin');
    }
    data.badTrackers = {};
    data.dailyTotals = {};
    if (localStorage.getItem('badTrackerTotals')) {
      data.badTrackers['totals'] = JSON.parse(localStorage.getItem('badTrackerTotals'));
    }
    var thisYear = (new Date()).getFullYear();
    for (var year=2011;year<=thisYear;year++) {
      if (localStorage.getItem('dailyTotals'+year)) {
        data.dailyTotals[year] = JSON.parse(localStorage.getItem('dailyTotals'+year));
      }
      for (var tracker in data.badTrackers['totals']) {
        var trackerYear = tracker + '.' + year;
        if (localStorage.getItem('badTrackerDaily.'+trackerYear)) {
          if (!('daily' in data.badTrackers)) {
            data.badTrackers['daily'] = {};
          }
          data.badTrackers['daily'][trackerYear] = JSON.parse(localStorage.getItem('badTrackerDaily.'+trackerYear));
        }
      }
    }
    callback();
  }

  function openMaskmeDB() {
    return openDatabase('idme_data', '1.0', "Storage for the idme extension", 5 * 1024 * 1024);
  }

  function getMaskmeData(callback) {
    if (HAS_MASKME_DATA === null) {
        conflictHandler.checkForMergedAddon(function(hasMergedData, requiresMigration){
          HAS_MASKME_DATA = hasMergedData;
          getMaskmeData(callback)
        });
        return;
    }

    if (HAS_MASKME_DATA === false) {
        callback(null);
        return;
    }

    var maskmeTables = "AccountField,Accounts,DisposableCards,DisposableEmails,Preferences,ShieldedEmails,Sites".split(",");
    var maskmeData = {};

    function getTableSchema(tx, tableIndex) {
        if (tableIndex >= maskmeTables.length) {
            callback(maskmeData);
            return;
        }
        var tableName = maskmeTables[tableIndex];
        tx.executeSql("SELECT * FROM sqlite_master WHERE type='table' AND name='"+tableName+"'", null, function(tx, results) {
            if (results.rows.length > 0) {
                maskmeData[tableName] = {schema: results.rows.item(0).sql};
                getTableData(tx, tableIndex);
            } else {
                getTableSchema(tx, tableIndex+1);
            }
        });
    }

    function getTableData(tx, tableIndex) {
        var tableName = maskmeTables[tableIndex];
        tx.executeSql("SELECT * FROM "+tableName, null, function(tx, results) {
            var rows = [];
            var len = results.rows.length;
            for (var i=0;i<len;i++) {
                rows.push(results.rows.item(i))
            }
            maskmeData[tableName]['rows'] = rows;
            getTableSchema(tx, tableIndex+1);
        });
    }
    
    openMaskmeDB().transaction(function(tx){
        getTableSchema(tx, 0);
    });
  }

  function exportFromMaskme(callback) {
    _.each(ABINE_DNTME.MASKME_EXTENSION_ID, function(val, id) {
      chrome.runtime.sendMessage(id, {message: 'export_to_dntme'},
        function(response) {
          if (response) {
            callback(response);
          }
        }
      );
    });
  }


  return {
    getMaskmeData: getMaskmeData,
    exportFromMaskme: exportFromMaskme,
    getOldData: function(callback) {
      if (localStorage.getItem('upgradeDone')) {
        callback(null);
        return;
      }
      localStorage.setItem('upgradeDone', true);
      openDB();
      var oldData = {};
      getTrackerSettings(oldData, function(){
        getTotalBlocked(oldData, function(){
          getGenericSettings(oldData, function(){
            getOldLocalStorage(oldData, function(){
              callback(oldData);
            });
          })
        });
      });
    }
  };
});
ABINE_DNTME.define('abine/compressedLocalStorage', [], function(){
  function wrap(localStorage) {

    // Copyright (c) 2013 Pieroxy <pieroxy@pieroxy.net>
// This work is free. You can redistribute it and/or modify it
// under the terms of the WTFPL, Version 2
// For more information see LICENSE.txt or http://www.wtfpl.net/
//
// For more information, the home page:
// http://pieroxy.net/blog/pages/lz-string/testing.html
//
// LZ-based compression algorithm, version 1.4.4
var LZString = (function() {

// private property
var f = String.fromCharCode;
var keyStrBase64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
var keyStrUriSafe = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+-$";
var baseReverseDic = {};

function getBaseValue(alphabet, character) {
  if (!baseReverseDic[alphabet]) {
    baseReverseDic[alphabet] = {};
    for (var i=0 ; i<alphabet.length ; i++) {
      baseReverseDic[alphabet][alphabet.charAt(i)] = i;
    }
  }
  return baseReverseDic[alphabet][character];
}

var LZString = {
  compressToBase64 : function (input) {
    if (input == null) return "";
    var res = LZString._compress(input, 6, function(a){return keyStrBase64.charAt(a);});
    switch (res.length % 4) { // To produce valid Base64
    default: // When could this happen ?
    case 0 : return res;
    case 1 : return res+"===";
    case 2 : return res+"==";
    case 3 : return res+"=";
    }
  },

  decompressFromBase64 : function (input) {
    if (input == null) return "";
    if (input == "") return null;
    return LZString._decompress(input.length, 32, function(index) { return getBaseValue(keyStrBase64, input.charAt(index)); });
  },

  compressToUTF16 : function (input) {
    if (input == null) return "";
    return LZString._compress(input, 15, function(a){return f(a+32);}) + " ";
  },

  decompressFromUTF16: function (compressed) {
    if (compressed == null) return "";
    if (compressed == "") return null;
    return LZString._decompress(compressed.length, 16384, function(index) { return compressed.charCodeAt(index) - 32; });
  },

  //compress into uint8array (UCS-2 big endian format)
  compressToUint8Array: function (uncompressed) {
    var compressed = LZString.compress(uncompressed);
    var buf=new Uint8Array(compressed.length*2); // 2 bytes per character

    for (var i=0, TotalLen=compressed.length; i<TotalLen; i++) {
      var current_value = compressed.charCodeAt(i);
      buf[i*2] = current_value >>> 8;
      buf[i*2+1] = current_value % 256;
    }
    return buf;
  },

  //decompress from uint8array (UCS-2 big endian format)
  decompressFromUint8Array:function (compressed) {
    if (compressed===null || compressed===undefined){
        return LZString.decompress(compressed);
    } else {
        var buf=new Array(compressed.length/2); // 2 bytes per character
        for (var i=0, TotalLen=buf.length; i<TotalLen; i++) {
          buf[i]=compressed[i*2]*256+compressed[i*2+1];
        }

        var result = [];
        buf.forEach(function (c) {
          result.push(f(c));
        });
        return LZString.decompress(result.join(''));

    }

  },


  //compress into a string that is already URI encoded
  compressToEncodedURIComponent: function (input) {
    if (input == null) return "";
    return LZString._compress(input, 6, function(a){return keyStrUriSafe.charAt(a);});
  },

  //decompress from an output of compressToEncodedURIComponent
  decompressFromEncodedURIComponent:function (input) {
    if (input == null) return "";
    if (input == "") return null;
    input = input.replace(/ /g, "+");
    return LZString._decompress(input.length, 32, function(index) { return getBaseValue(keyStrUriSafe, input.charAt(index)); });
  },

  compress: function (uncompressed) {
    return LZString._compress(uncompressed, 16, function(a){return f(a);});
  },
  _compress: function (uncompressed, bitsPerChar, getCharFromInt) {
    if (uncompressed == null) return "";
    var i, value,
        context_dictionary= {},
        context_dictionaryToCreate= {},
        context_c="",
        context_wc="",
        context_w="",
        context_enlargeIn= 2, // Compensate for the first entry which should not count
        context_dictSize= 3,
        context_numBits= 2,
        context_data=[],
        context_data_val=0,
        context_data_position=0,
        ii;

    for (ii = 0; ii < uncompressed.length; ii += 1) {
      context_c = uncompressed.charAt(ii);
      if (!Object.prototype.hasOwnProperty.call(context_dictionary,context_c)) {
        context_dictionary[context_c] = context_dictSize++;
        context_dictionaryToCreate[context_c] = true;
      }

      context_wc = context_w + context_c;
      if (Object.prototype.hasOwnProperty.call(context_dictionary,context_wc)) {
        context_w = context_wc;
      } else {
        if (Object.prototype.hasOwnProperty.call(context_dictionaryToCreate,context_w)) {
          if (context_w.charCodeAt(0)<256) {
            for (i=0 ; i<context_numBits ; i++) {
              context_data_val = (context_data_val << 1);
              if (context_data_position == bitsPerChar-1) {
                context_data_position = 0;
                context_data.push(getCharFromInt(context_data_val));
                context_data_val = 0;
              } else {
                context_data_position++;
              }
            }
            value = context_w.charCodeAt(0);
            for (i=0 ; i<8 ; i++) {
              context_data_val = (context_data_val << 1) | (value&1);
              if (context_data_position == bitsPerChar-1) {
                context_data_position = 0;
                context_data.push(getCharFromInt(context_data_val));
                context_data_val = 0;
              } else {
                context_data_position++;
              }
              value = value >> 1;
            }
          } else {
            value = 1;
            for (i=0 ; i<context_numBits ; i++) {
              context_data_val = (context_data_val << 1) | value;
              if (context_data_position ==bitsPerChar-1) {
                context_data_position = 0;
                context_data.push(getCharFromInt(context_data_val));
                context_data_val = 0;
              } else {
                context_data_position++;
              }
              value = 0;
            }
            value = context_w.charCodeAt(0);
            for (i=0 ; i<16 ; i++) {
              context_data_val = (context_data_val << 1) | (value&1);
              if (context_data_position == bitsPerChar-1) {
                context_data_position = 0;
                context_data.push(getCharFromInt(context_data_val));
                context_data_val = 0;
              } else {
                context_data_position++;
              }
              value = value >> 1;
            }
          }
          context_enlargeIn--;
          if (context_enlargeIn == 0) {
            context_enlargeIn = Math.pow(2, context_numBits);
            context_numBits++;
          }
          delete context_dictionaryToCreate[context_w];
        } else {
          value = context_dictionary[context_w];
          for (i=0 ; i<context_numBits ; i++) {
            context_data_val = (context_data_val << 1) | (value&1);
            if (context_data_position == bitsPerChar-1) {
              context_data_position = 0;
              context_data.push(getCharFromInt(context_data_val));
              context_data_val = 0;
            } else {
              context_data_position++;
            }
            value = value >> 1;
          }


        }
        context_enlargeIn--;
        if (context_enlargeIn == 0) {
          context_enlargeIn = Math.pow(2, context_numBits);
          context_numBits++;
        }
        // Add wc to the dictionary.
        context_dictionary[context_wc] = context_dictSize++;
        context_w = String(context_c);
      }
    }

    // Output the code for w.
    if (context_w !== "") {
      if (Object.prototype.hasOwnProperty.call(context_dictionaryToCreate,context_w)) {
        if (context_w.charCodeAt(0)<256) {
          for (i=0 ; i<context_numBits ; i++) {
            context_data_val = (context_data_val << 1);
            if (context_data_position == bitsPerChar-1) {
              context_data_position = 0;
              context_data.push(getCharFromInt(context_data_val));
              context_data_val = 0;
            } else {
              context_data_position++;
            }
          }
          value = context_w.charCodeAt(0);
          for (i=0 ; i<8 ; i++) {
            context_data_val = (context_data_val << 1) | (value&1);
            if (context_data_position == bitsPerChar-1) {
              context_data_position = 0;
              context_data.push(getCharFromInt(context_data_val));
              context_data_val = 0;
            } else {
              context_data_position++;
            }
            value = value >> 1;
          }
        } else {
          value = 1;
          for (i=0 ; i<context_numBits ; i++) {
            context_data_val = (context_data_val << 1) | value;
            if (context_data_position == bitsPerChar-1) {
              context_data_position = 0;
              context_data.push(getCharFromInt(context_data_val));
              context_data_val = 0;
            } else {
              context_data_position++;
            }
            value = 0;
          }
          value = context_w.charCodeAt(0);
          for (i=0 ; i<16 ; i++) {
            context_data_val = (context_data_val << 1) | (value&1);
            if (context_data_position == bitsPerChar-1) {
              context_data_position = 0;
              context_data.push(getCharFromInt(context_data_val));
              context_data_val = 0;
            } else {
              context_data_position++;
            }
            value = value >> 1;
          }
        }
        context_enlargeIn--;
        if (context_enlargeIn == 0) {
          context_enlargeIn = Math.pow(2, context_numBits);
          context_numBits++;
        }
        delete context_dictionaryToCreate[context_w];
      } else {
        value = context_dictionary[context_w];
        for (i=0 ; i<context_numBits ; i++) {
          context_data_val = (context_data_val << 1) | (value&1);
          if (context_data_position == bitsPerChar-1) {
            context_data_position = 0;
            context_data.push(getCharFromInt(context_data_val));
            context_data_val = 0;
          } else {
            context_data_position++;
          }
          value = value >> 1;
        }


      }
      context_enlargeIn--;
      if (context_enlargeIn == 0) {
        context_enlargeIn = Math.pow(2, context_numBits);
        context_numBits++;
      }
    }

    // Mark the end of the stream
    value = 2;
    for (i=0 ; i<context_numBits ; i++) {
      context_data_val = (context_data_val << 1) | (value&1);
      if (context_data_position == bitsPerChar-1) {
        context_data_position = 0;
        context_data.push(getCharFromInt(context_data_val));
        context_data_val = 0;
      } else {
        context_data_position++;
      }
      value = value >> 1;
    }

    // Flush the last char
    while (true) {
      context_data_val = (context_data_val << 1);
      if (context_data_position == bitsPerChar-1) {
        context_data.push(getCharFromInt(context_data_val));
        break;
      }
      else context_data_position++;
    }
    return context_data.join('');
  },

  decompress: function (compressed) {
    if (compressed == null) return "";
    if (compressed == "") return null;
    return LZString._decompress(compressed.length, 32768, function(index) { return compressed.charCodeAt(index); });
  },

  _decompress: function (length, resetValue, getNextValue) {
    var dictionary = [],
        next,
        enlargeIn = 4,
        dictSize = 4,
        numBits = 3,
        entry = "",
        result = [],
        i,
        w,
        bits, resb, maxpower, power,
        c,
        data = {val:getNextValue(0), position:resetValue, index:1};

    for (i = 0; i < 3; i += 1) {
      dictionary[i] = i;
    }

    bits = 0;
    maxpower = Math.pow(2,2);
    power=1;
    while (power!=maxpower) {
      resb = data.val & data.position;
      data.position >>= 1;
      if (data.position == 0) {
        data.position = resetValue;
        data.val = getNextValue(data.index++);
      }
      bits |= (resb>0 ? 1 : 0) * power;
      power <<= 1;
    }

    switch (next = bits) {
      case 0:
          bits = 0;
          maxpower = Math.pow(2,8);
          power=1;
          while (power!=maxpower) {
            resb = data.val & data.position;
            data.position >>= 1;
            if (data.position == 0) {
              data.position = resetValue;
              data.val = getNextValue(data.index++);
            }
            bits |= (resb>0 ? 1 : 0) * power;
            power <<= 1;
          }
        c = f(bits);
        break;
      case 1:
          bits = 0;
          maxpower = Math.pow(2,16);
          power=1;
          while (power!=maxpower) {
            resb = data.val & data.position;
            data.position >>= 1;
            if (data.position == 0) {
              data.position = resetValue;
              data.val = getNextValue(data.index++);
            }
            bits |= (resb>0 ? 1 : 0) * power;
            power <<= 1;
          }
        c = f(bits);
        break;
      case 2:
        return "";
    }
    dictionary[3] = c;
    w = c;
    result.push(c);
    while (true) {
      if (data.index > length) {
        return "";
      }

      bits = 0;
      maxpower = Math.pow(2,numBits);
      power=1;
      while (power!=maxpower) {
        resb = data.val & data.position;
        data.position >>= 1;
        if (data.position == 0) {
          data.position = resetValue;
          data.val = getNextValue(data.index++);
        }
        bits |= (resb>0 ? 1 : 0) * power;
        power <<= 1;
      }

      switch (c = bits) {
        case 0:
          bits = 0;
          maxpower = Math.pow(2,8);
          power=1;
          while (power!=maxpower) {
            resb = data.val & data.position;
            data.position >>= 1;
            if (data.position == 0) {
              data.position = resetValue;
              data.val = getNextValue(data.index++);
            }
            bits |= (resb>0 ? 1 : 0) * power;
            power <<= 1;
          }

          dictionary[dictSize++] = f(bits);
          c = dictSize-1;
          enlargeIn--;
          break;
        case 1:
          bits = 0;
          maxpower = Math.pow(2,16);
          power=1;
          while (power!=maxpower) {
            resb = data.val & data.position;
            data.position >>= 1;
            if (data.position == 0) {
              data.position = resetValue;
              data.val = getNextValue(data.index++);
            }
            bits |= (resb>0 ? 1 : 0) * power;
            power <<= 1;
          }
          dictionary[dictSize++] = f(bits);
          c = dictSize-1;
          enlargeIn--;
          break;
        case 2:
          return result.join('');
      }

      if (enlargeIn == 0) {
        enlargeIn = Math.pow(2, numBits);
        numBits++;
      }

      if (dictionary[c]) {
        entry = dictionary[c];
      } else {
        if (c === dictSize) {
          entry = w + w.charAt(0);
        } else {
          return null;
        }
      }
      result.push(entry);

      // Add w+entry[0] to the dictionary.
      dictionary[dictSize++] = w + entry.charAt(0);
      enlargeIn--;

      w = entry;

      if (enlargeIn == 0) {
        enlargeIn = Math.pow(2, numBits);
        numBits++;
      }

    }
  }
};
  return LZString;
})();


    var ENABLED = true;

    if (!ENABLED) {
      return localStorage;
    }

    var wrappedStorage = {
      getItem: function() {
        var value = localStorage.getItem.apply(localStorage, arguments);
        if (value && value.indexOf('LZ_') === 0) {
          value = LZString.decompress(value.substr(3));
        }
        return value;
      },
      setItem: function(name, value) {
        if (value && name && name.match(/mapping-|loginurl-|-to-push|form-rules/i) && value.length > 500) {
          value = 'LZ_'+LZString.compress(value);
        }
        return localStorage.setItem(name, value);
      },
      removeItem: function(name) {
        return localStorage.removeItem(name);
      },
      clear: function() {
        localStorage.clear();
      }
    };

    if (!wrappedStorage.getItem('compressed')) {
      var start = Date.now();
      LOG_FINEST && console.log("Compressing old mapping data");
      wrappedStorage.setItem('compressed', 1);
      var buckets = wrappedStorage.getItem('mapping-buckets', null);
      if (buckets) {
        buckets = JSON.parse(buckets);
        for (var i=0;i<buckets.length;i++) {
          var mappings = wrappedStorage.getItem('mapping-'+buckets[i], null);
          if (mappings) {
            wrappedStorage.setItem('mapping-'+buckets[i], mappings);
          }
        }
      }
      LOG_FINEST && console.log("Time taken for compressing mapping data "+(Date.now()-start)+" ms");
    }

    return wrappedStorage;
  }
  return {
    wrap: wrap
  };
});


ABINE_DNTME.define('abine/localStorage', ['abine/compressedLocalStorage'], function(wrapper){
  return wrapper.wrap(window.localStorage);
});
ABINE_DNTME.define("abine/pay_button", [], function() {
  return {
    show: function(tabId, host) {
      // disabled paybutton
      if (host && host.match(/^(1800flowers.com|bestbuy.com|walmart.com|kohls.com|target.com)$/)) {
        chrome.tabs.executeScript(tabId, {
          file: "/pages/pay_button.js",
          allFrames: false,
          runAt: 'document_end'
        });
      }
    }
  };
});
ABINE_DNTME.define('abine/tabs',['abine/core', 'abine/url'], function(abineCore, abineUrl){

  var TabHelper = abineCore.BaseClass.extend({
    init: function(backgroundMessenger){
      chrome.tabs.onActivated.addListener(function(activeInfo){
        if (activeInfo.tabId == -1) return;
        chrome.tabs.get(activeInfo.tabId, function(tab){
          if(tab && tab.url && (tab.url.match("https?://") || tab.url.match("chrome-extension://"))){
            backgroundMessenger.send("background:contextmenu:refresh",{domain:abineUrl.getTLD(tab.url)});
          }
        });
      });

      chrome.windows.onFocusChanged.addListener(function(windowId){
        if (windowId == -1) return;
        chrome.windows.get(windowId, { populate: true }, function(w){
          if(w && w.tabs){
            for(var i=0;i< w.tabs.length ;i++){
              if(w.tabs[i].active && w.tabs[i].url && (w.tabs[i].url.match("https?://") || w.tabs[i].url.match("chrome-extension://"))){
                backgroundMessenger.send("background:contextmenu:refresh",{domain:abineUrl.getTLD(w.tabs[i].url)});
              }
            }
          }
        });
      });
    }
  });

  return {
    tabHelper: new TabHelper()
  };
});

// # abine/toolbar
//

ABINE_DNTME.define("abine/toolbar",
    ["documentcloud/underscore", "abine/timer", "jquery", 'abine/securityManager', 'storage'],
  function(_, abineTimer, $, securityManager, storage) {

    // for any special initialization after fresh install
    function freshInstall(){}

    if (true) {
      function showLockedIcon(tabId) {
        chrome.browserAction.setIcon({path: 'pages/images/icon-lock.png', tabId: tabId});
      }

      function showUnlockedIcon(tabId) {
        chrome.browserAction.setIcon({path: 'images/Icon.png', tabId: tabId});
      }

      securityManager.on('background:securityManager:initialized', function() {
        securityManager.on('background:securityManager:locked', function(){
          chrome.tabs.query({}, _.bind(function(tabs){
              _.each(tabs, _.bind(function(tab){
                  showLockedIcon(tab.id);
              },this));
          },this));
        });
        securityManager.on('background:securityManager:unlocked', function(){
            chrome.tabs.query({}, _.bind(function(tabs){
                _.each(tabs, _.bind(function(tab){
                    showUnlockedIcon(tab.id);
                },this));
            },this));
        });
        if (securityManager.isLocked()) {
            chrome.tabs.query({}, _.bind(function(tabs){
                _.each(tabs, _.bind(function(tab){
                    showLockedIcon(tab.id);
                },this));
            },this));
        } else {
            chrome.tabs.query({}, _.bind(function(tabs){
                _.each(tabs, _.bind(function(tab){
                    showUnlockedIcon(tab.id);
                },this));
            },this));
        }
      });
    }

    function updateIcon(iconData) {
      var locked = securityManager.isLocked();
      var red = [255, 0, 0, 255];
      var green = [0, 125, 0, 255];
      var yellow = [225, 193, 0, 255]; //dark enough to go behind white text
      var color = green;
      if (iconData.total > iconData.blocked+iconData.suggested) {
        color = red;
      } else if (iconData.suggested > 0) {
        color = yellow;
      }
      var title = iconData.total+'';
      if (iconData.disabled || !ABINE_DNTME.dnt_api.getDefaultBlocking()) {
        title = 'off';
        color = red;
      }
      chrome.browserAction.setBadgeText({text: title, tabId: iconData.tabId});
      chrome.browserAction.setBadgeBackgroundColor({color: color, tabId: iconData.tabId});

      if (iconData.isBadge) {
        chrome.browserAction.setIcon({path: iconData.iconPath, tabId: iconData.tabId});
      } else if (iconData.customImage) {
        var canvas = document.createElement('canvas');
        var context = canvas.getContext('2d');

        // load image from data url
        var img = new Image();
        img.onload = function() {
          context.drawImage(this, 0, 0);
          chrome.browserAction.setIcon({imageData: context.getImageData(0, 0, img.width, img.height), tabId: iconData.tabId});
        };

        img.src = iconData.customImage;
      } else {
        chrome.browserAction.setIcon({path: locked?'pages/images/icon-lock.png':'/images/Icon.png', tabId: iconData.tabId});
      }
    }

    return {
      freshInstall: freshInstall,
      updateIcon: updateIcon
    };

});
ABINE_DNTME.define("abine/tour", [], function() {
  return {
    show: function(tabId) {
      chrome.tabs.insertCSS(tabId, {file: '/pages/postinstall.css'});
      chrome.tabs.executeScript(tabId, {
        file: "/pages/postinstall.js",
        allFrames: false,
        runAt: 'document_end'
      });
    }
  };
});

// # abine/window
// API for anything that needs to open new tabs, windows, or place panels in the content script

ABINE_DNTME.define("abine/window",
    ["documentcloud/underscore", "abine/timer", "jquery"],
    function(_, abineTimer, $) {

  var PANEL_HTML = "panel.html";
  var emptyCallback = function(){};

  // Keeps trying to append the given `div` element to the given `doc` body element
  var appendPanelToBody = function(doc, div, iframe, callback) {
    if (doc.documentElement) {
      doc.documentElement.appendChild(div);
    } else {
      // for some reason the body is not available yet, try again later
      abineTimer.setTimeout(function() {
        appendPanelToBody(doc, div);
      }, 100);
    }
  };

  function getStyleAttribute(styles) {
    var str = [];
    _.each(styles, function(value, key){
      str.push(key.replace(/([A-Z])/g, function($1){return "-"+$1.toLowerCase();})+":"+value);
    });
    return str.join(" !important;");
  }

  function setAttributes(element, attrs) {
    var str = [];
    _.each(attrs, function(value, key){
      element.setAttribute(key, value);
    });
  }

  // Public API
  return {

    // ## Background Scripts

    // ### function createTab(options)
    // - `localUrl` => a local url such as 'test/spec_runner.html'
    // - `url` => a fully qualified url to the outside world
    //
    // Opens a new tab with the given url. Only works in background.
    // Will check whether a tab is already open to that URL and focus to it,
    // unless force = true.
    createTab: function(options, callback) {
      callback = callback || emptyCallback;

      var url = options.localUrl ? chrome.extension.getURL(options.localUrl) : options.url;
      if (options.force) {
        chrome.tabs.create({ active: true, url: url });
        callback();
        return;
      }

      var baseUrl = url.replace(/http[s]?/i,'').replace(/\?.*/i,'').replace(/\#.*/i,'');

      function queryCallback(testTabs) {
        var tab = _.find(testTabs, function(tab){return tab.url.indexOf(baseUrl) != -1;});
        if (!tab && !options.doNotOpenNewTab) {
          chrome.tabs.create({ active: true, url: url });
          callback();
        } else if (tab) {
          chrome.tabs.update(tab.id, {
            active: !(options.doNotActivate || false),
            url: url
          });
          // check if window is minimized
          chrome.windows.get(tab.windowId, function(window){
            if (window.state == 'minimized') {
              chrome.windows.update(window.id, {state: 'normal'});
            }
            chrome.windows.update(window.id, {focused: true});
            callback();
          });
        }
      }

      chrome.tabs.query({}, queryCallback);
    },

    getWebappTabs: function(callback) {
      chrome.tabs.query({url:  ABINE_DNTME.config.protocol + ABINE_DNTME.config.webAppHost+'/*'}, function(tabs){
        var tabIds = [];
        _.each(tabs, function(tab){tabIds.push(tab.id)});
        callback(tabIds);
      });
    },

    openWindow: function(url, width, height) {
      chrome.windows.create({url: url, width: width, height: height, focused: true, type: 'popup'});
    },

    reloadTab: function(options, callback) {
      callback = callback || emptyCallback;

      var url = options.localUrl ? chrome.extension.getURL(options.localUrl) : options.url;

      var baseUrl = url.replace(/http[s]?/i,'').replace(/\?.*/i,'').replace(/\#.*/i,'');

      function queryCallback(testTabs) {
        var tab = _.find(testTabs, function(tab){return tab.url.indexOf(baseUrl) != -1;});
        if (!tab && !options.doNotOpenNewTab) {
          chrome.tabs.create({ active: true, url: url });
          callback();
        } else if (tab) {
          chrome.tabs.reload(tab.id);
          if (!options.doNotActivate) {
            chrome.tabs.update(tab.id, {
              active: true
            });
            // check if window is minimized
            chrome.windows.get(tab.windowId, function(window){
              if (window.state == 'minimized') {
                chrome.windows.update(window.id, {state: 'normal'});
              }
              chrome.windows.update(window.id, {focused: true});
              callback();
            });
          }
        }
      }

      chrome.tabs.query({}, queryCallback);
    },

    // ## Content Scripts

    // ### function createPanel(options)
    // - `localUrl` => a local url such as 'test/spec_runner.html'
    // - `url` => a fully qualified url to the outside world
    // - `document` => a document to populate, defaults to the local document
    // - 'style' => a jquery-compatible object of css styles to apply to the panel div
    // - returns - the iframe element that was appended
    //
    // Creates a new floating panel in the document with an internal iframe holding the given url.
    createPanel: function(options) {
      var url = options.localUrl ? chrome.extension.getURL(options.localUrl) : options.url;
      var doc = options.document || document || window.document;
      var panelWidth = (options.panelWidth? options.panelWidth: 368);
      if (!doc) {
        throw("Error. abine/window createPanel called without a document context.");
      }

      var id = options.id || 'abine'+Math.floor(Math.random()*100000000);
      url = (!!url) ? url : chrome.extension.getURL(PANEL_HTML)+"?"+id;

      // Build the panel html
      var iframe = doc.createElement('iframe');
      var div = doc.createElement('div');
      div.className = options.className || "abineContentPanel";
      iframe.className = "abineContentFrame";

      setAttributes(div, {
        style: getStyleAttribute(_.extend({
          backgroundColor: "transparent",
          margin: "0",
          padding: "0",
          display: "none",
          opacity: "1",
          filter: "alpha(opacity:100)"
        }, options.style))
      });

      setAttributes(iframe, {
        width: panelWidth + "px",
        allowTransparency: "true",
        frameborder: 0,
        height: options.panelHeight + "px",
        scrolling: "no",
        src: url,
        id: id,
        style: getStyleAttribute(_.extend({
            position: "relative",
            display: "block",
            background: "transparent",
            borderWidth: "0px",
            left: "0px",
            top: "0px",
            visibility: "visible",
            opacity: 1,
            filter: "alpha(opacity:100)",
            margin: '0',
            padding: '0',
            height: options.panelHeight + "px",
            width: panelWidth + "px"
          })
        )
      });

      div.appendChild(iframe);

      appendPanelToBody(doc, div, iframe);
      return iframe;
    },

    autoResizePanels: function(options){
      var doc = options.document || document || window.document;
      var body = doc.getElementsByTagName('body')[0];
      var panels;
      if (options.frameId) {
        var frame = doc.getElementById(options.frameId);
        if (frame) {
          panels = [frame.parentElement];
        }
      }
      if (!panels) {
        panels = doc.getElementsByClassName(options.className || 'abineContentPanel');
      }
      _(panels).each(function(el) {
        var iframe = $("iframe",el)[0];
        iframe.scrolling = "no";

        $(iframe).height('auto');

        var width = $(iframe).width();
        var height = options.documentHeight;

        if (options.extraWidth) width += options.extraWidth;
        if (options.extraHeight) height += options.extraHeight;

        $(iframe).width(width);
        $(iframe).height(height);

        el.style.width = width+"px";
        el.style.height = height+"px";

        // adjust drop-down LEFT to make it appear within viewport.
        try {
          var elementLeft = parseInt(el.style.left);
          var elementRight = elementLeft + el.offsetWidth;
          var frameWidth = el.ownerDocument.defaultView.innerWidth;
          var screenRight = el.ownerDocument.documentElement.scrollLeft + frameWidth;
          if (elementRight > screenRight) {
            var diff = elementRight-screenRight;
            if (elementLeft-diff > 0) {
              el.style.left = elementLeft-diff+'px';
            } else {
              el.style.left = '0px';
              if (frameWidth < (elementRight-elementLeft)) {
                // no enough space, zoom out to fit helper in available space.
                var zoom = Math.round((frameWidth/(elementRight-elementLeft))*100);
                zoom -= 1; // for border
                if (zoom < 80) zoom = 80; // < 80% will not be legible.
                iframe.style.zoom = zoom+"%";

                // TODO: send zoom level back to panel
                iframe.contentDocument.body.style.zoom = zoom+"%";
              }
            }
          }
        } catch(e){}

      });
    },

    // ### function removePanels(options)
    // - `document` - a document to purge, defaults to the local document
    //
    // Removes all iframe panels from the given document
    removePanels: function(options) {
      var doc = options.document || document || window.document;
      function removeFrame(div, iframe) {
        if (!iframe) {
          var frame$ = $('iframe', div);
          if (frame$.length > 0) {
            iframe = frame$[0];
          }
          if (!iframe) {
            return;
          }
        }
        if (iframe.id.indexOf('doNotRemove') != -1) {
          $(iframe.parentNode).hide();
        } else {
          iframe.parentNode.parentNode.removeChild(iframe.parentNode);
        }
      }
      if (options.iframe) {
        removeFrame(null, options.iframe);
        return
      }
      var panels = doc.getElementsByClassName('abineContentPanel');
      _(panels).each(function(el) {
        removeFrame(el);
      });
    }

  };

});





// Core API
// Provides core API functionality

ABINE_DNTME.define("abine/core",
    ["documentcloud/underscore"],
    function(_) {

  // Backbone style events

  // A module that can be mixed in to *any object* in order to provide it with
  // custom events. You may `bind` or `unbind` a callback function to an event;
  // `trigger`-ing an event fires all callbacks in succession.
  //
  //     var object = {};
  //     _.extend(object, Backbone.Events);
  //     object.bind('expand', function(){ alert('expanded'); });
  //     object.trigger('expand');
  //
  var Events = {

    // Bind an event, specified by a string name, `ev`, to a `callback` function.
    // Passing `"all"` will bind the callback to all events fired.
    on : function(ev, callback, context) {
      var calls = this._callbacks || (this._callbacks = {});
      var list  = calls[ev] || (calls[ev] = {});
      var tail = list.tail || (list.tail = list.next = {});
      tail.callback = callback;
      tail.context = context;
      list.tail = tail.next = {};
      return this;
    },

    // Remove one or many callbacks. If `callback` is null, removes all
    // callbacks for the event. If `ev` is null, removes all bound callbacks
    // for all events.
    off : function(ev, callback) {
      var calls, node, prev;
      if (!ev) {
        this._callbacks = null;
      } else if (calls = this._callbacks) {
        if (!callback) {
          calls[ev] = {};
        } else if (node = calls[ev]) {
          while ((prev = node) && (node = node.next)) {
            if (node.callback !== callback) continue;
            prev.next = node.next;
            node.context = node.callback = null;
            break;
          }
        }
      }
      return this;
    },

    // Trigger an event, firing all bound callbacks. Callbacks are passed the
    // same arguments as `trigger` is, apart from the event name.
    // Listening for `"all"` passes the true event name as the first argument.
    trigger : function(eventName) {
      var node, calls, callback, args, ev, events = ['all', eventName];
      if (!(calls = this._callbacks)) return this;
      while (ev = events.pop()) {
        if (!(node = calls[ev])) continue;
        args = ev == 'all' ? arguments : Array.prototype.slice.call(arguments, 1);
        while (node = node.next) if (callback = node.callback) callback.apply(node.context || this, args);
      }
      return this;
    },

    // Bind an event, callback is called only once and event handler is removed upon first call.
    once: function(eventName, callback, context) {
      var self = this;
      function wrapCallback() {
        self.off(eventName, wrapCallback);
        return callback.apply(this, arguments);
      }
      return this.on(eventName, wrapCallback, context);
    },

    // Bind all events, calling of any event will un-subscribe from all other events.
    // eg:  onceAny(['event1', callback1, context1], ['event2', callback2, context2], ....)
    onceAny: function() {
      var mainArguments = arguments;

      function offAll() {
        for (var i=0;i<mainArguments.length;i++) {
          // remove internal event handler
          this.off.apply(this, [mainArguments[i][0], offAll]);
          // remove input events
          this.off.apply(this, mainArguments[i]);
        }
      }

      for (var i=0;i<arguments.length;i++) {
        // hook with input arguments
        this.on.apply(this, arguments[i]);
        // add our internal event handler to remove all other event handlers
        this.on.apply(this, [arguments[i][0], offAll]);
      }

      return this;
    },

    // returns true if there is a handler for given event name
    hasListeners: function(eventName) {
      var node, calls;
      if (!(calls = this._callbacks)) return false;
      if (!(node = calls[eventName])) return false;
      while (node = node.next) {
        if (node.callback)
          return true;
      }
      return false;
    }

  };

  Events.bind = Events.on;
  Events.unbind = Events.off;

  // Backbone style inheritance system
  
  // Shared empty constructor function to aid in prototype-chain creation.
  var ctor = function(){};

  // Helper function to correctly set up the prototype chain, for subclasses.
  // Similar to `goog.inherits`, but uses a hash of prototype properties and
  // class properties to be extended.
  var inherits = function(parent, protoProps, staticProps) {
    var child;

    // The constructor function for the new subclass is either defined by you
    // (the "constructor" property in your `extend` definition), or defaulted
    // by us to simply call `super()`.
    if (protoProps && protoProps.hasOwnProperty('constructor')) {
      child = protoProps.constructor;
    } else {
      child = function(){ return parent.apply(this, arguments); };
    }

    // Inherit class (static) properties from parent.
    _.extend(child, parent);

    // Set the prototype chain to inherit from `parent`, without calling
    // `parent`'s constructor function.
    ctor.prototype = parent.prototype;
    child.prototype = new ctor();

    // Add prototype properties (instance properties) to the subclass,
    // if supplied.
    if (protoProps) _.extend(child.prototype, protoProps);

    // Add static properties to the constructor function, if supplied.
    if (staticProps) _.extend(child, staticProps);

    // Correctly set child's `prototype.constructor`.
    child.prototype.constructor = child;

    // Set a convenience property in case the parent's prototype is needed later.
    child.__super__ = parent.prototype;

    return child;
  };

  // The self-propagating extend function that Backbone classes use.
  var extend = function (protoProps, classProps) {
    var child = inherits(this, protoProps, classProps);
    child.extend = this.extend;
    return child;
  };

  // Base class
  var BaseClass = function BaseClass() {
    this.initialize.apply(this, arguments);
  };
  _.extend(BaseClass.prototype, Events, {
    initialize: function initialize() {}
  });
  
  // Give it inheritance 
  BaseClass.extend = extend;  
    
    
  // Public API
  return {
    
    // ### class BaseClass
    // Base class for Backbone style inheritance system. i.e.
    // var parent = new abineCore.baseClass()
    // var child = parent.extend({ ... }) 
    BaseClass: BaseClass,
    
    // ### mixin Events
    // Backbone-style event dispatcher. Mix in to any object that can dispatch events, i.e.
    // _.extend(myClass.prototype, abineCore.Events)
    Events: Events
    
  }

});
ABINE_DNTME.define("abine/backgroundEventLogger",
    ['abine/config', 'storage', 'persistence', 'documentcloud/underscore'],
    function (config, storage, persistence, _) {

  var FINEST = 0;
  var DEBUG = 1;
  var INFO = 2;
  var WARN = 3;
  var ERROR = 4;
  var NONE = 5;

  var noOp = function(){};

  function errorStringToJSON(errorString){
    var error = {}; // JSON representation

    var errorArray = errorString? errorString.split("#"):[];

    // parse the error string [ERROR HANDLER]#[abine/models/user]#[loadShieldedPhones]# error event in this.shieldedPhones.fetchAll
    if(errorArray.length == 4){
      error.error_tag = errorArray[0];
      error.module = errorArray[1];
      error.method = errorArray[2];
      error.message = errorArray[3];
    } else {
      error.error_tag = error.module = error.method = "not well-formed";
      error.message = errorString;
    }

    // get information from static config options
    error.browser = config.browser;
    error.build = config.buildNum;
    error.environment = config.environment;
    error.build_tag = config.tags[0];
    error.server_host = config.serverHost;
    error.mapping_server_host = config.mappingServerHost;
    error.license_server_host = config.licenseServerHost;
    error.phone_server_host = config.phoneServerHost;
    error.crypt = config.crypt;
    error.version = config.version;

    // get the user agent string to ID the browser version
    error.user_agent = window.navigator.userAgent;

    return error;
  }

  function sendErrorMessage(errorJSON){
    var data = {
      error: errorJSON
    };

    var callAjax = function(emailSalt){
      if(emailSalt){
        data.email_salt = emailSalt;
      }
      ABINE_DNTME.require(['abine/ajax'], function(abineAjax){
        abineAjax.ajax({
          type: 'POST',
          url: config.protocol + config.serverHost + "/api/errors",
          data: data,
          success: function(response){
            BackgroundEventLogger.debug("error reporting successful");
          },
          error: function(response){
            BackgroundEventLogger.debug("error reporting failed");
          }
        });
      });
    };

    if(storage.Preference){
        storage.Preference.findBy('key', 'Salt.ShieldedEmail', _.bind(function(emailSalt){
          callAjax(emailSalt?emailSalt.value:null);
        }, this));
    } else {
      callAjax();
    }
  }

  function getConsoleService() {
    var currentTime = new Date().toUTCString();
    if (typeof console == "undefined" || typeof(console.debug) == 'undefined') {
      function ConsoleWrap(msg) {
        try{Console(msg);}catch(e){}
      }
      return {
        finest: function(msg) { ConsoleWrap("[FINEST][" + currentTime + "]: " + msg); },
        debug:  function(msg) { ConsoleWrap("[DEBUG][" + currentTime + "]: "  + msg);   },
        info:   function(msg) { ConsoleWrap("[INFO][" + currentTime + "]: "  + msg);    },
        warn:   function(msg) { ConsoleWrap("[WARNING][" + currentTime + "]: "  + msg); },
        error: function(msg){
          ConsoleWrap("[ERROR][" + date + "]: "  + msg);
          if (false && storage.Preference){
            storage.Preference.findBy('key', 'shouldSendErrorReports', function(shouldSendErrorReports){
              if(shouldSendErrorReports.isTrue()){
                sendErrorMessage(errorStringToJSON(msg))
              }
            });
          }
        }
      }
    } else if (typeof console != "undefined") {
      return {
        finest: function(msg) {console.debug("[FINEST][" + currentTime + "]: " + msg); },
        debug:  function(msg) { console.debug("[DEBUG][" + currentTime + "]: "  + msg);   },
        info:   function(msg) { console.info("[INFO][" + currentTime + "]: "  + msg);    },
        warn:   function(msg) { console.warn("[WARNING][" + currentTime + "]: "  + msg); },
        error: function(msg){
          console.error("[ERROR][" + currentTime + "]: "  + msg);
          if(false && storage.Preference){
            storage.Preference.findBy('key', 'shouldSendErrorReports', function(shouldSendErrorReports){
              if(shouldSendErrorReports.isTrue()){
                sendErrorMessage(errorStringToJSON(msg))
              }
            });
          }
        }
      }
    }
  }

  var BackgroundEventLogger = getConsoleService();

  BackgroundEventLogger.dumpObj = function(obj){ // http://www.sitepoint.com/javascript-json-serialization/ - thanks, bro.
    var t = typeof (obj);
    if (t != "object" || obj === null) {
      if (t == "string") obj = '"'+obj+'"';
      return String(obj);
    }
    else {
      var n, v, json = [], arr = (obj && obj.constructor == Array);
      for (n in obj) {
        v = obj[n]; t = typeof(v);
        if (t == "string") v = '"'+v+'"';
        else if (t == "object" && v !== null) v = BackgroundEventLogger.dumpObj(v);
        json.push((arr ? "" : '"' + n + '":') + String(v));
      }
      return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
    }
  };

  // intentionally not using BREAK in case statement to let it disable all logging below given level.
  switch (config.logLevel) {
    case "NONE": BackgroundEventLogger.error = noOp;
    case "ERROR": BackgroundEventLogger.warn = noOp;
    case "WARN": BackgroundEventLogger.info = noOp;
    case "INFO": BackgroundEventLogger.debug = noOp;
    case "DEBUG":BackgroundEventLogger.finest = noOp;
  }

  BackgroundEventLogger.info("Log level set to "+config.logLevel);

  ABINE_DNTME.log = ABINE_DNTME.log || BackgroundEventLogger; // separate ABINE_DNTME variables for BG and FG, supposedly... firefox??

  return BackgroundEventLogger;

});

// common initialization code for background page for all browsers.

ABINE_DNTME.define("abine/backgroundInit",
    ['abine/storageRouter', 'abine/storage/registry',
      'abine/window', 'storage', 'abine/securityManager',
      'abine/stubs', 'abine/toolbar', 'abine/timer', 'abine/ajax', 'abine/config',
      'abine/pageEvents', 'abine/tabs', 'persistence',
      'abine/mappingServer','modules', 'documentcloud/underscore', 'abine/browserSyncStorage',
      'abine/core', 'abine/dntmeInit', 'abine/conflict_handler',
      'abine/saveForms', 'abine/formSubmitDetector', 'abine/localStorage', 'abine/responseCodes', 'abine/licenseServer',
      'abine/savePaymentForms'
    ],
    function(abineStorageRouter, abineStorageRegistry,
      abineWindow, storage, securityManager,
      abineStubs, toolbar, timer, abineAjax, config,
      pageEvents, abineTabs, persistence, mappingServer, modules, _, browserSyncStorage,
      abineCore, dntme, conflictHandler,
      abineSaveForms, abineFormSubmitDetector, localStorage, responseCodes, licenseServer,
      savePaymentForms) {

      ABINE_DNTME.setupUninstallUrl = function() {
        storage.Preference.findBy('key', 'installAuthToken', function(installAuthTokenPref){
          try {
            var url = ABINE_DNTME.config.protocol + ABINE_DNTME.config.webHost+'/blur/uninstall?p='+btoa(JSON.stringify({
                b: ABINE_DNTME.config.browser,
                v: ABINE_DNTME.config.version,
                t: ABINE_DNTME.config.tags[0],
                e: ABINE_DNTME.config.environment,
                i: securityManager.getUserId(),
                ia: (installAuthTokenPref ? installAuthTokenPref.value : null)
              }));
            if (typeof(browser) == 'undefined')
              chrome.runtime.setUninstallURL(url);
            else
              browser.runtime.setUninstallURL(url);
          } catch(e) {
            console.log(e)
          }
        });
      }

      var initialized = false;

      var emptyCallback = function(){};

      function initialize(backgroundMessenger) {
        if (initialized) return;

        // initialize stubs (an interface for calls from content script to background objects)
        abineStubs.start(backgroundMessenger);

        // init form submit detector
        abineFormSubmitDetector.formSubmitDetector.init(backgroundMessenger);

        // initialize form saver
        abineSaveForms.init(backgroundMessenger);

        savePaymentForms.init(backgroundMessenger);

        backgroundMessenger.on("background:show:notification:forward", function(notification, sender){
          backgroundMessenger.send("background:show:notification", notification, sender);
        });

        backgroundMessenger.on("background:create:panel:forward", function(options, sender){
          backgroundMessenger.send("background:create:panel", options, sender);
        });

        backgroundMessenger.on('contentManager:init', function(payload, sender){
          if (false) {
            ABINE_DNTME.require(['abine/pay_button'], function(button){
              button.show(sender, payload.domain);
            });
          }
        });

        initialized = true;

        _.each(_.values(modules), function(module){
          if(module.setMessenger){
            module.setMessenger(backgroundMessenger);
          }
        });

        abineTabs.tabHelper.init(backgroundMessenger);

        // Create a storage router
        var storageRouter = new abineStorageRouter.StorageRouter({
          messenger: backgroundMessenger,
          noMigrations: true,
          //fixtures: true,
          ready: function() {
            storage.Preference.setupUpgradeDefaults();
            try {
              afterStorageInitialization(backgroundMessenger);
            } catch(e){}
          }
        });
        abineStorageRegistry.registerStorageEntities(storageRouter);

        startTabService(backgroundMessenger);//should this be its own class?

        LOG_INFO && ABINE_DNTME.log.info("Common background scripts initialized");
      }

      function isNewerVersion(newVersion, oldVersion) {
        newVersion = newVersion.split(".");
        oldVersion = oldVersion.split(".");
        if (newVersion.length > oldVersion.length) {
          return true;
        } else {
          for (var i=0;i<newVersion.length;i++) {
            if (parseInt(newVersion[i], 10) < parseInt(oldVersion[i], 10)) {
              return false;
            }
            if (parseInt(newVersion[i], 10) > parseInt(oldVersion[i], 10)) {
              return true;
            }
          }
        }
        return false;
      }

      function startActivityMonitor() {
        var activityCheckFunction = function () {
          storage.Preference.findBy('key', 'authToken', function (authToken) {
            if (authToken && authToken.value) {
              storage.Preference.findBy('key', 'lastActivity', function (lastActivity) {
                storage.Preference.findBy('key', 'autolock', function (autolock) {
                  if (autolock && autolock.value == "restart") {
                    if (!ABINE_DNTME._started) {
                      modules.licenseModule.logout(authToken.value, function(){
                        persistence.remove(lastActivity);
                        securityManager.setAuthToken('', '', '');
                      });
                      return;
                    }
                  }
                  ABINE_DNTME._started = true;
                  if (autolock && autolock.value != "never") {
                    var now = Date.now();
                    if ((now - parseFloat(lastActivity.value)) >= (parseFloat(autolock.value) * 60 * 60 * 1000)) {
                      modules.licenseModule.logout(authToken.value, function(){
                        persistence.remove(lastActivity);
                        securityManager.setAuthToken('', '', '');
                      });
                    }
                  }
                });
              });
            } else {
              ABINE_DNTME._started = true;
            }
          });
        }

        timer.setInterval(activityCheckFunction, 60 * 1000);
        activityCheckFunction();
      }

      var afterStorageInitDone = false;
      // All initializations that must happen after storage
      function afterStorageInitialization(backgroundMessenger) {

        BackgroundInitializer.trigger('storage:initialized');

        // check if we have latest version
        storage.Preference.findBy('key', 'new_version', function(pref) {
          if (pref) {
            if (pref.value == config.version || isNewerVersion(pref.value, config.version)) {
              persistence.remove(pref);
              persistence.flush();
            }
          }
        });



        conflictHandler.checkForMaskme(function(maskmeInstalled) {
          LOG_DEBUG && ABINE_DNTME.log.debug(maskmeInstalled?"Maskme installed":"Maskme not found");
          try {
            dntme.initialize(backgroundMessenger);
          } catch(e){
            LOG_ERROR && ABINE_DNTME.log.error("error during dntme initialization "+ e.message+ e.stack);
          }

          if (afterStorageInitDone) return;

          afterStorageInitDone = true;
          // initialize page event handler
          pageEvents.initialize(backgroundMessenger);

          securityManager.load(backgroundMessenger);

          startActivityMonitor();

          modules.criticalProcess.setMessenger(backgroundMessenger);

          openMyInfoOnInstall(function(firstTime){
            var delay = 0;
            if(firstTime){
              LOG_DEBUG && ABINE_DNTME.log.debug("dntme loaded for the first time, delaying mapping panels for 15mins");
              delay = 2*60*1000;
            }
            timer.setTimeout(_.bind(function(){
              modules.mappingsModule.startDailyMappingCheck();
            },this), delay);
            ABINE_DNTME.setupUninstallUrl();
          });
        });
      }

      function setupInstallPrefs(callback) {
        callback = callback || emptyCallback;
        storage.Preference.batchCreateOrModify([
          {key: "installUserAgent", value: ABINE_DNTME.userAgent},
          {key: "installVersion", value: config.version},
          {key: "installTags", value: config.tags[0]},
          {key: "installDate", value: Date.now()}
        ], function(){
          callback();
        });
        if(config.silent){
          storage.Preference.batchCreateOrModify([
              {key: "showMedals",value: "false"},
              {key: "showCheckoutPanelGrowl", value: "false"},
              {key: "suggestProtectedEmail",value: "false"},
              {key: "suggestProtectedPhone",value: "false"},
              {key: "suggestProtectedCard",value: "false"},
              {key: "showTrackersOnload",value: "false"},
              {key: "showWeeklyReports",value: "false"}
          ],function(){
              callback();
          });
        }
      }

      function setupDynamicTag(callback) {
        callback = callback || emptyCallback;

        var protocol = "http://";
        if (ABINE_DNTME.config.environment == 'production') {
          protocol = "https://";
        }
        var url = protocol + ABINE_DNTME.config.webHost + '/maskme/v2/tag.php?currentTag='+config.tags[0];

        abineAjax.ajax({
            url: url,
            success: function(data, textStatus, jqXHR) {
              try{data = JSON.parse(data);}catch(e){data={};}
              if (data && data != '' && data.abineMaskMeTag && data.abineMaskMeTag != '') {
                config.tags.unshift(data.abineMaskMeTag);
                storage.Preference.createOrModify({key:'dynamicTag', value:data.abineMaskMeTag}, function(){
                  callback();
                });
              } else {
                callback();
              }
            },
            error: function(){
              callback();
            }
        });
      }

      function openMyInfoOnInstall(callback) {
        callback = callback || emptyCallback;

        // open My Info / Edit User page on first install
        storage.Preference.findBy('key', 'freshInstall', function (pref) {
          if (!pref || pref.value === 'false') {
            storage.Preference.findBy('key', 'installVersion', function (pref) {
              if (pref) {
                if (pref.value < '4.5') { // upgrade from preblur so show blur growl if we haven't already
                  storage.Preference.findBy('key', 'showBlurGrowl', function (pref) {
                    if (!pref) {
                      // only show the blur growl for upgrades to blur from non blur
                      storage.Preference.createOrModify({key: 'showBlurGrowl', value: 'true'}, function () {
                      });
                      storage.Preference.createOrModify({key: 'showBlurPage', value: 'true'}, function () {
                      });
                    }
                  });
                } else { // not an upgrade from preblur, set showblurgrowl to false if not already set
                  storage.Preference.findBy('key', 'showBlurGrowl', function (pref) {
                    if (!pref) {
                      // only show the blur growl for upgrades to blur from non blur
                      storage.Preference.createOrModify({key: 'showBlurGrowl', value: 'false'}, function () {
                      });
                      storage.Preference.createOrModify({key: 'showBlurPage', value: 'false'}, function () {
                      });
                    }
                  });
                }
              }
              storage.Preference.findBy('key', 'dynamicTag', function (pref) {
                if (pref) {
                  config.tags.unshift(pref.value);
                }
                if (!config.silent && config.tags[0].toUpperCase().indexOf('YT14') != -1) {
                  config.silent = true;
                }
                modules.licenseModule.startDailyTouch(function () {
                  // overrides on every browser restart but split tests + overrides only on install
                  disableIntrusive(function () {
                    setupConfigOverrides(callback);
                  });
                });
              });
            });
            return;
          }
          if (!config.silent && config.tags[0].toUpperCase().indexOf('YT14') != -1) {
            config.silent = true;
          }

          var firstTime = true;
          pref.value = 'false';
          persistence.add(pref);

          toolbar.freshInstall();

          setupInstallPrefs(function () {
            setupDynamicTag(function () {
              conflictHandler.checkForMergedAddon(function (hasMergedData, requiresMigration) {
                conflictHandler.installAuthTokenCheck(hasMergedData, function () {
                  modules.licenseModule.startDailyTouch(function () {
                    setupConfigOverrides(function () {
                      LOG_DEBUG && ABINE_DNTME.log.debug("Done with overrides");
                      setupSplitTests(function () {
                        LOG_DEBUG && ABINE_DNTME.log.debug("Done with split tests");
                        if (!config.silent) {
                          LOG_DEBUG && ABINE_DNTME.log.debug(hasMergedData ? "Merged Maskme found " + requiresMigration : "Merged Maskme not found");
                          var url = 'http://' + ABINE_DNTME.config.webHost + "/blur/postinstall?";
                          if (ABINE_DNTME.maskme_installed) {
                            url += 'maskme=1&';
                          }
                          if (hasMergedData && requiresMigration) {
                            url += 'merged=1&';
                            storage.Preference.createOrModify({
                              key: 'mergedMigrationPending',
                              value: 'true'
                            }, function () {
                            });
                          }
                          if (ABINE_DNTME.dntme_upgrade) {
                            url += 'upgrade=1&';
                          }
                          url += 'webapp=' + ABINE_DNTME.config.protocol + ABINE_DNTME.config.webAppHost;
                          url += '&tour=1'; // indicates it supports full page tour

                          savePostInstallUrl(url, function () {
                            securityManager.openPage(url, true);
                            callback(firstTime);
                          });
                        } else {
                          callback(firstTime);
                        }
                      });
                    })
                  });
                });
              });
            });
          });
        });
      }

      function savePostInstallUrl(url, callback) {
          callback = callback || emptyCallback;
          storage.Preference.createOrModify(
              {key: "postInstallUrl",value: url}
              ,function(){
                  callback();
              });
      }

      function startTabService(backgroundMessenger){
        backgroundMessenger.on('window:createTab', function(payload) {

          if(payload.panelData){
            if(!ABINE_DNTME.lastPanel){ ABINE_DNTME.lastPanel = {}; }
            ABINE_DNTME.lastPanel[payload.panelData.action] = payload.panelData.type;
          }

          abineWindow.createTab(payload);
        });
      }

      var enableWalletSplitTest = function(splitTests, callback) {
        // intentionally empty
        callback();
      };

      var enableIntrusiveSplitTest = function(splitTests, callback) {
        if(splitTests){
          var intrusiveSplit = splitTests["intrusive01"];
          if(!config.silent && intrusiveSplit != null){
            modules.licenseModule.logSplitData({bucket_id:intrusiveSplit,test_data:{int0:1}});
            if(intrusiveSplit == responseCodes.SPLIT_INTRUSIVE_NON_INTRUSIVE){
              storage.Preference.createOrModify(
                  {key: "showTrackersOnload",value: "false"}
                  ,function(){
                    callback();
                  });
            } else {
              callback();
            }
          } else {
            callback();
          }
        } else {
          callback();
        }
      };

      var enableIconPanelsSplitTest = function(splitTests, callback) {
        if(splitTests){
          var iconPanelsSplit = splitTests["iconpanels01"];
          if(!config.silent && iconPanelsSplit != null){
            modules.licenseModule.logSplitData({bucket_id:iconPanelsSplit,test_data:{int0:1}});
            if(iconPanelsSplit != responseCodes.SPLIT_ICON_PANELS_NORMAL){
              storage.Preference.createOrModify(
                  {key: "useIconPanelMode",value: iconPanelsSplit}
                  ,function(){
                    callback();
                  });
            }else{
              callback();
            }
          }else{
            callback();
          }
        }else{
          callback();
        }
      };

      function setupSplitTests(callback) {
        try  {
          var splitTests = [enableIntrusiveSplitTest, enableWalletSplitTest, enableIconPanelsSplitTest];
          var afterAllSplitTests = _.after(splitTests.length, callback);
          storage.Preference.findBy('key', 'splitTests', function(pref){
            if(pref && pref.value) {
              var splitTestJSON = JSON.parse(pref.value);
              _.each(splitTests, function(setupSplitTest) {
                setupSplitTest(splitTestJSON, afterAllSplitTests);
              });
            } else {
              callback();
            }
          });
        } catch (e) {
          callback();
        }
      }

      function setupConfigOverrides(callback) {
        try  {
          storage.Preference.findBy('key', 'configOverrides', function(pref){
            if(pref && pref.value) {
              var configOverridesJSON = JSON.parse(pref.value);
              for(var key in configOverridesJSON){
                if(key in ABINE_DNTME.config){
                  ABINE_DNTME.config[key] = configOverridesJSON[key];
                  ABINE_DNTME.config.overriddenKeys[key] = true;
                }
              }
            }
            callback();
          });
        } catch (e) {
          callback();
        }
      }

      function disableIntrusive(callback) {
          storage.Preference.createOrModify({key: 'showTrackersOnload', value: 'false'}, function(){
              callback();
          });
      }

      ABINE_DNTME.bguser = {
        shouldSendErrorReports: function(){
          var pref = storage.Preference.getSync('shouldSendErrorReports');
          return pref && pref.isTrue();
        },

        getEmailSalt: function() {
          var pref = storage.Preference.getSync('Salt.ShieldedEmail');
          return (pref && pref.value);
        }
      };

      var BackgroundInitializer = {
        initialize: initialize
      };

      _.extend(BackgroundInitializer, abineCore.Events);

      return BackgroundInitializer;
});
ABINE_DNTME.define("abine/badgeManager",
    ['documentcloud/underscore'],
  function(_) {

    return {
      badges: {
        'totalBlocked': {
          levels: [
              {name:'dntp.badges.bronze',limit:500, cssClass:'Bronze',icon:'/images/bronze-sm.png'},
              {name:'dntp.badges.silver',limit:5000,cssClass:'Silver',icon:'/images/silver-sm.png'},
              {name:'dntp.badges.gold',limit:10000,cssClass:'Gold',icon:'/images/gold-sm.png'},
              {name:'dntp.badges.platinum',cssClass:'Platinum',limit:25000,icon:'/images/platinum-sm.png', level:1}
          ],
          text: 'dntp.badges.totalBlocked.text',
          title: 'dntp.badges.totalBlocked.title',
          badgeText: 'dntp.badges.totalBlocked.badgeText'
        },
        'socialBlocked': {
          levels: [
              {name:'dntp.badges.bronze',limit:100,cssClass:'Bronze',icon:'/badge/bronze.png'},
              {name:'dntp.badges.silver',limit:1000,cssClass:'Silver',icon:'/badge/silver.png'},
              {name:'dntp.badges.gold',limit:5000,cssClass:'Gold',icon:'/badge/gold.png'},
              {name:'dntp.badges.platinum',cssClass:'Platinum',limit:15000,icon:'/badge/platinum.png', level:1}
          ],
          text: 'dntp.badges.socialBlocked.text',
          title: 'dntp.badges.socialBlocked.title',
          badgeText: 'dntp.badges.socialBlocked.badgeText'
        }
      },

      getBadgeAt: function(setting, index) {
        var badge = {name:'None',limit:-1,icon:'/badge/nobadge.png'};

        var badges = this.badges[setting];
        if (index >=0) {
          var levels = badges.levels;
          if (index >= levels.length) {
            // grow levels to make it infinite levels
            for (var i=levels.length-1;i<=index;i++) {
              var newBadge = {
                name: levels[i].name,
                cssClass: levels[i].cssClass,
                limit:levels[i].limit+(setting.indexOf('social') == 0?10000:25000),
                icon:levels[i].icon,
                level:levels[i].level+1
              };
              levels.push(newBadge);
            }
          }
          badge = levels[index];
        }

        badge.title = badges.title;
        badge.text = badges.text;
        badge.badgeText = badges.badgeText;
        badge.setting = setting;

        return badge;
      },

      compute: function(currentCount) {
        var currentBadge = {};

        var setting = 'totalBlocked';
        var i = 0;
        var maxIndex = -1;
        while (true) {
          var badge = this.getBadgeAt(setting, i);
          if (badge.limit > currentCount) {
            // count above badge limit, so stop
            break;
          }
          if (badge.limit < currentCount){
            maxIndex = i;
            currentBadge = badge;
          }
          i++;
        }

        return {badgeIndex: maxIndex, badge: currentBadge};

      }
    };

  }
);
ABINE_DNTME.define("abine/merged-dataset",
  ['documentcloud/underscore', 'abine/core', 'abine/localStorage', 'storage',
    'abine/timer', 'persistence', 'modules', 'abine/securityManager', 'aws', 'abine/crypto'
  ],
  function (_, abineCore, localStorage, storage,
            timer, persistence, modules, securityManager, AWS, crypto) {

    var MAX_RECORDS_PER_DATASET = localStorage.getItem('maxRecords') || 1000;

    var MergedDataset = function (client) {
      this.client = client;
    };

    MergedDataset.prototype.refreshDatasetNames = function (name, callback) {
      var self = this;
      this.client.refreshDatasetMetadata(function () {
        self.client.listDatasets(function (err, datasets) {
          self.datasetNames = [name];
          _.each(datasets, function (set) {
            if (set.datasetName && set.datasetName.indexOf(name) !== -1 && set.datasetName !== name) {
              self.datasetNames.push(set.datasetName);
            }
          });
          callback();
        });
      });
    };
    MergedDataset.prototype.updateDatasets = function (name, callback) {
      var self = this;
      var removedDatasets = _.clone(this.datasetNames);
      var newDataset = false;
      this.client.refreshDatasetMetadata(function () {
        self.client.listDatasets(function (err, datasets) {
          _.async.forEachSeries(datasets, function (set, next) {
            if (set.datasetName && set.datasetName.indexOf(name) !== -1) {
              if (self.datasetNames.indexOf(set.datasetName) === -1) {
                newDataset = true;
              } else {
                removedDatasets = _.without(removedDatasets, set.datasetName);
              }
              next();
            } else {
              next();
            }
          }, function () {
            if (newDataset || removedDatasets.length > 0) {
              self.openOrCreateDataset(name, callback);
            } else {
              callback();
            }
          });
        });
      });
    };
    MergedDataset.prototype.openOrCreateDataset = function (name, callback) {
      var self = this;
      this.refreshDatasetNames(name, function () {
        self.datasets = [];
        _.async.forEachSeries(self.datasetNames, function (name, next) {
          self.client.openOrCreateDataset(name, function (err, dataset) {
            self.datasets.push(dataset);
            next();
          });
        }, function () {
          callback();
        });
      });
    };
    MergedDataset.prototype.getAllRecords = function (callback) {
      var allRecords = [];
      _.async.forEachSeries(this.datasets, function (dataset, next) {
        dataset.getAllRecords(function (err, records) {
          allRecords = allRecords.concat(records);
          next();
        });
      }, function () {
        callback(null, allRecords);
      });
    };
    MergedDataset.prototype.remove = function (key, callback) {
      var deleted = false;
      var self = this;
      _.async.forEachSeries(this.datasets, function (dataset, next) {
        if (deleted) {
          next();
          return;
        }
        dataset.get(key, function (err, oldVal) {
          if (oldVal !== void 0) {
            deleted = true;
            dataset.remove(key, callback);
            if (dataset !== self.datasets[0]) {
              self.touchDefaultDataset();
            }
          }
          next();
        });
      }, function () {
        if (!deleted) {
          callback(null);
        }
      });
    };
    MergedDataset.prototype.getCorrectSyncCount = function (callback) {
      this.datasets[0].getCorrectSyncCount(callback);
    };
    MergedDataset.prototype.hasChanges = function (callback) {
      _.async.forEachSeries(this.datasets, function (dataset, next) {
        dataset.hasChanges(function (err, changed) {
          if (changed) {
            callback(null, true);
            return;
          }
          next();
        });
      }, function () {
        callback(null, false);
      });
    };
    MergedDataset.prototype.put = function (key, value, callback, skipSyncCountChecks) {
      var self = this;
      var saved = false;
      _.async.forEachSeries(this.datasets, function (dataset, next) {
        if (saved) {
          next();
          return;
        }
        dataset.get(key, function (err, oldVal) {
          if (oldVal !== void 0) {
            saved = true;
            dataset.put(key, value, function () {
            });
          }
          next();
        });
      }, function () {
        if (!saved) {
          _.async.forEachSeries(self.datasets, function (dataset, next) {
            if (saved) {
              next();
              return;
            }
            dataset.getAllRecords(function (err, records) {
              if (records && records.length < MAX_RECORDS_PER_DATASET) {
                saved = true;
                dataset.put(key, value, function () {
                });
              }
              next();
            });
          }, function () {
            var idx;
            if (!saved) {
              idx = 1;
              while (self.datasetNames.indexOf(idx + '_BlurData') !== -1) {
                idx = idx + 1;
              }
              self.client.openOrCreateDataset(idx + '_BlurData', function (err, dataset) {
                self.datasetNames.push(idx + '_BlurData');
                self.datasets.push(dataset);
                dataset.put(key, value, function () {
                  if (!skipSyncCountChecks) {
                    self.touchDefaultDataset(callback, true);
                  } else {
                    callback();
                  }
                });
              });
            } else if (!skipSyncCountChecks) {
              self.touchDefaultDataset(callback);
            } else {
              callback();
            }
          });
        } else if (!skipSyncCountChecks) {
          self.touchDefaultDataset(callback);
        } else {
          callback();
        }
      });
    };
    MergedDataset.prototype.touchDefaultDataset = function (callback, force) {
      var self = this;
      if (this.datasets.length > 1) {
        this.datasets[0].hasChanges(function (err, changed) {
          if (!changed || force) {
            self.datasets[0].put('Dummy:1', JSON.stringify({
              id: "1",
              data: Math.random(),
              modifiedAt: new Date(),
              numDatasets: self.datasets.length
            }), function () {
              if (callback) {
                callback();
              }
            });
          } else {
            if (callback) {
              callback();
            }
          }
        });
      } else {
        if (callback) {
          callback();
        }
      }
    };
    MergedDataset.prototype.putAll = function (items, callback) {
      var self = this;
      var keys = _.keys(items);
      _.async.forEachSeries(keys, function (key, next) {
        self.put(key, items[key], function () {
          next();
        });
      }, function () {
        self.touchDefaultDataset(callback);
      }, true);
    };
    MergedDataset.prototype.synchronize = function (response) {
      var self = this;
      var allSuccess = true;
      _.async.forEachSeries(this.datasets, function (dataset, next) {
        dataset.synchronize({
          onSuccess: function () {
            next();
          },
          onFailure: function (err) {
            allSuccess = false;
            response.onFailure(err);
          },
          onConflict: function (dataset, conflicts, callback) {
            var resolved;
            resolved = [];
            _.each(conflicts, function (conflict) {
              var local, localModifiedAt, remote, remoteModifiedAt;
              if (conflict.remoteRecord.value === "") {
                resolved.push(conflict.resolveWithRemoteRecord());
                LOG_FINEST && ABINE_DNTME.log.finest("conflict: accepted remote remove");
              } else if (conflict.localRecord.value === '') {
                resolved.push(conflict.resolveWithLocalRecord());
                LOG_FINEST && ABINE_DNTME.log.finest("conflict: accepted local remove");
              } else if (conflict.remoteRecord.value === conflict.localRecord.value) {
                resolved.push(conflict.resolveWithRemoteRecord());
                LOG_FINEST && ABINE_DNTME.log.finest("conflict: accepted remote as no change in value");
              } else {
                try {
                  local = JSON.parse(conflict.localRecord.value);
                  remote = JSON.parse(conflict.remoteRecord.value);
                } catch (_error) {
                }
                try {
                  localModifiedAt = new Date(local.modifiedAt);
                } catch (_error) {
                }
                try {
                  remoteModifiedAt = new Date(remote.modifiedAt);
                } catch (_error) {
                }
                if (localModifiedAt > remoteModifiedAt) {
                  resolved.push(conflict.resolveWithLocalRecord());
                  LOG_FINEST && ABINE_DNTME.log.finest("conflict: accepted local change");
                } else {
                  resolved.push(conflict.resolveWithRemoteRecord());
                  LOG_FINEST && ABINE_DNTME.log.finest("conflict: accepted remote change");
                }
              }
            });
            dataset.resolve(resolved, function (err) {
              if (!err) {
                callback(true);
              }
            });
          },
          onDatasetDeleted: function (dataset, datasetName, callback) {
            callback(true);
          },
          onDatasetsMerged: function (dataset, datasetNames, callback) {
            callback(true);
          }
        });
      }, function () {
        if (allSuccess) {
          response.onSuccess();
          self.datasets[0].get("Dummy:1", function (err, value) {
            var rec;
            if (value) {
              try {
                rec = JSON.parse(value);
              } catch (_error) {
              }
              if (rec && rec.numDatasets > self.datasets.length) {
                self.updateDatasets('BlurData', function () {
                  response.resync();
                });
              }
            }
          });
        }
      });
    };

    return MergedDataset;
  }
);
ABINE_DNTME.define("abine/blurstore-api",
  ['documentcloud/underscore', 'abine/core', 'abine/localStorage', 'storage',
    'abine/timer', 'persistence', 'modules', 'abine/securityManager', 'aws', 'abine/crypto',
    'abine/merged-dataset', 'abine/pageEvents', 'abine/perf'
  ],
  function(_, abineCore, localStorage, storage,
           timer, persistence, modules, securityManager, AWS, crypto,
           MergedDataset, pageEvents, Performance
    ) {

    // override local storage of cognito to our browser specific local-storage.
    AWS.CognitoSyncManager.localStorage = localStorage;

    var POLL_TIME = 10 * 60 * 1000;

    if (LOG_FINEST) {
      var msg = '';
      var logger = function(m){msg+=m+"\n";};
      timer.setInterval(function(){if(msg != '') {
        ABINE_DNTME.log.finest(msg);
        msg='';
      }}, 5000);
    } else {
      var logger = function(){};
    }

    AWS.CognitoSyncManager.Dataset.prototype.getCorrectSyncCount = function(callback) {
      var self = this;
      this.getDatasetMetadata(function(err, metadata) {
        if (metadata.lastSyncCount) {
          callback(metadata.lastSyncCount);
        } else {
          self.getAllRecords(function(err, records) {
            var lastSyncCount = 0;
            _.each(records, function(record) {
              if (record.syncCount && record.syncCount > lastSyncCount) {
                lastSyncCount = record.syncCount;
              }
            });
            callback(lastSyncCount);
          });
        }
      });
    };

    var BlurStoreApi = abineCore.BaseClass.extend({
      initialize: function() {
        this.tables = {};
        this.synchronize = _.debounce(_.bind(this._synchronize, this), 1000);
      },
      connect: function() {
        if (this._pendingConnect) return;
        this._connected = false;
        this._pendingConnect = true;
        modules.licenseModule.retrieveSyncstoreToken(_.bind(function(err, response){
          if (err || response.manager != 'blur') {
            this._pendingConnect = false;
            return;
          }

          var params = {
            AccountId: response.account_id,
            IdentityPoolId: response.identity_pool_id,
            RoleArn: response.role_arn,
            WebIdentityToken: response.token,
            RoleSessionName: 'extension'
          };
          AWS.config.region = response.region;
          AWS.config.correctClockSkew = true;
          AWS.config.credentials = new AWS.WebIdentityCredentials(params);

          if (this.datasetPollTimer) {
            timer.clearInterval(this.datasetPollTimer)
          }

          LOG_FINEST && ABINE_DNTME.log.finest('got blur keys');
          AWS.config.credentials.get(_.bind(function() {
            AWS.config.credentials.identityId = response.identity_id;

            this.client = new AWS.CognitoSyncManager({log: logger});
            this.dataset = new MergedDataset(this.client);
            this._connected = true;
            this._pendingConnect = false;
            this.dataset.openOrCreateDataset('BlurData', _.bind(function (err, dataset) {
              LOG_FINEST && ABINE_DNTME.log.finest('opened blur dataset');
              this.synchronize();
              this.datasetPollTimer = timer.setInterval(_.bind(this._synchronize, this), POLL_TIME);
            }, this));
          }, this));
        }, this));
      },

      connectAndLoad: function() {
        if (this._connected) {
          return;
        }
        this.lastSyncCount = -1;
        this.connect();
      },

      disconnect: function() {
        if (this._connected) {
          LOG_FINEST && ABINE_DNTME.log.finest('disconnected from blurstore');
          timer.clearInterval(this.datasetPollTimer);
        }
        this.lastSyncCount = -1;
        this._connected = false;
        this.client = null;
        this.dataset = null;
      },

      addTable: function(name) {
        this.tables[name] = 1;
      },

      updateAll: function() {
        _.each(this.tables, _.bind(function(val, table){
          this.load(table);
        }, this));
      },

      load: function(name) {
        if (!this._connected) return;

        var Entity = storage[name], encryptionKey = null;
        if (Entity.meta.encrypted) {
          encryptionKey = securityManager.getEncryptionKey();
        }

        var decryptionFailed = false;
        var recordMap = {};
        var toEncrypt = [];
        var verifyKey = true;

        function decryptRecord(record) {
          if (Entity.meta.encrypted && !decryptionFailed) {
            var state = crypto.decryptObject(encryptionKey, record, Entity.meta.encryptedFields, verifyKey);
            verifyKey = false;
            if (state == 1) {
              decryptionFailed = true;
            } else if (state == 2) {
              toEncrypt.push(record);
            }
          }
          if (record.masked_email) {
            delete record.masked_email;
          }
        }

        this.dataset.getAllRecords(_.bind(function(err, records){
          var perf = null;
          LOG_FINEST && (perf = new Performance("loading "+name+" record from cognito"));
          _.each(records, _.bind(function(record){
            if (!record.value || record.value.length == 0 || record.key.indexOf(name+":") != 0) {
              return;
            }
            var key = record.key;
            try {
              record = JSON.parse(record.value)
            } catch(e){}

            if (!record || !record.id) {
						  this.dataset.remove(key, function(){});
              return;
            }

            recordMap[record.id] = record;
          }, this));

          Entity.index(null, _.bind(function(entities) {
            LOG_FINEST && ABINE_DNTME.log.finest('got '+entities.length+' records from local store');
            _.each(entities, function (entity) {
              if (decryptionFailed) return;

              var syncRecord = recordMap[entity.id];
              if (entity.pushed == 1) {
                if (syncRecord) {
                  if (entity.modifiedAt != syncRecord.modifiedAt || entity.createdAt != syncRecord.createdAt) {
                    decryptRecord(syncRecord);
                    if (!decryptionFailed) {
                      Entity.updateCache(entity, syncRecord);
                      var changed = false;
                      for (var p in entity._dirtyProperties) {
                        if (entity._dirtyProperties.hasOwnProperty(p)) {
                          changed = true;
                          break;
                        }
                      }
                      // ignore any pushed=false value from server as it causes infinite sync
                      entity.pushed = 1;
                      LOG_FINEST && changed && ABINE_DNTME.log.finest("updating " + JSON.stringify(entity));
                    }
                  }
                  delete recordMap[entity.id];
                } else {
                  LOG_FINEST && ABINE_DNTME.log.finest("removing "+JSON.stringify(entity));
                  persistence.remove(entity);
                }
              } else {
                if (syncRecord) {
                  if (entity.modifiedAt > syncRecord.modifiedAt) {
                    // use local
                    entity.markDirty('pushed');
                  } else {
                    // use remote
                    if (entity.modifiedAt != syncRecord.modifiedAt || entity.createdAt != syncRecord.createdAt) {
                      decryptRecord(syncRecord);
                      if (!decryptionFailed) {
                        Entity.updateCache(entity, syncRecord);
                        entity.pushed = 1;
                      }
                    }
                  }
                  delete recordMap[entity.id];
                } else {
                  entity.markDirty('pushed');
                }
              }
            });

            if (decryptionFailed) {
              LOG_DEBUG && ABINE_DNTME.log.debug("Decryption failed, will not load from blurstore" );
              return;
            }

            _.each(recordMap, function (record) {
              LOG_FINEST && ABINE_DNTME.log.finest("adding "+JSON.stringify(record));
              decryptRecord(record);
              var entity = new storage[name](record);
              entity._dirtyProperties = {};
              entity._new = false;
              entity._notInLocalStorage = true;
              persistence.add(entity);
            });
            persistence.flush();
            if (toEncrypt.length > 0) {
              _.each(toEncrypt, _.bind(function(record){
                record.pushed = 0
              }, this));
              this.save(name);
            }
            LOG_FINEST && perf.stop();
          }, this));
        }, this));
      },

      synchronizeWhenOld: function(sync_count, from) {
        if (this.dataset && sync_count && !isNaN(sync_count)) {
          if (this.lastSyncCount < sync_count) {
            LOG_FINEST && ABINE_DNTME.log.finest("** sync on event from webapp ", sync_count, this.lastSyncCount);
            if (from == 'push') {
              this._externalSync = true;
            }
            this.synchronize();
          } else {
            LOG_FINEST && ABINE_DNTME.log.finest("** skip sync on event from webapp ", sync_count, this.lastSyncCount);
          }
        }
      },

      canUpdateAll: function(callback) {
        var self = this;
        this.dataset.getAllRecords(function(err, allRecords) {
          var hasRemoteRecords = !!_.find(allRecords, function(record) {
            return record.value && record.value.length > 0;
          });
          if (!hasRemoteRecords) {
            var hasLocalData = false, canUpdate = false;
            _.async.forEachSeries(_.keys(self.tables), function(name, cb){
              if (!hasLocalData) {
                storage[name].index(null, function(entities){
                  if (entities.length > 0) {
                    hasLocalData = true;
                    modules.licenseModule.retrieveSyncstoreToken(function(err, response){
                      if (err || response.manager != 'blur') {
                        self.disconnect();
                        securityManager.refreshData();
                      } else {
                        canUpdate = true;
                      }
                      cb();
                    });
                  } else {
                    cb();
                  }
                });
              } else {
                cb();
              }
            }, function(){
              callback(canUpdate);
            });
          } else {
            callback(true);
          }
        });
      },

      _synchronize: function(callback) {
        var self = this;
        if (!this._connected) {
          return;
        }
        if (this.syncInProgress) {
          if (callback) {
            this._syncCallbacks = this._syncCallbacks || [];
            this._syncCallbacks.push(callback);
          }
          return;
        }
        function wrapCallbacks() {
          if (self._syncCallbacks) {
            var callbacks = self._syncCallbacks;
            self._syncCallbacks = null;
            _.each(callbacks, function(cb){
              try{cb();}catch(e){}
            });
          }
        }

        this.syncInProgress = true;
        return this.dataset.synchronize({
          resync: function() {
            self.synchronize();
          },
          onSuccess: function(dataset, newRecords) {
            LOG_FINEST && ABINE_DNTME.log.finest("data saved to the cloud and newRecords received");
            self.syncInProgress = false;
            self._reconnectAttempt = 0;
            self.canUpdateAll(function(update){
              if (!update) {
                wrapCallbacks();
                return;
              }
              self.updateAll();
              wrapCallbacks();
              ABINE_DNTME.require(['abine/pageEvents', 'abine/pushNotifications'], function(pageEvents, notifications){
                self.dataset.getCorrectSyncCount(function(lastSyncCount){
                  if (self.lastSyncCount && self.lastSyncCount < lastSyncCount) {
                    ABINE_DNTME.backgroundMessenger.sendToWebapp('abine:store:sync', lastSyncCount);
                    if (!self._externalSync) {
                      notifications.pushSync(lastSyncCount);
                    }
                    self._externalSync = false;
                  }
                  self.lastSyncCount = lastSyncCount;
                });
              });

            });
          },
          onFailure: function(err) {
            self.syncInProgress = false;
            if (err && (err.code === 'CredentialsError' || (err.message && err.message.match(/signature expired/i)))) {
              if (!self._reconnectAttempt) {
                self._reconnectAttempt = 0;
              }
              self._reconnectAttempt++;
              var delay = 0;
              if (self._reconnectAttempt > 1) {
                delay = Math.random() * 5 * 60 * 1000 + 5 * 60 * 1000;
              }
              LOG_FINEST && ABINE_DNTME.log.finest("credential error, attempting reconnect in " + delay + "ms : " + err);
              timer.setTimeout(function() {
                self.disconnect();
                self.connect();
              }, delay);
            } else {
              LOG_FINEST && ABINE_DNTME.log.finest("unknown error during sync: " + err);
            }
            wrapCallbacks();
          }
        });
      },

      save: function(name) {
        if (!this._connected) return;
        var Entity = storage[name], encryptionKey = null;
        if (Entity.meta.encrypted) {
          encryptionKey = securityManager.getEncryptionKey();
        }
        var changed = false;
        var self = this;
        var recordsToSave = {};
        var sendToWebapp = [];
        Entity.index(null, function (entities) {
          _.each(entities, function (entity) {
            if (entity.pushed !== 1) {
              entity.pushed = 1;
              changed = true;
              var obj = entity.toJSON();

              // send change to webapp
              sendToWebapp.push({name: name, obj: _.clone(obj)});

              if (Entity.meta.encrypted && encryptionKey) {
                crypto.encryptObject(encryptionKey, obj, Entity.meta.encryptedFields);
              }
              LOG_FINEST && ABINE_DNTME.log.finest("pushing "+JSON.stringify(obj));
              recordsToSave[name+":"+obj.id] = JSON.stringify(obj);
            }
          });
          if (changed) {
            persistence.flush();

            function saveToStore() {
              self.dataset.putAll(recordsToSave, function(){
                self.synchronize();
                _.each(sendToWebapp, function(data){
                  ABINE_DNTME.backgroundMessenger.sendToWebapp('update:entity', [data.name, data.obj]);
                });
              });
            }

            if (self.syncInProgress) {
              self._synchronize(saveToStore);
            } else {
              saveToStore();
            }
          }
        });
      },

      delete: function(name, id) {
        var self = this;
        if (self._connected){

          try{
            self.dataset.remove(name + ":" + id, function(err, record){
              self.synchronize();

              // send change to webapp
              ABINE_DNTME.backgroundMessenger.sendToWebapp('delete:entity', [name, id]);
            });
          } catch(e){
            LOG_FINEST && ABINE_DNTME.log.finest("exception deleting from blurstore");
          }
        }
      }

    });

    return new BlurStoreApi();
  }
);
ABINE_DNTME.define('abine/contextHelper',['documentcloud/underscore', 'abine/window', 'storage',
            'abine/password', 'abine/config', 'abine/securityManager', 'abine/webapp'],
  function(_, abineWindow, storage, abinePassword, config, securityManager, webapp){

  return {

    onClick: function(bgMessenger, id, tabId, domain, element){
      if (id.indexOf("abine_dntme_accountfield_")>=0) {
        this.fillLogin(id, tabId, bgMessenger, element);
      } else if (id.indexOf("abine_dntme_fillcard_")>=0) {
        this.fillCard(id, tabId, bgMessenger, element);
      } else if (id.indexOf("abine_dntme_address_")>=0) {
        this.fillAddress(id, tabId, bgMessenger, element);
      } else if (id.indexOf("abine_dntme_phone_mask_")>=0) {
        this.fillDiposablePhone(id, tabId, bgMessenger, element);
      } else if (id.indexOf("abine_dntme_phone_disclose_")>=0) {
        this.fillRealPhone(id, tabId, bgMessenger, element);
      } else {

        switch(id){
          case "abine_dntme_ignore":
            this.fillFieldInTab(bgMessenger,tabId,{
              element: element,
              value: null,
              dataType: "/ignore",
              origin: ""
            });
            break;

          case "abine_dntme_email_mask":
            this.fillFieldInTab(bgMessenger,tabId,{
              element: element,
              dataType: "/contact/email",
              origin: "disposable",
              generate: true,
              formType: "registrationForm"
            });
            break;

          case "abine_dntme_email_disclose":
            storage.ShieldedEmail.index(null, _.bind(function(shieldedEmails){
              this.fillFieldInTab(bgMessenger,tabId,{
                element: element,
                value: shieldedEmails[0].email,
                dataType: "/contact/email",
                origin: "",
                formType: "registrationForm"
              });
            },this));
            break;
          case "abine_dntme_card_mask":
            this.triggerCreditCardPanelInTab(bgMessenger, tabId, {});
            break;

          case "abine_dntme_activate_card":
            webapp.openCards();
            break;

          case "abine_dntme_password_mask":
            var generatedPassword = abinePassword.generateRandomPassword();
            this.fillFieldInTab(bgMessenger,tabId,{
              element: element,
              value: generatedPassword,
              origin: "strong",
              generatedNow: true, // used to distinguish between generated passwords NOW vs using account field that was once generated
              dataType: "/password",
              formType: "registrationForm"
            });
            var account = {
              domain: domain,
              password: generatedPassword
            };
            bgMessenger.send("background:save:account", account);
            break;
          case "abine_dntme_unlock":
            webapp.openLogin();
            break;
          case "abine_dntme_setup_phone":
            webapp.openPhones();
            break;
          case "abine_dntme_map_form":
            bgMessenger.send("background:contextmenu:mapform", {}, tabId);
            break;
          case "abine_dntme_autofill_form":
            bgMessenger.send("background:contextmenu:mapform", {}, tabId);
            break;
          case "abine_dntme_blacklisted":
            bgMessenger.send('background:dntme:show', {tabId: tabId}, tabId);
            break;
        }
      }
    },

    fillLogin: function(id, tabId, bgMessenger, element) {
      var id = id.split("accountfield_")[1];
      var type = id.split('_')[0];
      var accId = id.split('_')[1];
      storage.Account.load(accId, _.bind(function(account){
        if (!account) return;
        var dataType = '/password', value = account.password;
        if (type == 'email') {
          dataType = '/contact/email';
          value = account.email;
        } else if (type == 'username') {
          dataType = '/nameperson/friendly';
          value = account.username;
        }
        this.fillFieldInTab(bgMessenger, tabId, {
          element: element,
          value: value,
          dataType: dataType,
          origin: '',
          formType: 'loginForm'
        });
      },this));
    },

    fillCard: function(id, tabId, bgMessenger, element) {
      var id = id.split("fillcard_")[1];
      var type = id.split('_')[0];
      var cardId = id.split('_')[1];
      storage.Address.index(null, _.bind(function(addresses) {
        var addressMap = {};
        _.each(addresses, function (address) {
          addressMap[address.id] = address.toJSON();
        });
        storage.RealCard.load(cardId, _.bind(function (card) {
          if (!card) return;
          var dataType = '/financial/creditcard/number', value = card.number;
          if (type == 'cvc') {
            dataType = '/financial/creditcard/verification';
            value = card.cvc;
          } else if (type == 'name') {
            dataType = '/financial/creditcard/issuedto';
            value = card.getFullName();
          } else if (type == 'month') {
            dataType = '/financial/creditcard/expiry_month';
            value = card.getMM();
          } else if (type == 'year') {
            dataType = '/financial/creditcard/expiry_year';
            value = card.expiry_year;
          } else if (type == 'yy') {
            dataType = '/financial/creditcard/expiry_year2';
            value = card.getYY();
          } else if (type == 'mmyyyy') {
            dataType = '/financial/creditcard/expiry';
            value = card.getMMYYYY();
          } else if (type == 'mmyy') {
            dataType = '/financial/creditcard/expiry2';
            value = card.getMMYY();
          }

          this.fillFieldInTab(bgMessenger, tabId, {
            element: element,
            value: value,
            dataType: dataType,
            origin: '',
            formType: 'checkoutForm'
          });

          var cardJson = card.jsonWithAddress(addressMap);
          this.triggerAutoFillCreditCardPanelInTab(bgMessenger, tabId, cardJson);
        }, this));
      }, this));
    },

    fillAddress: function(id, tabId, bgMessenger, element) {
      var id = id.split("address_")[1];
      var type = id.split('_')[0];
      var addressId = id.split('_')[1];
      storage.Address.load(addressId, _.bind(function(address) {
        if (!address) return;

        var dataType = '/contact/postaladdress', value = address.address1;
        if (type == 'line2') {
          dataType = '/contact/postaladdressAdditional';
          value = address.address2;
        } else if (type == 'city') {
          dataType = '/contact/city';
          value = address.city;
        } else if (type == 'state') {
          dataType = '/contact/state';
          value = address.state;
        } else if (type == 'country') {
          dataType = '/contact/country';
          value = address.country;
        } else if (type == 'zip') {
          dataType = '/contact/postalcode';
          value = address.zip;
        }

        this.fillFieldInTab(bgMessenger, tabId, {
          element: element,
          value: value,
          dataType: dataType,
          origin: ''
        });

      }, this));
    },

    fillPhone: function (type, fieldInfo, phone, bgMessenger, tabId) {
      switch (type) {
        case "full":
          fieldInfo.value = phone.getPrettyNumber();
          fieldInfo.dataType = "/contact/phone";
          this.fillFieldInTab(bgMessenger, tabId, fieldInfo);
          break;
        case "area":
          fieldInfo.value = phone.number.substr(0, 3);
          fieldInfo.dataType = "/contact/phone/areacode";
          this.fillFieldInTab(bgMessenger, tabId, fieldInfo);
          break;
        case "exchange":
          fieldInfo.value = phone.number.substr(3, 3);
          fieldInfo.dataType = "/contact/phone/exchange";
          this.fillFieldInTab(bgMessenger, tabId, fieldInfo);
          break;
        case "local":
          fieldInfo.value = phone.number.substr(6, 4);
          fieldInfo.dataType = "/contact/phone/local";
          this.fillFieldInTab(bgMessenger, tabId, fieldInfo);
          break;
        case "last7":
          fieldInfo.value = phone.number.substr(3, 7);
          fieldInfo.dataType = "/contact/phone/number";
          this.fillFieldInTab(bgMessenger, tabId, fieldInfo);
          break;
      }
    },

    fillDiposablePhone: function(id, tabId, bgMessenger, element) {
      var type = id.split("mask_")[1];
      storage.DisposablePhone.index(null, _.bind(function(disposablePhones){
        var phone = disposablePhones[0];
        var fieldInfo = {
          element: element,
          origin: "disposable",
          disposableGUID: phone.id
        };
        this.fillPhone(type, fieldInfo, phone, bgMessenger, tabId);
      },this));
    },

    fillRealPhone: function(id, tabId, bgMessenger, element) {
      var type = id.split("disclose_")[1];
      storage.ShieldedPhone.index(null, _.bind(function(shieldedPhones){
        var phone = shieldedPhones[0];
        var fieldInfo = {
          element: element,
          origin: ""
        };
        this.fillPhone(type, fieldInfo, phone, bgMessenger, tabId);
      },this));
    },

    fillFieldInTab: function(bgMessenger, tabId, fieldInfo){
      bgMessenger.send("background:contextmenu:fill",fieldInfo,tabId);
    },
    triggerCreditCardPanelInTab: function(bgMessenger, tabId, fieldInfo){
      bgMessenger.send("background:contextmenu:maskcard", fieldInfo, tabId);
    },
    triggerAutoFillCreditCardPanelInTab: function(bgMessenger, tabId, fieldInfo){
      bgMessenger.send("background:contextmenu:fillcard", fieldInfo, tabId);
    }
  };

});
ABINE_DNTME.define("abine/crypto",
  ["asmCrypto", "blueimp-md5", "abine/perf"],
  function(asmCrypto, MD5, Performance) {

    /*!
    mnemonic.js : Converts between 4-byte aligned strings and a human-readable
    sequence of words. Uses 1626 common words taken from wikipedia article:
    http://en.wiktionary.org/wiki/Wiktionary:Frequency_lists/Contemporary_poetry
    Originally written in python special for Electrum (lightweight Bitcoin client).
    This version has been reimplemented in javascript and placed in public domain.

 Copyright (c) 2012 Yiorgis Gozadinos,
 Crypho AS, Harpunveien 3, 3126 Nøtterøy, Norway

 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.
*/


function mn_encode(str) {
    var out = [];
    var n = mn_words.length;
    for (var i = 0; i < str.length; i += 8) {
        var x = parseInt(str.substr(i, 8), 16);
        var w1 = (x % n);
        var w2 = (Math.floor(x/n) + w1) % n;
        var w3 = (Math.floor(Math.floor(x/n)/n) + w2) % n;
        out = out.concat([mn_words[w1], mn_words[w2], mn_words[w3]]);
    }
    return out.join(' ');
}

function mn_mod(a, b) {
    return a < 0 ? b + a : a % b;
}

function mn_decode(str) {
    var out = '';
    var n = mn_words.length;
    var wlist = str.split(' ');
    for (var i = 0; i < wlist.length; i += 3) {
        var w1 = mn_words.indexOf(wlist[i]);
        var w2 = (mn_words.indexOf(wlist[i+1])) % n;
        var w3 = (mn_words.indexOf(wlist[i+2])) % n;
        var x = w1 + n * mn_mod((w2 - w1), n) + n * n * mn_mod((w3 - w2), n);
        out += ('0000000' + x.toString(16)).slice(-8);
    }
    return out;
}

var mn_words = [
    "like", "just", "love", "know", "never", "want", "time", "out", "there",
    "make", "look", "eye", "down", "only", "think", "heart", "back", "then",
    "into", "about", "more", "away", "still", "them", "take", "thing", "even",
    "through", "long", "always", "world", "too", "friend", "tell", "try",
    "hand", "thought", "over", "here", "other", "need", "smile", "again",
    "much", "cry", "been", "night", "ever", "little", "said", "end", "some",
    "those", "around", "mind", "people", "girl", "leave", "dream", "left",
    "turn", "myself", "give", "nothing", "really", "off", "before",
    "something", "find", "walk", "wish", "good", "once", "place", "ask",
    "stop", "keep", "watch", "seem", "everything", "wait", "got", "yet",
    "made", "remember", "start", "alone", "run", "hope", "maybe", "believe",
    "body", "hate", "after", "close", "talk", "stand", "own", "each", "hurt",
    "help", "home", "god", "soul", "new", "many", "two", "inside", "should",
    "true", "first", "fear", "mean", "better", "play", "another", "gone",
    "change", "use", "wonder", "someone", "hair", "cold", "open", "best",
    "any", "behind", "happen", "water", "dark", "laugh", "stay", "forever",
    "name", "work", "show", "sky", "break", "came", "deep", "door", "put",
    "black", "together", "upon", "happy", "such", "great", "white", "matter",
    "fill", "past", "please", "burn", "cause", "enough", "touch", "moment",
    "soon", "voice", "scream", "anything", "stare", "sound", "red", "everyone",
    "hide", "kiss", "truth", "death", "beautiful", "mine", "blood", "broken",
    "very", "pass", "next", "forget", "tree", "wrong", "air", "mother",
    "understand", "lip", "hit", "wall", "memory", "sleep", "free", "high",
    "realize", "school", "might", "skin", "sweet", "perfect", "blue", "kill",
    "breath", "dance", "against", "fly", "between", "grow", "strong", "under",
    "listen", "bring", "sometimes", "speak", "pull", "person", "become",
    "family", "begin", "ground", "real", "small", "father", "sure", "feet",
    "rest", "young", "finally", "land", "across", "today", "different", "guy",
    "line", "fire", "reason", "reach", "second", "slowly", "write", "eat",
    "smell", "mouth", "step", "learn", "three", "floor", "promise", "breathe",
    "darkness", "push", "earth", "guess", "save", "song", "above", "along",
    "both", "color", "house", "almost", "sorry", "anymore", "brother", "okay",
    "dear", "game", "fade", "already", "apart", "warm", "beauty", "heard",
    "notice", "question", "shine", "began", "piece", "whole", "shadow",
    "secret", "street", "within", "finger", "point", "morning", "whisper",
    "child", "moon", "green", "story", "glass", "kid", "silence", "since",
    "soft", "yourself", "empty", "shall", "angel", "answer", "baby", "bright",
    "dad", "path", "worry", "hour", "drop", "follow", "power", "war", "half",
    "flow", "heaven", "act", "chance", "fact", "least", "tired", "children",
    "near", "quite", "afraid", "rise", "sea", "taste", "window", "cover",
    "nice", "trust", "lot", "sad", "cool", "force", "peace", "return", "blind",
    "easy", "ready", "roll", "rose", "drive", "held", "music", "beneath",
    "hang", "mom", "paint", "emotion", "quiet", "clear", "cloud", "few",
    "pretty", "bird", "outside", "paper", "picture", "front", "rock", "simple",
    "anyone", "meant", "reality", "road", "sense", "waste", "bit", "leaf",
    "thank", "happiness", "meet", "men", "smoke", "truly", "decide", "self",
    "age", "book", "form", "alive", "carry", "escape", "damn", "instead",
    "able", "ice", "minute", "throw", "catch", "leg", "ring", "course",
    "goodbye", "lead", "poem", "sick", "corner", "desire", "known", "problem",
    "remind", "shoulder", "suppose", "toward", "wave", "drink", "jump",
    "woman", "pretend", "sister", "week", "human", "joy", "crack", "grey",
    "pray", "surprise", "dry", "knee", "less", "search", "bleed", "caught",
    "clean", "embrace", "future", "king", "son", "sorrow", "chest", "hug",
    "remain", "sat", "worth", "blow", "daddy", "final", "parent", "tight",
    "also", "create", "lonely", "safe", "cross", "dress", "evil", "silent",
    "bone", "fate", "perhaps", "anger", "class", "scar", "snow", "tiny",
    "tonight", "continue", "control", "dog", "edge", "mirror", "month",
    "suddenly", "comfort", "given", "loud", "quickly", "gaze", "plan", "rush",
    "stone", "town", "battle", "ignore", "spirit", "stood", "stupid", "yours",
    "brown", "build", "dust", "hey", "kept", "pay", "phone", "twist",
    "although", "ball", "beyond", "hidden", "nose", "taken", "fail", "float",
    "pure", "somehow", "wash", "wrap", "angry", "cheek", "creature",
    "forgotten", "heat", "rip", "single", "space", "special", "weak",
    "whatever", "yell", "anyway", "blame", "job", "choose", "country", "curse",
    "drift", "echo", "figure", "grew", "laughter", "neck", "suffer", "worse",
    "yeah", "disappear", "foot", "forward", "knife", "mess", "somewhere",
    "stomach", "storm", "beg", "idea", "lift", "offer", "breeze", "field",
    "five", "often", "simply", "stuck", "win", "allow", "confuse", "enjoy",
    "except", "flower", "seek", "strength", "calm", "grin", "gun", "heavy",
    "hill", "large", "ocean", "shoe", "sigh", "straight", "summer", "tongue",
    "accept", "crazy", "everyday", "exist", "grass", "mistake", "sent", "shut",
    "surround", "table", "ache", "brain", "destroy", "heal", "nature", "shout",
    "sign", "stain", "choice", "doubt", "glance", "glow", "mountain", "queen",
    "stranger", "throat", "tomorrow", "city", "either", "fish", "flame",
    "rather", "shape", "spin", "spread", "ash", "distance", "finish", "image",
    "imagine", "important", "nobody", "shatter", "warmth", "became", "feed",
    "flesh", "funny", "lust", "shirt", "trouble", "yellow", "attention",
    "bare", "bite", "money", "protect", "amaze", "appear", "born", "choke",
    "completely", "daughter", "fresh", "friendship", "gentle", "probably",
    "six", "deserve", "expect", "grab", "middle", "nightmare", "river",
    "thousand", "weight", "worst", "wound", "barely", "bottle", "cream",
    "regret", "relationship", "stick", "test", "crush", "endless", "fault",
    "itself", "rule", "spill", "art", "circle", "join", "kick", "mask",
    "master", "passion", "quick", "raise", "smooth", "unless", "wander",
    "actually", "broke", "chair", "deal", "favorite", "gift", "note", "number",
    "sweat", "box", "chill", "clothes", "lady", "mark", "park", "poor",
    "sadness", "tie", "animal", "belong", "brush", "consume", "dawn", "forest",
    "innocent", "pen", "pride", "stream", "thick", "clay", "complete", "count",
    "draw", "faith", "press", "silver", "struggle", "surface", "taught",
    "teach", "wet", "bless", "chase", "climb", "enter", "letter", "melt",
    "metal", "movie", "stretch", "swing", "vision", "wife", "beside", "crash",
    "forgot", "guide", "haunt", "joke", "knock", "plant", "pour", "prove",
    "reveal", "steal", "stuff", "trip", "wood", "wrist", "bother", "bottom",
    "crawl", "crowd", "fix", "forgive", "frown", "grace", "loose", "lucky",
    "party", "release", "surely", "survive", "teacher", "gently", "grip",
    "speed", "suicide", "travel", "treat", "vein", "written", "cage", "chain",
    "conversation", "date", "enemy", "however", "interest", "million", "page",
    "pink", "proud", "sway", "themselves", "winter", "church", "cruel", "cup",
    "demon", "experience", "freedom", "pair", "pop", "purpose", "respect",
    "shoot", "softly", "state", "strange", "bar", "birth", "curl", "dirt",
    "excuse", "lord", "lovely", "monster", "order", "pack", "pants", "pool",
    "scene", "seven", "shame", "slide", "ugly", "among", "blade", "blonde",
    "closet", "creek", "deny", "drug", "eternity", "gain", "grade", "handle",
    "key", "linger", "pale", "prepare", "swallow", "swim", "tremble", "wheel",
    "won", "cast", "cigarette", "claim", "college", "direction", "dirty",
    "gather", "ghost", "hundred", "loss", "lung", "orange", "present", "swear",
    "swirl", "twice", "wild", "bitter", "blanket", "doctor", "everywhere",
    "flash", "grown", "knowledge", "numb", "pressure", "radio", "repeat",
    "ruin", "spend", "unknown", "buy", "clock", "devil", "early", "false",
    "fantasy", "pound", "precious", "refuse", "sheet", "teeth", "welcome",
    "add", "ahead", "block", "bury", "caress", "content", "depth", "despite",
    "distant", "marry", "purple", "threw", "whenever", "bomb", "dull",
    "easily", "grasp", "hospital", "innocence", "normal", "receive", "reply",
    "rhyme", "shade", "someday", "sword", "toe", "visit", "asleep", "bought",
    "center", "consider", "flat", "hero", "history", "ink", "insane", "muscle",
    "mystery", "pocket", "reflection", "shove", "silently", "smart", "soldier",
    "spot", "stress", "train", "type", "view", "whether", "bus", "energy",
    "explain", "holy", "hunger", "inch", "magic", "mix", "noise", "nowhere",
    "prayer", "presence", "shock", "snap", "spider", "study", "thunder",
    "trail", "admit", "agree", "bag", "bang", "bound", "butterfly", "cute",
    "exactly", "explode", "familiar", "fold", "further", "pierce", "reflect",
    "scent", "selfish", "sharp", "sink", "spring", "stumble", "universe",
    "weep", "women", "wonderful", "action", "ancient", "attempt", "avoid",
    "birthday", "branch", "chocolate", "core", "depress", "drunk",
    "especially", "focus", "fruit", "honest", "match", "palm", "perfectly",
    "pillow", "pity", "poison", "roar", "shift", "slightly", "thump", "truck",
    "tune", "twenty", "unable", "wipe", "wrote", "coat", "constant", "dinner",
    "drove", "egg", "eternal", "flight", "flood", "frame", "freak", "gasp",
    "glad", "hollow", "motion", "peer", "plastic", "root", "screen", "season",
    "sting", "strike", "team", "unlike", "victim", "volume", "warn", "weird",
    "attack", "await", "awake", "built", "charm", "crave", "despair", "fought",
    "grant", "grief", "horse", "limit", "message", "ripple", "sanity",
    "scatter", "serve", "split", "string", "trick", "annoy", "blur", "boat",
    "brave", "clearly", "cling", "connect", "fist", "forth", "imagination",
    "iron", "jock", "judge", "lesson", "milk", "misery", "nail", "naked",
    "ourselves", "poet", "possible", "princess", "sail", "size", "snake",
    "society", "stroke", "torture", "toss", "trace", "wise", "bloom", "bullet",
    "cell", "check", "cost", "darling", "during", "footstep", "fragile",
    "hallway", "hardly", "horizon", "invisible", "journey", "midnight", "mud",
    "nod", "pause", "relax", "shiver", "sudden", "value", "youth", "abuse",
    "admire", "blink", "breast", "bruise", "constantly", "couple", "creep",
    "curve", "difference", "dumb", "emptiness", "gotta", "honor", "plain",
    "planet", "recall", "rub", "ship", "slam", "soar", "somebody", "tightly",
    "weather", "adore", "approach", "bond", "bread", "burst", "candle",
    "coffee", "cousin", "crime", "desert", "flutter", "frozen", "grand",
    "heel", "hello", "language", "level", "movement", "pleasure", "powerful",
    "random", "rhythm", "settle", "silly", "slap", "sort", "spoken", "steel",
    "threaten", "tumble", "upset", "aside", "awkward", "bee", "blank", "board",
    "button", "card", "carefully", "complain", "crap", "deeply", "discover",
    "drag", "dread", "effort", "entire", "fairy", "giant", "gotten", "greet",
    "illusion", "jeans", "leap", "liquid", "march", "mend", "nervous", "nine",
    "replace", "rope", "spine", "stole", "terror", "accident", "apple",
    "balance", "boom", "childhood", "collect", "demand", "depression",
    "eventually", "faint", "glare", "goal", "group", "honey", "kitchen",
    "laid", "limb", "machine", "mere", "mold", "murder", "nerve", "painful",
    "poetry", "prince", "rabbit", "shelter", "shore", "shower", "soothe",
    "stair", "steady", "sunlight", "tangle", "tease", "treasure", "uncle",
    "begun", "bliss", "canvas", "cheer", "claw", "clutch", "commit", "crimson",
    "crystal", "delight", "doll", "existence", "express", "fog", "football",
    "gay", "goose", "guard", "hatred", "illuminate", "mass", "math", "mourn",
    "rich", "rough", "skip", "stir", "student", "style", "support", "thorn",
    "tough", "yard", "yearn", "yesterday", "advice", "appreciate", "autumn",
    "bank", "beam", "bowl", "capture", "carve", "collapse", "confusion",
    "creation", "dove", "feather", "girlfriend", "glory", "government",
    "harsh", "hop", "inner", "loser", "moonlight", "neighbor", "neither",
    "peach", "pig", "praise", "screw", "shield", "shimmer", "sneak", "stab",
    "subject", "throughout", "thrown", "tower", "twirl", "wow", "army",
    "arrive", "bathroom", "bump", "cease", "cookie", "couch", "courage", "dim",
    "guilt", "howl", "hum", "husband", "insult", "led", "lunch", "mock",
    "mostly", "natural", "nearly", "needle", "nerd", "peaceful", "perfection",
    "pile", "price", "remove", "roam", "sanctuary", "serious", "shiny",
    "shook", "sob", "stolen", "tap", "vain", "void", "warrior", "wrinkle",
    "affection", "apologize", "blossom", "bounce", "bridge", "cheap",
    "crumble", "decision", "descend", "desperately", "dig", "dot", "flip",
    "frighten", "heartbeat", "huge", "lazy", "lick", "odd", "opinion",
    "process", "puzzle", "quietly", "retreat", "score", "sentence", "separate",
    "situation", "skill", "soak", "square", "stray", "taint", "task", "tide",
    "underneath", "veil", "whistle", "anywhere", "bedroom", "bid", "bloody",
    "burden", "careful", "compare", "concern", "curtain", "decay", "defeat",
    "describe", "double", "dreamer", "driver", "dwell", "evening", "flare",
    "flicker", "grandma", "guitar", "harm", "horrible", "hungry", "indeed",
    "lace", "melody", "monkey", "nation", "object", "obviously", "rainbow",
    "salt", "scratch", "shown", "shy", "stage", "stun", "third", "tickle",
    "useless", "weakness", "worship", "worthless", "afternoon", "beard",
    "boyfriend", "bubble", "busy", "certain", "chin", "concrete", "desk",
    "diamond", "doom", "drawn", "due", "felicity", "freeze", "frost", "garden",
    "glide", "harmony", "hopefully", "hunt", "jealous", "lightning", "mama",
    "mercy", "peel", "physical", "position", "pulse", "punch", "quit", "rant",
    "respond", "salty", "sane", "satisfy", "savior", "sheep", "slept",
    "social", "sport", "tuck", "utter", "valley", "wolf", "aim", "alas",
    "alter", "arrow", "awaken", "beaten", "belief", "brand", "ceiling",
    "cheese", "clue", "confidence", "connection", "daily", "disguise", "eager",
    "erase", "essence", "everytime", "expression", "fan", "flag", "flirt",
    "foul", "fur", "giggle", "glorious", "ignorance", "law", "lifeless",
    "measure", "mighty", "muse", "north", "opposite", "paradise", "patience",
    "patient", "pencil", "petal", "plate", "ponder", "possibly", "practice",
    "slice", "spell", "stock", "strife", "strip", "suffocate", "suit",
    "tender", "tool", "trade", "velvet", "verse", "waist", "witch", "aunt",
    "bench", "bold", "cap", "certainly", "click", "companion", "creator",
    "dart", "delicate", "determine", "dish", "dragon", "drama", "drum", "dude",
    "everybody", "feast", "forehead", "former", "fright", "fully", "gas",
    "hook", "hurl", "invite", "juice", "manage", "moral", "possess", "raw",
    "rebel", "royal", "scale", "scary", "several", "slight", "stubborn",
    "swell", "talent", "tea", "terrible", "thread", "torment", "trickle",
    "usually", "vast", "violence", "weave", "acid", "agony", "ashamed", "awe",
    "belly", "blend", "blush", "character", "cheat", "common", "company",
    "coward", "creak", "danger", "deadly", "defense", "define", "depend",
    "desperate", "destination", "dew", "duck", "dusty", "embarrass", "engine",
    "example", "explore", "foe", "freely", "frustrate", "generation", "glove",
    "guilty", "health", "hurry", "idiot", "impossible", "inhale", "jaw",
    "kingdom", "mention", "mist", "moan", "mumble", "mutter", "observe", "ode",
    "pathetic", "pattern", "pie", "prefer", "puff", "rape", "rare", "revenge",
    "rude", "scrape", "spiral", "squeeze", "strain", "sunset", "suspend",
    "sympathy", "thigh", "throne", "total", "unseen", "weapon", "weary"
];


    /**
     * Encode multi-byte Unicode string into utf-8 multiple single-byte characters
     * (BMP / basic multilingual plane only)
     *
     * Chars in range U+0080 - U+07FF are encoded in 2 chars, U+0800 - U+FFFF in 3 chars
     *
     * @return encoded string
     */
    function encodeUTF8(input) {
      // use regular expressions & String.replace callback function for better efficiency
      // than procedural approaches
      var str = input.replace(
        /[\u0080-\u07ff]/g,  // U+0080 - U+07FF => 2 bytes 110yyyyy, 10zzzzzz
        function(c) {
          var cc = c.charCodeAt(0);
          return String.fromCharCode(0xc0 | cc>>6, 0x80 | cc&0x3f); }
      );
      str = str.replace(
        /[\u0800-\uffff]/g,  // U+0800 - U+FFFF => 3 bytes 1110xxxx, 10yyyyyy, 10zzzzzz
        function(c) {
          var cc = c.charCodeAt(0);
          return String.fromCharCode(0xe0 | cc>>12, 0x80 | cc>>6&0x3F, 0x80 | cc&0x3f); }
      );
      return str;
    }

    /**
     * Decode utf-8 encoded string back into multi-byte Unicode characters
     *
     * @return decoded string
     */
    function decodeUTF8(input) {
      var str = input.replace(
        /[\u00c0-\u00df][\u0080-\u00bf]/g,                 // 2-byte chars
        function(c) {
          var cc = (c.charCodeAt(0)&0x1f)<<6 | c.charCodeAt(1)&0x3f;
          return String.fromCharCode(cc); }
      );
      str = str.replace(
        /[\u00e0-\u00ef][\u0080-\u00bf][\u0080-\u00bf]/g,  // 3-byte chars
        function(c) {
          var cc = ((c.charCodeAt(0)&0x0f)<<12) | ((c.charCodeAt(1)&0x3f)<<6) | ( c.charCodeAt(2)&0x3f);
          return String.fromCharCode(cc); }
      );
      return str;
    }

    function convertFromHex(hexStr) {
      var hexStrLength = hexStr.length;

      // Convert
      var str = [];
      for (var i = 0; i < hexStrLength; i += 2) {
        str[str.length] = String.fromCharCode(parseInt(hexStr.substr(i, 2), 16));
      }

      return str.join("");
    }

    var crypto_wrapper = {
      MD5: function(input){
        return MD5(input);
      },
      SHA1: function(input){
        return asmCrypto.SHA1.hex(input);
      },
      SHA256: function(input){
        return asmCrypto.SHA256.hex(input);
      },
      random: function(bytes) {
        var output = new Uint8Array(bytes);
        asmCrypto.getRandomValues(output);
        return asmCrypto.bytes_to_hex(output);
      },
      // openssl based key and iv generation
      createKeyAndIV: function(password, salt) {
        var data00 = password + convertFromHex(salt);
        var hashtarget = '';
        var result = '';
        var keymaterial = [];
        var count = 1; //openssl rounds
        var miter = 3;
        var loop = 0, key, iv;
        keymaterial[loop++] = data00;

        for (var j = 0; j < miter; j++) {
          if (j == 0) {
            result = data00;
          } else {
            hashtarget = convertFromHex(result);
            hashtarget += data00;
            result = hashtarget;
          }
          for (var c = 0; c < count; c++) {
            result = md5(result);
          }
          keymaterial[loop++] = result;
        }
        key = asmCrypto.hex_to_bytes(keymaterial[1] + keymaterial[2]);
        iv = asmCrypto.hex_to_bytes(keymaterial[3]);
        return {key: key, iv: iv, salt: salt};
      },
      aes_encrypt: function(context, input) {
        LOG_FINEST && Performance.increment("encrypt");
        var key = context.key;
        var iv = context.iv;
        var blockSize = 16;
        var originalInput = input;

        try {
          var inputBytes = asmCrypto.string_to_bytes(encodeUTF8(input));

          var charDiv = blockSize - ((inputBytes.length + 1) % blockSize);
          var paddedInputBytes =  new Uint8Array(inputBytes.length+charDiv+1);
          paddedInputBytes.set(inputBytes);

          // PKCS5 paddding
          var idx = 0;
          paddedInputBytes[inputBytes.length+idx++] = 10;
          for (var c = 0; c < charDiv; c++) {
            paddedInputBytes[inputBytes.length+idx++] = charDiv;
          }

          return asmCrypto.bytes_to_base64(asmCrypto.AES_CBC.encrypt(paddedInputBytes, key, null, iv));
        } catch (e) {
          // any failure return original input
          return originalInput;
        }
      },
      aes_decrypt: function(context, input) {
        LOG_FINEST && Performance.increment("decrypt");
        var key = context.key;
        var iv = context.iv;
        var originalInput = input;

        try {
          var plainBytes = asmCrypto.AES_CBC.decrypt(atob(input), key, null, iv);

          // remove pkcs5 padding
          var length = plainBytes.length;
          plainBytes = plainBytes.slice(0, length-plainBytes[length-1]-1);

          var plainText = asmCrypto.bytes_to_string(plainBytes);
          return decodeUTF8(plainText);
        } catch (e) {
          // any failure return original input
          return originalInput;
        }
      }
    };

    // returns MD5 of input string
    function md5(input) {
      return crypto_wrapper.MD5(input);
    }

    // returns sha256 of input string
    function sha256(input) {
      return crypto_wrapper.SHA256(input);
    }

    function sha1(input){
      return crypto_wrapper.SHA1(input);
    }

    var keyIVCache = {};

    // creates key/iv for given password and salt
    // returns an object with key, iv and salt
    function createKeyAndIV(password, salt) {
      var cacheKey = (password + salt);
      if (!(cacheKey in keyIVCache)) {
        keyIVCache[cacheKey] = crypto_wrapper.createKeyAndIV(password, salt);
      }

      return keyIVCache[cacheKey];
    }

    // encrypt plain text using given key/iv
    function encrypt(context, input) {
      return crypto_wrapper.aes_encrypt(context, input);
    }

    // decrypt cipher text using given key/iv/salt
    function decrypt(context, cipherText) {
      return crypto_wrapper.aes_decrypt(context, cipherText);
    }

    function encryptKey(password, plain, salt) {
      return encrypt(createKeyAndIV(password, salt||sha256(password)), plain);
    }

    function decryptKey(password, cipher, salt, verifyKey) {
      var keyAndIV = createKeyAndIV(password, salt || sha256(password));
      var plain = decrypt(keyAndIV, cipher);
      if (verifyKey) {
        if (cipher != encrypt(keyAndIV, plain)) {
          return cipher;
        }
      }
      return  plain;
    }

    function _verifyKey(key, cipher, salt) {
      try {
        return (cipher != decryptKey(key, cipher, salt, true));
      } catch(e) {}
      return false;
    }

    function encryptObject(password, object, fields) {
      var salt = object.id;
      for (var i=0;i<fields.length;i++) {
        var plain = object[fields[i]];
        if (plain && plain.toString().indexOf('ENC_') != 0) {
          var cipher = encryptKey(password, plain, salt);
          if (cipher != plain) {
            object[fields[i]] = 'ENC_'+cipher;
          }
        }
      }
    }

    function decryptObject(password, object, fields, verifyKey) {
      var response = 0;
      var salt = object.id;
      for (var i=0;i<fields.length;i++) {
        var cipher = object[fields[i]];
        if (cipher && cipher.length > 0) {
          if (cipher.indexOf('ENC_') == 0) {
            cipher = cipher.substr(4);
            if (password) {
              if (verifyKey) {
                if (!_verifyKey(password, cipher, salt)) {
                  return 1; // decryption failed
                }
                verifyKey = false;
              }
              var plain = decryptKey(password, cipher, salt);
              if (plain != cipher) {
                object[fields[i]] = plain;
              } else {
                return 1; // decryption failed
              }
            } else {
              return 1; // decryption failed
            }
          } else if (password) {
            response = 2; // needs encryption
          }
        }
      }
      return response;
    }

    // create random salt 8 byte
    function salt() {
      return crypto_wrapper.random(8);
    }

    // create random N byte string
    function randomString(n) {
      var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz!@#$%^&*(){}[]|\\/<>?,.;~";
      var pass = '';
      for (var i=0; i<n; i++) {
        var rnum = Math.floor(Math.random() * chars.length);
        pass += chars.substring(rnum,rnum+1);
      }
      return pass;
    }

    // create random 16 byte string
    function generateKey() {
      return crypto_wrapper.random(16);
    }

    function getEncryptedPasswordHash(password) {
      var context = createKeyAndIV(password, password);
      return sha256(encrypt(context, password));
    }

    function getServerPassword(inputPassword){
      return getEncryptedPasswordHash(getEncryptedPasswordHash(inputPassword));
    }

    return {
      md5: md5,
      sha256: sha256,
      sha1: sha1,
      salt: salt,
      createKeyAndIV: createKeyAndIV,
      encrypt: encrypt,
      decrypt: decrypt,
      encryptKey: encryptKey,
      decryptKey: decryptKey,
      verifyKey: _verifyKey,
      encryptObject: encryptObject,
      decryptObject: decryptObject,
      randomString: randomString,
      generateKey: generateKey,
      encode_mnemonic: mn_encode,
      decode_mnemonic: mn_decode,
      getServerPassword: getServerPassword
    }

  }
);
ABINE_DNTME.define("abine/perf", [],
  function () {
    var recent = null;
    var Performance = function(msg) {
      if (!LOG_FINEST) return;
      recent = this;
      var metrics = {};
      var start = performance.now();
      this.stop = function () {
        if (!LOG_FINEST) return;
        console.log(msg, (performance.now() - start).toFixed(2) + " ms");
        for (var name in metrics) {
          console.log(name, "called", metrics[name]);
        }
        recent = null;
      };

      this.increment = function(name) {
        if (!LOG_FINEST) return;
        if (!(name in metrics)) {
          metrics[name] = 0;
        }
        metrics[name]++;
      };
    };

    Performance.stop = function(name) {
      if (recent) {
        recent.stop(name);
      }
    };

    Performance.increment = function(name) {
      if (recent) {
        recent.increment(name);
      }
    };

    return Performance;
  });
ABINE_DNTME.define("abine/dntmeInit",
  ['documentcloud/underscore', 'abine/dnt/api', 'abine/localStorage', 'abine/toolbar', 'storage',
    'abine/timer', 'persistence', 'abine/dntmeUpgrade', 'abine/badgeManager', 'modules',
    'abine/securityManager', 'abine/cobranding'
  ],
  function(_, API, localStorage, toolbar, storage,
           timer, persistence, dntmeUpgrade, badgeManager, modules,
           securityManager, brand
    ) {

  // override api browser to match browser in DNTMe (required only for opera as it uses chrome api)
  if (typeof(ABINE_DNT) != 'undefined') {
    ABINE_DNT.browser = ABINE_DNTME.config.browser.toLowerCase();
  }

  var ONE_DAY_MS = 24 * 60 * 60 * 1000, rulesCheckTimeout;

  var backgroundMessenger;

  var socialTrackers = {
    'Facebook Connect' : 1,
    'Google +1' : 1,
    'Twitter Badge' : 1,
    'LinkedIn' : 1
  };

  var allTimeTotalTrackers = 0, allTimeTotalSocialTrackers = 0, dailyTrackerCounts = null, badTrackers = null;
  var currentBadge = null;
  var currentBadgeIndex = -1;
  var dntStatsChanged = false;

  var totalBlockedInThisInstance = 0;

  var todayDate = function() {
    var today = new Date();
    today.setHours(0); today.setMinutes(0); today.setSeconds(0); today.setMilliseconds(0); // js dates suck
    return today;
  };


  function updateRules() {
    ABINE_DNTME.dnt_api.updateRules(brand.get("rules.update.url"), function(status, rules) {
      LOG_DEBUG && ABINE_DNTME.log.debug("rules status = "+status);
      storage.Preference.findBy('key', 'lastRulesCheck', function(lastRulesCheck){
        if (!lastRulesCheck) {
          lastRulesCheck = new storage.Preference({key: 'lastRulesCheck'});
          persistence.add(lastRulesCheck);
        }
        lastRulesCheck.value = Math.floor(todayDate().valueOf()); // why is this a real in Chrome db?
        persistence.flush();
      });
      if (status == "updated") {
        localStorage.setItem('dnt-rules', rules);
      } else {
        delayRulesCheck();
      }
    });
  }

  function delayRulesCheck(restarted) {
    if (restarted) {
      // wait for a minute
      var wait = 60 * 1000;
    } else {
      // wait between 30mins and 90mins
      var wait = (30 * 60 * 1000) + (60 * 60 * 1000 * Math.random());
    }
    if (rulesCheckTimeout) {
      timer.clearTimeout(rulesCheckTimeout);
    }
    LOG_INFO && ABINE_DNTME.log.info("delaying rules update check by "+(wait/60000)+" minutes");
    rulesCheckTimeout = timer.setTimeout(startAutoUpdate, wait);
  }

  function startAutoUpdate() {
    storage.Preference.findBy('key', 'lastRulesCheck', function(lastRulesCheck){
      if (lastRulesCheck && lastRulesCheck.value) {
        var lastCheckDate = new Date(parseInt(lastRulesCheck.value));
        if (lastCheckDate.valueOf() < ((new Date()).getTime() - ONE_DAY_MS) ) {
          LOG_DEBUG && ABINE_DNTME.log.debug("making daily rules check because its been 24 hours since last time");
          updateRules();
        } else {
          delayRulesCheck();
        }
      } else {
        LOG_INFO && ABINE_DNTME.log.info("never checked rules, checking now");
        updateRules();
      }
    });
  }

  function upgradeSettings(oldData, settings, callback) {
    if (oldData) {
      if ('trackerMap' in oldData) {
        if (!settings) {
          settings = {};
        }
        if (!('trackerSettings' in settings)) {
          settings['trackerSettings'] = {};
        }
        for (var host in oldData.trackerMap) {
          if (!(host in settings.trackerSettings)) {
            settings.trackerSettings[host] = {};
          }
          for (var tracker in oldData.trackerMap[host]) {
            if (settings.trackerSettings[host][tracker] != oldData.trackerMap[host][tracker]) {
              settings.trackerSettings[host][tracker] = !!oldData.trackerMap[host][tracker];
            }
          }
        }
      }

      if ('settings' in oldData) {
        if ('dntp.showmedals' in oldData.settings) {
          storage.Preference.createOrModify({key: "showMedals", value: oldData.settings['dntp.showmedals']?'true':'false'});
        }

        if ('dntp.useglobalallow' in oldData.settings) {
          settings.useSuggestions = oldData.settings['dntp.useglobalallow'];
        }

        if ('defaultBlocked' in oldData.settings) {
          settings.defaultBlocked = oldData.settings.defaultBlocked;
        }

        if ('origin' in oldData.settings && oldData.settings['origin'] && oldData.settings['origin'] != '') {
          storage.Preference.createOrModify({key: "dynamicTag", value: oldData.settings['origin']});
          ABINE_DNTME.config.tags.unshift(oldData.settings['origin']);
        }
        if ('install.date' in oldData.settings) {
          storage.Preference.createOrModify({key: "installDate", value: oldData.settings['install.date']});
        }
        if ('original.version' in oldData.settings) {
          storage.Preference.createOrModify({key: "installVersion", value: oldData.settings['original.version']});
        }
        if ('disabled.sites' in oldData.settings && oldData.settings['disabled.sites']) {
          var oldDisabledsites = JSON.parse(oldData.settings['disabled.sites']);
          if (!('disabledSites' in settings)) {
            settings.disabledSites = {};
          }
          for (var site in oldDisabledsites) {
            if (site in settings.trackerSettings)
              delete settings.trackerSettings[site];
            if (site in settings.disabledSites) continue;
            settings.disabledSites[site] = 1;
            storage.Blacklist.findBy('domain', site, function(blacklist){
              if (!blacklist) {
                blacklist = new storage.Blacklist({domain: site});
                persistence.add(blacklist);
              }
              blacklist.tracker = true;
            });
          }
        }
        if ('blocked.sites' in oldData.settings && oldData.settings['blocked.sites']) {
          var oldBlockedsites = JSON.parse(oldData.settings['blocked.sites']);
          if (!('blockedSites' in settings)) {
            settings.blockedSites = {};
          }
          for (var site in oldBlockedsites) {
            if (site in settings.trackerSettings)
              delete settings.trackerSettings[site];
            if (site in settings.blockedSites) continue;
            settings.blockedSites[site] = 1;
            storage.Blacklist.findBy('domain', site, function(blacklist){
              if (blacklist) {
                blacklist.tracker = false;
              }
            });
          }
        }
      }

      if ('totals' in oldData) {
        if (oldData.totals.total)
          allTimeTotalTrackers = oldData.totals.total;
        if (oldData.totals.facebook)
          allTimeTotalSocialTrackers += oldData.totals.facebook;
        if (oldData.totals.twitter)
          allTimeTotalSocialTrackers += oldData.totals.twitter;
        if (oldData.totals.google)
          allTimeTotalSocialTrackers += oldData.totals.google;
        if (oldData.totals.linkedin)
          allTimeTotalSocialTrackers += oldData.totals.linkedin;
      }

      if ('dailyTotals' in oldData) {
        dailyTrackerCounts = oldData['dailyTotals'];
      }

      if ('badTrackers' in oldData) {
        badTrackers = oldData['badTrackers'];
      }

      dntStatsChanged = true;
      _saveDntStats();
      localStorage.setItem('dnt-settings', JSON.stringify(settings));
      persistence.flush();

    }
    callback(settings);
  }

  // compute badge with 3 seconds debounce for better performace
  var computeBadge = _.debounce(function() {
    var badgeData = badgeManager.compute(allTimeTotalTrackers);

    // this code should be in badgeManager
    if (badgeData.badgeIndex != -1 && badgeData.badgeIndex != currentBadgeIndex) {
      var badgesEarned = badgeData.badgeIndex - currentBadgeIndex;
      if(badgesEarned > 0){ // could badges earned ever actually be negative?
        storage.Preference.createOrIncrementByInteger({key:'cardsEarned', value: badgesEarned}, function(){}); // local approximation of how many cards user has earned. assumes 1 to 1 medal to cards!
         if(securityManager.hasPassword()){
           modules.licenseModule.incrementMedalsEarned(badgesEarned, function(){}); // empty callback; let's just ignore errors
         }
      }
      currentBadgeIndex = badgeData.badgeIndex;
      currentBadge = badgeData.badge;
      storage.DntStats.index(null, function(dntStats) {
        var dntStats = dntStats[0];
        dntStats.viewedBadge = false;
        dntStats.earnedOn = Date.now();
        dntStats.currentBadge = JSON.stringify(currentBadge);
        dntStats.currentBadgeIndex = currentBadgeIndex;
        persistence.flush();
      });
    }
  }, 3000);

  function pushTrackerData(tabData) {
    if (tabData && tabData.host) {
      var homePage = !!tabData.url.match(/^http[s]?:\/\/[^\/]+[\/]?((index|home)(\.[a-z]+)?)?([\?\#].*)?$/);
      var trackers = [];
      _.each(tabData.trackers, function(tracker){
        trackers.push(tracker.name+":"+(tracker.blocked?1:0))
      });
      storage.TrackerData.createLocal({
        payload: {t: trackers.join('|'), d: tabData.host,  h: homePage ? 1 : 0}
      });
    }
  }

  function trackerFound(tabId, ruleName, isBlocked, newTracker, allowedByTopDomain, url, badTracker){
    /*  RRJ - Blur 6.6 hotfix: disable pushing tracker data from clients
    if (newTracker) {
      try {
        var data = ABINE_DNTME.dnt_api.getTabData(tabId);
        pushTrackerData(data);
      } catch(e){}
    }
    */
    if (newTracker && isBlocked && dailyTrackerCounts) {
      var today = new Date();
      var thisDay = toDayOfYear(today)+"";
      var year = today.getFullYear();
      allTimeTotalTrackers++;
      totalBlockedInThisInstance++;
      if (ruleName in socialTrackers) {
        allTimeTotalSocialTrackers++;
      }
      var dailyStats = {};
      if (!(year in dailyTrackerCounts)) {
        dailyStats[thisDay] = allTimeTotalTrackers;
        dailyTrackerCounts[year] = dailyStats;
      } else {
        dailyStats = dailyTrackerCounts[year];
        if (!(thisDay in dailyStats)) {
          dailyStats[thisDay] = allTimeTotalTrackers;
        } else {
          dailyStats[thisDay] = dailyStats[thisDay] + 1;
        }
      }
      if (badTracker) {
        if (!('totals' in badTrackers)) {
          badTrackers.totals = {};
        }
        if (!(ruleName in badTrackers.totals)) {
          badTrackers.totals[ruleName] = 1;
        } else {
          badTrackers.totals[ruleName]++;
        }

        if (!('daily' in badTrackers)) {
          badTrackers.daily = {};
        }
        var dailyBadTracker = ruleName + '.' + year;
        if (!(dailyBadTracker in badTrackers.daily)) {
          badTrackers.daily[dailyBadTracker] = {};
        }
        if (!(thisDay in badTrackers.daily[dailyBadTracker])) {
          badTrackers.daily[dailyBadTracker][thisDay] = 1;
        } else {
          badTrackers.daily[dailyBadTracker][thisDay]++;
        }

      }

      computeBadge();

      dntStatsChanged = true;
      saveDntStats();
    }
  }

  function updateToolbarIcon(tabId, disabledHere, num_trackers, num_blocked, num_suggested, host, reloadRequired) {
    var iconData = {tabId: tabId, disabled: disabledHere, total: num_trackers, blocked: num_blocked, suggested: num_suggested, host: host, reloadRequired: reloadRequired};
    backgroundMessenger.send('background:dntme:tracker-count-changed', iconData, tabId);

    storage.Preference.findBy('key', 'showMedals', function(showMedals){
      storage.DntStats.index(null, function(dntStats) {
        if (!showMedals || showMedals.value == 'true') {
          var stats;
          if ((dntStats) && dntStats.length) {
            stats = dntStats[0];
          }

          if (stats.hasOwnProperty('viewedBadge') && !stats.viewedBadge) {
            iconData.isBadge = true;
            iconData.iconPath = currentBadge.icon;
            if (!stats.earnedOn) {
              stats.earnedOn = Date.now();
              persistence.flush();
            }
          }
        }

        if (!iconData.isBadge) {
          modules.panelsModule.getToolbarPanel(iconData, function(iconData){
            toolbar.updateIcon(iconData);
          });
        } else {
          toolbar.updateIcon(iconData);
        }
      });
    });
  }

  function toDayOfYear(dt) {
    dt = dt || new Date();
    var d = new Date(dt.getFullYear(), 0, 0);
    return Math.floor((dt-d)/8.64e+7);
  }

  function _saveDntStats(){
    if (!dntStatsChanged) return;
    dntStatsChanged = false;
    storage.DntStats.index(null, function(dntStats) {
      var dntStats = dntStats[0];
      dntStats.totalAllTimeTrackers = allTimeTotalTrackers;
      dntStats.totalAllTimeSocialTrackers = allTimeTotalSocialTrackers;
      dntStats.dailyTrackerCounts = JSON.stringify(dailyTrackerCounts);
      dntStats.badTrackers = JSON.stringify(badTrackers);
      dntStats.currentBadge = JSON.stringify(currentBadge);
      dntStats.currentBadgeIndex = currentBadgeIndex;
      persistence.flush();
    });
  }

  var saveDntStats = _.debounce(_saveDntStats, 5000);

  function initialize(backgroundMessenger){
    var settings = localStorage.getItem('dnt-settings');
    var rules = localStorage.getItem('dnt-rules');

    if (settings) {
      try {
        settings = JSON.parse(settings);
      } catch(e) {
        LOG_ERROR && ABINE_DNTME.log.error("Corrupted settings file");
      }
    }
    if (rules) {
      try {
        rules = JSON.parse(rules);
      } catch (e) {
        LOG_ERROR && ABINE_DNTME.log.error("Corrupted rules file");
      }
    }

    storage.DntStats.index(null, function(dntStats) {
      var dntStats, changed = false;
      if ((!dntStats) || (dntStats && !dntStats.length)) {
        dntStats = new storage.DntStats({
          totalAllTimeTrackers: 0,
          totalAllTimeSocialTrackers: 0,
          dailyTrackerCounts: '{}',
          badTrackers: '{}',
          viewedBadge: true,
          currentBadge: '{}',
          currentBadgeIndex: -1
        });
        persistence.add(dntStats);
        changed = true;
      } else {
        dntStats = dntStats[0];
      }

      if (!dntStats.badTrackers || dntStats.badTrackers == '') {
        dntStats.badTrackers = '{}';
        changed = true;
      }

      allTimeTotalSocialTrackers = dntStats.totalAllTimeSocialTrackers||0;
      allTimeTotalTrackers = dntStats.totalAllTimeTrackers;
      dailyTrackerCounts = JSON.parse(dntStats.dailyTrackerCounts);
      badTrackers = JSON.parse(dntStats.badTrackers);
      currentBadge = JSON.parse(dntStats.currentBadge);
      currentBadgeIndex = dntStats.currentBadgeIndex;
      if (changed) {
        persistence.flush();
      }
    });

    dntmeUpgrade.getOldData(function(oldData){
      if (oldData && 'trackerMap' in oldData) {
        ABINE_DNTME.dntme_upgrade = true;
      }
      function initDnt(settings){
        var dnt_api = new API(settings, rules);
        ABINE_DNTME.dnt_api = dnt_api;

        dnt_api.forceSaveStats = function() {
          _saveDntStats();
        };

        dnt_api.getAllTimeTotalTrackers = function() {
          return allTimeTotalTrackers;
        };

        dnt_api.addListener('config-changed', function(settings){
          localStorage.setItem('dnt-settings', JSON.stringify(settings));
        });

        dnt_api.addListener('update-count', updateToolbarIcon);

        dnt_api.addListener('tracker-found', trackerFound);

        backgroundMessenger.on('contentManager:init', function(payload, sender){
          dnt_api.triggerUpdateIcon(sender);
          if (payload.url.indexOf('dntmeTour=') != -1 || payload.url.indexOf('dntmeFullTour=') != -1) {
            ABINE_DNTME.require(['abine/tour'], function(tour){
              tour.show(sender);
            });
          }
        });

        backgroundMessenger.on('toolbar:clicked', function(payload) {
          var tabId = payload.tabId;
          if (!modules.panelsModule.doToolbarPanelAction(tabId)) {
            backgroundMessenger.send('background:dntme:show', {tabId: tabId, page: 'dashboard'}, tabId);
          }
            modules.licenseModule.logAction({user_action:"clicked icon"});
        });

        delayRulesCheck(true);
      };
      try {
        upgradeSettings(oldData, settings, initDnt);
      } catch(e) {
        LOG_ERROR && ABINE_DNTME.log.error("*** error during upgrade from 2.0\n"+e+"\n"+ e.stack);
        initDnt(settings);
      }
    });
  }
    return {
      initialize: function(bgMessenger){
        backgroundMessenger = bgMessenger;
        if (!ABINE_DNTME.dnt_api) {
          initialize(backgroundMessenger);
        }
        securityManager.on('background:securityManager:unlocked', function(){ // update server with # of medals on first auth token
          storage.Preference.findBy('key', 'handledFirstAuth', function(handledFirstAuthPref) {
            if (!handledFirstAuthPref) {
              storage.Preference.createOrModify({key:'handledFirstAuth', value: true}, function(){
                storage.Preference.findBy('key', 'cardsEarned', function(pref){ // rob/hilario say we want old users to have cards earned
                  var shouldIncrementCardsEarned = true;
                  if (pref) {
                    shouldIncrementCardsEarned = false;
                  }
                  modules.licenseModule.incrementMedalsEarnedWithCurrentMedals(shouldIncrementCardsEarned, function(){
                    // empty callback; how to handle errors?
                  });
                });
              });
            }
          });
        });
      },
      getTotalBlockedInThisInstance: function(){return totalBlockedInThisInstance;},
      updateRules: updateRules
    };
});
ABINE_DNTME.define("abine/emailServer",
    ['documentcloud/underscore', 'abine/ajax', 'abine/config', 'abine/server', 'abine/core'],
    function (_, abineAjax, config, abineServer, abineCore) {

      var HOST = config.protocol + config.emailServerHost;
      var READ_DISPOSABLES_URL = HOST + "/api/v3/disposables";
      var GENERATE_DISPOSABLE_URL = HOST + "/api/v3/disposables";

      var EmailServer = abineCore.BaseClass.extend({
        initialize: function(options){
          this.options = this.options || options || {};
          this.server = this.options.server || new abineServer.server();

        },

        readDisposables: function(data, callback){
          this.server.makeRequest({
            server: 'email',
            action: 'readDisposables',
            defaults: {},
            required: [],
            success: function(serverData, httpStatus){
              callback.call(this,null,serverData);
            },
            error: function(httpStatus, errorCode, responseJSON){
              callback.call(this,{status:httpStatus, errorCode: errorCode, responseJSON: responseJSON});
            },
            data: data,
            url: READ_DISPOSABLES_URL,
            type: "GET"
          });
        },

        generateDisposable: function(data, callback){
          this.server.makeRequest({
            server: 'email',
            action: 'generateDisposable',
            defaults: {domain: config.disposableDomain},
            required: ['domain', 'label'],
            success: function(serverData, httpStatus){
              callback.call(this,null,serverData);
            },
            error: function(httpStatus, errorCode, responseJSON){
              callback.call(this,{status:httpStatus, errorCode: errorCode, responseJSON: responseJSON});
            },
            data: data,
            url: GENERATE_DISPOSABLE_URL,
            type: "POST"
          });
        }

      });

      return EmailServer;
    });
ABINE_DNTME.define("abine/eventLogger", ['abine/config'], function (config) {

  var FINEST = 0
  var DEBUG = 1;
  var INFO = 2;
  var WARN = 3;
  var ERROR = 4;
  var NONE = 5;

  var noOp = function(){};

  function errorStringToJSON(errorString){
    var error = {}; // JSON representation

    var errorArray = errorString && errorString.split? errorString.split("#"):[];

    // parse the error string [ERROR HANDLER]#[abine/models/user]#[loadShieldedPhones]# error event in this.shieldedPhones.fetchAll
    if(errorArray.length == 4){
      error.error_tag = errorArray[0];
      error.module = errorArray[1];
      error.method = errorArray[2];
      error.message = errorArray[3];
    } else {
      error.error_tag = error.module = error.method = "not well-formed";
      // convert input to string. without this you will get infinite recursion of buildParams
      // inside jquery.ajax when input is a DOM object
      error.message = errorString+"";
    }

    // get information from static config options
    error.browser = config.browser;
    error.build = config.buildNum;
    error.environment = config.environment;
    error.build_tag = config.tags[0];
    error.server_host = config.serverHost;
    error.mapping_server_host = config.mappingServerHost;
    error.license_server_host = config.licenseServerHost;
    error.phone_server_host = config.phoneServerHost;
    error.crypt = config.crypt;
    error.version = config.version;

    // get the user agent string to ID the browser version
    error.user_agent = ABINE_DNTME.userAgent;

    return error;
  }

  function getConsoleService() {
    if (typeof console == "undefined" || typeof(console.debug) == 'undefined' || ABINE_DNTME.IE_BG) {
      function ConsoleWrap(msg) {
        try{Console(msg);}catch(e){}
      }
      return {
        finest: function(msg) { ConsoleWrap(ABINE_DNTME.context+" [FINEST][" + new Date() + "]: " + msg); },
        debug:  function(msg) { ConsoleWrap(ABINE_DNTME.context+" [DEBUG][" + new Date() + "]: "  + msg);   },
        info:   function(msg) { ConsoleWrap(ABINE_DNTME.context+" [INFO][" + new Date() + "]: "  + msg);    },
        warn:   function(msg) { ConsoleWrap(ABINE_DNTME.context+" [WARNING][" + new Date() + "]: "  + msg); },
        error:  function(msg) { ConsoleWrap(ABINE_DNTME.context+" [ERROR][" + new Date() + "]: "  + msg); }
      }
    } else if (typeof console != "undefined") {
      return {
        finest: function(msg) { console.debug(ABINE_DNTME.context+" [FINEST][" + new Date() + "]: " + msg); },
        debug:  function(msg) { console.debug(ABINE_DNTME.context+" [DEBUG][" + new Date() + "]: "  + msg);   },
        info:   function(msg) { console.info(ABINE_DNTME.context+" [INFO][" + new Date() + "]: "  + msg);    },
        warn:   function(msg) { console.warn(ABINE_DNTME.context+" [WARNING][" + new Date() + "]: "  + msg); },
        error:  function(msg) { console.error(ABINE_DNTME.context+" [ERROR][" + new Date() + "]: "  + msg); }
      }
    }
  }

  var EventLogger = getConsoleService();

  EventLogger.dumpObj = function(obj){ // http://www.sitepoint.com/javascript-json-serialization/ - thanks, bro.
    var t = typeof (obj);
    if (t != "object" || obj === null) {
      if (t == "string") obj = '"'+obj+'"';
      return String(obj);
    }
    else {
      var n, v, json = [], arr = (obj && obj.constructor == Array);
      for (n in obj) {
        v = obj[n]; t = typeof(v);
        if (t == "string") v = '"'+v+'"';
        else if (t == "object" && v !== null) v = EventLogger.dumpObj(v);
        json.push((arr ? "" : '"' + n + '":') + String(v));
      }
      return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
    }
  };

  // intentionally not using BREAK in case statement to let it disable all logging below given level.
  switch (config.logLevel) {
    case "NONE": EventLogger.error = noOp;
    case "ERROR": EventLogger.warn = noOp;
    case "WARN": EventLogger.info = noOp;
    case "INFO": EventLogger.debug = noOp;
    case "DEBUG":EventLogger.finest = noOp;
  }

  EventLogger.stackTrace = function(msg) {
    msg = msg || '';
    try {does.not=exist}catch(e) {
      var stack = (e.stack+"").split('\n');
      if (stack.length > 3) {
        stack.shift();
        stack.shift();
        stack.shift();
      }
      msg += "\n" + stack.join('\n');
    }
    this.debug(msg);
  };

  // for easy access in all places
  ABINE_DNTME.log = ABINE_DNTME.log || EventLogger;

  return EventLogger;

});
ABINE_DNTME.define('abine/formSubmitDetector',
  ['documentcloud/underscore', 'abine/core', 'abine/eventLogger'],
  function(_, abineCore, log) {

    // class for background script
    var FormSubmitDetector = abineCore.BaseClass.extend({

      init: function(backgroundMessenger, onBeforeRequest) {
        LOG_FINEST && log.finest("formSubmitDetector:init enter");
        var formSubmitDetector = this;

        backgroundMessenger.on('contentManager:formSubmit', function(data, sender){
          LOG_FINEST && log.finest("formSubmitDetector:init heard contentManager:formSubmit");
          formSubmitDetector.trigger('background:formSubmit',{formData:data, tabId: sender});
        });

        backgroundMessenger.on('contentManager:formFieldChanged', function(data,sender){
          LOG_FINEST && log.finest("formSubmitDetector:init heard contentManager:formFieldChanged");
          formSubmitDetector.trigger('background:formFieldChanged',{formData:data.formData, fieldData:data.fieldData, tabId: sender});
        });

        backgroundMessenger.on('contentManager:formFieldChanged:processMapping', function(data,sender){
          LOG_FINEST && log.finest("formSubmitDetector:init heard contentManager:formFieldChanged:processMapping");
          formSubmitDetector.trigger('background:formFieldChanged:processMapping',{formData:data.formData, fieldData:data.fieldData, checkoutButtons: data.checkoutButtons, tabId: sender});
        });

        backgroundMessenger.on('contentManager:formPaymentFieldChanged', function(data,sender){
          LOG_FINEST && log.finest("formSubmitDetector:init heard contentManager:formPaymentFieldChanged");
          formSubmitDetector.trigger('background:formPaymentFieldChanged',{formData:data.formData, fieldData:data.fieldData, tabId: sender});
        });

        backgroundMessenger.on('panel:fill', function(data, sender){
          LOG_FINEST && log.finest("formSubmitDetector:init heard panel:fill");
          formSubmitDetector.trigger('background:formFieldChanged',{formData:data.formData, fieldData:data.fieldData, tabId: sender});
          if (data.formData.number) {
            // its a payment form, so save card
            formSubmitDetector.trigger('background:formPaymentFieldChanged',{formData:data.formData, fieldData:data.fieldData, tabId: sender});
          }
        });

        LOG_FINEST && log.finest("formSubmitDetector:init exit");
      }
    });


    return {
      formSubmitDetector: new FormSubmitDetector()
    };
  });
ABINE_DNTME.define("abine/licenseServer",
  ['documentcloud/underscore', 'abine/ajax', 'abine/config', 'abine/server', 'abine/core', 'abine/responseCodes'],
  function (_, abineAjax, config, abineServer, abineCore, responseCodes) {

    var HOST = config.protocol + config.licenseServerHost;
    var USERS_URL = HOST + "/api/v2/users";
    var SESSION_URL = HOST + "/api/v2/sessions";
    var TOUCHES_URL = HOST + "/api/v2/touches";
    var BILLING_INFO_URL = HOST + "/api/v2/billing_info";
    var FEEDBACK_URL = HOST + "/api/v2/feedback_email";
    var PANEL_DATA_URL = HOST + "/api/v2/panel_data";
    var SIGNATURE_URL = HOST + "/api/v3/signatures";
    var SUBSCRIPTIONS_URL = HOST + "/api/v3/subscriptions";
    var STRIPE_SUBSCRIPTIONS_URL = HOST + "/api/v3/stripe_subscriptions";
    var MEDALS_URL = HOST + "/api/v3/medals";
    var LOG_ACTION_URL = HOST + "/api/v2/log_action";
    var LOG_SPLIT_DATA_URL = HOST + "/api/v2/log_split_data";
    var LOG_PASSWORD_FILL_DATA_URL = HOST + "/api/v2/log_password_fill_data";
    var LOG_CHECKOUT_URL = HOST + "/api/v3/checkout";

    var PUSH_SYNC_URL = HOST + "/api/v3/push/sync";

    var COMPANY_ACCOUNTS_URL = HOST + "/api/v3/company/accounts";

    var SYNCSTORE_TOKEN_URL = HOST + "/api/v3/syncstore";
    var DEVICES_URL = HOST + "/api/v2/devices";

    var BIOMETRIC_REQUEST_URL = HOST + "/api/v3/mfa/request";
    var PASSWORD_AUTH_URL = HOST + "/api/v3/mfa/token";
    var GENERIC_KEY_URL = HOST + "/api/v3/mfa/generic_key";

    var LicenseServer = abineCore.BaseClass.extend({
      initialize: function(options){
        this.options = this.options || options || {};
        this.server = this.options.server || new abineServer.server();
      },

      logAction: function(data, callback){
        this.server.makeRequest({
          server: 'license',
          action: 'logAction',
          defaults: {},
          required: ['user_action'],
          success: function(serverData, httpStatus){
            callback.call(this,null,serverData);
          },
          error: function(httpStatus, errorCode, responseJSON){
            callback.call(this,{status:httpStatus, errorCode: errorCode, responseJSON: responseJSON});
          },
          data: data,
          url: LOG_ACTION_URL,
          type: "POST"
        });
      },

      logSplitData: function(data, callback){
        this.server.makeRequest({
          server: 'license',
          action: 'logSplitData',
          defaults: {},
          required: ['bucket_id'],
          success: function(serverData, httpStatus){
            callback.call(this,null,serverData);
          },
          error: function(httpStatus, errorCode, responseJSON){
            callback.call(this,{status:httpStatus, errorCode: errorCode, responseJSON: responseJSON});
          },
          data: data,
          url: LOG_SPLIT_DATA_URL,
          type: "POST"
        });
      },

      logPasswordFillData: function(data, callback){
        this.server.makeRequest({
          server: 'license',
          action: 'logPasswordFillData',
          defaults: {},
          required: ['choice','domain'],
          success: function(serverData, httpStatus){
            callback.call(this,null,serverData);
          },
          error: function(httpStatus, errorCode, responseJSON){
            callback.call(this,{status:httpStatus, errorCode: errorCode, responseJSON: responseJSON});
          },
          data: data,
          url: LOG_PASSWORD_FILL_DATA_URL,
          type: "POST"
        });
      },

      sendPanelActivity: function(data, callback){
        this.server.makeRequest({
          server: 'license',
          action: 'sendPanelActivity',
          defaults: {},
          required: ['panel_data'],
          success: function(serverData, httpStatus){
            callback.call(this,null,serverData);
          },
          error: function(httpStatus, errorCode, responseJSON){
            callback.call(this,{status:httpStatus, errorCode: errorCode, responseJSON: responseJSON});
          },
          data: data,
          url: PANEL_DATA_URL,
          type: "POST"
        });
      },

      login: function(data, callback){
        this.server.makeRequest({
          server: 'license',
          action: 'login',
          defaults: {},
          required: ['user_login', 'application'],
          success: function(serverData, httpStatus){
            callback.call(this,null,serverData);
          },
          error: function(httpStatus, errorCode, responseJSON){
            callback.call(this,{status:httpStatus, errorCode: errorCode, responseJSON: responseJSON});
          },
          data: data,
          url: SESSION_URL,
          type: "POST"
        });
      },

      logout: function(data, callback){
        this.server.makeRequest({
          server: 'license',
          action: 'logout',
          defaults: {},
          required: ['auth_token'],
          success: function(serverData, httpStatus){
            callback.call(this,null,serverData);
          },
          error: function(httpStatus, errorCode, responseJSON){
            callback.call(this,{status:httpStatus, errorCode: errorCode, responseJSON: responseJSON});
          },
          data: data,
          url: SESSION_URL+"/"+data.auth_token,
          type: "DELETE"
        });
      },

      register: function(data, callback){
        this.server.makeRequest({
          server: 'license',
          action: 'register',
          defaults: {},
          required: ['user', 'application'],
          success: function(serverData, httpStatus){
            callback.call(this,null,serverData);
          },
          error: function(httpStatus, errorCode, responseJSON){
            callback.call(this,{status:httpStatus, errorCode: errorCode, responseJSON: responseJSON});
          },
          data: data,
          url: USERS_URL,
          type: "POST"
        });
      },

      passwordCardAuth: function(data, callback){
        this.server.makeRequest({
          server: 'license',
          action: 'passwordCardAuth',
          defaults: {},
          success: function(serverData, httpStatus){
            callback.call(this,null,serverData);
          },
          error: function(httpStatus, errorCode, responseJSON){
            callback.call(this,{status:httpStatus, errorCode: errorCode, responseJSON: responseJSON});
          },
          data: data,
          url: PASSWORD_AUTH_URL,
          type: "GET"
        });
      },

      getGenericBiometricKey: function(data, callback){
        this.server.makeRequest({
          server: 'license',
          action: 'getGenericBiometricKey',
          defaults: {},
          success: function(serverData, httpStatus){
            callback.call(this,null,serverData);
          },
          error: function(httpStatus, errorCode, responseJSON){
            callback.call(this,{status:httpStatus, errorCode: errorCode, responseJSON: responseJSON});
          },
          data: data,
          url: GENERIC_KEY_URL,
          type: "GET"
        });
      },

      createBiometricAuthRequest: function(data, callback){
        this.server.makeRequest({
          server: 'license',
          action: 'createBiometricAuthRequest',
          defaults: {},
          success: function(serverData, httpStatus){
            callback.call(this,null,serverData);
          },
          error: function(httpStatus, errorCode, responseJSON){
            callback.call(this,{status:httpStatus, errorCode: errorCode, responseJSON: responseJSON});
          },
          data: data,
          url: BIOMETRIC_REQUEST_URL,
          type: "POST"
        });
      },

      getBiometricAuthResponse: function(data, callback){
        this.server.makeRequest({
          server: 'license',
          action: 'getBiometricAuthResponse',
          defaults: {},
          success: function(serverData, httpStatus){
            callback.call(this,null,serverData);
          },
          error: function(httpStatus, errorCode, responseJSON){
            callback.call(this,{status:httpStatus, errorCode: errorCode, responseJSON: responseJSON});
          },
          data: data,
          url: BIOMETRIC_REQUEST_URL,
          type: "GET"
        });
      },

      signature: function(data, callback){
        this.server.makeRequest({
          server: 'license',
          action: 'signature',
          defaults: {},
          required: [],
          success: function(serverData, httpStatus){
            callback.call(this,null,serverData);
          },
          error: function(httpStatus, errorCode, responseJSON){
            callback.call(this,{status:httpStatus, errorCode: errorCode, responseJSON: responseJSON});
          },
          data: data,
          url: SIGNATURE_URL,
          type: "GET"
        });
      },

      subscribed: function(data, callback){
        this.server.makeRequest({
          server: 'license',
          action: 'subscribed',
          defaults: {},
          required: ['recurly_token'],
          success: function(serverData, httpStatus){
            callback.call(this,null,serverData);
          },
          error: function(httpStatus, errorCode, responseJSON){
            callback.call(this,{status:httpStatus, errorCode: errorCode, responseJSON: responseJSON});
          },
          data: data,
          url: SUBSCRIPTIONS_URL,
          type: "POST"
        });
      },

      stripe_subscribe: function(data, callback){
        this.server.makeRequest({
          server: 'license',
          action: 'stripe_subscribed',
          defaults: {},
          required: [],
          success: function(serverData, httpStatus){
            callback.call(this,null,serverData);
          },
          error: function(httpStatus, errorCode, responseJSON){
            callback.call(this,{status:httpStatus, errorCode: errorCode, responseJSON: responseJSON});
          },
          data: data,
          url: STRIPE_SUBSCRIPTIONS_URL,
          type: "POST"
        });
      },

      refreshEntitlements: function(data, callback){
        this.server.makeRequest({
          server: 'license',
          action: 'refreshEntitlements',
          defaults: {},
          required: [],
          success: function(serverData, httpStatus){
            callback.call(this,null,serverData);
          },
          error: function(httpStatus, errorCode, responseJSON){
            callback.call(this,{status:httpStatus, errorCode: errorCode, responseJSON: responseJSON});
          },
          data: data,
          url: USERS_URL,
          type: "GET"
        });
      },

      makeDailyTouch: function(data, callback){
        this.server.makeRequest({
          server: 'license',
          action: 'makeDailyTouch',
          defaults: {},
          required: [],
          success: function(serverData, httpStatus){
            callback.call(this,null,serverData);
          },
          error: function(httpStatus, errorCode, responseJSON){
            callback.call(this,{status:httpStatus, errorCode: errorCode, responseJSON: responseJSON});
          },
          data: data,
          url: TOUCHES_URL,
          type: "POST"
        });
      },

      pushSync: function(data){
        this.server.makeRequest({
          server: 'license',
          action: 'sync',
          defaults: {},
          required: [],
          success: function(serverData, httpStatus){
          },
          error: function(httpStatus, errorCode, responseJSON){
          },
          data: data,
          url: PUSH_SYNC_URL,
          type: "POST"
        });
      },

      sendFeedbackEmail: function(data, callback){
        this.server.makeRequest({
          server: 'license',
          action: 'sendFeedbackEmail',
          defaults: {},
          required: [],
          success: function(serverData, httpStatus){
            callback.call(this,null,serverData);
          },
          error: function(httpStatus, errorCode, responseJSON){
            callback.call(this,{status:httpStatus, errorCode: errorCode, responseJSON: responseJSON});
          },
          data: data,
          url: FEEDBACK_URL,
          type: "POST"
        });
      },

      billingInfo: function(data, callback){
        this.server.makeRequest({
          server: 'license',
          action: 'billingInfo',
          defaults: {},
          required: [],
          success: function(serverData, httpStatus){
            callback.call(this,null,serverData);
          },
          error: function(httpStatus, errorCode, responseJSON){
            callback.call(this,{status:httpStatus, errorCode: errorCode, responseJSON: responseJSON});
          },
          data: data,
          url: BILLING_INFO_URL,
          type: "GET"
        });
      },

      retrieveMedalsEarned: function(data, callback){
        this.server.makeRequest({
          server: 'license',
          action: 'retrieveMedalsEarned',
          defaults: {},
          required: [],
          success: function(serverData, httpStatus){
            callback.call(this,null,serverData);
          },
          error: function(httpStatus, errorCode, responseJSON){
            callback.call(this,{status:httpStatus, errorCode: errorCode, responseJSON: responseJSON});
          },
          data: data,
          url: MEDALS_URL,
          type: "GET"
        });
      },

      incrementMedalsEarned: function(data, callback){
        this.server.makeRequest({
          server: 'license',
          action: 'incrementMedalsEarned',
          defaults: {},
          required: [],
          success: function(serverData, httpStatus){
            callback.call(this,null,serverData);
          },
          error: function(httpStatus, errorCode, responseJSON){
            callback.call(this,{status:httpStatus, errorCode: errorCode, responseJSON: responseJSON});
          },
          data: data,
          url: MEDALS_URL,
          type: "PUT"
        });
      },

      logCheckout: function(data, callback){
        this.server.makeRequest({
          server: 'license',
          action: 'logCheckout',
          skip_auth_token: true,
          defaults: {},
          required: [],
          success: function(serverData, httpStatus){
            callback.call(this,null,serverData);
          },
          error: function(httpStatus, errorCode, responseJSON){
            callback.call(this,{status:httpStatus, errorCode: errorCode, responseJSON: responseJSON});
          },
          data: data,
          url: LOG_CHECKOUT_URL+"/"+data.method,
          type: "PUT"
        });
      },

      retrieveSyncstoreToken: function(data, callback){
        this.server.makeRequest({
          server: 'license',
          action: 'retrieveSyncstoreToken',
          defaults: {},
          required: [],
          success: function(serverData, httpStatus){
            callback.call(this,null,serverData);
          },
          error: function(httpStatus, errorCode, responseJSON){
            callback.call(this,{status:httpStatus, errorCode: errorCode, responseJSON: responseJSON});
          },
          data: data,
          url: SYNCSTORE_TOKEN_URL,
          type: "GET"
        });
      },

      retrieveDevices: function(data, callback){
        this.server.makeRequest({
          server: 'license',
          action: 'retrieveDevices',
          defaults: {},
          required: [],
          success: function(serverData, httpStatus){
            callback.call(this,null,serverData);
          },
          error: function(httpStatus, errorCode, responseJSON){
            callback.call(this,{status:httpStatus, errorCode: errorCode, responseJSON: responseJSON});
          },
          data: data,
          url: DEVICES_URL,
          type: "GET"
        });
      },

      getCompanyAccounts: function(data, callback){
        this.server.makeRequest({
          server: 'license',
          action: 'getCompanyAccounts',
          defaults: {},
          required: [],
          success: function(serverData, httpStatus){
            callback.call(this,null,serverData);
          },
          error: function(httpStatus, errorCode, responseJSON){
            callback.call(this,{status:httpStatus, errorCode: errorCode, responseJSON: responseJSON});
          },
          data: data,
          url: COMPANY_ACCOUNTS_URL,
          type: "GET"
        });
      }

    });

    return LicenseServer;

  });
ABINE_DNTME.define("abine/lruCache", [],
  function () {
    var LRUCache = function (size) {
      var cache = {};
      var maxSize = size;
      var size = 0;
      var cacheHead = null;
      var cacheTail = null;

      this.add = function (id, data) {
        var link = {
          id: id,
          data: data,
          previous:null,
          next: cacheHead
        };

        cache[id] = link;

        if (cacheHead) {
          cacheHead.previous = link;
        }
        cacheHead = link;

        if (size === maxSize) {
          var lruLink = cacheTail;
          cacheTail = cacheTail.previous;
          cacheTail.next = null;
          delete cache[lruLink.id];
          delete lruLink;
        } else {
          if (size === 0) {
            cacheTail = link;
          }
          size++;
        }
      },

      this.size = function() {
        return size;
      };

      this.fetch = function (id) {
        var link = cache[id];
        if (link) {
          if (size > 1) {
            var previous = link.previous;
            var next = link.next;

            if (cacheTail == link) {
              cacheTail = previous;
            }
            else if (cacheHead != link) {
              previous.next = next;
              next.previous = previous;
            }

            link.previous = null;
            link.next = cacheHead;

            cacheHead.previous = link;
            cacheHead = link;
          }

          return link.data;
        }
        else {
          return null;
        }
      }
    };

    return LRUCache;
  });
ABINE_DNTME.define("abine/mappingServer",
  ['documentcloud/underscore', 'abine/ajax', 'abine/config', 'abine/server', 'abine/core'],
  function (_, abineAjax, config, abineServer, abineCore) {

    var HOST = config.protocol + config.mappingServerHost,
    PUSH_URL = HOST + "/api/v2/dntmappings",
    MANIFEST_URL = HOST + "/api/v2/mappings/manifest",
    MAPPINGS_URL = "https://rules.abine.com/mappings/"+(ABINE_DNTME.config.environment == 'production' ? 'production' : 'test')+"/dntme/"+ABINE_DNTME.config.browser.toLowerCase()+'/mappings.json',
    MAPPING_VERSION_URL = "https://rules.abine.com/mappings/"+(ABINE_DNTME.config.environment == 'production' ? 'production' : 'test')+"/version.txt",
    FIELD_DATA_URL = HOST + "/api/v3/field_data",
    TRACKER_DATA_URL = HOST + "/api/v3/tracker_data",
    PAGE_DATA_URL = HOST + "/api/v3/page_data",
    MAPPER_DETAILS_URL = HOST + "/api/v2/mappers",
    FORM_MAPPINGS_URL = HOST + "/api/v2/mappings",
    FORM_BLOB_URL = HOST + "/api/v2/formblob",
    AUTOFILL_FEEDBACK_URL = HOST + "/api/v2/autofill_feedback",
    PANELS_URL = HOST + "/api/v2/panels",
    TRACKER_ALLOWS_URL = HOST + "/api/v3/tracker_allow";


    var emptyCallback = function(){};

    var MappingServer = abineCore.BaseClass.extend({
      initialize: function(options){
        this.options = this.options || options || {};
        this.server = this.options.server || new abineServer.server();
      },

      sendPanelActivity: function(data, callback){
        callback = callback || emptyCallback;

        this.server.makeRequest({
          server: 'mapping',
          action: 'getMappingManifest',
          defaults: {},
          required: [],
          success: function(serverData, httpStatus){
            callback.call(this,null,serverData);
          },
          error: function(httpStatus, errorCode, responseJSON){
            callback.call(this,{status:httpStatus, errorCode: errorCode, responseJSON: responseJSON});
          },
          data: data,
          url: MANIFEST_URL,
          type: "POST"
        });

      },

      pushMapping: function(data, callback){
        this.server.makeRequest({
          server: 'mapping',
          action: 'pushMapping',
          defaults: {},
          required: [],
          success: function(serverData, httpStatus){
            callback.call(this,null,serverData);
          },
          error: function(httpStatus, errorCode, responseJSON){
            callback.call(this,{status:httpStatus, errorCode: errorCode, responseJSON: responseJSON});
          },
          data: data,
          url: PUSH_URL,
          type: "POST"
        });
      },

      getMappingVersion: function(callback){
        this.server.makeRequest({
          server: 'mapping',
          action: 'getMappingVersion',
          defaults: {},
          required: [],
          success: function(serverData, httpStatus){
            callback.call(this,null,serverData);
          },
          error: function(httpStatus, errorCode, responseJSON){
            callback.call(this,{status:httpStatus, errorCode: errorCode, responseJSON: responseJSON});
          },
          url: MAPPING_VERSION_URL,
          raw: true,
          skip_auth_token: true,
          skip_ca: true,
          type: "GET"
        });
      },

      getMappings: function(callback){
        this.server.makeRequest({
          server: 'mapping',
          action: 'getMappingManifest',
          defaults: {},
          required: [],
          success: function(serverData, httpStatus){
            callback.call(this,null,serverData);
          },
          error: function(httpStatus, errorCode, responseJSON){
            callback.call(this,{status:httpStatus, errorCode: errorCode, responseJSON: responseJSON});
          },
          url: MAPPINGS_URL,
          raw: true,
          skip_auth_token: true,
          skip_ca: true,
          type: "GET"
        });
      },

      getFormMappings: function(data, callback){
        this.server.makeRequest({
          server: 'mapping',
          action: 'getFormMappings',
          defaults: {},
          required: ['signatures'],
          success: function(serverData, httpStatus){
            callback.call(this,null,serverData);
          },
          error: function(httpStatus, errorCode, responseJSON){
            callback.call(this,{status:httpStatus, errorCode: errorCode, responseJSON: responseJSON});
          },
          data: data,
          url: MAPPINGS_URL,
          type: "POST"
        });
      },

      getMapperDetails: function(data, callback){
        this.server.makeRequest({
          server: 'mapping',
          action: 'getMapperDetails',
          defaults: {},
          required: [],
          success: function(serverData, httpStatus){
            callback.call(this,null,serverData);
          },
          error: function(httpStatus, errorCode, responseJSON){
            callback.call(this,{status:httpStatus, errorCode: errorCode, responseJSON: responseJSON});
          },
          data: data,
          url: MAPPER_DETAILS_URL,
          type: "GET"
        });
      },

      getOneFormMappings: function(data, callback){
        this.server.makeRequest({
          server: 'mapping',
          action: 'getFormMappings',
          defaults: {},
          required: ['id'],
          success: function(serverData, httpStatus){
            callback.call(this,null,serverData);
          },
          error: function(httpStatus, errorCode, responseJSON){
            callback.call(this,{status:httpStatus, errorCode: errorCode, responseJSON: responseJSON});
          },
          data: data,
          url: FORM_MAPPINGS_URL+"/"+data.id,
          type: "GET"
        });
      },

      sendAutofillFeedback: function(data, callback){
        this.server.makeRequest({
          server: 'mapping',
          action: 'sendAutofillFeedback',
          defaults: {},
          required: [],
          success: function(serverData, httpStatus){
            callback.call(this,null,serverData);
          },
          error: function(httpStatus, errorCode, responseJSON){
            callback.call(this,{status:httpStatus, errorCode: errorCode, responseJSON: responseJSON});
          },
          data: data,
          url: AUTOFILL_FEEDBACK_URL,
          type: "POST"
        });
      },

      sendFormBlob: function(data, callback){
        this.server.makeRequest({
          server: 'mapping',
          action: 'sendFormBlob',
          defaults: {},
          required: ['form_id', 'html'],
          success: function(serverData, httpStatus){
            callback.call(this,null,serverData);
          },
          error: function(httpStatus, errorCode, responseJSON){
            callback.call(this,{status:httpStatus, errorCode: errorCode, responseJSON: responseJSON});
          },
          data: data,
          url: FORM_BLOB_URL,
          type: "POST"
        });
      },

      getPanels: function(data, callback){
        this.server.makeRequest({
          server: 'mapping',
          action: 'getPanels',
          defaults: {},
          required: ['browser','tag','user_state'],
          success: function(serverData, httpStatus){
            callback.call(this,null,serverData);
          },
          error: function(httpStatus, errorCode, responseJSON){
            callback.call(this,{status:httpStatus, errorCode: errorCode, responseJSON: responseJSON});
          },
          data: data,
          url: PANELS_URL,
          type: "GET"
        });
      },

      saveTrackerData: function(data, callback){
        callback = callback || emptyCallback;

        this.server.makeRequest({
          server: 'mapping',
          action: 'saveTrackerData',
          skip_auth_token: true,
          defaults: {},
          required: [],
          success: function(serverData, httpStatus){
            callback.call(this,null,serverData);
          },
          error: function(httpStatus, errorCode, responseJSON){
            callback.call(this,{status:httpStatus, errorCode: errorCode, responseJSON: responseJSON});
          },
          data: data,
          url: TRACKER_DATA_URL,
          type: "POST"
        });
      },

      savePageData: function(data, callback){
        callback = callback || emptyCallback;

        this.server.makeRequest({
          server: 'mapping',
          action: 'savePageData',
          skip_auth_token: true,
          defaults: {},
          required: [],
          success: function(serverData, httpStatus){
            callback.call(this,null,serverData);
          },
          error: function(httpStatus, errorCode, responseJSON){
            callback.call(this,{status:httpStatus, errorCode: errorCode, responseJSON: responseJSON});
          },
          data: data,
          url: PAGE_DATA_URL,
          type: "POST"
        });
      },

      sendTrackerAllow: function(data, callback) {
        callback = callback || emptyCallback;

        this.server.makeRequest({
          server: 'mapping',
          action: 'sendTrackerAllow',
          skip_auth_token: true,
          defaults: {},
          required: [],
          success: function(serverData, httpStatus){
            callback.call(this,null,serverData);
          },
          error: function(httpStatus, errorCode, responseJSON){
            callback.call(this,{status:httpStatus, errorCode: errorCode, responseJSON: responseJSON});
          },
          data: data,
          url: TRACKER_ALLOWS_URL,
          type: "POST"
        });
      },

      saveFieldData: function(data, callback){
        callback = callback || emptyCallback;

        this.server.makeRequest({
          server: 'mapping',
          action: 'saveFieldData',
          skip_auth_token: true,
          defaults: {},
          required: [],
          success: function(serverData, httpStatus){
            callback.call(this,null,serverData);
          },
          error: function(httpStatus, errorCode, responseJSON){
            callback.call(this,{status:httpStatus, errorCode: errorCode, responseJSON: responseJSON});
          },
          data: data,
          url: FIELD_DATA_URL,
          type: "POST"
        });
      }
    });

    return MappingServer;
    
  });
ABINE_DNTME.define("abine/memoryStore",
  ["documentcloud/underscore", "abine/core", "abine/lruCache"],
function(_, abineCore, LRUCache) {

  var store = new LRUCache(1000);

  return {
    set: function(name, value) {
      store.add(name, value);
    },
    get: function(name, callback) {
      callback(store.fetch(name));
    }
  }
});

// Events from MM server pages to our addon.
// be careful if you start using events from web-page for modifying any internal data
//   OR to send response back to webpage.

ABINE_DNTME.define('abine/pageEvents',
    ['documentcloud/underscore', 'abine/core','abine/securityManager', 'abine/config', 'storage', 'abine/timer', 'persistence',
      'modules', 'abine/window',
      'abine/dntmeUpgrade', 'abine/url', 'abine/localStorage', 'abine/webapp'],
    function(_, abineCore, securityManager, config, storage, abineTimer, persistence, modules, abineWindow,
             dntmeUpgrade, abineUrl, localStorage, webapp) {

    var allowedHostsRegex = null;


    var _javascriptErrors = null;
    try {
      if (typeof(window) != 'undefined') {
        _javascriptErrors = [];
        window.onerror = function (errorMsg, url, lineNumber, column, errorObj) {
          if (errorObj)
            errorObj = errorObj.stack;
          else
            errorObj = '';
          _javascriptErrors.push([errorMsg, url, lineNumber, column, errorObj]);
          if (_javascriptErrors.length > 20)
            _javascriptErrors.shift();
        };
      }
    } catch (e) {}

    var emptyCallback = function(){};

    // allow events only from *.abine.com
    if (config.environment == 'development') {
      allowedHostsRegex = /.*/i;
    } else {
      allowedHostsRegex = /^(.+\.)?abine\.com$/i;
    }

    var PageEventsModule = abineCore.BaseClass.extend({
      initialize: function(){

      },

      processAction: function(data, callback){
        LOG_FINEST && ABINE_DNTME.log.finest('webapp request '+JSON.stringify(data));

        callback = callback || emptyCallback;

        if (data.host.indexOf(ABINE_DNTME.config.licenseServerHost.replace(/:[0-9]+/,'')) != -1) {
          switch (data.action) {
            case "blurstore:setup:complete":
              this.refreshData(data.params, callback);
              webapp.reload();
              break;
          }
        } else if (data.host.indexOf(ABINE_DNTME.config.mappingServerHost.replace(/:[0-9]+/,'')) != -1) {
          switch (data.action) {
            case "get:mapping:review:mode":
              this.getReviewMode(callback);
              break;
            case "start:mapping:review:mode":
              this.switchToReviewMode(true, callback);
              break;
            case "end:mapping:review:mode":
              this.switchToReviewMode(false, callback);
              break;
            case "set:mapper:mode":
              this.switchToMapperMode(data.params, callback);
              break;
            case "exit:mapper:mode":
              this.clearMapperSettings(callback);
              break;
            case "set:autofill:mode":
              this.switchToAutofillMode(data.params, callback);
              break;
            case "updateFormRules":
              localStorage.setItem('form-rules', data.params);
              modules.mappingsModule.setFormRules(JSON.parse(data.params));
              callback();
              break;
            case "getVersion":
              callback({version: ABINE_DNTME.config.version, tag: ABINE_DNTME.config.tags[0]});
              break;
          }
        } else if (data.host.indexOf(ABINE_DNTME.config.webAppHost.replace(/:[0-9]+/,'')) != -1) {
          if (data.action == 'importAccounts') {
            // this can happen when user installs DNTMe first and then disables MaskMe
            ABINE_DNTME.maskme_installed = false;
          }
          if (ABINE_DNTME.maskme_installed && !data.action.match(/canAutoImport|exportFromMaskme/i)) {
            // do not respond to webapp messages when maskme is installed.
            callback();
            return;
          }
          // message from this plugin's webapp
          switch (data.action) {
            case "refreshTag":
              this.refreshTag(callback);
              break;
            case "setAuthToken":
              this.setAuthToken(data.params, callback);
              break;
            case "retrieveAuthToken":
              if (data.secure) {
                this.retrieveAuthToken(data.params, callback);
              } else {
                callback(null);
              }
              break;
            case "retrieveWebsocketId":
              this.retrieveWebsocketId(callback);
              break;
            case "emitPushEvent":
              this.emitPushEvent(data.params);
              callback();
              break;
            case "email:created":
              this.emailCreated(data.params, callback);
              break;
            case "email:deleted":
              this.emailDeleted(data.params, callback);
              break;
            case "refreshData":
              this.refreshData(data.params, callback);
              break;
            case "retrieveDntStats":
              this.retrieveDntStats(data.params, callback);
              break;
            case "retrieveDntData":
              this.retrieveDntData(data.params, callback);
              break;
            case "retrieveInstallAuthToken":
              this.retrieveInstallAuthToken(data.params, callback);
              break;
            case "reloadCompanyAccounts":
              this.reloadCompanyAccounts(data.params, callback);
              break;
            case "migrationComplete":
              this.migrationComplete(data.params, callback);
              break;
            case "dntNewFeaturePageVisited":
              this.newFeaturePageVisited(data.params, callback);
              break;
            case "importAccounts":
              this.importAccounts(data.params, callback);
              break;
            case "retrieveEntities":
              if (data.secure) {
                this.retrieveEntities(data.params, callback);
              } else {
                callback(null);
              }
              break;
            case "updateEntity":
              if (data.secure) {
                this.updateEntity(data.params, callback);
              } else {
                callback(null);
              }
              break;
            case "deleteEntity":
              if (data.secure) {
                this.deleteEntity(data.params, callback);
              } else {
                callback(null);
              }
              break;
            case "resetEntity":
              if (data.secure) {
                this.resetEntity(data.params, callback);
              } else {
                callback(null);
              }
              break;
            case "loginAccount":
              if (data.secure) {
                this.loginAccount(data.params, callback);
              } else {
                callback(null);
              }
              break;
            case "getBrowserLoginCount":
              if (data.secure) {
                this.getBrowserLoginCount(data.params, callback);
              } else {
                callback(null);
              }
              break;
            case "getBrowserLogins":
              if (data.secure) {
                this.getBrowserLogins(data.params, callback);
              } else {
                callback(null);
              }
              break;
            case "userDeleted":
              if (data.secure) {
                this.userDeleted(data.params, callback);
              } else {
                callback(null);
              }
              break;
            case "canAutoImport":
              callback(ABINE_DNTME.maskme_installed && ABINE_DNTME.maskme_can_export);
              break;
            case "exportFromMaskme":
              if (data.secure) {
                dntmeUpgrade.exportFromMaskme(callback);
              } else {
                callback(null);
              }
              break;
            case "blurstore:setup:complete":
              this.refreshData(data.params, callback);
              webapp.reload();
              break;
            case "disableBlurGrowl":
              this.disableBlurGrowl(data.params, callback);
              break;
            case "checkBlurGrowl":
              this.checkBlurGrowl(data.params, callback);
              break;
            case "abineStoreSync":
              this.abineStoreSync(data.params, callback);
              break;
            case "webappActive":
              this.webappActive(data.params, callback);
              break;
            case "languageChanged":
              this.changeLanguage(data.params, callback);
              break;
            case "checkExtensionActivity":
              this.checkExtensionActivity(data.params, callback);
              break;
            case "sendAutolockPref":
              this.sendAutolockPref(data.params, callback);
              break;
            case "disconnectPushNotification":
              this.disconnectPushNotification();
              callback();
              break;
            case "getVersion":
              callback({version: ABINE_DNTME.config.version, tag: ABINE_DNTME.config.tags[0]});
              break;
            case "getExtensionErrors":
              callback(_javascriptErrors);
              break;
            default:
              if (data.action && data.action.indexOf("retrieveEntities") == 0 && data.secure)
                this.retrieveEntities(data.params, callback);
              else
                callback();
          }
        } else if (data.host.match(allowedHostsRegex)) {
          // message from other abine.com sites.

          switch(data.action) {
            case "getVersion":
              callback({version: ABINE_DNTME.config.version, tag: ABINE_DNTME.config.tags[0]});
              break;
            case "updateFormRules":
              localStorage.setItem('form-rules', data.params);
              modules.mappingsModule.setFormRules(JSON.parse(data.params));
              callback();
              break;
            case "updateRules":
              ABINE_DNTME.dnt_api.setupRules(data.params);
              callback();
              break;
            case "getDailyTotals":
              this.getDailyTotalsByYear(data.params[0], callback);
              break;
            case "getBadTrackerTotals":
              this.getBadTrackerTotals(callback);
              break;
          }
          callback();
        } else {
          callback();
        }

      },

      refreshTag: function(callback){
        storage.Preference.findBy('key', 'userTag', function(pref){
          if (pref && pref.value && !securityManager.isLocked()) {
            callback(pref.value);
          } else {
            callback(ABINE_DNTME.config.tags[0]);
          }
        });
      },

      setAuthToken: function(params, callback){
        securityManager.setAuthToken(params.auth_token, params.key, params.companyKey);
        callback({});
      },

      retrieveAuthToken: function(params, callback){
        var authToken = securityManager.getAuthToken();
        if (!authToken || authToken == '') {
          callback(null);
        } else {
          callback({
            auth_token:  authToken,
            key: securityManager.getEncryptionKey()||'',
            companyKey: securityManager.getCompanyEncryptionKey()||'',
            user_id: securityManager.getUserId()
          });
        }
      },

      retrieveWebsocketId: function(callback){
        ABINE_DNTME.require(['abine/pushNotifications'], function(notifications){
          callback(notifications.getId());
        });
      },

      emitPushEvent: function(data){
        ABINE_DNTME.require(['abine/pushNotifications'], function(notifications){
          notifications.emit(data.event, data.payload);
        });
      },

      disableBlurGrowl: function(params, callback){
        storage.Preference.createOrModify({key:'showBlurGrowl', value:'false'},function(){callback({})});
        storage.Preference.createOrModify({key:'showBlurPage', value:'false'},function(){callback({})});
        storage.Preference.createOrModify({key:'blurGrowlTab', value:'false'},function(){callback({})});
      },

      checkBlurGrowl: function(params, callback){
        storage.Preference.findBy('key','showBlurPage',function(showBlurPage){
          if(!showBlurPage || showBlurPage.isFalse()){
              callback(null);
          }else{
              callback(true);
          }
          storage.Preference.createOrModify({key:'showBlurPage', value:'false'}, function(){});
        });
      },

      checkExtensionActivity: function(params, callback){
        if (securityManager.isLocked()) {
          // lastActivity is being set even when its locked, so ignore it when locked.
          callback(null);
          return;
        }
        storage.Preference.findBy('key','lastActivity', function(lastActivity){
          if(!lastActivity || !lastActivity.value){
            callback(null)
          }else{
            callback({activeTime: lastActivity.value});
          }
        });
      },

      webappActive: function(params, callback){
        storage.Preference.createOrModify({key: 'lastActivity', value: Date.now()}, function(){});
        callback(null);
      },

      changeLanguage: function(params, callback){
        if(params.lang == "en-US")
          params.lang = "en";
        storage.Preference.createOrModify({key: 'dnt_language', value: params.lang}, function(){
          callback(null);
        });
      },

      sendAutolockPref: function(thePref, callback){
        storage.Preference.createOrModify({key: 'autolock', value: thePref}, function(){callback({})});
      },

      abineStoreSync: function(sync_count, callback){
        ABINE_DNTME.require(['abine/blurstore-api'], function(blurStore){
          blurStore.synchronizeWhenOld(sync_count);
        });
        callback(null);
      },

      importAccounts: function(params, callback) {
        var records = [];
        try {
          if (params.id && params.key) {
            // migrating MaskMe user
            securityManager.__setUserIdAndKey(params.id, params.key);
          } else {
            // importing from MaskMe into existing DNTMe account
          }

          storage.Account.clearCache(function(){
            storage.Account.index(null, function(entities){
              var doneIds = {};
              _.each(entities, function(entity){
                doneIds[entity.id] = true;
                records.push(entity.toJSON());
              });

              _.each(params.accounts, function(account){
                if (account.id in doneIds) return;
                account.domain = abineUrl.getTLD(account.domain);  // some accounts had full url as domain in maskme (CLT-1130)
                var newAccount = new storage.Account(account);
                persistence.add(newAccount);
                records.push(newAccount.toJSON())
              });

              storage.Account.saveToLocalStorage(function(){
                if (params.id && params.key) {
                  securityManager.__setUserIdAndKey(null, null);
                }
                callback(records);
              });
            });
          });
        } catch(e){}
      },

      retrieveEntities: function(entityName, callback) {
        LOG_FINEST&& ABINE_DNTME.log.finest("retrieve entities received for "+entityName);
        var records = [];
        if (storage[entityName]) {
          securityManager.afterRefresh(function(){
            try {
              if (storage[entityName].decryptionFailed) {
                LOG_FINEST && ABINE_DNTME.log.finest("decryption failed");
                callback('decryption failed');
                return;
              }
              storage[entityName].index(null, function (entities) {
                _.each(entities, function (entity) {
                  records.push(entity.toJSON());
                });
                LOG_FINEST && ABINE_DNTME.log.finest("sending " + records.length + " " + entityName + "s to webapp");
                callback(records);
              });
              return;
            } catch (e) {
              callback(records);
            }
          });
          return;
        }
        callback(records);
      },

      updateEntity: function(params, callback) {
        if (storage[params.name]) {
          var record = params.entity;
          if (record.id) { // to break infinite loop when webapp app local-storage has bad data.
            storage[params.name].findBy('id', record.id, function(entity){
              if (!entity) {
                entity = new storage[params.name](record);
                persistence.add(entity);
              } else {
                _.extend(entity, record)
              }
              persistence.flush();
              callback();
            });
            return;
          }
        }
        callback();
      },

      deleteEntity: function(params, callback) {
        if (storage[params.name]) {
          var id = params.id;
          storage[params.name].findBy('id', id, function(entity){
            if (entity) {
              persistence.remove(entity);
              persistence.flush();
            }
            callback();
          });
        } else {
          callback();
        }
      },

      resetEntity: function(params, callback) {
        var Entity = storage[params.name];
        if (Entity) {
          Entity.clearCache(function(){
            if (params.user_id) {
              localStorage.removeItem(params.name+'.'+params.user_id);
            } else {
              localStorage.removeItem(params.name);
            }
            callback();
          });
        } else {
          callback();
        }
      },

      emailCreated: function(params, callback){
        var email = new storage.DisposableEmail(params);
        email.createdAt = params.created_at;
        persistence.add(email);
      },

      emailDeleted: function(params, callback){
        storage.DisposableEmail.findBy('guid', params.guid, function(email){
          if (email) {
            persistence.remove(email);
            persistence.flush();
          }
        });
      },

      refreshData: function(params, callback){
        persistence.reset();
        securityManager.refreshData();
      },

      // just removes email... not really deleting the user?
      userDeleted: function(params, callback){
        storage.Preference.findBy('key', 'oldEmail', function(pref){
          if (pref) {
            persistence.remove(pref);
            storage.Preference.createOrModify({key:'loginNotLock', value:'true'},function(){
                persistence.flush();
                if(callback) callback({});
            });
          }
        });
      },

      loginAccount: function(param, callback){
        var id = param.id || param;
        var webappLoginUrl = param.login_url;
        storage.Account.findBy('id', id, function(acc){
          storage.CompanyAccount.findBy('id', id, function(companyAcc){
            if (!acc && companyAcc) {
              acc = companyAcc;
            }
            storage.LoginUrl.loginUrlForSite({params: [acc.domain]}, function(loginUrl){
              if (!loginUrl) {
                loginUrl = webappLoginUrl;
              }
              if (loginUrl) {
                loginUrl = loginUrl.href;
              }
              if (!loginUrl) {
                loginUrl = acc.login_url;
              }
              if (!loginUrl && acc.page_host) {
                loginUrl = 'http://'+acc.page_host;
              }
              if (!loginUrl && acc.domain) {
                loginUrl = 'http://'+acc.domain;
              }
              if (loginUrl) {
                securityManager.openPage(loginUrl);
              }
            });
          });
        });
        callback();
      },

      getBrowserLoginCount: function(params, callback) {
        if (ABINE_DNTME.config.browser == 'Firefox') {
          ABINE_DNTME.require(['abine/firefox_import'], function(firefox){
            callback(firefox.getLoginCount());
          });
          return;
        }
        callback(-1);
      },

      getBrowserLogins: function(params, callback) {
        if (ABINE_DNTME.config.browser == 'Firefox') {
          ABINE_DNTME.require(['abine/firefox_import'], function(firefox){
            callback(firefox.getLogins());
          });
          return;
        }
        callback(null);
      },

      migrationComplete: function(params, callback) {
        storage.Preference.findBy('key', 'mergedMigrationPending', function(pref){
          if (pref) {
            persistence.remove(pref);
            persistence.flush();
          }
        });
      },

      retrieveDntStats: function(params, callback){
        ABINE_DNTME.dnt_api.forceSaveStats();
        storage.DntStats.index(null, function(dntStats){
          if(dntStats.length > 0){
            callback(dntStats[0].toJSON());
          } else {
            callback(null);
          }
        });
      },

      retrieveInstallAuthToken: function(params, callback){
        storage.Preference.findBy('key','installAuthToken', function(pref){
          if(!pref || pref.value == ""){
            callback(null)
          }else{
            callback({installAuthToken: pref.value})
          }
        });
      },

      reloadCompanyAccounts: function(params, callback) {
        storage.CompanyAccount.clearCache();
        storage.CompanyAccount.fetchFromServer();
        callback(null);
      },

      getDailyTotalsByYear: function(year, callback) {
        storage.DntStats.index(null, function(dntStats){
          if(dntStats.length > 0){
            var dntStat = dntStats[0];
            var totals = JSON.parse(dntStat.dailyTrackerCounts);
            if (year in totals) {
              totals[year].hasBadTrackers = true;
              callback(totals[year]);
              return;
            }
          }
          callback({});
        });
      },

      getBadTrackerTotals: function(callback) {
        storage.DntStats.index(null, function(dntStats){
          if(dntStats.length > 0){
            var dntStat = dntStats[0];
            var badTrackers = JSON.parse(dntStat.badTrackers);
            if (!badTrackers.totals) {
              callback({});
              return;
            }
            var year = (new Date()).getFullYear();
            var badTrackersTotal = badTrackers.totals;
            var badTrackersDailyTotal = {};
            var years = [year, year-1];
            for (var tracker in badTrackersTotal) {
              for (var y=0;y<years.length;y++) {
                var trackerYear = tracker+'.'+years[y];
                if (!badTrackers.daily || !(trackerYear in badTrackers.daily)) continue;
                badTrackersDailyTotal[tracker] = {};
                badTrackersDailyTotal[tracker][years[y]] = badTrackers.daily[trackerYear];
              }
            }

            var links = {};
            securityManager.getLink({}, function(baseUrl){
              for (var tracker in badTrackersTotal) {
                links[tracker] = baseUrl+'&type=badTracker.'+tracker;
              }
              callback({totals: badTrackersTotal, daily: badTrackersDailyTotal, links: links});
            });
            return;
          }
          callback({});
        });
      },

      retrieveDntData: function(params, callback){
        dntmeUpgrade.getMaskmeData(callback);
      },

      disconnectPushNotification: function(){
        ABINE_DNTME.require(['abine/pushNotifications'], function(notifications){
          notifications.disconnect();
        });
      },

      newFeaturePageVisited: function(params, callback){
        storage.Preference.createOrModify({key:'new_feature_explored', value: (new Date()).toString()}, function(){
          callback();
        });
      },

      getReviewMode: function(callback) {
        storage.Preference.findBy('key', 'mapping:reviewer', function(pref) {
          if (pref) {
            callback({review: true, email: pref.value});
          } else {
            callback({review: false});
          }
        });
      },

      clearMapperSettings: function(callback) {
        storage.Preference.batchRemoveBy([
          {by:'key', value:'mapping:reviewer'},
          {by:'key', value:'mapping:mapper'},
          {by:'key', value:'mapping:batch'},
          {by:'key', value:'mapping:form:id'},
          {by:'key', value:'autofill:mapper'},
          {by:'key', value:'autofill:batch'},
          {by:'key', value:'autofill:form:id'}
        ], callback);
      },

      switchToMapperMode: function(params, callback) {
        this.clearMapperSettings(function () {
          storage.Preference.batchCreateOrModify([
            {'key': 'mapping:mapper', 'value': cleanupName(params.mapper)},
            {'key': 'mapping:batch', 'value': cleanupName(params.batch)},
            {'key': 'mapping:form:id', 'value': params.id}
          ], function () {

            callback(params.mapper);
          });
        });
      },

      switchToAutofillMode: function(params, callback) {
        this.clearMapperSettings(function () {
          storage.Preference.batchCreateOrModify([
            {'key': 'autofill:mapper', 'value': cleanupName(params.mapper)},
            {'key': 'autofill:batch', 'value': cleanupName(params.batch)},
            {'key': 'autofill:form:id', 'value': params.id}
          ], function () {
            modules.mappingsModule.forceMappingUpdate(true, _.bind(function(data) {
              callback(params.mapper);
            }, this));
          });
        });
      },

      switchToReviewMode: function(state, callback) {
        if (state) {
          modules.mappingsModule.getMapperDetails(_.bind(function(data){
            if (data) {
              if (data.is_reviewer) {
                this.clearMapperSettings(function () {
                  storage.Preference.createOrModify({key:'mapping:reviewer', value: data.name}, function(){
                    callback(data.name);
                  });
                });
                return;
              }
            }
            callback();
          }, this));
        } else {
          this.clearMapperSettings(function () {
            callback('');
          });
        }
      }
    });

    function cleanupName(name) {
      if (!name) name = '';
      name = name.replace(/&lt;|&gt;|[\s\<\>]+/g, '');
      return name;
    }

    var pageEventsModule = new PageEventsModule({});

    return pageEventsModule;
});
ABINE_DNTME.define("abine/paymentsServer",
    ['documentcloud/underscore', 'abine/ajax', 'abine/config', 'abine/server', 'abine/core'],
    function (_, abineAjax, config, abineServer, abineCore) {

    var HOST = config.protocol + config.paymentsServerHost;
    var PAYMENTS_URL = HOST + "/api/v3/cards";
    var CARD_CREDITS_URL = HOST + "/api/v3/cardcredit/client"
    var CARD_FEE_URL = HOST + "/api/v3/cardfee"
    var VERIFY_CARD_REQUEST_URL = HOST + "/api/v3/verify_card_request"

    var PaymentsServer = abineCore.BaseClass.extend({
      initialize: function(options){
        this.options = this.options || options || {};
        this.server = this.options.server || new abineServer.server();
      },

      createCard: function(data, callback){
        this.server.makeRequest({
          server: 'payments',
          action: 'createCard',
          defaults: {},
          required: ['request_id','amount','domain', 'card_use'],
          success: function(serverData, httpStatus){
            callback.call(this,null,serverData);
          },
          error: function(httpStatus, errorCode, responseJSON){
            callback.call(this,{status:httpStatus, errorCode: errorCode, responseJSON: responseJSON});
          },
          data: data,
          url: PAYMENTS_URL,
          type: "POST"
        });
      },

      getCard: function(guid, callback){
        this.server.makeRequest({
          server: 'payments',
          action: 'getCard',
          defaults: {},
          required: [],
          success: function(serverData, httpStatus){
            callback.call(this,null,serverData);
          },
          error: function(httpStatus, errorCode, responseJSON){
            callback.call(this,{status:httpStatus, errorCode: errorCode, responseJSON: responseJSON});
          },
          data: {},
          url: PAYMENTS_URL+"/"+guid,
          type: "GET"
        });
      },

      updateCard: function(data, callback){
        this.server.makeRequest({
          server: 'payments',
          action: 'updateCard',
          defaults: {},
          required: [],
          success: function(serverData, httpStatus){
            callback.call(this,null,serverData);
          },
          error: function(httpStatus, errorCode, responseJSON){
            callback.call(this,{status:httpStatus, errorCode: errorCode, responseJSON: responseJSON});
          },
          data: data,
          url: PAYMENTS_URL+"/"+data.card_id,
          type: "PUT"
        });
      },

      retrieveCardCredit: function(data, callback){
        this.server.makeRequest({
          server: 'payments',
          action: 'retrieveCardCredit',
          defaults: {},
          required: [],
          success: function(serverData, httpStatus){
            callback.call(this,null,serverData);
          },
          error: function(httpStatus, errorCode, responseJSON){
            callback.call(this,{status:httpStatus, errorCode: errorCode, responseJSON: responseJSON});
          },
          data: data,
          url: CARD_CREDITS_URL,
          type: "GET"
        });
      },

      retrieveCardFee: function(data, callback){
        this.server.makeRequest({
          server: 'payments',
          action: 'retrieveCardFee',
          defaults: {},
          required: [],
          success: function(serverData, httpStatus){
            callback.call(this,null,serverData);
          },
          error: function(httpStatus, errorCode, responseJSON){
            callback.call(this,{status:httpStatus, errorCode: errorCode, responseJSON: responseJSON});
          },
          data: data,
          url: CARD_FEE_URL,
          type: "GET"
        });
      },

      verifyCardRequest: function(data, callback){
        this.server.makeRequest({
          server: 'payments',
          action: 'verifyCardRequest',
          defaults: {},
          required: [],
          success: function(serverData, httpStatus){
            callback.call(this,null,serverData);
          },
          error: function(httpStatus, errorCode, responseJSON){
            callback.call(this,{status:httpStatus, errorCode: errorCode, responseJSON: responseJSON});
          },
          data: data,
          url: VERIFY_CARD_REQUEST_URL,
          type: "GET"
        });
      }
    });

    return PaymentsServer;

});
ABINE_DNTME.define("abine/phoneServer",
  ['documentcloud/underscore', 'abine/ajax', 'abine/config', 'abine/server', 'abine/core'],
  function (_, abineAjax, config, abineServer, abineCore) {

    var HOST = config.protocol + config.phoneServerHost;
    var PHONE_URL =  HOST + "/api/v3/shieldedphones";

    var PhoneServer = abineCore.BaseClass.extend({
      initialize: function(options){
        this.options = this.options || options || {};
        this.server = this.options.server || new abineServer.server();
      },

      getPhone: function(data, callback){
        this.server.makeRequest({
          server: 'phone',
          action: 'getPhone',
          defaults: {},
          required: [],
          success: function(serverData, httpStatus){
            callback.call(this,null,serverData);
          },
          error: function(httpStatus, errorCode, responseJSON){
            callback.call(this,{status:httpStatus, errorCode: errorCode, responseJSON: responseJSON});
          },
          data: data,
          url: PHONE_URL,
          type: "GET"
        });
      },

      logDisposableFilled: function(id, callback){
        this.server.makeRequest({
          server: 'phone',
          action: 'restorePhone',
          defaults: {},
          required: ['filled'],
          success: function(serverData, httpStatus){
            callback.call(this,null,serverData);
          },
          error: function(httpStatus, errorCode, responseJSON){
            callback.call(this,{status:httpStatus, errorCode: errorCode, responseJSON: responseJSON});
          },
          data: {filled: true},
          url: PHONE_URL+'/'+id,
          type: "PUT"
        });
      },

      restorePhone: function(data, callback){
        this.server.makeRequest({
          server: 'phone',
          action: 'restorePhone',
          defaults: {},
          required: ['id'],
          success: function(serverData, httpStatus){
            callback.call(this,null,serverData);
          },
          error: function(httpStatus, errorCode, responseJSON){
            callback.call(this,{status:httpStatus, errorCode: errorCode, responseJSON: responseJSON});
          },
          data: data,
          url: PHONE_URL+'/'+data.id,
          type: "PUT"
        });
      }

    });

    return PhoneServer;
});
ABINE_DNTME.define('abine/password', [], function() {
  var
    DEFAULT_PASSWORD_SIZE = 12,
    TWO_TO_THREE_UPPER = 1,
    ONE_TO_THREE_DIGITS = 2,
    ONE_TO_THREE_SPECIAL = 4,
    ONE_ALT_SPECIAL_CHAR = 8;

  var DIGITS = '0123456789';
  var SPECIAL_CHARS = '`~!@#$%^&*()_-+={}|[]\\:";\'<>?,/';
  var ALT_SPECIAL_CHARS = '.-_';
  var UPPER_CASE_CHARS = 'ABCDEFGHJKLMNOPQRSTUVWXYZ';

  var DEFAULT_PASSWORD_CHARSET = UPPER_CASE_CHARS.toLowerCase();

  function random(min, max) {
    if (typeof(max) == 'undefined') {
      max = min;
      min = 0;
    }
    return Math.floor(Math.random()*(max-min)+min);
  }

  function addRandomChars(password, chars, max) {
    for (var i=1;i<=max;i++) {
      var pos = random(0, password.length);
      password = [password.slice(0, pos), chars[random(chars.length)], password.slice(pos)].join('');
    }
    return password;
  }

  var Password = {

    generateRandomPassword: function(charset, passwordSz, flags) {
      charset = charset || DEFAULT_PASSWORD_CHARSET;
      if (!passwordSz || passwordSz<1) passwordSz = DEFAULT_PASSWORD_SIZE;
      var password = "";

      flags = flags || (TWO_TO_THREE_UPPER | ONE_TO_THREE_DIGITS | ONE_TO_THREE_SPECIAL);

      if (flags & TWO_TO_THREE_UPPER) {
        password = addRandomChars(password, UPPER_CASE_CHARS, random(2, 3));
      }

      if (flags & ONE_TO_THREE_SPECIAL) {
        password = addRandomChars(password, SPECIAL_CHARS, random(1, 3));
      }

      if (flags & ONE_ALT_SPECIAL_CHAR) {
        password = addRandomChars(password, ALT_SPECIAL_CHARS, 1);
      }

      if (flags & ONE_TO_THREE_DIGITS) {
        password = addRandomChars(password, DIGITS, random(1, 3));
      }

      if (password.length < passwordSz) {
        password = addRandomChars(password, charset, passwordSz-password.length);
      }

      return password;
    },

    generateSpecialCharsPassword: function() {
      return this.generateRandomPassword(null, null, TWO_TO_THREE_UPPER|ONE_TO_THREE_DIGITS|ONE_ALT_SPECIAL_CHAR);
    },

    generateNoSpecialCharsPassword: function() {
      return this.generateRandomPassword(null, null, TWO_TO_THREE_UPPER|ONE_TO_THREE_DIGITS);
    },

    generateNoDigitsPassword: function() {
      return this.generateRandomPassword(null, null, TWO_TO_THREE_UPPER);
    },

    generateNewOptionsPassword: function(length, numbers, special) {
      var charSet = TWO_TO_THREE_UPPER;
      if (numbers){
        charSet = charSet | ONE_TO_THREE_DIGITS;
      }
      if (special){
        charSet = charSet | ONE_TO_THREE_SPECIAL;
      }
      return this.generateRandomPassword(null, length, charSet);
    }

  };

  return Password;

});
ABINE_DNTME.define("abine/responseCodes",[],function(){

  return {
    // CORE
    UNKNOWN_ERROR: "unknown error",
    SUCCESS: "successful",
    ERROR: "error",
    SERVICE_UNAVAILABLE_ERROR: 'service unavailable error',
    SCHEDULED_DOWNTIME: 'scheduled downtime',
    MISSING_PARAMETERS: 'missing parameters',
    UNAUTHORIZED: "unauthorized bad token",
    NO_ACCESS_FOR_FEATURE: "no access to requested feature",

    // SECURITY MANAGER
    INVALID_PASSWORD: "invalid password",
    BIOMETRIC_AUTH_FAILED: 'biometric auth failed',

    // LICENSE
    ACCOUNT_ALREADY_EXISTS: "account already exists for that email",
    SAVE_NEW_USER_ERROR: "error saving new user",
    FAILED_CREATING_TARGET_EMAIL: "failed to create target email",
    FAILED_TO_UPDATE_USER: "error updating user",
    NO_ACCOUNT_FOUND_FOR_EMAIL: "no account found for email",
    ACCOUNT_MARKED_FOR_DELETION: "account has been marked for deletion",
    ACCOUNT_HAS_BEEN_LOCKED: "account has been locked",
    ACCOUNT_HAS_BEEN_BANNED: "account has been banned",
    WRONG_PASSWORD: "wrong password",
    INVALID_EMAIL_ADDRESS: "invalid email address",
    NO_RECURLY_ACCOUNT_FOUND: "no recurly account found",
    MFA_CODE_REQUIRED: "mfa code required",

    // SYNC
    DOWNGRADE_SCHEMA_NOT_ALLOWED: 'downgrade of schema_version is not allowed',
    SAVE_CLIENT_DATA_ERROR: "errors saving client data",
    DESTROY_CLIENT_DATA_ERROR: "errors destroying client data",
    NO_CLIENT_DATA_FOUND: "no client data found",
    USER_EXISTS_FOR_CLIENT_DATA_DELETION: "cannot destroy client data when user exists",
    UPGRADE_REQUIRED: "upgrade required",

    // PHONE
    PHONE_NOT_FOUND: "phone not found",
    NO_DISPOSABLE_PHONE: "no disposable phone",
    FAILED_TO_SAVE_PHONE: "failed to save phone",
    PHONE_ALREADY_VALIDATED: "phone already validated",
    NO_NUMBERS_AVAILABLE: "no numbers available",
    FAILED_TO_SEND_VERIFICATION_CODE: "failed to send verification code",
    WRONG_VERIFICATION_CODE: "wrong verification code",
    OLD_PHONE_NOT_FOUND: "old phone not found",
    COUNTRY_NOT_SUPPORTED: "country not supported",
    COUNTRY_CHANGE_NOT_SUPPORTED: "country change not supported",
    ERROR_CREATING_DISPOSABLE: "error creating disposable",

    MISSING_USER_PARAMS_ERROR_CODE: 'missing user params',
    NO_BILLING_ACCOUNT_FOR_USER_ERROR_CODE: 'no billing account for user',
    REQUEST_ALREADY_EXISTS_ERROR_CODE: 'request already exists',
    NO_CARDS_AVAILABLE_ERROR_CODE: 'no cards available',
    REQUESTED_AMOUNT_OVER_LIMIT_ERROR_CODE: 'requested amount over limit',
    REQUESTED_AMOUNT_BELOW_MINIMUM_ERROR_CODE: 'requested amount below minimum',
    REQUESTED_AMOUNT_INVALID_ERROR_CODE: 'requested amount invalid',
    BILLING_FAILED_ERROR_CODE: 'billing failed',
    BILLING_DECLINED_ERROR_CODE: 'billing declined',
    BILLING_COMPANY_FAILED_ERROR_CODE: 'billing company failed',
    NO_CARD_FOUND_ERROR_CODE: 'no card found',
    NO_BILLING_TRANSACTION_FOUND_ERROR_CODE: 'no billing transaction found',
    FRAUD_SOFT_LIMIT_REACHED: 'fraud soft limit',
    REFUND_FAILED_ERROR_CODE: 'refund failed',
    PROVIDER_CANCEL_FAILED_ERROR_CODE: 'provider cancel failed',
    PROVIDER_READ_FAILED_ERROR_CODE: 'provider read failed',
    PROVIDER_CARD_CREATION_FAILED_ERROR_CODE: 'provider card creation failed',
    FRAUD_HARD_LIMIT_REACHED: 'fraud hard limit reached',
    CARD_ALREADY_DEACTIVATED_ERROR_CODE: 'card already deactivated',
    FRAUD_VERIFIED_SOFT_LIMIT_REACHED: 'fraud verified soft limit',
    FRAUD_PLATFORM_UNAVAILABLE: 'fraud verification unavailable',
    FRAUD_HIGH_RISK_COUNTRY_ERROR_CODE: 'high risk country ban',
    BILLING_IN_NON_US_ERROR_CODE: 'billing in non us',
    GREY_LISTED_ERROR_CODE: 'grey listed user',
    GLOBAL_SOFT_LIMIT_REACHED: 'global soft limit',
    DUPLICATE_CARD_ERROR_CODE: 'duplicate card used ban',
    REJECTED_DOMAIN_ERROR_CODE: 'domains known to not accept masked cards',
    BANNED_DOMAIN_ERROR_CODE: 'banned domain',

    // MAPPING
    // never fails!

    //SPLIT TESTS
    SPLIT_INTRUSIVE_NON_INTRUSIVE: '7',
    SPLIT_INTRUSIVE_CONTROL: '8',
    SPLIT_INTRUSIVE_ALWAYS_REG: '9',
    SPLIT_INTRUSIVE_GOT_IT_REG: '10',
    SPLIT_INTRUSIVE_COMPLETE_SETUP_BUTTON: '11',

    SPLIT_WALLET_OFF: "12",
    SPLIT_WALLET_ON: "13",

    SPLIT_ICON_PANELS_CLICK_ICON_FOR_PANEL: '32',
    SPLIT_ICON_PANELS_SHOW_ICON_WITH_NORMAL_PANELS: '33',
    SPLIT_ICON_PANELS_NORMAL: '34'
  };
});
ABINE_DNTME.define("abine/saveForms",
  ['documentcloud/underscore', 'abine/formSubmitDetector', 'abine/core',
    'storage', 'persistence', 'abine/url',
    'abine/securityManager', 'abine/timer', 'abine/feedbackLoop'],
  function(_, abineFormSubmitDetector, abineCore,
           storage, persistence, abineUrl,
           securityManager, abineTimer, FeedbackLoop) {

    var tabAccounts = {};
    var tabNotifications = {};
    var notificationTimeouts = {};
    var autoLoginAccounts = {};

    function isMatchingForm(formData, formSubmitData) {
      if (formData.formSignature == formSubmitData.formSignature) {
        return true;
      }
      var match = 0, mismatch = 0;
      _.each(formData.fields, function(signature, dataType){
        if (dataType in formSubmitData.fields) {
          match++;
        } else {
          mismatch++;
        }
      });
      return ((match >= 2 && match > mismatch) || (match == 1 && match >= mismatch));
    }

    function handlePageChange(data, sender) {
      LOG_FINEST && ABINE_DNTME.log.finest("saveForms:init processed unsaved accounts on contentManager:init");
      //if there is a temp account for this tab...
      if (tabAccounts[sender]) {
        tabAccounts[sender].timesLoaded = tabAccounts[sender].timesLoaded ? tabAccounts[sender].timesLoaded + 1 : 1;

        // delete if we've held on to 4x or if it's just an autofilled login account that we don't need to manage
        if(tabAccounts[sender].timesLoaded > 4 || (tabAccounts[sender].origin == 'MaskMe' && tabAccounts[sender].type == 'loginForm')){
          //(for multipage logins, try to hold onto domain for up to 3x before removing it even if domain hasn't changed)
          deleteTabAccountFromTabAccounts(sender, 'tab account has already been loaded >4 times or origin was autofill');
          return;
        }

        LOG_DEBUG && ABINE_DNTME.log.debug("temp account " + tabAccounts[sender].page_host + " for tab " + sender + " in map");
        try{LOG_FINEST && ABINE_DNTME.log.finest("temp account: " + JSON.stringify(tabAccounts[sender]));}catch(e){LOG_FINEST && ABINE_DNTME.log.finest("could not stringify tabAccount");}

        // save account
        processFormSubmit({tabId: sender, formData: tabAccounts[sender], currentDomain: data.domain}, true);
      }
    }

    var SaveForms = abineCore.BaseClass.extend({
      init: function(backgroundMessenger) {
        LOG_FINEST && ABINE_DNTME.log.finest("saveForms:init enter");
        this.messenger = backgroundMessenger;

        // save login accounts for this tab
        this.messenger.on('contentManager:init', handlePageChange);
        this.messenger.on('contentManager:reinit', handlePageChange);

        // detect login failure
        this.messenger.on('contentManager:formsInitialized', function(formsData, sender) {
          LOG_FINEST && ABINE_DNTME.log.finest("contentManager:formsInitialized heard");
          //if there is a notification for this tab
          var tabNotification = tabNotifications[sender];
          if (tabNotification) {
            var tabAccount = tabNotification.account;
            var canceled = formsData.hasLogin;

            if (tabAccount.type != 'loginForm'){
              LOG_DEBUG && ABINE_DNTME.log.debug('formsData.hasLogin is false, check for matching login forms');
              // skip cancellation checks - the logic for login forms seems to seeing if registration succeeded/failed
              showCurrentTabNotification(sender);
            } else {
              if (tabNotification.type != 'password_with_no_login'){ // ignore failed login checks for notifications with associating generated password with no login notifications
                if (!canceled) {
                  LOG_DEBUG && ABINE_DNTME.log.debug('formsData.hasLogin is false, check for matching login forms');
                  _.each(formsData.forms, function(form) {
                    if (!canceled && form.type == "loginForm" && form.visible && isMatchingForm(form, tabAccount) && form.id != tabAccount.id) {
                      LOG_DEBUG && ABINE_DNTME.log.debug("form submit failure detected by matching form");
                      tabAccount.timesLoaded = tabAccount.timesLoaded ? tabAccount.timesLoaded - 1 : null; // decrement how many times we've loaded this if we loaded again because of form submit failure
                      canceled = true;
                    }
                  });
                } else {
                  LOG_DEBUG && ABINE_DNTME.log.debug("form submit failure detected by presence of log-in link");
                  tabAccount.timesLoaded = tabAccount.timesLoaded ? tabAccount.timesLoaded - 1 : null; // decrement how many times we've loaded this if we loaded again because of form submit failure
                }

                if (!canceled) {
                  showCurrentTabNotification(sender);
                } else {
                  // clear any existing notification since account save is canceled
                  LOG_DEBUG && ABINE_DNTME.log.debug('cancelling notification');
                  abineTimer.clearTimeout(notificationTimeouts[sender]);
                  delete notificationTimeouts[sender];
                  delete tabNotifications[sender];
                }
              } else {
                showCurrentTabNotification(sender);
              }
            }

          } else if (autoLoginAccounts[sender]) { // we just tried to autofill and autologin with an account. let the user know if we think they need to update their account info if failed
            var canceled = false;
            LOG_DEBUG && ABINE_DNTME.log.debug('check for matching login forms');
            _.each(formsData.forms, function(form) {
              if (!canceled && form.type == "loginForm" && form.visible && isMatchingForm(form, autoLoginAccounts[sender]) && form.id != autoLoginAccounts[sender].id) {
                LOG_DEBUG && ABINE_DNTME.log.debug("form submit failure detected by matching form");
                canceled = true;
              }
            });
            if (canceled){
              // display a growl to ask if user wants to update their account if they were trying to login with a login account
              var notification = {
                type: 'failed_login',
                account: autoLoginAccounts[sender],
                neverFade: true,
                panelWidth: 420,
                locked: securityManager.isLocked()
              };
              saveForms.messenger.send('background:saveForm:notification', notification, sender);
            }
          }
          delete autoLoginAccounts[sender];
        });

        this.messenger.on('background:notification:clear', function(data, sender){
          delete tabNotifications[sender];
        });

        this.messenger.on('background:save:account', function(data, sender){
          LOG_FINEST && ABINE_DNTME.log.finest("saveForms: process background:save:account event");
          delete tabNotifications[sender];
          if (data.pageUrl && data.pageUrl.match(/^http[s]?:\/\//i))
            data.login_url = data.pageUrl;
          saveTabAccount(data);
          if (data.alwaysSaveAccounts) {
            storage.Preference.createOrModify({key: 'promptWhenSavingAccounts', value: data.alwaysSaveAccounts?'false':'true'});
          }
        });

        this.messenger.on('background:save:or:update:account', function(data, sender){
          // check if account already exists again. for case if user was trying to save account but not logged in
          // after user logged in, check again on this call to see if we should update instead of overwriting
          findAccount(data, function(account){
            if (!account) {
              delete tabNotifications[sender];
              if (data.pageUrl && data.pageUrl.match(/^http[s]?:\/\//i))
                data.login_url = data.pageUrl;
              saveTabAccount(data);
              return;
            }
            if (account.isCompanyAccount) {
              // no need to update password for company account
              return;
            }

            var tabAccount = data;
            var notificationType;
            if (account.password != tabAccount.password) {
              if (account.password && account.password.length > 0) {
                // password changed.  ask if they want it to be saved.
                notificationType = 'change_password';
              } else {
                // setting password for first time.  so just save it. (eg: espn.go.com)
                saveTabAccount(tabAccount);
              }
            } else {

            }

            if (notificationType) {
              var notification = {
                type: notificationType,
                account: tabAccount,
                neverFade: true,
                panelWidth: 420,
                locked: securityManager.isLocked()
              };

              saveForms.messenger.send('background:saveForm:notification', notification, sender);
            }
          });
        });
      }
    });

    function deleteTabAccountFromTabAccounts(tabId, debugReason) { // for easier debugging
      debugReason = debugReason || "";
      LOG_FINEST && ABINE_DNTME.log.finest("saveForms:deleteTabAccountFromTabAccounts enter: " + debugReason);
      delete tabAccounts[tabId];
      LOG_FINEST && ABINE_DNTME.log.finest("saveForms:deleteTabAccountFromTabAccounts return");
    }

    function processFormSubmit(payload, saveLogin) {
      LOG_FINEST && ABINE_DNTME.log.finest("saveForms:processFormSubmit enter");
      LOG_FINEST && ABINE_DNTME.log.finest("saveForms:processFormSubmit saveLogin?: " + saveLogin);
      var tabId = payload.tabId;
      var currentDomain = payload.currentDomain;
      payload = payload.formData;

      if (payload.origin == 'MaskMe' && payload.type == 'loginForm') { // we autofilled for them the data... will this ever be wrong or partially true for the form?
        autoLoginAccounts[tabId] = payload;
        return;
      }

      var tabAccount = tabAccounts[tabId]; // payload and tabAccount are same when processing due to page load + existing tab account
      if (tabAccount) {
        payload.metrics = tabAccount.metrics;
        payload.accountId = payload.accountId || tabAccount.accountId;
      } else {
        tabAccount = {};
      }
      if (!tabAccount && !isAccountComplete(payload)) {
        LOG_FINEST && ABINE_DNTME.log.finest("saveForms:processFormSubmit nothing to save");
        return;
      }

      // multi page login try to merge old data with new payload do it here and in field submit changes
      if((tabAccount.type == 'loginForm' && payload.type == 'loginForm') || (tabAccount.type == 'registrationForm' && payload.type == 'registrationForm')) {
        LOG_FINEST && ABINE_DNTME.log.finest("saveForms:processFormSubmit temp account and payload type are both login or both registration: try to merge any data");
        if (tabAccount.domain == payload.domain) {
          LOG_FINEST && ABINE_DNTME.log.finest("saveForms:processFormSubmit attempt to merge temp account and payload");
          payload = mergeData(tabAccount, payload);
          tabAccount = payload;
          if(isAccountComplete(payload) || isChangePasswordComplete(payload)){
            saveLogin = true; // we received password in form submit and not previous temp account - try to save login now.
          }
        }
      } else if (payload.generated_password && payload.password && payload.password.length > 0){
        LOG_FINEST && ABINE_DNTME.log.finest("saveForms:processFormSubmit password was generated so need to save it");
        saveLogin = true; // attempt to save the password (check to see if it should be associated with a username on detected form submits
      } else if (isAccountComplete(payload) || isChangePasswordComplete(payload)) {
        LOG_FINEST && ABINE_DNTME.log.finest("saveForms:processFormSubmit payload is complete already so see if we should default to timeout case and ask to save");
        saveLogin = true; // ask to save login/registration even if there's no page load - it will default to the timeout case (4000 ms before this code runs which may cancel save)
      }

      var domain = payload.domain;
      storage.Blacklist.findBy("domain", domain, function(blacklist){
        var blacklisted = blacklist? blacklist.save_account: false;
        storage.Preference.findBy('key', 'rememberAccounts', function(pref){
          if (!tabAccount.saveRequired && (!pref || pref.isFalse() || blacklisted)) {
            LOG_FINEST && ABINE_DNTME.log.finest("saveForms:processFormSubmit abort");
            return;
          }
          if (tabAccount.saveRequired) {
            saveTabAccount(payload);
            deleteTabAccountFromTabAccounts(tabId, 'form submit saveRequired was true - autosaved');
            if (!(payload.username || payload.email) && payload.generated_password && payload.password && payload.password.length > 0) {
              // detected a generated password on form submit of registration form but a username and email wasn't in payload
              // this is probably a multistep registration and user generated password - let's ask if the password should be associated with a username
              //findEmailOrUserOnlyAccounts(payload, function (accounts) {
                LOG_FINEST && ABINE_DNTME.log.finest("saveForms:processFormSubmit ask for saving password with no login");
                var notificationType = 'password_with_no_login';
                var notification = {
                  type: notificationType,
                  account: payload,
                  //loginAccounts: accounts,
                  neverFade: true,
                  panelWidth: 420,
                  locked: securityManager.isLocked()
                };
                tabNotifications[tabId] = notification;
              //});
            }
          } else if (saveLogin){
            LOG_FINEST && ABINE_DNTME.log.finest("saveForms:processFormSubmit save login flow entered");
            if(isAccountComplete(payload) || isChangePasswordComplete(payload)) { // check if payload is complete again in case we merged some data
              LOG_FINEST && ABINE_DNTME.log.finest("saveForms:processFormSubmit account complete or change password complete flow");
              findAccount(payload, function(account){
                var notificationType = null;
                var usernamesAreDifferent = function(account, payload){
                  return account.username && account.username.length > 0 && payload.username && payload.username.length > 0 && account.username != payload.username;
                };

                var emailsAreDifferent = function(account, payload){
                  return account.email && account.email.length > 0 && payload.email && payload.email.length > 0 && account.email != payload.email;
                };

                if (!account || usernamesAreDifferent(account, payload) || emailsAreDifferent(account, payload)) {
                  storage.Preference.findBy('key', 'promptWhenSavingAccounts', function(promptPref){
                    if (securityManager.isLocked() || (promptPref && promptPref.isTrue())){
                      // no account for email/username.  so ask if they want it to be saved.
                      LOG_FINEST && ABINE_DNTME.log.finest("saveForms:processFormSubmit ask for saving login");

                      notificationType = 'login';
                      var notification = {
                        type: notificationType,
                        account: payload,
                        neverFade: true,
                        panelWidth: 420,
                        locked: securityManager.isLocked()
                      };
                      tabNotifications[tabId] = notification;
                    } else {
                      saveTabAccount(payload);
                      deleteTabAccountFromTabAccounts(tabId, 'formSubmit promptWhenSaving is off - autosaved');
                    }
                  })
                } else if (account.password != payload.password) {
                  if (account.password && account.password.length > 0) {
                    // password changed.  ask if they want it to be saved.
                    LOG_FINEST && ABINE_DNTME.log.finest("saveForms:processFormSubmit ask for saving changed password");
                    notificationType = 'change_password';
                    var notification = {
                      type: notificationType,
                      account: payload,
                      neverFade: true,
                      panelWidth: 420,
                      locked: securityManager.isLocked()
                    };
                    tabNotifications[tabId] = notification;
                  } else {
                    // setting password for first time.  so just save it. (eg: espn.go.com)
                    LOG_FINEST && ABINE_DNTME.log.finest("saveForms:processFormSubmit save account as old password is empty");
                    saveTabAccount(payload);
                  }
                }

                if(tabNotifications[tabId]){
                  // see if we should save in X secs for form submit events without new page loads
                  abineTimer.clearTimeout(notificationTimeouts[tabId]);
                  var notificationTimeout = abineTimer.setTimeout(function(){
                    LOG_FINEST && ABINE_DNTME.log.finest("saveForms:processFormSubmit showing current tab notification after 4000 ms with no new form load");
                    showCurrentTabNotification(tabId);
                  }, 4000);
                  notificationTimeouts[tabId] = notificationTimeout;
                }
              });
              deleteTabAccountFromTabAccounts(tabId, 'form submit created save notification already');
            }  else if (!(payload.username || payload.email) && payload.generated_password && payload.password && payload.password.length > 0){
              // detected a generated password on form submit of registration form but a username and email wasn't in payload
              // this is probably a multistep registration and user generated password - let's ask if the password should be associated with a username
                LOG_FINEST && ABINE_DNTME.log.finest("saveForms:processFormSubmit ask for saving password with no login");
                var notificationType = 'password_with_no_login';
                var notification = {
                  type: notificationType,
                  account: payload,
                  neverFade: true,
                  panelWidth: 420,
                  locked: securityManager.isLocked()
                };
                tabNotifications[tabId] = notification;
            } else {
              // account not complete but we tried to save - hold on til next time if we're still on the same domain
              // for multipage login saving
              LOG_FINEST && ABINE_DNTME.log.finest("saveForms:processFormSubmit holding on to partial account for multipage login");
              if(tabAccount.domain != currentDomain){
                LOG_FINEST && ABINE_DNTME.log.finest("saveForms:processFormSubmit domain changed delete any tab account");
                deleteTabAccountFromTabAccounts(tabId, 'domain has changed on tab')
              }
            }
          }
          LOG_FINEST && ABINE_DNTME.log.finest("saveForms:processFormSubmit return");
        });
      });
    }

    function showCurrentTabNotification(tabId) {
      LOG_FINEST && ABINE_DNTME.log.finest("saveForms:showCurrentTabNotification enter");

      abineTimer.clearTimeout(notificationTimeouts[tabId]);
      delete notificationTimeouts[tabId]

      var tabNotification = tabNotifications[tabId];
      if(tabNotification){
        saveForms.messenger.send('background:saveForm:notification', tabNotification, tabId);
      }
      LOG_FINEST && ABINE_DNTME.log.finest("saveForms:showCurrentTabNotification return");
    }

    function isAccountComplete(tabAccount) {
      // save if we have all fields.
      var emailDone = tabAccount.email && tabAccount.email.length > 0;
      var passwordDone = tabAccount.password && tabAccount.password.length > 0;
      var usernameDone = tabAccount.username && tabAccount.username.length > 0;
      return (passwordDone && (emailDone || usernameDone)) || (passwordDone && tabAccount.generated_password);
    }

    // method to merge data for when new payload is missing fields that previous payload/current tabaccount had.
    function mergeData(previousPayload, newPayload) {
      newPayload.username = newPayload.username || previousPayload.username;
      newPayload.email = newPayload.email || previousPayload.email;
      newPayload.password = newPayload.password || previousPayload.password;
      LOG_FINEST && ABINE_DNTME.log.finest("saveForms:mergeData merged temp account and payload");
      return newPayload;
    }

    function isChangePasswordComplete(tabAccount) {
        return tabAccount.type == 'changePasswordForm' && tabAccount.password && tabAccount.password.length > 0 && tabAccount.oldpassword && tabAccount.oldpassword.length > 0;
    }

    function processFieldSubmit(payload) {
      LOG_FINEST && ABINE_DNTME.log.finest("saveForms:processFieldSubmit enter");
      var tabAccount = tabAccounts[payload.tabId] || {};
      if (tabAccount.saveInProgress) {
        LOG_FINEST && ABINE_DNTME.log.finest("saveForms:processFieldSubmit delay processing while save account is in progress");
        // delay processing while save account is in progress, otherwise it will affect panel metrics
        abineTimer.setTimeout(function(){processFieldSubmit(payload)}, 100);
        return;
      }
      // easter egg to update mapping
      if (payload.formData.email && payload.formData.email == 'mapping-update@abine.com') {
        ABINE_DNTME.require(['modules'], function(modules){
          modules.mappingsModule.forceMappingUpdate(function(){
            saveForms.messenger.send('background:dntme:show');
          });
        });
        return;
      }
      if (tabAccount && tabAccount.id != payload.formData.id) {
        if (tabAccount.saveRequired && isAccountComplete(tabAccount)) {
          saveTabAccount(tabAccount);
          deleteTabAccountFromTabAccounts(payload.tabId, 'processFieldSubmit tabaccount and form data id is different and saveRequired and account is complete - autosaved');
          tabAccount = {};
        }
      }
      var saveRequired = false;
      var origin = payload.fieldData.origin;
      var isPhoneField = payload.fieldData.dataType && payload.fieldData.dataType.match(/phone/);
      if (tabAccount.saveRequired || (origin && origin.match(/disposable|primary|strong/i) && !isPhoneField)) {
        saveRequired = true;
      }
      var savedMetrics = tabAccount.metrics || {};
      var oldAccountId = tabAccount.accountId;

      // multi page login case. stored username last time with no password
      // now seeing password with no username, add username in there if domains are same
      if((tabAccount.type == 'loginForm' && payload.formData.type == 'loginForm') || (tabAccount.type == 'registrationForm' && payload.formData.type == 'registrationForm')) {
        LOG_FINEST && ABINE_DNTME.log.finest("saveForms:processFieldSubmit temp account and payload type are both login or both registration: try to merge any data");
        if (tabAccount.domain == payload.formData.domain) {
          payload.formData = mergeData(tabAccount, payload.formData);
        }
      }

      tabAccount = payload.formData; // lose old tabAccount reference

      if (oldAccountId || payload.fieldData.accountId) {
        tabAccount.accountId = payload.fieldData.accountId || oldAccountId;
      }

      tabAccount.saveRequired = saveRequired;
      tabAccount.metrics = savedMetrics;
      tabAccounts[payload.tabId] = tabAccount;
      if (saveRequired && isAccountComplete(tabAccount)) {
        saveTabAccount(tabAccount);
        deleteTabAccountFromTabAccounts(payload.tabId, 'processFieldSubmit saverequired and account complete');
      }
    }

    function findAccount(tabAccount, callback) {
      var accountId = tabAccount.accountId || tabAccount.id;
      storage.Account.findBy('id', accountId, function(account) {
        if (!account) {
          storage.CompanyAccount.findBy('id', accountId, function(account) {
            if (!account) {
              var accountFilter = function(account){
                return (tabAccount.domain == account.domain) &&
                  (
                  (tabAccount.email && tabAccount.email.length > 0 && (account.email == tabAccount.email || account.username == tabAccount.email)) ||
                  (tabAccount.username && tabAccount.username.length > 0 && (account.username == tabAccount.username || account.email == tabAccount.username)) ||
                  (tabAccount.oldpassword && tabAccount.oldpassword.length > 0 && (account.password == tabAccount.oldpassword))
                  );
              };
              storage.Account.listCached(accountFilter, function(accounts) {
                if (accounts.length == 0) {
                  storage.CompanyAccount.listCached(accountFilter, function(accounts) {
                    if (accounts.length == 0) {
                      callback(null);
                    } else {
                      callback(accounts[0]);
                    }
                  });
                } else {
                  callback(accounts[0]);
                }
              });
              return;
            }
            callback(account);
          });
          return;
        }
        callback(account);
      });
    }

    function saveTabAccount(tabAccount) {
      LOG_FINEST && ABINE_DNTME.log.finest("saveForms:saveTabAccount entered");
      if (!tabAccount.email && !tabAccount.username && !tabAccount.password) {
        LOG_FINEST && ABINE_DNTME.log.finest("saveForms:saveTabAccount never save empty account");
        return;
      }
      storage.Blacklist.findBy("domain", tabAccount.domain, function(blacklist){
        var blacklisted = blacklist? blacklist.save_account: false;
        storage.Preference.findBy('key', 'rememberAccounts', function(pref) {
          if ((!pref || pref.isFalse()) || blacklisted) {
            LOG_FINEST && ABINE_DNTME.log.finest("saveForms:saveTabAccount abort");
            return;
          } else {
            tabAccount.saveInProgress = true;
            function saveAccount(account) {
              account.domain = tabAccount.domain;
              account.page_host = tabAccount.page_host;
              account.form_host = tabAccount.form_host;
              account.protocol = tabAccount.protocol;

              var changePasswordForm = tabAccount.type == 'changePasswordForm';
              if (!changePasswordForm) {
                if (tabAccount.email && tabAccount.email.length > 0)
                  account.email = tabAccount.email;

                if (tabAccount.username && tabAccount.username.length > 0)
                  account.username = tabAccount.username;

                if (tabAccount.login_url && tabAccount.login_url.length > 0)
                  account.login_url = tabAccount.login_url;

                account.times_used = account.times_used||0;
              }

              if (tabAccount.password && tabAccount.password.length > 0)
                account.password = tabAccount.password;

              account.pushed = 0;

              account.modifiedAt = new Date();
              account.userModifiedAt = account.modifiedAt;

              account.beforeSave();

              LOG_FINEST && ABINE_DNTME.log.finest("saveForms:saveTabAccount flushing persistence");
              persistence.flush();

              persistence.removeIndex(account);
              persistence.addIndex(account);

              // revert previously saved metrics
              var savedMetrics = tabAccount.metrics || {};
              for (var method in savedMetrics) {
                storage.PanelActivity[method]({payload:{panelId:7000005,revert:true}});
              }

              tabAccount.metrics = {};
              // BL: save PWM engagement as panel data
              var disposableEmail = tabAccount.email && (tabAccount.generated_email || tabAccount.email.indexOf(ABINE_DNTME.config.disposableDomain) != -1);
              if(tabAccount.generated_password && disposableEmail){ // both credentials were masked
                tabAccount.metrics['addMasked'] = true;
              } else if (tabAccount.generated_password){ // only password was masked
                tabAccount.metrics['addClosed'] = true;
              } else if (disposableEmail){ // only email was masked
                tabAccount.metrics['addImpression'] = true;
              } else { // neither were masked credentials
                tabAccount.metrics['addDisclosed'] = true;
              }

              // apply new metrics
              for (var method in tabAccount.metrics) {
                storage.PanelActivity[method]({payload:{panelId:7000005}});
              }

              delete tabAccount.saveInProgress;
              LOG_FINEST && ABINE_DNTME.log.finest("saveForms:saveTabAccount finished");
            }

            findAccount(tabAccount, function(account){
              if (!account) {
                account = new storage.Account({id: tabAccount.id});
                account.createdAt = new Date();
                account.email = account.password = account.username = account.login_url = '';
                //storage.Account.findBy('domain', tabAccount.domain, function(matchingAccount) {
                //  if (matchingAccount && matchingAccount.label && matchingAccount.label.length > 0 && matchingAccount.label != tabAccount.domain) {
                //    account.label = matchingAccount.label;
                //    persistence.flush();
                //  }
                //});
                persistence.add(account);
              }
              saveAccount(account);
            });
          }
         });
      });
    }

    var saveForms = new SaveForms();

    // hook to form submit events
    abineFormSubmitDetector.formSubmitDetector.on('background:formSubmit', processFormSubmit);
    abineFormSubmitDetector.formSubmitDetector.on('background:formFieldChanged', processFieldSubmit);

    return saveForms;
  });
ABINE_DNTME.define("abine/savePaymentForms",
  ['documentcloud/underscore', 'abine/formSubmitDetector', 'abine/core',
    'storage', 'persistence', 'abine/url',
    'abine/securityManager', 'abine/timer'],
  function(_, abineFormSubmitDetector, abineCore,
           storage, persistence, abineUrl,
           securityManager, abineTimer) {

    var tabCards = {};
    var tabNotifications = {};
    var tabMaskedCards = {};
    var notificationTimeouts = {};

    function verifyLuhnNumber(number) {
      var sum = 0, alt = false;
      var i = number.length - 1;
      while (i >= 0) {
        var digit = parseInt(number.charAt(i));
        if (alt) {
          digit *= 2;
          if (digit > 9) {
            digit -= 9;
          }
        }
        sum += digit;
        i--;
        alt = !alt;
      }
      return (sum % 10) == 0;
    }


    function isMatchingForm(formData, formSubmitData) {
      if (formData.formSignature == formSubmitData.formSignature) {
        return true;
      }
      var match = 0, mismatch = 0;
      _.each(formData.fields, function(signature, dataType){
        if (dataType in formSubmitData.fields) {
          match++;
        } else {
          mismatch++;
        }
      });
      return ((match >= 3 && match > mismatch));
    }

    function handlePageChange(data, sender) {
      LOG_FINEST && ABINE_DNTME.log.finest("savePaymentForms:init processed unsaved cards on contentManager:init");
      //if there is a temp card for this tab...
      var tabCard = tabCards[sender];
      if (tabCard) {
        findCard(tabCard, function(card){
          if (card && card._type == 'DisposableCards') {
            tabCard.card = card;
            tabMaskedCards[sender] = tabCard;
          }
          LOG_DEBUG && ABINE_DNTME.log.debug("temp card " + tabCard.page_host + " for tab " + sender + " in map");
          try{LOG_FINEST && ABINE_DNTME.log.finest("temp card: " + JSON.stringify(tabCard));}catch(e){LOG_FINEST && ABINE_DNTME.log.finest("could not stringify tabCard");}

          // save card
          processFormSubmit({tabId: sender, formData: tabCard}, true);
        });
      }
    }

    var SavePaymentForms = abineCore.BaseClass.extend({
      init: function(backgroundMessenger) {
        LOG_FINEST && ABINE_DNTME.log.finest("savePaymentForms:init enter");
        this.messenger = backgroundMessenger;

        // save card for this tab
        this.messenger.on('contentManager:init', handlePageChange);
        this.messenger.on('contentManager:reinit', handlePageChange);

        // detect submit failure
        this.messenger.on('contentManager:formsInitialized', function(formsData, sender) {
          LOG_FINEST && ABINE_DNTME.log.finest("contentManager:formsInitialized heard");
          if (tabMaskedCards[sender]) {
            var tabCard = tabMaskedCards[sender];
            var canceled = false;
            _.each(formsData.forms, function(form) {
              if (!canceled && isMatchingForm(form, tabCard)) {
                LOG_DEBUG && ABINE_DNTME.log.debug("masked card: form submit failure detected by matching form");
                canceled = true;
              }
            });
            if (canceled) {
              showMaskedCardDeclinedNotification(tabCard, sender);
            }
            delete tabMaskedCards[sender];
            return;
          }
          //if there is a notification for this tab
          var tabNotification = tabNotifications[sender];
          if (tabNotification) {
            var tabCard = tabNotification.card;
            var canceled = false;
            _.each(formsData.forms, function(form) {
              if (!canceled && isMatchingForm(form, tabCard)) {
                LOG_DEBUG && ABINE_DNTME.log.debug("form submit failure detected by matching form");
                canceled = true;
              }
            });
            if (!canceled) {
              showCurrentTabNotification(sender);
            } else {
              // clear any existing notification since card save is canceled
              LOG_DEBUG && ABINE_DNTME.log.debug('cancelling notification');
              abineTimer.clearTimeout(notificationTimeouts[sender]);
              delete notificationTimeouts[sender];
              delete tabNotifications[sender];

              // remove card if there was a failure detected
              deleteTabCardFromTabCards(sender, 'form submit failed');
            }

          }
        });

        this.messenger.on('background:notification:clear', function(data, sender){
          delete tabNotifications[sender];
        });

        this.messenger.on('background:save:card', function(data, sender){
          LOG_FINEST && ABINE_DNTME.log.finest("savePaymentForms: process background:save:card event");
          delete tabNotifications[sender];
          saveTabCard(data);
          if ('alwaysSaveCards' in data) {
            storage.Preference.createOrModify({key: 'promptWhenSavingCards', value: data.alwaysSaveCards?'false':'true'});
          }
        });

        this.messenger.on('background:save:or:update:card', function(data, sender){
          // check if card already exists again. for case if user was trying to save card but not logged in
          // after user logged in, check again on this call to see if we should add or ignore
          findCard(data, function(card){
            if (!card) {
              delete tabNotifications[sender];
              saveTabCard(data);
            }
          });
        })
      }
    });

    function showMaskedCardDeclinedNotification(tabCard, tabId) {
      LOG_FINEST && ABINE_DNTME.log.finest("savePaymentForms:showMaskedCardDeclinedNotification enter: ");
      var notification = {
        msg: "",
        targetUrl: ABINE_DNTME.config.protocol + ABINE_DNTME.config.webAppHost + "/#/cards",
        targetId: "",
        model: {maskedCard: tabCard.card.toJSON(), declined: {
          title: "Masked Card Declined?"
        }},
        height: 260,
        type: 'card',
        neverFade: true,
        locked: securityManager.isLocked()
      };
      savePaymentForms.messenger.send('background:show:notification', notification, tabId);

      LOG_FINEST && ABINE_DNTME.log.finest("savePaymentForms:showMaskedCardDeclinedNotification exit: ");
    }

    function deleteTabCardFromTabCards(tabId, debugReason) { // for easier debugging
      debugReason = debugReason || "";
      LOG_FINEST && ABINE_DNTME.log.finest("savePaymentForms:deleteTabCardFromTabCards enter: " + debugReason);
      delete tabCards[tabId];
      LOG_FINEST && ABINE_DNTME.log.finest("savePaymentForms:deleteTabCardFromTabCards return");
    }

    function processFormSubmit(payload, saveCard) {
      LOG_FINEST && ABINE_DNTME.log.finest("savePaymentForms:processFormSubmit enter");
      LOG_FINEST && ABINE_DNTME.log.finest("savePaymentForms:processFormSubmit saveCard?: " + saveCard);
      var tabId = payload.tabId;
      payload = payload.formData;

      var tabCard = tabCards[tabId]; // payload and tabCard are same when processing due to page load + existing tab card
      if (tabCard) {
        payload.metrics = tabCard.metrics;
        payload.cardId = payload.cardId || tabCard.cardId;
        _.each(['expiry_month', 'expiry_year', 'number', 'cvc', 'issuedto'], function(key){
          tabCards[tabId][key] = payload[key] || tabCards[tabId][key];
        });
      } else {
        tabCard = {};
        tabCards[tabId] = payload;
      }

      if (!tabCard.card_type && tabCard.number) {
        tabCard.card_type = storage.RealCard.detectCardType(tabCard.number);
      }

      if (!tabCard.styles && tabCard.card_type) {
        tabCard.brand = 'generic';
        tabCard.styles = storage.RealCard.getCardStyles(tabCard.card_type, tabCard.brand);
      }

      if (!tabCard && !isCardComplete(payload)) {
        LOG_FINEST && ABINE_DNTME.log.finest("savePaymentForms:processFormSubmit nothing to save");
        return;
      }

      var domain = payload.domain;
      storage.Blacklist.findBy("domain", domain, function(blacklist){
        var blacklisted = blacklist ? blacklist.save_card: false;
        storage.Preference.findBy('key', 'rememberCards', function(pref){
          if (!tabCard.saveRequired && (!pref || pref.isFalse() || blacklisted)) {
            LOG_FINEST && ABINE_DNTME.log.finest("savePaymentForms:processFormSubmit abort");
            return;
          }
          if (tabCard.saveRequired) {
            saveTabCard(payload);
            deleteTabCardFromTabCards(tabId, 'form submit saveRequired was true - autosaved');
          } else if (saveCard){
            LOG_FINEST && ABINE_DNTME.log.finest("savePaymentForms:processFormSubmit save card flow entered");
            if(isCardComplete(payload)) { // check if payload is complete again in case we merged some data
              LOG_FINEST && ABINE_DNTME.log.finest("savePaymentForms:processFormSubmit card complete flow");
              findCard(payload, function(card){
                if (!card) {
                  storage.Preference.findBy('key', 'promptWhenSavingCards', function(promptPref){
                    if (securityManager.isLocked() || (promptPref && promptPref.isTrue())){
                      // no card.  so ask if they want it to be saved.
                      LOG_FINEST && ABINE_DNTME.log.finest("savePaymentForms:processFormSubmit ask for saving card");

                      var notification = {
                        type: 'save_card',
                        card: payload,
                        neverFade: true,
                        panelWidth: 420,
                        locked: securityManager.isLocked()
                      };
                      tabNotifications[tabId] = notification;
                    } else {
                      saveTabCard(payload);
                      deleteTabCardFromTabCards(tabId, 'formSubmit promptWhenSaving is off - autosaved');
                    }
                  })
                }

                if(tabNotifications[tabId]){
                  // see if we should save in X secs for form submit events without new page loads
                  abineTimer.clearTimeout(notificationTimeouts[tabId]);
                  var notificationTimeout = abineTimer.setTimeout(function(){
                    LOG_FINEST && ABINE_DNTME.log.finest("savePaymentForms:processFormSubmit showing current tab notification after 4000 ms with no new form load");
                    showCurrentTabNotification(tabId);
                  }, 4000);
                  notificationTimeouts[tabId] = notificationTimeout;
                }
              });
              deleteTabCardFromTabCards(tabId, 'form submit created save notification already');
            } else {
              deleteTabCardFromTabCards(tabId, 'card not complete')
            }
          }
          LOG_FINEST && ABINE_DNTME.log.finest("savePaymentForms:processFormSubmit return");
        });
      });
    }

    function showCurrentTabNotification(tabId) {
      LOG_FINEST && ABINE_DNTME.log.finest("savePaymentForms:showCurrentTabNotification enter");

      abineTimer.clearTimeout(notificationTimeouts[tabId]);
      delete notificationTimeouts[tabId];

      var tabNotification = tabNotifications[tabId];
      if(tabNotification){
        savePaymentForms.messenger.send('background:saveForm:notification', tabNotification, tabId);
      }
      LOG_FINEST && ABINE_DNTME.log.finest("savePaymentForms:showCurrentTabNotification return");
    }

    function isCardComplete(tabCard) {
      // save if we have all fields.
      var numberDone = tabCard.number && tabCard.number.length > 0 && verifyLuhnNumber(tabCard.number);
      var expiryMonthDone = tabCard.expiry_month && tabCard.expiry_month.length > 0;
      var expiryYearDone = tabCard.expiry_year && tabCard.expiry_year.length > 0;
      return numberDone && expiryMonthDone && expiryYearDone;
    }
 
    function processFieldSubmit(payload) {
      LOG_FINEST && ABINE_DNTME.log.finest("savePaymentForms:processFieldSubmit enter");
      var tabCard = tabCards[payload.tabId] || {};
      if (tabCard.saveInProgress) {
        LOG_FINEST && ABINE_DNTME.log.finest("savePaymentForms:processFieldSubmit delay processing while save is in progress");
        // delay processing while save  is in progress, otherwise it will affect panel metrics
        abineTimer.setTimeout(function(){processFieldSubmit(payload)}, 100);
        return;
      }

      if (tabCard && tabCard.id != payload.formData.id) {
        if (tabCard.saveRequired && isCardComplete(tabCard)) {
          saveTabCard(tabCard);
          deleteTabCardFromTabCards(payload.tabId, 'processFieldSubmit tabcard and form data id is different and saveRequired and card is complete - autosaved');
          tabCard = {};
        }
      }
      var saveRequired = false;
      if (tabCard.saveRequired) {
        saveRequired = true;
      }
      var savedMetrics = tabCard.metrics || {};
      var oldCardId = tabCard.cardId;

      tabCard = payload.formData; // lose old tabCard reference

      if (oldCardId || payload.fieldData.cardId) {
        tabCard.cardId = payload.fieldData.cardId || oldCardId;
      }

      tabCard.saveRequired = saveRequired;
      tabCard.metrics = savedMetrics;
      tabCards[payload.tabId] = tabCard;
      tabCard.card_type = storage.RealCard.detectCardType(tabCard.number);

      tabCard.brand = 'generic';
      tabCard.styles = storage.RealCard.getCardStyles(tabCard.card_type, tabCard.brand);

      storage.RealCard.detectCardBrand(tabCard.number, function(brand){
        tabCard.brand = brand;
        tabCard.styles = storage.RealCard.getCardStyles(tabCard.card_type, tabCard.brand);
        if (saveRequired && isCardComplete(tabCard)) {
          saveTabCard(tabCard);
          deleteTabCardFromTabCards(payload.tabId, 'processFieldSubmit saverequired and card complete');
        }
      });
    }

    function findCard(tabCard, callback) {
      var cardId = tabCard.cardId || tabCard.id;
      storage.RealCard.findBy('id', cardId, function(card) {
        if (!card) {
          storage.DisposableCard.findBy('id', cardId, function(card) {
            if (!card) {
              var cardFilter = function(card){
                return (tabCard.number == card.number);
              };
              storage.RealCard.listCached(cardFilter, function(cards) {
                if (cards.length == 0) {
                  storage.DisposableCard.listCached(cardFilter, function(cards) {
                    if (cards.length == 0) {
                      callback(null);
                    } else {
                      callback(cards[0]);
                    }
                  });
                } else {
                  callback(cards[0]);
                }
              });
              return;
            }
            callback(card);
          });
          return;
        }
        callback(card);
      });
    }

    function saveTabCard(tabCard) {
      LOG_FINEST && ABINE_DNTME.log.finest("savePaymentForms:saveTabCard entered");
      storage.Blacklist.findBy("domain", tabCard.domain, function(blacklist){
        var blacklisted = blacklist? blacklist.save_card: false;
        storage.Preference.findBy('key', 'rememberCards', function(pref) {
          if ((!pref || pref.isFalse()) || blacklisted) {
            LOG_FINEST && ABINE_DNTME.log.finest("savePaymentForms:saveTabCard abort");
            return;
          } else {
            tabCard.saveInProgress = true;
            function saveAccount(card) {
              card.domain = tabCard.domain;

              if (tabCard.number && tabCard.number.length > 0)
                card.number = tabCard.number;

              if (tabCard.cvc && tabCard.cvc.length > 0)
                card.cvc = tabCard.cvc;

              if (tabCard.expiry_month && tabCard.expiry_month.length > 0)
                card.expiry_month = tabCard.expiry_month;

              if (tabCard.expiry_year && tabCard.expiry_year.length > 0)
                card.expiry_year = tabCard.expiry_year;

              if (tabCard.issuedto && tabCard.issuedto.length > 0) {
                var parts = tabCard.issuedto.split(' ');
                card.first_name = parts[0];
                if (parts.length >= 2) {
                  card.last_name = parts[1];
                }
              }

              card.detectCardType();

              card.label = card.type.substr(0,1).toUpperCase() + card.type.replace(/[^a-z]+/g, ' ').substr(1) + ' ' + card.number.substr(-4)

              card.pushed = 0;

              card.modifiedAt = new Date();

              LOG_FINEST && ABINE_DNTME.log.finest("savePaymentForms:saveTabCard flushing persistence");
              persistence.flush();

              // revert previously saved metrics
              var savedMetrics = tabCard.metrics || {};
              for (var method in savedMetrics) {
                storage.PanelActivity[method]({payload:{panelId:8000091,revert:true}});
              }

              tabCard.metrics = {};
              tabCard.metrics['addImpression'] = true;

              // apply new metrics
              for (var method in tabCard.metrics) {
                storage.PanelActivity[method]({payload:{panelId:8000091}});
              }

              delete tabCard.saveInProgress;
              LOG_FINEST && ABINE_DNTME.log.finest("savePaymentForms:saveTabCard finished");
            }

            findCard(tabCard, function(card){
              if (!card) {
                card = new storage.RealCard({id: tabCard.id});
                card.createdAt = new Date();
                card.number = card.first_name = card.last_name = card.expiry_month = card.expiry_year = card.cvc = '';
                //storage.Account.findBy('domain', tabCard.domain, function(matchingAccount) {
                //  if (matchingAccount && matchingAccount.label && matchingAccount.label.length > 0 && matchingAccount.label != tabCard.domain) {
                //    card.label = matchingAccount.label;
                //    persistence.flush();
                //  }
                //});
                persistence.add(card);
              }
              saveAccount(card);
            });
          }
         });
      });
    }

    var savePaymentForms = new SavePaymentForms();

    // hook to form submit events
    abineFormSubmitDetector.formSubmitDetector.on('background:formSubmit', processFormSubmit);
    abineFormSubmitDetector.formSubmitDetector.on('background:formPaymentFieldChanged', processFieldSubmit);

    return savePaymentForms;
  });
ABINE_DNTME.define("abine/feedbackLoop",
  ['documentcloud/underscore', 'abine/formSubmitDetector', 'abine/core',
    'storage', 'persistence', 'abine/url',
    'abine/securityManager'],
  function(_, abineFormSubmitDetector, abineCore,
           storage, persistence, abineUrl,
           securityManager) {

    var userData = null;
    var globalUserDataRegex = null;

    var attributeToDataTypeMap = {
      "address-address1":"/contact/postaladdress",
      "address-address2":"/contact/postaladdressAdditional",
      "address-city":"/contact/city",
      "address-country":"/contact/country",
      "address-state":"/contact/state",
      "address-zip":"/contact/postalcode",
      "card-cvc": "/financial/creditcard/verification",
      "card-expiry_month":"/financial/creditcard/expirymonth",
      "card-expiry_year":"/financial/creditcard/expiryyear",
      "card-first_name":"/financial/creditcard/issuedto/first",
      "card-last_name":"/financial/creditcard/issuedto/last",
      "card-number":"/financial/creditcard/number",
      "card-type":"/financial/creditcard/type",
      "phone-country_code":"/contact/phone/countrycode",
      "phone-number":"/contact/phone"
    };

    var dataTypeToExpansionFunctionMap = {
      "/financial/creditcard/number": expandCreditCardNumber,
      "/contact/phone": expandPhoneNumber,
      "/contact/state": expandState,
      "/contact/country": expandCountry
    };
    
    var ignoreDataTypes = /search|subject|message/i;

    function addValueToMap(map, type, value){
      if (!value) return;

      if (!map[type]) map[type] = [];

      value = (value+"").toLowerCase().replace(/\s/ig,"");

      if (value.length > 0) {
        var valueRegex = "(^" + value + "$)";

        if(map[type].indexOf(valueRegex) == -1){
          map[type].push(valueRegex);
        }
      }
    }

    function expandPhoneNumber(map, value){
      if(value.length == 10){
        addValueToMap(map, "/contact/phone/areacode", value.substr(0,3));
        addValueToMap(map, "/contact/phone/exchange", value.substr(3,3));
        addValueToMap(map, "/contact/phone/local", value.substr(6,4));
      }
    }

    function expandCreditCardNumber(map, value){
      if(value.length == 16){
        addValueToMap(map, "/financial/creditcard/number/part1", value.substr(0,4));
        addValueToMap(map, "/financial/creditcard/number/part2", value.substr(4,4));
        addValueToMap(map, "/financial/creditcard/number/part3", value.substr(8,4));
        addValueToMap(map, "/financial/creditcard/number/part4", value.substr(12,4));
      }
    }

    function expandState(map, value){
      if(value.length == 2 && stateNames[value]){
        addValueToMap(map, "/contact/state", stateNames[value])
      }
    }

    function expandCountry(map, value){
      if(value.length == 2 && countryNames[value]){
        addValueToMap(map, "/contact/country", countryNames[value])
      }
    }

    function buildMapFromEntities(map, entities, prefix){
      // attributes to ignore on following objects
      var ignoreTypeRegex = /createdAt|label|modifiedAt|pushed|address_id|brand|auth_token|is_validated|pushVersion|rails_id/ig;

      entities.forEach(function(entity){
        for(var type in entity._data){
          if(type.match(ignoreTypeRegex)) 
            continue

          var mappingType = attributeToDataTypeMap[prefix + "-" + type];
          var value = entity._data[type];

          addValueToMap(map, mappingType, value);

          if(dataTypeToExpansionFunctionMap[mappingType]){
            dataTypeToExpansionFunctionMap[mappingType](map, value);
          }
        }
      });
    }

    function loadUserDataForMatching(cb, force){
      if (userData && !force) {
        cb();
        return;
      } else {
        userData = {};
      }

      storage.Address.index(null, function(addresses){
        buildMapFromEntities(userData, addresses, "address");
        storage.RealCard.index(null, function(cards){
          buildMapFromEntities(userData, cards, "card");
          storage.ShieldedPhone.index(null, function(phones){
            buildMapFromEntities(userData, phones, "phone");
            
            var allRegex = [];
            for(var type in userData){
              allRegex.push(userData[type].join("|"));
              userData[type] = new RegExp(userData[type].join("|"))
            }
            globalUserDataRegex = new RegExp(allRegex.join("|"));
            cb();
          });
        });
      });
    }

    function getMatchingDataType(fieldValue) {
      if (!fieldValue) return null;
      fieldValue = fieldValue.toLowerCase().replace(/\s/ig,"");
      var matchedKnownDataType = null;
      if (fieldValue.length > 0 && fieldValue.match(globalUserDataRegex)) {
        for (var type in userData) {
          if (fieldValue.match(userData[type])) {
            matchedKnownDataType = type;
            break;
          }
        }
      }
      return matchedKnownDataType;
    }

    function getMask(value, dataType) {
      if (!value) {
        return value;
      }
      if (dataType && dataType.indexOf('/password') == 0) {
        return "**********";
      }
      return value.replace(/[a-z]/ig, "a").replace(/[0-9]/g, "d").replace(/[^aAd@]/g, 'x').substr(0, 50);
    }

    function getFieldData(fieldData, formData) {
      var maskedValue = getMask(fieldData.value, fieldData.dataType);
      var matchedKnownDataType = getMatchingDataType(fieldData.value);
      return {
        className: fieldData.className,
        dataType: fieldData.dataType,
        detectedType: fieldData.detectedType,
        mappedType: fieldData.mappedType,
        fieldType: fieldData.fieldType,
        fieldId: fieldData.id,
        label: fieldData.label,
        maxlength: fieldData.maxlength,
        name: fieldData.name,
        position: fieldData.position,
        signature: fieldData.signature,
        size: fieldData.size,
        hidden: fieldData.hidden,
        rule_locale: fieldData.matchedLocale,
        maskedValue: maskedValue,
        matchedDataType: matchedKnownDataType,
        formId: formData.id
      };
    }

    function processFieldForMapping(payload){
      if (!payload.formData.type || !payload.formData.type.match(/checkout|login|regist|change|address/)) {
        return;
      }
      storage.Preference.findBy('key', 'shouldSendErrorReports', _.bind(function(sendData) {
        if (!sendData || sendData.isTrue()) {
          loadUserDataForMatching(function () {
            var fieldValue = payload.fieldData.value;
            var hasBlacklistFields = false;
            if (fieldValue && fieldValue.length <= 50) {
              var allFields = [], matchedKnownDataType = false, curFieldData = null;
              var formData = payload.formData;
              var checkoutButtons = payload.checkoutButtons;
              var form = {
                domain: formData.domain,
                formSignature: formData.formSignature,
                formHost: formData.form_host,
                formId: formData.id,
                fieldCount: formData.allFields.length,
                pageHost: formData.page_host,
                formType: formData.type,
                language: formData.language,
                registered: securityManager.getUserId() != null,
                premium: securityManager.isPremium(),
                local_mapping: formData.local_mapping,
                mappedFormType: formData.mappedFormType,
                detected_locales: formData.detectedLocales,
                formless_depth: formData.formlessDepth,
                tag_name: formData.tagName,
                pageUrl: null, // formData.pageUrl,
                amex: checkoutButtons.amex,
                amazon: checkoutButtons.amazon,
                master: checkoutButtons.master,
                visa: checkoutButtons.visa,
                paypal: checkoutButtons.paypal,
                chase: checkoutButtons.chase
              };
              _.each(payload.formData.allFields, function (field) {
                if (field.dataType && field.dataType.match(ignoreDataTypes)) {
                  hasBlacklistFields = true;
                }
                var fieldData = getFieldData(field, payload.formData);
                allFields.push(fieldData);
                if (fieldData.matchedDataType) {
                  matchedKnownDataType = true;
                }
                if (!curFieldData && field.signature == payload.fieldData.signature) {
                  curFieldData = fieldData;
                  curFieldData.fillType = payload.fieldData.blurFilled ? 1 : 2;
                }
              });
              if (matchedKnownDataType || !hasBlacklistFields) {
                storage.FieldData.createLocal({
                  payload: curFieldData,
                  allFields: allFields,
                  form: form
                }, function (err) {

                });
              }
            }
          });
        }
      }, this));
    }

    // handle logout
    securityManager.on("background:securityManager:locked", function(){userData = null; globalUserDataRegex = null;});

    // refresh data on change to any entity
    var loadUserDataForMatchingDebounce = _.debounce(function(){loadUserDataForMatching(function(){}, true);});
    securityManager.on("entity:changed:RealCard", loadUserDataForMatchingDebounce);
    securityManager.on("entity:changed:Address", loadUserDataForMatchingDebounce);


    abineFormSubmitDetector.formSubmitDetector.on('background:formFieldChanged:processMapping', processFieldForMapping);

    var countryNames = {
      "AF":'Afghanistan',
      "AL":'Albania',
      "DZ":'Algeria',
      "AS":'American Samoa',
      "AD":'Andorra',
      "AO":'Angola',
      "AI":'Anguilla',
      "AQ":'Antarctica',
      "AG":'Antigua and Barbuda',
      "AR":'Argentina',
      "AM":'Armenia',
      "AW":'Aruba',
      "AC":'Ascension Island',
      "AU":'Australia',
      "AT":'Austria',
      "AZ":'Azerbaijan',
      "BS":'Bahamas',
      "BH":'Bahrain',
      "BD":'Bangladesh',
      "BB":'Barbados',
      "BE":'Belgium',
      "BZ":'Belize',
      "BJ":'Benin',
      "BM":'Bermuda',
      "BT":'Bhutan',
      "BO":'Bolivia',
      "BA":'Bosnia and Herzegovina',
      "BW":'Botswana',
      "BV":'Bouvet Island',
      "BR":'Brazil',
      "BQ":'British Antarctic Territory',
      "IO":'British Indian Ocean Territory',
      "VG":'British Virgin Islands',
      "BN":'Brunei',
      "BG":'Bulgaria',
      "BF":'Burkina Faso',
      "BI":'Burundi',
      "KH":'Cambodia',
      "CM":'Cameroon',
      "CA":'Canada',
      "IC":'Canary Islands',
      "CT":'Canton and Enderbury Islands',
      "CV":'Cape Verde',
      "KY":'Cayman Islands',
      "CF":'Central African Republic',
      "EA":'Ceuta and Melilla',
      "TD":'Chad',
      "CL":'Chile',
      "CN":'China',
      "CX":'Christmas Island',
      "CP":'Clipperton Island',
      "CC":'Cocos [Keeling] Islands',
      "CO":'Colombia',
      "KM":'Comoros',
      "CD":'Congo [DRC]',
      "CK":'Cook Islands',
      "CR":'Costa Rica',
      "HR":'Croatia',
      "CU":'Cuba',
      "CY":'Cyprus',
      "CZ":'Czech Republic',
      "DK":'Denmark',
      "DG":'Diego Garcia',
      "DJ":'Djibouti',
      "DM":'Dominica',
      "DO":'Dominican Republic',
      "NQ":'Dronning Maud Land',
      "TL":'East Timor',
      "EC":'Ecuador',
      "EG":'Egypt',
      "SV":'El Salvador',
      "EE":'Estonia',
      "ET":'Ethiopia',
      "FK":'Falkland Islands [Islas Malvinas]',
      "FO":'Faroe Islands',
      "FJ":'Fiji',
      "FI":'Finland',
      "FR":'France',
      "GF":'French Guiana',
      "PF":'French Polynesia',
      "TF":'French Southern Territories',
      "FQ":'French Southern and Antarctic Territories',
      "GA":'Gabon',
      "GM":'Gambia',
      "GE":'Georgia',
      "DE":'Germany',
      "GH":'Ghana',
      "GI":'Gibraltar',
      "GR":'Greece',
      "GL":'Greenland',
      "GD":'Grenada',
      "GP":'Guadeloupe',
      "GU":'Guam',
      "GT":'Guatemala',
      "GG":'Guernsey',
      "GW":'Guinea-Bissau',
      "GY":'Guyana',
      "HT":'Haiti',
      "HM":'Heard Island and McDonald Islands',
      "HN":'Honduras',
      "HK":'Hong Kong',
      "HU":'Hungary',
      "IS":'Iceland',
      "IN":'India',
      "ID":'Indonesia',
      "IE":'Ireland',
      "IM":'Isle of Man',
      "IL":'Israel',
      "IT":'Italy',
      "JM":'Jamaica',
      "JP":'Japan',
      "JE":'Jersey',
      "JT":'Johnston Island',
      "JO":'Jordan',
      "KZ":'Kazakhstan',
      "KE":'Kenya',
      "KI":'Kiribati',
      "KW":'Kuwait',
      "KG":'Kyrgyzstan',
      "LA":'Laos',
      "LV":'Latvia',
      "LS":'Lesotho',
      "LY":'Libya',
      "LI":'Liechtenstein',
      "LT":'Lithuania',
      "LU":'Luxembourg',
      "MO":'Macau',
      "MK":'Macedonia [FYROM]',
      "MG":'Madagascar',
      "MW":'Malawi',
      "MY":'Malaysia',
      "MV":'Maldives',
      "ML":'Mali',
      "MT":'Malta',
      "MH":'Marshall Islands',
      "MQ":'Martinique',
      "MR":'Mauritania',
      "MU":'Mauritius',
      "YT":'Mayotte',
      "FX":'Metropolitan France',
      "MX":'Mexico',
      "FM":'Micronesia',
      "MI":'Midway Islands',
      "MD":'Moldova',
      "MC":'Monaco',
      "MN":'Mongolia',
      "ME":'Montenegro',
      "MS":'Montserrat',
      "MA":'Morocco',
      "MZ":'Mozambique',
      "NA":'Namibia',
      "NR":'Nauru',
      "NP":'Nepal',
      "NL":'Netherlands',
      "AN":'Netherlands Antilles',
      "NT":'Neutral Zone',
      "NC":'New Caledonia',
      "NZ":'New Zealand',
      "NI":'Nicaragua',
      "NE":'Niger',
      "NG":'Nigeria',
      "NU":'Niue',
      "NF":'Norfolk Island',
      "VD":'North Vietnam',
      "MP":'Northern Mariana Islands',
      "NO":'Norway',
      "OM":'Oman',
      "QO":'Outlying Oceania',
      "PC":'Pacific Islands Trust Territory',
      "PK":'Pakistan',
      "PW":'Palau',
      "PS":'Palestinian Territories',
      "PA":'Panama',
      "PZ":'Panama Canal Zone',
      "PY":'Paraguay',
      "YD":'People\'s Democratic Republic of Yemen',
      "PE":'Peru',
      "PH":'Philippines',
      "PN":'Pitcairn Islands',
      "PL":'Poland',
      "PT":'Portugal',
      "PR":'Puerto Rico',
      "QA":'Qatar',
      "RO":'Romania',
      "RU":'Russia',
      "RW":'Rwanda',
      "RE":'R&eacute;union',
      "BL":'Saint Barth&eacute;lemy',
      "SH":'Saint Helena',
      "KN":'Saint Kitts and Nevis',
      "LC":'Saint Lucia',
      "MF":'Saint Martin',
      "PM":'Saint Pierre and Miquelon',
      "VC":'Saint Vincent and the Grenadines',
      "WS":'Samoa',
      "SM":'San Marino',
      "SA":'Saudi Arabia',
      "SN":'Senegal',
      "RS":'Serbia',
      "CS":'Serbia and Montenegro',
      "SC":'Seychelles',
      "SL":'Sierra Leone',
      "SG":'Singapore',
      "SK":'Slovakia',
      "SI":'Slovenia',
      "SB":'Solomon Islands',
      "ZA":'South Africa',
      "GS":'South Georgia and the South Sandwich Islands',
      "KR":'South Korea',
      "ES":'Spain',
      "LK":'Sri Lanka',
      "SR":'Suriname',
      "SJ":'Svalbard and Jan Mayen',
      "SZ":'Swaziland',
      "SE":'Sweden',
      "CH":'Switzerland',
      "ST":'S&atilde;o Tom&eacute; and Pr&iacute;ncipe',
      "TW":'Taiwan',
      "TJ":'Tajikistan',
      "TZ":'Tanzania',
      "TH":'Thailand',
      "TG":'Togo',
      "TK":'Tokelau',
      "TO":'Tonga',
      "TT":'Trinidad and Tobago',
      "TA":'Tristan da Cunha',
      "TN":'Tunisia',
      "TR":'Turkey',
      "TM":'Turkmenistan',
      "TC":'Turks and Caicos Islands',
      "TV":'Tuvalu',
      "UM":'U.S. Minor Outlying Islands',
      "PU":'U.S. Miscellaneous Pacific Islands',
      "VI":'U.S. Virgin Islands',
      "UG":'Uganda',
      "UA":'Ukraine',
      "AE":'United Arab Emirates',
      "GB":'United Kingdom',
      "US":'United States',
      "UY":'Uruguay',
      "UZ":'Uzbekistan',
      "VU":'Vanuatu',
      "VA":'Vatican City',
      "VE":'Venezuela',
      "VN":'Vietnam',
      "WK":'Wake Island',
      "WF":'Wallis and Futuna',
      "EH":'Western Sahara',
      "YE":'Yemen',
      "ZM":'Zambia',
      "AX":'&Aring;land Islands'
    };
    var stateNames = {
      "AK":'Alaska',
      "AL":'Alabama',
      "AP":'Armed Forces Pacific',
      "AR":'Arkansas',
      "AS":'American Samoa',
      "AZ":'Arizona',
      "CA":'California',
      "CO":'Colorado',
      "CT":'Connecticut',
      "DC":'District of Columbia',
      "DE":'Delaware',
      "FL":'Florida',
      "FM":'Federated States of Micronesia',
      "GA":'Georgia',
      "GU":'Guam',
      "HI":'Hawaii',
      "IA":'Iowa',
      "ID":'Idaho',
      "IL":'Illinois',
      "IN":'Indiana',
      "KS":'Kansas',
      "KY":'Kentucky',
      "LA":'Louisiana',
      "MA":'Massachusetts',
      "MD":'Maryland',
      "ME":'Maine',
      "MH":'Marshall Islands',
      "MI":'Michigan',
      "MN":'Minnesota',
      "MO":'Missouri',
      "MP":'Northern Mariana Islands',
      "MS":'Mississippi',
      "MT":'Montana',
      "NC":'North Carolina',
      "ND":'North Dakota',
      "NE":'Nebraska',
      "NH":'New Hampshire',
      "NJ":'New Jersey',
      "NM":'New Mexico',
      "NV":'Nevada',
      "NY":'New York',
      "OH":'Ohio',
      "OK":'Oklahoma',
      "OR":'Oregon',
      "PA":'Pennsylvania',
      "PR":'Puerto Rico',
      "PW":'Palau',
      "RI":'Rhode Island',
      "SC":'South Carolina',
      "SD":'South Dakota',
      "TN":'Tennessee',
      "TX":'Texas',
      "UT":'Utah',
      "VA":'Virginia',
      "VI":'Virgin Islands',
      "VT":'Vermont',
      "WA":'Washington',
      "WV":'West Virginia',
      "WI":'Wisconsin',
      "WY":'Wyoming'
    };

    return {};
  });

// common security manager for all browsers.
// will use browser specific crypto library

ABINE_DNTME.define("abine/securityManager",
  ["documentcloud/underscore", "abine/core", "storage",
    "persistence", "abine/timer", "abine/config", "abine/window", "abine/localStorage", 'abine/crypto'],
function(_, abineCore, storage, persistence, timer, config, abineWindow, localStorage, crypto) {

  var locked = true, hasPassword = false, authToken = null, userState = null;

  var initialized = false;

  var encryptionKey = null, companyEncryptionKey = null, userId = null;

  var billing_manager = null;

  var emptyCallback = function(){};

  var SecurityManager = abineCore.BaseClass.extend({
    initialize: function(){
    },

    load: function(backgroundMessenger, silentUnlock) {
      if (backgroundMessenger) {
        this.messenger = backgroundMessenger;
        this.messenger.on("biometric:auth", _.bind(this.biometricCardAuth, this));
        this.messenger.on("password:auth", _.bind(this.passwordCardAuth, this));
      }
      var done = function(){
        initialized = true;
        var triggerInitialized = function(){securityManager.trigger('background:securityManager:initialized')};
        if (!locked) {
          triggerUnlocked();
        }
        triggerInitialized();
      };
      var self = this;
      // read encryption status form DB and initialize state
      storage.Preference.findBy('key', 'authToken', function(pref){
        storage.Preference.findBy('key', 'key', function(keyPref){
          storage.Preference.findBy('key', 'ckey', function(ckeyPref){
            if (!pref || pref.value == "") {
              locked = true;
              hasPassword = false;
              authToken = null;
              userState = null;
              self.setEncryptionKey(null, done);
            } else {
              // has password
              hasPassword = true;
              locked = false;
              authToken = pref.value;
              self.setEncryptionKey(keyPref?keyPref.value:null, ckeyPref?ckeyPref.value:null, done);
            }
          });
        });
      });

    },

    getUserId: function() {
      return userId;
    },

    setUserId: function(id) {
      userId = id;
    },

    getCompanyEncryptionKey: function() {
      return companyEncryptionKey;
    },

    getAuthToken: function() {
      return authToken;
    },

    getEncryptionKey: function() {
      return encryptionKey;
    },

    isPremium: function() {
      return userState == 'premium';
    },

    setEncryptionKey: function(key, companyKey, callback) {
      callback = callback || emptyCallback;

      function clearCache() {
        storage.Account.clearCache(function(){
          storage.Address.clearCache(function(){
            storage.Note.clearCache(function(){
              storage.AccountHistory.clearCache(function() {
                storage.RealCard.clearCache(function(){
                  storage.CompanyAccount.clearCache(function(){
                    callback();
                  });
                });
              });
            });
          });
        });
      }

      if (key && key != "" && key != "undefined") {
        if (encryptionKey != key || companyEncryptionKey != companyKey) {
          encryptionKey = key;
          companyEncryptionKey = companyKey;
          storage.Preference.createOrModify({key: 'key', value: key}, function(){
            storage.Preference.createOrModify({key: 'ckey', value: companyKey}, function(){
              clearCache();
            });
          });
          return;
        }
      } else {
        if (encryptionKey != null) {
          billing_manager = null;
          encryptionKey = null;
          companyEncryptionKey = null;
          clearCache();
          return;
        }
      }
      callback();
    },

    // used only by page_event.js for importing accounts from maskme
    __setUserIdAndKey: function(id, key) {
      userId = id;
      encryptionKey = key && key != '' && key != 'undefined'?key:null;
    },

    getAuthToken: function() {
      return authToken;
    },

    setAuthToken: function(token, key, companyKey, callback) {
      callback = callback || emptyCallback;
      var hasToken = token && token.length > 0;
      storage.Preference.createOrModify({key: 'lastActivity', value: Date.now()}, _.bind(function(){
        storage.Preference.findBy('key', 'authToken', _.bind(function(pref){
          if (pref && pref.value == token && token == authToken) {
            this.setEncryptionKey(key, companyKey, function(){
              // no change in token
              if (hasToken) {
                securityManager.refreshData(callback);
              }
            });
            return;
          }
          persistence.reset();
          this.setEncryptionKey(key, companyKey, _.bind(function(){
            storage.Preference.findBy('key', 'hasRegistered', _.bind(function(oldHasRegistered){
              storage.Preference.batchCreateOrModify([
                {key: "authToken", value: token},
                {key: "key", value: key},
                {key: "ckey", value: companyKey},
                {key: "oldHasRegistered", value: oldHasRegistered?oldHasRegistered.value:'false'},
                {key: "hasRegistered", value: hasToken?'true':'false'}
              ], _.bind(function(){
                if (token && token != '') {
                  ABINE_DNTME._started = true;
                  locked = false;
                  hasPassword = true;
                  authToken = token;
                  triggerUnlocked(null, callback);
                } else {
                  storage.Preference.removeByKey('altEmail1');
                  storage.Preference.removeByKey('altEmail2');
                  storage.Preference.createOrModify({key: 'userTag', value: ''});
                  storage.Preference.findBy('key', 'dynamicTag', function(pref){
                    if (pref && pref.value) {
                      ABINE_DNTME.config.updateTag(pref.value);
                    }
                  });

                  locked = true;
                  userId = null;
                  authToken = null;
                  ABINE_DNTME.require(["abine/memoryStore"], function(store){
                    store.set("biometricKeyToken", null);
                  });
                  ABINE_DNTME.require(["abine/blurstore-api"], function(api){
                    api.disconnect();
                  });
                  ABINE_DNTME.require(['abine/pushNotifications'], function(notifications){
                    notifications.disconnect();
                  });
                  storage.Preference.retainDefaults(_.bind(function(){
                    this.messenger.broadcastToAllTabs('broadcast:preference:reload', {});
                    this.messenger.broadcastToAllTabs('background:securityManager:locked', {});
                    securityManager.trigger('background:securityManager:locked');
                    callback();
                  }, this));
                }
              }, this));
            }, this));
          }, this));
        }, this));
      }, this));
    },

    login: function() {
      abineWindow.reloadTab({
        url: ABINE_DNTME.config.protocol+ABINE_DNTME.config.webAppHost
      })
    },

    logout: function() {
      this.setAuthToken(null, null);
    },

    openPage: function(page, panelData, refreshOnly){

      if (panelData === true) {
        var params = '';
        if (!page.match(/[&\?]$/)) params = '&';
        params += 'v='+ABINE_DNTME.config.version;
        params += '&tag='+ABINE_DNTME.config.tags[0];
        page += params;
      } else if (panelData) {
        if(!ABINE_DNTME.lastPanel){ ABINE_DNTME.lastPanel = {}; }
        ABINE_DNTME.lastPanel[panelData.action] = panelData.id;
      }

      if (page.indexOf('http') !== 0) {
        if (!page.match(/^\//)) page = '/'+page;
        page = ABINE_DNTME.config.protocol+ABINE_DNTME.config.webAppHost + page;
      }

      abineWindow.createTab({
        url: page,
        doNotActivate: (refreshOnly == true),
        doNotOpenNewTab: (refreshOnly == true)
      });
    },

    passwordCardAuth: function(payload, sender) {
      var self = this;
      ABINE_DNTME.require(['modules'], function (modules) {
        modules.licenseModule.passwordCardAuth(payload, function(response){
          self.messenger.send(payload.responseId, response, sender);
        });
      });
    },

    biometricCardAuth: function(payload, sender) {
      var self = this;
      ABINE_DNTME.require(['abine/pushNotifications'], function(push){
        push.biometricAuthCancel();
        push.requestBiometricAuth(payload.type || 'card', payload.key, payload.domain, payload.amount, payload.number, payload.account, function(response){
          self.messenger.send(payload.responseId, response, sender);
        });
      });
    },

    openPostInstall: function() {
        storage.Preference.findBy('key', 'postInstallUrl', _.bind(function(pref){
            if(pref && pref.value) {
                this.openPage(pref.value);
            }
        }, this));
    },

    openWindow: function(url, width, height) {
      abineWindow.openWindow(url, width, height);
    },

    openLink: function(params){
      this.getLink(params, function(url) {
        abineWindow.createTab({
          url: url
        });
      });
    },

    getLink: function(params, callback) {
      params.lang = 'en';
      params.v = ABINE_DNTME.config.version;

      storage.Preference.batchFindBy([
        {by:"key",value:"installVersion"},
        {by:"key",value:"installDate"}
      ], function(prefMap){
        if (prefMap.installVersion) {
          params.ov = prefMap.installVersion.value;
        }
        if (prefMap.installDate) {
          var dt = (new Date(parseInt(prefMap.installDate.value)));
          params.cohort = dt.getFullYear()+"-"+(dt.getMonth()+1)+'-'+dt.getDate();
        }

        params.o = ABINE_DNTME.config.tags[0];

        var queryString = '';
        for (var i in params) {
          queryString += (queryString == '')?'?':'&';
          queryString += encodeURIComponent(i)+'='+encodeURIComponent(params[i]);
        }

        callback('http://donottrackplus.com/link.php'+queryString)
      });
    },

    isLocked: function() {
      return locked;
    },

    hasPassword: function() {
      return hasPassword;
    },

    isInitialized: function() {
      return initialized;
    },

    isMaskmeInstalled: function() {
      return ABINE_DNTME.maskme_installed;
    },

    refreshRules: function() {
      ABINE_DNTME.require(['abine/dntmeInit'], function(dntme){dntme.updateRules();});
    },

    afterRefresh: function(callback) {
      if (this._refreshInProgress) {
        this._refreshCallbacks.push(callback);
      } else {
        callback();
      }
    },

    getBillingManager: function() {
      return billing_manager;
    },

    getAnonymousId: function() {
      var item = localStorage.getItem("anonymousId");
      if (!item) {
        item = crypto.sha1(persistence.createUUID());
        localStorage.setItem("anonymousId", item)
      }
      return item;
    },

    refreshData: function(forceMappingUpdate, callback) {
      ABINE_DNTME.require(['abine/pushNotifications'], function(notifications){
        notifications.connect();
      });

      if (forceMappingUpdate && forceMappingUpdate !== true) {
        callback = forceMappingUpdate;
        forceMappingUpdate = false;
      }
      callback = callback || emptyCallback;

      // allow single refreshData at a time to prevent race condition
      if (this._refreshInProgress) {
        LOG_FINEST && ABINE_DNTME.log.finest('another refresh data is in progress');
        this._refreshCallbacks.push(callback);
        return;
      }

      LOG_FINEST && ABINE_DNTME.log.finest('refresh data started');

      this._refreshCallbacks = [callback];
      this._refreshInProgress = true;

      var self = this;
      function wrapCallback() {
        function allCallbacks() {
          _.each(self._refreshCallbacks, function(cb){
            try{cb();}catch(e){}
          });
          delete self._refreshCallbacks;
          delete self._refreshInProgress;
          LOG_FINEST && ABINE_DNTME.log.finest('refresh data completed');
        }
        if (companyEncryptionKey && companyEncryptionKey != '') {
          storage.Preference.createOrModify({key:"hasCompanyKey",value:'true'});
          storage.CompanyAccount.fetchFromServer(allCallbacks);
        } else {
          storage.Preference.createOrModify({key:"hasCompanyKey",value:'false'});
          allCallbacks();
        }
      }

      ABINE_DNTME.require(['modules', "abine/modules/mappings", "abine/modules/panels"], function (modules) {
        function getUserState(callback) {
          storage.Preference.batchFindBy([
            {by:'key', value: 'oldHasRegistered'},
            {by:'key', value: 'hasRegistered'},
            {by:"key",value:"hasEnabledPayments"},
            {by:'key', value: 'entitledToPhone'}
          ], _.bind(function(prefMap){

            // capture state to see if state changes for panel conversion handling
            var userState = 'unregistered', cardEnabled = false;

            if (prefMap['oldHasRegistered']) {
              persistence.remove(prefMap['oldHasRegistered']);
              prefMap['hasRegistered'] = prefMap['oldHasRegistered'];
            }

            if (prefMap['hasRegistered'] && prefMap['hasRegistered'].isTrue()) {
              if (prefMap['entitledToPhone'] && prefMap['entitledToPhone'].isTrue()) {
                userState = 'premium';
                if (prefMap['hasEnabledPayments'] && prefMap['hasEnabledPayments'].isTrue()) {
                  cardEnabled = true;
                }
              } else {
                userState = 'registered';
              }
            }

            callback(userState, cardEnabled);

          }, this));
        }

        function panelUpdateCheck(){
          // not using any panels now + it is causing all rules to be downloaded on every browser restart
          return;

          // fetch panels if user-state changed
          if (forceMappingUpdate) {
            modules.mappingsModule.makeDailyMappingCheck();
          } else {
            modules.mappingsModule.refreshPanels(forceMappingUpdate);
          }
        }

        getUserState(_.bind(function(oldUserState, oldCardEnabled){
          storage.Preference.retainDefaults(function(){
            modules.licenseModule.refreshEntitlements(function (err, serverData) {
              storage.ShieldedEmail.cached = true;

              securityManager.setUserId(null);
              if (!err) {
                securityManager.setUserId(serverData.user.id);
                ABINE_DNTME.setupUninstallUrl();
                storage.Preference.findBy('key', 'dynamicTag', function(pref){
                  storage.Preference.createOrModify({key: 'userTag', value: serverData.user.tag||''});
                  if (serverData.user.tag) {
                    ABINE_DNTME.config.updateTag(serverData.user.tag);
                  }
                });
                storage.Preference.createOrModify({key: 'has_mobile', value: (serverData.user.first_phone_use_date || serverData.user.first_tablet_use_date)?'true':'false'});
                storage.Preference.createOrModify({key: 'sync_store_manager', value: serverData.user.sync_store_manager});
                storage.Preference.createOrModify({key: 'masked_card_auth', value: serverData.user.masked_card_auth?serverData.user.masked_card_auth:'none'});
                storage.Preference.createOrModify({key: 'biometric_type', value: serverData.user.biometric_type?serverData.user.biometric_type:''});
                if (serverData.user.company && serverData.user.company.has_cards) {
                  storage.Preference.createOrModify({key: 'hasCompanyCards', value: 'true'});
                } else {
                  storage.Preference.removeByKey('hasCompanyCards');
                }
                billing_manager = serverData.user.billing_manager;
                // clear cache whenever user-id changes
                storage.clearAllCache(serverData.user.sync_store_manager, _.bind(function(){

                  getUserState(_.bind(function(newUserState, newCardEnabled){

                    userState = newUserState;

                    var conversionChecks = [];
                    if (oldUserState == 'unregistered' && newUserState != 'unregistered') {
                      conversionChecks.push('open_registration_page');
                    }
                    if (oldUserState != 'premium' && newUserState == 'premium') {
                      conversionChecks.push('open_plans_page');
                      conversionChecks.push('open_snowman_page');
                    }
                    if (!oldCardEnabled && newCardEnabled) {
                      conversionChecks.push('open_activate_cards_page');
                    }

                    _.async.forEach(conversionChecks, function(conversion, cb){
                      modules.panelsModule.panelConversionCheck(conversion, cb);
                    }, panelUpdateCheck);


                    storage.ShieldedEmail.findBy('email', serverData.user.email, function (email) {
                      if (email) {
                        persistence.remove(email);
                      }
                      storage.Preference.createOrModify({key: 'oldEmail', value: serverData.user.email});
                      if (serverData.user.alternate_email1) {
                        storage.Preference.createOrModify({key: 'altEmail1', value: serverData.user.alternate_email1});
                      } else {
                        storage.Preference.removeByKey('altEmail1');
                      }
                      if (serverData.user.alternate_email2) {
                        storage.Preference.createOrModify({key: 'altEmail2', value: serverData.user.alternate_email2});
                      } else {
                        storage.Preference.removeByKey('altEmail2');
                      }
                      email = new storage.ShieldedEmail({email: serverData.user.email});
                      persistence.add(email);
                      storage.Preference.findBy('key', 'loginNotLock', function(loginNotLock){
                        if (loginNotLock) {
                          persistence.remove(loginNotLock);
                        }

                        storage.DisposableEmail.indexFromServer(null, function () {
                          storage.Preference.findBy('key', 'entitledToPhone', function(entitledToPhone){
                            if (!entitledToPhone || entitledToPhone.isFalse()) {
                              // do not make a request to phone platform if user not premium
                              wrapCallback();
                              return;
                            }
                            modules.phoneModule.getPhone(null, function(err, serverData){
                              storage.ShieldedPhone.cached = true;
                              if (err || !serverData.shielded_phone) {
                                wrapCallback();
                                return;
                              }
                              var serverPhone = serverData.shielded_phone;
                              storage.ShieldedPhone.findBy('number', serverPhone.number, function (phone) {
                                if (phone) {
                                  persistence.remove(phone);
                                }
                                phone = new storage.ShieldedPhone({
                                  country_code: serverPhone.country_code,
                                  number: serverPhone.number,
                                  is_validated: serverPhone.is_validated?'true':'false',
                                  rails_id: serverPhone.id
                                });
                                persistence.add(phone);
                                if (serverPhone.disposable_phones.length > 0) {
                                  var serverDispPhone = serverPhone.disposable_phones[0];
                                  storage.DisposablePhone.findBy('number', serverDispPhone.number, function (dispPhone) {
                                    if (dispPhone) {
                                      persistence.remove(dispPhone);
                                    }
                                    dispPhone = new storage.DisposablePhone({
                                      country_code: serverDispPhone.country_code,
                                      number: serverDispPhone.number,
                                      released: serverDispPhone.released,
                                      rails_id: serverDispPhone.id,
                                      shielded_phone_id: phone.id
                                    });
                                    persistence.add(dispPhone);
                                    wrapCallback();
                                  });
                                } else {
                                  wrapCallback();
                                }
                              })
                            });
                          });
                        });
                      });

                    });
                  }, this))
                }, this));
              } else {
                panelUpdateCheck();
                wrapCallback();
              }
            });
          });
        }, this));

      });

    }
  });

  function triggerUnlocked(payload, callback, silent) {
    callback = callback || emptyCallback;
    ABINE_DNTME.require(["abine/memoryStore"], function(memoryStore){
      memoryStore.set('login-panel-ignored', 0);
      memoryStore.set('login-panel-closed', null);
    });
    securityManager.refreshData(function(){
      securityManager.messenger.broadcastToAllTabs('background:securityManager:unlocked', {});
      securityManager.trigger('background:securityManager:unlocked', payload);
      callback(null, payload);
    });
  }

  var securityManager = new SecurityManager();
  return securityManager;

});
ABINE_DNTME.define("abine/server",
  ['documentcloud/underscore','abine/ajax', 'abine/config', 'storage','abine/core', 'abine/responseCodes',
    "abine/window", "persistence", "abine/timer", "abine/securityManager", "abine/localStorage"],
  function (_, abineAjax, config, storage, abineCore, responseCodes,
            abineWindow, persistence, abineTimer, securityManager, localStorage){

    var emptyCallback = function() {};

    var addAuthTokenToOptions = function(options, callback){
      if (options.skip_auth_token) {
        callback.call(this, options);
        return;
      }
      storage.Preference.findBy('key', 'authToken', function(pref){
        storage.Preference.findBy('key', 'installAuthToken', function(installAuthTokenPref){
          // use authToken only if its not empty and can be decrypted
          var authToken = (!pref || pref.isEmpty())?null:pref.value;
          var installAuthToken = (!installAuthTokenPref || installAuthTokenPref.isEmpty())?null:installAuthTokenPref.value;

          if (authToken) {
            options.data.auth_token = authToken;
          }

          if(installAuthToken){
            options.data.install_auth_token = installAuthToken;
          }

          callback.call(this, options);
        });
      });
    };

    var addClientApplicationToOptions = function(options){
      if (options.skip_ca) {
        return;
      }
      var data = options.data;
      var version = config.version;
      if(ABINE_DNTME.dntme_upgrade && !ABINE_DNTME.merged_data_available){
          version = "dnt2-" + version;
      }
      data.client_application.version = version;
      data.client_application.environment = config.environment;
      data.client_application.tag = config.tags[0];
      data.client_application.browser = config.browser;
      data.client_application.build_num = config.buildNum;
      data.client_application.user_agent = ABINE_DNTME.userAgent;
      data.client_application.product = "DNT3";
      data.client_application.device = "Extension";
      var device_id = localStorage.getItem('device_id');
      if (!device_id) {
        device_id = Math.floor(Math.random()*100000000);
        localStorage.setItem('device_id', ""+device_id);
      }
      data.client_application.device_id = device_id;
    }

    var Server = abineCore.BaseClass.extend({
      makeRequest: function (options) {
        options.defaults = options.defaults || {};
        options.required = options.required || [];
        options.data = _.defaults(options.data || {}, options.defaults);
        options.type = options.type || "POST";
        options.dataType = options.dataType || "json";


        var clientSuccess = options.success || emptyCallback;
        var clientError = options.error || emptyCallback;

        if(!options.data.client_application){
          options.data.client_application = {};
        }

        options.success = _.bind(function(data, textStatus, jqXHR){
          var httpStatus = jqXHR.status;
          if (options.raw) {
            var serverData = data;
          } else {
            var serverData = data['data'];
          }
          if (!options.raw && data.error_code == 'scheduled downtime') {
            clientError(httpStatus, responseCodes.SCHEDULED_DOWNTIME, {error_code: responseCodes.SCHEDULED_DOWNTIME});
          } else {
            clientSuccess(serverData, httpStatus);
          }
        }, this);

        options.error = _.bind(function(jqXHR, textStatus, errorThrown){
          var httpStatus = jqXHR.status;
          var responseJSON = {error_code: responseCodes.UNKNOWN_ERROR};
          try { responseJSON = JSON.parse(jqXHR.responseText); } catch(e){}
          var errorCode = responseJSON["error_code"];

          if(httpStatus == 500){
            errorCode = responseCodes.UNKNOWN_ERROR;
          } else if(httpStatus == 503){
            errorCode = responseCodes.SERVICE_UNAVAILABLE_ERROR;
          }

          if (errorCode == responseCodes.UNAUTHORIZED) {
            LOG_FINEST && ABINE_DNTME.log.finest("#["+options.server+"]#["+options.action+"]# "+jqXHR.responseText+"\n"+JSON.stringify(options));
            securityManager.setAuthToken('', '', '');
          }

          clientError(httpStatus, errorCode, responseJSON);
        },this);


        if(!this.hasRequiredParams(options.required, options.data)){
          LOG_ERROR && ABINE_DNTME.log.error("[ERROR HANDLER]#["+options.server+"]#["+options.action+"]# client missing required parameters");
          clientError(422,responseCodes.MISSING_PARAMETERS, {});
          return;
        }

        addAuthTokenToOptions(options, function(options){
          addClientApplicationToOptions(options);
          abineAjax.ajax(options);
        });
      },

      hasRequiredParams: function(required, data){
        var hasAll=true;
        _.each(required, _.bind(function(key){
          if(!_.has(data,key) || this.isEmpty(data[key])){
            hasAll=false;
          }
        },this));
        return hasAll;
      },

      isEmpty: function(val){
        return (val === null || val === undefined || val === NaN || val === "");
      }
    });

    // For mocking server
    // Doesn't hold state for many requests.
    // Inspect "lastRequest" and use "respondWithSuccess" to trigger the callback.

    var Mock = Server.extend({
      makeRequest: function (options) {
        if (!options.url) { throw("server calls must have a url"); }
        options.data = options.data || {};
        options.type = options.type || "POST";
        options.dataType = options.dataType || "json";
        var clientSuccess = options.success || emptyCallback;
        var clientError = options.error || emptyCallback;

        options.success = function(data, textStatus, jqXHR){
          var httpStatus = jqXHR.status;
          var serverData = data['data'];
          clientSuccess(serverData, httpStatus, this);
        };

        options.error = function(jqXHR, textStatus, errorThrown){
          var httpStatus = jqXHR.status;
          var responseJSON = {error_code: responseCodes.UNKNOWN_ERROR};
          try { responseJSON = JSON.parse(jqXHR.responseText); } catch(e){}
          var errorCode = responseJSON["error_code"];

          if(httpStatus == 500){
            errorCode = responseCodes.UNKNOWN_ERROR;
          }

          clientError(httpStatus, errorCode, responseJSON);
        };

        if(!options.data.client_application){
          options.data.client_application = {};
        }

        this.lastRequest = options;
        this.tokenSet = false;
        var self = this;
        addAuthTokenToOptions(options, function(options) {
          addClientApplicationToOptions(options);
          self.tokenSet = true;
          if (self.waitingRequest) {
            self.waitingRequest();
            delete self.waitingRequest;
          }
        });
        this.trigger('request:made');
      },

      respondWithSuccess: function(jqXHR, textStatus, errorThrown) {
        if (!this.tokenSet) {
          this.waitingRequest = _.bind(this.respondWithSuccess, this, jqXHR, textStatus, errorThrown);
          return;
        }
        this.lastRequest.success.call(this.lastRequest, jqXHR, textStatus, errorThrown);
        delete this.lastRequest;
      },

      respondWithError: function(jqXHR, textStatus, errorThrown) {
        if (!this.tokenSet) {
          this.waitingRequest = _.bind(this.respondWithError, this, jqXHR, textStatus, errorThrown);
          return;
        }
        this.lastRequest.error = true;
        this.lastRequest.error.call(this.lastRequest, jqXHR, textStatus, errorThrown);
        delete this.lastRequest;
      }
    });

    return {
      server : Server,
      mock: Mock
    };
  });
ABINE_DNTME.define("abine/storageMixins",
    ['documentcloud/underscore', 'persistence', 'abine/localStorage', 'abine/crypto', 'abine/securityManager'],
    function(_, persistence, localStorage, crypto, securityManager) {

  // DefaultActions defines the default CRUD actions useful to most storage entities.
  // This can be mixed into any entity that needs the normal actions.
  // Special cases can be overloaded.


  function triggerEntityChanged(entity) {
    ABINE_DNTME.require(['abine/pageEvents'], function(pageEvents){
      ABINE_DNTME.backgroundMessenger.sendToWebapp('entity:changed', entity);
    });
    // only these two are used, so avoid other events
    if (entity == 'Address') {
      securityManager.trigger('entity:changed:'+entity);
    } else if (entity == 'RealCard') {
      securityManager.trigger('entity:changed:'+entity);
      ABINE_DNTME.backgroundMessenger.broadcastToAllTabs('entity:changed:'+entity);
    }
  }

  var DefaultActions = {

    // ### function index(request)
    // Return all instances of this entity.
    index: function(request, callback) {
      var Entity = this;
      if (!Entity.meta.cached || !Entity.cached) {
        if (Entity.cachingQueryInProgress) {
          LOG_DEBUG && ABINE_DNTME.log.debug(Entity.meta.name+": waiting for caching query to complete "+Entity.waitingCallbacks.length);
          Entity.waitingCallbacks.push({req:request, cb:callback});
        } else {

          Entity.cachingQueryInProgress = true;
          Entity.waitingCallbacks = [];
          try {
            if (Entity.meta.localStorage) {
              try {
                Entity.loadFromLocalStorage();
              } catch(e){
                LOG_FINEST && ABINE_DNTME.log.finest(""+e);
              }
            }

            this.all().list(null, function(entities) {
              if (Entity.meta.cached) {
                Entity.cached = true;
              }
              // cachingQueryInProgress related state must be cleared before calling any callback as they could result in
              // nested calls to this method leading to infinite recursion.
              var otherCallbacks = Entity.waitingCallbacks;

              delete Entity.cachingQueryInProgress;
              delete Entity.waitingCallbacks;

              try {
                Entity.respondWithEntities(entities, request, callback);
              } catch(e){LOG_ERROR && ABINE_DNTME.log.error(e);}

              for (var i=0;i<otherCallbacks.length;i++) {
                // callbacks might have created more entities, make sure they reach subsequent callbacks.
                // hence recreate entities list from persistencejs cache.
                try {
                  entities = persistence.getCachedEntities(Entity);
                  Entity.respondWithEntities(entities, otherCallbacks[i].req, otherCallbacks[i].cb);
                } catch(e){LOG_ERROR && ABINE_DNTME.log.error(e);}
              }

              triggerEntityChanged(Entity.meta.name);
            });
          } catch (e) {
            // first call to caching failed, reset state to initiate caching again on future calls
            // otherwise, all other calls to load entity will keep waiting for a callback which will never happen
            LOG_DEBUG && ABINE_DNTME.log.debug(e);
            var otherCallbacks = Entity.waitingCallbacks;
            delete Entity.cachingQueryInProgress;
            delete Entity.waitingCallbacks;
            for (var i=0;i<otherCallbacks.length;i++) {
              try {
                Entity.respondWithEntities([], otherCallbacks[i].req, otherCallbacks[i].cb);
              } catch(e){LOG_ERROR && ABINE_DNTME.log.error(e);}
            }
          }
        }
      } else {
        // cached, so return entities from cache
        var entities = persistence.getCachedEntities(Entity);
        Entity.respondWithEntities(entities, request, callback);
      }
    },

    isCached: function() {
      return this.meta.cached && this.cached;
    },

    // ### function show(request)
    // Return a single instance by id.
    show: function(request, callback) {
      var id = request.params[0];
      this.load(id, function(entity) {
        if (callback) {
          callback.call(this, entity);
        } else if (request.returnEntities) {
          request.returnEntities(entity);
        }
      });
    },

    // ### function create(request)
    // Create a new instance of this entity, initialized with the properties in the message payload.
    create: function(request, callback) {
      var Constructor = this;
      request.payload.createdAt = new Date();
      request.payload.modifiedAt = new Date();
      var entity = new Constructor(request.payload);
      persistence.add(entity);
      persistence.flush(null, function() {
        if (callback) {
          callback.call(this, entity);
        } else if (request.returnEntities) {
          request.returnEntities(entity);
        }
      });
    },

    // ### function update(request)
    // Update an instance of this entity, writing the properties in the message payload.
    update: function(request, callback) {
      var id = request.params[0];
      request.payload.modifiedAt = new Date();
      this.load(id, function(entity) {
        _.extend(entity, request.payload);
        persistence.flush(null, function() {
          if (callback) {
            callback.call(this, entity);
          } else if (request.returnEntities) {
            request.returnEntities(entity);
          }
        });
      });
    },

    // ### function destroy(request)
    // Delete an instance of this entity.
    // Returns only status ok.
    destroy: function(request, callback) {
      var id = request.params[0];
      this.load(id, function(entity) {
        persistence.remove(entity);
        persistence.flush(null, function() {
          if (callback) {
            callback.call();
          } else if (request.returnStatus) {
            request.returnStatus("ok");
          }
        });
      });
    },

    respondWithEntities: function(data, request, callback) {
      if (callback) {
        callback.call(this, data);
      } else if (request && request.returnEntities) {
        request.returnEntities(data);
      }
    },

    respondWithJSON: function(data, request, callback) {
      if (callback) {
        callback.call(null, data);
      } else if (request.returnJSON) {
        request.returnJSON(data);
      }
    },

    clearCache: function(callback) {
      this.decryptionFailed = false;
      if (!this.cached) {
        if (callback) callback();
        return;
      }
      LOG_FINEST && ABINE_DNTME.log.finest("clearing cache for "+this.meta.name);
      try {
        this.index(null, _.bind(function(entities){
          _.each(entities, function(entity){
            persistence.clearFromEntityCache(entity);
          });
          this.cached = false;
          this.decryptionFailed = false;
          LOG_FINEST && ABINE_DNTME.log.finest("clearing cache for "+this.meta.name+" done");
          if (callback) callback();
        }, this));
      } catch(e) {}
    },

    // ### listCached: function(field, value, filterCallback, callback)
    // filters entities by using cached entities and given criteria
    listCached: function(field, value, filterCallback, callback) {
      if (this.meta.cached) {
        if (typeof field == 'function') {
          filterCallback = field;
          callback = value;
          field = null;
        } else if (!callback) {
          callback = filterCallback;
          filterCallback = null;
        }
        this.index(null, function(allEntities){
          var entities = [];
          _.each(allEntities, function(entity){
            if ((!field || entity[field] == value) && (!filterCallback || filterCallback(entity))) {
              entities.push(entity);
            }
          });
          if (callback) callback(entities);
        });
      } else {
        throw "listCached cannot be used with entities that do not have caching enabled."
      }
    },

    // ### listByIndex: function(indexValue, callback)
    // filters entities by using given index
    listByIndex: function(indexValue, callback) {
      var Entity = this;
      if (Entity.meta.indexed) {
        if (!Entity.meta.cached || !Entity.cached) {
          this.index(null, function(allEntities){
            callback(persistence.getEntitiesByIndex(Entity, indexValue));
          });
        } else {
          // cached, so return entities from cache
          callback(persistence.getEntitiesByIndex(Entity, indexValue));
        }
      } else {
        throw "listByIndexed cannot be used with entities that do not have indexing enabled."
      }
    },

    // ### initCache: function(callback)
    // initializes cache if not already done
    initCache: function(callback) {
      if (this.meta.cached && !this.cached) {
        this.index(null, function(){
          callback();
        });
      } else {
        callback();
      }
    },

    useLocalStorage: function() {
      this.meta.localStorage = true;
    },

    useEncryption: function(fields) {
      this.meta.encrypted = true;
      this.meta.encryptedFields = fields || [];
    },

    useSyncStorage: function() {
      this.useLocalStorage();
      this.meta.syncStorage = true;
      this.meta.keepId = true;
      ABINE_DNTME.require(['abine/blurstore-api'], _.bind(function(blurstore) {
        blurstore.addTable(this.meta.name);
      },this));
    },

    loadFromLocalStorage: function() {
      var Entity = this;
      var needsEncryption = false, encryptionKey = null;
      var storageKey = Entity.meta.name;
      var str = localStorage.getItem(storageKey);
      var userId = securityManager.getUserId();
      if (userId && Entity.meta.encrypted) {
        encryptionKey = securityManager.getEncryptionKey();
        storageKey += '.'+userId;
        if (str) {
          if (!localStorage.getItem(storageKey)) {
            localStorage.setItem(storageKey, str);
          }
          localStorage.removeItem(Entity.meta.name);
        }
        str = localStorage.getItem(storageKey);
      }
      Entity.decryptionFailed = false;
      var objects = null;
      if (str && str != '') {
        if (str.indexOf('ENC_') != -1) {
          str = str.substr(4);
          if (encryptionKey) {
            LOG_FINEST && ABINE_DNTME.log.finest("decrypting "+storageKey+" object ");
            var plainStr = crypto.decryptKey(encryptionKey, str, null, true);
            if (plainStr == str) {
              Entity.decryptionFailed = true;
              throw "decryption error for "+this.meta.name;
            } else {
              str = plainStr;
            }
          } else {
            Entity.decryptionFailed = true;
            throw "encryption key not present for decrypting "+storageKey;
          }
        } else if (encryptionKey) {
          needsEncryption = true;
        }
        try {
          objects = JSON.parse(str);
        } catch(e){}
      } else {
        objects = this._defaults;
      }
      if (objects) {
        _.each(objects, function(obj){
          var entity = new Entity(obj);
          persistence.forceAdd(entity);
          entity._dirtyProperties = {};
          entity._new = false;
        });
      }
      LOG_FINEST && ABINE_DNTME.log.finest("loaded "+(objects&&objects.length||0)+" "+this.meta.name+" objects from local storage "+storageKey);
      if (this.meta.syncStorage) {
        ABINE_DNTME.require(['abine/blurstore-api'], _.bind(function(blurstore) {
          blurstore.load(Entity.meta.name);
        }, this));
      }
      if (needsEncryption) {
        Entity.saveToLocalStorage();
      }
    },

    saveToLocalStorage: function(callback) {
      LOG_FINEST && ABINE_DNTME.log.finest("saving to "+this.meta.name+" local storage ");
      var Entity = this;
      var userId = securityManager.getUserId();
      var storageKey = Entity.meta.name;
      if (userId && Entity.meta.encrypted) {
        storageKey += '.'+userId;
      }
      this.index(null, function(items){
        var jsonObjects = [];
        _.each(items, function(item){
          item._new = false;
          item = item.toJSON();
          if (!Entity.meta.keepId) {
            delete item['id'];
          }
          jsonObjects.push(item);
        });
        var oldVal = localStorage.getItem(storageKey);
        var newVal = JSON.stringify(jsonObjects);

        var doNoOverwrite = false;
        var encryptionKey = securityManager.getEncryptionKey();
        if (Entity.meta.encrypted) {
          if (encryptionKey) {
            LOG_FINEST && ABINE_DNTME.log.finest("encrypting "+this.meta.name+" object ");
            var cipherVal = crypto.encryptKey(encryptionKey, newVal);
            if (cipherVal != newVal) {
              newVal = 'ENC_'+cipherVal;
            }
          } else if (oldVal && oldVal.indexOf('ENC_') != -1) {
            doNoOverwrite = true;
          }
        }

        if (!doNoOverwrite) {
          localStorage.setItem(storageKey, newVal);
        } else {
          LOG_FINEST && ABINE_DNTME.log.finest("do not overwrite "+Entity.meta.name+" as it already as data encrypted with another key.");
        }

        triggerEntityChanged(Entity.meta.name);
        LOG_FINEST && ABINE_DNTME.log.finest("saved "+jsonObjects.length+" "+storageKey+" object to local storage ");

        if (this.meta.syncStorage) {
          ABINE_DNTME.require(['abine/blurstore-api'], _.bind(function(blurstore) {
            blurstore.save(Entity.meta.name, encryptionKey);
            if (callback) callback();
          }, this));
        } else if (callback) {
          callback();
        }
      });
    },

    updateCache: function(entity, changes) {
      persistence.removeIndex(entity);
      _.extend(entity, changes);
      persistence.addIndex(entity);
    },

    removeFromSyncStore: function(objectsToRemove, callback){
      callback = callback || function(){};
      var afterCallback = _.after(objectsToRemove.length, callback);
      ABINE_DNTME.require(['abine/blurstore-api'], _.bind(function(blurstore) {
        _.each(objectsToRemove, function(object){
            blurstore.delete(object.name, object.id);
            afterCallback();
        });
      }, this));
    }
  };

  persistence.flushHooks.push(function(session, tx, callback){
    var trackedObjects = persistence.trackedObjects;
    var changedEntities = [];
    var syncObjectsToRemove = [];
    var doneEntities = {};

    // handle new or changed entities
    for (var id in trackedObjects) {
      if (trackedObjects.hasOwnProperty(id)) {
        if (persistence.objectsToRemove.hasOwnProperty(id)) {
          continue;
        }

        var obj = trackedObjects[id];
        var Entity = persistence.define(obj._type);
        var entityTypeChanged = (Entity.meta.name in doneEntities);

        if (entityTypeChanged) {
          // already in changed list, so just remove flags and clear dirty properties
          if (obj._notInLocalStorage) {
            delete obj._notInLocalStorage;
          }
          obj._dirtyProperties = {};
          continue;
        }
        var changed = false;
        if (Entity.meta.localStorage) {
          if (obj._notInLocalStorage) {
            changed = true;
            delete obj._notInLocalStorage;
          } else {
            for (var p in obj._dirtyProperties) {
              if (obj._dirtyProperties.hasOwnProperty(p)) {
                changed = true;
                break;
              }
            }
            obj._dirtyProperties = {};
          }
          if (changed) {
            doneEntities[Entity.meta.name] = true;
            changedEntities.push(Entity);
          }
        }
      }
    }

    // handle deleted entities
    for (var id in persistence.objectsToRemove) {
      if (persistence.objectsToRemove.hasOwnProperty(id)) {
        var obj = persistence.objectsToRemove[id];
        var Entity = persistence.define(obj._type);
        if (Entity.meta.syncStorage) {
          syncObjectsToRemove.push({name: Entity.meta.name, id: id});
        }
        if (Entity.meta.localStorage) {
          if (!(Entity.meta.name in doneEntities)) {
            doneEntities[Entity.meta.name] = true;
            changedEntities.push(Entity);
          }
        }
        delete persistence.objectsToRemove[id];
        try{delete trackedObjects[id];}catch(e){}
      }
    }

    callback();
    _.each(changedEntities, function(Entity){
      Entity.saveToLocalStorage();
    });

    if (syncObjectsToRemove.length > 0) {
      Entity.removeFromSyncStore(syncObjectsToRemove);
    }
  });

  return {
    DefaultActions: DefaultActions
  };

});

// Storage router receives messages, forwards them to the correct controller action,
// and responds with a response message

ABINE_DNTME.define("abine/storageRouter",
    ['documentcloud/underscore', 'persistence', 'abine/core', 'abine/fixtures', 
     'abine/backgroundMessenger'],
    function(_, persistence, abineCore, abineFixtures, abineBackgroundMessenger) {

  var DEFAULT_DB_OPTIONS = {
    dbName: "idme_data",
    dbDescription: "Storage for the dntme extension",
    dbSizeInBytes: 5 * 1024 * 1024
    };

  var namedParam    = /:([\w\d]+)/g;
  var splatParam    = /\*([\w\d]+)/g;
  var escapeRegExp  = /[-[\]{}()+?.,\\^$|#\s]/g;

  // ## StorageRouter
  // The storage router initializes the entire storage system,
  // and routes all requests for storage access in and out.

  var StorageRouter = abineCore.BaseClass.extend({

    // ### function initialize(options)
    // - `messenger` - must be passed a messenger to listen on.
    //   maybe there is a global one it can pick up in the future.
    initialize: function(options) {
      options = options || {};
      var router = this, readyCallback = options.ready || function() {};
      this.routes = [];
      this.storage = {};
      _.defaults(options, DEFAULT_DB_OPTIONS);
      this.messenger = options.messenger || new abineBackgroundMessenger.BackgroundMessenger(); 
      // Start using websql storage, then create fixtures, then start the listener
      if (!StorageRouter.initialized) {
        options.ready = function() {
          router.startListener();
          readyCallback.call();
        }
        StorageRouter.initializeWebSQLStorage(options);
      } else {
        router.startListener();
        readyCallback.call();
      }
    },
    
    initializeMemoryStorage: function(callback) {
      persistence.store.memory.config(persistence);
      callback.call(this);
    },
    
    // ### function startListener()
    // Begin listening for storage requests
    startListener: function() {
      this.messenger.on('all', this.messageHandler, this);
    },
    
    // ### registerEntity(entityClass, entityName)
    // - `entityClass` - a storage entity constructor
    // - `entityName` - the camelCase name of the entity (ie "sampleEntity")
    //
    // Registers the entity and all of its routes with the StorageRouter.
    registerEntity: function(entityClass, entityName) {
      this.storage[entityName] = entityClass;
      _(entityClass.routes).each(function(routeName, route) {
        if (!routeName || !route) { throw("Bad routes on entity " + entityName); }
        if (!_.isRegExp(route)) { 
          // convert to regexp route
          route = route.replace(escapeRegExp, "\\$&")
                       .replace(namedParam, "([^\/]*)")
                       .replace(splatParam, "(.*?)");
          route = new RegExp('^' + route + '$',"i");
        }
        // Add to routes
        // 0 = the regexp route
        // 1 = the entity name
        // 2 = the method name on entity
        if (!route) { throw("Bad route for " + routeName); }
        this.routes.push([route, entityName, routeName]);
      }, this);
    },
    
    // ### function messageHandler(eventName, payload, sender)
    // - `eventName` - the full event name from the messenger
    // - `payload` - the message payload
    // - `sender` - id of the sender to respond to
    //
    // Accepts a message of the form "action:entity:route..." ie "read:sampleEntity:2"
    // And passes it to the correct method on the entity.
    // Any params in route are extracted, and merged with the payload. All this is passed in the request params.
    messageHandler: function(eventName, payload, sender) {
      var verb = eventName.split(":")[0];
      if (!_(['create', 'read', 'update', 'delete']).include(verb)) { 
        return; // this is not an event we care about. return. 
      }
      // Change colons to slashes for route match
      var routeEvent = eventName.replace(/\:/g, '/');
      routeEvent = routeEvent.replace(/\/\//g, ':');
      // Match against all our routes

      var matchedRoute = _(this.routes).find(function(route) {
        return routeEvent.match(route[0]);
      });
      
      if (matchedRoute) { 
        LOG_INFO && ABINE_DNTME.log.info("Routing storage request for " + routeEvent);
      } else {
        LOG_WARN && ABINE_DNTME.log.warn("No storage route matched for " + routeEvent);
      }

      // Build up a request and handle the message
      var entity = this.storage[matchedRoute[1]];
      var methodName = matchedRoute[2];
      var params = matchedRoute[0].exec(routeEvent).slice(1);
      var request = new StorageRequest({
        entityName: matchedRoute[1],
        route: eventName,
        payload: payload,
        sender: sender,
        params: params,
        messenger: this.messenger
      });
      // Call the entity method, with "this" bound to the entity class,
      // and passing the request object to the method.
      entity[methodName].call(entity, request);
      // Return the request object
      return request;
    },
    
    send: function() {
      return this.messageHandler.apply(this, arguments);
    }

  });
  
  StorageRouter.initializeWebSQLStorage = function(options) {
    var callback = options.ready || function() {};
    if (StorageRouter.initialized) { 
      LOG_WARN && ABINE_DNTME.log.warn("Ignoring attempt to reinitialize storage.");
      callback();
      return; 
    }
    persistence.store.memory.config(persistence, options.dbName, options.dbDescription, options.dbSizeInBytes);
    StorageRouter.initialized = true;
    LOG_INFO && ABINE_DNTME.log.info("Storage and persistence initialized.");
    if (options.fixtures) {
      abineFixtures.createFixtures(function() { callback(); });
    } else {
      callback();
    }
  }

  // Storage Request
  // When the router passes the request to the entity, it's wrapped in this storage request object
  
  var StorageRequest = abineCore.BaseClass.extend({
    
    // ### function initialize(options)
    // - `route` - the original message route
    // - `params` - an array of the params extracted from the route
    // - `payload` - event payload
    // - `entityName` - the name of the entity to be sent to
    // - `sender` - the sender of the original message
    // - `messenger` - the background messenger to use to send the response
    initialize: function(options) {
      options = options || {};
      this.route = options.route;
      this.params = options.params;
      this.entityName = options.entityName;
      this.sender = options.sender || "background";
      this.messenger = options.messenger;
      this.payload = options.payload;
    },
    
    // ### function getReturnRoute()
    // Returns the route for the response message. The action is swapped with "response".
    // ie "read:sampleEntity:all" => "response:sampleEntity:all"
    getReturnRoute: function() {
      var routeParts = this.route.split(':');
      routeParts[0] = "response";
      return routeParts.join(':');
    },

    getReturnErrorRoute: function(){
      var routeParts = this.route.split(":");
      routeParts[0] = "error";
      return routeParts.join(":");
    },

    returnErrObj: function(payload){
      payload = payload || {};
      this.messenger.send(this.getReturnErrorRoute(this.route), payload, this.sender);
      return true;
    },

    returnError: function(msg, status){
      status = status || 500;
      msg = msg || "Server error";
      var payload = { error: true, message: msg, status: status};
      this.messenger.send(this.getReturnErrorRoute(this.route), payload, this.sender);
      return true;
    },
    
    // ### function returnStatus(status)
    // - `status` - String status to include in return message payload
    //
    // Sends a return message with a simple status response.
    returnStatus: function(status) {
      var payload = { status: status };
      this.messenger.send(this.getReturnRoute(this.route), payload, this.sender);
      return true;
    },
    
    // ### function returnEntities(entities)
    // - `entities` - a single entity, or an array of entities
    // 
    // Sends a return message with a payload of the given entities.
    // If a single entity is passed, the payload will be JSON for that entity.
    // If an array is passed, the payload will be an array of entities.
    // The entity will be serialized with the default `toJSON` method.
    returnEntities: function(entities, jsonSpec) {
      var payload;
      if (!entities || entities.length == 0) {
        payload = [];
      } else if (_.isArray(entities) && entities.length == 1) {
          payload = entities[0].toJSON();
      } else if (_.isArray(entities) && entities.length > 1) {
          payload = _(entities).map(function(e) { return e.toJSON(); });
      } else if (_.isObject(entities)) {
          payload = entities.toJSON();
      }
      this.messenger.send(this.getReturnRoute(this.route), payload, this.sender);
    },
    
    returnJSON: function(payload) {
      this.messenger.send(this.getReturnRoute(this.route), payload, this.sender);
    }
    
  });

  return {
    StorageRouter: StorageRouter,
    StorageRequest: StorageRequest
  };

});

ABINE_DNTME.define("abine/pushNotifications",
  ['documentcloud/underscore', 'abine/core', 'storage',
    'abine/timer', 'abine/securityManager', 'abine/ajax', 'modules', 'abine/crypto'
  ],
  function(_, abineCore, storage, timer, securityManager, $, modules, crypto) {

    var biometricAuthCallback = null;

    var firstCall = true;

    // start with websocket only, as that reduces # of requests to push server. normal socket.io handshake will require 3 more requests.
    var transports = ['websocket'];

    var reconnectAttempts = 0, reconnectTimer;
    var MS = 1000;
    var myId = Math.floor(Math.random() * 10000000);

    function connect() {
      var go = function(){
        var Socket = ABINE_DNTME.Socket;
        if (!Socket) Socket = io;
        this.socket = Socket.connect(ABINE_DNTME.config.pushNotificationUrl, {
          query: "authToken=" + securityManager.getAuthToken() + "&env=" + ABINE_DNTME.config.environment,
          multiplex: false,
          reconnection: true,
          reconnectionDelay: 10000,
          reconnectionDelayMax: 60000,
          reconnectionAttempts: 5,
          transports: transports
        });

        this.socket.myId = myId;

        // listen for messages
        this.socket.on('connect', function(){reconnectAttempts=0;});
        this.socket.on('connect_error', function(err){
          if (err.message == 'websocket error') {
            // websocket didnt work.  fallback to regular socket.io handshake.
            transports = null;
            timer.setTimeout(connect, 3000);
            try{disconnect();}catch(e){}
          }

        });
        this.socket.on('error', _.bind(this.error, this));
        this.socket.on('reconnect_failed', _.bind(function(){
          disconnect.call(this);
          delayReconnect.call(this);
        }, this));
        this.socket.on('logout', _.bind(this.logout, this));
        this.socket.on('sync', _.bind(this.sync, this));
        this.socket.on('mfaChanged', _.bind(this.mfaChanged, this));
        this.socket.on('storeChanged', _.bind(this.storeChanged, this));
        this.socket.on('biometricAuthResponse', _.bind(this.biometricAuthResponse, this));
        this.socket.on('notify', _.bind(this.notify, this));
        this.socket.on('growl', _.bind(this.growl, this));
      };
      if (!firstCall) {
        $.ajax({
          url: ABINE_DNTME.config.pushNotificationUrl+'/ping',
          xhrFields: {withCredentials: true},
          success: _.bind(go, this),
          error: _.bind(delayReconnect, this)
        });
      } else {
        firstCall = false;
        timer.setTimeout(_.bind(go, this), 0);
      }
    }

    function disconnect() {
      if (this.socket) {
        try{this.socket.disconnect();}catch(e){}
        this.socket.off('connect');
        this.socket.off('connect_error');
        this.socket.off('error');
        this.socket.off('reconnect_failed');
        this.socket.off('logout');
        this.socket.off('sync');
        this.socket.off('mfaChanged');
        this.socket.off('storeChanged');
        this.socket.off('biometricAuthResponse');
        this.socket.off('notify');
        this.socket.off('growl');
        this.socket = null;
      }
    }

    function delayReconnect() {
		  // random exponential delay between 2 and 128 seconds, once it reaches 128 seconds it will restart from 16 seconds
      if (reconnectAttempts++ > 6)
        reconnectAttempts = 4;
      var delay = (Math.pow(2,reconnectAttempts)*MS) + (Math.round(Math.random() * MS));
      LOG_DEBUG && ABINE_DNTME.log.debug("push server reconnect delayed by "+delay/MS+" seconds");
      timer.clearTimeout(reconnectTimer);
      reconnectTimer = timer.setTimeout(_.bind(connect, this), delay);
    }

    function sendToWebapp(event, payload) {
      ABINE_DNTME.backgroundMessenger.sendToWebapp(event, payload);
    }

    var Api = abineCore.BaseClass.extend({
      connect: function() {
        if (!this.socket) {
          connect.call(this);
        } else {
          this.socket.connect();
        }
      },

      disconnect: function() {
        if (this.socket) {
          disconnect.call(this);
        }
      },

      error: function(message) {
        reconnectAttempts = 0;
        if (message == 'not authorized') {
          securityManager.logout();
        }
      },

      logout: function() {
        LOG_DEBUG && ABINE_DNTME.log.debug("Logout notification received");
        securityManager.logout();
        sendToWebapp("websocket:push", {event: 'logout', payload: null})
      },

      sync: function(syncCount) {
        LOG_DEBUG && ABINE_DNTME.log.debug("Sync notification received "+syncCount);
        ABINE_DNTME.require(['abine/blurstore-api'], function(blurStore){
          blurStore.synchronizeWhenOld(syncCount);
        });
        sendToWebapp("websocket:push", {event: 'sync', payload: syncCount})
      },

      mfaChanged: function() {
        LOG_DEBUG && ABINE_DNTME.log.debug("MFA changed notification received ");
        securityManager.refreshData();
        sendToWebapp("websocket:push", {event: 'mfaChanged', payload: null})
      },

      storeChanged: function(data) {
        LOG_DEBUG && ABINE_DNTME.log.debug("Store Changed notification received "+data.store);
        securityManager.refreshData();
        sendToWebapp("websocket:push", {event: 'storeChanged', payload: data})
      },

      pushSync: function(syncCount) {
        modules.licenseModule.pushSync(syncCount);
        if (this.socket) {
          this.socket.emit('broadcast', {event: 'sync', payload: syncCount});
        }
      },

      emit: function(event, payload) {
        if (this.socket) {
          LOG_DEBUG && ABINE_DNTME.log.debug("** Sending event from webapp "+event+" "+JSON.stringify(payload));
          this.socket.emit('broadcast', {event: event, payload: payload});
        }
      },

      biometricAuthResponse: function(response) {
        if (biometricAuthCallback) {
          biometricAuthCallback(response);
          if (response && (response.ack || (response.error && response.error.match(/locked/i)))) {
            return;
          }
          biometricAuthCallback = null;
        } else {
          sendToWebapp("websocket:push", {event: 'biometricAuthResponse', payload: response})
        }
      },

      biometricAuthCancel: function() {
        biometricAuthCallback = null;
      },

      requestBiometricAuth: function(type, key, domain, amount, number, account, callback) {
        var self = this;
        timer.clearTimeout(this.biometricTimer);
        if (biometricAuthCallback) {
          try{biometricAuthCallback({success: false});}catch(e){}
        }
        biometricAuthCallback = callback;
        var params = {
          event: "biometricAuth",
          payload: {
            type: type,
            biometricKey: key,
            amount: amount,
            number: number,
            account: account,
            sender: myId,
            domain: domain,
            domain_hash: crypto.sha1("abine"+domain).toString()
          }
        };

        modules.licenseModule.createBiometricAuthRequest(params, function(response){
          function pollRequest(retry) {
            modules.licenseModule.getBiometricAuthResponse(null, function (data) {
              if (!data.status) {
                if (retry > 0) {
                  self.biometricTimer = timer.setTimeout(function(){pollRequest(retry-1);}, 5000);
                } else {
                  self.biometricAuthResponse({success: false, error: "No response from mobile device"});
                }
              } else {
                self.biometricAuthResponse({success: data.status == 'success', token: data.token, error: data.status});
                if (data.status && data.status.match(/locked/i)) {
                  self.biometricTimer = timer.setTimeout(function(){pollRequest(retry-1);}, 5000);
                }
              }
            });
          }
          if (response.success) {
            // successful call to create biometric request can be treated as ACK
            self.biometricAuthResponse({
              ack: true
            });
            pollRequest(20);
          } else {
            self.biometricAuthResponse(response);
          }
        });
      },

      growl: function (obj) {
        if (typeof obj == 'string') {
          try {
            obj = JSON.parse(obj);
          } catch(e){}
        }
        if (obj.name && obj.name.match(/card-declined/)) {
          storage.DisposableCard.findOrFetchCard(obj.guid, function(card) {
            if (!card || card.source != 'dnt-panel' || !card.active) return;
            var notification = {
              msg: "",
              targetUrl: ABINE_DNTME.config.protocol + ABINE_DNTME.config.webAppHost + "/#/cards",
              targetId: "",
              model: {maskedCard: card.toJSON(), declined: obj},
              height: 260,
              type: 'card',
              neverFade: true,
              locked: securityManager.isLocked()
            };
            securityManager.messenger.broadcastToAllTabs('background:show:notification:'+card.domain, notification);
          });
        }
        sendToWebapp("websocket:push", {event: 'growl', payload: data})
      },

      notify: function (notification_type) {
        notification_type = JSON.parse(notification_type);
        if (notification_type == "expiration_warning") {
          var notification = {
            type: "expire_warning",
            neverFade: true,
            panelWidth: 320,
            locked: securityManager.isLocked()
          };
          securityManager.messenger.send('background:show:notification', notification);
        } else if (notification_type == "expiration_notification") {
          var notification = {
            type: "expire_notify",
            neverFade: true,
            panelWidth: 320,
            locked: securityManager.isLocked()
          };
          securityManager.messenger.send('background:show:notification', notification);
        }
        sendToWebapp("websocket:push", {event: 'notify', payload: notification_type})
      },

      getId: function() {
        return myId;
      }
    });

    return new Api();
  }
);
ABINE_DNTME.define("abine/stubs",
    ["documentcloud/underscore", "abine/core"],
  function (_, abineCore) {

  var backgroundMessenger;

  function start(backgroundMsgr) {
    backgroundMessenger = backgroundMsgr;
    backgroundMsgr.on('contentManager:proxy:connect', function(callId, sender){
      backgroundMessenger.send('background:stub:response:'+callId, sender, sender);
    });
    backgroundMsgr.on('contentManager:proxy:call', function(payload, sender){
      ABINE_DNTME.require([payload.require], function(realObj) {
        var callId = payload.callId;
        var response = realObj[payload.method].apply(realObj, payload.params);
        backgroundMessenger.send('background:stub:response:'+callId, response, sender);
      });
    });
    backgroundMsgr.on('contentManager:proxy:callAsync', function(payload, sender){
      ABINE_DNTME.require([payload.require], function(realObj) {
        var callId = payload.callId;
        var callback = function(err, data){
          backgroundMessenger.send('background:stub:response:'+callId, {err:err, data: data}, sender);
        };

        payload.params = payload.params || [];
        payload.params.push(callback);

        realObj[payload.method].apply(realObj, payload.params);
      });
    });
  }

  // used for tests
  function stop() {
    if (backgroundMessenger) {
      backgroundMessenger.off('contentManager:proxy:connect');
      backgroundMessenger.off('contentManager:proxy:call');
      backgroundMessenger.off('contentManager:proxy:callAsync');
      backgroundMessenger = null;
    }
  }
  return {
    start: start,
    stop: stop
  };
});
ABINE_DNTME.define("abine/webapp",
  ["documentcloud/underscore", "abine/core", "storage", "abine/securityManager", 'abine/window'],
function(_, abineCore, storage, securityManager, abineWindow) {

  function openPage(page, panelData, refreshOnly) {
    storage.Preference.findBy('key', 'mergedMigrationPending', function(pref) {
      if (pref && pref.isTrue()) {
        securityManager.openPage('/#/migrate');
      } else {
        securityManager.openPage(page, panelData, refreshOnly);
      }
    })
  }

  function openHome(panelData) {
    openPage('/#/client/home', panelData);
  }

  function openTour() {
      openPage('/#/client/dashboard/guide');
  }

  function openDashboard(refreshOnly) {
    openPage('/#/client/dashboard', false, refreshOnly);
  }

  function openEmails() {
    openPage('/#/client/emails');
  }

  function openAccounts(domain) {
    if (domain)
      openPage('/#/client/accounts/'+domain+'/1');
    else
      openPage('/#/client/accounts');
  }

  function openWallet() {
    openPage('/#/client/wallet');
  }

  function openMasking() {
    openPage('/#/client/masking');
  }

  function openHelp(modal) {
    modal = modal || "";
    openPage('/#/client/help' + modal);
  }

  function openForgotPassword() {
    openPage('/#/forgot');
  }

  function openFAQ(type, promotion) {
    type = type || "";
    promotion = promotion || "";
    var url = '/#/help/faq' + '/' + type;
    if(promotion){
      url = url + '/' + promotion;
    }
    openPage(url);
  }

  function openMaskedEmailsIn30() {
    openPage('/#/client/emailsin30');
  }

  function openStats() {
    if (ABINE_DNTME.maskme_installed)
      securityManager.openLink({type: 'dntReport'});
    else
      openPage('/#/client/stats');
  }

  function openUnderstandingTracking() {
    openPage('/#/client/understandingTracking');
  }

  function openCashForCardsSignup() {
    storage.Preference.findBy('key', 'cardsEarned', function(pref){
      if(pref){
        openPage('/#/client/cashforcards/signup/' + pref.value);
      } else {
        openPage('/#/client/cashforcards/signup');
      }
    });
  }

  function openCashForCardsFaq() {
    storage.Preference.findBy('key', 'cardsEarned', function(pref){
      if(pref){
        openPage('/#/client/cashforcards/faq/' + pref.value);
      } else {
        openPage('/#/client/cashforcards/faq');
      }
    });
  }

  function openDashboardOrStats() {
    storage.Preference.findBy("key","authToken", function(pref) {
      if (pref && pref.value && pref.value.length > 0) {
        openDashboard();
      } else {
        openStats();
      }
    });
  }

  function openPhones() {
    storage.Preference.batchFindBy([
      {by:"key",value:"authToken"},
      {by:"key",value:"entitledToPhone"}
    ], function(prefMap){
      var loggedIn = prefMap.authToken && prefMap.authToken.value != "";
      var phonePref = prefMap.entitledToPhone;

      if (!loggedIn) {
        openPage('/#/client/premium');
      } else if(!phonePref || phonePref.isFalse()){
        openPage('/#/client/premium');
      } else {
        storage.ShieldedPhone.index(null, function(phones){
          if (phones && phones.length > 0 && !phones[0].isValidated())
            openPage('/#/client/verifyPhone');
          else
            openPage('/#/client/phones');
        });
      }
    });
  }

  function openPremium(link, feature, panelData) {
    link = link?"/link/"+link:"";
    feature = feature || "";
    storage.Preference.batchFindBy([
      {by:"key",value:"authToken"},
      {by:"key",value:"entitledToPhone"}
    ], function(prefMap){
      var loggedIn = prefMap.authToken && prefMap.authToken.value != "";
      var entitiledToPhone = prefMap.entitledToPhone;

      if (!loggedIn) {
        openPage('/#'+link+'/client/premium/' + feature, panelData);
      } else if(!entitiledToPhone || entitiledToPhone.isFalse()){
        openPage('/#'+link+'/client/premium/' + feature, panelData);
      } else {
        openPage('/#/client/settings');
      }
    });
  }

  function openPiwikCampaign(campaign, page) {
    openPage('/?pk_campaign='+campaign+'#'+page);
  }

  function openNewFeatureAdded(type, panelData) {
    openPage('/#/client/featureAdded');
  }

  function openSnowman(type, panelData) {
    type = type || "";
    storage.Preference.batchFindBy([
      {by:"key",value:"authToken"},
      {by:"key",value:"entitledToPhone"}
    ], function(prefMap){
      var loggedIn = prefMap.authToken && prefMap.authToken.value != "";
      var entitiledToPhone = prefMap.entitledToPhone;

      if (!loggedIn) {
        openPage('/#/client/holiday/' + type, panelData);
      } else if(!entitiledToPhone || entitiledToPhone.isFalse()){
        openPage('/#/client/holiday/' + type, panelData);
      } else {
        openPage('/#/client/settings');
      }
    });
  }

  function openCards(panelData) {
    storage.ShieldedCard.index(null, function(cards) {
      storage.Preference.batchFindBy([
        {by:"key",value:"authToken"},
        {by:"key",value:"entitledToPayments"},
        {by:"key",value:"hasEnabledPayments"}
      ], function(prefMap){
        var loggedIn = prefMap.authToken && prefMap.authToken.value != "";
        var entitledPref = prefMap.entitledToPayments;

        if (!loggedIn) {
          openPage('/#/client/premium', panelData);
        } else if(!entitledPref || entitledPref.isFalse()){
          openPage('/#/client/premium', panelData);
        } else if (cards.length > 0) {
          openPage('/#/client/cards', panelData);
        } else {
          openPage('/#/client/premium/setup/dashboard', panelData);
        }
      })
    });
  }

  function openSettings() {
    openPage('/#/client/settings');
  }

  function openLogin(type) {
    type = type || "";    
    openPage('/#/client/login/' + type);
  }

  function openDnt2UpgradePage() {
    openPage('/#/dnt2upgrade')
  }

  function openPasswordSetup(email) {
    email = email || "";
    openPage('/#/client/setupPassword/' + email);
  }

  function openBlacklist() {
    openSettings();
  }

  function openWeeklyReport() {
    openPage('/#/client/weeklyReport');
  }

  function openDntAnywhere() {
    openPage('/#/mobilePrivacy');
  }

  function reload() {
    abineWindow.reloadTab({doNotActivate: true, doNotOpenNewTab: true, url: ABINE_DNTME.config.protocol+ABINE_DNTME.config.webAppHost})
  }

  return {
    reload: reload,
    openHome: openHome,
    openDashboard: openDashboard,
    openWallet: openWallet,
    openMasking: openMasking,
    openEmails: openEmails,
    openStats: openStats,
    openUnderstandingTracking: openUnderstandingTracking,
    openCards: openCards,
    openPhones: openPhones,
    openLogin: openLogin,
    openNewFeatureAdded: openNewFeatureAdded,
    openSnowman: openSnowman,
    openPremium: openPremium,
    openBlacklist: openBlacklist,
    openSettings: openSettings,
    openPasswordSetup: openPasswordSetup,
    openPiwikCampaign: openPiwikCampaign,
    openHelp: openHelp,
    openForgotPassword: openForgotPassword,
    openFAQ: openFAQ,
    openDashboardOrStats: openDashboardOrStats,
    openDnt2UpgradePage: openDnt2UpgradePage,
    openMaskedEmailsIn30: openMaskedEmailsIn30,
    openPage: openPage,
    openWeeklyReport: openWeeklyReport,
    openDntAnywhere: openDntAnywhere,
    openCashForCardsFaq: openCashForCardsFaq,
    openCashForCardsSignup: openCashForCardsSignup,
    openTour: openTour,
    openAccounts: openAccounts
  };

});





































ABINE_DNTME.define('blueimp-md5', function(){
  return function(input){
    var i, output = [];
    output[(input.length >> 2) - 1] = undefined;
    for (i = 0; i < output.length; i += 1) {
      output[i] = 0;
    }
    for (i = 0; i < input.length * 8; i += 8) {
      output[i >> 5] |= (input.charCodeAt(i / 8)) << (i % 32);
    }
    return MD5(output, input.length);
  };
});
/*
 * JavaScript MD5 1.0.1
 * https://github.com/blueimp/JavaScript-MD5
 *
 * Copyright 2011, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 *
 * Based on
 * A JavaScript implementation of the RSA Data Security, Inc. MD5 Message
 * Digest Algorithm, as defined in RFC 1321.
 * Version 2.2 Copyright (C) Paul Johnston 1999 - 2009
 * Other contributors: Greg Holt, Andrew Kepert, Ydnar, Lostinet
 * Distributed under the BSD License
 * See http://pajhome.org.uk/crypt/md5 for more info.
 */
/*jslint bitwise: true */
/*global unescape, define */


(function ($) {
  'use strict';

  /*
   * Add integers, wrapping at 2^32. This uses 16-bit operations internally
   * to work around bugs in some JS interpreters.
   */
  function safe_add(x, y) {
    var lsw = (x & 0xFFFF) + (y & 0xFFFF),
      msw = (x >> 16) + (y >> 16) + (lsw >> 16);
    return (msw << 16) | (lsw & 0xFFFF);
  }

  /*
   * Bitwise rotate a 32-bit number to the left.
   */
  function bit_rol(num, cnt) {
    return (num << cnt) | (num >>> (32 - cnt));
  }

  /*
   * These functions implement the four basic operations the algorithm uses.
   */
  function md5_cmn(q, a, b, x, s, t) {
    return safe_add(bit_rol(safe_add(safe_add(a, q), safe_add(x, t)), s), b);
  }
  function md5_ff(a, b, c, d, x, s, t) {
    return md5_cmn((b & c) | ((~b) & d), a, b, x, s, t);
  }
  function md5_gg(a, b, c, d, x, s, t) {
    return md5_cmn((b & d) | (c & (~d)), a, b, x, s, t);
  }
  function md5_hh(a, b, c, d, x, s, t) {
    return md5_cmn(b ^ c ^ d, a, b, x, s, t);
  }
  function md5_ii(a, b, c, d, x, s, t) {
    return md5_cmn(c ^ (b | (~d)), a, b, x, s, t);
  }

  /*
   * Calculate the MD5 of an array of little-endian words, and a bit length.
   */
  function binl_md5(x, len) {
    /* append padding */
    x[len >> 5] |= 0x80 << (len % 32);
    x[(((len + 64) >>> 9) << 4) + 14] = len;

    var i, olda, oldb, oldc, oldd,
      a =  1732584193,
      b = -271733879,
      c = -1732584194,
      d =  271733878;

    for (i = 0; i < x.length; i += 16) {
      olda = a;
      oldb = b;
      oldc = c;
      oldd = d;

      a = md5_ff(a, b, c, d, x[i],       7, -680876936);
      d = md5_ff(d, a, b, c, x[i +  1], 12, -389564586);
      c = md5_ff(c, d, a, b, x[i +  2], 17,  606105819);
      b = md5_ff(b, c, d, a, x[i +  3], 22, -1044525330);
      a = md5_ff(a, b, c, d, x[i +  4],  7, -176418897);
      d = md5_ff(d, a, b, c, x[i +  5], 12,  1200080426);
      c = md5_ff(c, d, a, b, x[i +  6], 17, -1473231341);
      b = md5_ff(b, c, d, a, x[i +  7], 22, -45705983);
      a = md5_ff(a, b, c, d, x[i +  8],  7,  1770035416);
      d = md5_ff(d, a, b, c, x[i +  9], 12, -1958414417);
      c = md5_ff(c, d, a, b, x[i + 10], 17, -42063);
      b = md5_ff(b, c, d, a, x[i + 11], 22, -1990404162);
      a = md5_ff(a, b, c, d, x[i + 12],  7,  1804603682);
      d = md5_ff(d, a, b, c, x[i + 13], 12, -40341101);
      c = md5_ff(c, d, a, b, x[i + 14], 17, -1502002290);
      b = md5_ff(b, c, d, a, x[i + 15], 22,  1236535329);

      a = md5_gg(a, b, c, d, x[i +  1],  5, -165796510);
      d = md5_gg(d, a, b, c, x[i +  6],  9, -1069501632);
      c = md5_gg(c, d, a, b, x[i + 11], 14,  643717713);
      b = md5_gg(b, c, d, a, x[i],      20, -373897302);
      a = md5_gg(a, b, c, d, x[i +  5],  5, -701558691);
      d = md5_gg(d, a, b, c, x[i + 10],  9,  38016083);
      c = md5_gg(c, d, a, b, x[i + 15], 14, -660478335);
      b = md5_gg(b, c, d, a, x[i +  4], 20, -405537848);
      a = md5_gg(a, b, c, d, x[i +  9],  5,  568446438);
      d = md5_gg(d, a, b, c, x[i + 14],  9, -1019803690);
      c = md5_gg(c, d, a, b, x[i +  3], 14, -187363961);
      b = md5_gg(b, c, d, a, x[i +  8], 20,  1163531501);
      a = md5_gg(a, b, c, d, x[i + 13],  5, -1444681467);
      d = md5_gg(d, a, b, c, x[i +  2],  9, -51403784);
      c = md5_gg(c, d, a, b, x[i +  7], 14,  1735328473);
      b = md5_gg(b, c, d, a, x[i + 12], 20, -1926607734);

      a = md5_hh(a, b, c, d, x[i +  5],  4, -378558);
      d = md5_hh(d, a, b, c, x[i +  8], 11, -2022574463);
      c = md5_hh(c, d, a, b, x[i + 11], 16,  1839030562);
      b = md5_hh(b, c, d, a, x[i + 14], 23, -35309556);
      a = md5_hh(a, b, c, d, x[i +  1],  4, -1530992060);
      d = md5_hh(d, a, b, c, x[i +  4], 11,  1272893353);
      c = md5_hh(c, d, a, b, x[i +  7], 16, -155497632);
      b = md5_hh(b, c, d, a, x[i + 10], 23, -1094730640);
      a = md5_hh(a, b, c, d, x[i + 13],  4,  681279174);
      d = md5_hh(d, a, b, c, x[i],      11, -358537222);
      c = md5_hh(c, d, a, b, x[i +  3], 16, -722521979);
      b = md5_hh(b, c, d, a, x[i +  6], 23,  76029189);
      a = md5_hh(a, b, c, d, x[i +  9],  4, -640364487);
      d = md5_hh(d, a, b, c, x[i + 12], 11, -421815835);
      c = md5_hh(c, d, a, b, x[i + 15], 16,  530742520);
      b = md5_hh(b, c, d, a, x[i +  2], 23, -995338651);

      a = md5_ii(a, b, c, d, x[i],       6, -198630844);
      d = md5_ii(d, a, b, c, x[i +  7], 10,  1126891415);
      c = md5_ii(c, d, a, b, x[i + 14], 15, -1416354905);
      b = md5_ii(b, c, d, a, x[i +  5], 21, -57434055);
      a = md5_ii(a, b, c, d, x[i + 12],  6,  1700485571);
      d = md5_ii(d, a, b, c, x[i +  3], 10, -1894986606);
      c = md5_ii(c, d, a, b, x[i + 10], 15, -1051523);
      b = md5_ii(b, c, d, a, x[i +  1], 21, -2054922799);
      a = md5_ii(a, b, c, d, x[i +  8],  6,  1873313359);
      d = md5_ii(d, a, b, c, x[i + 15], 10, -30611744);
      c = md5_ii(c, d, a, b, x[i +  6], 15, -1560198380);
      b = md5_ii(b, c, d, a, x[i + 13], 21,  1309151649);
      a = md5_ii(a, b, c, d, x[i +  4],  6, -145523070);
      d = md5_ii(d, a, b, c, x[i + 11], 10, -1120210379);
      c = md5_ii(c, d, a, b, x[i +  2], 15,  718787259);
      b = md5_ii(b, c, d, a, x[i +  9], 21, -343485551);

      a = safe_add(a, olda);
      b = safe_add(b, oldb);
      c = safe_add(c, oldc);
      d = safe_add(d, oldd);
    }
    return [a, b, c, d];
  }

  /*
   * Convert an array of little-endian words to a string
   */
  function binl2rstr(input) {
    var i,
      output = '';
    for (i = 0; i < input.length * 32; i += 8) {
      output += String.fromCharCode((input[i >> 5] >>> (i % 32)) & 0xFF);
    }
    return output;
  }

  /*
   * Convert a raw string to an array of little-endian words
   * Characters >255 have their high-byte silently ignored.
   */
  function rstr2binl(input) {
    var i,
      output = [];
    output[(input.length >> 2) - 1] = undefined;
    for (i = 0; i < output.length; i += 1) {
      output[i] = 0;
    }
    for (i = 0; i < input.length * 8; i += 8) {
      output[i >> 5] |= (input.charCodeAt(i / 8) & 0xFF) << (i % 32);
    }
    return output;
  }

  /*
   * Calculate the MD5 of a raw string
   */
  function rstr_md5(s) {
    return binl2rstr(binl_md5(rstr2binl(s), s.length * 8));
  }

  /*
   * Calculate the HMAC-MD5, of a key and some data (raw strings)
   */
  function rstr_hmac_md5(key, data) {
    var i,
      bkey = rstr2binl(key),
      ipad = [],
      opad = [],
      hash;
    ipad[15] = opad[15] = undefined;
    if (bkey.length > 16) {
      bkey = binl_md5(bkey, key.length * 8);
    }
    for (i = 0; i < 16; i += 1) {
      ipad[i] = bkey[i] ^ 0x36363636;
      opad[i] = bkey[i] ^ 0x5C5C5C5C;
    }
    hash = binl_md5(ipad.concat(rstr2binl(data)), 512 + data.length * 8);
    return binl2rstr(binl_md5(opad.concat(hash), 512 + 128));
  }

  /*
   * Convert a raw string to a hex string
   */
  function rstr2hex(input) {
    var hex_tab = '0123456789abcdef',
      output = '',
      x,
      i;
    for (i = 0; i < input.length; i += 1) {
      x = input.charCodeAt(i);
      output += hex_tab.charAt((x >>> 4) & 0x0F) +
        hex_tab.charAt(x & 0x0F);
    }
    return output;
  }

  /*
   * Encode a string as utf-8
   */
  function str2rstr_utf8(input) {
    return unescape(encodeURIComponent(input));
  }

  /*
   * Take string arguments and return either raw or hex encoded strings
   */
  function raw_md5(s) {
    return rstr_md5(str2rstr_utf8(s));
  }
  function hex_md5(s) {
    return rstr2hex(raw_md5(s));
  }
  function raw_hmac_md5(k, d) {
    return rstr_hmac_md5(str2rstr_utf8(k), str2rstr_utf8(d));
  }
  function hex_hmac_md5(k, d) {
    return rstr2hex(raw_hmac_md5(k, d));
  }

  function md5(string, key, raw) {
    if (!key) {
      if (!raw) {
        return hex_md5(string);
      }
      return raw_md5(string);
    }
    if (!raw) {
      return hex_hmac_md5(key, string);
    }
    return raw_hmac_md5(key, string);
  }

  $.MD5 = function(s, len) {
    return rstr2hex(binl2rstr(binl_md5(s, len * 8)));
  };
}(this));
ABINE_DNTME.define('asmCrypto', function(){
  var define, module, require;
  
  /*! asmCrypto, (c) 2013 Artem S Vybornov, opensource.org/licenses/MIT */

!function(a,b){function c(){var a=Error.apply(this,arguments);this.message=a.message,this.stack=a.stack}function d(){var a=Error.apply(this,arguments);this.message=a.message,this.stack=a.stack}function e(){var a=Error.apply(this,arguments);this.message=a.message,this.stack=a.stack}function f(a,b){b=!!b;for(var c=a.length,d=new Uint8Array(b?4*c:c),e=0,f=0;c>e;e++){var g=a.charCodeAt(e);if(b&&g>=55296&&56319>=g){if(++e>=c)throw new Error("Malformed string, low surrogate expected at position "+e);g=(55296^g)<<10|65536|56320^a.charCodeAt(e)}else if(!b&&g>>>8)throw new Error("Wide characters are not allowed.");!b||127>=g?d[f++]=g:2047>=g?(d[f++]=192|g>>6,d[f++]=128|63&g):65535>=g?(d[f++]=224|g>>12,d[f++]=128|g>>6&63,d[f++]=128|63&g):(d[f++]=240|g>>18,d[f++]=128|g>>12&63,d[f++]=128|g>>6&63,d[f++]=128|63&g)}return d.subarray(0,f)}function g(a){var b=a.length;1&b&&(a="0"+a,b++);for(var c=new Uint8Array(b>>1),d=0;b>d;d+=2)c[d>>1]=parseInt(a.substr(d,2),16);return c}function h(a){return f(atob(a))}function i(a,b){b=!!b;for(var c=a.length,d=new Array(c),e=0,f=0;c>e;e++){var g=a[e];if(!b||128>g)d[f++]=g;else if(g>=192&&224>g&&c>e+1)d[f++]=(31&g)<<6|63&a[++e];else if(g>=224&&240>g&&c>e+2)d[f++]=(15&g)<<12|(63&a[++e])<<6|63&a[++e];else{if(!(g>=240&&248>g&&c>e+3))throw new Error("Malformed UTF8 character at byte offset "+e);var h=(7&g)<<18|(63&a[++e])<<12|(63&a[++e])<<6|63&a[++e];65535>=h?d[f++]=h:(h^=65536,d[f++]=55296|h>>10,d[f++]=56320|1023&h)}}for(var i="",j=16384,e=0;f>e;e+=j)i+=String.fromCharCode.apply(String,d.slice(e,f>=e+j?e+j:f));return i}function j(a){for(var b="",c=0;c<a.length;c++){var d=(255&a[c]).toString(16);d.length<2&&(b+="0"),b+=d}return b}function k(a){return btoa(i(a))}function l(a){return a-=1,a|=a>>>1,a|=a>>>2,a|=a>>>4,a|=a>>>8,a|=a>>>16,a+=1}function m(a){return"number"==typeof a}function n(a){return"string"==typeof a}function o(a){return a instanceof ArrayBuffer}function p(a){return a instanceof Uint8Array}function q(a){return a instanceof Int8Array||a instanceof Uint8Array||a instanceof Int16Array||a instanceof Uint16Array||a instanceof Int32Array||a instanceof Uint32Array||a instanceof Float32Array||a instanceof Float64Array}function r(a,b){var c=b.heap,d=c?c.byteLength:b.heapSize||65536;if(4095&d||0>=d)throw new Error("heap size must be a positive integer and a multiple of 4096");return c=c||new a(new ArrayBuffer(d))}function s(a,b,c,d,e){var f=a.length-b,g=e>f?f:e;return a.set(c.subarray(d,d+g),b),g}function t(a){a=a||{},this.heap=r(Uint8Array,a).subarray(Xb.HEAP_DATA),this.asm=a.asm||Xb(b,null,this.heap.buffer),this.mode=null,this.key=null,this.reset(a)}function u(a){if(void 0!==a){if(o(a)||p(a))a=new Uint8Array(a);else{if(!n(a))throw new TypeError("unexpected key type");a=f(a)}var b=a.length;if(16!==b&&24!==b&&32!==b)throw new d("illegal key size");var c=new DataView(a.buffer,a.byteOffset,a.byteLength);this.asm.set_key(b>>2,c.getUint32(0),c.getUint32(4),c.getUint32(8),c.getUint32(12),b>16?c.getUint32(16):0,b>16?c.getUint32(20):0,b>24?c.getUint32(24):0,b>24?c.getUint32(28):0),this.key=a}else if(!this.key)throw new Error("key is required")}function v(a){if(void 0!==a){if(o(a)||p(a))a=new Uint8Array(a);else{if(!n(a))throw new TypeError("unexpected iv type");a=f(a)}if(16!==a.length)throw new d("illegal iv size");var b=new DataView(a.buffer,a.byteOffset,a.byteLength);this.iv=a,this.asm.set_iv(b.getUint32(0),b.getUint32(4),b.getUint32(8),b.getUint32(12))}else this.iv=null,this.asm.set_iv(0,0,0,0)}function w(a){void 0!==a?this.padding=!!a:this.padding=!0}function x(a){return a=a||{},this.result=null,this.pos=0,this.len=0,u.call(this,a.key),this.hasOwnProperty("iv")&&v.call(this,a.iv),this.hasOwnProperty("padding")&&w.call(this,a.padding),this}function y(a){if(n(a)&&(a=f(a)),o(a)&&(a=new Uint8Array(a)),!p(a))throw new TypeError("data isn't of expected type");for(var b=this.asm,c=this.heap,d=Xb.ENC[this.mode],e=Xb.HEAP_DATA,g=this.pos,h=this.len,i=0,j=a.length||0,k=0,l=h+j&-16,m=0,q=new Uint8Array(l);j>0;)m=s(c,g+h,a,i,j),h+=m,i+=m,j-=m,m=b.cipher(d,e+g,h),m&&q.set(c.subarray(g,g+m),k),k+=m,h>m?(g+=m,h-=m):(g=0,h=0);return this.result=q,this.pos=g,this.len=h,this}function z(a){var b=null,c=0;void 0!==a&&(b=y.call(this,a).result,c=b.length);var e=this.asm,f=this.heap,g=Xb.ENC[this.mode],h=Xb.HEAP_DATA,i=this.pos,j=this.len,k=16-j%16,l=j;if(this.hasOwnProperty("padding")){if(this.padding){for(var m=0;k>m;++m)f[i+j+m]=k;j+=k,l=j}else if(j%16)throw new d("data length must be a multiple of the block size")}else j+=k;var n=new Uint8Array(c+l);return c&&n.set(b),j&&e.cipher(g,h+i,j),l&&n.set(f.subarray(i,i+l),c),this.result=n,this.pos=0,this.len=0,this}function A(a){if(n(a)&&(a=f(a)),o(a)&&(a=new Uint8Array(a)),!p(a))throw new TypeError("data isn't of expected type");var b=this.asm,c=this.heap,d=Xb.DEC[this.mode],e=Xb.HEAP_DATA,g=this.pos,h=this.len,i=0,j=a.length||0,k=0,l=h+j&-16,m=0,q=0;this.hasOwnProperty("padding")&&this.padding&&(m=h+j-l||16,l-=m);for(var r=new Uint8Array(l);j>0;)q=s(c,g+h,a,i,j),h+=q,i+=q,j-=q,q=b.cipher(d,e+g,h-(j?0:m)),q&&r.set(c.subarray(g,g+q),k),k+=q,h>q?(g+=q,h-=q):(g=0,h=0);return this.result=r,this.pos=g,this.len=h,this}function B(a){var b=null,c=0;void 0!==a&&(b=A.call(this,a).result,c=b.length);var f=this.asm,g=this.heap,h=Xb.DEC[this.mode],i=Xb.HEAP_DATA,j=this.pos,k=this.len,l=k;if(k>0){if(k%16){if(this.hasOwnProperty("padding"))throw new d("data length must be a multiple of the block size");k+=16-k%16}if(f.cipher(h,i+j,k),this.hasOwnProperty("padding")&&this.padding){var m=g[j+l-1];if(1>m||m>16||m>l)throw new e("bad padding");for(var n=0,o=m;o>1;o--)n|=m^g[j+l-o];if(n)throw new e("bad padding");l-=m}}var p=new Uint8Array(c+l);return c>0&&p.set(b),l>0&&p.set(g.subarray(j,j+l),c),this.result=p,this.pos=0,this.len=0,this}function C(a){this.padding=!0,this.iv=null,t.call(this,a),this.mode="CBC"}function D(a){C.call(this,a)}function E(a){C.call(this,a)}function F(a){this.nonce=null,this.counter=0,this.counterSize=0,t.call(this,a),this.mode="CTR"}function G(a){F.call(this,a)}function H(a,b,c){if(void 0!==c){if(8>c||c>48)throw new d("illegal counter size");this.counterSize=c;var e=Math.pow(2,c)-1;this.asm.set_mask(0,0,e/4294967296|0,0|e)}else this.counterSize=c=48,this.asm.set_mask(0,0,65535,4294967295);if(void 0===a)throw new Error("nonce is required");if(o(a)||p(a))a=new Uint8Array(a);else{if(!n(a))throw new TypeError("unexpected nonce type");a=f(a)}var g=a.length;if(!g||g>16)throw new d("illegal nonce size");this.nonce=a;var h=new DataView(new ArrayBuffer(16));if(new Uint8Array(h.buffer).set(a),this.asm.set_nonce(h.getUint32(0),h.getUint32(4),h.getUint32(8),h.getUint32(12)),void 0!==b){if(!m(b))throw new TypeError("unexpected counter type");if(0>b||b>=Math.pow(2,c))throw new d("illegal counter value");this.counter=b,this.asm.set_counter(0,0,b/4294967296|0,0|b)}else this.counter=b=0}function I(a){return a=a||{},x.call(this,a),H.call(this,a.nonce,a.counter,a.counterSize),this}function J(a){for(var b=this.heap,c=this.asm,d=0,e=a.length||0,f=0;e>0;){for(f=s(b,0,a,d,e),d+=f,e-=f;15&f;)b[f++]=0;c.mac(Xb.MAC.GCM,Xb.HEAP_DATA,f)}}function K(a){this.nonce=null,this.adata=null,this.iv=null,this.counter=1,this.tagSize=16,t.call(this,a),this.mode="GCM"}function L(a){K.call(this,a)}function M(a){K.call(this,a)}function N(a){a=a||{},x.call(this,a);var b=this.asm,c=this.heap;b.gcm_init();var e=a.tagSize;if(void 0!==e){if(!m(e))throw new TypeError("tagSize must be a number");if(4>e||e>16)throw new d("illegal tagSize value");this.tagSize=e}else this.tagSize=16;var g=a.nonce;if(void 0===g)throw new Error("nonce is required");if(p(g)||o(g))g=new Uint8Array(g);else{if(!n(g))throw new TypeError("unexpected nonce type");g=f(g)}this.nonce=g;var h=g.length||0,i=new Uint8Array(16);12!==h?(J.call(this,g),c[0]=c[1]=c[2]=c[3]=c[4]=c[5]=c[6]=c[7]=c[8]=c[9]=c[10]=0,c[11]=h>>>29,c[12]=h>>>21&255,c[13]=h>>>13&255,c[14]=h>>>5&255,c[15]=h<<3&255,b.mac(Xb.MAC.GCM,Xb.HEAP_DATA,16),b.get_iv(Xb.HEAP_DATA),b.set_iv(),i.set(c.subarray(0,16))):(i.set(g),i[15]=1);var j=new DataView(i.buffer);this.gamma0=j.getUint32(12),b.set_nonce(j.getUint32(0),j.getUint32(4),j.getUint32(8),0),b.set_mask(0,0,0,4294967295);var k=a.adata;if(void 0!==k&&null!==k){if(p(k)||o(k))k=new Uint8Array(k);else{if(!n(k))throw new TypeError("unexpected adata type");k=f(k)}if(k.length>bc)throw new d("illegal adata length");k.length?(this.adata=k,J.call(this,k)):this.adata=null}else this.adata=null;var l=a.counter;if(void 0!==l){if(!m(l))throw new TypeError("counter must be a number");if(1>l||l>4294967295)throw new RangeError("counter must be a positive 32-bit integer");this.counter=l,b.set_counter(0,0,0,this.gamma0+l|0)}else this.counter=1,b.set_counter(0,0,0,this.gamma0+1|0);var q=a.iv;if(void 0!==q){if(!m(l))throw new TypeError("counter must be a number");this.iv=q,v.call(this,q)}return this}function O(a){if(n(a)&&(a=f(a)),o(a)&&(a=new Uint8Array(a)),!p(a))throw new TypeError("data isn't of expected type");var b=0,c=a.length||0,d=this.asm,e=this.heap,g=this.counter,h=this.pos,i=this.len,j=0,k=i+c&-16,l=0;if((g-1<<4)+i+c>bc)throw new RangeError("counter overflow");for(var m=new Uint8Array(k);c>0;)l=s(e,h+i,a,b,c),i+=l,b+=l,c-=l,l=d.cipher(Xb.ENC.CTR,Xb.HEAP_DATA+h,i),l=d.mac(Xb.MAC.GCM,Xb.HEAP_DATA+h,l),l&&m.set(e.subarray(h,h+l),j),g+=l>>>4,j+=l,i>l?(h+=l,i-=l):(h=0,i=0);return this.result=m,this.counter=g,this.pos=h,this.len=i,this}function P(){var a=this.asm,b=this.heap,c=this.counter,d=this.tagSize,e=this.adata,f=this.pos,g=this.len,h=new Uint8Array(g+d);a.cipher(Xb.ENC.CTR,Xb.HEAP_DATA+f,g+15&-16),g&&h.set(b.subarray(f,f+g));for(var i=g;15&i;i++)b[f+i]=0;a.mac(Xb.MAC.GCM,Xb.HEAP_DATA+f,i);var j=null!==e?e.length:0,k=(c-1<<4)+g;return b[0]=b[1]=b[2]=0,b[3]=j>>>29,b[4]=j>>>21,b[5]=j>>>13&255,b[6]=j>>>5&255,b[7]=j<<3&255,b[8]=b[9]=b[10]=0,b[11]=k>>>29,b[12]=k>>>21&255,b[13]=k>>>13&255,b[14]=k>>>5&255,b[15]=k<<3&255,a.mac(Xb.MAC.GCM,Xb.HEAP_DATA,16),a.get_iv(Xb.HEAP_DATA),a.set_counter(0,0,0,this.gamma0),a.cipher(Xb.ENC.CTR,Xb.HEAP_DATA,16),h.set(b.subarray(0,d),g),this.result=h,this.counter=1,this.pos=0,this.len=0,this}function Q(a){var b=O.call(this,a).result,c=P.call(this).result,d=new Uint8Array(b.length+c.length);return b.length&&d.set(b),c.length&&d.set(c,b.length),this.result=d,this}function R(a){if(n(a)&&(a=f(a)),o(a)&&(a=new Uint8Array(a)),!p(a))throw new TypeError("data isn't of expected type");var b=0,c=a.length||0,d=this.asm,e=this.heap,g=this.counter,h=this.tagSize,i=this.pos,j=this.len,k=0,l=j+c>h?j+c-h&-16:0,m=j+c-l,q=0;if((g-1<<4)+j+c>bc)throw new RangeError("counter overflow");for(var r=new Uint8Array(l);c>m;)q=s(e,i+j,a,b,c-m),j+=q,b+=q,c-=q,q=d.mac(Xb.MAC.GCM,Xb.HEAP_DATA+i,q),q=d.cipher(Xb.DEC.CTR,Xb.HEAP_DATA+i,q),q&&r.set(e.subarray(i,i+q),k),g+=q>>>4,k+=q,i=0,j=0;return c>0&&(j+=s(e,0,a,b,c)),this.result=r,this.counter=g,this.pos=i,this.len=j,this}function S(){var a=this.asm,b=this.heap,d=this.tagSize,f=this.adata,g=this.counter,h=this.pos,i=this.len,j=i-d,k=0;if(d>i)throw new c("authentication tag not found");for(var l=new Uint8Array(j),m=new Uint8Array(b.subarray(h+j,h+i)),n=j;15&n;n++)b[h+n]=0;k=a.mac(Xb.MAC.GCM,Xb.HEAP_DATA+h,n),k=a.cipher(Xb.DEC.CTR,Xb.HEAP_DATA+h,n),j&&l.set(b.subarray(h,h+j));var o=null!==f?f.length:0,p=(g-1<<4)+i-d;b[0]=b[1]=b[2]=0,b[3]=o>>>29,b[4]=o>>>21,b[5]=o>>>13&255,b[6]=o>>>5&255,b[7]=o<<3&255,b[8]=b[9]=b[10]=0,b[11]=p>>>29,b[12]=p>>>21&255,b[13]=p>>>13&255,b[14]=p>>>5&255,b[15]=p<<3&255,a.mac(Xb.MAC.GCM,Xb.HEAP_DATA,16),a.get_iv(Xb.HEAP_DATA),a.set_counter(0,0,0,this.gamma0),a.cipher(Xb.ENC.CTR,Xb.HEAP_DATA,16);for(var q=0,n=0;d>n;++n)q|=m[n]^b[n];if(q)throw new e("data integrity check failed");return this.result=l,this.counter=1,this.pos=0,this.len=0,this}function T(a){var b=R.call(this,a).result,c=S.call(this).result,d=new Uint8Array(b.length+c.length);return b.length&&d.set(b),c.length&&d.set(c,b.length),this.result=d,this}function U(a,b,c,d){if(void 0===a)throw new SyntaxError("data required");if(void 0===b)throw new SyntaxError("key required");return new C({heap:fc,asm:gc,key:b,padding:c,iv:d}).encrypt(a).result}function V(a,b,c,d){if(void 0===a)throw new SyntaxError("data required");if(void 0===b)throw new SyntaxError("key required");return new C({heap:fc,asm:gc,key:b,padding:c,iv:d}).decrypt(a).result}function W(a,b,c,d,e){if(void 0===a)throw new SyntaxError("data required");if(void 0===b)throw new SyntaxError("key required");if(void 0===c)throw new SyntaxError("nonce required");return new K({heap:fc,asm:gc,key:b,nonce:c,adata:d,tagSize:e}).encrypt(a).result}function X(a,b,c,d,e){if(void 0===a)throw new SyntaxError("data required");if(void 0===b)throw new SyntaxError("key required");if(void 0===c)throw new SyntaxError("nonce required");return new K({heap:fc,asm:gc,key:b,nonce:c,adata:d,tagSize:e}).decrypt(a).result}function Y(){return this.result=null,this.pos=0,this.len=0,this.asm.reset(),this}function Z(a){if(null!==this.result)throw new c("state must be reset before processing new data");if(n(a)&&(a=f(a)),o(a)&&(a=new Uint8Array(a)),!p(a))throw new TypeError("data isn't of expected type");for(var b=this.asm,d=this.heap,e=this.pos,g=this.len,h=0,i=a.length,j=0;i>0;)j=s(d,e+g,a,h,i),g+=j,h+=j,i-=j,j=b.process(e,g),e+=j,g-=j,g||(e=0);return this.pos=e,this.len=g,this}function $(){if(null!==this.result)throw new c("state must be reset before processing new data");return this.asm.finish(this.pos,this.len,0),this.result=new Uint8Array(this.HASH_SIZE),this.result.set(this.heap.subarray(0,this.HASH_SIZE)),this.pos=0,this.len=0,this}function _(a,b,c){"use asm";var d=0,e=0,f=0,g=0,h=0,i=0,j=0;var k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0;var u=new a.Uint8Array(c);function v(a,b,c,i,j,k,l,m,n,o,p,q,r,s,t,u){a=a|0;b=b|0;c=c|0;i=i|0;j=j|0;k=k|0;l=l|0;m=m|0;n=n|0;o=o|0;p=p|0;q=q|0;r=r|0;s=s|0;t=t|0;u=u|0;var v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0,M=0,N=0,O=0,P=0,Q=0,R=0,S=0,T=0,U=0,V=0,W=0,X=0,Y=0,Z=0,$=0,_=0,aa=0,ba=0,ca=0,da=0,ea=0,fa=0,ga=0,ha=0,ia=0,ja=0,ka=0,la=0,ma=0,na=0,oa=0,pa=0,qa=0,ra=0,sa=0,ta=0,ua=0,va=0,wa=0,xa=0,ya=0,za=0,Aa=0,Ba=0,Ca=0,Da=0,Ea=0,Fa=0,Ga=0,Ha=0,Ia=0,Ja=0,Ka=0,La=0;v=d;w=e;x=f;y=g;z=h;B=a+(v<<5|v>>>27)+z+(w&x|~w&y)+1518500249|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;B=b+(v<<5|v>>>27)+z+(w&x|~w&y)+1518500249|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;B=c+(v<<5|v>>>27)+z+(w&x|~w&y)+1518500249|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;B=i+(v<<5|v>>>27)+z+(w&x|~w&y)+1518500249|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;B=j+(v<<5|v>>>27)+z+(w&x|~w&y)+1518500249|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;B=k+(v<<5|v>>>27)+z+(w&x|~w&y)+1518500249|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;B=l+(v<<5|v>>>27)+z+(w&x|~w&y)+1518500249|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;B=m+(v<<5|v>>>27)+z+(w&x|~w&y)+1518500249|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;B=n+(v<<5|v>>>27)+z+(w&x|~w&y)+1518500249|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;B=o+(v<<5|v>>>27)+z+(w&x|~w&y)+1518500249|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;B=p+(v<<5|v>>>27)+z+(w&x|~w&y)+1518500249|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;B=q+(v<<5|v>>>27)+z+(w&x|~w&y)+1518500249|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;B=r+(v<<5|v>>>27)+z+(w&x|~w&y)+1518500249|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;B=s+(v<<5|v>>>27)+z+(w&x|~w&y)+1518500249|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;B=t+(v<<5|v>>>27)+z+(w&x|~w&y)+1518500249|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;B=u+(v<<5|v>>>27)+z+(w&x|~w&y)+1518500249|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=s^n^c^a;C=A<<1|A>>>31;B=C+(v<<5|v>>>27)+z+(w&x|~w&y)+1518500249|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=t^o^i^b;D=A<<1|A>>>31;B=D+(v<<5|v>>>27)+z+(w&x|~w&y)+1518500249|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=u^p^j^c;E=A<<1|A>>>31;B=E+(v<<5|v>>>27)+z+(w&x|~w&y)+1518500249|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=C^q^k^i;F=A<<1|A>>>31;B=F+(v<<5|v>>>27)+z+(w&x|~w&y)+1518500249|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=D^r^l^j;G=A<<1|A>>>31;B=G+(v<<5|v>>>27)+z+(w^x^y)+1859775393|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=E^s^m^k;H=A<<1|A>>>31;B=H+(v<<5|v>>>27)+z+(w^x^y)+1859775393|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=F^t^n^l;I=A<<1|A>>>31;B=I+(v<<5|v>>>27)+z+(w^x^y)+1859775393|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=G^u^o^m;J=A<<1|A>>>31;B=J+(v<<5|v>>>27)+z+(w^x^y)+1859775393|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=H^C^p^n;K=A<<1|A>>>31;B=K+(v<<5|v>>>27)+z+(w^x^y)+1859775393|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=I^D^q^o;L=A<<1|A>>>31;B=L+(v<<5|v>>>27)+z+(w^x^y)+1859775393|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=J^E^r^p;M=A<<1|A>>>31;B=M+(v<<5|v>>>27)+z+(w^x^y)+1859775393|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=K^F^s^q;N=A<<1|A>>>31;B=N+(v<<5|v>>>27)+z+(w^x^y)+1859775393|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=L^G^t^r;O=A<<1|A>>>31;B=O+(v<<5|v>>>27)+z+(w^x^y)+1859775393|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=M^H^u^s;P=A<<1|A>>>31;B=P+(v<<5|v>>>27)+z+(w^x^y)+1859775393|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=N^I^C^t;Q=A<<1|A>>>31;B=Q+(v<<5|v>>>27)+z+(w^x^y)+1859775393|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=O^J^D^u;R=A<<1|A>>>31;B=R+(v<<5|v>>>27)+z+(w^x^y)+1859775393|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=P^K^E^C;S=A<<1|A>>>31;B=S+(v<<5|v>>>27)+z+(w^x^y)+1859775393|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=Q^L^F^D;T=A<<1|A>>>31;B=T+(v<<5|v>>>27)+z+(w^x^y)+1859775393|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=R^M^G^E;U=A<<1|A>>>31;B=U+(v<<5|v>>>27)+z+(w^x^y)+1859775393|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=S^N^H^F;V=A<<1|A>>>31;B=V+(v<<5|v>>>27)+z+(w^x^y)+1859775393|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=T^O^I^G;W=A<<1|A>>>31;B=W+(v<<5|v>>>27)+z+(w^x^y)+1859775393|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=U^P^J^H;X=A<<1|A>>>31;B=X+(v<<5|v>>>27)+z+(w^x^y)+1859775393|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=V^Q^K^I;Y=A<<1|A>>>31;B=Y+(v<<5|v>>>27)+z+(w^x^y)+1859775393|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=W^R^L^J;Z=A<<1|A>>>31;B=Z+(v<<5|v>>>27)+z+(w^x^y)+1859775393|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=X^S^M^K;$=A<<1|A>>>31;B=$+(v<<5|v>>>27)+z+(w&x|w&y|x&y)-1894007588|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=Y^T^N^L;_=A<<1|A>>>31;B=_+(v<<5|v>>>27)+z+(w&x|w&y|x&y)-1894007588|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=Z^U^O^M;aa=A<<1|A>>>31;B=aa+(v<<5|v>>>27)+z+(w&x|w&y|x&y)-1894007588|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=$^V^P^N;ba=A<<1|A>>>31;B=ba+(v<<5|v>>>27)+z+(w&x|w&y|x&y)-1894007588|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=_^W^Q^O;ca=A<<1|A>>>31;B=ca+(v<<5|v>>>27)+z+(w&x|w&y|x&y)-1894007588|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=aa^X^R^P;da=A<<1|A>>>31;B=da+(v<<5|v>>>27)+z+(w&x|w&y|x&y)-1894007588|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=ba^Y^S^Q;ea=A<<1|A>>>31;B=ea+(v<<5|v>>>27)+z+(w&x|w&y|x&y)-1894007588|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=ca^Z^T^R;fa=A<<1|A>>>31;B=fa+(v<<5|v>>>27)+z+(w&x|w&y|x&y)-1894007588|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=da^$^U^S;ga=A<<1|A>>>31;B=ga+(v<<5|v>>>27)+z+(w&x|w&y|x&y)-1894007588|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=ea^_^V^T;ha=A<<1|A>>>31;B=ha+(v<<5|v>>>27)+z+(w&x|w&y|x&y)-1894007588|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=fa^aa^W^U;ia=A<<1|A>>>31;B=ia+(v<<5|v>>>27)+z+(w&x|w&y|x&y)-1894007588|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=ga^ba^X^V;ja=A<<1|A>>>31;B=ja+(v<<5|v>>>27)+z+(w&x|w&y|x&y)-1894007588|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=ha^ca^Y^W;ka=A<<1|A>>>31;B=ka+(v<<5|v>>>27)+z+(w&x|w&y|x&y)-1894007588|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=ia^da^Z^X;la=A<<1|A>>>31;B=la+(v<<5|v>>>27)+z+(w&x|w&y|x&y)-1894007588|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=ja^ea^$^Y;ma=A<<1|A>>>31;B=ma+(v<<5|v>>>27)+z+(w&x|w&y|x&y)-1894007588|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=ka^fa^_^Z;na=A<<1|A>>>31;B=na+(v<<5|v>>>27)+z+(w&x|w&y|x&y)-1894007588|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=la^ga^aa^$;oa=A<<1|A>>>31;B=oa+(v<<5|v>>>27)+z+(w&x|w&y|x&y)-1894007588|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=ma^ha^ba^_;pa=A<<1|A>>>31;B=pa+(v<<5|v>>>27)+z+(w&x|w&y|x&y)-1894007588|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=na^ia^ca^aa;qa=A<<1|A>>>31;B=qa+(v<<5|v>>>27)+z+(w&x|w&y|x&y)-1894007588|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=oa^ja^da^ba;ra=A<<1|A>>>31;B=ra+(v<<5|v>>>27)+z+(w&x|w&y|x&y)-1894007588|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=pa^ka^ea^ca;sa=A<<1|A>>>31;B=sa+(v<<5|v>>>27)+z+(w^x^y)-899497514|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=qa^la^fa^da;ta=A<<1|A>>>31;B=ta+(v<<5|v>>>27)+z+(w^x^y)-899497514|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=ra^ma^ga^ea;ua=A<<1|A>>>31;B=ua+(v<<5|v>>>27)+z+(w^x^y)-899497514|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=sa^na^ha^fa;va=A<<1|A>>>31;B=va+(v<<5|v>>>27)+z+(w^x^y)-899497514|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=ta^oa^ia^ga;wa=A<<1|A>>>31;B=wa+(v<<5|v>>>27)+z+(w^x^y)-899497514|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=ua^pa^ja^ha;xa=A<<1|A>>>31;B=xa+(v<<5|v>>>27)+z+(w^x^y)-899497514|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=va^qa^ka^ia;ya=A<<1|A>>>31;B=ya+(v<<5|v>>>27)+z+(w^x^y)-899497514|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=wa^ra^la^ja;za=A<<1|A>>>31;B=za+(v<<5|v>>>27)+z+(w^x^y)-899497514|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=xa^sa^ma^ka;Aa=A<<1|A>>>31;B=Aa+(v<<5|v>>>27)+z+(w^x^y)-899497514|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=ya^ta^na^la;Ba=A<<1|A>>>31;B=Ba+(v<<5|v>>>27)+z+(w^x^y)-899497514|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=za^ua^oa^ma;Ca=A<<1|A>>>31;B=Ca+(v<<5|v>>>27)+z+(w^x^y)-899497514|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=Aa^va^pa^na;Da=A<<1|A>>>31;B=Da+(v<<5|v>>>27)+z+(w^x^y)-899497514|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=Ba^wa^qa^oa;Ea=A<<1|A>>>31;B=Ea+(v<<5|v>>>27)+z+(w^x^y)-899497514|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=Ca^xa^ra^pa;Fa=A<<1|A>>>31;B=Fa+(v<<5|v>>>27)+z+(w^x^y)-899497514|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=Da^ya^sa^qa;Ga=A<<1|A>>>31;B=Ga+(v<<5|v>>>27)+z+(w^x^y)-899497514|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=Ea^za^ta^ra;Ha=A<<1|A>>>31;B=Ha+(v<<5|v>>>27)+z+(w^x^y)-899497514|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=Fa^Aa^ua^sa;Ia=A<<1|A>>>31;B=Ia+(v<<5|v>>>27)+z+(w^x^y)-899497514|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=Ga^Ba^va^ta;Ja=A<<1|A>>>31;B=Ja+(v<<5|v>>>27)+z+(w^x^y)-899497514|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=Ha^Ca^wa^ua;Ka=A<<1|A>>>31;B=Ka+(v<<5|v>>>27)+z+(w^x^y)-899497514|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;A=Ia^Da^xa^va;La=A<<1|A>>>31;B=La+(v<<5|v>>>27)+z+(w^x^y)-899497514|0;z=y;y=x;x=w<<30|w>>>2;w=v;v=B;d=d+v|0;e=e+w|0;f=f+x|0;g=g+y|0;h=h+z|0}function w(a){a=a|0;v(u[a|0]<<24|u[a|1]<<16|u[a|2]<<8|u[a|3],u[a|4]<<24|u[a|5]<<16|u[a|6]<<8|u[a|7],u[a|8]<<24|u[a|9]<<16|u[a|10]<<8|u[a|11],u[a|12]<<24|u[a|13]<<16|u[a|14]<<8|u[a|15],u[a|16]<<24|u[a|17]<<16|u[a|18]<<8|u[a|19],u[a|20]<<24|u[a|21]<<16|u[a|22]<<8|u[a|23],u[a|24]<<24|u[a|25]<<16|u[a|26]<<8|u[a|27],u[a|28]<<24|u[a|29]<<16|u[a|30]<<8|u[a|31],u[a|32]<<24|u[a|33]<<16|u[a|34]<<8|u[a|35],u[a|36]<<24|u[a|37]<<16|u[a|38]<<8|u[a|39],u[a|40]<<24|u[a|41]<<16|u[a|42]<<8|u[a|43],u[a|44]<<24|u[a|45]<<16|u[a|46]<<8|u[a|47],u[a|48]<<24|u[a|49]<<16|u[a|50]<<8|u[a|51],u[a|52]<<24|u[a|53]<<16|u[a|54]<<8|u[a|55],u[a|56]<<24|u[a|57]<<16|u[a|58]<<8|u[a|59],u[a|60]<<24|u[a|61]<<16|u[a|62]<<8|u[a|63])}function x(a){a=a|0;u[a|0]=d>>>24;u[a|1]=d>>>16&255;u[a|2]=d>>>8&255;u[a|3]=d&255;u[a|4]=e>>>24;u[a|5]=e>>>16&255;u[a|6]=e>>>8&255;u[a|7]=e&255;u[a|8]=f>>>24;u[a|9]=f>>>16&255;u[a|10]=f>>>8&255;u[a|11]=f&255;u[a|12]=g>>>24;u[a|13]=g>>>16&255;u[a|14]=g>>>8&255;u[a|15]=g&255;u[a|16]=h>>>24;u[a|17]=h>>>16&255;u[a|18]=h>>>8&255;u[a|19]=h&255}function y(){d=1732584193;e=4023233417;f=2562383102;g=271733878;h=3285377520;i=j=0}function z(a,b,c,k,l,m,n){a=a|0;b=b|0;c=c|0;k=k|0;l=l|0;m=m|0;n=n|0;d=a;e=b;f=c;g=k;h=l;i=m;j=n}function A(a,b){a=a|0;b=b|0;var c=0;if(a&63)return-1;while((b|0)>=64){w(a);a=a+64|0;b=b-64|0;c=c+64|0}i=i+c|0;if(i>>>0<c>>>0)j=j+1|0;return c|0}function B(a,b,c){a=a|0;b=b|0;c=c|0;var d=0,e=0;if(a&63)return-1;if(~c)if(c&31)return-1;if((b|0)>=64){d=A(a,b)|0;if((d|0)==-1)return-1;a=a+d|0;b=b-d|0}d=d+b|0;i=i+b|0;if(i>>>0<b>>>0)j=j+1|0;u[a|b]=128;if((b|0)>=56){for(e=b+1|0;(e|0)<64;e=e+1|0)u[a|e]=0;w(a);b=0;u[a|0]=0}for(e=b+1|0;(e|0)<59;e=e+1|0)u[a|e]=0;u[a|56]=j>>>21&255;u[a|57]=j>>>13&255;u[a|58]=j>>>5&255;u[a|59]=j<<3&255|i>>>29;u[a|60]=i>>>21&255;u[a|61]=i>>>13&255;u[a|62]=i>>>5&255;u[a|63]=i<<3&255;w(a);if(~c)x(c);return d|0}function C(){d=k;e=l;f=m;g=n;h=o;i=64;j=0}function D(){d=p;e=q;f=r;g=s;h=t;i=64;j=0}function E(a,b,c,u,w,x,z,A,B,C,D,E,F,G,H,I){a=a|0;b=b|0;c=c|0;u=u|0;w=w|0;x=x|0;z=z|0;A=A|0;B=B|0;C=C|0;D=D|0;E=E|0;F=F|0;G=G|0;H=H|0;I=I|0;y();v(a^1549556828,b^1549556828,c^1549556828,u^1549556828,w^1549556828,x^1549556828,z^1549556828,A^1549556828,B^1549556828,C^1549556828,D^1549556828,E^1549556828,F^1549556828,G^1549556828,H^1549556828,I^1549556828);p=d;q=e;r=f;s=g;t=h;y();v(a^909522486,b^909522486,c^909522486,u^909522486,w^909522486,x^909522486,z^909522486,A^909522486,B^909522486,C^909522486,D^909522486,E^909522486,F^909522486,G^909522486,H^909522486,I^909522486);k=d;l=e;m=f;n=g;o=h;i=64;j=0}function F(a,b,c){a=a|0;b=b|0;c=c|0;var i=0,j=0,k=0,l=0,m=0,n=0;if(a&63)return-1;if(~c)if(c&31)return-1;n=B(a,b,-1)|0;i=d,j=e,k=f,l=g,m=h;D();v(i,j,k,l,m,2147483648,0,0,0,0,0,0,0,0,0,672);if(~c)x(c);return n|0}function G(a,b,c,i,j){a=a|0;b=b|0;c=c|0;i=i|0;j=j|0;var k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0;if(a&63)return-1;if(~j)if(j&31)return-1;u[a+b|0]=c>>>24;u[a+b+1|0]=c>>>16&255;u[a+b+2|0]=c>>>8&255;u[a+b+3|0]=c&255;F(a,b+4|0,-1)|0;k=p=d,l=q=e,m=r=f,n=s=g,o=t=h;i=i-1|0;while((i|0)>0){C();v(p,q,r,s,t,2147483648,0,0,0,0,0,0,0,0,0,672);p=d,q=e,r=f,s=g,t=h;D();v(p,q,r,s,t,2147483648,0,0,0,0,0,0,0,0,0,672);p=d,q=e,r=f,s=g,t=h;k=k^d;l=l^e;m=m^f;n=n^g;o=o^h;i=i-1|0}d=k;e=l;f=m;g=n;h=o;if(~j)x(j);return 0}return{reset:y,init:z,process:A,finish:B,hmac_reset:C,hmac_init:E,hmac_finish:F,pbkdf2_generate_block:G}}function aa(a){a=a||{},this.heap=r(Uint8Array,a),this.asm=a.asm||_(b,null,this.heap.buffer),this.BLOCK_SIZE=hc,this.HASH_SIZE=ic,this.reset()}function ba(){return null===kc&&(kc=new aa({heapSize:1048576})),kc}function ca(a){if(void 0===a)throw new SyntaxError("data required");return ba().reset().process(a).finish().result}function da(a){var b=ca(a);return j(b)}function ea(a){var b=ca(a);return k(b)}function fa(a,b,c){"use asm";var d=0,e=0,f=0,g=0,h=0,i=0,j=0,k=0,l=0,m=0;var n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0;var D=new a.Uint8Array(c);function E(a,b,c,l,m,n,o,p,q,r,s,t,u,v,w,x){a=a|0;b=b|0;c=c|0;l=l|0;m=m|0;n=n|0;o=o|0;p=p|0;q=q|0;r=r|0;s=s|0;t=t|0;u=u|0;v=v|0;w=w|0;x=x|0;var y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0;y=d;z=e;A=f;B=g;C=h;D=i;E=j;F=k;G=a+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+1116352408|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;G=b+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+1899447441|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;G=c+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+3049323471|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;G=l+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+3921009573|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;G=m+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+961987163|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;G=n+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+1508970993|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;G=o+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+2453635748|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;G=p+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+2870763221|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;G=q+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+3624381080|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;G=r+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+310598401|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;G=s+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+607225278|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;G=t+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+1426881987|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;G=u+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+1925078388|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;G=v+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+2162078206|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;G=w+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+2614888103|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;G=x+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+3248222580|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;a=G=(b>>>7^b>>>18^b>>>3^b<<25^b<<14)+(w>>>17^w>>>19^w>>>10^w<<15^w<<13)+a+r|0;G=G+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+3835390401|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;b=G=(c>>>7^c>>>18^c>>>3^c<<25^c<<14)+(x>>>17^x>>>19^x>>>10^x<<15^x<<13)+b+s|0;G=G+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+4022224774|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;c=G=(l>>>7^l>>>18^l>>>3^l<<25^l<<14)+(a>>>17^a>>>19^a>>>10^a<<15^a<<13)+c+t|0;G=G+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+264347078|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;l=G=(m>>>7^m>>>18^m>>>3^m<<25^m<<14)+(b>>>17^b>>>19^b>>>10^b<<15^b<<13)+l+u|0;G=G+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+604807628|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;m=G=(n>>>7^n>>>18^n>>>3^n<<25^n<<14)+(c>>>17^c>>>19^c>>>10^c<<15^c<<13)+m+v|0;G=G+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+770255983|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;n=G=(o>>>7^o>>>18^o>>>3^o<<25^o<<14)+(l>>>17^l>>>19^l>>>10^l<<15^l<<13)+n+w|0;G=G+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+1249150122|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;o=G=(p>>>7^p>>>18^p>>>3^p<<25^p<<14)+(m>>>17^m>>>19^m>>>10^m<<15^m<<13)+o+x|0;G=G+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+1555081692|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;p=G=(q>>>7^q>>>18^q>>>3^q<<25^q<<14)+(n>>>17^n>>>19^n>>>10^n<<15^n<<13)+p+a|0;G=G+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+1996064986|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;q=G=(r>>>7^r>>>18^r>>>3^r<<25^r<<14)+(o>>>17^o>>>19^o>>>10^o<<15^o<<13)+q+b|0;G=G+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+2554220882|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;r=G=(s>>>7^s>>>18^s>>>3^s<<25^s<<14)+(p>>>17^p>>>19^p>>>10^p<<15^p<<13)+r+c|0;G=G+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+2821834349|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;s=G=(t>>>7^t>>>18^t>>>3^t<<25^t<<14)+(q>>>17^q>>>19^q>>>10^q<<15^q<<13)+s+l|0;G=G+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+2952996808|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;t=G=(u>>>7^u>>>18^u>>>3^u<<25^u<<14)+(r>>>17^r>>>19^r>>>10^r<<15^r<<13)+t+m|0;G=G+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+3210313671|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;u=G=(v>>>7^v>>>18^v>>>3^v<<25^v<<14)+(s>>>17^s>>>19^s>>>10^s<<15^s<<13)+u+n|0;G=G+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+3336571891|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;v=G=(w>>>7^w>>>18^w>>>3^w<<25^w<<14)+(t>>>17^t>>>19^t>>>10^t<<15^t<<13)+v+o|0;G=G+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+3584528711|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;w=G=(x>>>7^x>>>18^x>>>3^x<<25^x<<14)+(u>>>17^u>>>19^u>>>10^u<<15^u<<13)+w+p|0;G=G+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+113926993|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;
x=G=(a>>>7^a>>>18^a>>>3^a<<25^a<<14)+(v>>>17^v>>>19^v>>>10^v<<15^v<<13)+x+q|0;G=G+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+338241895|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;a=G=(b>>>7^b>>>18^b>>>3^b<<25^b<<14)+(w>>>17^w>>>19^w>>>10^w<<15^w<<13)+a+r|0;G=G+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+666307205|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;b=G=(c>>>7^c>>>18^c>>>3^c<<25^c<<14)+(x>>>17^x>>>19^x>>>10^x<<15^x<<13)+b+s|0;G=G+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+773529912|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;c=G=(l>>>7^l>>>18^l>>>3^l<<25^l<<14)+(a>>>17^a>>>19^a>>>10^a<<15^a<<13)+c+t|0;G=G+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+1294757372|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;l=G=(m>>>7^m>>>18^m>>>3^m<<25^m<<14)+(b>>>17^b>>>19^b>>>10^b<<15^b<<13)+l+u|0;G=G+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+1396182291|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;m=G=(n>>>7^n>>>18^n>>>3^n<<25^n<<14)+(c>>>17^c>>>19^c>>>10^c<<15^c<<13)+m+v|0;G=G+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+1695183700|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;n=G=(o>>>7^o>>>18^o>>>3^o<<25^o<<14)+(l>>>17^l>>>19^l>>>10^l<<15^l<<13)+n+w|0;G=G+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+1986661051|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;o=G=(p>>>7^p>>>18^p>>>3^p<<25^p<<14)+(m>>>17^m>>>19^m>>>10^m<<15^m<<13)+o+x|0;G=G+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+2177026350|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;p=G=(q>>>7^q>>>18^q>>>3^q<<25^q<<14)+(n>>>17^n>>>19^n>>>10^n<<15^n<<13)+p+a|0;G=G+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+2456956037|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;q=G=(r>>>7^r>>>18^r>>>3^r<<25^r<<14)+(o>>>17^o>>>19^o>>>10^o<<15^o<<13)+q+b|0;G=G+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+2730485921|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;r=G=(s>>>7^s>>>18^s>>>3^s<<25^s<<14)+(p>>>17^p>>>19^p>>>10^p<<15^p<<13)+r+c|0;G=G+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+2820302411|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;s=G=(t>>>7^t>>>18^t>>>3^t<<25^t<<14)+(q>>>17^q>>>19^q>>>10^q<<15^q<<13)+s+l|0;G=G+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+3259730800|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;t=G=(u>>>7^u>>>18^u>>>3^u<<25^u<<14)+(r>>>17^r>>>19^r>>>10^r<<15^r<<13)+t+m|0;G=G+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+3345764771|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;u=G=(v>>>7^v>>>18^v>>>3^v<<25^v<<14)+(s>>>17^s>>>19^s>>>10^s<<15^s<<13)+u+n|0;G=G+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+3516065817|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;v=G=(w>>>7^w>>>18^w>>>3^w<<25^w<<14)+(t>>>17^t>>>19^t>>>10^t<<15^t<<13)+v+o|0;G=G+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+3600352804|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;w=G=(x>>>7^x>>>18^x>>>3^x<<25^x<<14)+(u>>>17^u>>>19^u>>>10^u<<15^u<<13)+w+p|0;G=G+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+4094571909|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;x=G=(a>>>7^a>>>18^a>>>3^a<<25^a<<14)+(v>>>17^v>>>19^v>>>10^v<<15^v<<13)+x+q|0;G=G+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+275423344|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;a=G=(b>>>7^b>>>18^b>>>3^b<<25^b<<14)+(w>>>17^w>>>19^w>>>10^w<<15^w<<13)+a+r|0;G=G+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+430227734|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;b=G=(c>>>7^c>>>18^c>>>3^c<<25^c<<14)+(x>>>17^x>>>19^x>>>10^x<<15^x<<13)+b+s|0;G=G+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+506948616|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;c=G=(l>>>7^l>>>18^l>>>3^l<<25^l<<14)+(a>>>17^a>>>19^a>>>10^a<<15^a<<13)+c+t|0;G=G+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+659060556|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;l=G=(m>>>7^m>>>18^m>>>3^m<<25^m<<14)+(b>>>17^b>>>19^b>>>10^b<<15^b<<13)+l+u|0;G=G+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+883997877|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;m=G=(n>>>7^n>>>18^n>>>3^n<<25^n<<14)+(c>>>17^c>>>19^c>>>10^c<<15^c<<13)+m+v|0;G=G+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+958139571|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;n=G=(o>>>7^o>>>18^o>>>3^o<<25^o<<14)+(l>>>17^l>>>19^l>>>10^l<<15^l<<13)+n+w|0;G=G+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+1322822218|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;o=G=(p>>>7^p>>>18^p>>>3^p<<25^p<<14)+(m>>>17^m>>>19^m>>>10^m<<15^m<<13)+o+x|0;G=G+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+1537002063|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;p=G=(q>>>7^q>>>18^q>>>3^q<<25^q<<14)+(n>>>17^n>>>19^n>>>10^n<<15^n<<13)+p+a|0;G=G+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+1747873779|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;q=G=(r>>>7^r>>>18^r>>>3^r<<25^r<<14)+(o>>>17^o>>>19^o>>>10^o<<15^o<<13)+q+b|0;G=G+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+1955562222|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;r=G=(s>>>7^s>>>18^s>>>3^s<<25^s<<14)+(p>>>17^p>>>19^p>>>10^p<<15^p<<13)+r+c|0;G=G+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+2024104815|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;s=G=(t>>>7^t>>>18^t>>>3^t<<25^t<<14)+(q>>>17^q>>>19^q>>>10^q<<15^q<<13)+s+l|0;G=G+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+2227730452|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;t=G=(u>>>7^u>>>18^u>>>3^u<<25^u<<14)+(r>>>17^r>>>19^r>>>10^r<<15^r<<13)+t+m|0;G=G+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+2361852424|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;u=G=(v>>>7^v>>>18^v>>>3^v<<25^v<<14)+(s>>>17^s>>>19^s>>>10^s<<15^s<<13)+u+n|0;G=G+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+2428436474|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;v=G=(w>>>7^w>>>18^w>>>3^w<<25^w<<14)+(t>>>17^t>>>19^t>>>10^t<<15^t<<13)+v+o|0;G=G+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+2756734187|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;w=G=(x>>>7^x>>>18^x>>>3^x<<25^x<<14)+(u>>>17^u>>>19^u>>>10^u<<15^u<<13)+w+p|0;G=G+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+3204031479|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;x=G=(a>>>7^a>>>18^a>>>3^a<<25^a<<14)+(v>>>17^v>>>19^v>>>10^v<<15^v<<13)+x+q|0;G=G+F+(C>>>6^C>>>11^C>>>25^C<<26^C<<21^C<<7)+(E^C&(D^E))+3329325298|0;F=E;E=D;D=C;C=B+G|0;B=A;A=z;z=y;y=G+(z&A^B&(z^A))+(z>>>2^z>>>13^z>>>22^z<<30^z<<19^z<<10)|0;d=d+y|0;e=e+z|0;f=f+A|0;g=g+B|0;h=h+C|0;i=i+D|0;j=j+E|0;k=k+F|0}function F(a){a=a|0;E(D[a|0]<<24|D[a|1]<<16|D[a|2]<<8|D[a|3],D[a|4]<<24|D[a|5]<<16|D[a|6]<<8|D[a|7],D[a|8]<<24|D[a|9]<<16|D[a|10]<<8|D[a|11],D[a|12]<<24|D[a|13]<<16|D[a|14]<<8|D[a|15],D[a|16]<<24|D[a|17]<<16|D[a|18]<<8|D[a|19],D[a|20]<<24|D[a|21]<<16|D[a|22]<<8|D[a|23],D[a|24]<<24|D[a|25]<<16|D[a|26]<<8|D[a|27],D[a|28]<<24|D[a|29]<<16|D[a|30]<<8|D[a|31],D[a|32]<<24|D[a|33]<<16|D[a|34]<<8|D[a|35],D[a|36]<<24|D[a|37]<<16|D[a|38]<<8|D[a|39],D[a|40]<<24|D[a|41]<<16|D[a|42]<<8|D[a|43],D[a|44]<<24|D[a|45]<<16|D[a|46]<<8|D[a|47],D[a|48]<<24|D[a|49]<<16|D[a|50]<<8|D[a|51],D[a|52]<<24|D[a|53]<<16|D[a|54]<<8|D[a|55],D[a|56]<<24|D[a|57]<<16|D[a|58]<<8|D[a|59],D[a|60]<<24|D[a|61]<<16|D[a|62]<<8|D[a|63])}function G(a){a=a|0;D[a|0]=d>>>24;D[a|1]=d>>>16&255;D[a|2]=d>>>8&255;D[a|3]=d&255;D[a|4]=e>>>24;D[a|5]=e>>>16&255;D[a|6]=e>>>8&255;D[a|7]=e&255;D[a|8]=f>>>24;D[a|9]=f>>>16&255;D[a|10]=f>>>8&255;D[a|11]=f&255;D[a|12]=g>>>24;D[a|13]=g>>>16&255;D[a|14]=g>>>8&255;D[a|15]=g&255;D[a|16]=h>>>24;D[a|17]=h>>>16&255;D[a|18]=h>>>8&255;D[a|19]=h&255;D[a|20]=i>>>24;D[a|21]=i>>>16&255;D[a|22]=i>>>8&255;D[a|23]=i&255;D[a|24]=j>>>24;D[a|25]=j>>>16&255;D[a|26]=j>>>8&255;D[a|27]=j&255;D[a|28]=k>>>24;D[a|29]=k>>>16&255;D[a|30]=k>>>8&255;D[a|31]=k&255}function H(){d=1779033703;e=3144134277;f=1013904242;g=2773480762;h=1359893119;i=2600822924;j=528734635;k=1541459225;l=m=0}function I(a,b,c,n,o,p,q,r,s,t){a=a|0;b=b|0;c=c|0;n=n|0;o=o|0;p=p|0;q=q|0;r=r|0;s=s|0;t=t|0;d=a;e=b;f=c;g=n;h=o;i=p;j=q;k=r;l=s;m=t}function J(a,b){a=a|0;b=b|0;var c=0;if(a&63)return-1;while((b|0)>=64){F(a);a=a+64|0;b=b-64|0;c=c+64|0}l=l+c|0;if(l>>>0<c>>>0)m=m+1|0;return c|0}function K(a,b,c){a=a|0;b=b|0;c=c|0;var d=0,e=0;if(a&63)return-1;if(~c)if(c&31)return-1;if((b|0)>=64){d=J(a,b)|0;if((d|0)==-1)return-1;a=a+d|0;b=b-d|0}d=d+b|0;l=l+b|0;if(l>>>0<b>>>0)m=m+1|0;D[a|b]=128;if((b|0)>=56){for(e=b+1|0;(e|0)<64;e=e+1|0)D[a|e]=0;F(a);b=0;D[a|0]=0}for(e=b+1|0;(e|0)<59;e=e+1|0)D[a|e]=0;D[a|56]=m>>>21&255;D[a|57]=m>>>13&255;D[a|58]=m>>>5&255;D[a|59]=m<<3&255|l>>>29;D[a|60]=l>>>21&255;D[a|61]=l>>>13&255;D[a|62]=l>>>5&255;D[a|63]=l<<3&255;F(a);if(~c)G(c);return d|0}function L(){d=n;e=o;f=p;g=q;h=r;i=s;j=t;k=u;l=64;m=0}function M(){d=v;e=w;f=x;g=y;h=z;i=A;j=B;k=C;l=64;m=0}function N(a,b,c,D,F,G,I,J,K,L,M,N,O,P,Q,R){a=a|0;b=b|0;c=c|0;D=D|0;F=F|0;G=G|0;I=I|0;J=J|0;K=K|0;L=L|0;M=M|0;N=N|0;O=O|0;P=P|0;Q=Q|0;R=R|0;H();E(a^1549556828,b^1549556828,c^1549556828,D^1549556828,F^1549556828,G^1549556828,I^1549556828,J^1549556828,K^1549556828,L^1549556828,M^1549556828,N^1549556828,O^1549556828,P^1549556828,Q^1549556828,R^1549556828);v=d;w=e;x=f;y=g;z=h;A=i;B=j;C=k;H();E(a^909522486,b^909522486,c^909522486,D^909522486,F^909522486,G^909522486,I^909522486,J^909522486,K^909522486,L^909522486,M^909522486,N^909522486,O^909522486,P^909522486,Q^909522486,R^909522486);n=d;o=e;p=f;q=g;r=h;s=i;t=j;u=k;l=64;m=0}function O(a,b,c){a=a|0;b=b|0;c=c|0;var l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0;if(a&63)return-1;if(~c)if(c&31)return-1;t=K(a,b,-1)|0;l=d,m=e,n=f,o=g,p=h,q=i,r=j,s=k;M();E(l,m,n,o,p,q,r,s,2147483648,0,0,0,0,0,0,768);if(~c)G(c);return t|0}function P(a,b,c,l,m){a=a|0;b=b|0;c=c|0;l=l|0;m=m|0;var n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0;if(a&63)return-1;if(~m)if(m&31)return-1;D[a+b|0]=c>>>24;D[a+b+1|0]=c>>>16&255;D[a+b+2|0]=c>>>8&255;D[a+b+3|0]=c&255;O(a,b+4|0,-1)|0;n=v=d,o=w=e,p=x=f,q=y=g,r=z=h,s=A=i,t=B=j,u=C=k;l=l-1|0;while((l|0)>0){L();E(v,w,x,y,z,A,B,C,2147483648,0,0,0,0,0,0,768);v=d,w=e,x=f,y=g,z=h,A=i,B=j,C=k;M();E(v,w,x,y,z,A,B,C,2147483648,0,0,0,0,0,0,768);v=d,w=e,x=f,y=g,z=h,A=i,B=j,C=k;n=n^d;o=o^e;p=p^f;q=q^g;r=r^h;s=s^i;t=t^j;u=u^k;l=l-1|0}d=n;e=o;f=p;g=q;h=r;i=s;j=t;k=u;if(~m)G(m);return 0}return{reset:H,init:I,process:J,finish:K,hmac_reset:L,hmac_init:N,hmac_finish:O,pbkdf2_generate_block:P}}function ga(a){a=a||{},this.heap=r(Uint8Array,a),this.asm=a.asm||fa(b,null,this.heap.buffer),this.BLOCK_SIZE=lc,this.HASH_SIZE=mc,this.reset()}function ha(){return null===oc&&(oc=new ga({heapSize:1048576})),oc}function ia(a){if(void 0===a)throw new SyntaxError("data required");return ha().reset().process(a).finish().result}function ja(a){var b=ia(a);return j(b)}function ka(a){var b=ia(a);return k(b)}function la(a){if(a=a||{},!a.hash)throw new SyntaxError("option 'hash' is required");if(!a.hash.HASH_SIZE)throw new SyntaxError("option 'hash' supplied doesn't seem to be a valid hash function");return this.hash=a.hash,this.BLOCK_SIZE=this.hash.BLOCK_SIZE,this.HMAC_SIZE=this.hash.HASH_SIZE,this.key=null,this.verify=null,this.result=null,(void 0!==a.password||void 0!==a.verify)&&this.reset(a),this}function ma(a,b){if(o(b)&&(b=new Uint8Array(b)),n(b)&&(b=f(b)),!p(b))throw new TypeError("password isn't of expected type");var c=new Uint8Array(a.BLOCK_SIZE);return b.length>a.BLOCK_SIZE?c.set(a.reset().process(b).finish().result):c.set(b),c}function na(a){if(o(a)||p(a))a=new Uint8Array(a);else{if(!n(a))throw new TypeError("verify tag isn't of expected type");a=f(a)}if(a.length!==this.HMAC_SIZE)throw new d("illegal verification tag size");this.verify=a}function oa(a){a=a||{};var b=a.password;if(null===this.key&&!n(b)&&!b)throw new c("no key is associated with the instance");this.result=null,this.hash.reset(),(b||n(b))&&(this.key=ma(this.hash,b));for(var d=new Uint8Array(this.key),e=0;e<d.length;++e)d[e]^=54;this.hash.process(d);var f=a.verify;return void 0!==f?na.call(this,f):this.verify=null,this}function pa(a){if(null===this.key)throw new c("no key is associated with the instance");if(null!==this.result)throw new c("state must be reset before processing new data");return this.hash.process(a),this}function qa(){if(null===this.key)throw new c("no key is associated with the instance");if(null!==this.result)throw new c("state must be reset before processing new data");for(var a=this.hash.finish().result,b=new Uint8Array(this.key),d=0;d<b.length;++d)b[d]^=92;var e=this.verify,f=this.hash.reset().process(b).process(a).finish().result;if(e)if(e.length===f.length){for(var g=0,d=0;d<e.length;d++)g|=e[d]^f[d];this.result=!g}else this.result=!1;else this.result=f;return this}function ra(a){return a=a||{},a.hash instanceof aa||(a.hash=ba()),la.call(this,a),this}function sa(a){a=a||{},this.result=null,this.hash.reset();var b=a.password;if(void 0!==b){n(b)&&(b=f(b));var c=this.key=ma(this.hash,b);this.hash.reset().asm.hmac_init(c[0]<<24|c[1]<<16|c[2]<<8|c[3],c[4]<<24|c[5]<<16|c[6]<<8|c[7],c[8]<<24|c[9]<<16|c[10]<<8|c[11],c[12]<<24|c[13]<<16|c[14]<<8|c[15],c[16]<<24|c[17]<<16|c[18]<<8|c[19],c[20]<<24|c[21]<<16|c[22]<<8|c[23],c[24]<<24|c[25]<<16|c[26]<<8|c[27],c[28]<<24|c[29]<<16|c[30]<<8|c[31],c[32]<<24|c[33]<<16|c[34]<<8|c[35],c[36]<<24|c[37]<<16|c[38]<<8|c[39],c[40]<<24|c[41]<<16|c[42]<<8|c[43],c[44]<<24|c[45]<<16|c[46]<<8|c[47],c[48]<<24|c[49]<<16|c[50]<<8|c[51],c[52]<<24|c[53]<<16|c[54]<<8|c[55],c[56]<<24|c[57]<<16|c[58]<<8|c[59],c[60]<<24|c[61]<<16|c[62]<<8|c[63])}else this.hash.asm.hmac_reset();var d=a.verify;return void 0!==d?na.call(this,d):this.verify=null,this}function ta(){if(null===this.key)throw new c("no key is associated with the instance");if(null!==this.result)throw new c("state must be reset before processing new data");var a=this.hash,b=this.hash.asm,d=this.hash.heap;b.hmac_finish(a.pos,a.len,0);var e=this.verify,f=new Uint8Array(ic);if(f.set(d.subarray(0,ic)),e)if(e.length===f.length){for(var g=0,h=0;h<e.length;h++)g|=e[h]^f[h];this.result=!g}else this.result=!1;else this.result=f;return this}function ua(){return null===rc&&(rc=new ra),rc}function va(a){return a=a||{},a.hash instanceof ga||(a.hash=ha()),la.call(this,a),this}function wa(a){a=a||{},this.result=null,this.hash.reset();var b=a.password;if(void 0!==b){n(b)&&(b=f(b));var c=this.key=ma(this.hash,b);this.hash.reset().asm.hmac_init(c[0]<<24|c[1]<<16|c[2]<<8|c[3],c[4]<<24|c[5]<<16|c[6]<<8|c[7],c[8]<<24|c[9]<<16|c[10]<<8|c[11],c[12]<<24|c[13]<<16|c[14]<<8|c[15],c[16]<<24|c[17]<<16|c[18]<<8|c[19],c[20]<<24|c[21]<<16|c[22]<<8|c[23],c[24]<<24|c[25]<<16|c[26]<<8|c[27],c[28]<<24|c[29]<<16|c[30]<<8|c[31],c[32]<<24|c[33]<<16|c[34]<<8|c[35],c[36]<<24|c[37]<<16|c[38]<<8|c[39],c[40]<<24|c[41]<<16|c[42]<<8|c[43],c[44]<<24|c[45]<<16|c[46]<<8|c[47],c[48]<<24|c[49]<<16|c[50]<<8|c[51],c[52]<<24|c[53]<<16|c[54]<<8|c[55],c[56]<<24|c[57]<<16|c[58]<<8|c[59],c[60]<<24|c[61]<<16|c[62]<<8|c[63])}else this.hash.asm.hmac_reset();var d=a.verify;return void 0!==d?na.call(this,d):this.verify=null,this}function xa(){if(null===this.key)throw new c("no key is associated with the instance");if(null!==this.result)throw new c("state must be reset before processing new data");var a=this.hash,b=this.hash.asm,d=this.hash.heap;b.hmac_finish(a.pos,a.len,0);var e=this.verify,f=new Uint8Array(mc);if(f.set(d.subarray(0,mc)),e)if(e.length===f.length){for(var g=0,h=0;h<e.length;h++)g|=e[h]^f[h];this.result=!g}else this.result=!1;else this.result=f;return this}function ya(){return null===tc&&(tc=new va),tc}function za(a,b){if(void 0===a)throw new SyntaxError("data required");if(void 0===b)throw new SyntaxError("password required");return ua().reset({password:b}).process(a).finish().result}function Aa(a,b){var c=za(a,b);return j(c)}function Ba(a,b){var c=za(a,b);return k(c)}function Ca(a,b){if(void 0===a)throw new SyntaxError("data required");if(void 0===b)throw new SyntaxError("password required");return ya().reset({password:b}).process(a).finish().result}function Da(a,b){var c=Ca(a,b);return j(c)}function Ea(a,b){var c=Ca(a,b);return k(c)}function Fa(a){if(a=a||{},!a.hmac)throw new SyntaxError("option 'hmac' is required");if(!a.hmac.HMAC_SIZE)throw new SyntaxError("option 'hmac' supplied doesn't seem to be a valid HMAC function");this.hmac=a.hmac,this.count=a.count||4096,this.length=a.length||this.hmac.HMAC_SIZE,this.result=null;var b=a.password;return(b||n(b))&&this.reset(a),this}function Ga(a){return this.result=null,this.hmac.reset(a),this}function Ha(a,b,e){if(null!==this.result)throw new c("state must be reset before processing new data");if(!a&&!n(a))throw new d("bad 'salt' value");b=b||this.count,e=e||this.length,this.result=new Uint8Array(e);for(var f=Math.ceil(e/this.hmac.HMAC_SIZE),g=1;f>=g;++g){var h=(g-1)*this.hmac.HMAC_SIZE,i=(f>g?0:e%this.hmac.HMAC_SIZE)||this.hmac.HMAC_SIZE,j=new Uint8Array(this.hmac.reset().process(a).process(new Uint8Array([g>>>24&255,g>>>16&255,g>>>8&255,255&g])).finish().result);this.result.set(j.subarray(0,i),h);for(var k=1;b>k;++k){j=new Uint8Array(this.hmac.reset().process(j).finish().result);for(var l=0;i>l;++l)this.result[h+l]^=j[l]}}return this}function Ia(a){return a=a||{},a.hmac instanceof ra||(a.hmac=ua()),Fa.call(this,a),this}function Ja(a,b,e){if(null!==this.result)throw new c("state must be reset before processing new data");if(!a&&!n(a))throw new d("bad 'salt' value");b=b||this.count,e=e||this.length,this.result=new Uint8Array(e);for(var f=Math.ceil(e/this.hmac.HMAC_SIZE),g=1;f>=g;++g){var h=(g-1)*this.hmac.HMAC_SIZE,i=(f>g?0:e%this.hmac.HMAC_SIZE)||this.hmac.HMAC_SIZE;this.hmac.reset().process(a),this.hmac.hash.asm.pbkdf2_generate_block(this.hmac.hash.pos,this.hmac.hash.len,g,b,0),this.result.set(this.hmac.hash.heap.subarray(0,i),h)}return this}function Ka(){return null===wc&&(wc=new Ia),wc}function La(a){return a=a||{},a.hmac instanceof va||(a.hmac=ya()),Fa.call(this,a),this}function Ma(a,b,e){if(null!==this.result)throw new c("state must be reset before processing new data");if(!a&&!n(a))throw new d("bad 'salt' value");b=b||this.count,e=e||this.length,this.result=new Uint8Array(e);for(var f=Math.ceil(e/this.hmac.HMAC_SIZE),g=1;f>=g;++g){var h=(g-1)*this.hmac.HMAC_SIZE,i=(f>g?0:e%this.hmac.HMAC_SIZE)||this.hmac.HMAC_SIZE;this.hmac.reset().process(a),this.hmac.hash.asm.pbkdf2_generate_block(this.hmac.hash.pos,this.hmac.hash.len,g,b,0),this.result.set(this.hmac.hash.heap.subarray(0,i),h)}return this}function Na(){return null===yc&&(yc=new La),yc}function Oa(a,b,c,d){if(void 0===a)throw new SyntaxError("password required");if(void 0===b)throw new SyntaxError("salt required");return Ka().reset({password:a}).generate(b,c,d).result}function Pa(a,b,c,d){var e=Oa(a,b,c,d);return j(e)}function Qa(a,b,c,d){var e=Oa(a,b,c,d);return k(e)}function Ra(a,b,c,d){if(void 0===a)throw new SyntaxError("password required");if(void 0===b)throw new SyntaxError("salt required");return Na().reset({password:a}).generate(b,c,d).result}function Sa(a,b,c,d){var e=Ra(a,b,c,d);return j(e)}function Ta(a,b,c,d){var e=Ra(a,b,c,d);return k(e)}function Ua(){if(void 0!==Ec)d=new Uint8Array(32),zc.call(Ec,d),Hc(d);else{var a,c,d=new Ub(3);d[0]=Cc(),d[1]=Bc(),d[2]=Fc(),d=new Uint8Array(d.buffer);var e=Na();for(a=0;100>a;a++)d=e.reset({password:d}).generate(b.location.href,1e3,32).result,c=Fc(),d[0]^=c>>>24,d[1]^=c>>>16,d[2]^=c>>>8,d[3]^=c;Hc(d)}Ic=0,Jc=!0}function Va(a){if(!o(a)&&!q(a))throw new TypeError("bad seed type");var b=a.byteOffest||0,c=a.byteLength||a.length,d=new Uint8Array(a.buffer||a,b,c);Hc(d),Ic=0;for(var e=0,f=0;f<d.length;f++)e|=d[f],d[f]=0;return 0!==e&&(Lc+=4*c),Kc=Lc>=Mc}function Wa(a){if(Jc||Ua(),!Kc&&void 0===Ec){if(!Nc)throw new e("No strong PRNGs available. Use asmCrypto.random.seed().");void 0!==Vb&&Vb.error("No strong PRNGs available; your security is greatly lowered. Use asmCrypto.random.seed().")}if(!Oc&&!Kc&&void 0!==Ec&&void 0!==Vb){var b=(new Error).stack;Pc[b]|=0,Pc[b]++||Vb.warn("asmCrypto PRNG not seeded; your security relies on your system PRNG. If this is not acceptable, use asmCrypto.random.seed().")}if(!o(a)&&!q(a))throw new TypeError("unexpected buffer type");var c,d,f=a.byteOffset||0,g=a.byteLength||a.length,h=new Uint8Array(a.buffer||a,f,g);for(void 0!==Ec&&zc.call(Ec,h),c=0;g>c;c++)0===(3&c)&&(Ic>=1099511627776&&Ua(),d=Gc(),Ic++),h[c]^=d,d>>>=8;return a}function Xa(){(!Jc||Ic>=1099511627776)&&Ua();var a=(1048576*Gc()+(Gc()>>>12))/4503599627370496;return Ic+=2,a}function Ya(a,b){return a*b|0}function Za(a,b,c){"use asm";var d=0;var e=new a.Uint32Array(c);var f=a.Math.imul;function g(a){a=a|0;d=a=a+31&-32;return a|0}function h(a){a=a|0;var b=0;b=d;d=b+(a+31&-32)|0;return b|0}function i(a){a=a|0;d=d-(a+31&-32)|0}function j(a,b,c){a=a|0;b=b|0;c=c|0;var d=0;if((b|0)>(c|0)){for(;(d|0)<(a|0);d=d+4|0){e[c+d>>2]=e[b+d>>2]}}else{for(d=a-4|0;(d|0)>=0;d=d-4|0){e[c+d>>2]=e[b+d>>2]}}}function k(a,b,c){a=a|0;b=b|0;c=c|0;var d=0;for(;(d|0)<(a|0);d=d+4|0){e[c+d>>2]=b}}function l(a,b,c,d){a=a|0;b=b|0;c=c|0;d=d|0;var f=0,g=0,h=0,i=0,j=0;if((d|0)<=0)d=b;if((d|0)<(b|0))b=d;g=1;for(;(j|0)<(b|0);j=j+4|0){f=~e[a+j>>2];h=(f&65535)+g|0;i=(f>>>16)+(h>>>16)|0;e[c+j>>2]=i<<16|h&65535;g=i>>>16}for(;(j|0)<(d|0);j=j+4|0){e[c+j>>2]=g-1|0}return g|0}function m(a,b,c,d){a=a|0;b=b|0;c=c|0;d=d|0;var f=0,g=0,h=0;if((b|0)>(d|0)){for(h=b-4|0;(h|0)>=(d|0);h=h-4|0){if(e[a+h>>2]|0)return 1}}else{for(h=d-4|0;(h|0)>=(b|0);h=h-4|0){if(e[c+h>>2]|0)return-1}}for(;(h|0)>=0;h=h-4|0){f=e[a+h>>2]|0,g=e[c+h>>2]|0;if(f>>>0<g>>>0)return-1;if(f>>>0>g>>>0)return 1}return 0}function n(a,b){a=a|0;b=b|0;var c=0;for(c=b-4|0;(c|0)>=0;c=c-4|0){if(e[a+c>>2]|0)return c+4|0}return 0}function o(a,b,c,d,f,g){a=a|0;b=b|0;c=c|0;d=d|0;f=f|0;g=g|0;var h=0,i=0,j=0,k=0,l=0,m=0;if((b|0)<(d|0)){k=a,a=c,c=k;k=b,b=d,d=k}if((g|0)<=0)g=b+4|0;if((g|0)<(d|0))b=d=g;for(;(m|0)<(d|0);m=m+4|0){h=e[a+m>>2]|0;i=e[c+m>>2]|0;k=((h&65535)+(i&65535)|0)+j|0;l=((h>>>16)+(i>>>16)|0)+(k>>>16)|0;e[f+m>>2]=k&65535|l<<16;j=l>>>16}for(;(m|0)<(b|0);m=m+4|0){h=e[a+m>>2]|0;k=(h&65535)+j|0;l=(h>>>16)+(k>>>16)|0;e[f+m>>2]=k&65535|l<<16;j=l>>>16}for(;(m|0)<(g|0);m=m+4|0){e[f+m>>2]=j|0;j=0}return j|0}function p(a,b,c,d,f,g){a=a|0;b=b|0;c=c|0;d=d|0;f=f|0;g=g|0;var h=0,i=0,j=0,k=0,l=0,m=0;if((g|0)<=0)g=(b|0)>(d|0)?b+4|0:d+4|0;if((g|0)<(b|0))b=g;if((g|0)<(d|0))d=g;if((b|0)<(d|0)){for(;(m|0)<(b|0);m=m+4|0){h=e[a+m>>2]|0;i=e[c+m>>2]|0;k=((h&65535)-(i&65535)|0)+j|0;l=((h>>>16)-(i>>>16)|0)+(k>>16)|0;e[f+m>>2]=k&65535|l<<16;j=l>>16}for(;(m|0)<(d|0);m=m+4|0){i=e[c+m>>2]|0;k=j-(i&65535)|0;l=(k>>16)-(i>>>16)|0;e[f+m>>2]=k&65535|l<<16;j=l>>16}}else{for(;(m|0)<(d|0);m=m+4|0){h=e[a+m>>2]|0;i=e[c+m>>2]|0;k=((h&65535)-(i&65535)|0)+j|0;l=((h>>>16)-(i>>>16)|0)+(k>>16)|0;e[f+m>>2]=k&65535|l<<16;j=l>>16}for(;(m|0)<(b|0);m=m+4|0){h=e[a+m>>2]|0;k=(h&65535)+j|0;l=(h>>>16)+(k>>16)|0;e[f+m>>2]=k&65535|l<<16;j=l>>16}}for(;(m|0)<(g|0);m=m+4|0){e[f+m>>2]=j|0}return j|0}function q(a,b,c,d,g,h){a=a|0;b=b|0;c=c|0;d=d|0;g=g|0;h=h|0;var i=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0,M=0,N=0,O=0,P=0,Q=0,R=0,S=0,T=0,U=0,V=0,W=0,X=0,Y=0,Z=0,$=0,_=0,aa=0,ba=0,ca=0,da=0,ea=0,fa=0,ga=0,ha=0,ia=0,ja=0,ka=0;if((b|0)>(d|0)){ca=a,da=b;a=c,b=d;c=ca,d=da}fa=b+d|0;if((h|0)>(fa|0)|(h|0)<=0)h=fa;if((h|0)<(b|0))b=h;if((h|0)<(d|0))d=h;for(;(ga|0)<(b|0);ga=ga+32|0){ha=a+ga|0;q=e[(ha|0)>>2]|0,r=e[(ha|4)>>2]|0,s=e[(ha|8)>>2]|0,t=e[(ha|12)>>2]|0,u=e[(ha|16)>>2]|0,v=e[(ha|20)>>2]|0,w=e[(ha|24)>>2]|0,x=e[(ha|28)>>2]|0,i=q&65535,j=r&65535,k=s&65535,l=t&65535,m=u&65535,n=v&65535,o=w&65535,p=x&65535,q=q>>>16,r=r>>>16,s=s>>>16,t=t>>>16,u=u>>>16,v=v>>>16,w=w>>>16,x=x>>>16;W=X=Y=Z=$=_=aa=ba=0;for(ia=0;(ia|0)<(d|0);ia=ia+32|0){ja=c+ia|0;ka=g+(ga+ia|0)|0;G=e[(ja|0)>>2]|0,H=e[(ja|4)>>2]|0,I=e[(ja|8)>>2]|0,J=e[(ja|12)>>2]|0,K=e[(ja|16)>>2]|0,L=e[(ja|20)>>2]|0,M=e[(ja|24)>>2]|0,N=e[(ja|28)>>2]|0,y=G&65535,z=H&65535,A=I&65535,B=J&65535,C=K&65535,D=L&65535,E=M&65535,F=N&65535,G=G>>>16,H=H>>>16,I=I>>>16,J=J>>>16,K=K>>>16,L=L>>>16,M=M>>>16,N=N>>>16;O=e[(ka|0)>>2]|0,P=e[(ka|4)>>2]|0,Q=e[(ka|8)>>2]|0,R=e[(ka|12)>>2]|0,S=e[(ka|16)>>2]|0,T=e[(ka|20)>>2]|0,U=e[(ka|24)>>2]|0,V=e[(ka|28)>>2]|0;ca=((f(i,y)|0)+(W&65535)|0)+(O&65535)|0;da=((f(q,y)|0)+(W>>>16)|0)+(O>>>16)|0;ea=((f(i,G)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(q,G)|0)+(da>>>16)|0)+(ea>>>16)|0;O=ea<<16|ca&65535;ca=((f(i,z)|0)+(fa&65535)|0)+(P&65535)|0;da=((f(q,z)|0)+(fa>>>16)|0)+(P>>>16)|0;ea=((f(i,H)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(q,H)|0)+(da>>>16)|0)+(ea>>>16)|0;P=ea<<16|ca&65535;ca=((f(i,A)|0)+(fa&65535)|0)+(Q&65535)|0;da=((f(q,A)|0)+(fa>>>16)|0)+(Q>>>16)|0;ea=((f(i,I)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(q,I)|0)+(da>>>16)|0)+(ea>>>16)|0;Q=ea<<16|ca&65535;ca=((f(i,B)|0)+(fa&65535)|0)+(R&65535)|0;da=((f(q,B)|0)+(fa>>>16)|0)+(R>>>16)|0;ea=((f(i,J)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(q,J)|0)+(da>>>16)|0)+(ea>>>16)|0;R=ea<<16|ca&65535;ca=((f(i,C)|0)+(fa&65535)|0)+(S&65535)|0;da=((f(q,C)|0)+(fa>>>16)|0)+(S>>>16)|0;ea=((f(i,K)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(q,K)|0)+(da>>>16)|0)+(ea>>>16)|0;S=ea<<16|ca&65535;ca=((f(i,D)|0)+(fa&65535)|0)+(T&65535)|0;da=((f(q,D)|0)+(fa>>>16)|0)+(T>>>16)|0;ea=((f(i,L)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(q,L)|0)+(da>>>16)|0)+(ea>>>16)|0;T=ea<<16|ca&65535;ca=((f(i,E)|0)+(fa&65535)|0)+(U&65535)|0;da=((f(q,E)|0)+(fa>>>16)|0)+(U>>>16)|0;ea=((f(i,M)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(q,M)|0)+(da>>>16)|0)+(ea>>>16)|0;U=ea<<16|ca&65535;ca=((f(i,F)|0)+(fa&65535)|0)+(V&65535)|0;da=((f(q,F)|0)+(fa>>>16)|0)+(V>>>16)|0;ea=((f(i,N)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(q,N)|0)+(da>>>16)|0)+(ea>>>16)|0;V=ea<<16|ca&65535;W=fa;ca=((f(j,y)|0)+(X&65535)|0)+(P&65535)|0;da=((f(r,y)|0)+(X>>>16)|0)+(P>>>16)|0;ea=((f(j,G)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(r,G)|0)+(da>>>16)|0)+(ea>>>16)|0;P=ea<<16|ca&65535;ca=((f(j,z)|0)+(fa&65535)|0)+(Q&65535)|0;da=((f(r,z)|0)+(fa>>>16)|0)+(Q>>>16)|0;ea=((f(j,H)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(r,H)|0)+(da>>>16)|0)+(ea>>>16)|0;Q=ea<<16|ca&65535;ca=((f(j,A)|0)+(fa&65535)|0)+(R&65535)|0;da=((f(r,A)|0)+(fa>>>16)|0)+(R>>>16)|0;ea=((f(j,I)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(r,I)|0)+(da>>>16)|0)+(ea>>>16)|0;R=ea<<16|ca&65535;ca=((f(j,B)|0)+(fa&65535)|0)+(S&65535)|0;da=((f(r,B)|0)+(fa>>>16)|0)+(S>>>16)|0;ea=((f(j,J)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(r,J)|0)+(da>>>16)|0)+(ea>>>16)|0;S=ea<<16|ca&65535;ca=((f(j,C)|0)+(fa&65535)|0)+(T&65535)|0;da=((f(r,C)|0)+(fa>>>16)|0)+(T>>>16)|0;ea=((f(j,K)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(r,K)|0)+(da>>>16)|0)+(ea>>>16)|0;T=ea<<16|ca&65535;ca=((f(j,D)|0)+(fa&65535)|0)+(U&65535)|0;da=((f(r,D)|0)+(fa>>>16)|0)+(U>>>16)|0;ea=((f(j,L)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(r,L)|0)+(da>>>16)|0)+(ea>>>16)|0;U=ea<<16|ca&65535;ca=((f(j,E)|0)+(fa&65535)|0)+(V&65535)|0;da=((f(r,E)|0)+(fa>>>16)|0)+(V>>>16)|0;ea=((f(j,M)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(r,M)|0)+(da>>>16)|0)+(ea>>>16)|0;V=ea<<16|ca&65535;ca=((f(j,F)|0)+(fa&65535)|0)+(W&65535)|0;da=((f(r,F)|0)+(fa>>>16)|0)+(W>>>16)|0;ea=((f(j,N)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(r,N)|0)+(da>>>16)|0)+(ea>>>16)|0;W=ea<<16|ca&65535;X=fa;ca=((f(k,y)|0)+(Y&65535)|0)+(Q&65535)|0;da=((f(s,y)|0)+(Y>>>16)|0)+(Q>>>16)|0;ea=((f(k,G)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(s,G)|0)+(da>>>16)|0)+(ea>>>16)|0;Q=ea<<16|ca&65535;ca=((f(k,z)|0)+(fa&65535)|0)+(R&65535)|0;da=((f(s,z)|0)+(fa>>>16)|0)+(R>>>16)|0;ea=((f(k,H)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(s,H)|0)+(da>>>16)|0)+(ea>>>16)|0;R=ea<<16|ca&65535;ca=((f(k,A)|0)+(fa&65535)|0)+(S&65535)|0;da=((f(s,A)|0)+(fa>>>16)|0)+(S>>>16)|0;ea=((f(k,I)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(s,I)|0)+(da>>>16)|0)+(ea>>>16)|0;S=ea<<16|ca&65535;ca=((f(k,B)|0)+(fa&65535)|0)+(T&65535)|0;da=((f(s,B)|0)+(fa>>>16)|0)+(T>>>16)|0;ea=((f(k,J)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(s,J)|0)+(da>>>16)|0)+(ea>>>16)|0;T=ea<<16|ca&65535;ca=((f(k,C)|0)+(fa&65535)|0)+(U&65535)|0;da=((f(s,C)|0)+(fa>>>16)|0)+(U>>>16)|0;ea=((f(k,K)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(s,K)|0)+(da>>>16)|0)+(ea>>>16)|0;U=ea<<16|ca&65535;ca=((f(k,D)|0)+(fa&65535)|0)+(V&65535)|0;da=((f(s,D)|0)+(fa>>>16)|0)+(V>>>16)|0;ea=((f(k,L)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(s,L)|0)+(da>>>16)|0)+(ea>>>16)|0;V=ea<<16|ca&65535;ca=((f(k,E)|0)+(fa&65535)|0)+(W&65535)|0;da=((f(s,E)|0)+(fa>>>16)|0)+(W>>>16)|0;ea=((f(k,M)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(s,M)|0)+(da>>>16)|0)+(ea>>>16)|0;W=ea<<16|ca&65535;ca=((f(k,F)|0)+(fa&65535)|0)+(X&65535)|0;da=((f(s,F)|0)+(fa>>>16)|0)+(X>>>16)|0;ea=((f(k,N)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(s,N)|0)+(da>>>16)|0)+(ea>>>16)|0;X=ea<<16|ca&65535;Y=fa;ca=((f(l,y)|0)+(Z&65535)|0)+(R&65535)|0;da=((f(t,y)|0)+(Z>>>16)|0)+(R>>>16)|0;ea=((f(l,G)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(t,G)|0)+(da>>>16)|0)+(ea>>>16)|0;R=ea<<16|ca&65535;ca=((f(l,z)|0)+(fa&65535)|0)+(S&65535)|0;da=((f(t,z)|0)+(fa>>>16)|0)+(S>>>16)|0;ea=((f(l,H)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(t,H)|0)+(da>>>16)|0)+(ea>>>16)|0;S=ea<<16|ca&65535;ca=((f(l,A)|0)+(fa&65535)|0)+(T&65535)|0;da=((f(t,A)|0)+(fa>>>16)|0)+(T>>>16)|0;ea=((f(l,I)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(t,I)|0)+(da>>>16)|0)+(ea>>>16)|0;T=ea<<16|ca&65535;ca=((f(l,B)|0)+(fa&65535)|0)+(U&65535)|0;da=((f(t,B)|0)+(fa>>>16)|0)+(U>>>16)|0;ea=((f(l,J)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(t,J)|0)+(da>>>16)|0)+(ea>>>16)|0;U=ea<<16|ca&65535;ca=((f(l,C)|0)+(fa&65535)|0)+(V&65535)|0;da=((f(t,C)|0)+(fa>>>16)|0)+(V>>>16)|0;ea=((f(l,K)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(t,K)|0)+(da>>>16)|0)+(ea>>>16)|0;V=ea<<16|ca&65535;ca=((f(l,D)|0)+(fa&65535)|0)+(W&65535)|0;da=((f(t,D)|0)+(fa>>>16)|0)+(W>>>16)|0;ea=((f(l,L)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(t,L)|0)+(da>>>16)|0)+(ea>>>16)|0;W=ea<<16|ca&65535;ca=((f(l,E)|0)+(fa&65535)|0)+(X&65535)|0;da=((f(t,E)|0)+(fa>>>16)|0)+(X>>>16)|0;ea=((f(l,M)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(t,M)|0)+(da>>>16)|0)+(ea>>>16)|0;X=ea<<16|ca&65535;ca=((f(l,F)|0)+(fa&65535)|0)+(Y&65535)|0;da=((f(t,F)|0)+(fa>>>16)|0)+(Y>>>16)|0;ea=((f(l,N)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(t,N)|0)+(da>>>16)|0)+(ea>>>16)|0;Y=ea<<16|ca&65535;Z=fa;ca=((f(m,y)|0)+($&65535)|0)+(S&65535)|0;da=((f(u,y)|0)+($>>>16)|0)+(S>>>16)|0;ea=((f(m,G)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(u,G)|0)+(da>>>16)|0)+(ea>>>16)|0;S=ea<<16|ca&65535;ca=((f(m,z)|0)+(fa&65535)|0)+(T&65535)|0;da=((f(u,z)|0)+(fa>>>16)|0)+(T>>>16)|0;ea=((f(m,H)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(u,H)|0)+(da>>>16)|0)+(ea>>>16)|0;T=ea<<16|ca&65535;ca=((f(m,A)|0)+(fa&65535)|0)+(U&65535)|0;da=((f(u,A)|0)+(fa>>>16)|0)+(U>>>16)|0;ea=((f(m,I)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(u,I)|0)+(da>>>16)|0)+(ea>>>16)|0;U=ea<<16|ca&65535;ca=((f(m,B)|0)+(fa&65535)|0)+(V&65535)|0;da=((f(u,B)|0)+(fa>>>16)|0)+(V>>>16)|0;ea=((f(m,J)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(u,J)|0)+(da>>>16)|0)+(ea>>>16)|0;V=ea<<16|ca&65535;ca=((f(m,C)|0)+(fa&65535)|0)+(W&65535)|0;da=((f(u,C)|0)+(fa>>>16)|0)+(W>>>16)|0;
ea=((f(m,K)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(u,K)|0)+(da>>>16)|0)+(ea>>>16)|0;W=ea<<16|ca&65535;ca=((f(m,D)|0)+(fa&65535)|0)+(X&65535)|0;da=((f(u,D)|0)+(fa>>>16)|0)+(X>>>16)|0;ea=((f(m,L)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(u,L)|0)+(da>>>16)|0)+(ea>>>16)|0;X=ea<<16|ca&65535;ca=((f(m,E)|0)+(fa&65535)|0)+(Y&65535)|0;da=((f(u,E)|0)+(fa>>>16)|0)+(Y>>>16)|0;ea=((f(m,M)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(u,M)|0)+(da>>>16)|0)+(ea>>>16)|0;Y=ea<<16|ca&65535;ca=((f(m,F)|0)+(fa&65535)|0)+(Z&65535)|0;da=((f(u,F)|0)+(fa>>>16)|0)+(Z>>>16)|0;ea=((f(m,N)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(u,N)|0)+(da>>>16)|0)+(ea>>>16)|0;Z=ea<<16|ca&65535;$=fa;ca=((f(n,y)|0)+(_&65535)|0)+(T&65535)|0;da=((f(v,y)|0)+(_>>>16)|0)+(T>>>16)|0;ea=((f(n,G)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(v,G)|0)+(da>>>16)|0)+(ea>>>16)|0;T=ea<<16|ca&65535;ca=((f(n,z)|0)+(fa&65535)|0)+(U&65535)|0;da=((f(v,z)|0)+(fa>>>16)|0)+(U>>>16)|0;ea=((f(n,H)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(v,H)|0)+(da>>>16)|0)+(ea>>>16)|0;U=ea<<16|ca&65535;ca=((f(n,A)|0)+(fa&65535)|0)+(V&65535)|0;da=((f(v,A)|0)+(fa>>>16)|0)+(V>>>16)|0;ea=((f(n,I)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(v,I)|0)+(da>>>16)|0)+(ea>>>16)|0;V=ea<<16|ca&65535;ca=((f(n,B)|0)+(fa&65535)|0)+(W&65535)|0;da=((f(v,B)|0)+(fa>>>16)|0)+(W>>>16)|0;ea=((f(n,J)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(v,J)|0)+(da>>>16)|0)+(ea>>>16)|0;W=ea<<16|ca&65535;ca=((f(n,C)|0)+(fa&65535)|0)+(X&65535)|0;da=((f(v,C)|0)+(fa>>>16)|0)+(X>>>16)|0;ea=((f(n,K)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(v,K)|0)+(da>>>16)|0)+(ea>>>16)|0;X=ea<<16|ca&65535;ca=((f(n,D)|0)+(fa&65535)|0)+(Y&65535)|0;da=((f(v,D)|0)+(fa>>>16)|0)+(Y>>>16)|0;ea=((f(n,L)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(v,L)|0)+(da>>>16)|0)+(ea>>>16)|0;Y=ea<<16|ca&65535;ca=((f(n,E)|0)+(fa&65535)|0)+(Z&65535)|0;da=((f(v,E)|0)+(fa>>>16)|0)+(Z>>>16)|0;ea=((f(n,M)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(v,M)|0)+(da>>>16)|0)+(ea>>>16)|0;Z=ea<<16|ca&65535;ca=((f(n,F)|0)+(fa&65535)|0)+($&65535)|0;da=((f(v,F)|0)+(fa>>>16)|0)+($>>>16)|0;ea=((f(n,N)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(v,N)|0)+(da>>>16)|0)+(ea>>>16)|0;$=ea<<16|ca&65535;_=fa;ca=((f(o,y)|0)+(aa&65535)|0)+(U&65535)|0;da=((f(w,y)|0)+(aa>>>16)|0)+(U>>>16)|0;ea=((f(o,G)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(w,G)|0)+(da>>>16)|0)+(ea>>>16)|0;U=ea<<16|ca&65535;ca=((f(o,z)|0)+(fa&65535)|0)+(V&65535)|0;da=((f(w,z)|0)+(fa>>>16)|0)+(V>>>16)|0;ea=((f(o,H)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(w,H)|0)+(da>>>16)|0)+(ea>>>16)|0;V=ea<<16|ca&65535;ca=((f(o,A)|0)+(fa&65535)|0)+(W&65535)|0;da=((f(w,A)|0)+(fa>>>16)|0)+(W>>>16)|0;ea=((f(o,I)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(w,I)|0)+(da>>>16)|0)+(ea>>>16)|0;W=ea<<16|ca&65535;ca=((f(o,B)|0)+(fa&65535)|0)+(X&65535)|0;da=((f(w,B)|0)+(fa>>>16)|0)+(X>>>16)|0;ea=((f(o,J)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(w,J)|0)+(da>>>16)|0)+(ea>>>16)|0;X=ea<<16|ca&65535;ca=((f(o,C)|0)+(fa&65535)|0)+(Y&65535)|0;da=((f(w,C)|0)+(fa>>>16)|0)+(Y>>>16)|0;ea=((f(o,K)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(w,K)|0)+(da>>>16)|0)+(ea>>>16)|0;Y=ea<<16|ca&65535;ca=((f(o,D)|0)+(fa&65535)|0)+(Z&65535)|0;da=((f(w,D)|0)+(fa>>>16)|0)+(Z>>>16)|0;ea=((f(o,L)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(w,L)|0)+(da>>>16)|0)+(ea>>>16)|0;Z=ea<<16|ca&65535;ca=((f(o,E)|0)+(fa&65535)|0)+($&65535)|0;da=((f(w,E)|0)+(fa>>>16)|0)+($>>>16)|0;ea=((f(o,M)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(w,M)|0)+(da>>>16)|0)+(ea>>>16)|0;$=ea<<16|ca&65535;ca=((f(o,F)|0)+(fa&65535)|0)+(_&65535)|0;da=((f(w,F)|0)+(fa>>>16)|0)+(_>>>16)|0;ea=((f(o,N)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(w,N)|0)+(da>>>16)|0)+(ea>>>16)|0;_=ea<<16|ca&65535;aa=fa;ca=((f(p,y)|0)+(ba&65535)|0)+(V&65535)|0;da=((f(x,y)|0)+(ba>>>16)|0)+(V>>>16)|0;ea=((f(p,G)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(x,G)|0)+(da>>>16)|0)+(ea>>>16)|0;V=ea<<16|ca&65535;ca=((f(p,z)|0)+(fa&65535)|0)+(W&65535)|0;da=((f(x,z)|0)+(fa>>>16)|0)+(W>>>16)|0;ea=((f(p,H)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(x,H)|0)+(da>>>16)|0)+(ea>>>16)|0;W=ea<<16|ca&65535;ca=((f(p,A)|0)+(fa&65535)|0)+(X&65535)|0;da=((f(x,A)|0)+(fa>>>16)|0)+(X>>>16)|0;ea=((f(p,I)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(x,I)|0)+(da>>>16)|0)+(ea>>>16)|0;X=ea<<16|ca&65535;ca=((f(p,B)|0)+(fa&65535)|0)+(Y&65535)|0;da=((f(x,B)|0)+(fa>>>16)|0)+(Y>>>16)|0;ea=((f(p,J)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(x,J)|0)+(da>>>16)|0)+(ea>>>16)|0;Y=ea<<16|ca&65535;ca=((f(p,C)|0)+(fa&65535)|0)+(Z&65535)|0;da=((f(x,C)|0)+(fa>>>16)|0)+(Z>>>16)|0;ea=((f(p,K)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(x,K)|0)+(da>>>16)|0)+(ea>>>16)|0;Z=ea<<16|ca&65535;ca=((f(p,D)|0)+(fa&65535)|0)+($&65535)|0;da=((f(x,D)|0)+(fa>>>16)|0)+($>>>16)|0;ea=((f(p,L)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(x,L)|0)+(da>>>16)|0)+(ea>>>16)|0;$=ea<<16|ca&65535;ca=((f(p,E)|0)+(fa&65535)|0)+(_&65535)|0;da=((f(x,E)|0)+(fa>>>16)|0)+(_>>>16)|0;ea=((f(p,M)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(x,M)|0)+(da>>>16)|0)+(ea>>>16)|0;_=ea<<16|ca&65535;ca=((f(p,F)|0)+(fa&65535)|0)+(aa&65535)|0;da=((f(x,F)|0)+(fa>>>16)|0)+(aa>>>16)|0;ea=((f(p,N)|0)+(da&65535)|0)+(ca>>>16)|0;fa=((f(x,N)|0)+(da>>>16)|0)+(ea>>>16)|0;aa=ea<<16|ca&65535;ba=fa;e[(ka|0)>>2]=O,e[(ka|4)>>2]=P,e[(ka|8)>>2]=Q,e[(ka|12)>>2]=R,e[(ka|16)>>2]=S,e[(ka|20)>>2]=T,e[(ka|24)>>2]=U,e[(ka|28)>>2]=V}ka=g+(ga+ia|0)|0;e[(ka|0)>>2]=W,e[(ka|4)>>2]=X,e[(ka|8)>>2]=Y,e[(ka|12)>>2]=Z,e[(ka|16)>>2]=$,e[(ka|20)>>2]=_,e[(ka|24)>>2]=aa,e[(ka|28)>>2]=ba}}function r(a,b,c){a=a|0;b=b|0;c=c|0;var d=0,g=0,h=0,i=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0,M=0,N=0,O=0,P=0,Q=0,R=0,S=0,T=0,U=0,V=0,W=0,X=0,Y=0,Z=0,$=0,_=0,aa=0,ba=0,ca=0,da=0,ea=0,fa=0,ga=0,ha=0,ia=0,ja=0,ka=0,la=0,ma=0,na=0,oa=0;for(;(ja|0)<(b|0);ja=ja+4|0){oa=c+(ja<<1)|0;n=e[a+ja>>2]|0,d=n&65535,n=n>>>16;_=f(d,d)|0;aa=(f(d,n)|0)+(_>>>17)|0;ba=(f(n,n)|0)+(aa>>>15)|0;e[oa>>2]=aa<<17|_&131071;e[(oa|4)>>2]=ba}for(ia=0;(ia|0)<(b|0);ia=ia+8|0){ma=a+ia|0,oa=c+(ia<<1)|0;n=e[ma>>2]|0,d=n&65535,n=n>>>16;D=e[(ma|4)>>2]|0,v=D&65535,D=D>>>16;_=f(d,v)|0;aa=(f(d,D)|0)+(_>>>16)|0;ba=(f(n,v)|0)+(aa&65535)|0;ea=((f(n,D)|0)+(aa>>>16)|0)+(ba>>>16)|0;fa=e[(oa|4)>>2]|0;_=(fa&65535)+((_&65535)<<1)|0;ba=((fa>>>16)+((ba&65535)<<1)|0)+(_>>>16)|0;e[(oa|4)>>2]=ba<<16|_&65535;ca=ba>>>16;fa=e[(oa|8)>>2]|0;_=((fa&65535)+((ea&65535)<<1)|0)+ca|0;ba=((fa>>>16)+(ea>>>16<<1)|0)+(_>>>16)|0;e[(oa|8)>>2]=ba<<16|_&65535;ca=ba>>>16;if(ca){fa=e[(oa|12)>>2]|0;_=(fa&65535)+ca|0;ba=(fa>>>16)+(_>>>16)|0;e[(oa|12)>>2]=ba<<16|_&65535}}for(ia=0;(ia|0)<(b|0);ia=ia+16|0){ma=a+ia|0,oa=c+(ia<<1)|0;n=e[ma>>2]|0,d=n&65535,n=n>>>16,o=e[(ma|4)>>2]|0,g=o&65535,o=o>>>16;D=e[(ma|8)>>2]|0,v=D&65535,D=D>>>16,E=e[(ma|12)>>2]|0,w=E&65535,E=E>>>16;_=f(d,v)|0;aa=f(n,v)|0;ba=((f(d,D)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(n,D)|0)+(aa>>>16)|0)+(ba>>>16)|0;L=ba<<16|_&65535;_=(f(d,w)|0)+(ea&65535)|0;aa=(f(n,w)|0)+(ea>>>16)|0;ba=((f(d,E)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(n,E)|0)+(aa>>>16)|0)+(ba>>>16)|0;M=ba<<16|_&65535;N=ea;_=(f(g,v)|0)+(M&65535)|0;aa=(f(o,v)|0)+(M>>>16)|0;ba=((f(g,D)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(o,D)|0)+(aa>>>16)|0)+(ba>>>16)|0;M=ba<<16|_&65535;_=((f(g,w)|0)+(N&65535)|0)+(ea&65535)|0;aa=((f(o,w)|0)+(N>>>16)|0)+(ea>>>16)|0;ba=((f(g,E)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(o,E)|0)+(aa>>>16)|0)+(ba>>>16)|0;N=ba<<16|_&65535;O=ea;fa=e[(oa|8)>>2]|0;_=(fa&65535)+((L&65535)<<1)|0;ba=((fa>>>16)+(L>>>16<<1)|0)+(_>>>16)|0;e[(oa|8)>>2]=ba<<16|_&65535;ca=ba>>>16;fa=e[(oa|12)>>2]|0;_=((fa&65535)+((M&65535)<<1)|0)+ca|0;ba=((fa>>>16)+(M>>>16<<1)|0)+(_>>>16)|0;e[(oa|12)>>2]=ba<<16|_&65535;ca=ba>>>16;fa=e[(oa|16)>>2]|0;_=((fa&65535)+((N&65535)<<1)|0)+ca|0;ba=((fa>>>16)+(N>>>16<<1)|0)+(_>>>16)|0;e[(oa|16)>>2]=ba<<16|_&65535;ca=ba>>>16;fa=e[(oa|20)>>2]|0;_=((fa&65535)+((O&65535)<<1)|0)+ca|0;ba=((fa>>>16)+(O>>>16<<1)|0)+(_>>>16)|0;e[(oa|20)>>2]=ba<<16|_&65535;ca=ba>>>16;for(la=24;!!ca&(la|0)<32;la=la+4|0){fa=e[(oa|la)>>2]|0;_=(fa&65535)+ca|0;ba=(fa>>>16)+(_>>>16)|0;e[(oa|la)>>2]=ba<<16|_&65535;ca=ba>>>16}}for(ia=0;(ia|0)<(b|0);ia=ia+32|0){ma=a+ia|0,oa=c+(ia<<1)|0;n=e[ma>>2]|0,d=n&65535,n=n>>>16,o=e[(ma|4)>>2]|0,g=o&65535,o=o>>>16,p=e[(ma|8)>>2]|0,h=p&65535,p=p>>>16,q=e[(ma|12)>>2]|0,i=q&65535,q=q>>>16;D=e[(ma|16)>>2]|0,v=D&65535,D=D>>>16,E=e[(ma|20)>>2]|0,w=E&65535,E=E>>>16,F=e[(ma|24)>>2]|0,x=F&65535,F=F>>>16,G=e[(ma|28)>>2]|0,y=G&65535,G=G>>>16;_=f(d,v)|0;aa=f(n,v)|0;ba=((f(d,D)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(n,D)|0)+(aa>>>16)|0)+(ba>>>16)|0;L=ba<<16|_&65535;_=(f(d,w)|0)+(ea&65535)|0;aa=(f(n,w)|0)+(ea>>>16)|0;ba=((f(d,E)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(n,E)|0)+(aa>>>16)|0)+(ba>>>16)|0;M=ba<<16|_&65535;_=(f(d,x)|0)+(ea&65535)|0;aa=(f(n,x)|0)+(ea>>>16)|0;ba=((f(d,F)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(n,F)|0)+(aa>>>16)|0)+(ba>>>16)|0;N=ba<<16|_&65535;_=(f(d,y)|0)+(ea&65535)|0;aa=(f(n,y)|0)+(ea>>>16)|0;ba=((f(d,G)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(n,G)|0)+(aa>>>16)|0)+(ba>>>16)|0;O=ba<<16|_&65535;P=ea;_=(f(g,v)|0)+(M&65535)|0;aa=(f(o,v)|0)+(M>>>16)|0;ba=((f(g,D)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(o,D)|0)+(aa>>>16)|0)+(ba>>>16)|0;M=ba<<16|_&65535;_=((f(g,w)|0)+(N&65535)|0)+(ea&65535)|0;aa=((f(o,w)|0)+(N>>>16)|0)+(ea>>>16)|0;ba=((f(g,E)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(o,E)|0)+(aa>>>16)|0)+(ba>>>16)|0;N=ba<<16|_&65535;_=((f(g,x)|0)+(O&65535)|0)+(ea&65535)|0;aa=((f(o,x)|0)+(O>>>16)|0)+(ea>>>16)|0;ba=((f(g,F)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(o,F)|0)+(aa>>>16)|0)+(ba>>>16)|0;O=ba<<16|_&65535;_=((f(g,y)|0)+(P&65535)|0)+(ea&65535)|0;aa=((f(o,y)|0)+(P>>>16)|0)+(ea>>>16)|0;ba=((f(g,G)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(o,G)|0)+(aa>>>16)|0)+(ba>>>16)|0;P=ba<<16|_&65535;Q=ea;_=(f(h,v)|0)+(N&65535)|0;aa=(f(p,v)|0)+(N>>>16)|0;ba=((f(h,D)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(p,D)|0)+(aa>>>16)|0)+(ba>>>16)|0;N=ba<<16|_&65535;_=((f(h,w)|0)+(O&65535)|0)+(ea&65535)|0;aa=((f(p,w)|0)+(O>>>16)|0)+(ea>>>16)|0;ba=((f(h,E)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(p,E)|0)+(aa>>>16)|0)+(ba>>>16)|0;O=ba<<16|_&65535;_=((f(h,x)|0)+(P&65535)|0)+(ea&65535)|0;aa=((f(p,x)|0)+(P>>>16)|0)+(ea>>>16)|0;ba=((f(h,F)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(p,F)|0)+(aa>>>16)|0)+(ba>>>16)|0;P=ba<<16|_&65535;_=((f(h,y)|0)+(Q&65535)|0)+(ea&65535)|0;aa=((f(p,y)|0)+(Q>>>16)|0)+(ea>>>16)|0;ba=((f(h,G)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(p,G)|0)+(aa>>>16)|0)+(ba>>>16)|0;Q=ba<<16|_&65535;R=ea;_=(f(i,v)|0)+(O&65535)|0;aa=(f(q,v)|0)+(O>>>16)|0;ba=((f(i,D)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(q,D)|0)+(aa>>>16)|0)+(ba>>>16)|0;O=ba<<16|_&65535;_=((f(i,w)|0)+(P&65535)|0)+(ea&65535)|0;aa=((f(q,w)|0)+(P>>>16)|0)+(ea>>>16)|0;ba=((f(i,E)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(q,E)|0)+(aa>>>16)|0)+(ba>>>16)|0;P=ba<<16|_&65535;_=((f(i,x)|0)+(Q&65535)|0)+(ea&65535)|0;aa=((f(q,x)|0)+(Q>>>16)|0)+(ea>>>16)|0;ba=((f(i,F)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(q,F)|0)+(aa>>>16)|0)+(ba>>>16)|0;Q=ba<<16|_&65535;_=((f(i,y)|0)+(R&65535)|0)+(ea&65535)|0;aa=((f(q,y)|0)+(R>>>16)|0)+(ea>>>16)|0;ba=((f(i,G)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(q,G)|0)+(aa>>>16)|0)+(ba>>>16)|0;R=ba<<16|_&65535;S=ea;fa=e[(oa|16)>>2]|0;_=(fa&65535)+((L&65535)<<1)|0;ba=((fa>>>16)+(L>>>16<<1)|0)+(_>>>16)|0;e[(oa|16)>>2]=ba<<16|_&65535;ca=ba>>>16;fa=e[(oa|20)>>2]|0;_=((fa&65535)+((M&65535)<<1)|0)+ca|0;ba=((fa>>>16)+(M>>>16<<1)|0)+(_>>>16)|0;e[(oa|20)>>2]=ba<<16|_&65535;ca=ba>>>16;fa=e[(oa|24)>>2]|0;_=((fa&65535)+((N&65535)<<1)|0)+ca|0;ba=((fa>>>16)+(N>>>16<<1)|0)+(_>>>16)|0;e[(oa|24)>>2]=ba<<16|_&65535;ca=ba>>>16;fa=e[(oa|28)>>2]|0;_=((fa&65535)+((O&65535)<<1)|0)+ca|0;ba=((fa>>>16)+(O>>>16<<1)|0)+(_>>>16)|0;e[(oa|28)>>2]=ba<<16|_&65535;ca=ba>>>16;fa=e[oa+32>>2]|0;_=((fa&65535)+((P&65535)<<1)|0)+ca|0;ba=((fa>>>16)+(P>>>16<<1)|0)+(_>>>16)|0;e[oa+32>>2]=ba<<16|_&65535;ca=ba>>>16;fa=e[oa+36>>2]|0;_=((fa&65535)+((Q&65535)<<1)|0)+ca|0;ba=((fa>>>16)+(Q>>>16<<1)|0)+(_>>>16)|0;e[oa+36>>2]=ba<<16|_&65535;ca=ba>>>16;fa=e[oa+40>>2]|0;_=((fa&65535)+((R&65535)<<1)|0)+ca|0;ba=((fa>>>16)+(R>>>16<<1)|0)+(_>>>16)|0;e[oa+40>>2]=ba<<16|_&65535;ca=ba>>>16;fa=e[oa+44>>2]|0;_=((fa&65535)+((S&65535)<<1)|0)+ca|0;ba=((fa>>>16)+(S>>>16<<1)|0)+(_>>>16)|0;e[oa+44>>2]=ba<<16|_&65535;ca=ba>>>16;for(la=48;!!ca&(la|0)<64;la=la+4|0){fa=e[oa+la>>2]|0;_=(fa&65535)+ca|0;ba=(fa>>>16)+(_>>>16)|0;e[oa+la>>2]=ba<<16|_&65535;ca=ba>>>16}}for(ga=32;(ga|0)<(b|0);ga=ga<<1){ha=ga<<1;for(ia=0;(ia|0)<(b|0);ia=ia+ha|0){oa=c+(ia<<1)|0;da=0;for(ja=0;(ja|0)<(ga|0);ja=ja+32|0){ma=(a+ia|0)+ja|0;n=e[ma>>2]|0,d=n&65535,n=n>>>16,o=e[(ma|4)>>2]|0,g=o&65535,o=o>>>16,p=e[(ma|8)>>2]|0,h=p&65535,p=p>>>16,q=e[(ma|12)>>2]|0,i=q&65535,q=q>>>16,r=e[(ma|16)>>2]|0,j=r&65535,r=r>>>16,s=e[(ma|20)>>2]|0,k=s&65535,s=s>>>16,t=e[(ma|24)>>2]|0,l=t&65535,t=t>>>16,u=e[(ma|28)>>2]|0,m=u&65535,u=u>>>16;T=U=V=W=X=Y=Z=$=ca=0;for(ka=0;(ka|0)<(ga|0);ka=ka+32|0){na=((a+ia|0)+ga|0)+ka|0;D=e[na>>2]|0,v=D&65535,D=D>>>16,E=e[(na|4)>>2]|0,w=E&65535,E=E>>>16,F=e[(na|8)>>2]|0,x=F&65535,F=F>>>16,G=e[(na|12)>>2]|0,y=G&65535,G=G>>>16,H=e[(na|16)>>2]|0,z=H&65535,H=H>>>16,I=e[(na|20)>>2]|0,A=I&65535,I=I>>>16,J=e[(na|24)>>2]|0,B=J&65535,J=J>>>16,K=e[(na|28)>>2]|0,C=K&65535,K=K>>>16;L=M=N=O=P=Q=R=S=0;_=((f(d,v)|0)+(L&65535)|0)+(T&65535)|0;aa=((f(n,v)|0)+(L>>>16)|0)+(T>>>16)|0;ba=((f(d,D)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(n,D)|0)+(aa>>>16)|0)+(ba>>>16)|0;L=ba<<16|_&65535;_=((f(d,w)|0)+(M&65535)|0)+(ea&65535)|0;aa=((f(n,w)|0)+(M>>>16)|0)+(ea>>>16)|0;ba=((f(d,E)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(n,E)|0)+(aa>>>16)|0)+(ba>>>16)|0;M=ba<<16|_&65535;_=((f(d,x)|0)+(N&65535)|0)+(ea&65535)|0;aa=((f(n,x)|0)+(N>>>16)|0)+(ea>>>16)|0;ba=((f(d,F)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(n,F)|0)+(aa>>>16)|0)+(ba>>>16)|0;N=ba<<16|_&65535;_=((f(d,y)|0)+(O&65535)|0)+(ea&65535)|0;aa=((f(n,y)|0)+(O>>>16)|0)+(ea>>>16)|0;ba=((f(d,G)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(n,G)|0)+(aa>>>16)|0)+(ba>>>16)|0;O=ba<<16|_&65535;_=((f(d,z)|0)+(P&65535)|0)+(ea&65535)|0;aa=((f(n,z)|0)+(P>>>16)|0)+(ea>>>16)|0;ba=((f(d,H)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(n,H)|0)+(aa>>>16)|0)+(ba>>>16)|0;P=ba<<16|_&65535;_=((f(d,A)|0)+(Q&65535)|0)+(ea&65535)|0;aa=((f(n,A)|0)+(Q>>>16)|0)+(ea>>>16)|0;ba=((f(d,I)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(n,I)|0)+(aa>>>16)|0)+(ba>>>16)|0;Q=ba<<16|_&65535;_=((f(d,B)|0)+(R&65535)|0)+(ea&65535)|0;aa=((f(n,B)|0)+(R>>>16)|0)+(ea>>>16)|0;ba=((f(d,J)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(n,J)|0)+(aa>>>16)|0)+(ba>>>16)|0;R=ba<<16|_&65535;_=((f(d,C)|0)+(S&65535)|0)+(ea&65535)|0;aa=((f(n,C)|0)+(S>>>16)|0)+(ea>>>16)|0;ba=((f(d,K)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(n,K)|0)+(aa>>>16)|0)+(ba>>>16)|0;S=ba<<16|_&65535;T=ea;_=((f(g,v)|0)+(M&65535)|0)+(U&65535)|0;aa=((f(o,v)|0)+(M>>>16)|0)+(U>>>16)|0;ba=((f(g,D)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(o,D)|0)+(aa>>>16)|0)+(ba>>>16)|0;M=ba<<16|_&65535;_=((f(g,w)|0)+(N&65535)|0)+(ea&65535)|0;aa=((f(o,w)|0)+(N>>>16)|0)+(ea>>>16)|0;ba=((f(g,E)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(o,E)|0)+(aa>>>16)|0)+(ba>>>16)|0;N=ba<<16|_&65535;_=((f(g,x)|0)+(O&65535)|0)+(ea&65535)|0;aa=((f(o,x)|0)+(O>>>16)|0)+(ea>>>16)|0;ba=((f(g,F)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(o,F)|0)+(aa>>>16)|0)+(ba>>>16)|0;O=ba<<16|_&65535;_=((f(g,y)|0)+(P&65535)|0)+(ea&65535)|0;aa=((f(o,y)|0)+(P>>>16)|0)+(ea>>>16)|0;ba=((f(g,G)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(o,G)|0)+(aa>>>16)|0)+(ba>>>16)|0;P=ba<<16|_&65535;_=((f(g,z)|0)+(Q&65535)|0)+(ea&65535)|0;aa=((f(o,z)|0)+(Q>>>16)|0)+(ea>>>16)|0;ba=((f(g,H)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(o,H)|0)+(aa>>>16)|0)+(ba>>>16)|0;Q=ba<<16|_&65535;_=((f(g,A)|0)+(R&65535)|0)+(ea&65535)|0;aa=((f(o,A)|0)+(R>>>16)|0)+(ea>>>16)|0;ba=((f(g,I)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(o,I)|0)+(aa>>>16)|0)+(ba>>>16)|0;R=ba<<16|_&65535;_=((f(g,B)|0)+(S&65535)|0)+(ea&65535)|0;aa=((f(o,B)|0)+(S>>>16)|0)+(ea>>>16)|0;ba=((f(g,J)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(o,J)|0)+(aa>>>16)|0)+(ba>>>16)|0;S=ba<<16|_&65535;_=((f(g,C)|0)+(T&65535)|0)+(ea&65535)|0;aa=((f(o,C)|0)+(T>>>16)|0)+(ea>>>16)|0;ba=((f(g,K)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(o,K)|0)+(aa>>>16)|0)+(ba>>>16)|0;T=ba<<16|_&65535;U=ea;_=((f(h,v)|0)+(N&65535)|0)+(V&65535)|0;aa=((f(p,v)|0)+(N>>>16)|0)+(V>>>16)|0;ba=((f(h,D)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(p,D)|0)+(aa>>>16)|0)+(ba>>>16)|0;N=ba<<16|_&65535;_=((f(h,w)|0)+(O&65535)|0)+(ea&65535)|0;aa=((f(p,w)|0)+(O>>>16)|0)+(ea>>>16)|0;ba=((f(h,E)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(p,E)|0)+(aa>>>16)|0)+(ba>>>16)|0;O=ba<<16|_&65535;_=((f(h,x)|0)+(P&65535)|0)+(ea&65535)|0;aa=((f(p,x)|0)+(P>>>16)|0)+(ea>>>16)|0;ba=((f(h,F)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(p,F)|0)+(aa>>>16)|0)+(ba>>>16)|0;P=ba<<16|_&65535;_=((f(h,y)|0)+(Q&65535)|0)+(ea&65535)|0;aa=((f(p,y)|0)+(Q>>>16)|0)+(ea>>>16)|0;ba=((f(h,G)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(p,G)|0)+(aa>>>16)|0)+(ba>>>16)|0;Q=ba<<16|_&65535;_=((f(h,z)|0)+(R&65535)|0)+(ea&65535)|0;aa=((f(p,z)|0)+(R>>>16)|0)+(ea>>>16)|0;ba=((f(h,H)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(p,H)|0)+(aa>>>16)|0)+(ba>>>16)|0;R=ba<<16|_&65535;_=((f(h,A)|0)+(S&65535)|0)+(ea&65535)|0;aa=((f(p,A)|0)+(S>>>16)|0)+(ea>>>16)|0;ba=((f(h,I)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(p,I)|0)+(aa>>>16)|0)+(ba>>>16)|0;S=ba<<16|_&65535;_=((f(h,B)|0)+(T&65535)|0)+(ea&65535)|0;aa=((f(p,B)|0)+(T>>>16)|0)+(ea>>>16)|0;ba=((f(h,J)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(p,J)|0)+(aa>>>16)|0)+(ba>>>16)|0;T=ba<<16|_&65535;_=((f(h,C)|0)+(U&65535)|0)+(ea&65535)|0;aa=((f(p,C)|0)+(U>>>16)|0)+(ea>>>16)|0;ba=((f(h,K)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(p,K)|0)+(aa>>>16)|0)+(ba>>>16)|0;U=ba<<16|_&65535;V=ea;_=((f(i,v)|0)+(O&65535)|0)+(W&65535)|0;aa=((f(q,v)|0)+(O>>>16)|0)+(W>>>16)|0;ba=((f(i,D)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(q,D)|0)+(aa>>>16)|0)+(ba>>>16)|0;O=ba<<16|_&65535;_=((f(i,w)|0)+(P&65535)|0)+(ea&65535)|0;aa=((f(q,w)|0)+(P>>>16)|0)+(ea>>>16)|0;ba=((f(i,E)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(q,E)|0)+(aa>>>16)|0)+(ba>>>16)|0;P=ba<<16|_&65535;_=((f(i,x)|0)+(Q&65535)|0)+(ea&65535)|0;aa=((f(q,x)|0)+(Q>>>16)|0)+(ea>>>16)|0;ba=((f(i,F)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(q,F)|0)+(aa>>>16)|0)+(ba>>>16)|0;Q=ba<<16|_&65535;_=((f(i,y)|0)+(R&65535)|0)+(ea&65535)|0;aa=((f(q,y)|0)+(R>>>16)|0)+(ea>>>16)|0;ba=((f(i,G)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(q,G)|0)+(aa>>>16)|0)+(ba>>>16)|0;R=ba<<16|_&65535;_=((f(i,z)|0)+(S&65535)|0)+(ea&65535)|0;aa=((f(q,z)|0)+(S>>>16)|0)+(ea>>>16)|0;ba=((f(i,H)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(q,H)|0)+(aa>>>16)|0)+(ba>>>16)|0;S=ba<<16|_&65535;_=((f(i,A)|0)+(T&65535)|0)+(ea&65535)|0;aa=((f(q,A)|0)+(T>>>16)|0)+(ea>>>16)|0;ba=((f(i,I)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(q,I)|0)+(aa>>>16)|0)+(ba>>>16)|0;T=ba<<16|_&65535;_=((f(i,B)|0)+(U&65535)|0)+(ea&65535)|0;aa=((f(q,B)|0)+(U>>>16)|0)+(ea>>>16)|0;ba=((f(i,J)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(q,J)|0)+(aa>>>16)|0)+(ba>>>16)|0;U=ba<<16|_&65535;_=((f(i,C)|0)+(V&65535)|0)+(ea&65535)|0;aa=((f(q,C)|0)+(V>>>16)|0)+(ea>>>16)|0;ba=((f(i,K)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(q,K)|0)+(aa>>>16)|0)+(ba>>>16)|0;V=ba<<16|_&65535;W=ea;_=((f(j,v)|0)+(P&65535)|0)+(X&65535)|0;aa=((f(r,v)|0)+(P>>>16)|0)+(X>>>16)|0;ba=((f(j,D)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(r,D)|0)+(aa>>>16)|0)+(ba>>>16)|0;P=ba<<16|_&65535;_=((f(j,w)|0)+(Q&65535)|0)+(ea&65535)|0;aa=((f(r,w)|0)+(Q>>>16)|0)+(ea>>>16)|0;ba=((f(j,E)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(r,E)|0)+(aa>>>16)|0)+(ba>>>16)|0;Q=ba<<16|_&65535;_=((f(j,x)|0)+(R&65535)|0)+(ea&65535)|0;aa=((f(r,x)|0)+(R>>>16)|0)+(ea>>>16)|0;ba=((f(j,F)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(r,F)|0)+(aa>>>16)|0)+(ba>>>16)|0;R=ba<<16|_&65535;_=((f(j,y)|0)+(S&65535)|0)+(ea&65535)|0;aa=((f(r,y)|0)+(S>>>16)|0)+(ea>>>16)|0;ba=((f(j,G)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(r,G)|0)+(aa>>>16)|0)+(ba>>>16)|0;S=ba<<16|_&65535;_=((f(j,z)|0)+(T&65535)|0)+(ea&65535)|0;aa=((f(r,z)|0)+(T>>>16)|0)+(ea>>>16)|0;ba=((f(j,H)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(r,H)|0)+(aa>>>16)|0)+(ba>>>16)|0;T=ba<<16|_&65535;_=((f(j,A)|0)+(U&65535)|0)+(ea&65535)|0;aa=((f(r,A)|0)+(U>>>16)|0)+(ea>>>16)|0;ba=((f(j,I)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(r,I)|0)+(aa>>>16)|0)+(ba>>>16)|0;U=ba<<16|_&65535;_=((f(j,B)|0)+(V&65535)|0)+(ea&65535)|0;aa=((f(r,B)|0)+(V>>>16)|0)+(ea>>>16)|0;ba=((f(j,J)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(r,J)|0)+(aa>>>16)|0)+(ba>>>16)|0;V=ba<<16|_&65535;_=((f(j,C)|0)+(W&65535)|0)+(ea&65535)|0;aa=((f(r,C)|0)+(W>>>16)|0)+(ea>>>16)|0;ba=((f(j,K)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(r,K)|0)+(aa>>>16)|0)+(ba>>>16)|0;W=ba<<16|_&65535;X=ea;_=((f(k,v)|0)+(Q&65535)|0)+(Y&65535)|0;aa=((f(s,v)|0)+(Q>>>16)|0)+(Y>>>16)|0;ba=((f(k,D)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(s,D)|0)+(aa>>>16)|0)+(ba>>>16)|0;Q=ba<<16|_&65535;_=((f(k,w)|0)+(R&65535)|0)+(ea&65535)|0;aa=((f(s,w)|0)+(R>>>16)|0)+(ea>>>16)|0;ba=((f(k,E)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(s,E)|0)+(aa>>>16)|0)+(ba>>>16)|0;R=ba<<16|_&65535;_=((f(k,x)|0)+(S&65535)|0)+(ea&65535)|0;aa=((f(s,x)|0)+(S>>>16)|0)+(ea>>>16)|0;ba=((f(k,F)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(s,F)|0)+(aa>>>16)|0)+(ba>>>16)|0;S=ba<<16|_&65535;_=((f(k,y)|0)+(T&65535)|0)+(ea&65535)|0;aa=((f(s,y)|0)+(T>>>16)|0)+(ea>>>16)|0;ba=((f(k,G)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(s,G)|0)+(aa>>>16)|0)+(ba>>>16)|0;T=ba<<16|_&65535;_=((f(k,z)|0)+(U&65535)|0)+(ea&65535)|0;aa=((f(s,z)|0)+(U>>>16)|0)+(ea>>>16)|0;ba=((f(k,H)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(s,H)|0)+(aa>>>16)|0)+(ba>>>16)|0;U=ba<<16|_&65535;_=((f(k,A)|0)+(V&65535)|0)+(ea&65535)|0;aa=((f(s,A)|0)+(V>>>16)|0)+(ea>>>16)|0;ba=((f(k,I)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(s,I)|0)+(aa>>>16)|0)+(ba>>>16)|0;V=ba<<16|_&65535;_=((f(k,B)|0)+(W&65535)|0)+(ea&65535)|0;aa=((f(s,B)|0)+(W>>>16)|0)+(ea>>>16)|0;ba=((f(k,J)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(s,J)|0)+(aa>>>16)|0)+(ba>>>16)|0;W=ba<<16|_&65535;_=((f(k,C)|0)+(X&65535)|0)+(ea&65535)|0;aa=((f(s,C)|0)+(X>>>16)|0)+(ea>>>16)|0;ba=((f(k,K)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(s,K)|0)+(aa>>>16)|0)+(ba>>>16)|0;X=ba<<16|_&65535;Y=ea;_=((f(l,v)|0)+(R&65535)|0)+(Z&65535)|0;aa=((f(t,v)|0)+(R>>>16)|0)+(Z>>>16)|0;ba=((f(l,D)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(t,D)|0)+(aa>>>16)|0)+(ba>>>16)|0;R=ba<<16|_&65535;_=((f(l,w)|0)+(S&65535)|0)+(ea&65535)|0;aa=((f(t,w)|0)+(S>>>16)|0)+(ea>>>16)|0;ba=((f(l,E)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(t,E)|0)+(aa>>>16)|0)+(ba>>>16)|0;S=ba<<16|_&65535;_=((f(l,x)|0)+(T&65535)|0)+(ea&65535)|0;aa=((f(t,x)|0)+(T>>>16)|0)+(ea>>>16)|0;ba=((f(l,F)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(t,F)|0)+(aa>>>16)|0)+(ba>>>16)|0;T=ba<<16|_&65535;_=((f(l,y)|0)+(U&65535)|0)+(ea&65535)|0;aa=((f(t,y)|0)+(U>>>16)|0)+(ea>>>16)|0;ba=((f(l,G)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(t,G)|0)+(aa>>>16)|0)+(ba>>>16)|0;U=ba<<16|_&65535;_=((f(l,z)|0)+(V&65535)|0)+(ea&65535)|0;aa=((f(t,z)|0)+(V>>>16)|0)+(ea>>>16)|0;ba=((f(l,H)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(t,H)|0)+(aa>>>16)|0)+(ba>>>16)|0;V=ba<<16|_&65535;_=((f(l,A)|0)+(W&65535)|0)+(ea&65535)|0;aa=((f(t,A)|0)+(W>>>16)|0)+(ea>>>16)|0;ba=((f(l,I)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(t,I)|0)+(aa>>>16)|0)+(ba>>>16)|0;W=ba<<16|_&65535;_=((f(l,B)|0)+(X&65535)|0)+(ea&65535)|0;aa=((f(t,B)|0)+(X>>>16)|0)+(ea>>>16)|0;ba=((f(l,J)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(t,J)|0)+(aa>>>16)|0)+(ba>>>16)|0;X=ba<<16|_&65535;_=((f(l,C)|0)+(Y&65535)|0)+(ea&65535)|0;aa=((f(t,C)|0)+(Y>>>16)|0)+(ea>>>16)|0;ba=((f(l,K)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(t,K)|0)+(aa>>>16)|0)+(ba>>>16)|0;Y=ba<<16|_&65535;Z=ea;_=((f(m,v)|0)+(S&65535)|0)+($&65535)|0;aa=((f(u,v)|0)+(S>>>16)|0)+($>>>16)|0;ba=((f(m,D)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(u,D)|0)+(aa>>>16)|0)+(ba>>>16)|0;S=ba<<16|_&65535;_=((f(m,w)|0)+(T&65535)|0)+(ea&65535)|0;aa=((f(u,w)|0)+(T>>>16)|0)+(ea>>>16)|0;ba=((f(m,E)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(u,E)|0)+(aa>>>16)|0)+(ba>>>16)|0;T=ba<<16|_&65535;_=((f(m,x)|0)+(U&65535)|0)+(ea&65535)|0;aa=((f(u,x)|0)+(U>>>16)|0)+(ea>>>16)|0;ba=((f(m,F)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(u,F)|0)+(aa>>>16)|0)+(ba>>>16)|0;U=ba<<16|_&65535;_=((f(m,y)|0)+(V&65535)|0)+(ea&65535)|0;aa=((f(u,y)|0)+(V>>>16)|0)+(ea>>>16)|0;ba=((f(m,G)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(u,G)|0)+(aa>>>16)|0)+(ba>>>16)|0;V=ba<<16|_&65535;_=((f(m,z)|0)+(W&65535)|0)+(ea&65535)|0;aa=((f(u,z)|0)+(W>>>16)|0)+(ea>>>16)|0;ba=((f(m,H)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(u,H)|0)+(aa>>>16)|0)+(ba>>>16)|0;W=ba<<16|_&65535;_=((f(m,A)|0)+(X&65535)|0)+(ea&65535)|0;aa=((f(u,A)|0)+(X>>>16)|0)+(ea>>>16)|0;ba=((f(m,I)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(u,I)|0)+(aa>>>16)|0)+(ba>>>16)|0;X=ba<<16|_&65535;_=((f(m,B)|0)+(Y&65535)|0)+(ea&65535)|0;aa=((f(u,B)|0)+(Y>>>16)|0)+(ea>>>16)|0;ba=((f(m,J)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(u,J)|0)+(aa>>>16)|0)+(ba>>>16)|0;Y=ba<<16|_&65535;_=((f(m,C)|0)+(Z&65535)|0)+(ea&65535)|0;aa=((f(u,C)|0)+(Z>>>16)|0)+(ea>>>16)|0;ba=((f(m,K)|0)+(aa&65535)|0)+(_>>>16)|0;ea=((f(u,K)|0)+(aa>>>16)|0)+(ba>>>16)|0;Z=ba<<16|_&65535;$=ea;la=ga+(ja+ka|0)|0;fa=e[oa+la>>2]|0;_=((fa&65535)+((L&65535)<<1)|0)+ca|0;ba=((fa>>>16)+(L>>>16<<1)|0)+(_>>>16)|0;e[oa+la>>2]=ba<<16|_&65535;ca=ba>>>16;la=la+4|0;fa=e[oa+la>>2]|0;_=((fa&65535)+((M&65535)<<1)|0)+ca|0;ba=((fa>>>16)+(M>>>16<<1)|0)+(_>>>16)|0;e[oa+la>>2]=ba<<16|_&65535;ca=ba>>>16;la=la+4|0;fa=e[oa+la>>2]|0;_=((fa&65535)+((N&65535)<<1)|0)+ca|0;ba=((fa>>>16)+(N>>>16<<1)|0)+(_>>>16)|0;e[oa+la>>2]=ba<<16|_&65535;ca=ba>>>16;la=la+4|0;fa=e[oa+la>>2]|0;_=((fa&65535)+((O&65535)<<1)|0)+ca|0;ba=((fa>>>16)+(O>>>16<<1)|0)+(_>>>16)|0;e[oa+la>>2]=ba<<16|_&65535;ca=ba>>>16;la=la+4|0;fa=e[oa+la>>2]|0;_=((fa&65535)+((P&65535)<<1)|0)+ca|0;ba=((fa>>>16)+(P>>>16<<1)|0)+(_>>>16)|0;e[oa+la>>2]=ba<<16|_&65535;ca=ba>>>16;la=la+4|0;fa=e[oa+la>>2]|0;_=((fa&65535)+((Q&65535)<<1)|0)+ca|0;ba=((fa>>>16)+(Q>>>16<<1)|0)+(_>>>16)|0;e[oa+la>>2]=ba<<16|_&65535;ca=ba>>>16;la=la+4|0;fa=e[oa+la>>2]|0;_=((fa&65535)+((R&65535)<<1)|0)+ca|0;ba=((fa>>>16)+(R>>>16<<1)|0)+(_>>>16)|0;e[oa+la>>2]=ba<<16|_&65535;ca=ba>>>16;la=la+4|0;fa=e[oa+la>>2]|0;_=((fa&65535)+((S&65535)<<1)|0)+ca|0;ba=((fa>>>16)+(S>>>16<<1)|0)+(_>>>16)|0;e[oa+la>>2]=ba<<16|_&65535;ca=ba>>>16}la=ga+(ja+ka|0)|0;fa=e[oa+la>>2]|0;_=(((fa&65535)+((T&65535)<<1)|0)+ca|0)+da|0;ba=((fa>>>16)+(T>>>16<<1)|0)+(_>>>16)|0;e[oa+la>>2]=ba<<16|_&65535;ca=ba>>>16;la=la+4|0;fa=e[oa+la>>2]|0;_=((fa&65535)+((U&65535)<<1)|0)+ca|0;ba=((fa>>>16)+(U>>>16<<1)|0)+(_>>>16)|0;e[oa+la>>2]=ba<<16|_&65535;ca=ba>>>16;la=la+4|0;fa=e[oa+la>>2]|0;_=((fa&65535)+((V&65535)<<1)|0)+ca|0;ba=((fa>>>16)+(V>>>16<<1)|0)+(_>>>16)|0;e[oa+la>>2]=ba<<16|_&65535;ca=ba>>>16;la=la+4|0;fa=e[oa+la>>2]|0;_=((fa&65535)+((W&65535)<<1)|0)+ca|0;ba=((fa>>>16)+(W>>>16<<1)|0)+(_>>>16)|0;e[oa+la>>2]=ba<<16|_&65535;ca=ba>>>16;la=la+4|0;fa=e[oa+la>>2]|0;_=((fa&65535)+((X&65535)<<1)|0)+ca|0;ba=((fa>>>16)+(X>>>16<<1)|0)+(_>>>16)|0;e[oa+la>>2]=ba<<16|_&65535;ca=ba>>>16;la=la+4|0;fa=e[oa+la>>2]|0;_=((fa&65535)+((Y&65535)<<1)|0)+ca|0;ba=((fa>>>16)+(Y>>>16<<1)|0)+(_>>>16)|0;e[oa+la>>2]=ba<<16|_&65535;ca=ba>>>16;la=la+4|0;fa=e[oa+la>>2]|0;_=((fa&65535)+((Z&65535)<<1)|0)+ca|0;ba=((fa>>>16)+(Z>>>16<<1)|0)+(_>>>16)|0;e[oa+la>>2]=ba<<16|_&65535;ca=ba>>>16;la=la+4|0;fa=e[oa+la>>2]|0;_=((fa&65535)+(($&65535)<<1)|0)+ca|0;ba=((fa>>>16)+($>>>16<<1)|0)+(_>>>16)|0;e[oa+la>>2]=ba<<16|_&65535;da=ba>>>16}for(la=la+4|0;!!da&(la|0)<ha<<1;la=la+4|0){fa=e[oa+la>>2]|0;_=(fa&65535)+da|0;ba=(fa>>>16)+(_>>>16)|0;e[oa+la>>2]=ba<<16|_&65535;da=ba>>>16}}}}function s(a,b,c,d,g){a=a|0;b=b|0;c=c|0;d=d|0;g=g|0;var h=0,i=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0;for(x=b-1&-4;(x|0)>=0;x=x-4|0){h=e[a+x>>2]|0;if(h){b=x;break}}for(x=d-1&-4;(x|0)>=0;x=x-4|0){i=e[c+x>>2]|0;if(i){d=x;break}}while((i&2147483648)==0){i=i<<1;j=j+1|0}l=e[a+b>>2]|0;if(j){k=l>>>(32-j|0);for(x=b-4|0;(x|0)>=0;x=x-4|0){h=e[a+x>>2]|0;e[a+x+4>>2]=l<<j|(j?h>>>(32-j|0):0);l=h}e[a>>2]=l<<j}if(j){m=e[c+d>>2]|0;for(x=d-4|0;(x|0)>=0;x=x-4|0){i=e[c+x>>2]|0;e[c+x+4>>2]=m<<j|i>>>(32-j|0);m=i}e[c>>2]=m<<j}m=e[c+d>>2]|0;n=m>>>16,o=m&65535;for(x=b;(x|0)>=(d|0);x=x-4|0){y=x-d|0;l=e[a+x>>2]|0;p=(k>>>0)/(n>>>0)|0,r=(k>>>0)%(n>>>0)|0,t=f(p,o)|0;while((p|0)==65536|t>>>0>(r<<16|l>>>16)>>>0){p=p-1|0,r=r+n|0,t=t-o|0;if((r|0)>=65536)break}v=0,w=0;for(z=0;(z|0)<=(d|0);z=z+4|0){i=e[c+z>>2]|0;t=(f(p,i&65535)|0)+(v>>>16)|0;u=(f(p,i>>>16)|0)+(t>>>16)|0;i=v&65535|t<<16;v=u;h=e[a+y+z>>2]|0;t=((h&65535)-(i&65535)|0)+w|0;u=((h>>>16)-(i>>>16)|0)+(t>>16)|0;e[a+y+z>>2]=u<<16|t&65535;w=u>>16}t=((k&65535)-(v&65535)|0)+w|0;u=((k>>>16)-(v>>>16)|0)+(t>>16)|0;k=u<<16|t&65535;w=u>>16;if(w){p=p-1|0;w=0;for(z=0;(z|0)<=(d|0);z=z+4|0){i=e[c+z>>2]|0;h=e[a+y+z>>2]|0;t=(h&65535)+w|0;u=(h>>>16)+i+(t>>>16)|0;e[a+y+z>>2]=u<<16|t&65535;w=u>>>16}k=k+w|0}l=e[a+x>>2]|0;h=k<<16|l>>>16;q=(h>>>0)/(n>>>0)|0,s=(h>>>0)%(n>>>0)|0,t=f(q,o)|0;while((q|0)==65536|t>>>0>(s<<16|l&65535)>>>0){q=q-1|0,s=s+n|0,t=t-o|0;if((s|0)>=65536)break}v=0,w=0;for(z=0;(z|0)<=(d|0);z=z+4|0){i=e[c+z>>2]|0;t=(f(q,i&65535)|0)+(v&65535)|0;u=((f(q,i>>>16)|0)+(t>>>16)|0)+(v>>>16)|0;i=t&65535|u<<16;v=u>>>16;h=e[a+y+z>>2]|0;t=((h&65535)-(i&65535)|0)+w|0;u=((h>>>16)-(i>>>16)|0)+(t>>16)|0;w=u>>16;e[a+y+z>>2]=u<<16|t&65535}t=((k&65535)-(v&65535)|0)+w|0;u=((k>>>16)-(v>>>16)|0)+(t>>16)|0;w=u>>16;if(w){q=q-1|0;w=0;for(z=0;(z|0)<=(d|0);z=z+4|0){i=e[c+z>>2]|0;h=e[a+y+z>>2]|0;t=((h&65535)+(i&65535)|0)+w|0;u=((h>>>16)+(i>>>16)|0)+(t>>>16)|0;w=u>>>16;e[a+y+z>>2]=t&65535|u<<16}}e[g+y>>2]=p<<16|q;k=e[a+x>>2]|0}if(j){l=e[a>>2]|0;for(x=4;(x|0)<=(d|0);x=x+4|0){h=e[a+x>>2]|0;e[a+x-4>>2]=h<<(32-j|0)|l>>>j;l=h}e[a+d>>2]=l>>>j}}function t(a,b,c,d,g,l){a=a|0;b=b|0;c=c|0;d=d|0;g=g|0;l=l|0;var n=0,o=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0;n=h(d<<1)|0;k(d<<1,0,n);j(b,a,n);for(z=0;(z|0)<(d|0);z=z+4|0){q=e[n+z>>2]|0,r=q&65535,q=q>>>16;t=g>>>16,s=g&65535;u=f(r,s)|0,v=((f(r,t)|0)+(f(q,s)|0)|0)+(u>>>16)|0;r=u&65535,q=v&65535;y=0;for(A=0;(A|0)<(d|0);A=A+4|0){B=z+A|0;t=e[c+A>>2]|0,s=t&65535,t=t>>>16;x=e[n+B>>2]|0;u=((f(r,s)|0)+(y&65535)|0)+(x&65535)|0;v=((f(r,t)|0)+(y>>>16)|0)+(x>>>16)|0;w=((f(q,s)|0)+(v&65535)|0)+(u>>>16)|0;y=((f(q,t)|0)+(w>>>16)|0)+(v>>>16)|0;x=w<<16|u&65535;e[n+B>>2]=x}B=z+A|0;x=e[n+B>>2]|0;u=((x&65535)+(y&65535)|0)+o|0;v=((x>>>16)+(y>>>16)|0)+(u>>>16)|0;e[n+B>>2]=v<<16|u&65535;o=v>>>16}j(d,n+d|0,l);i(d<<1);if(o|(m(c,d,l,d)|0)<=0){p(l,d,c,d,l,d)|0}}return{sreset:g,salloc:h,sfree:i,z:k,tst:n,neg:l,cmp:m,add:o,sub:p,mul:q,sqr:r,div:s,mredc:t}}function $a(a){return a instanceof _a}function _a(a){var b=Tc,c=0,d=0;if(n(a)&&(a=f(a)),o(a)&&(a=new Uint8Array(a)),void 0===a);else if(m(a)){var e=Math.abs(a);e>4294967295?(b=new Uint32Array(2),b[0]=0|e,b[1]=e/4294967296|0,c=52):e>0?(b=new Uint32Array(1),b[0]=e,c=32):(b=Tc,c=0),d=0>a?-1:1}else if(p(a)){if(c=8*a.length,!c)return Vc;b=new Uint32Array(c+31>>5);for(var g=a.length-4;g>=0;g-=4)b[a.length-4-g>>2]=a[g]<<24|a[g+1]<<16|a[g+2]<<8|a[g+3];-3===g?b[b.length-1]=a[0]:-2===g?b[b.length-1]=a[0]<<8|a[1]:-1===g&&(b[b.length-1]=a[0]<<16|a[1]<<8|a[2]),d=1}else{if("object"!=typeof a||null===a)throw new TypeError("number is of unexpected type");b=new Uint32Array(a.limbs),c=a.bitLength,d=a.sign}this.limbs=b,this.bitLength=c,this.sign=d}function ab(a){a=a||16;var b=this.limbs,c=this.bitLength,e="";if(16!==a)throw new d("bad radix");for(var f=(c+31>>5)-1;f>=0;f--){var g=b[f].toString(16);e+="00000000".substr(g.length),e+=g}return e=e.replace(/^0+/,""),e.length||(e="0"),this.sign<0&&(e="-"+e),e}function bb(){var a=this.bitLength,b=this.limbs;if(0===a)return new Uint8Array(0);for(var c=a+7>>3,d=new Uint8Array(c),e=0;c>e;e++){var f=c-e-1;d[e]=b[f>>2]>>((3&f)<<3)}return d}function cb(){var a=this.limbs,b=this.bitLength,c=this.sign;if(!c)return 0;if(32>=b)return c*(a[0]>>>0);if(52>=b)return c*(4294967296*(a[1]>>>0)+(a[0]>>>0));var d,e,f=0;for(d=a.length-1;d>=0;d--)if(0!==(e=a[d])){for(;0===(e<<f&2147483648);)f++;break}return 0===d?c*(a[0]>>>0):c*(1048576*((a[d]<<f|(f?a[d-1]>>>32-f:0))>>>0)+((a[d-1]<<f|(f&&d>1?a[d-2]>>>32-f:0))>>>12))*Math.pow(2,32*d-f-52)}function db(a){var b=this.limbs,c=this.bitLength;if(a>=c)return this;var d=new _a,e=a+31>>5,f=a%32;return d.limbs=new Uint32Array(b.subarray(0,e)),d.bitLength=a,d.sign=this.sign,f&&(d.limbs[e-1]&=-1>>>32-f),d}function eb(a,b){if(!m(a))throw new TypeError("TODO");if(void 0!==b&&!m(b))throw new TypeError("TODO");var c=this.limbs,d=this.bitLength;if(0>a)throw new RangeError("TODO");if(a>=d)return Vc;(void 0===b||b>d-a)&&(b=d-a);var e,f=new _a,g=a>>5,h=a+b+31>>5,i=b+31>>5,j=a%32,k=b%32;if(e=new Uint32Array(i),j){for(var l=0;h-g-1>l;l++)e[l]=c[g+l]>>>j|c[g+l+1]<<32-j;e[l]=c[g+l]>>>j}else e.set(c.subarray(g,h));return k&&(e[i-1]&=-1>>>32-k),f.limbs=e,f.bitLength=b,f.sign=this.sign,f}function fb(){var a=new _a;return a.limbs=this.limbs,a.bitLength=this.bitLength,a.sign=-1*this.sign,a}function gb(a){$a(a)||(a=new _a(a));var b=this.limbs,c=b.length,d=a.limbs,e=d.length,f=0;return this.sign<a.sign?-1:this.sign>a.sign?1:(Sc.set(b,0),Sc.set(d,c),f=Za.cmp(0,c<<2,c<<2,e<<2),f*this.sign)}function hb(a){
if($a(a)||(a=new _a(a)),!this.sign)return a;if(!a.sign)return this;var b,c,d,e,f=this.bitLength,g=this.limbs,h=g.length,i=this.sign,j=a.bitLength,k=a.limbs,l=k.length,m=a.sign,n=new _a;b=(f>j?f:j)+(i*m>0?1:0),c=b+31>>5,Za.sreset();var o=Za.salloc(h<<2),p=Za.salloc(l<<2),q=Za.salloc(c<<2);return Za.z(q-o+(c<<2),0,o),Sc.set(g,o>>2),Sc.set(k,p>>2),i*m>0?(Za.add(o,h<<2,p,l<<2,q,c<<2),d=i):i>m?(e=Za.sub(o,h<<2,p,l<<2,q,c<<2),d=e?m:i):(e=Za.sub(p,l<<2,o,h<<2,q,c<<2),d=e?i:m),e&&Za.neg(q,c<<2,q,c<<2),0===Za.tst(q,c<<2)?Vc:(n.limbs=new Uint32Array(Sc.subarray(q>>2,(q>>2)+c)),n.bitLength=b,n.sign=d,n)}function ib(a){return $a(a)||(a=new _a(a)),this.add(a.negate())}function jb(a){if($a(a)||(a=new _a(a)),!this.sign||!a.sign)return Vc;var b,c,d=this.bitLength,e=this.limbs,f=e.length,g=a.bitLength,h=a.limbs,i=h.length,j=new _a;b=d+g,c=b+31>>5,Za.sreset();var k=Za.salloc(f<<2),l=Za.salloc(i<<2),m=Za.salloc(c<<2);return Za.z(m-k+(c<<2),0,k),Sc.set(e,k>>2),Sc.set(h,l>>2),Za.mul(k,f<<2,l,i<<2,m,c<<2),j.limbs=new Uint32Array(Sc.subarray(m>>2,(m>>2)+c)),j.sign=this.sign*a.sign,j.bitLength=b,j}function kb(){if(!this.sign)return Vc;var a,b,c=this.bitLength,d=this.limbs,e=d.length,f=new _a;a=c<<1,b=a+31>>5,Za.sreset();var g=Za.salloc(e<<2),h=Za.salloc(b<<2);return Za.z(h-g+(b<<2),0,g),Sc.set(d,g>>2),Za.sqr(g,e<<2,h),f.limbs=new Uint32Array(Sc.subarray(h>>2,(h>>2)+b)),f.bitLength=a,f.sign=1,f}function lb(a){$a(a)||(a=new _a(a));var b,c,d=this.bitLength,e=this.limbs,f=e.length,g=a.bitLength,h=a.limbs,i=h.length,j=Vc,k=Vc;Za.sreset();var l=Za.salloc(f<<2),m=Za.salloc(i<<2),n=Za.salloc(f<<2);return Za.z(n-l+(f<<2),0,l),Sc.set(e,l>>2),Sc.set(h,m>>2),Za.div(l,f<<2,m,i<<2,n),b=Za.tst(n,f<<2)>>2,b&&(j=new _a,j.limbs=new Uint32Array(Sc.subarray(n>>2,(n>>2)+b)),j.bitLength=b<<5>d?d:b<<5,j.sign=this.sign*a.sign),c=Za.tst(l,i<<2)>>2,c&&(k=new _a,k.limbs=new Uint32Array(Sc.subarray(l>>2,(l>>2)+c)),k.bitLength=c<<5>g?g:c<<5,k.sign=this.sign),{quotient:j,remainder:k}}function mb(a,b){var c,d,e,f,g=0>a?-1:1,h=0>b?-1:1,i=1,j=0,k=0,l=1;for(a*=g,b*=h,f=b>a,f&&(e=a,a=b,b=e,e=g,g=h,h=e),d=Math.floor(a/b),c=a-d*b;c;)e=i-d*j,i=j,j=e,e=k-d*l,k=l,l=e,a=b,b=c,d=Math.floor(a/b),c=a-d*b;return j*=g,l*=h,f&&(e=j,j=l,l=e),{gcd:b,x:j,y:l}}function nb(a,b){$a(a)||(a=new _a(a)),$a(b)||(b=new _a(b));var c=a.sign,d=b.sign;0>c&&(a=a.negate()),0>d&&(b=b.negate());var e=a.compare(b);if(0>e){var f=a;a=b,b=f,f=c,c=d,d=f}var g,h,i,j=Wc,k=Vc,l=b.bitLength,m=Vc,n=Wc,o=a.bitLength;for(g=a.divide(b);(h=g.remainder)!==Vc;)i=g.quotient,g=j.subtract(i.multiply(k).clamp(l)).clamp(l),j=k,k=g,g=m.subtract(i.multiply(n).clamp(o)).clamp(o),m=n,n=g,a=b,b=h,g=a.divide(b);if(0>c&&(k=k.negate()),0>d&&(n=n.negate()),0>e){var f=k;k=n,n=f}return{gcd:b,x:k,y:n}}function ob(){if(_a.apply(this,arguments),this.valueOf()<1)throw new RangeError;if(!(this.bitLength<=32)){var a;if(1&this.limbs[0]){var b=(this.bitLength+31&-32)+1,c=new Uint32Array(b+31>>5);c[c.length-1]=1,a=new _a,a.sign=1,a.bitLength=b,a.limbs=c;var d=mb(4294967296,this.limbs[0]).y;this.coefficient=0>d?-d:4294967296-d,this.comodulus=a,this.comodulusRemainder=a.divide(this).remainder,this.comodulusRemainderSquare=a.square().divide(this).remainder}}}function pb(a){return $a(a)||(a=new _a(a)),a.bitLength<=32&&this.bitLength<=32?new _a(a.valueOf()%this.valueOf()):a.compare(this)<0?a:a.divide(this).remainder}function qb(a){a=this.reduce(a);var b=nb(this,a);return 1!==b.gcd.valueOf()?null:(b=b.y,b.sign<0&&(b=b.add(this).clamp(this.bitLength)),b)}function rb(a,b){$a(a)||(a=new _a(a)),$a(b)||(b=new _a(b));for(var c=0,d=0;d<b.limbs.length;d++)for(var e=b.limbs[d];e;)1&e&&c++,e>>>=1;var f=8;b.bitLength<=4536&&(f=7),b.bitLength<=1736&&(f=6),b.bitLength<=630&&(f=5),b.bitLength<=210&&(f=4),b.bitLength<=60&&(f=3),b.bitLength<=12&&(f=2),1<<f-1>=c&&(f=1),a=sb(this.reduce(a).multiply(this.comodulusRemainderSquare),this);var g=sb(a.square(),this),h=new Array(1<<f-1);h[0]=a,h[1]=sb(a.multiply(g),this);for(var d=2;1<<f-1>d;d++)h[d]=sb(h[d-1].multiply(g),this);for(var i=this.comodulusRemainder,j=i,d=b.limbs.length-1;d>=0;d--)for(var e=b.limbs[d],k=32;k>0;)if(2147483648&e){for(var l=e>>>32-f,m=f;0===(1&l);)l>>>=1,m--;for(var n=h[l>>>1];l;)l>>>=1,j!==i&&(j=sb(j.square(),this));j=j!==i?sb(j.multiply(n),this):n,e<<=m,k-=m}else j!==i&&(j=sb(j.square(),this)),e<<=1,k--;return j=sb(j,this)}function sb(a,b){var c=a.limbs,d=c.length,e=b.limbs,f=e.length,g=b.coefficient;Za.sreset();var h=Za.salloc(d<<2),i=Za.salloc(f<<2),j=Za.salloc(f<<2);Za.z(j-h+(f<<2),0,h),Sc.set(c,h>>2),Sc.set(e,i>>2),Za.mredc(h,d<<2,i,f<<2,g,j);var k=new _a;return k.limbs=new Uint32Array(Sc.subarray(j>>2,(j>>2)+f)),k.bitLength=b.bitLength,k.sign=1,k}function tb(a){var b=new _a(this),c=0;for(b.limbs[0]-=1;0===b.limbs[c>>5];)c+=32;for(;0===(b.limbs[c>>5]>>(31&c)&1);)c++;b=b.slice(c);for(var d=new ob(this),e=this.subtract(Wc),f=new _a(this),g=this.limbs.length-1;0===f.limbs[g];)g--;for(;--a>=0;){for(Wa(f.limbs),f.limbs[0]<2&&(f.limbs[0]+=2);f.compare(e)>=0;)f.limbs[g]>>>=1;var h=d.power(f,b);if(0!==h.compare(Wc)&&0!==h.compare(e)){for(var i=c;--i>0;){if(h=h.square().divide(d).remainder,0===h.compare(Wc))return!1;if(0===h.compare(e))break}if(0===i)return!1}}return!0}function ub(a){a=a||80;var b=this.limbs,c=0;if(0===(1&b[0]))return!1;if(1>=a)return!0;var d=0,e=0,f=0;for(c=0;c<b.length;c++){for(var g=b[c];g;)d+=3&g,g>>>=2;for(var h=b[c];h;)e+=3&h,h>>>=2,e-=3&h,h>>>=2;for(var i=b[c];i;)f+=15&i,i>>>=4,f-=15&i,i>>>=4}return d%3&&e%5&&f%17?2>=a?!0:tb.call(this,a>>>1):!1}function vb(a){if(Yc.length>=a)return Yc.slice(0,a);for(var b=Yc[Yc.length-1]+2;Yc.length<a;b+=2){for(var c=0,d=Yc[c];b>=d*d&&b%d!=0;d=Yc[++c]);d*d>b&&Yc.push(b)}return Yc}function wb(a,c){var d=a+31>>5,e=new _a({sign:1,bitLength:a,limbs:d}),f=e.limbs,g=1e4;512>=a&&(g=2200),256>=a&&(g=600);var h=vb(g),i=new Uint32Array(g),j=a*b.Math.LN2|0,k=27;for(a>=250&&(k=12),a>=450&&(k=6),a>=850&&(k=3),a>=1300&&(k=2);;){Wa(f),f[0]|=1,f[d-1]|=1<<(a-1&31),31&a&&(f[d-1]&=l(a+1&31)-1),i[0]=1;for(var m=1;g>m;m++)i[m]=e.divide(h[m]).remainder.valueOf();a:for(var n=0;j>n;n+=2,f[0]+=2){for(var m=1;g>m;m++)if((i[m]+n)%h[m]===0)continue a;if(("function"!=typeof c||c(e))&&tb.call(e,k))return e}}}function xb(a){a=a||{},this.key=null,this.result=null,this.reset(a)}function yb(a){a=a||{},this.result=null;var b=a.key;if(void 0!==b){if(!(b instanceof Array))throw new TypeError("unexpected key type");var c=b.length;if(2!==c&&3!==c&&8!==c)throw new SyntaxError("unexpected key type");var d=[];d[0]=new ob(b[0]),d[1]=new _a(b[1]),c>2&&(d[2]=new _a(b[2])),c>3&&(d[3]=new ob(b[3]),d[4]=new ob(b[4]),d[5]=new _a(b[5]),d[6]=new _a(b[6]),d[7]=new _a(b[7])),this.key=d}return this}function zb(a){if(!this.key)throw new c("no key is associated with the instance");n(a)&&(a=f(a)),o(a)&&(a=new Uint8Array(a));var b;if(p(a))b=new _a(a);else{if(!$a(a))throw new TypeError("unexpected data type");b=a}if(this.key[0].compare(b)<=0)throw new RangeError("data too large");var d=this.key[0],e=this.key[1],g=d.power(b,e).toBytes(),h=d.bitLength+7>>3;if(g.length<h){var i=new Uint8Array(h);i.set(g,h-g.length),g=i}return this.result=g,this}function Ab(a){if(!this.key)throw new c("no key is associated with the instance");if(this.key.length<3)throw new c("key isn't suitable for decription");n(a)&&(a=f(a)),o(a)&&(a=new Uint8Array(a));var b;if(p(a))b=new _a(a);else{if(!$a(a))throw new TypeError("unexpected data type");b=a}if(this.key[0].compare(b)<=0)throw new RangeError("data too large");var d;if(this.key.length>3){for(var e=this.key[0],g=this.key[3],h=this.key[4],i=this.key[5],j=this.key[6],k=this.key[7],l=g.power(b,i),m=h.power(b,j),q=l.subtract(m);q.sign<0;)q=q.add(g);var r=g.reduce(k.multiply(q));d=r.multiply(h).add(m).clamp(e.bitLength).toBytes()}else{var e=this.key[0],s=this.key[2];d=e.power(b,s).toBytes()}var t=e.bitLength+7>>3;if(d.length<t){var u=new Uint8Array(t);u.set(d,t-d.length),d=u}return this.result=d,this}function Bb(a,b){if(a=a||2048,b=b||65537,512>a)throw new d("bit length is too small");if(n(b)&&(b=f(b)),o(b)&&(b=new Uint8Array(b)),!(p(b)||m(b)||$a(b)))throw new TypeError("unexpected exponent type");if(b=new _a(b),0===(1&b.limbs[0]))throw new d("exponent must be an odd number");var c,b,e,g,h,i,j,k,l,q;g=wb(a>>1,function(a){return i=new _a(a),i.limbs[0]-=1,1==nb(i,b).gcd.valueOf()}),h=wb(a-(a>>1),function(d){return c=new ob(g.multiply(d)),c.limbs[(a+31>>5)-1]>>>(a-1&31)?(j=new _a(d),j.limbs[0]-=1,1==nb(j,b).gcd.valueOf()):!1}),e=new ob(i.multiply(j)).inverse(b),k=e.divide(i).remainder,l=e.divide(j).remainder,g=new ob(g),h=new ob(h);var q=g.inverse(h);return[c,b,e,g,h,k,l,q]}function Cb(a){if(a=a||{},!a.hash)throw new SyntaxError("option 'hash' is required");if(!a.hash.HASH_SIZE)throw new SyntaxError("option 'hash' supplied doesn't seem to be a valid hash function");this.hash=a.hash,this.label=null,this.reset(a)}function Db(a){a=a||{};var b=a.label;if(void 0!==b){if(o(b)||p(b))b=new Uint8Array(b);else{if(!n(b))throw new TypeError("unexpected label type");b=f(b)}this.label=b.length>0?b:null}else this.label=null;yb.call(this,a)}function Eb(a){if(!this.key)throw new c("no key is associated with the instance");var b=Math.ceil(this.key[0].bitLength/8),e=this.hash.HASH_SIZE,g=a.byteLength||a.length||0,h=b-g-2*e-2;if(g>b-2*this.hash.HASH_SIZE-2)throw new d("data too large");var i=new Uint8Array(b),j=i.subarray(1,e+1),k=i.subarray(e+1);if(p(a))k.set(a,e+h+1);else if(o(a))k.set(new Uint8Array(a),e+h+1);else{if(!n(a))throw new TypeError("unexpected data type");k.set(f(a),e+h+1)}k.set(this.hash.reset().process(this.label||"").finish().result,0),k[e+h]=1,Wa(j);for(var l=Gb.call(this,j,k.length),m=0;m<k.length;m++)k[m]^=l[m];for(var q=Gb.call(this,k,j.length),m=0;m<j.length;m++)j[m]^=q[m];return zb.call(this,i),this}function Fb(a){if(!this.key)throw new c("no key is associated with the instance");var b=Math.ceil(this.key[0].bitLength/8),f=this.hash.HASH_SIZE,g=a.byteLength||a.length||0;if(g!==b)throw new d("bad data");Ab.call(this,a);var h=this.result[0],i=this.result.subarray(1,f+1),j=this.result.subarray(f+1);if(0!==h)throw new e("decryption failed");for(var k=Gb.call(this,j,i.length),l=0;l<i.length;l++)i[l]^=k[l];for(var m=Gb.call(this,i,j.length),l=0;l<j.length;l++)j[l]^=m[l];for(var n=this.hash.reset().process(this.label||"").finish().result,l=0;f>l;l++)if(n[l]!==j[l])throw new e("decryption failed");for(var o=f;o<j.length;o++){var p=j[o];if(1===p)break;if(0!==p)throw new e("decryption failed")}if(o===j.length)throw new e("decryption failed");return this.result=j.subarray(o+1),this}function Gb(a,b){a=a||"",b=b||0;for(var c=this.hash.HASH_SIZE,d=new Uint8Array(b),e=new Uint8Array(4),f=Math.ceil(b/c),g=0;f>g;g++){e[0]=g>>>24,e[1]=g>>>16&255,e[2]=g>>>8&255,e[3]=255&g;var h=d.subarray(g*c),i=this.hash.reset().process(a).process(e).finish().result;i.length>h.length&&(i=i.subarray(0,h.length)),h.set(i)}return d}function Hb(a){if(a=a||{},!a.hash)throw new SyntaxError("option 'hash' is required");if(!a.hash.HASH_SIZE)throw new SyntaxError("option 'hash' supplied doesn't seem to be a valid hash function");this.hash=a.hash,this.saltLength=4,this.reset(a)}function Ib(a){a=a||{},yb.call(this,a);var b=a.saltLength;if(void 0!==b){if(!m(b)||0>b)throw new TypeError("saltLength should be a non-negative number");if(null!==this.key&&Math.ceil((this.key[0].bitLength-1)/8)<this.hash.HASH_SIZE+b+2)throw new SyntaxError("saltLength is too large");this.saltLength=b}else this.saltLength=4}function Jb(a){if(!this.key)throw new c("no key is associated with the instance");var b=this.key[0].bitLength,d=this.hash.HASH_SIZE,e=Math.ceil((b-1)/8),f=this.saltLength,g=e-f-d-2,h=new Uint8Array(e),i=h.subarray(e-d-1,e-1),j=h.subarray(0,e-d-1),k=j.subarray(g+1),l=new Uint8Array(8+d+f),m=l.subarray(8,8+d),n=l.subarray(8+d);m.set(this.hash.reset().process(a).finish().result),f>0&&Wa(n),j[g]=1,k.set(n),i.set(this.hash.reset().process(l).finish().result);for(var o=Gb.call(this,i,j.length),p=0;p<j.length;p++)j[p]^=o[p];h[e-1]=188;var q=8*e-b+1;return q%8&&(h[0]&=255>>>q),Ab.call(this,h),this}function Kb(a,b){if(!this.key)throw new c("no key is associated with the instance");var d=this.key[0].bitLength,f=this.hash.HASH_SIZE,g=Math.ceil((d-1)/8),h=this.saltLength,i=g-h-f-2;zb.call(this,a);var j=this.result;if(188!==j[g-1])throw new e("bad signature");var k=j.subarray(g-f-1,g-1),l=j.subarray(0,g-f-1),m=l.subarray(i+1),n=8*g-d+1;if(n%8&&j[0]>>>8-n)throw new e("bad signature");for(var o=Gb.call(this,k,l.length),p=0;p<l.length;p++)l[p]^=o[p];n%8&&(j[0]&=255>>>n);for(var p=0;i>p;p++)if(0!==l[p])throw new e("bad signature");if(1!==l[i])throw new e("bad signature");var q=new Uint8Array(8+f+h),r=q.subarray(8,8+f),s=q.subarray(8+f);r.set(this.hash.reset().process(b).finish().result),s.set(m);for(var t=this.hash.reset().process(q).finish().result,p=0;f>p;p++)if(k[p]!==t[p])throw new e("bad signature");return this}function Lb(a,b){if(void 0===a)throw new SyntaxError("bitlen required");if(void 0===b)throw new SyntaxError("e required");for(var c=Bb(a,b),d=0;d<c.length;d++)$a(c[d])&&(c[d]=c[d].toBytes());return c}function Mb(a,b,c){if(void 0===a)throw new SyntaxError("data required");if(void 0===b)throw new SyntaxError("key required");return new Cb({hash:ba(),key:b,label:c}).encrypt(a).result}function Nb(a,b,c){if(void 0===a)throw new SyntaxError("data required");if(void 0===b)throw new SyntaxError("key required");return new Cb({hash:ba(),key:b,label:c}).decrypt(a).result}function Ob(a,b,c){if(void 0===a)throw new SyntaxError("data required");if(void 0===b)throw new SyntaxError("key required");return new Cb({hash:ha(),key:b,label:c}).encrypt(a).result}function Pb(a,b,c){if(void 0===a)throw new SyntaxError("data required");if(void 0===b)throw new SyntaxError("key required");return new Cb({hash:ha(),key:b,label:c}).decrypt(a).result}function Qb(a,b,c){if(void 0===a)throw new SyntaxError("data required");if(void 0===b)throw new SyntaxError("key required");return new Hb({hash:ba(),key:b,saltLength:c}).sign(a).result}function Rb(a,b,c,d){if(void 0===a)throw new SyntaxError("signature required");if(void 0===b)throw new SyntaxError("data required");if(void 0===c)throw new SyntaxError("key required");try{return new Hb({hash:ba(),key:c,saltLength:d}).verify(a,b),!0}catch(a){if(!(a instanceof e))throw a}return!1}function Sb(a,b,c){if(void 0===a)throw new SyntaxError("data required");if(void 0===b)throw new SyntaxError("key required");return new Hb({hash:ha(),key:b,saltLength:c}).sign(a).result}function Tb(a,b,c,d){if(void 0===a)throw new SyntaxError("signature required");if(void 0===b)throw new SyntaxError("data required");if(void 0===c)throw new SyntaxError("key required");try{return new Hb({hash:ha(),key:c,saltLength:d}).verify(a,b),!0}catch(a){if(!(a instanceof e))throw a}return!1}c.prototype=Object.create(Error.prototype,{name:{value:"IllegalStateError"}}),d.prototype=Object.create(Error.prototype,{name:{value:"IllegalArgumentError"}}),e.prototype=Object.create(Error.prototype,{name:{value:"SecurityError"}});var Ub=b.Float64Array||b.Float32Array,Vb=b.console,Wb=!b.location.protocol.search(/https:|file:|chrome:|chrome-extension:/);Wb||void 0===Vb||Vb.warn("asmCrypto seems to be load from an insecure origin; this may cause to MitM-attack vulnerability. Consider using secure transport protocol."),a.string_to_bytes=f,a.hex_to_bytes=g,a.base64_to_bytes=h,a.bytes_to_string=i,a.bytes_to_hex=j,a.bytes_to_base64=k,b.IllegalStateError=c,b.IllegalArgumentError=d,b.SecurityError=e;var Xb=function(){"use strict";function a(){e=[],f=[];var a,b,c=1;for(a=0;255>a;a++)e[a]=c,b=128&c,c<<=1,c&=255,128===b&&(c^=27),c^=e[a],f[e[a]]=a;e[255]=e[0],f[0]=0,k=!0}function b(a,b){var c=e[(f[a]+f[b])%255];return(0===a||0===b)&&(c=0),c}function c(a){var b=e[255-f[a]];return 0===a&&(b=0),b}function d(){function d(a){var b,d,e;for(d=e=c(a),b=0;4>b;b++)d=255&(d<<1|d>>>7),e^=d;return e^=99}k||a(),g=[],h=[],i=[[],[],[],[]],j=[[],[],[],[]];for(var e=0;256>e;e++){var f=d(e);g[e]=f,h[f]=e,i[0][e]=b(2,f)<<24|f<<16|f<<8|b(3,f),j[0][f]=b(14,e)<<24|b(9,e)<<16|b(13,e)<<8|b(11,e);for(var l=1;4>l;l++)i[l][e]=i[l-1][e]>>>8|i[l-1][e]<<24,j[l][f]=j[l-1][f]>>>8|j[l-1][f]<<24}}var e,f,g,h,i,j,k=!1,l=!1,m=function(a,b,c){function e(a,b,c,d,e,h,i,k,l){var n=f.subarray(0,60),o=f.subarray(256,316);n.set([b,c,d,e,h,i,k,l]);for(var p=a,q=1;4*a+28>p;p++){var r=n[p-1];(p%a===0||8===a&&p%a===4)&&(r=g[r>>>24]<<24^g[r>>>16&255]<<16^g[r>>>8&255]<<8^g[255&r]),p%a===0&&(r=r<<8^r>>>24^q<<24,q=q<<1^(128&q?27:0)),n[p]=n[p-a]^r}for(var s=0;p>s;s+=4)for(var t=0;4>t;t++){var r=n[p-(4+s)+(4-t)%4];4>s||s>=p-4?o[s+t]=r:o[s+t]=j[0][g[r>>>24]]^j[1][g[r>>>16&255]]^j[2][g[r>>>8&255]]^j[3][g[255&r]]}m.set_rounds(a+5)}l||d();var f=new Uint32Array(c);f.set(g,512),f.set(h,768);for(var k=0;4>k;k++)f.set(i[k],4096+1024*k>>2),f.set(j[k],8192+1024*k>>2);var m=function(a,b,c){"use asm";var d=0,e=0,f=0,g=0,h=0,i=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0;var y=new a.Uint32Array(c),z=new a.Uint8Array(c);function A(a,b,c,h,i,j,k,l){a=a|0;b=b|0;c=c|0;h=h|0;i=i|0;j=j|0;k=k|0;l=l|0;var m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0;m=c|1024,n=c|2048,o=c|3072;i=i^y[(a|0)>>2],j=j^y[(a|4)>>2],k=k^y[(a|8)>>2],l=l^y[(a|12)>>2];for(t=16;(t|0)<=h<<4;t=t+16|0){p=y[(c|i>>22&1020)>>2]^y[(m|j>>14&1020)>>2]^y[(n|k>>6&1020)>>2]^y[(o|l<<2&1020)>>2]^y[(a|t|0)>>2],q=y[(c|j>>22&1020)>>2]^y[(m|k>>14&1020)>>2]^y[(n|l>>6&1020)>>2]^y[(o|i<<2&1020)>>2]^y[(a|t|4)>>2],r=y[(c|k>>22&1020)>>2]^y[(m|l>>14&1020)>>2]^y[(n|i>>6&1020)>>2]^y[(o|j<<2&1020)>>2]^y[(a|t|8)>>2],s=y[(c|l>>22&1020)>>2]^y[(m|i>>14&1020)>>2]^y[(n|j>>6&1020)>>2]^y[(o|k<<2&1020)>>2]^y[(a|t|12)>>2];i=p,j=q,k=r,l=s}d=y[(b|i>>22&1020)>>2]<<24^y[(b|j>>14&1020)>>2]<<16^y[(b|k>>6&1020)>>2]<<8^y[(b|l<<2&1020)>>2]^y[(a|t|0)>>2],e=y[(b|j>>22&1020)>>2]<<24^y[(b|k>>14&1020)>>2]<<16^y[(b|l>>6&1020)>>2]<<8^y[(b|i<<2&1020)>>2]^y[(a|t|4)>>2],f=y[(b|k>>22&1020)>>2]<<24^y[(b|l>>14&1020)>>2]<<16^y[(b|i>>6&1020)>>2]<<8^y[(b|j<<2&1020)>>2]^y[(a|t|8)>>2],g=y[(b|l>>22&1020)>>2]<<24^y[(b|i>>14&1020)>>2]<<16^y[(b|j>>6&1020)>>2]<<8^y[(b|k<<2&1020)>>2]^y[(a|t|12)>>2]}function B(a,b,c,d){a=a|0;b=b|0;c=c|0;d=d|0;A(0,2048,4096,x,a,b,c,d)}function C(a,b,c,d){a=a|0;b=b|0;c=c|0;d=d|0;var f=0;A(1024,3072,8192,x,a,d,c,b);f=e,e=g,g=f}function D(a,b,c,l){a=a|0;b=b|0;c=c|0;l=l|0;A(0,2048,4096,x,h^a,i^b,j^c,k^l);h=d,i=e,j=f,k=g}function E(a,b,c,l){a=a|0;b=b|0;c=c|0;l=l|0;var m=0;A(1024,3072,8192,x,a,l,c,b);m=e,e=g,g=m;d=d^h,e=e^i,f=f^j,g=g^k;h=a,i=b,j=c,k=l}function F(a,b,c,l){a=a|0;b=b|0;c=c|0;l=l|0;A(0,2048,4096,x,h,i,j,k);h=d=d^a,i=e=e^b,j=f=f^c,k=g=g^l}function G(a,b,c,l){a=a|0;b=b|0;c=c|0;l=l|0;A(0,2048,4096,x,h,i,j,k);d=d^a,e=e^b,f=f^c,g=g^l;h=a,i=b,j=c,k=l}function H(a,b,c,l){a=a|0;b=b|0;c=c|0;l=l|0;A(0,2048,4096,x,h,i,j,k);h=d,i=e,j=f,k=g;d=d^a,e=e^b,f=f^c,g=g^l}function I(a,b,c,h){a=a|0;b=b|0;c=c|0;h=h|0;A(0,2048,4096,x,l,m,n,o);o=~s&o|s&o+1,n=~r&n|r&n+((o|0)==0),m=~q&m|q&m+((n|0)==0),l=~p&l|p&l+((m|0)==0);d=d^a,e=e^b,f=f^c,g=g^h}function J(a,b,c,d){a=a|0;b=b|0;c=c|0;d=d|0;var e=0,f=0,g=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0;a=a^h,b=b^i,c=c^j,d=d^k;e=t|0,f=u|0,g=v|0,l=w|0;for(;(q|0)<128;q=q+1|0){if(e>>>31){m=m^a,n=n^b,o=o^c,p=p^d}e=e<<1|f>>>31,f=f<<1|g>>>31,g=g<<1|l>>>31,l=l<<1;r=d&1;d=d>>>1|c<<31,c=c>>>1|b<<31,b=b>>>1|a<<31,a=a>>>1;if(r)a=a^3774873600}h=m,i=n,j=o,k=p}function K(a){a=a|0;x=a}function L(a,b,c,h){a=a|0;b=b|0;c=c|0;h=h|0;d=a,e=b,f=c,g=h}function M(a,b,c,d){a=a|0;b=b|0;c=c|0;d=d|0;h=a,i=b,j=c,k=d}function N(a,b,c,d){a=a|0;b=b|0;c=c|0;d=d|0;l=a,m=b,n=c,o=d}function O(a,b,c,d){a=a|0;b=b|0;c=c|0;d=d|0;p=a,q=b,r=c,s=d}function P(a,b,c,d){a=a|0;b=b|0;c=c|0;d=d|0;o=~s&o|s&d,n=~r&n|r&c,m=~q&m|q&b,l=~p&l|p&a}function Q(a){a=a|0;if(a&15)return-1;z[a|0]=d>>>24,z[a|1]=d>>>16&255,z[a|2]=d>>>8&255,z[a|3]=d&255,z[a|4]=e>>>24,z[a|5]=e>>>16&255,z[a|6]=e>>>8&255,z[a|7]=e&255,z[a|8]=f>>>24,z[a|9]=f>>>16&255,z[a|10]=f>>>8&255,z[a|11]=f&255,z[a|12]=g>>>24,z[a|13]=g>>>16&255,z[a|14]=g>>>8&255,z[a|15]=g&255;return 16}function R(a){a=a|0;if(a&15)return-1;z[a|0]=h>>>24,z[a|1]=h>>>16&255,z[a|2]=h>>>8&255,z[a|3]=h&255,z[a|4]=i>>>24,z[a|5]=i>>>16&255,z[a|6]=i>>>8&255,z[a|7]=i&255,z[a|8]=j>>>24,z[a|9]=j>>>16&255,z[a|10]=j>>>8&255,z[a|11]=j&255,z[a|12]=k>>>24,z[a|13]=k>>>16&255,z[a|14]=k>>>8&255,z[a|15]=k&255;return 16}function S(){B(0,0,0,0);t=d,u=e,v=f,w=g}function T(a,b,c){a=a|0;b=b|0;c=c|0;var h=0;if(b&15)return-1;while((c|0)>=16){V[a&7](z[b|0]<<24|z[b|1]<<16|z[b|2]<<8|z[b|3],z[b|4]<<24|z[b|5]<<16|z[b|6]<<8|z[b|7],z[b|8]<<24|z[b|9]<<16|z[b|10]<<8|z[b|11],z[b|12]<<24|z[b|13]<<16|z[b|14]<<8|z[b|15]);z[b|0]=d>>>24,z[b|1]=d>>>16&255,z[b|2]=d>>>8&255,z[b|3]=d&255,z[b|4]=e>>>24,z[b|5]=e>>>16&255,z[b|6]=e>>>8&255,z[b|7]=e&255,z[b|8]=f>>>24,z[b|9]=f>>>16&255,z[b|10]=f>>>8&255,z[b|11]=f&255,z[b|12]=g>>>24,z[b|13]=g>>>16&255,z[b|14]=g>>>8&255,z[b|15]=g&255;h=h+16|0,b=b+16|0,c=c-16|0}return h|0}function U(a,b,c){a=a|0;b=b|0;c=c|0;var d=0;if(b&15)return-1;while((c|0)>=16){W[a&1](z[b|0]<<24|z[b|1]<<16|z[b|2]<<8|z[b|3],z[b|4]<<24|z[b|5]<<16|z[b|6]<<8|z[b|7],z[b|8]<<24|z[b|9]<<16|z[b|10]<<8|z[b|11],z[b|12]<<24|z[b|13]<<16|z[b|14]<<8|z[b|15]);d=d+16|0,b=b+16|0,c=c-16|0}return d|0}var V=[B,C,D,E,F,G,H,I];var W=[D,J];return{set_rounds:K,set_state:L,set_iv:M,set_nonce:N,set_mask:O,set_counter:P,get_state:Q,get_iv:R,gcm_init:S,cipher:T,mac:U}}(a,b,c);return m.set_key=e,m};return m.ENC={ECB:0,CBC:2,CFB:4,OFB:6,CTR:7},m.DEC={ECB:1,CBC:3,CFB:5,OFB:6,CTR:7},m.MAC={CBC:0,GCM:1},m.HEAP_DATA=16384,m}(),Yb=C.prototype;Yb.BLOCK_SIZE=16,Yb.reset=x,Yb.encrypt=z,Yb.decrypt=B;var Zb=D.prototype;Zb.BLOCK_SIZE=16,Zb.reset=x,Zb.process=y,Zb.finish=z;var $b=E.prototype;$b.BLOCK_SIZE=16,$b.reset=x,$b.process=A,$b.finish=B;var _b=F.prototype;_b.BLOCK_SIZE=16,_b.reset=I,_b.encrypt=z,_b.decrypt=z;var ac=G.prototype;ac.BLOCK_SIZE=16,ac.reset=I,ac.process=y,ac.finish=z;var bc=68719476704,cc=K.prototype;cc.BLOCK_SIZE=16,cc.reset=N,cc.encrypt=Q,cc.decrypt=T;var dc=L.prototype;dc.BLOCK_SIZE=16,dc.reset=N,dc.process=O,dc.finish=P;var ec=M.prototype;ec.BLOCK_SIZE=16,ec.reset=N,ec.process=R,ec.finish=S;var fc=new Uint8Array(1048576),gc=Xb(b,null,fc.buffer);a.AES_CBC=C,a.AES_CBC.encrypt=U,a.AES_CBC.decrypt=V,a.AES_CBC.Encrypt=D,a.AES_CBC.Decrypt=E,a.AES_GCM=K,a.AES_GCM.encrypt=W,a.AES_GCM.decrypt=X,a.AES_GCM.Encrypt=L,a.AES_GCM.Decrypt=M;var hc=64,ic=20;aa.BLOCK_SIZE=hc,aa.HASH_SIZE=ic;var jc=aa.prototype;jc.reset=Y,jc.process=Z,jc.finish=$;var kc=null;aa.bytes=ca,aa.hex=da,aa.base64=ea,a.SHA1=aa;var lc=64,mc=32;ga.BLOCK_SIZE=lc,ga.HASH_SIZE=mc;var nc=ga.prototype;nc.reset=Y,nc.process=Z,nc.finish=$;var oc=null;ga.bytes=ia,ga.hex=ja,ga.base64=ka,a.SHA256=ga;var pc=la.prototype;pc.reset=oa,pc.process=pa,pc.finish=qa,ra.BLOCK_SIZE=aa.BLOCK_SIZE,ra.HMAC_SIZE=aa.HASH_SIZE;var qc=ra.prototype;qc.reset=sa,qc.process=pa,qc.finish=ta;var rc=null;va.BLOCK_SIZE=ga.BLOCK_SIZE,va.HMAC_SIZE=ga.HASH_SIZE;var sc=va.prototype;sc.reset=wa,sc.process=pa,sc.finish=xa;var tc=null;a.HMAC=la,ra.bytes=za,ra.hex=Aa,ra.base64=Ba,a.HMAC_SHA1=ra,va.bytes=Ca,va.hex=Da,va.base64=Ea,a.HMAC_SHA256=va;var uc=Fa.prototype;uc.reset=Ga,uc.generate=Ha;var vc=Ia.prototype;vc.reset=Ga,vc.generate=Ja;var wc=null,xc=La.prototype;xc.reset=Ga,xc.generate=Ma;var yc=null;a.PBKDF2=a.PBKDF2_HMAC_SHA1={bytes:Oa,hex:Pa,base64:Qa},a.PBKDF2_HMAC_SHA256={bytes:Ra,hex:Sa,base64:Ta};var zc,Ac=function(){function a(){function a(){b^=d<<11,l=l+b|0,d=d+f|0,d^=f>>>2,m=m+d|0,f=f+l|0,f^=l<<8,n=n+f|0,l=l+m|0,l^=m>>>16,o=o+l|0,m=m+n|0,m^=n<<10,p=p+m|0,n=n+o|0,n^=o>>>4,b=b+n|0,o=o+p|0,o^=p<<8,d=d+o|0,p=p+b|0,p^=b>>>9,f=f+p|0,b=b+d|0}var b,d,f,l,m,n,o,p;h=i=j=0,b=d=f=l=m=n=o=p=2654435769;for(var q=0;4>q;q++)a();for(var q=0;256>q;q+=8)b=b+g[0|q]|0,d=d+g[1|q]|0,f=f+g[2|q]|0,l=l+g[3|q]|0,m=m+g[4|q]|0,n=n+g[5|q]|0,o=o+g[6|q]|0,p=p+g[7|q]|0,a(),e.set([b,d,f,l,m,n,o,p],q);for(var q=0;256>q;q+=8)b=b+e[0|q]|0,d=d+e[1|q]|0,f=f+e[2|q]|0,l=l+e[3|q]|0,m=m+e[4|q]|0,n=n+e[5|q]|0,o=o+e[6|q]|0,p=p+e[7|q]|0,a(),e.set([b,d,f,l,m,n,o,p],q);c(1),k=256}function b(b){var c,d,e,h,i;if(q(b))b=new Uint8Array(b.buffer);else if(m(b))h=new Ub(1),h[0]=b,b=new Uint8Array(h.buffer);else if(n(b))b=f(b);else{if(!o(b))throw new TypeError("bad seed type");b=new Uint8Array(b)}for(i=b.length,d=0;i>d;d+=1024){for(e=d,c=0;1024>c&&i>e;e=d|++c)g[c>>2]^=b[e]<<((3&c)<<3);a()}}function c(a){a=a||1;for(var b,c,d;a--;)for(j=j+1|0,i=i+j|0,b=0;256>b;b+=4)h^=h<<13,h=e[b+128&255]+h|0,c=e[0|b],e[0|b]=d=e[c>>>2&255]+(h+i|0)|0,g[0|b]=i=e[d>>>10&255]+c|0,h^=h>>>6,h=e[b+129&255]+h|0,c=e[1|b],e[1|b]=d=e[c>>>2&255]+(h+i|0)|0,g[1|b]=i=e[d>>>10&255]+c|0,h^=h<<2,h=e[b+130&255]+h|0,c=e[2|b],e[2|b]=d=e[c>>>2&255]+(h+i|0)|0,g[2|b]=i=e[d>>>10&255]+c|0,h^=h>>>16,h=e[b+131&255]+h|0,c=e[3|b],e[3|b]=d=e[c>>>2&255]+(h+i|0)|0,g[3|b]=i=e[d>>>10&255]+c|0}function d(){return k--||(c(1),k=255),g[k]}var e=new Uint32Array(256),g=new Uint32Array(256),h=0,i=0,j=0,k=0;return{seed:b,prng:c,rand:d}}(),Vb=b.console,Bc=b.Date.now,Cc=b.Math.random,Dc=b.performance,Ec=b.crypto||b.msCrypto;void 0!==Ec&&(zc=Ec.getRandomValues);var Fc,Gc=Ac.rand,Hc=Ac.seed,Ic=0,Jc=!1,Kc=!1,Lc=0,Mc=256,Nc=!1,Oc=!1,Pc={};if(void 0!==Dc)Fc=function(){return 1e3*Dc.now()|0};else{var Qc=1e3*Bc()|0;Fc=function(){return 1e3*Bc()-Qc|0}}a.random=Xa,a.random.seed=Va,Object.defineProperty(Xa,"allowWeak",{get:function(){return Nc},set:function(a){Nc=a}}),Object.defineProperty(Xa,"skipSystemRNGWarning",{get:function(){return Oc},set:function(a){Oc=a}}),a.getRandomValues=Wa,a.getRandomValues.seed=Va,Object.defineProperty(Wa,"allowWeak",{get:function(){return Nc},set:function(a){Nc=a}}),Object.defineProperty(Wa,"skipSystemRNGWarning",{get:function(){return Oc},set:function(a){Oc=a}}),b.Math.random=Xa,void 0===b.crypto&&(b.crypto={}),b.crypto.getRandomValues=Wa;var Rc;Rc=void 0===b.Math.imul?function(a,c,d){b.Math.imul=Ya;var e=Za(a,c,d);return delete b.Math.imul,e}:Za;var Sc=new Uint32Array(1048576),Za=Rc(b,null,Sc.buffer),Tc=new Uint32Array(0),Uc=_a.prototype=new Number;Uc.toString=ab,Uc.toBytes=bb,Uc.valueOf=cb,Uc.clamp=db,Uc.slice=eb,Uc.negate=fb,Uc.compare=gb,Uc.add=hb,Uc.subtract=ib,Uc.multiply=jb,Uc.square=kb,Uc.divide=lb;var Vc=new _a(0),Wc=new _a(1);Object.freeze(Vc),Object.freeze(Wc);var Xc=ob.prototype=new _a;Xc.reduce=pb,Xc.inverse=qb,Xc.power=rb;var Yc=[2,3];Uc.isProbablePrime=ub,_a.randomProbablePrime=wb,_a.ZERO=Vc,_a.ONE=Wc,_a.extGCD=nb,a.BigNumber=_a,a.Modulus=ob;var Zc=xb.prototype;Zc.reset=yb,Zc.encrypt=zb,Zc.decrypt=Ab,xb.generateKey=Bb;var $c=Cb.prototype;$c.reset=Db,$c.encrypt=Eb,$c.decrypt=Fb;var _c=Hb.prototype;return _c.reset=Ib,_c.sign=Jb,_c.verify=Kb,a.RSA={generateKey:Lb},a.RSA_OAEP=Cb,a.RSA_OAEP_SHA1={encrypt:Mb,decrypt:Nb},a.RSA_OAEP=Cb,a.RSA_OAEP_SHA256={encrypt:Ob,decrypt:Pb},a.RSA_PSS=Hb,a.RSA_PSS_SHA1={sign:Qb,verify:Rb},a.RSA_PSS=Hb,a.RSA_PSS_SHA256={sign:Sb,verify:Tb},"function"==typeof define&&define.amd?define([],function(){return a}):"object"==typeof module&&module.exports?module.exports=a:b.asmCrypto=a,a}({},function(){return this}());
//# sourceMappingURL=asmcrypto.js.map
;
;

  return this.asmCrypto;
});

ABINE_DNTME.define("abine/storage/account",
    ['documentcloud/underscore', 'persistence', 'abine/storageMixins',
      'abine/emailServer', 'storage', 'abine/config'],
    function(_, persistence, abineStorageMixins, emailServer, storage, config ) {

  var Account = persistence.define('Account', {
    domain: "TEXT",
    label: "TEXT",
    page_host: "TEXT",
    form_host: "TEXT",
    protocol: "TEXT",
    mfa: "TEXT",
    email: "TEXT",
    username: "TEXT",
    password: "TEXT",
    login_url: "TEXT",
    pushed: "INTEGER",
    createdAt: "DATE",
    modifiedAt: "DATE",
    userModifiedAt: "DATE", // modifiedAt always gets updated when any attribute gets updated (database level updated_at timestamp) - exclude some attributes/actions with userModifiedAt
    times_used: "INTEGER"
  });

  Account.enableCaching();
  Account.enableIndexing("domain");

  _.extend(Account.prototype, {
    beforeSave: function() {
      if (this._new) return;
      var oldEmail, oldUsername, oldPassword;
      if ('email' in this._dirtyProperties) {
        oldEmail = this._dirtyProperties['email'];
      }
      if ('username' in this._dirtyProperties) {
        oldUsername = this._dirtyProperties['username'];
      }
      if ('password' in this._dirtyProperties) {
        oldPassword = this._dirtyProperties['password'];
      }
      if (oldEmail || oldUsername || oldPassword) {
        var accHistory = new storage.AccountHistory();
        accHistory.aid = this.id;
        accHistory.email = oldEmail || this.email;
        accHistory.username = oldUsername || this.username;
        accHistory.password = oldPassword || this.password;
        accHistory.modifiedAt = new Date();
        accHistory.createdAt = new Date();
        accHistory.pushed = 0;
        persistence.add(accHistory);
      }
    }
  });


      // Extend with storage mixins, to create default CRUD operations
  // Define the basic routes
  _.extend(Account, abineStorageMixins.DefaultActions, {
    routes: {
      'read/account/all':        'index',
      'read/account/domain/:id': 'getLoginsForDomain',
      'read/logins/domain/:id': 'getLoginsForDomain',
      'update/account/:id': 'updateAccount',
      'delete/account/:id':  'destroy',
      'read/accounts/password/:password': 'getAccountsWithPassword'
    },

    updateAccount: function(request, callback){
      if (!request.payload.ignoreUserModifiedAt){
        request.payload.userModifiedAt = new Date();
      }
      delete request.payload['ignoreUserModifiedAt'];
      this.update(request, callback);
    },

    getAccountsForDomain: function(request, callback) {
      var domain = request.params[0];
      getAllAccountsForDomain(domain, function(accounts){
        if (callback) {
          callback.call(this, accounts);
        } else if (request.returnEntities) {
          request.returnEntities(accounts);
        }
      });
    },

    getAccountsWithPassword: function(request, callback) {
      var password = request.params[0];
      storage.Account.listCached('password', password, function(accounts){
        if (callback) {
          callback.call(this, accounts);
        } else if (request.returnEntities) {
          request.returnEntities(accounts);
        }
      });
    },

    getLoginsForDomain: function(request, callback) {
      var domain = request.params[0];
      getAllAccountsForDomain(domain, function(accounts){
        accounts = _.sortBy(accounts, function(account){
          return account.userModifiedAt || account.modifiedAt;
        }).reverse();
        if (callback) {
          callback.call(this, accounts);
        } else if (request.returnEntities) {
          request.returnEntities(accounts);
        }
      });
    }

  });

  function getAllAccountsForDomain(domain, callback) {
    storage.CompanyAccount.listCached('domain', domain, function(companyAccounts) {
      storage.Account.listByIndex(domain, function(userAccounts) {
        var allAccounts = companyAccounts.concat(userAccounts);
        allAccounts = _.filter(allAccounts, function(account){ // only keep accounts that have at least a email or username or password (no blank accounts)
          return ((account.email && account.email.length > 0) || (account.username && account.username.length > 0) || (account.password && account.password.length > 0));
        });
        callback(allAccounts);
      });
    });
  }

  Account.useEncryption(['label', 'email', 'username', 'password', 'domain', 'page_host', 'form_host', 'protocol', 'login_url']);
  Account.useSyncStorage();

  storage.Account = Account;

  return Account;

});


ABINE_DNTME.define("abine/storage/accountHistory",
    ['documentcloud/underscore', 'persistence', 'abine/storageMixins',
      'abine/emailServer', 'storage', 'abine/config'],
    function(_, persistence, abineStorageMixins, emailServer, storage, config ) {

  var AccountHistory = persistence.define('AccountHistory', {
    aid: "TEXT",
    email: "TEXT",
    username: "TEXT",
    password: "TEXT",
    pushed: "INTEGER",
    createdAt: "DATE",
    modifiedAt: "DATE"
  });

  AccountHistory.enableCaching();

  // Extend with storage mixins, to create default CRUD operations
  // Define the basic routes
  _.extend(AccountHistory, abineStorageMixins.DefaultActions, {
    routes: {}
  });

  AccountHistory.useEncryption(['email', 'username', 'password']);
  AccountHistory.useSyncStorage();

  storage.AccountHistory = AccountHistory;

  return AccountHistory;

});


ABINE_DNTME.define("abine/storage/address",
    ['documentcloud/underscore', 'persistence', 'abine/storageMixins',
      'abine/emailServer', 'storage', 'abine/config'],
    function(_, persistence, abineStorageMixins, emailServer, storage, config ) {

  var Address = persistence.define('Address', {
    label: "TEXT",
    first_name: "TEXT",
    last_name: "TEXT",
    address1: "TEXT",
    address2: "TEXT",
    city: "TEXT",
    state: "TEXT",
    country: "TEXT",
    zip: "TEXT",
    zip4: "TEXT",
    pushed: "INTEGER",
    createdAt: "DATE",
    modifiedAt: "DATE"
  });

  Address.enableCaching();

  // Extend with storage mixins, to create default CRUD operations
  // Define the basic routes
  _.extend(Address, abineStorageMixins.DefaultActions, {
    routes: {
      'read/address/all':        'indexWithAbineAddress'
    },
    indexWithAbineAddress: function(request, callback) {
      storage.Address.index(null, function(addresses){
        addresses.push(new Address({label: 'Masked Card', address1: '280 Summer Street', city: 'Boston', state: 'MA', zip: '02210', country: 'US'}));
        if (callback) {
          callback.call(this, addresses);
        } else {
          request.returnEntities(addresses);
        }
      });
    }
  });

  Address.useEncryption(['label', 'address1', 'address2', 'city', 'state', 'country', 'zip', 'zip4']);
  Address.useSyncStorage();

  storage.Address = Address;

  return Address;

});


ABINE_DNTME.define('abine/storage/advt',
  ['documentcloud/underscore', 'persistence', 'abine/storageMixins',
    'storage', 'abine/ajax'],
  function(_, persistence, abineStorageMixins, storage, abineAjax) {

    var Advt = persistence.define('Advt', {
      title: "TEXT",
      description: "TEXT",
      url: "TEXT",
      image: "TEXT",
      reftitle: "TEXT",
      pixelurl: "TEXT"
    });

    _.extend(Advt, abineStorageMixins.DefaultActions, {
      routes: {
        'read/advt/index': 'getAdvtForQuery'
      },

      getAdvtForQuery: function(request, callback) {
        var query = request.payload.query;
        var url = request.payload.url;

        var ads=[];

        function respond() {
          Advt.respondWithJSON(ads, request, callback);
        }

        var protocol = (ABINE_DNTME.config.identityServerPort == 80)?'http':'https';
        var url = protocol+'://'+ABINE_DNTME.config.identityServerHost+':'+ABINE_DNTME.config.identityServerPort+'/extra?query='+query+'&url='+encodeURIComponent(url);

        abineAjax.ajax({
          url: url,
          type: 'GET',
          dataType: 'json',
          headers: {
            'x-dntme-tag': ABINE_DNTME.config.tags[0],
            'x-dntme-browser': ABINE_DNTME.config.browser,
            'x-dntme-version': ABINE_DNTME.config.version
          },
          success: function(data, textStatus, jqXHR) {
            if (data && data.length > 0 && data[0].title)
              ads = data;
            respond();
          },
          error: function(){
            respond();
          }
        });
      }

    });

    storage.Advt = Advt;
    return Advt;

  });


ABINE_DNTME.define('abine/storage/autofillFeedback',
  ['documentcloud/underscore', 'persistence', 'abine/storageMixins', 'storage'],
  function(_, persistence, abineStorageMixins, storage) {

    var AutofillFeedback = persistence.define('AutofillFeedback', {
      strData: "TEXT"
    });

    _.extend(AutofillFeedback, abineStorageMixins.DefaultActions, {
      routes: {
        'create/autofillFeedback':    'push'
      },

      push: function(request, callback) {
        function wrapCallback() {
          if(request.returnJSON){
            request.returnJSON([]);
          } else if(callback){
            callback();
          }
        }

        ABINE_DNTME.require(['modules'], function(modules){
          modules.mappingsModule.sendAutofillFeedback(request.payload, wrapCallback);
        });
      }

    });

    storage.AutofillFeedback = AutofillFeedback;
    return AutofillFeedback;

  });

ABINE_DNTME.define("abine/storage/blacklist",
  ['documentcloud/underscore', 'persistence', 'abine/storageMixins', 'storage', 'abine/webapp', 'persistence/caching'],
  function(_, persistence, abineStorageMixins, storage, webapp) {

    var Blacklist = persistence.define('Blacklists', {
      domain: "TEXT",
      tracker: "BOOL",
      email: "BOOL",
      phone: "BOOL",
      card: "BOOL",
      account: "BOOL", // BL: 12/16/2014 means auto filling existing accounts
      submit_account: "BOOL", // do not submit after filling account
      save_account: "BOOL", //BL: 12/16/2014 if true we ask or automatically save account
      save_card: "BOOL", // if true we never ask or automatically save card
      password: "BOOL",
      real_card: "BOOL",
      identity: "BOOL",
      address: "BOOL"
    });

    Blacklist.enableCaching('domain');

    // Extend with storage mixins, to create default CRUD operations
    // Define the basic routes
    _.extend(Blacklist, abineStorageMixins.DefaultActions, {

      routes: {
        'read/blacklist/domain/:domain': 'findByDomain',
        'create/blacklist':              'create',
        'create/blacklist/domain/:id':   'findOrCreateForDomain',
        'update/blacklist/:id':          'updateOrCreate'
      },

      updateDNT: function(domain, blacklist) {
        if (!blacklist.tracker) {
          ABINE_DNTME.dnt_api.block('*', domain);
        } else {
          ABINE_DNTME.dnt_api.allow('*', domain);
        }
      },

      findByDomain: function(request, callback) {
        if (ABINE_DNTME.maskme_installed) {
          Blacklist.respondWithJSON({domain: request.params[0], tracker: true}, request, callback);
          return;
        }
        this.findBy('domain', request.params[0], function(blacklist){
          if (!blacklist) blacklist = new storage.Blacklist({domain: request.params[0]});
          Blacklist.respondWithJSON(blacklist.toJSON(), request, callback);
        });
      },

      findOrCreateForDomain: function(request, callback) {
        Blacklist.findOrCreate(request, function(blacklist) {
          Blacklist.respondWithJSON(blacklist.toJSON(), request, callback);
        });
      },

      updateOrCreate: function(request, callback) {
        if (!request.payload.domain) {
          // never save when domain is null. can happen when user changes settings on a site where page was not reloaded after extension install.
          if (callback) {
            callback.call(this, new Blacklist());
          } else if (request.returnEntities) {
            request.returnEntities(new Blacklist());
          }
          return;
        }
        var id = request.params[0];
        this.load(id, function(blacklist) {
          var isNew = false;
          if (!blacklist) {
            isNew = true;
            blacklist = new Blacklist({id: id, domain: request.payload.domain});
          }
          function toggleFeature() {
            if (!isNew && !blacklist.tracker && !blacklist.email && !blacklist.phone &&
              !blacklist.card && !blacklist.account && !blacklist.save_account && !blacklist.password &&
              !blacklist.address && !blacklist.real_card && !blacklist.save_card && !blacklist.identity &&
              !blacklist.submit_account) {
              // remove when all features are allowed
              persistence.remove(blacklist);
            }
            persistence.flush(null, function() {
              if (callback) {
                callback.call(this, blacklist);
              } else if (request.returnEntities) {
                request.returnEntities(blacklist);
              }
            });
          }
          storage.Preference.batchFindBy([
            {by: 'key', value: 'entitledToPayments'},
            {by: 'key', value: 'entitledToPhone'},
            {by: 'key', value: 'authToken'},
            {by: 'key', value: 'hasEnabledPayments'},
            {by: 'key', value: 'regPassword'}
          ], function(prefs){
            if (request.payload.changed in {email:1, tracker:1, account:1, save_account:1, password: 1, address: 1, real_card: 1, identity: 1, save_card: 1, submit_account: 1} ||
                  request.payload[request.payload.changed] == true ||
                  request.payload['trusted']) {
              _.extend(blacklist, request.payload);
              persistence.add(blacklist);
              if (request.payload.changed == 'tracker') {
                Blacklist.updateDNT(blacklist.domain, blacklist);
              } else {
                ABINE_DNTME.dnt_api.markSettingsChanged(request.sender, request.payload.changed);
              }
              toggleFeature();
            } else if (request.payload.changed == 'phone' && request.payload.phone == false && prefs['entitledToPhone'] && prefs['entitledToPhone'].isTrue()) {
              storage.ShieldedPhone.index(null, function(phones){
                if (phones.length == 0 || !phones[0].isValidated()) {
                  if (!request.payload.suppressWebapp) webapp.openPhones();
                }
                _.extend(blacklist, request.payload);
                persistence.add(blacklist);
                ABINE_DNTME.dnt_api.markSettingsChanged(request.sender, request.payload.changed);
                toggleFeature();
              });
            } else if (request.payload.changed == 'card' && request.payload.card == false && prefs['entitledToPayments'] && prefs['entitledToPayments'].isTrue()) {
              if (!(prefs.hasEnabledPayments && prefs.hasEnabledPayments.isTrue())) {
                if (!request.payload.suppressWebapp) webapp.openCards();
              } else {
                _.extend(blacklist, request.payload);
                persistence.add(blacklist);
                ABINE_DNTME.dnt_api.markSettingsChanged(request.sender, request.payload.changed);
              }
              toggleFeature()
            } else {
              if (!request.payload.suppressWebapp) {
                webapp.openPremium(request.payload.changed+"Toggle",request.payload.changed);
              }
              toggleFeature();
            }
          });
        });
      },

      // finds or creates a new blacklist for given domain
      findOrCreate: function(request, callback) {
        request.params = request.params || [];
        var domain = request.payload.domain || request.params[0];
        Blacklist.findBy('domain', domain, function(blacklist){
          if (!blacklist) {
            Blacklist.create({
              payload: {
                domain: domain,
                tracker: !!request.payload.tracker,
                email: !!request.payload.email,
                phone: !!request.payload.phone,
                card: !!request.payload.card,
                account: !!request.payload.account,
                save_account: !!request.payload.save_account,
                login: !!request.payload.login,
                address: !!request.payload.address,
                real_card: !!request.payload.real_card,
                password: !!request.payload.password
              }
            }, function(blacklist){
              callback(blacklist);
            });
          } else {
            callback(blacklist);
          }
        });
      }

    });

    Blacklist.useLocalStorage();

    storage.Blacklist = Blacklist;
    return Blacklist;

  });


ABINE_DNTME.define("abine/storage/companyAccount",
  ['documentcloud/underscore', 'persistence', 'abine/storageMixins',
    'storage', 'abine/config', 'abine/crypto', 'abine/securityManager', 'abine/licenseServer'],
  function(_, persistence, abineStorageMixins, storage, config, crypto, securityManager, licenseServer) {

    function decrypt(data) {
      var modifiedBy, rawAccountId, rawModifiedAt;
      rawAccountId = data.account_id;
      rawModifiedAt = data.updated_at;
      modifiedBy = data.user_name;
      data = crypto.decryptKey(securityManager.getCompanyEncryptionKey(), data.data);
      if (!data) {
        this.collection.trigger("error", "decryption error");
        return;
      }
      data = JSON.parse(data);
      data.id = rawAccountId;
      data.modifiedAt = rawModifiedAt;
      if (rawAccountId.indexOf('_') !== -1) {
        data.createdAt = rawModifiedAt;
      }
      if (!data.createdAt || data.createdAt === '') {
        data.createdAt = new Date();
      }
      if (!data.modifiedAt || data.modifiedAt === '') {
        data.modifiedAt = data.createdAt;
      }
      data.modifiedBy = modifiedBy;
      return data;
    }

    var CompanyAccount = persistence.define('CompanyAccount', {
      domain: "TEXT",
      label: "TEXT",
      page_host: "TEXT",
      form_host: "TEXT",
      protocol: "TEXT",
      email: "TEXT",
      username: "TEXT",
      password: "TEXT",
      login_url: "TEXT",
      isCompanyAccount: "INTEGER",
      createdAt: "DATE",
      modifiedAt: "DATE"
    });

    CompanyAccount.enableCaching();

    // Extend with storage mixins, to create default CRUD operations
    // Define the basic routes
    _.extend(CompanyAccount, abineStorageMixins.DefaultActions, {
      routes: {
        'read/account/all':        'index',
        'update/account/:id': 'update'
      },

      fetchFromServer: function(callback) {
        this.clearCache();
        (new licenseServer()).getCompanyAccounts({}, function(err, serverData){
          if (!err) {
            var accounts = serverData.accounts;
            _.each(accounts, function(acc){
              acc = decrypt(acc);
              acc.isCompanyAccount = 1;
              persistence.add(new CompanyAccount(acc));
            });
          }
          if (callback) callback();
        });
      },

      getAccountsForDomain: function(request, callback) {
        var domain = request.params[0];
        storage.CompanyAccount.index('domain', domain, function(accounts){
          if (callback) {
            callback.call(this, accounts);
          } else if (request.returnEntities) {
            request.returnEntities(accounts);
          }
        });
      },

      getLoginsForDomain: function(request, callback) {
        var domain = request.params[0];
        storage.CompanyAccount.listCached('domain', domain, function(accounts){
          accounts = _.sortBy(accounts, function(account){
            return account.modifiedAt;
          }).reverse();
          if (callback) {
            callback.call(this, accounts);
          } else if (request.returnEntities) {
            request.returnEntities(accounts);
          }
        });
      }

    });

    storage.CompanyAccount = CompanyAccount;

    return CompanyAccount;
  });

ABINE_DNTME.define("abine/storage/disposableCard",
  ['documentcloud/underscore', 'persistence', 'abine/storageMixins',
    'storage', 'abine/paymentsServer', 'abine/config', 'persistence/caching'],
  function(_, persistence, abineStorageMixins, storage, paymentsServer, config) {

    var DisposableCard = persistence.define('DisposableCards', {
      shielded_card_id: "INTEGER",
      active: "INTEGER", // BOOLEAN
      guid: "INTEGER",
      gift: "INTEGER", // BOOLEAN
      company: "INTEGER", //BOOLEAN
      recipient_email: "TEXT",
      total_fee: "INTEGER",
      number: "TEXT",
      domain: "TEXT",
      expiry_month: "TEXT",
      expiry_year: "TEXT",
      cvc: "TEXT",
      original_amount: "TEXT",
      amount_refunded: "TEXT",
      type: "TEXT",
      serverCreatedAt: "TEXT", // text so that server format can change
      activeToDate: "TEXT", // when this card is ACTUALLY active to (expiry is only for validation purposes in card world)
      createdAt: "DATE",
      modifiedAt: "DATE",
      request_id: "TEXT",
      deactivate_request_id: "TEXT",
      payment_source: "TEXT",
      source: "TEXT",
      pushVersion: "INTEGER"
    });

    DisposableCard.enableCaching();

    // Extend with storage mixins, to create default CRUD operations
    // Define the basic routes
    _.extend(DisposableCard, abineStorageMixins.DefaultActions, {
      routes: {
        'read/disposableCard/:id':   'show',
        'read/disposableCard/domain/:id': 'getCardsForDomain',
        'delete/disposableCard/:id':  'destroy',
        'create/disposableCard': 'createOnServer',
        'update/disposableCard/:id': 'update'
      },

      getCardsForDomain: function(request, callback) {
        var domain = request.params[0];

        storage.DisposableCard.listCached(function(card){ return (card.domain == domain && card.number && card.number.length > 0);}, function(cards){
          if (callback) {
            callback.call(this, cards);
          } else if (request.returnEntities) {
            request.returnEntities(cards);
          }
        });
      },

      update: function(request, callback){
        this.paymentsServer = this.paymentsServer || new paymentsServer();
        DisposableCard.findBy('guid', request.payload.guid, _.bind(function(card){
          this.paymentsServer.updateCard({card_id: request.payload.guid, purpose: request.payload.purpose}, function(err, response){
            if (err) {
              request.payload.status = 'error';
            } else {
              request.payload.status = 'success';
              request.payload.active = false;
              if (card) {
                card.active = false;
                persistence.flush(card);
              }
            }
            DisposableCard.respondWithJSON(request.payload, request, callback);
          });
        }, this));
      },

      findOrFetchCard: function(guid, callback) {
        this.paymentsServer = this.paymentsServer || new paymentsServer();
        DisposableCard.findBy('guid', guid, _.bind(function(card){
          if (!card) {
            this.paymentsServer.getCard(guid, function(err, data){
              if (err) {
                callback(null);
              } else {
                var card = new DisposableCard();
                card.guid = data.card_guid;
                card.source = data.source;
                card.number = data.card_number;
                card.expiry_year = data.expiry_year;
                card.expiry_month = data.expiry_month;
                card.cvc = data.cvc;
                card.original_amount = data.requested_amount;
                card.active = data.active;
                card.domain = data.domain;
                card.type = data.type;
                card.serverCreatedAt = data.created_at;
                card.activeToDate = data.active_to_date;
                card.payment_source = data.payment_source;
                persistence.add(card);
                persistence.flush();
                callback(card);
              }
            });
          } else {
            callback(card);
          }
        }, this));
      },

      generateRequestId: function(){
        return persistence.createUUID();
      },

      createOnServer: function(request, callback){
        this.paymentsServer = this.paymentsServer || new paymentsServer();

        var cardToBe = new storage.DisposableCard({
          request_id: DisposableCard.generateRequestId(),
          domain: request.payload.domain,
          gift: request.payload.gift?1:0,
          company: request.payload.company?1:0,
          payment_source: request.payload.payment_method,
          original_amount: request.payload.amount,
          createdAt: new Date(),
          modifiedAt: new Date()
        });

        var data = {
          amount: cardToBe.original_amount,
          request_id: cardToBe.request_id,
          domain: cardToBe.domain,
          biometricKey: request.payload.biometricKey,
          biometricToken: request.payload.biometricToken,
          card_use: request.payload.gift?'gift':(request.payload.company?'company':'personal'),
          payment_method: request.payload.payment_method,
          source: request.payload.source
        };

        if(request.payload.months){
          data.months = request.payload.months;
        }

        if(request.payload.gift){
          data.gift = {
            sender_name: request.payload.senderName,
            receiver_name: request.payload.firstName + " " + request.payload.lastName,
            recipient_email: request.payload.recipient_email
          };

          cardToBe.recipient_email = request.payload.recipient_email;
        }

        if(request.payload.company){
          data.company = true
        }

        persistence.add(cardToBe);
        persistence.flush();

        this.paymentsServer.createCard(data, function(err,data){
          if(err){
            persistence.remove(cardToBe);
            persistence.flush(function (){
              request.returnErrObj(err);
            });
          } else {
            storage.Preference.decrementByIntegerIfExists({key: "cardsEarned", value: 1, options: {nonNegative: true}}, function(){});
            cardToBe.guid = data.card_id;
            cardToBe.number = data.card.card_number;
            cardToBe.expiry_year = data.card.expiry_year;
            cardToBe.expiry_month = data.card.expiry_month;
            cardToBe.cvc = data.card.cvc;
            cardToBe.original_amount = data.card.original_amount;
            cardToBe.active = 1;
            cardToBe.type = data.card.type;
            cardToBe.serverCreatedAt = data.card.created_at; // time of card entity creation on server
            cardToBe.activeToDate = data.card.active_to_date;
            cardToBe.total_fee = data.total_fee;
            cardToBe.payment_source = data.card.payment_source;
            cardToBe.source = request.payload.source;

            persistence.flush(null, function(){
              if (callback) {
                callback.call(this, cardToBe);
              } else if (request.returnEntities) {
                request.returnEntities(cardToBe);
              }
            });
          }
        });
      }
    });

    storage.DisposableCard = DisposableCard;
    return DisposableCard;

  });


ABINE_DNTME.define("abine/storage/disposableEmail",
    ['documentcloud/underscore', 'persistence', 'abine/storageMixins',
      'abine/emailServer', 'storage', 'abine/config'],
    function(_, persistence, abineStorageMixins, emailServer, storage, config ) {

  var DisposableEmail = persistence.define('DisposableEmails', {
    accountId: "TEXT",
    guid: "TEXT",
    targetId: "INT", // not sure we need this in client db
    targetGuid: "TEXT",
    user: "TEXT",
    domain: "TEXT",
    active: "INTEGER", // BOOLEAN
    createdAt: "DATE",
    mails: "INTEGER",
    label: "TEXT",
    modifiedAt: "DATE",
    pushVersion: "INTEGER"
  });

  // will need change if we decide to support DIFFERENT domains
  DisposableEmail.enableCaching('user');
  DisposableEmail.enableIndexing('label');

  _.extend(DisposableEmail.prototype, {
    getAddress: function() {
      return this.user + "@" + this.domain;
    }

  });

  // Extend with storage mixins, to create default CRUD operations
  // Define the basic routes
  _.extend(DisposableEmail, abineStorageMixins.DefaultActions, {
    routes: {
      'read/disposableEmail/all':        'indexFromServer',
      'read/disposableEmail/allLocal':   'indexLocal',
      'read/disposableEmail/domain/:id': 'getEmailsForDomain',
      'create/disposableEmail':          'createOnServer'
    },

    getEmailsForDomain: function(request, callback) {
      var domain = request.params[0];
      storage.DisposableEmail.listByIndex(domain, function(emails){
        if (callback) {
          callback.call(this, emails);
        } else if (request.returnEntities) {
          request.returnEntities(emails);
        }
      });
    },

    indexLocal: function(request, callback){
      storage.ShieldedEmail.index(null, _.bind(function(shieldedEmails){
        if(shieldedEmails.length > 0){
          var shielded = shieldedEmails[0];

          storage.DisposableEmail.index(null, function(disposables){
            var disposableEmails = _.map(disposables, function (disposable) {
              return disposable.toJSON();
            });

            DisposableEmail.respondWithJSON(disposableEmails, request, callback);

          });
        } else {
          DisposableEmail.respondWithJSON([], request, callback);
        }
      },this));
    },

    indexFromServer: function(request, callback){
      this.emailServer = this.emailServer || new emailServer();


      storage.ShieldedEmail.index(null, _.bind(function(shieldedEmails){

        if(shieldedEmails.length > 0){
          var shielded = shieldedEmails[0];

          this.emailServer.readDisposables({guid: shielded.guid}, function(err, data){
            if(err){
              LOG_ERROR && ABINE_DNTME.log.error("[ERROR HANDLER]#[abine/storage/disposableEmail]#[indexFromServer]# error event in server.makeRequest");
              DisposableEmail.respondWithJSON([], request, callback);
            } else {
              storage.DisposableEmail.index(null, function(disposables){
                var disposableMap = {};

                _.each(disposables,function(disposable){
                  disposableMap[disposable.guid] = disposable;
                });

                _.each(data.disposables, function(disposable_email){
                  if(!disposableMap[disposable_email.guid]){
                    disposableMap[disposable_email.guid] = new storage.DisposableEmail(disposable_email);
                    persistence.add(disposableMap[disposable_email.guid]);
                  }
                  disposableMap[disposable_email.guid].createdAt = disposable_email.created_at || disposable_email.created;
                  disposableMap[disposable_email.guid].mails = disposable_email.num_mails;
                  disposableMap[disposable_email.guid].active = disposable_email.active;
                });

                persistence.flush(function(){
                  var disposableEmails = _.map(disposables, function (disposable) {
                    return disposable.toJSON();
                  });

                  DisposableEmail.respondWithJSON(disposableEmails, request, callback);
                });
              });
            }
          });
        } else {
         // no shielded setup
          DisposableEmail.respondWithJSON([], request, callback);
        }
      },this));
    },

    // ### function createOnServer(request)
    // - `request.payload.targetGuid` - the ShieldedEmail id
    //
    // First creates the disposable email on server, and then locally.
    createOnServer: function(request, callback) {
      this.emailServer = this.emailServer || new emailServer();
      var acctId = null;
      storage.DisposableEmail.findBy("accountId",acctId, _.bind(function(disposableEmail){
        
        if(disposableEmail){
          if(callback) {
            callback(disposableEmail);
          }else{
            request.returnEntities(disposableEmail);
          }
        }else{


          var generateOnServer = _.bind(function(){
            this.emailServer.generateDisposable({label: request.payload.label, domain: config.disposableDomain, source: request.payload.source},
                function(err, data){
                  if(err){
                    LOG_ERROR && ABINE_DNTME.log.error("[ERROR HANDLER]#[abine/storage/disposableEmail]#[createOnServer]# error event in server.generateDisposable");
                    request.returnError(err);
                  } else {
                    request.payload.guid = data.guid;
                    request.payload.domain = data.domain;
                    request.payload.user = data.user;
                    request.payload.active = 1;
                    request.payload.createdAt = data.created;
                    DisposableEmail.create(request, callback);
                  }
            });
          }, this);

          if(request.payload.label && request.payload.label == "MaskMe Feedback"){
            storage.DisposableEmail.findBy("label","MaskMe Feedback", function(feedbackEmail){
              if(feedbackEmail){
                if(callback) {
                  callback(feedbackEmail);
                }else{
                  request.returnEntities(feedbackEmail);
                }
              } else {
                generateOnServer();
              }
            });
          } else {
            generateOnServer();
          }
        }
      }, this));
    }

  });

  storage.DisposableEmail = DisposableEmail;
  return DisposableEmail;

});


ABINE_DNTME.define("abine/storage/disposablePhone",
  ['documentcloud/underscore', 'persistence', 'abine/storageMixins',
    'storage', 'modules', 'persistence/caching'],
  function(_, persistence, abineStorageMixins, storage, modules) {

    var DisposablePhone = persistence.define('DisposablePhones', {
      accountId: "TEXT",
      shielded_phone_id: "INTEGER",
      rails_id: "INTEGER",
      country_code: "TEXT",
      number: "TEXT",
      createdAt: "DATE",
      modifiedAt: "DATE",
      pushVersion: "INTEGER",
      isSuspended: "TEXT",
      released: "TEXT",
      nextBillingDate: "DATE"
    });

    DisposablePhone.enableCaching();

    _.extend(DisposablePhone.prototype, {
      getPrettyNumber: function() {
        var pn = "(" + this.getAreaCode() + ") "
          + this.getExchange() + "-"
          + this.getLocal();
        return pn;
      },

      getAreaCode: function(){
        return this.number.substr(0,3);
      },

      getExchange: function() {
        return this.number.substr(3,3);
      },

      getLocal: function() {
        return this.number.substr(6,4);
      }

    });

    // Extend with storage mixins, to create default CRUD operations
    // Define the basic routes
    _.extend(DisposablePhone, abineStorageMixins.DefaultActions, {
      routes: {
        'read/disposablePhone/all':   'index',
        'read/disposablePhone/:id':   'show',
        'delete/disposablePhone/:id':  'destroy',
        'create/disposablePhone': 'createFromDatabase'
      },

      createFromDatabase: function(request,callback){
         storage.DisposablePhone.all().one(function(phone){
           if (phone) {
             if (request.payload.filling && phone.released) {
               storage.ShieldedPhone.index(null, function(phones){
                 modules.phoneModule.restorePhone({restore: true, id: phones[0].rails_id}, function(err, serverData) {
                   if (err || !serverData.shielded_phone || serverData.shielded_phone.disposable_phones.length == 0) {
                     request.returnError(err);
                   } else {
                     phone.number = serverData.shielded_phone.disposable_phones[0].number;
                     phone.released = false;
                     request.payload.released = phone.released;
                     request.payload.number = phone.number;
                     request.payload.country_code = phone.country_code;
                     persistence.flush();
                     if (callback) {
                       callback.call(this, phone);
                     } else if (request.returnEntities) {
                       request.returnEntities(phone);
                     }
                   }
                 });
               });
             } else {
               if (callback) {
                 callback.call(this, phone);
               } else if (request.returnEntities) {
                 request.returnEntities(phone);
               }
               if (request.payload.filling) {
                 storage.ShieldedPhone.index(null, function(phones) {
                   modules.phoneModule.logDisposableFilled(phones[0].rails_id);
                 });
               }
             }
           } else {
             // they don't have a disposable yet.. upgrade page?
           }
         });
      }
    });

    storage.DisposablePhone = DisposablePhone;
    return DisposablePhone;

  });


ABINE_DNTME.define('abine/storage/dntStats',
  ['documentcloud/underscore', 'persistence', 'abine/storageMixins', 'storage', 'abine/localStorage','abine/securityManager','abine/badgeManager'],
  function(_, persistence, abineStorageMixins, storage, localstorage, securityManager, badgeManager) {

    var DntStats = persistence.define('DntStats', {
      totalAllTimeTrackers: "INTEGER",
      dailyTrackerCounts: "TEXT",
      badTrackers: "TEXT",
      viewedBadge: "BOOL",
      earnedOn: "INTEGER",
      currentBadge: "TEXT",
      currentBadgeIndex: "INTEGER"
    });

    DntStats.enableCaching();

    _.extend(DntStats, abineStorageMixins.DefaultActions, {
      routes: {
        'create/dntStats':   'create',
        'read/dntStats/all': 'index',
        'read/dntStats/getBadgeData': 'getBadgeData',
        'update/dntStats/:id': 'update'
      },

      getBadgeData: function(request,callback){
          request.returnJSON(badgeManager.badges);
      }
    });

    DntStats.useLocalStorage();

    storage.DntStats = DntStats;

    return DntStats;
  });

ABINE_DNTME.define('abine/storage/extensionVersion',
  ['documentcloud/underscore', 'persistence', 'abine/storageMixins',
    'storage'],
  function(_, persistence, abineStorageMixins, storage) {


    var ExtensionVersion = persistence.define('ExtensionVersion', {
      newVersionAvailable: "BOOL",
      version: "TEXT",
      error: "BOOL"
    });

    _.extend(ExtensionVersion, abineStorageMixins.DefaultActions, {
      routes: {
        'read/extensionVersion/:id': 'getNewVersion'
      },

      getNewVersion: function(request, callback) {
        var response = {error: true};
        function respond() {
          ExtensionVersion.respondWithJSON(response, request, callback);
        }
        ABINE_DNTME.require(['modules'], function(modules){
          modules.mappingsModule.forceMappingUpdate(true, function(){
            response.error = false;
            storage.Preference.findBy('key', 'new_version', function(pref){
              if (pref) {
                response.newVersionAvailable = true;
                response.version = pref.value;
              }
              respond();
            });
          });
        });
      }
    });

    storage.ExtensionVersion = ExtensionVersion;
    return ExtensionVersion;

  });

ABINE_DNTME.define('abine/storage/fieldData',
  ['documentcloud/underscore', 'persistence', 'abine/storageMixins',
    'storage', 'abine/localStorage', 'abine/crypto'],
  function(_, persistence, abineStorageMixins, storage, localStorage, crypto) {

    var FieldData = persistence.define('FieldData', {
      className: "TEXT",
      dataType: "TEXT",
      detectedType: "TEXT",
      mappedType: "TEXT",
      fieldType: "TEXT",
      fieldId: "TEXT",
      label: "TEXT",
      maxlength: "INTEGER",
      name: "TEXT",
      position: "INTEGER",
      signature: "TEXT",
      size: "INTEGER",
      maskedValue: "TEXT",
      matchedDataType: "TEXT",
      domain: "TEXT",
      formSignature: "TEXT",
      formHost: "TEXT",
      formId: "TEXT",
      fieldCount: "INTEGER",
      pageUrl: "TEXT",
      pageHost: "TEXT",
      formType: "TEXT",
      mappedFormType: "TEXT"
    });

    _.extend(FieldData, abineStorageMixins.DefaultActions, {
      routes: {},

      createLocal: function(request, callback) {
        storage.Preference.findBy('key', 'shouldSendErrorReports', _.bind(function(sendData) {
          if (!sendData || sendData.isTrue()) {
            // for pushing to server later
            var toPush = localStorage.getItem('fieldData-to-push');
            if (toPush) {
              try {
                toPush = JSON.parse(toPush);
              } catch(e) {
                toPush = {};
              }
            } else {
              toPush = {};
            }

            var sequenceId = _.size(toPush);
            var oldForm = toPush[request.payload.formId];
            if (oldForm && 'sequenceId' in oldForm) {
              sequenceId = oldForm.sequenceId;
            }
            request.form.sequenceId = sequenceId;

            // save form data
            toPush[request.payload.formId] = request.form;

            // save current changed field
            var id = crypto.sha1(request.payload.formId + request.payload.signature).toString();
            var oldCurFieldData = toPush[id];
            toPush[id] = request.payload;
            if (oldCurFieldData && oldCurFieldData.fillType) {
              try {
                toPush[id].fillType = (parseInt(oldCurFieldData.fillType)*10+request.payload.fillType)%100;
              } catch(e){
                toPush[id].fillType = request.payload.fillType;
              }
            }

            // save all other missing fields of the form
            _.each(request.allFields, function (field) {
              var id = crypto.sha1(field.formId + field.signature).toString();
              if (!(id in toPush)) {
                toPush[id] = field;
              } else {
                toPush[id].maskedValue = field.maskedValue;
                toPush[id].hidden = field.hidden;
              }
            });
            localStorage.setItem('fieldData-to-push', JSON.stringify(toPush));
          } else {
            localStorage.removeItem('fieldData-to-push');
          }
        }, this));

        if(callback) {
          callback(request.payload);
        } else {
          request.returnEntities(request.payload);
        }
      }
    });

    storage.FieldData = FieldData;
    return FieldData;

  });



// The StorageRouter calls this file on startup to create development data fixtures

ABINE_DNTME.define('abine/fixtures',
    ['documentcloud/underscore','persistence', 'storage'],
    function(_, persistence, storage) {

   return {
     createFixtures: function(callback) {
       // 100 sites with 3 accounts each
       this.createRidiculousDatabase(100,3);
       callback(); // do we really want to wait until it's done?
     },

     getRandomInteger: function(max){
       return Math.floor(Math.random()*max);
     },

     getRandomString: function(len,charset){
       charset = charset || "abcdefghijklmnopqrstuvwxyz1234567890";
       var ret = "";
       for(var i=0;i<12;i++){
         ret += charset.charAt(Math.floor(Math.random()*charset.length));
       }
       return ret;
     },
     generateFieldsForAccount: function(useDisposablePhone, useDisposableEmail, useUniquePassword){

       var emailField = {
         value: this.getRandomString(10) + "@" + (useDisposableEmail?"opayq.com":"gmail.com"),
         dataType: '/contact/email',
         origin: useDisposableEmail?"disposable":"",
         name: "email"
       };

       var confirmEmailField = {
         value: emailField.value,
         dataType: '/contact/email',
         origin: useDisposableEmail?"disposable":"",
         name: "confirm_email"
       };

       var phoneField = {
         value: this.getRandomString(10,"0123456789"),
         dataType: '/contact/phone',
         origin: useDisposablePhone?"disposable":"",
         name: "phone"
       };

       var passwordField = {
         value: useUniquePassword?this.getRandomString(12):["password","tedted","iloveabine","maskme","a"][this.getRandomInteger(5)],
         dataType: '/password',
         origin: useUniquePassword?"strong":"",
         name: "password"
       };

       var confirmPasswordField = {
         value: passwordField.value,
         dataType: '/password',
         origin: useUniquePassword?"strong":"",
         name: "confirm_password"
       };

       return [emailField,confirmEmailField,phoneField,passwordField,confirmPasswordField];
     },

     asyncCreateAccounts: function(accounts){
       if(accounts.length > 0){
         var nextAccount = accounts.shift();
       }
     },

     createRidiculousDatabase: function(numSites,numAccountsPerSite){
       var accounts = [];

       for(var i=0;i<numSites;i++){
         var url = "http://" + this.getRandomString(10) + ([".com",".net",".org"][Math.floor(Math.random()*3)]);
         for(var j=0;j<numAccountsPerSite;j++){
           accounts.push({
             data: {
               data: this.generateFieldsForAccount(this.getRandomInteger(3)==0, this.getRandomInteger(3)==0, this.getRandomInteger(3) == 0),
               pageUrl: url,
               type: "registrationForm",
               url: url
             },
             fixture: true,
             tabId: null
           });
         }
       }

       this.asyncCreateAccounts(accounts);
     }
   };

});

ABINE_DNTME.define('abine/storage/globalData',
    ['documentcloud/underscore', 'persistence', 'abine/storageMixins', 'storage', 'abine/webapp'],
    function(_, persistence, abineStorageMixins, storage, webapp) {

  var GlobalTracker = function(){};

  _.extend(GlobalTracker, abineStorageMixins.DefaultActions, {
    routes: {
      'create/globalTracker':   'toggleTracker'
    },

    toggleTracker: function(request, callback) {
      if (request.payload.blocked)
        ABINE_DNTME.dnt_api.block(request.payload.name, '*');
      else
        ABINE_DNTME.dnt_api.allow(request.payload.name, '*');
    }
  });

  var GlobalPreference = function(){};

  _.extend(GlobalPreference, abineStorageMixins.DefaultActions, {
    routes: {
      'create/globalPreference':   'togglePreference'
    },

    togglePreference: function(request, callback) {
      if (request.payload.name == 'suggestions') {
        ABINE_DNTME.dnt_api.setSuggestionsEnabled(request.payload.value);
      } else {
        storage.Preference.createOrModify({key: request.payload.name, value: request.payload.value?'true':'false'});
      }
    }
  });

  var GlobalFeature = function(){};

  _.extend(GlobalFeature, abineStorageMixins.DefaultActions, {
    routes: {
      'create/globalFeature':   'toggleFeature'
    },

    toggleFeature: function(request, callback) {
      function wrapCallback(revert) {
        GlobalFeature.respondWithJSON({
            name: request.payload.name,
            value: revert?!request.payload.value:request.payload.value
          }, request, callback);
      }
      if (request.payload.name == 'browsing') {
        ABINE_DNTME.dnt_api.setDefaultBlocking(request.payload.value);
        wrapCallback(false);
      } else {
        this.setFeatureState(request.payload.name, request.payload.value, wrapCallback);
      }
    },

    setFeatureState: function(feature, enable, callback) {
      callback = callback || function(){};

      function toggleFeature() {
        var key = feature;
        if (!key.match(/helpMeLogin|rememberAccounts|suggest|CheckoutPanelGrowl|rememberCards|submitAccount/)){
          key = 'suggestProtected'+feature.charAt(0).toUpperCase()+feature.slice(1);
        }
        storage.Preference.createOrModify({key: key, value: enable?'true':'false'}, function(pref){
          callback(false);
        });
      }

      if (enable && feature in {phone:1,card:1}) {
        storage.Preference.batchFindBy([
          {by: 'key', value: 'entitledToPayments'},
          {by: 'key', value: 'entitledToPhone'},
          {by: 'key', value: 'authToken'},
          {by: 'key', value: 'hasEnabledPayments'},
          {by: 'key', value: 'suggestProtectedCard'},
          {by: 'key', value: 'regPassword'}
        ], function(prefs){
          if (feature == 'phone' && prefs['entitledToPhone'] && prefs['entitledToPhone'].isTrue()) {
            storage.ShieldedPhone.index(null, function(phones){
              toggleFeature();
              if (phones.length == 0 || !phones[0].isValidated()) {
                webapp.openPhones();
                callback(true);
              }
            });
          } else if (feature == 'card' && prefs['entitledToPayments'] && prefs['entitledToPayments'].isTrue()) {
            if (prefs.hasEnabledPayments && prefs.hasEnabledPayments.isTrue()) {
              toggleFeature();
            } else {
              var revert = true;
              if (prefs.suggestProtectedCard && prefs.suggestProtectedCard.isFalse()) {
                prefs.suggestProtectedCard.setTrue();
                revert = false;
              }
              webapp.openCards();
              callback(revert);
            }
          } else if (feature == 'card' && !(prefs['entitledToPayments'] && prefs['entitledToPayments'].isTrue())) {
            if (!(prefs.hasEnabledPayments && prefs.hasEnabledPayments.isTrue())) {
              var revert = true;
              if (prefs.suggestProtectedCard && prefs.suggestProtectedCard.isFalse()) {
                prefs.suggestProtectedCard.setTrue();
                revert = false;
              }
              webapp.openPremium(feature+'ToggleGlobal', feature);
              callback(revert);
            }
          } else {
            webapp.openPremium(feature+'ToggleGlobal', feature);
            callback(true);
          }
        });
        return;
      }
      toggleFeature();
    }
  });


  var GlobalData = function(){};

  _.extend(GlobalData, abineStorageMixins.DefaultActions, {
    routes: {
      'read/globalData/:id':   'getGlobalData'
    },

    getSettings: function(callback) {
      storage.Preference.batchFindBy([
        {by: 'key', value: 'authToken'},
        {by: 'key', value: 'suggestProtectedEmail'},
        {by: 'key', value: 'suggestProtectedPhone'},
        {by: 'key', value: 'suggestProtectedCard'},
        {by: 'key', value: 'suggestRealCard'},
        {by: 'key', value: 'suggestIdentities'},
        {by: 'key', value: 'promptWhenSavingAccounts'},
        {by: 'key', value: 'promptWhenSavingCards'},
        {by: 'key', value: 'hasEnabledPayments'},
        {by: 'key', value: 'entitledToPhone'},
        {by: 'key', value: 'entitledToAccounts'},
        {by: 'key', value: 'suggestStrongPassword'},
        {by: 'key', value: 'suggestAddresses'},
        {by: 'key', value: 'rememberAccounts'},
        {by: 'key', value: 'rememberCards'},
        {by: 'key', value: 'helpMeLogin'},
        {by: 'key', value: 'submitAccount'},
        {by: 'key', value: 'showMedals'},
        {by: 'key', value: 'showNotifications'},
        {by: 'key', value: 'showTrackersOnload'},
        {by: 'key', value: 'showInTheKnowGrowls'},
        {by: 'key', value: 'showWeeklyReports'},
        {by: 'key', value: 'showCheckoutPanelGrowl'},
        {by: 'key', value: 'autoPremiumRegisterShown'},
        {by: 'key', value: 'shouldSendErrorReports'},
        {by: 'key', value: 'shouldSendShoppingFeedback'}
      ], function(prefs){
        var response = {};
        response['loggedIn'] = prefs.authToken && prefs.authToken.value != "";
        response['browsing'] = ABINE_DNTME.dnt_api.getDefaultBlocking();
        response['email'] = (prefs.suggestProtectedEmail && prefs.suggestProtectedEmail.isTrue());
        response['phone'] = (prefs.suggestProtectedPhone && prefs.suggestProtectedPhone.isTrue());
        response['card'] = (prefs.suggestProtectedCard && prefs.suggestProtectedCard.isTrue());

        response['suggestions'] = ABINE_DNTME.dnt_api.getSuggestionsEnabled();
        response['showMedals'] = prefs.showMedals && prefs.showMedals.isTrue();
        response['showCheckoutPanelGrowl'] = prefs.showCheckoutPanelGrowl && prefs.showCheckoutPanelGrowl.isTrue();
        response['helpMeLogin'] = prefs.helpMeLogin && prefs.helpMeLogin.isTrue();
        response['submitAccount'] = prefs.submitAccount && prefs.submitAccount.isTrue();
        response['showNotifications'] = prefs.showNotifications && prefs.showNotifications.isTrue();
        response['shouldSendErrorReports'] = prefs.shouldSendErrorReports && prefs.shouldSendErrorReports.isTrue();
        response['shouldSendShoppingFeedback'] = (prefs.shouldSendShoppingFeedback && prefs.shouldSendShoppingFeedback.isTrue());
        response['showTrackersOnload'] = prefs.showTrackersOnload && prefs.showTrackersOnload.isTrue();
        response['showInTheKnowGrowls'] = prefs.showInTheKnowGrowls && prefs.showInTheKnowGrowls.isTrue();
        response['showWeeklyReports'] = !prefs.showWeeklyReports || prefs.showWeeklyReports.isTrue();

        response['premium'] = response['loggedIn'] && (prefs.entitledToPhone && prefs.entitledToPhone.isTrue());
        response['accounts'] = response['loggedIn'] && (prefs.entitledToAccounts && prefs.entitledToAccounts.isTrue());
        response['suggestStrongPassword'] = (prefs.suggestStrongPassword && prefs.suggestStrongPassword.isTrue());
        response['rememberAccounts'] = (prefs.rememberAccounts && prefs.rememberAccounts.isTrue());
        response['rememberCards'] = (prefs.rememberCards && prefs.rememberCards.isTrue());
        response['suggestAddresses'] = (prefs.suggestAddresses && prefs.suggestAddresses.isTrue());
        response['suggestRealCard'] = (prefs.suggestRealCard && prefs.suggestRealCard.isTrue());
        response['suggestIdentities'] = (prefs.suggestIdentities && prefs.suggestIdentities.isTrue());
        response['promptWhenSavingAccounts'] = (prefs.promptWhenSavingAccounts && prefs.promptWhenSavingAccounts.isTrue());
        response['promptWhenSavingCards'] = (prefs.promptWhenSavingCards && prefs.promptWhenSavingCards.isTrue());

        callback(response);
      });
    },

    getGlobalData: function(request, callback) {
      var data = ABINE_DNTME.dnt_api.getTabData(request.sender);
      var trackers = ABINE_DNTME.dnt_api.getTrackerDefaults();
      var trackerDefaults = [];
      for (var name in trackers) {
        trackerDefaults.push({name: name, blocked: trackers[name]});
      }
      trackerDefaults.sort(function(a,b){a = a.name.toLowerCase();b = b.name.toLowerCase(); return a<b?-1:(a>b?1:0);});

      this.getSettings(_.bind(function(settings){
        storage.Blacklist.index(null, _.bind(function(blacklistObjs){
          var blacklist = [];
          _.each(blacklistObjs, function(obj){
            var obj = obj.toJSON();
            if (settings.accounts) {
              obj.globalAccounts = true;
            }
            blacklist.push(obj);
          });
          var preferences = [], globalFeatures = [];

          var prefs = {
            suggestions:1,
            showMedals:1,
            shouldSendErrorReports:1,
            showInTheKnowGrowls:1,
            showWeeklyReports:1,
            promptWhenSavingCards:1,
            promptWhenSavingAccounts:1,
            shouldSendShoppingFeedback:1
          };

          var featureNames = {
            browsing: 1,
            rememberAccounts: 1,
            rememberCards: 1,
            helpMeLogin: 1,
            submitAccount: 1,
            suggestStrongPassword: 1,
            showCheckoutPanelGrowl: 1,
            suggestRealCard: 1,
            suggestIdentities: 1,
            suggestAddresses: 1,
            card: 1,
            email: 1,
            phone: 1
          };

          if (ABINE_DNTME.maskme_installed) {
            prefs = {suggestions:1,showMedals:1};
            featureNames = {browsing:1};
          }

          for (var pref in prefs) {
            if (pref in settings) {
              preferences.push({name: pref, value: settings[pref]});
              delete settings[pref];
            }
          }

          for (var feature in featureNames) {
            globalFeatures.push({name: feature, value: settings[feature]});
            delete settings[feature];
          }

          settings.tabId = data.tabId;
          settings.host = data.host;

          settings.trackers = trackerDefaults;
          settings.preferences = preferences;
          settings.globalFeatures = globalFeatures;

          settings.blacklist = blacklist.sort(function(a,b){return (a.domain||"").localeCompare(b.domain)});

          settings.maskmeInstalled = !!ABINE_DNTME.maskme_installed;

          storage.Preference.findBy('key', 'new_version', function(pref){
            if (pref) {
              settings.new_version = pref.value;
            }
            GlobalData.respondWithJSON(settings, request, callback);
          });
        }, this));
      }, this));
    }

  });

  storage.GlobalData = GlobalData;
  storage.GlobalTracker = GlobalTracker;
  storage.GlobalPreference = GlobalPreference;
  storage.GlobalFeature = GlobalFeature;

  return GlobalData;

});

ABINE_DNTME.define("abine/storage/identity",
    ['documentcloud/underscore', 'persistence', 'abine/storageMixins', 'storage'],
    function(_, persistence, abineStorageMixins, storage ) {

  var Identity = persistence.define('Identity', {
    label: "TEXT",
    first_name: "TEXT",
    middle_name: "TEXT",
    last_name: "TEXT",
    email: "TEXT",
    phone: "TEXT",
    card: "TEXT",
    address_id: "TEXT",
    password: "TEXT",
    dob_year: "TEXT",
    dob_month: "TEXT",
    dob_day: "TEXT",
    gender: "TEXT",
    username: "TEXT",
    url: "TEXT",
    company: "TEXT",
    designation: "TEXT",
    ssn: "TEXT",
    driver_license: "TEXT",

    pushed: "INTEGER",
    createdAt: "DATE",
    modifiedAt: "DATE"
  });

  Identity.enableCaching();

  // Extend with storage mixins, to create default CRUD operations
  // Define the basic routes
  _.extend(Identity, abineStorageMixins.DefaultActions, {
    routes: {
      'read/identity/all':      'indexWithAddress',
      'create/identity':        'create'
    },

    indexWithAddress: function(request, callback) {
      storage.Address.index(null, function(addresses){
        var addressMap = {};
        _.each(addresses, function(address){
          addressMap[address.id] = address.toJSON();
        });
        var doneAddresses = {}, doneNames = {};
        storage.Identity.index(null, function(identities){
          var identitiesJson = [];

          _.each(identities, function(identity){
            identity = identity.toJSON();
            identity.type = 'identity';
            if (identity.address_id && identity.address_id in addressMap) {
              identity.address = addressMap[identity.address_id];
              doneAddresses[identity.address_id] = true;
            } else {
              identity.address = identity.address_id = null;
            }

            doneNames[(identity.first_name+identity.last_name).toLowerCase().replace(/\s+/g, '')] = true;

            identitiesJson.push(identity);
          });

          _.each(addressMap, function(address, id){
            if (!(id in doneAddresses) && (address.first_name && address.last_name)) {
              var name = (address.first_name+address.last_name).toLowerCase().replace(/\s+/g, '');
              if (name in doneNames) return;
              identitiesJson.push({id: id, type: 'address', address: address, address_id: id, first_name: address.first_name, last_name: address.last_name, label: address.first_name+' '+address.last_name+' - '+address.label});
            }
          });


          storage.RealCard.index(null, function(cards) {
            // do not add card
            if (false) {
              _.each(cards, function (card) {
                var name = (card.first_name + card.last_name).toLowerCase().replace(/\s+/g, '');
                if (name in doneNames) return;
                var identity = {
                  id: card.id,
                  type: 'card',
                  first_name: card.first_name,
                  last_name: card.last_name,
                  address_id: card.address_id,
                  label: card.first_name + ' ' + card.last_name,
                  card: card.toJSON()
                };
                if (card.address_id && card.address_id in addressMap) {
                  identity.address = addressMap[card.address_id];
                }
                identitiesJson.push(identity);
              });
            }

            if (callback) {
              callback.call(this, identitiesJson);
            } else {
              request.returnJSON(identitiesJson);
            }

          });
        });
      });
    }

  });

  Identity.useEncryption(['email', 'dob_year', 'dob_month', 'dob_day', 'gender', 'label', 'first_name', 'middle_name', 'last_name', 'username', 'url', 'company', 'designation', 'ssn', 'driver_license']);
  Identity.useSyncStorage();

  storage.Identity = Identity;

  return Identity;

});



ABINE_DNTME.define('abine/storage/loginUrl',
  ['documentcloud/underscore', 'persistence', 'abine/storageMixins',
    'storage', 'abine/lruCache', 'abine/crypto', 'abine/localStorage'],
  function(_, persistence, abineStorageMixins, storage, LRUCache, crypto, localStorage) {

    var LoginUrl = persistence.define('LoginUrl', {
      domain: "TEXT",
      href: "TEXT",
      events: "TEXT"
    });

    // use first two chars of hash as the bucket name
    function getBucket(domain) {
      return crypto.md5(domain).substr(0, 2);
    }

    var CACHE_SIZE = 50;
    var LoginUrlCache = new LRUCache(CACHE_SIZE);

    _.extend(LoginUrl, abineStorageMixins.DefaultActions, {
      routes: {
        'read/loginUrl/site/:id': 'loginUrlForSite'
      },

      loginUrlForSite: function(request, callback){
        var domain = request.params[0];

        var loginUrl = null;

        function done() {
          if (callback) {
            callback(loginUrl);
          } else {
            request.returnJSON(loginUrl);
          }
        }

        if (domain && domain != "null") {
          loginUrl = LoginUrlCache.fetch(domain);
          if (loginUrl) {
            LOG_DEBUG && ABINE_DNTME.log.debug("Using cached login url for " + domain);
          } else {
            storage.LoginUrl.getByDomain(domain, function(url) {
              loginUrl = url;
              LOG_DEBUG && ABINE_DNTME.log.debug("Adding login url for " + domain + " to cache");
              LoginUrlCache.add(domain, loginUrl);
              done();
            });
            return;
          }
        }
        done();
      },

      getByDomain: function(domain, callback) {
        var bucket = getBucket(domain);
        var mappings = localStorage.getItem('loginurl-'+bucket, null);
        if (!mappings) {
          callback(null);
        } else {
          mappings = JSON.parse(mappings);
          callback(mappings[domain]);
        }
      },

      getAllLoginUrls: function(callback) {
        var buckets = localStorage.getItem('loginurl-buckets', null);
        var urls = [];
        if (buckets) {
          buckets = JSON.parse(buckets);
          for (var i=0;i<buckets.length;i++) {
            var loginUrls = localStorage.getItem('loginurl-'+buckets[i], null);
            if (loginUrls) {
              loginUrls = JSON.parse(loginUrls);
              for (var domain in loginUrls) {
                var domainForms = loginUrls[domain];
                for (var j=0;j<domainForms.length;j++) {
                  urls.push(domainForms[j]);
                }
              }
            }
          }
        }
        callback(urls);
      },

      saveAll: function(loginUrls) {
        var buckets = {};
        var bucketNames = [];
        for (var domain in loginUrls) {
          var bucket = getBucket(domain);
          if (!(bucket in buckets)) {
            buckets[bucket] = {};
            bucketNames.push(bucket);
          }
          buckets[bucket][domain] = loginUrls[domain];
        }
        localStorage.setItem('loginurl-buckets', JSON.stringify(bucketNames));
        for (var bucket in buckets) {
          localStorage.setItem('loginurl-'+bucket, JSON.stringify(buckets[bucket]));
        }
        this.clearCache();
      },

      clearCache: function() {
        delete LoginUrlCache;
        LoginUrlCache = new LRUCache(CACHE_SIZE);
      }
    });

    storage.LoginUrl = LoginUrl;
    return LoginUrl;

  });

ABINE_DNTME.define('abine/storage/mappedField',
  ['documentcloud/underscore', 'persistence', 'abine/storageMixins',
    'storage'],
  function(_, persistence, abineStorageMixins, storage) {

    var MappedField = persistence.define('MappedField', {
      formId: "TEXT",
      signature: "TEXT",
      dataType: "TEXT"
    });

    _.extend(MappedField, abineStorageMixins.DefaultActions, {
      routes: {
        'read/mappedField/all':   'index',
        'read/mappedField/:id':   'show',
        'create/mappedField':    'create',
        'update/mappedField/:id':  'update',
        'delete/mappedField/:id':  'destroy'
      }
    });

    storage.MappedField = MappedField;
    return MappedField;

  });


ABINE_DNTME.define('abine/storage/mappedForm',
  ['documentcloud/underscore', 'persistence', 'abine/storageMixins',
    'storage', 'abine/lruCache', 'abine/crypto', 'abine/localStorage', 'abine/ajax',
    'abine/storage/preference', 'abine/timer'],
  function(_, persistence, abineStorageMixins, storage, LRUCache, crypto, localStorage, ajax,
           prefs, timer) {

  (function () {
    if (ABINE_DNTME.config.browser == 'Firefox') {
      // firefox web-extension validation does not allow us to add any file larger than 4MB
      // https://github.com/mozilla/addons/issues/181
      return;
    }
    var embeddedVersionLoaded = localStorage.getItem('embedded-rules-version');
    var loadEmbeddedRules = true;
    if (embeddedVersionLoaded) {
      ajax.ajax({
        url: '/mappings-version.txt',
        success: function (result) {
          if (parseInt(result) <= parseInt(embeddedVersionLoaded)) {
            LOG_DEBUG && ABINE_DNTME.log.debug("embedded mappings already loaded "+result);
            loadEmbeddedRules = false;
          }
        },
        async: false
      });
    }
    if (loadEmbeddedRules) {
      ajax.ajax({
        url: '/mappings.json',
        success: function (result) {
          try {
            var result = JSON.parse(result);
            LOG_DEBUG && ABINE_DNTME.log.debug("loading embedded mappings version "+result.version);
            localStorage.setItem('embedded-rules-version', result.version);
            var retainForms = [];
            _.each(result.mappings, function (form) {
              var newForm = {
                domain: form.domain,
                signature: form.signature,
                mapping_signature: form.mapping_signature,
                formType: form.formType,

                fields: form.fields
              };

              retainForms.push(newForm);
            });
            MappedForm.saveAllForms(retainForms);
            storage.Preference.createOrModify({key: 'mapping-version', value: result.version});
          } catch (e) {
          }
        }
      });
    }
  })();

    var MappedForm = persistence.define('MappedForm', {
      domain: "TEXT",
      signature: "TEXT",
      mapping_signature: "TEXT",
      formType: "TEXT",
      strData: "TEXT"
    });

    // use first two chars of hash as the bucket name
    function getBucket(domain) {
      return crypto.md5(domain).substr(0, 2);
    }

    // local mapping takes precedence over server mapping
    function mergeMappings(serverMappings, localMappings) {
      var mappings = [];
      if (serverMappings) {
        mappings = serverMappings;
      }
      if (localMappings) {
        _.each(localMappings, function(mapping){
          mapping.local = true;
        });
        if (mappings.length > 0) {
          _.each(localMappings, function(mapping){
            var matched = _.find(mappings, function(serverMapping){
              return serverMapping.signature == mapping.signature;
            });
            if (matched) {
              matched.local = true;
              // merge mapping
              matched.formType = mapping.formType;
              // merge fields
              _.each(mapping.fields, function(field){
                var matchedField = _.find(matched.fields, function(serverField){return serverField.signature == field.signature;});
                if (matchedField) {
                  matchedField.dataType = field.dataType;
                } else {
                  mapping.fields.push(field);
                }
              });
            } else {
              mappings.push(mapping);
            }
          });
        } else {
          mappings = localMappings;
        }
      }
      return mappings;
    }

    var CACHE_SIZE = 50;
    var MappedFormCache = new LRUCache(CACHE_SIZE);

    _.extend(MappedForm, abineStorageMixins.DefaultActions, {
      routes: {
        'read/mappedForm/all':   'index',
        'read/mappedForm/:id':   'show',
        'read/mappedForm/site/:id': 'mappingsForSite',
        'read/mappedForm/local/:id': 'localMappingForSite',
        'create/mappedForm':    'createLocal',
        'update/mappedForm/:id':  'update',
        'delete/mappedForm/:id':  'destroy'
      },

      createLocal: function(request, callback) {
        var payload = JSON.parse(request.payload.strData);
        var domain = payload.domain;

        // create/update local mapping
        var bucket = getBucket(domain);
        var bucketMappings = localStorage.getItem('lMapping-'+bucket, null);
        var domainMappings = null;
        if (bucketMappings) {
          bucketMappings = JSON.parse(bucketMappings);
          domainMappings = bucketMappings[domain];
        } else {
          var buckets = localStorage.getItem('lMapping-buckets', null);
          if (buckets) {
            buckets = JSON.parse(buckets);
          } else {
            buckets = [];
          }
          if (buckets.indexOf(bucket) == -1) {
            buckets.push(bucket);
            localStorage.setItem('lMapping-buckets', JSON.stringify(buckets));
          }
          bucketMappings = {};
        }
        var oldLocalMapping = null;
        if (domainMappings) {
          oldLocalMapping = _.find(domainMappings, function(oldMapping){return oldMapping.signature == payload.signature;});
          if (oldLocalMapping) {
            domainMappings = _.filter(domainMappings, function(oldMapping){return oldMapping.signature != payload.signature;});

            // copy right-click flag from old local mapping as new field mapping should not overwrite old field mapping on same form.
            _.each(oldLocalMapping.fields, function(oldField){
              if (oldField.right_click) {
                var newField = _.find(payload.fields, function(fld){return fld.signature == oldField.signature});
                if (newField) {
                  newField.right_click = true;
                }
              }
            });
          }
        } else {
          domainMappings = [];
        }
        if (request.payload.changed) {
          var localMapping = {
            domain: domain,
            signature: payload.signature,
            formType: payload.formType,
            fields: []
          };
          _.each(payload.fields, function(field){
            localMapping.fields.push({
              signature: field.signature,
              dataType: field.dataType,
              right_click: !!field.right_click
            });
          });
          domainMappings.push(localMapping);
        }
        bucketMappings[domain] = domainMappings;
        localStorage.setItem('lMapping-'+bucket, JSON.stringify(bucketMappings));

        storage.Preference.findBy('key', 'shouldSendErrorReports', _.bind(function(sendData) {
          if (!sendData || sendData.isTrue()) {
            // for pushing to server later
            var toPush = localStorage.getItem('mapping-to-push');
            if (toPush) {
              try {
                toPush = JSON.parse(toPush);
              } catch(e){
                toPush = {};
              }
            } else {
              toPush = {};
            }
            if (request.payload.changed) {

              toPush[payload.signature] = payload;
              _.each(payload.fields, function (field) {
                // trim field labels
                if (field.label) {
                  field.label = field.label.replace(/(^\s+)|(\s+$)/g, '').replace(/\s+/g, ' ');
                }
              });
            } else {
              delete toPush[payload.signature];
            }
            localStorage.setItem('mapping-to-push', JSON.stringify(toPush));
          } else {
            localStorage.removeItem('mapping-to-push');
          }
        }, this));

        // clear cache
        this.clearCache();

        function wrapCallback() {
          if(request.returnJSON){
            request.returnJSON([]);
          } else if(callback){
            callback();
          }
        }

        storage.Preference.findBy('key', 'mapping:mapper', function(mapper){
          storage.Preference.findBy('key', 'mapping:reviewer', function(reviewer){
            if (mapper || reviewer) {
            console.log("**** pushing mapping")
              // review mode, so push immediately
              ABINE_DNTME.require(['modules'], function(modules){
                modules.mappingsModule.forceMappingUpdate(function(){
                  wrapCallback();
                  if (request.payload.formHtml) {
                    var data = {
                      id: request.payload.formSignature,
                      my_mapping: true,
                      mapper: mapper ? mapper.value : null
                    };
                    modules.mappingsModule.getOneFormMappings(data, _.bind(function(mappings){
                      var mapping = _.find(mappings, function(mapping){return mapping.self;});
                      if (mapping && mapping.id) {
                        modules.mappingsModule.sendFormBlob({form_id: mapping.id, html: request.payload.formHtml}, function(){});
                      }
                    }, this));

                  }
                });
              });
            } else {
              console.log("**** not pushing mapping")
              wrapCallback();
            }
          });
        });

      },

      mappingsForSite: function(request, callback){
        var domain = request.params[0];

        var mappings = [];

        // AL: Why are there null domains sometimes?
        if (domain == "null") {
          if(request.returnJSON){
            request.returnJSON([]);
          } else if(callback){
            callback();
          }

          return;
        }

        var cachedMappings = MappedFormCache.fetch(domain);
        if (cachedMappings) {
          LOG_DEBUG && ABINE_DNTME.log.debug("Using cached mappings for " + domain);
          request.returnJSON(cachedMappings);
        }
        else {
          storage.MappedForm.getByDomain(domain, function(forms) {
            LOG_DEBUG && ABINE_DNTME.log.debug("Adding mappings for " + domain + " to cache");
            MappedFormCache.add(domain, forms);
            request.returnJSON(forms);
          });
        }
      },

      localMappingForSite: function(request, callback){
        var domain = request.params[0];
        storage.MappedForm.getByDomain(domain, function(forms) {
          request.returnJSON(forms);
        }, true);
      },

      getByDomain: function(domain, callback, localOnly) {
        var bucket = getBucket(domain);
        if (!localOnly) {
          var serverMappings = localStorage.getItem('mapping-'+bucket, null);
          if (serverMappings) {
            serverMappings = JSON.parse(serverMappings);
            serverMappings = serverMappings[domain];
          }
        }
        var localMappings = localStorage.getItem('lMapping-'+bucket, null);
        if (localMappings) {
          localMappings = JSON.parse(localMappings);
          localMappings = localMappings[domain];
        }
        callback(mergeMappings(serverMappings, localMappings));
      },

      getAllForms: function(callback) {
        var buckets = localStorage.getItem('mapping-buckets', null);
        var forms = [];
        if (buckets) {
          buckets = JSON.parse(buckets);
          for (var i=0;i<buckets.length;i++) {
            var mappings = localStorage.getItem('mapping-'+buckets[i], null);
            if (mappings) {
              mappings = JSON.parse(mappings);
              for (var domain in mappings) {
                var domainForms = mappings[domain];
                for (var j=0;j<domainForms.length;j++) {
                  forms.push(domainForms[j]);
                }
              }
            }
          }
        }
        callback(forms);
      },

      saveAllForms: function(forms) {
        var buckets = {};
        var bucketNames = [];
        for (var i=0;i<forms.length;i++) {
          var form = forms[i];
          var bucket = getBucket(form.domain);
          if (!(bucket in buckets)) {
            buckets[bucket] = {};
            bucketNames.push(bucket);
          }
          if (!(form.domain in buckets[bucket])) {
            buckets[bucket][form.domain] = [];
          }
          buckets[bucket][form.domain].push(form);
        }
        // remove all old mappings
        for (var i=0;i<256;i++) {
          localStorage.removeItem('mapping-'+((i < 16) ? '0'+i.toString(16) : i.toString(16)));
        }
        localStorage.setItem('mapping-buckets', JSON.stringify(bucketNames));
        var delay=0;
        for (var bucket in buckets) {
          timer.setTimeout((function(bucket){
            return function(){
              localStorage.setItem('mapping-'+bucket, JSON.stringify(buckets[bucket]));
            };
          })(bucket), delay);
          delay = delay + 20;
        }
        this.clearCache();
      },

      clearForms: function() {
        var buckets = localStorage.getItem('mapping-buckets', null);
        if (buckets) {
          localStorage.removeItem('mapping-buckets');
          buckets = JSON.parse(buckets);
          for (var i=0;i<buckets.length;i++) {
            localStorage.removeItem('mapping-'+buckets[i]);
          }
        }
      },

      clearCache: function() {
        delete MappedFormCache;
        MappedFormCache = new LRUCache(CACHE_SIZE);
      }
    });

    storage.MappedForm = MappedForm;
    return MappedForm;

  });

ABINE_DNTME.define("abine/storage/note",
    ['documentcloud/underscore', 'persistence', 'abine/storageMixins',
      'abine/emailServer', 'storage', 'abine/config'],
    function(_, persistence, abineStorageMixins, emailServer, storage, config ) {

  var Note = persistence.define('Note', {
    label: "TEXT",
    domain: "TEXT",
    description: "TEXT",
    pushed: "INTEGER",
    createdAt: "DATE",
    modifiedAt: "DATE"
  });

  Note.enableCaching();

  // Extend with storage mixins, to create default CRUD operations
  // Define the basic routes
  _.extend(Note, abineStorageMixins.DefaultActions, {
    routes: {
      'read/note/all':        'index'
    }
  });

  Note.useEncryption(['label', 'description', 'domain']);
  Note.useSyncStorage();

  storage.Note = Note;

  return Note;

});


ABINE_DNTME.define('abine/storage/pageData',
  ['documentcloud/underscore', 'persistence', 'abine/storageMixins',
    'storage', 'abine/localStorage'],
  function(_, persistence, abineStorageMixins, storage, localStorage) {

    var PageData = persistence.define('PageData', {
      domain: "TEXT"
    });

    _.extend(PageData, abineStorageMixins.DefaultActions, {
      routes: {
        'create/pageData':    'createLocal'
      },

      createLocal: function(request, callback) {
        storage.Preference.findBy('key', 'shouldSendErrorReports', _.bind(function(sendData) {
          if (!sendData || sendData.isTrue()) {
            // for pushing to server later
            var toPush = localStorage.getItem('pageData-to-push');
            if (toPush) {
              try {
                toPush = JSON.parse(toPush);
              } catch(e) {
                toPush = {};
              }
            } else {
              toPush = {};
            }

            var payload = request.payload;
            var key = payload.domain;
            // format for value "page_type:page_views:login:register:checkout:login_fill:register_fill:checkout_fill"
            var val = toPush[key] || '0:0:0:0:0:0:0:0';
            var parts = val.split(':');
            var page_type = '0'; // 0 for website and 1 for android apps.
            var page_views = parseInt(parts[1]);
            var login = parseInt(parts[2]);
            var register = parseInt(parts[3]);
            var checkout = parseInt(parts[4]);
            var login_fill = parseInt(parts[5]);
            var register_fill = parseInt(parts[6]);
            var checkout_fill = parseInt(parts[7]);
            if (payload.page_view) {
              page_views++;
            }
            if (payload.login) {
              login++;
            }
            if (payload.register) {
              register++;
            }
            if (payload.checkout) {
              checkout++;
            }
            if (payload.login_fill) {
              login_fill++;
            }
            if (payload.register_fill) {
              register_fill++;
            }
            if (payload.checkout_fill) {
              checkout_fill++;
            }
            toPush[key] = page_type+':'+page_views+':'+login+':'+register+':'+checkout+':'+login_fill+':'+register_fill+':'+checkout_fill;

            localStorage.setItem('pageData-to-push', JSON.stringify(toPush));
          } else {
            localStorage.removeItem('pageData-to-push');
          }
        }, this));

        if (callback) {
          callback({});
        }
      }
    });

    storage.PageData = PageData;
    return PageData;

  });


ABINE_DNTME.define('abine/storage/panelActivity',
  ['documentcloud/underscore', 'persistence', 'abine/storageMixins',
    'storage'],
  function(_, persistence, abineStorageMixins, storage) {

    var PanelActivity = persistence.define('PanelActivity', {
      panelId: "INTEGER",
      impressions: "INTEGER",
      clicks: "INTEGER",
      users: "INTEGER",
      completions: "INTEGER",
      masks: "INTEGER",
      discloses: "INTEGER",
      closes: "INTEGER"
    });

    PanelActivity.enableCaching('panelId');

    _.extend(PanelActivity.prototype, {
      reset: function(sentData){
        if (sentData) {
          this.impressions -= sentData.impressions;
          this.clicks -= sentData.clicks;
          this.completions -= sentData.completions;
          this.users -= sentData.users;
          this.masks -= sentData.masks;
          this.discloses -= sentData.discloses;
          this.closes -= sentData.closes;
        } else {
          this.impressions = 0;
          this.clicks = 0;
          this.completions = 0;
          this.users = 0;
          this.masks = 0;
          this.discloses = 0;
          this.closes = 0;
        }
        persistence.flush();
      },
      hasData: function(){
        return ((this.impressions + this.clicks + this.completions + this.masks + this.discloses + this.closes) > 0);
      },

      isMarketingPanel: function(){
        return this.panelId < 1000000;
      }
    });

    _.extend(PanelActivity, abineStorageMixins.DefaultActions, {
      routes: {
        'read/panelActivity/all':   'index',
        'read/panelActivity/:id':   'showByPanelId',
        'create/panelActivity':    'create',
        'update/panelActivity/click': 'addClick',
        'update/panelActivity/conversion': 'addConversion',
        'update/panelActivity/impression': 'addImpression',
        'update/panelActivity/masked' : 'addMasked',
        'update/panelActivity/disclosed' : 'addDisclosed',
        'update/panelActivity/closed' : 'addClosed',
        'update/panelActivity/:id':  'update',
        'delete/panelActivity/:id':  'destroy'
      },

      createPanelActivity: function(id){
        var panelActivity = new storage.PanelActivity({
          panelId: id,
          impressions: 0,
          users:0,
          clicks:0,
          masks:0,
          discloses:0,
          closes:0,
          completions:0
        });

        persistence.add(panelActivity);

        return panelActivity;
      },

      updateMetric: function (request, type, callback) {
        storage.PanelActivity.findBy('panelId', request.payload.panelId, _.bind(function (panelActivity) {
          panelActivity = panelActivity || this.createPanelActivity(request.payload.panelId);
          if (request.payload.revert) {
            panelActivity[type]--;
          } else {
            panelActivity[type]++;
          }
          persistence.flush();

          if (callback) {
            callback.call(this, panelActivity);
          } else if (request.returnEntities) {
            request.returnEntities(panelActivity);
          }
        }, this));
      },

      addImpression: function(request, callback){
        this.updateMetric(request, 'impressions', callback);
      },

      addClick: function(request, callback){
        this.updateMetric(request, 'clicks', callback);
      },

      addMasked: function(request, callback){
        this.updateMetric(request, 'masks', callback);
      },

      addDisclosed: function(request, callback){
        this.updateMetric(request, 'discloses', callback);
      },

      addClosed: function(request, callback){
        this.updateMetric(request, 'closes', callback);
      },

      addConversion: function(request, callback){
        this.updateMetric(request, 'completions', callback);
      },

      showByPanelId: function(request, callback){
        storage.PanelActivity.findBy('panelId',request.params[0], function(panelActivity){
          if (callback) {
            callback.call(this, panelActivity);
          } else if (request.returnEntities) {
            request.returnEntities(panelActivity);
          }
        });
      }
    });

    PanelActivity.useLocalStorage();

    storage.PanelActivity = PanelActivity;
    return PanelActivity;

  });


// User preferences



ABINE_DNTME.define('abine/storage/preference',
    ['documentcloud/underscore', 'persistence', 'abine/storageMixins',
      'storage', 'abine/config', 'persistence/caching'],
    function(_, persistence, abineStorageMixins, storage, config) {

  var emptyCallback = function() {};

  var Preference = persistence.define('Preferences', {
    key: "TEXT",
    value: "TEXT"
  });

  Preference.enableCaching('key');

  _.extend(Preference.prototype, {
    setTrue: function() {
      this.value = 'true';
    },
    setFalse: function() {
      this.value = 'false';
    },
    isTrue: function() {
      return this.value == true || this.value === 'true';
    },
    isFalse: function() {
      return !this.isTrue();
    },
    isEmpty: function(){
      return !this.value || this.value == "" || (this.value+"").toLowerCase() == "null";
    }
  });

  _.extend(Preference, abineStorageMixins.DefaultActions, {

    routes: {
      'read/preference/all':       'index',
      'read/preference/name/:key': 'showOneByKey',
      'read/preference/prefspage': 'showForPrefsPage',
      'read/preference/:id':       'show',
      'create/preference':         'create',
      'update/preference/restore': 'restore',
      'update/preference/:id':     'update',
      'delete/preference/:id':     'destroy'
    },

    // Routes

    showOneByKey: function(request, callback) {
      Preference.findBy('key', request.params[0], function(entity) {
        if (callback) {
          callback.call(this, entity);
        } else if (request && request.returnEntities) {
          request.returnEntities(entity);
        }
      });
    },

    // Class

    // returns preference object if its cached, otherwise null (never hit db)
    getSync: function(key) {
      var pref = null;
      if (storage.Preference.isCached())
        storage.Preference.findBy('key', key, function(p){pref=p;});
      return pref;
    },

    findOrCreate: function(data, callback) {
      callback = callback || emptyCallback;
      storage.Preference.findBy('key', data.key, function(pref) {
        if (pref) {
          callback(pref);
        } else {
          var newPref = new storage.Preference(data);
          persistence.add(newPref);
          persistence.flush(function() { 
            callback(newPref); 
          });
        }
      });
    },
    
    removeByKey: function(key, callback) {
      callback = callback || emptyCallback;
      storage.Preference.findBy('key', key, function(pref) {
        if (pref) {
          persistence.remove(pref);
          persistence.flush(function() {
            callback(pref);
          });
        } else {
          callback(pref);
        }
      });
    },

    createOrModify: function(data, callback) {
      callback = callback || emptyCallback;
      storage.Preference.findBy('key', data.key, function(pref) {
        if (pref) {
          pref.value = data.value;
        } else {
          pref = new storage.Preference(data);
          persistence.add(pref);
        }
        persistence.flush(function() {
          callback(pref);
        });
      });
    },

    createOrIncrementByInteger: function(data, callback) {
      callback = callback || emptyCallback;
      storage.Preference.findBy('key', data.key, function(pref) {
        if (pref) {
          if (pref.value === parseInt(pref.value)) {
            pref.value = pref.value + data.value;
          }
        } else {
          pref = new storage.Preference(data);
          persistence.add(pref);
        }
        persistence.flush(function() {
          callback(pref);
        });
      });
    },

    decrementByIntegerIfExists: function(data, callback) {
      callback = callback || emptyCallback;
      storage.Preference.findBy('key', data.key, function(pref) {
        if (pref) {
          if (pref.value === parseInt(pref.value)) {
            pref.value = pref.value - data.value;
            if(data.options && data.options.nonNegative){
              if(pref.value < 0){
                pref.value = 0;
              }
            }
          }
        }
        persistence.flush(function() {
          callback(pref);
        });
      });
    },

    batchFindBy: function(prefs, callback){
      callback = callback || emptyCallback;

      var prefMap = {};

      _.async.forEach(prefs, _.bind(function(pref, cb){
        this.findBy(pref.by, pref.value, function(p){
          prefMap[pref.value] = p;
          cb();
        })
      },this), function(err){
        callback(prefMap);
      });
    },

    batchRemoveBy: function(prefs, callback){
      callback = callback || emptyCallback;

      _.async.forEach(prefs, _.bind(function(pref, cb){
        this.findBy(pref.by, pref.value, function(p){
          if (p) {
            persistence.remove(p);
          }
          cb();
        })
      },this), function(err){
        persistence.flush(function(){
          callback();
        });
      });
    },

    batchCreateOrModify: function(prefs, callback){
      callback = callback || emptyCallback;

      _.async.forEach(prefs, _.bind(function(pref, cb){
        this.createOrModify(pref, function(){cb();});
      },this), function(err){
        callback();
      });
    },

    retainDefaults: function(callback) {
      var keyFilter = function(pref){ return pref.key in {entitledToSync:1,entitledToPayments:1,entitledToPhone:1,hasEnabledPayments:1}; };

      storage.Preference.listCached(keyFilter, _.bind(function(prefs){
        _.each(prefs, function(pref){persistence.remove(pref);})
        persistence.flush(function(){callback();});
      }, this));
    },

    setupUpgradeDefaults: function(callback) {
      callback = callback || emptyCallback;

      var prefMap = {};

      _.async.forEach(Preference._upgradeDefaults, _.bind(function(pref, cb){
        this.findBy('key', pref.key, function(p){
          prefMap[pref.key] = p;
          cb();
        })
      },this), function(){
        _.each(Preference._upgradeDefaults, _.bind(function(pref){
          if (!prefMap[pref.key]) {
            persistence.add(new Preference(pref));
          }
          persistence.flush(function(){callback()});
        }, this));
      });
    }
  });

  Preference.useLocalStorage();

  // fresh install defaults
  Preference._defaults = [
    {key: 'freshInstall', value: 'true'},
    {key: 'showNotifications', value: 'true'},
    {key: 'helpMeLogin', value: 'true'},
    {key: 'showMedals', value: config.silent?'false':'true'},
    {key: 'suggestProtectedEmail', value: config.silent?'false':'true'},
    {key: 'promptWhenSavingAccounts', value: config.silent?'false':'true'},
    {key: 'promptWhenSavingCards', value: config.silent?'false':'true'},
    {key: 'suggestStrongPassword', value: config.silent?'false':'true'},
    {key: 'suggestProtectedPhone', value: config.silent?'false':'true'},
    {key: 'suggestProtectedCard', value: config.silent?'false':'true'},
    {key: 'suggestRealCard', value: config.silent?'false':'true'},
    {key: 'suggestAddresses', value: config.silent?'false':'true'},
    {key: 'suggestIdentities', value: config.silent?'false':'true'},
    {key: 'confirmProtectedCard', value: 'true'},
    {key: 'shouldSendErrorReports', value:'true'},
    {key: 'shouldSendShoppingFeedback', value:'true'},
    {key: 'showMapper', value:'false'},
    {key: 'showTrackersOnload', value:config.silent?'false':'false'},
    {key: 'showInTheKnowGrowls', value:'true'},
    {key: 'showPremiumSubscriptionPanel', value:'true'},
    {key: 'rememberAccounts', value:config.silent?'false':'true'},
    {key: 'submitAccount', value:'true'},
    {key: 'rememberCards', value:config.silent?'false':'true'},
    {key: 'showWeeklyReports', value:config.silent?'false':'true'},
    {key: 'footerCampaign', value:Math.random()>0.5?'A':'B'},
    {key: 'countNotifications', value:'0'},
    {key: 'showCheckoutPanelGrowl', value: 'false'},
    {key: 'splitTests', value: ""},
    {key: 'dnt_language', value: "en"}
  ];

  // upgrade defaults
  Preference._upgradeDefaults = [
    {key: 'showPrivateSearchState', value: 'true'},
    {key: 'suggestRealCard', value: config.silent?'false':'true'},
    {key: 'suggestIdentities', value: config.silent?'false':'true'},
    {key: 'submitAccount', value:'true'},
    {key: 'promptWhenSavingAccounts', value: config.silent?'false':'true'},
    {key: 'promptWhenSavingCards', value: config.silent?'false':'true'},
    {key: 'shouldSendShoppingFeedback', value: 'true'},
    {key: 'dnt_language', value: "en"}
  ];

  storage.Preference = Preference;
  return Preference;

});

ABINE_DNTME.define("abine/storage/realCard",
    ['documentcloud/underscore', 'persistence', 'abine/storageMixins',
      'storage', 'abine/config', 'abine/ajax', 'abine/localStorage'],
    function(_, persistence, abineStorageMixins, storage, config, abineAjax, localStorage) {

  var cardRules = {
  "version": 5,
  "rules": {
    "visa": {
      "prefixes": [4],
      "amair": {
        "name": "AAdvantage",
        "bank": "no-match",
        "style": "amair"
      },
      "bofa_cash": {
        "group": "Bank of America",
        "name": "Cash Rewards",
        "bank": "no-match",
        "style": "chip_card_1"
      },
      "bofa_travel": {
        "group": "Bank of America",
        "name": "Travel Rewards",
        "bank": "no-match",
        "style": "chip_card_1"
      },
      "bofa_americard": {
        "group": "Bank of America",
        "name": "BankAmericard",
        "bank": "no-match",
        "style": "chip_card_1"
      },
      "bofa_default": {
        "group": "Bank of America",
        "name": "Others",
        "bank": "bank.*of.*america",
        "style": "chip_card_1"
      },
      "barclays_default": {
        "name": "Barclays Bank",
        "bank": "barclays.*bank",
        "style": "chip_card_1"
      },
      "capone_business_platinum": {
        "group": "Capital One",
        "name": "Business Platinum",
        "bank": "no-match",
        "style": "capone_business_platinum"
      },
      "capone_quicksilver": {
        "group": "Capital One",
        "name": "Quicksilver",
        "bank": "no-match",
        "style": "capone_quicksilver"
      },
      "capone_venture": {
        "group": "Capital One",
        "name": "Venture",
        "bank": "no-match",
        "style": "capone_venture"
      },
      "capone_default": {
        "group": "Capital One",
        "name": "Others",
        "bank": "capital.*one",
        "style": "chip_card_1"
      },
      "chase_freedom":  {
        "group": "Chase",
        "name": "Freedom",
        "bank": "no-match",
        "style": "chase"
      },
      "chase_sapphire":  {
        "group": "Chase",
        "name": "Sapphire",
        "bank": "no-match",
        "style": "chase_sapphire"
      },
      "chase_slate":  {
        "group": "Chase",
        "name": "Slate",
        "bank": "no-match",
        "style": "chase_slate"
      },
      "chase_united":  {
        "group": "Chase",
        "name": "United",
        "bank": "no-match",
        "style": "chase"
      },
      "chase_marriott":  {
        "group": "Chase",
        "name": "Marriott Rewards",
        "bank": "no-match",
        "style": "chip_card_2"
      },
      "chase_default":  {
        "group": "Chase",
        "name": "Others",
        "bank": "chase.*bank",
        "style": "chase"
      },
      "citi_rewards":  {
        "group": "Citi",
        "name": "Rewards",
        "bank": "no-match",
        "style": "citi"
      },
      "citi_ty_prem":  {
        "group": "Citi",
        "name": "Thankyou Premiere",
        "bank": "no-match",
        "style": "citi"
      },
      "citi_default":  {
        "group": "Citi",
        "name": "Others",
        "bank": "no-match",
        "style": "citi"
      },
      "first_default":  {
        "name": "First National Bank",
        "bank": "first.*national.*bank",
        "style": "chip_card_1"
      },
      "gecap_default":  {
        "name": "GE Capital",
        "bank": "no-match",
        "style": "chip_card_1"
      },
      "greendot_visa":  {
        "name": "Green Dot",
        "bank": "no-match",
        "style": "greendot"
      },
      "marriott":  {
        "name": "Marriott Rewards",
        "bank": "no-match",
        "style": "chip_card_2"
      },
      "southwest":  {
        "name": "Southwest Rewards",
        "bank": "no-match",
        "style": "chip_card_2"
      },
      "pnc_default":  {
        "name": "PNC Bank",
        "bank": "pnc.*bank",
        "style": "chip_card_1"
      },
      "usaa_default":  {
        "name": "USAA",
        "bank": "usaa.*bank",
        "style": "chip_card_1"
      },
      "usbank_default":  {
        "name": "US Bank",
        "bank": "us bank",
        "style": "chip_card_1"
      },
      "wellsfargo_default":  {
        "name": "Wells Fargo Bank",
        "bank": "wells.*fargo",
        "style": "chip_card_1"
      },
      "visa_generic": {
        "name": "Visa",
        "bank": ".*",
        "style": "generic"
      }
    },
    "mastercard": {
      "prefixes": [51, 52, 53, 54, 55],
      "bofa_mlb": {
        "group": "Bank of America",
        "name": "MLB",
        "bank": "no-match",
        "style": "chip_card_1"
      },
      "bofa_default": {
        "group": "Bank of America",
        "name": "Others",
        "bank": "bank.*of.*america",
        "style": "chip_card_1"
      },
      "barclays_default": {
        "name": "Barclays Bank",
        "bank": "barclays.*bank",
        "style": "chip_card_1"
      },
      "capone_default": {
        "name": "Capital One",
        "bank": "capital.*one",
        "style": "chip_card_1"
      },
      "chase_default":  {
        "name": "Chase",
        "bank": "chase.*bank",
        "style": "chase"
      },
      "citi_biz":  {
        "group": "Citi",
        "name": "Business",
        "bank": "no-match",
        "style": "citi"
      },
      "citi_cashback":  {
        "group": "Citi",
        "name": "Cash Back",
        "bank": "no-match",
        "style": "citi"
      },
      "citi_doublecash":  {
        "group": "Citi",
        "name": "Double Cash",
        "bank": "no-match",
        "style": "citi_doublecash"
      },
      "citi_prof":  {
        "group": "Citi",
        "name": "Professional",
        "bank": "no-match",
        "style": "citi"
      },
      "citi_simplicity":  {
        "group": "Citi",
        "name": "Simplicity",
        "bank": "no-match",
        "style": "citi"
      },
      "citi_ty_pref":  {
        "group": "Citi",
        "name": "Thankyou Preferred",
        "bank": "no-match",
        "style": "citi"
      },
      "citi_default":  {
        "group": "Citi",
        "name": "Others",
        "bank": "no-match",
        "style": "citi"
      },
      "first_default":  {
        "name": "First National Bank",
        "bank": "first.*national.*bank",
        "style": "chip_card_1"
      },
      "gecap_default":  {
        "name": "GE Capital",
        "bank": "no-match",
        "style": "chip_card_1"
      },
      "greendot_mc":  {
        "name": "Green Dot",
        "bank": "no-match",
        "style": "greendot"
      },
      "neteller_mc":  {
        "name": "Netellar",
        "bank": "no-match",
        "style": "chip_card_1"
      },
      "optimal_mc":  {
        "name": "Optimal Payments",
        "bank": "no-match",
        "style": "chip_card_3"
      },
      "pnc_default":  {
        "name": "PNC Bank",
        "bank": "pnc.*bank",
        "style": "chip_card_1"
      },
      "skrill_mc":  {
        "name": "Skrill",
        "bank": "no-match",
        "style": "chip_card_3"
      },
      "usaa_default":  {
        "name": "USAA",
        "bank": "usaa.*bank",
        "style": "chip_card_1"
      },
      "usbank_default":  {
        "name": "US Bank",
        "bank": "us bank",
        "style": "chip_card_1"
      },
      "wellsfargo_default":  {
        "name": "Wells Fargo Bank",
        "bank": "wells.*fargo",
        "style": "chip_card_1"
      },
      "master_generic": {
        "name": "Master Card",
        "bank": ".*",
        "style": "generic"
      }
    },
    "american_express": {
      "prefixes": [34, 37],
      "amex_platinum": {
        "name": "American Express Platinum",
        "bank": "american.*express",
        "category": "platinum",
        "style": "amex_platinum"
      },
      "amex_gold": {
        "name": "American Express Gold",
        "bank": "american.*express",
        "category": "gold",
        "style": "chip_card_1"
      },
      "amex_cash": {
        "name": "American Express Cash",
        "bank": "no-match",
        "style": "amex_cash"
      },
      "britair": {
        "name": "British Airways",
        "bank": "british.*airways",
        "style": "chip_card_1"
      },
      "amex_delta": {
        "name": "Delta Skymiles",
        "bank": "delta.*sky.*miles",
        "style": "chip_card_1"
      },
      "amex_generic": {
        "name": "American Express",
        "bank": ".*",
        "style": "generic"
      }
    },
    "discover": {
      "prefixes": [6011, 62, 64, 65],
      "discover_it": {
        "name": "Discover IT",
        "bank": "no-match",
        "style": "discover_it"
      },
      "discover_generic": {
        "name": "Discover",
        "bank": ".*",
        "style": "generic"
      }
    },
    "diners_club": {
      "prefixes": [305, 36, 38],
      "diners_generic": {
        "name": "Diners Club",
        "bank": ".*",
        "style": "generic"
      }
    },
    "carte_blanche": {
      "prefixes": [300, 301, 302, 303, 304, 305]
    },
    "jcb": {
      "prefixes": [35]
    },
    "enroute": {
      "prefixes": [2014, 2149]
    },
    "maestro": {
      "prefixes": [5018, 5020, 5038, 6304, 6759, 6761]
    },
    "laser": {
      "prefixes": [6304, 6706, 6771, 6709]
    },
    "all": {
      "homedepot": {
        "name": "Home Depot",
        "bank": "no-match",
        "style": "homedepot"
      },
      "kohls": {
        "name": "Kohls",
        "bank": "no-match",
        "style": "kohls"
      },
      "generic": {
        "name": "Others",
        "bank": "no-match",
        "style": "generic"
      }
    }
  },

  "styles": {
    "generic": {
      "base": {"font-size": 15,"color": "white"},
      "number": {"top": 73, "left": 32, "font-size": 28},
      "name": {"top": 175, "left": 20},
      "expiry": {"top": 125, "left": 200},
      "cvc": {"top": 125, "left": 50}
    },
    "amex_platinum": {
      "base": {"font-size": 15,"color": "white"},
      "number": {"top": 120, "left": 32, "font-size": 28, "padding": 5, "background-color": "rgba(0,0,0,0.5)", "border-radius": 5},
      "name": {"top": 180, "left": 40},
      "expiry": {"top": 180, "left": 200},
      "cvc": {"top": 160, "left": 200}
    },
    "amex_cash": {
      "base": {"font-size": 15,"color": "white"},
      "number": {"top": 120, "left": 32, "font-size": 28, "padding": 5, "background-color": "rgba(0,0,0,0.5)", "border-radius": 5},
      "name": {"top": 180, "left": 40, "color": "black"},
      "expiry": {"top": 180, "left": 235, "color": "black"},
      "cvc": {"top": 160, "left": 235, "color": "black"}
    },
    "amair": {
      "base": {"font-size": 15,"color": "white"},
      "number": {"top": 110, "left": 32, "font-size": 28},
      "name": {"top": 180, "left": 20},
      "expiry": {"top": 180, "left": 150},
      "cvc": {"top": 160, "left": 150}
    },
    "chip_card_1": {
      "base": {"font-size": 15,"color": "white"},
      "number": {"top": 120, "left": 42, "font-size": 28},
      "name": {"top": 180, "left": 30},
      "expiry": {"top": 180, "left": 156},
      "cvc": {"top": 160, "left": 150}
    },
    "chip_card_2": {
      "base": {"font-size": 15,"color": "white"},
      "number": {"top": 125, "left": 32, "font-size": 28},
      "name": {"top": 180, "left": 20},
      "expiry": {"top": 180, "left": 150},
      "cvc": {"top": 160, "left": 150}
    },
    "chip_card_3": {
      "base": {"font-size": 15,"color": "black"},
      "number": {"top": 125, "left": 32, "font-size": 28},
      "name": {"top": 180, "left": 20},
      "expiry": {"top": 180, "left": 150},
      "cvc": {"top": 160, "left": 150}
    },
    "capone_business_platinum": {
      "base": {"font-size": 15,"color": "black"},
      "number": {"top": 93, "left": 32, "font-size": 28},
      "name": {"top": 180, "left": 120},
      "expiry": {"top": 135, "left": 200},
      "cvc": {"top": 137, "left": 50}
    },
    "capone_quicksilver": {
      "base": {"font-size": 15,"color": "white"},
      "number": {"top": 113, "left": 38, "font-size": 28},
      "name": {"top": 180, "left": 20},
      "expiry": {"top": 140, "left": 164},
      "cvc": {"top": 160, "left": 164}
    },
    "capone_venture": {
      "base": {"font-size": 15,"color": "white"},
      "number": {"top": 110, "left": 35, "font-size": 28},
      "name": {"top": 180, "left": 20},
      "expiry": {"top": 180, "left": 230},
      "cvc": {"top": 160, "left": 228}
    },
    "chase_sapphire": {
      "base": {"font-size": 15,"color": "white"},
      "number": {"top": 130, "left": 35, "font-size": 28},
      "name": {"top": 180, "left": 20},
      "expiry": {"top": 180, "left": 230},
      "cvc": {"top": 160, "left": 228}
    },
    "chase": {
      "base": {"font-size": 15,"color": "white"},
      "number": {"top": 125, "left": 32, "font-size": 28},
      "name": {"top": 180, "left": 20},
      "expiry": {"top": 180, "left": 150},
      "cvc": {"top": 160, "left": 150}
    },
    "chase_slate": {
      "base": {"font-size": 15,"color": "white"},
      "number": {"top": 125, "left": 32, "font-size": 28},
      "name": {"top": 180, "left": 20},
      "expiry": {"top": 180, "left": 200},
      "cvc": {"top": 160, "left": 200}
    },
    "citi_doublecash": {
      "base": {"font-size": 15,"color": "white"},
      "number": {"top": 125, "left": 32, "font-size": 28},
      "name": {"top": 180, "left": 20},
      "expiry": {"top": 180, "left": 200},
      "cvc": {"top": 160, "left": 200}
    },
    "citi": {
      "base": {"font-size": 15,"color": "white"},
      "number": {"top": 120, "left": 32, "font-size": 28},
      "name": {"top": 180, "left": 20},
      "expiry": {"top": 180, "left": 150},
      "cvc": {"top": 160, "left": 150}
    },
    "discover_it": {
      "base": {"font-size": 15,"color": "white"},
      "number": {"top": 110, "left": 35, "font-size": 28},
      "name": {"top": 180, "left": 20},
      "expiry": {"top": 180, "left": 230},
      "cvc": {"top": 160, "left": 228}
    },
    "homedepot": {
      "base": {"font-size": 15,"color": "white"},
      "number": {"top": 85, "left": 35, "font-size": 28},
      "name": {"top": 180, "left": 20},
      "expiry": {"top": 180, "left": 170},
      "cvc": {"top": 160, "left": 179}
    },
    "greendot": {
      "base": {"font-size": 15,"color": "white","text-shadow": "1px 1px 1px #000"},
      "number": {"top": 93, "left": 32, "font-size": 28},
      "name": {"top": 180, "left": 20},
      "expiry": {"top": 135, "left": 150},
      "cvc": {"top": 137, "left": 20}
    },
    "kohls": {
      "base": {"font-size": 15,"color": "white"},
      "number": {"top": 90, "left": 60, "font-size": 28},
      "name": {"top": 180, "left": 60},
      "expiry": {"top": 180, "left": 230},
      "cvc": {"top": 160, "left": 228}
    }
  },

  "offers": [
    {"id": "homedepot", "brand": "homedepot", "domain": "homedepot.com", "url":"https://www.citicards.com/cards/acq/Apply.do?screenID=3211&app=UNSOL&sc=30005&langId=EN&siteId=PLCN_HOMEDEPOT"},
    {"id": "britair", "brand": "britair", "domain": "britishairways.com", "url":"https://creditcards.chase.com/credit-cards/british-airways-credit-card.aspx"}
  ]
};

  function detectCardType(cardNumber) {
    cardNumber = cardNumber.replace(/\D/g, '');
    var cards = cardRules.rules;

    for (var cardType in cards) {
      if (cards.hasOwnProperty(cardType) && 'prefixes' in cards[cardType]) {
        var cardTypeRules = cards[cardType];
        for (var pi = 0, pl = cardTypeRules.prefixes.length; pi < pl; ++pi) {
          if (cardTypeRules.prefixes.hasOwnProperty(pi)) {
            var prefix = cardTypeRules.prefixes[pi];
            if (new RegExp('^' + prefix.toString()).test(cardNumber))
              return cardType;
          }
        }
      }
    }

    return false;
  }

  var bankCache = {};
  function detectCardBrand(cardNumber, callback) {
    if (!cardNumber) {
      callback({});
      return;
    }
    cardNumber = cardNumber.replace(/\D/g, '');
    var prefix = cardNumber.substr(0, 6);
    var type = detectCardType(cardNumber);
    function wrapCallback(data) {
      var brand = type+'_generic';
      if (data && data.bin) {
        if (type in cardRules.rules) {
          var rules = cardRules.rules[type];
          for (var brandName in rules) {
            var brandRule = rules[brandName];
            if (brandRule.bank && data.bank.match(brandRule.bank)) {
              if (brandRule.category) {
                if (data.card_category && data.card_category.match(brandRule.category)) {
                  brand = brandName;
                  break;
                }
              } else {
                brand = brandName;
              }
            }
          }
        }
      }
      callback(brand);
    }

    if (!(prefix in bankCache)) {
      abineAjax.ajax({
        url: 'https://www.binlist.net/json/'+prefix,
        success: function(response){
          bankCache[prefix] = response;
          wrapCallback(response)
        },
        error: function(response){
          bankCache[prefix] = {};
          wrapCallback({})
        }
      });
    } else {
      wrapCallback(bankCache[prefix]);
    }

  }

  function getCardStyle(type, brand) {
    if (!type) type = '';
    if (type.match(/amex/i)) type = 'american_express';
    var types = [type, 'all'], found = null;
    _.each(types, function(type){
      if (found) return;
      if (type in cardRules.rules && brand in cardRules.rules[type]) {
        var style = cardRules.rules[type][brand].style;
        if (style in cardRules.styles) {
          found = cardRules.styles[style];
        }
      }
    });
    if (!found && brand != 'generic') {
      found = getCardStyle(type, 'generic');
    }
    return found || {};
  }

  function setupRules(changed) {
    var newRules;
    try {
      newRules = localStorage.getItem('card-rules');
    } catch(e) {
      // IE takes time to setup the localStorage API.  retry after 500ms.
      setTimeout(function(){setupRules(true)}, 500);
      return;
    }
    if (newRules) {
      newRules = JSON.parse(newRules);
      if (newRules.version > cardRules.version) {
        cardRules = newRules;
        changed = true;
      }
    }
    if (changed) {
      // setup regex
      _.each(cardRules.rules, function(ruleType){
        _.each(ruleType, function(rule){
          if (rule.bank) {
            rule.bank = new RegExp(rule.bank, "i");
          }
          if (rule.category) {
            rule.category = new RegExp(rule.category, "i");
          }
        })
      });
      _.each(cardRules.offers, function(offer){
        if (offer.domain) {
          offer.domain_str = offer.domain;
          offer.domain = new RegExp(offer.domain.replace(/\./g, '\\.'), "i");
        }
        offer.cardType = 'offer';
      });
    }
  }

  // on startup
  setupRules(true);

  var RealCard = persistence.define('RealCard', {
    label: "TEXT",
    first_name: "TEXT",
    last_name: "TEXT",
    number: "TEXT",
    expiry_month: "TEXT",
    expiry_year: "TEXT",
    cvc: "TEXT",
    type: "TEXT",
    brand: "TEXT",
    address_id: "TEXT",
    pushed: "INTEGER",
    createdAt: "DATE",
    modifiedAt: "DATE"
  });

  _.extend(RealCard.prototype, {
    detectCardType: function() {
      this.type = detectCardType(this.number);
    },
    detectCardBrand: function() {
      detectCardBrand(this.number, _.bind(function(brand){
        this.brand = brand;
      }, this));
    },
    getFullName: function() {
      return ((this.first_name||'')+' '+(this.last_name||'')).replace(/^[\s]+|[\s]+$]/g, '')
    },
    getMM: function() {
      var month = parseInt(this.expiry_month);
      return ((month < 10)? '0' : '')+month;
    },
    getYY: function() {
      var year = parseInt(this.expiry_year);
      return year%100;
    },
    getMMYY: function() {
      var month = this.getMM();
      var year = parseInt(this.expiry_year);
      return month+'/'+(year%100);
    },
    getMMYYYY: function() {
      var month = this.getMM();
      var year = parseInt(this.expiry_year);
      return month+'/'+year;
    },
    jsonWithAddress: function(addressMap) {
      var card = this.toJSON();
      if (addressMap && card.address_id && card.address_id in addressMap) {
        card.address = addressMap[card.address_id];
      } else {
        card.address = card.address_id = null;
      }

      card.cardType = "real_card";

      if (!card.type) {
        card.type = detectCardType(card.number) || "";
      }

      if (!card.brand || card.brand == "") {
        if (card.type.match(/visa/)) {
          card.brand = "visa_generic";
        } else if (card.type.match(/master/)) {
          card.brand = "master_generic";
        } else if (card.type.match(/discover/)) {
          card.brand = "discover_generic";
        } else if (card.type.match(/diner/)) {
          card.brand = "diners_generic";
        } else if (card.type.match(/amex|american/)) {
          card.brand = "amex_generic";
        } else {
          card.brand = "generic";
        }
      }

      card.styles = getCardStyle(card.type, card.brand);

      return card;
    }
  });


  RealCard.enableCaching();

  // Extend with storage mixins, to create default CRUD operations
  // Define the basic routes
  _.extend(RealCard, abineStorageMixins.DefaultActions, {
    routes: {
      'read/realCard/all':        'indexWithAddress',
      'read/realCard/site/:id':   'indexWithAddressAndOffers',
      'read/realCard/offers/:id':   'indexOffers'
    },

    detectCardType: function(number) {
      return detectCardType(number);
    },

    detectCardBrand: function(number, callback) {
      detectCardBrand(number, callback);
    },

    getCardStyles: function(type, brand) {
      return getCardStyle(type, brand);
    },

    indexWithAddressAndOffers: function(request, callback){
      ABINE_DNTME.require(['modules'], function(modules){
        storage.RealCard.indexWithAddress(null, function(cards){
          var brands = _.map(cards, function(c) { return c.brand; });
          storage.RealCard.getOfferCards(request, function(offerCards){

            // only add offer card if user doesn't have that brand already
            // e.g. if user has homedepot card don't show homedepot offer
            offerCards.forEach(function(oc) {
              if(brands.indexOf(oc.brand) == -1){
                cards.unshift(oc);
              }
            });

            if (callback) {
              callback.call(this, cards);
            } else {
              request.returnJSON(cards);
            }
          });
        });
      });
    },

    indexOffers: function(request, callback){
      ABINE_DNTME.require(['modules'], function(modules){
        storage.RealCard.index(null, function(cards){
          var brands = _.map(cards, function(c) { return c.brand; });
          var cards = [];
          storage.RealCard.getOfferCards(request, function(offerCards){

            // only add offer card if user doesn't have that brand already
            // e.g. if user has homedepot card don't show homedepot offer
            offerCards.forEach(function(oc) {
              if(brands.indexOf(oc.brand) == -1){
                cards.unshift(oc);
              }
            });

            if (callback) {
              callback.call(this, cards);
            } else {
              request.returnJSON(cards);
            }
          });
        });
      });
    },

    indexWithAddress: function(request, callback) {
      storage.Address.index(null, function(addresses){
        var addressMap = {};
        _.each(addresses, function(address){
          addressMap[address.id] = address.toJSON();
        });
        storage.RealCard.index(null, function(cards){
          var cardsJson = [];
          _.each(cards, function(card){
            cardsJson.push(card.jsonWithAddress(addressMap));
          });
          if (callback) {
            callback.call(this, cardsJson);
          } else {
            request.returnJSON(cardsJson);
          }
        });
      });
    },

    updateCardRules: function() {
      var url = 'https://rules.abine.com/card-rules.json?from=extension';

      abineAjax.ajax({
        url: url,
        success: function(data) {
          if (data && data.version > cardRules.version) {
            localStorage.setItem('card-rules', JSON.stringify(data));
            setupRules();
          }
        }
      });

    },

    getOfferCards: function(request, callback) {
      var domain = request.params[0];
      var offerCards = cardRules.offers;
      var offerCardsForDomain = _.filter(offerCards, function(oc){ return domain.match(oc.domain)});

      var cards = [];
      _.each(offerCardsForDomain, function(card){
        card = _.extend({}, card);
        card.domain = card.domain_str;
        delete card.domain_str;
        cards.push(card);
      });

      if (callback){
        callback.call(this, cards);
      } else {
        request.returnJSON(cards);
      }
    }

  });

  RealCard.useEncryption(['label', 'first_name', 'last_name', 'number', 'expiry_month', 'expiry_year', 'cvc', 'type']);
  RealCard.useSyncStorage();

  storage.RealCard = RealCard;

  return RealCard;

});



// Require all storage entities

ABINE_DNTME.define('abine/storage/registry',
    ['abine/timer', 'abine/storageRouter', 'storage'],
    function(abineTimer, abineStorageRouter, storage) {
  return {

    // Register all storage entities with the router
    registerStorageEntities: function(storageRouter) {
      storageRouter.registerEntity(storage.Blacklist,       'blacklist');
      storageRouter.registerEntity(storage.DisposableCard,  'disposableCard');
      storageRouter.registerEntity(storage.DisposableEmail, 'disposableEmail');
      storageRouter.registerEntity(storage.DisposablePhone, 'disposablePhone');
      storageRouter.registerEntity(storage.LoginUrl,        'loginUrl');
      storageRouter.registerEntity(storage.AutofillFeedback,'autofillFeedback');
      storageRouter.registerEntity(storage.MappedForm,      'mappedForm');
      storageRouter.registerEntity(storage.MappedField,     'mappedField');
      storageRouter.registerEntity(storage.PanelActivity,   'panelActivity');
      storageRouter.registerEntity(storage.Preference,      'preference');
      storageRouter.registerEntity(storage.ShieldedCard,    'shieldedCard');
      storageRouter.registerEntity(storage.ShieldedEmail,   'shieldedEmail');
      storageRouter.registerEntity(storage.ShieldedPhone,   'shieldedPhone');
      storageRouter.registerEntity(storage.TabData,         'tabData');
      storageRouter.registerEntity(storage.Address,         'address');
      storageRouter.registerEntity(storage.Account,         'account');
      storageRouter.registerEntity(storage.Identity,        'identity');
      storageRouter.registerEntity(storage.RealCard,        'realCard');
      storageRouter.registerEntity(storage.Note,            'note');
      storageRouter.registerEntity(storage.PageTracker,     'pageTracker');
      storageRouter.registerEntity(storage.PageData,        'pageData');
      storageRouter.registerEntity(storage.CustomTabData,   'customTabData');
      storageRouter.registerEntity(storage.BlacklistOrPreference,   'blacklistOrPreference');
      storageRouter.registerEntity(storage.GlobalData,      'globalData');
      storageRouter.registerEntity(storage.GlobalTracker,   'globalTracker');
      storageRouter.registerEntity(storage.GlobalPreference,'globalPreference');
      storageRouter.registerEntity(storage.GlobalFeature,   'globalFeature');
      storageRouter.registerEntity(storage.DntStats,        'dntStats');
      storageRouter.registerEntity(storage.TrackerInfo,     'trackerInfo');
      storageRouter.registerEntity(storage.Advt,            'advt');
      storageRouter.registerEntity(storage.ExtensionVersion,'extensionVersion');
    },

    // Erases everything

    emptyDatabase: function(dbname, callback) {
      abineStorageRouter.StorageRouter.initializeWebSQLStorage({
        dbName: "idme_test",
        dbDescription: "Storage for the unit tests",
        dbSizeInBytes: 5 * 1024 * 1024,
        ready: function() {
          LOG_INFO && ABINE_DNTME.log.info("Clearing test database");
          var callbacks = 15, interval;
          storage.Account.all().destroyAll(function() { callbacks--; });
          storage.Address.all().destroyAll(function() { callbacks--; });
          storage.Blacklist.all().destroyAll(function() { callbacks--; });
          storage.RealCard.all().destroyAll(function() { callbacks--; });
          storage.Identity.all().destroyAll(function() { callbacks--; });
          storage.Note.all().destroyAll(function() { callbacks--; });
          storage.DisposableCard.all().destroyAll(function() { callbacks--; });
          storage.DisposableEmail.all().destroyAll(function() { callbacks--; });
          storage.DisposablePhone.all().destroyAll(function() { callbacks--; });
          storage.Preference.all().destroyAll(function() { callbacks--; });
          storage.MappedForm.all().destroyAll(function() { callbacks--; });
          storage.MappedField.all().destroyAll(function() { callbacks--; });
          storage.ShieldedCard.all().destroyAll(function() { callbacks--; });
          storage.ShieldedEmail.all().destroyAll(function() { callbacks--; });
          storage.ShieldedPhone.all().destroyAll(function() { callbacks--; });
          var checkCallback = function() {
            if (callbacks > 0) {
              abineTimer.setTimeout(checkCallback, 50);
            } else {
              LOG_INFO && ABINE_DNTME.log.info("Cleared test database");
              callback.call();
            }
          }
          abineTimer.setTimeout(checkCallback, 50);
        }
      });

    }

  };

});

ABINE_DNTME.define("abine/storage/shieldedCard",
  ['documentcloud/underscore', 'persistence', 'abine/storageMixins',
    'storage', 'abine/eventLogger', 'persistence/caching', 'abine/config', 'abine/licenseServer'],
  function(_, persistence, abineStorageMixins, storage, logger, autoDate, caching, config, licenseServer) {

    var ShieldedCard = persistence.define('ShieldedCards', {
      last_four: "TEXT",
      first_name: "TEXT",
      last_name: "TEXT",
      month: "TEXT",
      year: "TEXT",
      card_type: "TEXT",
      address1: "TEXT",
      address2: "TEXT",
      city: "TEXT",
      state_abbreviation: "TEXT",
      zip: "TEXT",
      createdAt: "DATE",
      modifiedAt: "DATE",
      pushVersion: "INTEGER"
    });

    ShieldedCard.enableCaching();

    // Extend with storage mixins, to create default CRUD operations
    // Define the basic routes
    _.extend(ShieldedCard.prototype, {
    });

    _.extend(ShieldedCard, abineStorageMixins.DefaultActions, {
      routes: {
        'read/shieldedCard/all':   'index',
        'read/shieldedCard/:id':   'show',
        'create/shieldedCard':     'createFromServer',
        'update/shieldedCard/:id': 'update',
        'delete/shieldedCard/:id':  'destroy'
      },

      // ### function createFromServer(request)
      createFromServer: function(request, callback) {

        storage.Preference.findBy('key','authToken', function(authToken){
          if(!authToken || authToken.isEmpty()){
            // return an error, no auth token :(
          }

          this.licenseServer = this.licenseServer || new licenseServer();
          this.licenseServer.billingInfo({}, function(err, data){
            if(err){
              LOG_ERROR && ABINE_DNTME.log.error("[ERROR HANDLER]#[abine/storage/shieldedCard]#[createFromServer]# error event in shieldedCard.createFromServer call to license server");
              // returnError
            } else {
                request.payload.first_name = data.first_name;
                request.payload.last_name = data.last_name;
                request.payload.month = data.month;
                request.payload.year = data.year;
                request.payload.card_type = data.card_type;
                request.payload.zip = data.zip;
                request.payload.last_four = data.last_four;
                request.payload.address1 = data.address1;
                request.payload.address2 = data.address2;
                request.payload.city = data.city;
                request.payload.state_abbreviation = data.state;

                ShieldedCard.create(request, callback);
            }
          });
        });
      }

    });

    storage.ShieldedCard = ShieldedCard;
    return ShieldedCard;

  });


ABINE_DNTME.define("abine/storage/shieldedEmail",
    ['documentcloud/underscore', 'persistence', 'abine/storageMixins', 'abine/config',
      'abine/emailServer', 'storage', 'persistence/caching'],
    function(_, persistence, abineStorageMixins, config, emailServer, storage) {

  var ShieldedEmail = persistence.define('ShieldedEmails', {
    guid: "TEXT",
    userId: "INT",
    email: "TEXT",
    active: "BOOL",
    createdAt: "DATE",
    modifiedAt: "DATE",
    pushVersion: "INTEGER"
  });

  ShieldedEmail.enableCaching('email');

  // Extend with storage mixins, to create default CRUD operations
  // Define the basic routes
  _.extend(ShieldedEmail, abineStorageMixins.DefaultActions, {
    routes: {
      'read/shieldedEmail/all':   'indexWithAltEmail',
      'read/shieldedEmail/:id':   'show',
      'delete/shieldedEmail/:id':  'destroy'
    },

    indexWithAltEmail: function(request, callback){
      storage.Preference.batchFindBy([
        {by:"key",value:"altEmail1"},
        {by:"key",value:"altEmail2"}
      ], function(prefMap) {
        storage.Identity.index(null, function(identities){
          ShieldedEmail.index(request, function (emails) {
            var email = emails && emails.length > 0 ? emails[0] : null;
            if (email) {
              email = email.toJSON();
              email.altEmails = [];
              var doneEmails = {};
              doneEmails[email.email]=true;
              if (prefMap.altEmail1 && prefMap.altEmail1.value) {
                email.altEmails.push({email:prefMap.altEmail1.value});
                doneEmails[prefMap.altEmail1.value.toLowerCase()] = 1;
              }
              if (prefMap.altEmail2 && prefMap.altEmail2.value) {
                email.altEmails.push({email:prefMap.altEmail2.value});
                doneEmails[prefMap.altEmail2.value.toLowerCase()] = 1;
              }
              _.each(identities, function(identity){
                if (identity.email && identity.email.indexOf('@') != -1) {
                  if (!doneEmails[identity.email.toLowerCase()]) {
                    email.altEmails.push({email: identity.email});
                    doneEmails[identity.email.toLowerCase()]=1;
                  }
                }
              });
              ShieldedEmail.respondWithJSON([email], request, callback);
            } else {
              ShieldedEmail.respondWithEntities(email, request, callback);
            }
          });
        });
      });
    }

  });

  storage.ShieldedEmail = ShieldedEmail;
  return ShieldedEmail;

});


ABINE_DNTME.define("abine/storage/shieldedPhone",
  ['documentcloud/underscore', 'persistence', 'abine/storageMixins',
    'storage', 'persistence/caching', 'abine/phoneServer', 'abine/config'],
  function(_, persistence, abineStorageMixins, storage, autoDate, caching, phoneServer, config) {

    var ShieldedPhone = persistence.define('ShieldedPhones', {
      country_code: "TEXT",
      number: "TEXT",
      is_validated: "TEXT",
      rails_id: "INTEGER",
      auth_token: "TEXT",
      createdAt: "DATE",
      modifiedAt: "DATE",
      pushVersion: "INTEGER"
    });

    ShieldedPhone.enableCaching();

    // Extend with storage mixins, to create default CRUD operations
    // Define the basic routes
    _.extend(ShieldedPhone.prototype, {
      refresh: function(callback){
        // no longer need to do this?
        callback();
      },
      getPrettyNumber: function() {
        var pn = "(" + this.getAreaCode() + ") "
          + this.getExchange() + "-"
          + this.getLocal();
        return pn;
      },

      getAreaCode: function(){
        return this.number.substr(0,3);
      },

      getExchange: function() {
        return this.number.substr(3,3);
      },

      getLocal: function() {
        return this.number.substr(6,4);
      },

      isValidated: function(){
        return this.is_validated == "true";
      }
    });

    _.extend(ShieldedPhone, abineStorageMixins.DefaultActions, {
      routes: {
        'read/shieldedPhone/all':   'indexWithRefresh',
        'read/shieldedPhone/:id':   'showWithRefresh'
      },

      showWithRefresh: function(request,callback){
        ShieldedPhone.load(request.params[0],function(shieldedPhone){
          shieldedPhone.refresh(function(){
            ShieldedPhone.show(request,callback);
          });
        });
      },

      indexWithRefresh: function(request, callback){
        ShieldedPhone.index(null, function(shieldedPhones) {
          _.async.forEach(shieldedPhones, function(shieldedPhone, cb){
            shieldedPhone.refresh(cb);
          }, function(err){
            if(err) {LOG_ERROR && ABINE_DNTME.log.error("[ERROR CALLBACK]#[abine/storage/shieldedPhone]#[indexWithRefresh]# error event in ShieldedPhone.index async forEach");}
            storage.Identity.index(null, function (identities) {
              ShieldedPhone.index(request, function (shieldedPhones) {
                var donePhones = {};
                var phones = [];
                if (shieldedPhones.length > 0) {
                  var phone = shieldedPhones[0].toJSON();
                  donePhones[phone.number.toString()] = 1;
                  phones.push(phone)
                } else {
                  phones.push({});
                }
                _.each(identities, function (identity) {
                  if (identity.phone && identity.phone.indexOf('+') != -1) {
                    var parts = identity.phone.replace('+', '').split(' ');
                    var country = parts[0];
                    var number = parts[1];
                    if (!donePhones[number]) {
                      phones.push({country_code: country, number: number, identity: true});
                      donePhones[number] = 1;
                    }
                  }
                });
                ShieldedPhone.respondWithJSON(phones, request, callback);
              });
            });
          });
        });
      }
    });

    storage.ShieldedPhone = ShieldedPhone;
    return ShieldedPhone;

  });


ABINE_DNTME.define('abine/storage/tabData',
    ['documentcloud/underscore', 'persistence', 'abine/storageMixins', 'storage', 'abine/ajax'],
    function(_, persistence, abineStorageMixins, storage, abineAjax) {

  var ONE_DAY = 24*60*60*1000;

  function toDayOfYear(dt) {
    dt = dt || new Date();
    var d = new Date(dt.getFullYear(), 0, 0);
    return Math.floor((dt-d)/8.64e+7);
  }

  function sendTrackerAllow(domain, url, tracker)  {
    try {
      ABINE_DNTME.require(['modules'], function(modules){
        modules.mappingsModule.sendTrackerAllow(domain, url, tracker, ABINE_DNTME.dnt_api.getRulesVersion());
      });
    } catch(e){}
  }

  var PageTracker = function(){};

  _.extend(PageTracker, abineStorageMixins.DefaultActions, {
    routes: {
      'create/pageTracker':   'toggleTracker'
    },

    toggleTracker: function(request, callback) {
      var tabId = request.sender;

      var tabData = ABINE_DNTME.dnt_api.getTabData(tabId);
      if (request.payload.blocked) {
        ABINE_DNTME.dnt_api.block(request.payload.name, tabData.host);
      } else {
        ABINE_DNTME.dnt_api.allow(request.payload.name, tabData.host);
        sendTrackerAllow(tabData.host, tabData.url, request.payload.name);
      }
    }

  });

  var CustomTabData = function(){};

  _.extend(CustomTabData, abineStorageMixins.DefaultActions, {
    routes: {
      'create/customTabData':   'setCustomData'
    },

    setCustomData: function(request, callback) {
      ABINE_DNTME.dnt_api.setCustomTabData(request.sender, request.payload.name, request.payload.value);
    }

  });

  var BlacklistOrPreference = function(){};

  _.extend(BlacklistOrPreference, abineStorageMixins.DefaultActions, {
    routes: {
      'update/blacklistOrPreference/:id':   'save'
    },

    save: function(request, callback) {
      var type = request.payload.type;
      if (type == 'blacklist') {
        var newRequest = {
          payload: {
            domain: request.payload.domain,
            changed: request.payload.name,
            suppressWebapp: request.payload.suppressWebapp
          },
          params: [request.payload.id],
          sender: request.sender
        };
        newRequest.payload[request.payload.name] = request.payload.value;

        if (request.payload.name == 'tracker' && request.payload.value) {
          var tabData = ABINE_DNTME.dnt_api.getTabData(request.sender);
          sendTrackerAllow(request.payload.domain, tabData.url, '*');
        }

        storage.Blacklist.updateOrCreate(newRequest, callback);
      } else {
        storage.GlobalPreference.togglePreference(request, callback);
      }
    }

  });

  var TabData = function(){};

  _.extend(TabData, abineStorageMixins.DefaultActions, {
    routes: {
      'read/tabData/:id':   'getTabData',
      'create/tabData':     'reportIssue'
    },

    reportIssue: function(request, callback) {
      var tabData = ABINE_DNTME.dnt_api.getTabData(request.sender);
      storage.Preference.batchFindBy([
        {by:"key",value:"installVersion"}
      ], function(prefMap){
        var trackers = '';
        if (tabData) {
          var trackers = [];
          for (var i=0;i<tabData.trackers.length;i++) {
            trackers.push(tabData.trackers[i].name);
          }
          trackers = trackers.join(',');
        }
        var data = {};
        data['reason'] = "dntme-feedback";
        data['version'] = ABINE_DNTME.config.version
        data['origin'] = ABINE_DNTME.config.tags[0];
        data['revision'] = ABINE_DNTME.config.buildNum;
        data['oversion'] = prefMap.installVersion?prefMap.installVersion.value:'';
        data['url'] = tabData?encodeURIComponent(tabData.url):'';
        data['description']= encodeURIComponent(request.payload.feedback);
        data['rulesversion']= encodeURIComponent(ABINE_DNTME.dnt_api.getRulesVersion());
        data['trackers'] = trackers;
        data['domain'] = encodeURIComponent(tabData?tabData.host:'');
        data['log'] = '';

        abineAjax.ajax({url: 'http://www.abine.com/checkin/dntmefeedback.php', data: "data="+JSON.stringify(data)});
      });

    },

    getWeeklyReportData: function (today, installDate, weeklyReportClickedOn, dntStats) {
      // installed at least 1 week data as on previous sunday and its before thursday
      var numDaySinceInstall = (today.getTime() - installDate.getTime()) / ONE_DAY;
      var requiredInstallAge = 7;
      var installDay = installDate.getDay();
      if (installDay > 1) {
        requiredInstallAge += (7 - installDate.getDay());
      }
      var weekDay = today.getDay();
      if (numDaySinceInstall >= requiredInstallAge && weekDay > 0 && weekDay <= 3) {
        var clickedOn = null;
        if (weeklyReportClickedOn) {
          clickedOn = parseInt(weeklyReportClickedOn);
        }
        var sunday = new Date(today.getTime() - (weekDay * ONE_DAY));
        sunday.setHours(0, 0, 0, 0);
        var hasData = false;
        var totals = dntStats['dailyTrackerCounts']?JSON.parse(dntStats['dailyTrackerCounts']):{};
        for (var i=0;i<7;i++) {
          var curDate = new Date(sunday.getFullYear(), sunday.getMonth(), sunday.getDate()-i);
          var day = toDayOfYear(curDate);
          var year = curDate.getFullYear();
          if (year in totals && totals[year][day] > 0) {
            hasData = true;
            break;
          }
        }
        if (hasData && (!clickedOn || clickedOn < sunday.getTime())) {
          return ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'][sunday.getMonth()] + ' ' + sunday.getDate();
        }
      }
      return null;
    },

    getTabData: function(request, callback) {
      var tabData = ABINE_DNTME.dnt_api.getTabData(request.sender);

      if (!tabData) {
        ABINE_DNTME.require(['abine/dnt/tabData'], function(TabData){
          TabData.get(request.sender);
          tabData = ABINE_DNTME.dnt_api.getTabData(request.sender);
        }, null, true);
      }

      if (request.params && request.params.length > 0 && request.params[0] == 'trackers') {
        TabData.respondWithJSON(tabData, request, callback);
        return;
      }

      var self = this;

      storage.Preference.batchFindBy([
        {by: 'key', value: 'authToken'},
        {by: 'key', value: 'installDate'},
        {by: 'key', value: 'blockTrackers'},
        {by: 'key', value: 'helpMeLogin'},
        {by: 'key', value: 'submitAccount'},
        {by: 'key', value: 'suggestProtectedEmail'},
        {by: 'key', value: 'suggestProtectedPhone'},
        {by: 'key', value: 'suggestProtectedCard'},
        {by: 'key', value: 'suggestRealCard'},
        {by: 'key', value: 'suggestAddresses'},
        {by: 'key', value: 'suggestIdentities'},
        {by: 'key', value: 'hasEnabledPayments'},
        {by: 'key', value: 'entitledToPayments'},
        {by: 'key', value: 'entitledToPhone'},
        {by: 'key', value: 'entitledToAccounts'},
        {by: 'key', value: 'entitledToSync'},
        {by: 'key', value: 'rememberAccounts'},
        {by: 'key', value: 'rememberCards'},
        {by: 'key', value: 'sync_store_manager'},
        {by: 'key', value: 'understanding_tracker_clicked'},
        {by: 'key', value: 'has_mobile'},
        {by: 'key', value: 'showMedals'},
        {by: 'key', value: 'oldEmail'},
        {by: 'key', value: 'loginNotLock'},
        {by: 'key', value: 'suggestStrongPassword'},
        {by: 'key', value: 'showTrackersOnload'},
        {by: 'key', value: 'showWeeklyReports'},
        {by: 'key', value: 'weeklyReportClickedOn'},
        {by: 'key', value: 'autoPremiumRegisterShown'},
        {by: 'key', value: 'showCheckoutPanelGrowl'}
      ], function(prefs){
        storage.ShieldedPhone.index(null, function(phones){
          storage.DisposableEmail.index(null, function(emails){
            storage.Blacklist.findBy('domain', tabData.host, function(blacklist){
              ABINE_DNTME.dnt_api.forceSaveStats();
              storage.DntStats.index(null, function(dntStats) {
                tabData.dntStats = dntStats[0].toJSON();
                tabData.dntStats.showMedals = (!prefs.showMedals || prefs.showMedals.value == 'true');

                tabData.loggedIn = prefs.authToken && prefs.authToken.value && prefs.authToken.value != "";
                if (!tabData.loggedIn && prefs.oldEmail && prefs.oldEmail.value && prefs.oldEmail.value != '') {
                  tabData.email = prefs.oldEmail.value;
                }
                tabData.loginNotLock = prefs.loginNotLock && prefs.loginNotLock.isTrue();
                tabData.loginNotRegister = tabData.loginNotLock || tabData.email;
                tabData.unlockScreen = tabData.email;

                tabData.showTrackersOnload = prefs.showTrackersOnload && prefs.showTrackersOnload.isTrue();

                tabData.hasMobileApp = tabData.loggedIn && (prefs.has_mobile && prefs.has_mobile.isTrue());
                tabData.dropboxEnabled = tabData.loggedIn && (prefs.sync_store_manager ? (prefs.sync_store_manager.value == 'dropbox' || prefs.sync_store_manager.value == "blur"): false);
                tabData.entitledToSync = prefs.entitledToSync && prefs.entitledToSync.isTrue();

                // Get list of all blacklisted site so it can be accessed from tab data
                storage.Blacklist.index(null, function(blacklistCollection){
                  // Alphabetize The List of Blacklisted Sites
                  blacklistCollection.sort(function(a, b){
                      if(a.domain < b.domain) return -1;
                      if(a.domain > b.domain) return 1;
                      return 0;
                  })
                  blacklistCollection = _.map(blacklistCollection, function(item){
                      return item._data;
                  });
                  // Add IDs to blacklist collection
                  for (var index in blacklistCollection) {
                    var domain = blacklistCollection[index].domain;
                    storage.Blacklist.findBy('domain', domain, function(blacklist){
                      blacklistCollection[index].id = blacklist.id;
                    });
                  }
                  tabData.blacklistCollection = blacklistCollection;
                });

                if (!blacklist) blacklist = new storage.Blacklist({domain: tabData.host, tracker: tabData.disabled});
                tabData.blacklist = blacklist.toJSON();

                tabData.blacklist.loggedIn = tabData.loggedIn;
                tabData.blacklist.globalTracker = ABINE_DNTME.dnt_api.getDefaultBlocking();
                tabData.blacklist.globalPhone = prefs.suggestProtectedPhone && prefs.suggestProtectedPhone.isTrue();
                tabData.blacklist.globalCard = prefs.suggestProtectedCard && prefs.suggestProtectedCard.isTrue();
                tabData.blacklist.globalEmail = (prefs.suggestProtectedEmail && prefs.suggestProtectedEmail.isTrue());
                tabData.blacklist.globalPassword = (prefs.suggestStrongPassword && prefs.suggestStrongPassword.isTrue());
                tabData.blacklist.globalLogin = (prefs.helpMeLogin && prefs.helpMeLogin.isTrue());
                tabData.blacklist.globalRealCard = (prefs.suggestRealCard && prefs.suggestRealCard.isTrue());
                tabData.blacklist.globalAddress = (prefs.suggestAddresses && prefs.suggestAddresses.isTrue());
                tabData.blacklist.globalAccounts = (prefs.rememberAccounts && prefs.rememberAccounts.isTrue());
                tabData.blacklist.globalIdentity = (prefs.suggestIdentities && prefs.suggestIdentities.isTrue());
                tabData.blacklist.globalSaveCard = (prefs.rememberCards && prefs.rememberCards.isTrue());
                tabData.blacklist.globalSubmitAccount = (prefs.submitAccount && prefs.submitAccount.isTrue());

                tabData.blacklist.trackerOff = tabData.blacklist.tracker || !tabData.blacklist.globalTracker;
                tabData.blacklist.phoneOff = tabData.blacklist.phone || !tabData.blacklist.globalPhone;
                tabData.blacklist.cardOff = tabData.blacklist.card || !tabData.blacklist.globalCard;
                tabData.blacklist.emailOff = tabData.blacklist.email || !tabData.blacklist.globalEmail;
                tabData.blacklist.addressOff = tabData.blacklist.address || !tabData.blacklist.globalAddress;
                tabData.blacklist.accountOff = tabData.blacklist.save_account || !tabData.blacklist.globalAccounts;
                tabData.blacklist.realCardOff = tabData.blacklist.real_card || !tabData.blacklist.globalRealCard;
                tabData.blacklist.loginOff = tabData.blacklist.account || !tabData.blacklist.globalLogin;
                tabData.blacklist.passwordOff = tabData.blacklist.password || !tabData.blacklist.globalPassword;
                tabData.blacklist.identityOff = tabData.blacklist.identity || !tabData.blacklist.globalIdentity;
                tabData.blacklist.saveCardOff = tabData.blacklist.save_card || !tabData.blacklist.globalSaveCard;
                tabData.blacklist.submitAccountOff = tabData.blacklist.submit_account || !tabData.blacklist.globalSubmitAccount;

                tabData.blacklist.hasEnabledPhone = prefs.entitledToPhone && prefs.entitledToPhone.isTrue() && phones.length > 0 && phones[0].isValidated();
                tabData.blacklist.hasEnabledCard = (prefs.autoPremiumRegisterShown && prefs.autoPremiumRegisterShown.isTrue()) || (prefs.hasEnabledPayments && prefs.hasEnabledPayments.isTrue());
                tabData.blacklist.hasEnabledAccounts = (prefs.entitledToAccounts && prefs.entitledToAccounts.isTrue())
                tabData.blacklist.has_masked_emails = emails.length > 1;
                tabData.blacklist.has_masked_phones = phones.length > 0;

                tabData.blacklist.premium = tabData.premium;

                tabData.understanding_tracker_clicked = (prefs.understanding_tracker_clicked && prefs.understanding_tracker_clicked.isTrue());

                tabData.premium = (prefs.authToken && prefs.authToken.value != "") && (prefs.entitledToPhone && prefs.entitledToPhone.isTrue());
                tabData.cardMoney = (prefs.authToken && prefs.authToken.value != "") && (prefs.entitledToPayments && prefs.entitledToPayments.isTrue()) && !tabData.premium;
                tabData.showFreeCard = !(tabData.premium || tabData.cardMoney);

                tabData.dntStats.premium = tabData.premium;
                tabData.dntStats.maskmeInstalled = tabData.blacklist.maskmeInstalled = tabData.maskmeInstalled = !!ABINE_DNTME.maskme_installed;
//                  tabData.dntStats.noFreeCardOffer = tabData.dntStats.maskmeInstalled || tabData.dntStats.premium;

                var today = new Date();
                today.setHours(0,0,0,0);

                var installDate = new Date(prefs.installDate ? prefs.installDate.value : today.getTime());
                installDate.setHours(0,0,0,0);

                tabData.installMonth = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'][installDate.getMonth()]+' \''+installDate.getFullYear().toString().substr(2);

                tabData.weeklyReport = null;
                if (!ABINE_DNTME.maskme_installed && (!prefs.showWeeklyReports || prefs.showWeeklyReports.isTrue())) {
                  var clickedOn = null;
                  if (prefs.weeklyReportClickedOn) clickedOn = prefs.weeklyReportClickedOn.value;
                  tabData.weeklyReport = self.getWeeklyReportData(today, installDate, clickedOn, tabData.dntStats);
                }

                TabData.respondWithJSON(tabData, request, callback);
              });
            });
          });
        });
      });
    }

  });

  storage.TabData = TabData;
  storage.PageTracker = PageTracker;
  storage.CustomTabData = CustomTabData;
  storage.BlacklistOrPreference = BlacklistOrPreference;

  return TabData;

});

ABINE_DNTME.define('abine/storage/trackerData',
  ['documentcloud/underscore', 'persistence', 'abine/storageMixins',
    'storage', 'abine/localStorage'],
  function(_, persistence, abineStorageMixins, storage, localStorage) {

    var TrackerData = persistence.define('TrackerData', {
      domain: "TEXT"
    });

    _.extend(TrackerData, abineStorageMixins.DefaultActions, {
      routes: {},

      createLocal: function(request, callback) {
        storage.Preference.findBy('key', 'shouldSendErrorReports', _.bind(function(sendData) {
          if (!sendData || sendData.isTrue()) {
            // for pushing to server later
            var toPush = localStorage.getItem('trackerData-to-push');
            if (toPush) {
              try {
                toPush = JSON.parse(toPush);
              } catch(e) {
                toPush = {};
              }
            } else {
              toPush = {};
            }

            var key = request.payload.d+":"+request.payload.h;
            toPush[key] = request.payload.t;

            localStorage.setItem('trackerData-to-push', JSON.stringify(toPush));
          } else {
            localStorage.removeItem('trackerData-to-push');
          }
        }, this));

        if (callback) {
          callback({});
        }
      }
    });

    storage.TrackerData = TrackerData;
    return TrackerData;

  });

ABINE_DNTME.define('abine/storage/trackerInfo',
  ['documentcloud/underscore', 'persistence', 'abine/storageMixins',
    'storage', 'abine/ajax'],
  function(_, persistence, abineStorageMixins, storage, abineAjax) {

    var trackerCache = {};
    var TRACKER_API_URL = '/api/v1/lookup'

    var TrackerInfo = persistence.define('TrackerInfo', {
      domain: "TEXT",
      trackers: "TEXT",
      color: "TEXT",
      info: "TEXT"
    });

    _.extend(TrackerInfo, abineStorageMixins.DefaultActions, {
      routes: {
        'read/trackerInfo/index': 'getTrackersForDomains'
      },

      getTrackersForDomains: function(request, callback) {
        var domains = request.payload.domains;

        var trackerInfo = [], missingDomains=[];
        for (var domain in domains) {
          if (trackerCache[domain]) {
            trackerInfo.push(trackerCache[domain]);
          } else {
            missingDomains.push(domain);
          }
        }

        function respond() {
          TrackerInfo.respondWithJSON(trackerInfo, request, callback);
        }

        if (missingDomains.length > 0) {
          var url = 'https://' + ABINE_DNTME.config.trackerHost + TRACKER_API_URL;
          abineAjax.ajax({
            url: url,
            type: 'POST',
            data: {urls: missingDomains},
            success: function(data, textStatus, jqXHR) {
              for (var domain in data) {
                if (!data[domain].trackers) continue;
                if (data[domain].trackers == 'No data') {
                  trackerCache[domain] = {domain: domain};
                } else {
                  var numTrackers = data[domain].trackers.length;
                  var color;
                  if (numTrackers <= 1) {
                    color = 'green';
                  } else if (numTrackers > 1 && numTrackers <= 5) {
                    color = 'yellow';
                  } else {
                    color = 'red';
                  }

                  trackerCache[domain] = {domain: domain, trackers: data[domain].trackers.join(':'), color: color, info: data[domain].info};
                }

                trackerInfo.push(trackerCache[domain]);
              }
              respond();
            },
            error: function(){
              respond();
            }
          });

        } else {
          respond();
        }
      }

    });

    storage.TrackerInfo = TrackerInfo;
    return TrackerInfo;

  });




ABINE_DNTME.define('storage', ['persistence', 'documentcloud/underscore'], function(persistence, _) {

  var Storage = {

    dumpToJSON: function(){
      var entities = [];

      for(var i in Storage){
        if('meta' in Storage[i]){
          entities.push(Storage[i]);
        }
      }

      persistence.dump(entities, function(obj){
        var jsonString = JSON.stringify(obj);
        // post it?
      });
    },

    purgeDatabase: function(callback){
      persistence.clean(); // clear cache

      var entities = [];

      for(var i in Storage){
        if('meta' in Storage[i]){
          entities.push(Storage[i]);
        }
      }

      _.async.forEach(entities, function(entity, cb) {
        persistence.transaction(function(t) {
          t.executeSql('delete from ' + entity.meta.name, null, function(){cb();});
        });
      }, function(err) {
        callback();
      });
    },

    clearAllCache: function(sync_store_manager, callback) {
      ABINE_DNTME.require(["abine/blurstore-api"], function (blurstore) {
        Storage.ShieldedPhone.clearCache(function () {
          Storage.DisposablePhone.clearCache(function () {
            Storage.Account.clearCache(function () {
              Storage.Address.clearCache(function () {
                Storage.Note.clearCache(function () {
                  Storage.RealCard.clearCache(function () {
                    Storage.AccountHistory.clearCache(function () {
                      // disconnect if there was a change in store before fetching again
                      if (sync_store_manager != 'blur') {
                        blurstore.disconnect();
                      }
                      Storage.Account.index(null, function (accounts) {
                        Storage.Address.index(null, function (addresss) {
                          Storage.Note.index(null, function (notes) {
                            Storage.RealCard.index(null, function (cards) {
                              Storage.AccountHistory.index(null, function (accountHistory) {
                                if (sync_store_manager == 'blur') {
                                  blurstore.connectAndLoad();
                                } else {
                                  // mark accounts as not pushed to avoid deletion of accounts on subsequent connect to syncstore
                                  _.each(accounts, function (account) {
                                    account.pushed = 0;
                                  });
                                  // mark account history as not pushed to avoid deletion of accounts on subsequent connect to syncstore
                                  _.each(accountHistory, function (accHistory) {
                                    accHistory.pushed = 0;
                                  });
                                  // mark addresses as not pushed to avoid deletion of accounts on subsequent connect to syncstore
                                  _.each(addresss, function (address) {
                                    address.pushed = 0;
                                  });
                                  // mark notes as not pushed to avoid deletion of accounts on subsequent connect to syncstore
                                  _.each(notes, function (note) {
                                    note.pushed = 0;
                                  });
                                  // mark cards as not pushed to avoid deletion of accounts on subsequent connect to syncstore
                                  _.each(cards, function (card) {
                                    card.pushed = 0;
                                  });

                                  persistence.flush();
                                }
                                Storage.CompanyAccount.clearCache(function() {
                                  callback();
                                });
                              });
                            });
                          });
                        });
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
    }
  };

  return Storage;
});

ABINE_DNTME.define("abine/modules/license",
  ["documentcloud/underscore", "abine/core", 'abine/timer', 'storage', 'persistence',
    'abine/ajax', 'abine/config', 'abine/securityManager', 'abine/licenseServer','abine/responseCodes',
    'modules','abine/crypto', 'abine/ajax', 'abine/webapp', "abine/memoryStore"],
  function (_, abineCore, abineTimer, storage, persistence,
            abineAjax, config, securityManager, licenseServer,
            responseCodes, modules, crypto, abineAjax, webapp, memoryStore) {

    var emptyCallback = function() {};

    var ONE_DAY_MS = 24 * 60 * 60 * 1000;

    var todayDate = function() {
      var today = new Date();
      today.setHours(0); today.setMinutes(0); today.setSeconds(0); today.setMilliseconds(0); // js dates suck
      return today;
    };

    var LicenseModule = abineCore.BaseClass.extend({

      initialize: function(options) {
        this.options = options;
        this.licenseServer = this.options.licenseServer || new licenseServer();
      },

      setMessenger: function(messenger){
        this.messenger = messenger;
      },

      // Touch the daily active users counter once per day ...
      startDailyTouch: function(callback) {
        callback = callback || emptyCallback;

        storage.Preference.findBy('key', 'lastTouch', _.bind(function(lastTouchPref){
          if (lastTouchPref && lastTouchPref.value) {
            var lastTouchDate = new Date(parseInt(lastTouchPref.value));
            if (lastTouchDate.valueOf() < todayDate().valueOf()) {
              this.makeDailyActiveTouch(false, callback);
            } else {
              LOG_INFO && ABINE_DNTME.log.info("already touched today.");
              this.delayDailyActiveTouch(callback);
            }
          } else {
            LOG_INFO && ABINE_DNTME.log.info("touch on install (first time).");

            this.makeDailyActiveTouch(true, function(){
                callback();
            });
          }
        },this));
      },

      makeDailyActiveTouch: function(firstTime, callback) {
        callback = callback || emptyCallback;

        if (ABINE_DNTME.maskme_installed) {
          callback();
          return;
        }

        LOG_INFO && ABINE_DNTME.log.info("making request to daily touch API");

        var params = {};
        params.install = !!(firstTime);

        storage.Preference.batchFindBy([
          {by: 'key', value: 'authToken'},
          {by: 'key', value: 'installAuthToken'}
        ], _.bind(function(prefMap){
          var authTokenPref = prefMap['authToken'];
          var installAuthTokenPref = prefMap['installAuthToken'];

          if (authTokenPref && !authTokenPref.isEmpty()) {
            params.auth_token = authTokenPref.value;
          }

          if (installAuthTokenPref && !installAuthTokenPref.isEmpty()) {
            params.install_auth_token = installAuthTokenPref.value;
          }

          this.licenseServer.makeDailyTouch(params, _.bind(function(err, serverData){
            if(err){
              this.delayDailyActiveTouch(function(){
                callback(err);
              });
            } else {
              LOG_INFO && ABINE_DNTME.log.info("touched daily active user api for today");
              this.writeDailyTouchPref(_.bind(function(){
                this.delayDailyActiveTouch(function(){
                  if(serverData.install_auth_token){
                    if(serverData.splits && serverData.splits.length > 0){
                      var splitTestJSON = _.reduce(serverData.splits,function(memo,obj){
                        memo[obj.test_name]=obj.bucket_id;
                        return memo;
                      },{});
                      try{
                        storage.Preference.createOrModify({
                          key: 'splitTests',
                          value: JSON.stringify(splitTestJSON)
                        });
                      } catch(e){}
                    }

                    if(serverData.config_overrides && serverData.config_overrides.length > 0){
                      var configOverridesJSON = _.reduce(serverData.config_overrides,function(memo, obj){
                        memo[obj.config_name]=obj.config_value;
                        return memo;
                      }, {});
                      try {
                        storage.Preference.createOrModify({
                          key: 'configOverrides',
                          value: JSON.stringify(configOverridesJSON)
                        });
                      } catch(e){}
                    }

                    if(serverData.other_data){
                      try {
                        storage.Preference.createOrModify({
                          key: 'isBlurUpgrade',
                          value: serverData.other_data.is_blur_upgrade
                        });
                      } catch(e){}
                    }

                    storage.Preference.createOrModify({key: 'installAuthToken', value: serverData.install_auth_token}, callback);
                  } else {
                    callback();
                  }
                });
              },this));
            }
          },this));
        },this));
      },

      removeDailyTouchPref: function(callback) {
        storage.Preference.findBy('key', 'lastTouch', function(lastTouchPref) {
          if (lastTouchPref) {
            persistence.remove(lastTouchPref);
            persistence.flush(callback);
          } else {
            callback();
          }
        });
      },

      // start up a timer so that it happens tomorrow if we're still running
      delayDailyActiveTouch: function(callback) {
        callback = callback || emptyCallback;

        // wait between 30mins and 90mins
        var wait = (30 * 60 * 1000) + (60 * 60 * 1000 * Math.random());
        if (this.dailyActiveTimeout) {
          abineTimer.clearTimeout(this.dailyActiveTimeout);
        }
        LOG_INFO && ABINE_DNTME.log.info("delaying daily touch by "+(wait/60000)+" minutes");
        this.dailyActiveTimeout = abineTimer.setTimeout(_.bind(this.startDailyTouch, this), wait);

        callback();
      },

      writeDailyTouchPref: function(callback) {
        callback = callback || emptyCallback;

        storage.Preference.findOrCreate({key:'lastTouch'}, function(lastTouchPref){
          lastTouchPref.value = Math.floor(todayDate().valueOf()); // why is this a real in Chrome db?
          lastTouchPref.modifiedAt = todayDate();
          persistence.flush(function(){
            callback();
          });
        });
      },

      sendFeedbackEmail: function(data, callback){
        callback = callback || emptyCallback;

        data.browserUserAgent = "Extension " + ABINE_DNTME.config.browser + " " + ABINE_DNTME.config.version;

        this.licenseServer.sendFeedbackEmail(data, _.bind(function(err, serverData){
          if(err){
            callback(err);
          } else {
            callback(null, serverData);
          }
        }, this));
      },

      logAction: function(data, callback){
        callback = callback || emptyCallback;

        storage.Preference.findBy('key', 'sendToolbarClick', _.bind(function(sendToolbarClickPref) {
          if(!sendToolbarClickPref){
            storage.Preference.createOrModify({key: 'sendToolbarClick', value: 'true'}, _.bind(function(){
              this.licenseServer.logAction(data, _.bind(function(err, serverData){
                if(err){
                  callback(err);
                } else {
                  callback(null, serverData);
                }
              }, this));
            }, this));
          }
        }, this));
      },

      logSplitData: function(data, callback){
        callback = callback || emptyCallback;

        this.licenseServer.logSplitData(data, _.bind(function(err, serverData){
          if(err){
            callback(err);
          } else {
            callback(null, serverData);
          }
        }, this));
      },

      logPasswordFillData: function(data, callback){
        callback = callback || emptyCallback;

        this.licenseServer.logPasswordFillData(data, _.bind(function(err, serverData){
          if(err){
            callback(err);
          } else {
            callback(null, serverData);
          }
        }, this));
      },


      logout: function(token, callback) {
        callback = callback || emptyCallback;

        this.licenseServer.logout({auth_token: token}, _.bind(function(err, serverData){
          callback(err, serverData);
        },this));
      },

      login: function(data, callback) {
        callback = callback || emptyCallback;

        var inputPassword = data.password;
        data.password = crypto.getServerPassword(inputPassword);

        var key = crypto.generateKey();
        data.key = crypto.encryptKey(inputPassword, key);
        data.verify_key = crypto.encryptKey(key, key);

        this.licenseServer.login({application: 'manageme', user_login: data, mfa_code: data.mfa_code}, _.bind(function(err, serverData){
          if(err){
            data.password = inputPassword;
            callback(err, serverData);
          } else {
            LOG_INFO && ABINE_DNTME.log.info("Signed in and authenticated with server.");
            this._afterLoginOrRegister(serverData, inputPassword, function(){
              callback(null, serverData);
            });
          }
        },this));
      },

      _afterLoginOrRegister: function (serverData, rawPassword, callback) {
        var key = crypto.decryptKey(rawPassword, serverData.user.key, null, true);
        if (key == serverData.user.key) {
          key = null;
        }
        var companyKey = null;
        if (serverData.user.companyKey) {
          companyKey = crypto.decryptKey(rawPassword, serverData.user.companyKey, null, true);
          if (companyKey == serverData.user.companyKey) {
            companyKey = null;
          }
        }
        storage.Preference.batchCreateOrModify([
          {key: "authToken", value: serverData["auth_token"]},
          {key: "key", value: key},
          {key: "ckey", value: companyKey},
          {key: "installAuthToken", value: serverData["install_auth_token"]},
          {key: "hasRegistered", value: "true"},
          {key: "passwordHint", value: serverData["password_hint"]}
        ], _.bind(function () {
          securityManager.setAuthToken(serverData['auth_token'], key, companyKey, _.bind(function(){
            persistence.flush(_.bind(function () {
              // delay mapping update (and panel metrics push) by 20 secs to send any masking activity as part of auto-reg.
              abineTimer.setTimeout(function () {
                modules.mappingsModule.refreshPanels();
              }, 20000);

              this.messenger.broadcastToAllTabs('broadcast:preference:reload', {});
              this.messenger.broadcastToAllTabs('broadcast:register:success', {});

              // reload any open webapp tabs.
              webapp.reload();

              callback();
            }, this));
          }, this));
        }, this));
      },

      register: function(data, callback) {
        callback = callback || emptyCallback;
        var inputPassword = data.password;
        var registerType = data.register_type;
        var send_password_email = ("send_password_email" in data)?data.send_password_email:true;

        if (!data.generated_password) {
          var key = crypto.generateKey();
          data.key = crypto.encryptKey(inputPassword, key);
          data.verify_key = crypto.encryptKey(key, key);
        }

        delete data.generated_password;

        data.password = crypto.getServerPassword(inputPassword);
        data.password_confirmation = data.password;
        data.tag = config.tags[0];

        delete data.register_type;
        delete data.send_password_email;
        var userData = data;

        storage.Preference.createOrModify({key: 'registerInProgress', value: 'true'}, _.bind(function(progress){
          this.licenseServer.register({
            user: userData,
            application: "manageme",
            send_password_email: send_password_email,
            register_type: registerType
          }, _.bind(function(err, serverData){
            if(err){
              data.password = inputPassword;
              data.password_confirmation = inputPassword;
              persistence.remove(progress);
              persistence.flush(_.bind(function(){
                this.messenger.broadcastToAllTabs('broadcast:register:failed', {err: err, serverData: serverData});
                callback(err, serverData);
              }, this));
            } else {
              persistence.remove(progress);
              this._afterLoginOrRegister(serverData, inputPassword, function(){callback(null, serverData);});
            }
          },this));
        },this));

      },

      passwordCardAuth: function(data, callback) {
        callback = callback || emptyCallback;

        data.password = crypto.getServerPassword(data.password);

        this.licenseServer.passwordCardAuth(data, _.bind(function(err, serverData){
          if (err) {
            callback({success: false});
          } else {
            callback({success: true, token: serverData.token});
          }
        },this));
      },

      getGenericBiometricKey: function(data, callback) {
        callback = callback || emptyCallback;
        memoryStore.get("biometricKeyToken", _.bind(function(oldToken){
          var data = null;
          if (oldToken) {
            data = {old_token: oldToken};
          }

          this.licenseServer.getGenericBiometricKey(data, _.bind(function(err, serverData){
            if (err) {
              callback({success: false});
            } else {
              callback({success: true, key: serverData.key});
            }
          },this));
        }, this));
      },

      createBiometricAuthRequest: function(data, callback) {
        callback = callback || emptyCallback;

        this.licenseServer.createBiometricAuthRequest(data, _.bind(function(err, serverData){
          if (err) {
            callback({success: false, error: err.errorCode});
          } else {
            callback({success: true});
          }
        },this));
      },

      getBiometricAuthResponse: function(data, callback) {
        callback = callback || emptyCallback;

        this.licenseServer.getBiometricAuthResponse(data, _.bind(function(err, serverData){
          if (err) {
            callback({success: false});
          } else {
            callback(serverData);
          }
        },this));
      },

      subscribe: function(billing_info, subscriptionType, source, callback) {
        callback = callback || emptyCallback;

        this.licenseServer.signature({}, _.bind(function(err, serverData){
          if (serverData[subscriptionType + '_options_signature']) {
            // submit billing data to recurly
            var data = {
              signature: serverData[subscriptionType + '_options_signature'],
              account: serverData[subscriptionType + '_options'].account,
              billing_info: billing_info,
              subscription: serverData[subscriptionType + '_options'].subscription,
              currency: serverData.recurly_config.currency
            };

            var subdomain = serverData.recurly_config.subdomain;
            var url = 'https://'+ subdomain+'.recurly.com/jsonp/'+subdomain+'/subscribe';

            function handleRecurlyResponse(data) {
              var startIdx = data.indexOf('{');
              var endIdx = data.lastIndexOf('}');
              var response = JSON.parse(data.substr(startIdx, endIdx - startIdx + 1));
              if (response.success) {
                this.licenseServer.subscribed({recurly_token: response.success.token, source: source}, _.bind(function(err, serverData){
                  if (!err) {
                    this.refreshEntitlements(_.bind(function(){
                      this.refreshLastFourCardDigits({}, function(){
                        callback(false, {});
                      });
                    }, this));
                  } else {
                    callback(true, {errors: {base:['Error activating payments']}})
                  }
                }, this));
              } else {
                callback(true, response);
              }
            }

            abineAjax.ajax({
              dataType: 'text',
              url: url,
              data: data,
              success: _.bind(handleRecurlyResponse, this),
              error: _.bind(handleRecurlyResponse, this)
            });
          } else {
            // submit billing data to stripe
            var card = {
              name: (billing_info.first_name + ' ' + billing_info.last_name).replace(/^[\s]+|[\s]+$]/, '').replace(/[\s]+]/, ' '),
              number: billing_info.number,
              exp_month: billing_info.month,
              exp_year: billing_info.year,
              cvc: billing_info.verification_value,
              address_line1: billing_info.address1,
              address_line2: billing_info.address2,
              address_city: billing_info.city,
              address_state: billing_info.state,
              address_zip: billing_info.zip,
              address_country: billing_info.country
            };

            var plan_code = serverData[subscriptionType + '_options'].subscription.plan_code;

            function handleStripeResponse(status, data) {
              if (status != 200) {
                callback(true, {errors: {base:[data.error.message]}})
                return;
              }
              if (data.id) {
                var params = {
                  stripe_token: data.id, source: source, ip_address: data.client_ip,
                  plan_code: plan_code, first_six: card.number.substr(0, 6)
                };
              } else {
                var params = {
                  source: source,
                  plan_code: plan_code,
                  use_existing: 'true'
                };
              }
              this.licenseServer.stripe_subscribe(params, _.bind(function(err, serverData){
                if (!err) {
                  this.refreshEntitlements(_.bind(function(){
                    this.refreshLastFourCardDigits({}, function(){
                      callback(false, {});
                    });
                  }, this));
                } else {
                  callback(true, {errors: {base:['Error activating payments']}})
                }
              }, this));
            }

            if (card.number) {
              Stripe.setPublishableKey(config.stripe_key);
              Stripe.createToken(card, _.bind(handleStripeResponse, this));
            } else {
              handleStripeResponse.apply(this, [200, {}])
            }
          }

        }, this));
      },


      refreshEntitlements: function(callback){
        callback = callback || emptyCallback;

        this.licenseServer.refreshEntitlements({}, _.bind(function(err,serverData){
          if(err){
            callback(err, serverData);
          } else {
            this.updateEntitlementPrefs(serverData.user.licenses, _.bind(function(){
              this.updateHasEnabledPaymentsPref(serverData.user.activated_cards_at, _.bind(function(){
                this.updateHasAuthorizedCoinbase(serverData.user.has_authorized_coinbase, _.bind(function(){
                  persistence.flush(_.bind(function(){
                    this.messenger.broadcastToAllTabs('broadcast:preference:reload', {});
                    callback(null, serverData);
                  }, this));
                }, this));
              },this));
            },this));
          }
        }, this));
      },

      updateEntitlementPrefs: function(licenses, callback){
        callback = callback || emptyCallback;

        var keyToCodeMap = {
          'entitledToSync': 'sync',
          'entitledToPhone': 'protected_phone',
          'entitledToPayments': 'payments',
          'entitledToAccounts': 'accounts'
        };

        var codePrefMap = {};
        var licenseFeatureCodes = {};

        var keyFilter = function(pref){ return pref.key in keyToCodeMap; };

        storage.Preference.listCached(keyFilter, _.bind(function(prefs){
          _.each(prefs, function(pref){
            codePrefMap[keyToCodeMap[pref.key]] = pref;
          });

          for (var key in keyToCodeMap) {
            if (keyToCodeMap[key] in codePrefMap) continue;
            codePrefMap[keyToCodeMap[key]] = new storage.Preference({key: key, value: 'false'});
            persistence.add(codePrefMap[keyToCodeMap[key]]);
          }

          var hadPhone = codePrefMap['protected_phone'].isTrue();
          var hadPayments = codePrefMap['payments'].isTrue();
          var hadAccounts = codePrefMap['accounts'].isTrue();

          _.each(licenses, function(license){
            if(codePrefMap[license.feature_code]){
              codePrefMap[license.feature_code].setTrue();
            }
            licenseFeatureCodes[license.feature_code] = 1;
          });

          // if it wasn't one of the features in the licenses returned, there's no license for it, so mark it false
          _.each(_.keys(codePrefMap), function(key){
            if(!(key in licenseFeatureCodes)){ // if you didn't find the key
              codePrefMap[key].setFalse();
            }
          });

          // when your subscription expires or is canceled remove shielded/masked phones from local db
          if(hadPhone && codePrefMap['protected_phone'].isFalse()){
            storage.ShieldedPhone.all().destroyAll();
            storage.DisposablePhone.all().destroyAll();
          }

          storage.Preference.createOrModify({key: 'hadEmail', value: 'true'});
          storage.Preference.createOrModify({key: 'hadPhone', value: codePrefMap['protected_phone']&&codePrefMap['protected_phone'].isTrue()?'true':'false'});
          storage.Preference.createOrModify({key: 'hadCard', value: codePrefMap['payments']&&codePrefMap['payments'].isTrue()?'true':'false'});

          if (codePrefMap['payments'] && codePrefMap['payments'].isTrue()){
            // try fetching shielded card from server in case recurly has it (from previous install or other browser)
            this.refreshLastFourCardDigits();
          }

          // "once you have payments, it does not expire, ever." --Hilario
          if (hadPayments) {
            codePrefMap['payments'].setTrue();
          }

          if (!hadAccounts && codePrefMap['accounts'] && codePrefMap['accounts'].isTrue()) {
            storage.Preference.batchCreateOrModify([
              {key: 'suggestStrongPassword', value: 'true'},
              {key: 'suggestAddresses', value: 'true'},
              {key: 'helpMeLogin', value: 'true'},
              {key: 'rememberAccounts', value: 'true'}
            ]);
          }

          _.each(prefs, function(pref){
            persistence.forceAdd(pref);
          });

          persistence.flush(function(){
            callback();
          });

        },this));
      },


      refreshLastFourCardDigits: function(data, callback){
        callback = callback || emptyCallback;

        this.licenseServer.billingInfo({}, _.bind(function(err, serverData){
          if(err){
            callback(err, serverData);
          } else {
            if (serverData.subscription && !serverData.subscription.card_not_setup) {
              storage.Preference.findBy('key', 'cardNotSetup', function(pref){
                if (pref) {
                  persistence.remove(pref);
                }
              });

              storage.ShieldedCard.index(null, _.bind(function(shieldedCards){
                if(shieldedCards && shieldedCards[0]){
                  var shieldedCard = shieldedCards[0];
                  shieldedCard.last_four = serverData.last_four;
                  shieldedCard.first_name = serverData.first_name;
                  shieldedCard.last_name = serverData.last_name;
                  shieldedCard.month = serverData.month;
                  shieldedCard.year = serverData.year;
                  shieldedCard.card_type = serverData.card_type;
                  shieldedCard.zip = serverData.zip;

                  persistence.flush(_.bind(function(){
                    this.messenger.broadcast("broadcast:billinginfo:updated",{});
                    callback(null, serverData);
                  },this));
                } else {
                  storage.ShieldedCard.create({
                    payload: serverData
                  }, _.bind(function(){
                    this.messenger.broadcast("broadcast:billinginfo:updated",{});
                    callback(null, serverData);
                  },this));
                }
              },this))
            } else {
              if (serverData.subscription && serverData.subscription.card_not_setup) {
                storage.Preference.createOrModify({key: 'cardNotSetup', value: 'true'}, function(){
                  callback(null, serverData);
                });
              } else {
                callback(null, serverData);
              }
            }
          }
        },this));
      },

      updateHasEnabledPaymentsPref: function(isTrue, callback){
        callback = callback || emptyCallback();

        storage.Preference.batchFindBy([
          {by: 'key', value: 'hasEnabledPayments'},
          {by: 'key', value: 'suggestProtectedCard'},
          {by: 'key', value: 'autoPremiumRegisterShown'}
        ], _.bind(function(prefMap){
          var pref = prefMap.hasEnabledPayments;
          if (!pref) {
            pref = new storage.Preference({key: 'hasEnabledPayments', value: 'false'});
            persistence.add(pref);
          }
          if (pref && pref.isTrue() != Boolean(isTrue)) {
            if(isTrue) {
              if (prefMap.autoPremiumRegisterShown && prefMap.autoPremiumRegisterShown.isTrue()) {
                // enable card if it was disabled earlier
                persistence.remove(prefMap.autoPremiumRegisterShown);
                if (!prefMap.suggestProtectedCard) {
                  prefMap.suggestProtectedCard = new storage.Preference({key:'suggestProtectedCard', value: 'true'});
                  persistence.add(prefMap.suggestProtectedCard);
                }
                prefMap.suggestProtectedCard.setTrue();
              }
              pref.setTrue();
            } else {
              pref.setFalse();
            }
          }
          persistence.flush(function(){callback();});
        }, this));
      },

      updateHasAuthorizedCoinbase: function(isTrue, callback){
        callback = callback || emptyCallback();

        storage.Preference.createOrModify({key: 'hasAuthorizedCoinbase', value: isTrue?true:false}, function(){
          callback();
        });
      },

      retrieveMedalsEarned: function(callback){
        this.licenseServer.retrieveMedalsEarned({}, _.bind(function(err, serverData){
          if(err){
            callback(err);
          } else {
            callback(null, serverData);
          }
        }));
      },

      pushSync: function(syncCount){
        this.licenseServer.pushSync({count:syncCount});
      },

      incrementMedalsEarned: function(medalsEarned, callback){
        var data = {additional_medals_earned: medalsEarned};
        this.licenseServer.incrementMedalsEarned(data, _.bind(function(err, serverData){
          if(err){
            callback(err);
          } else {
            callback(null, serverData);
          }
        }, this));
      },

      incrementMedalsEarnedWithCurrentMedals: function(incrementCardsEarned, callback){
        storage.DntStats.index(null, _.bind(function(dntStats){
          var dntStats = dntStats[0];
          var numberOfMedals = dntStats.currentBadgeIndex + 1; // badge index is 0 indexed and initialized at -1
          if(numberOfMedals > 0){
            if (incrementCardsEarned) {
              storage.Preference.createOrIncrementByInteger({key:'cardsEarned', value: numberOfMedals}, function(){}); // local approximation of how many cards user has earned. assumes 1 to 1 medal to cards!
            }
            this.incrementMedalsEarned(numberOfMedals, callback); // assumes we'll always want 1 medal = 1 free card. make this configurable?
          } else {
            callback();
          }
        }, this));
      },

      logCheckout: function(data, callback){
        if (securityManager.isLocked()) {
          if (callback) {
            callback(true);
          }
          return;
        }
        storage.Preference.findBy('key', 'shouldSendShoppingFeedback', _.bind(function(sendData) {
          callback = callback || emptyCallback;
          if (!sendData || sendData.isTrue()) {
            if (data.amount && !isNaN(parseFloat(data.amount))) {
              data.amount = parseInt(parseFloat(data.amount) * 100);
            }
            data.device_id = securityManager.getAnonymousId();
            this.licenseServer.logCheckout(data, _.bind(function (err, serverData) {
              if (err) {
                callback(err);
              } else {
                callback(null, serverData);
              }
            }));
          } else {
            callback(true);
          }
        }, this));
      },

      retrieveSyncstoreToken: function(callback){
        callback = callback || emptyCallback;
        this.licenseServer.retrieveSyncstoreToken({}, _.bind(function(err, serverData){
          if(err){
            callback(err);
          } else {
            callback(null, serverData);
          }
        }));
      }
    });

    var licenseModule = new LicenseModule({});
    modules.licenseModule = licenseModule;
    return licenseModule;

  });

ABINE_DNTME.define("abine/modules/panels",
  ["documentcloud/underscore", "abine/core", 'abine/timer', 'storage', 'persistence',
    'abine/ajax', 'abine/config', 'abine/securityManager', 'abine/mappingServer','abine/responseCodes',
    'modules', 'abine/licenseServer', 'abine/webapp'],
  function (_, abineCore, abineTimer, storage, persistence, abineAjax, config, securityManager, mappingServer,
            responseCodes, modules, licenseServer, webapp) {

    var emptyCallback = function() {};

    var installDate = null;
    var cachedIconPanels = null, currentTabIcon = {};
    var cachedDntmePanels = null;
    var cachedDntmeGrowls = null;

    var shownGrowls = {};


    function getBestMatchingPanels(cache, host, trackers, featureExploredDate) {
      var bestOptions = [];
      for (var id in cache) {
        if (id == 'length') continue;
        var panel = cache[id];
        var showRate = parseFloat(panel.show_rate);
        if (Math.random() > showRate || parseInt(panel.masked_emails) > 0) {
          // skip this panel
          continue;
        }

        if(panel.field_type.indexOf('new-feature-added') != -1){
          if(featureExploredDate < new Date(panel.created_at)){
            if(panelOptionScore(panel, host, trackers)){
              bestOptions.unshift(panel); // new feature added always gets precedence over any other panel
              break;
            }
          }
        } else {
          if(panelOptionScore(panel, host, trackers)){
            if(panelOptionScore > 1){
              bestOptions.unshift(panel); // at this time, > 1 means put to the front
            } else {
              bestOptions.push(panel);
            }
          }
        }

      }
      return bestOptions;
    }

    // returns score for each panel 0-10; 10 gets highest priority at this time, only returns 0, 1, 10
    function panelOptionScore(panel, host, trackers){
      var option = panel.targeting_option;
      if (option) {
        if (option.masking) {
          if ((option.masking == 'off' && !ABINE_DNTME.maskme_installed) ||
              (option.masking == 'on' && ABINE_DNTME.maskme_installed)) {
            LOG_DEBUG && ABINE_DNTME.log.debug("panel skip as masking option does not match " + panel.id);
            return 0;
          }
        }
        if (option.blocked) {
          if (ABINE_DNTME.dnt_api.getAllTimeTotalTrackers() < parseInt(option.blocked)) {
            LOG_DEBUG && ABINE_DNTME.log.debug("panelskip as total blocked is less " + panel.id);
            return 0;
          }
        }
        if (option.install_age) {
          if (parseInt(option.install_age) > (Date.now() - installDate) / (24 * 60 * 60 * 1000)) {
            LOG_DEBUG && ABINE_DNTME.log.debug("panel skip as install_age is less " + panel.id);
            return 0;
          }
        }

        if (option.domain) {
          if (host && host.match(new RegExp(option.domain, 'i'))) {
            LOG_DEBUG && ABINE_DNTME.log.debug("panel matched by domain " + panel.id);
            return 10;
          }
        } else if (option.trackers) {
          if (trackers >= parseInt(option.trackers)) {
            LOG_DEBUG && ABINE_DNTME.log.debug("panel matched by trackers " + panel.id);
            return 1;
          }
        } else {
          LOG_DEBUG && ABINE_DNTME.log.debug("panel matched as no negative targeting option " + panel.id);
          return 1;
        }
      } else {
        LOG_DEBUG && ABINE_DNTME.log.debug("panel matched as no targeting option " + panel.id);
        return 1;
      }
    }

    function findBestToolbarIconPanel(iconData, tabId, callback) {
      storage.Preference.findBy('key', 'new_feature_explored', function(pref){
        var newFeatureExplored = false;
        if (pref){
          newFeatureExplored = new Date(pref.value);
        }

        var bestOptions = getBestMatchingPanels(cachedIconPanels, iconData.host, iconData.total, newFeatureExplored);
        if (bestOptions.length > 0) {
          if (!bestOptions[0].image) {
            var panelsection = bestOptions[0].panel_positions[0].panel_section;
            var match = panelsection.body.match(/data:[^\'"]+/);
            if (match) {
              bestOptions[0].image = match[0];
            }
          }
          storage.PanelActivity.addImpression({payload:{panelId:bestOptions[0].id}});
          currentTabIcon[tabId] = {id: bestOptions[0].id, host: iconData.host};
        } else {
          currentTabIcon[tabId] = {id: -1, host: iconData.host};
        }

        callback(currentTabIcon[tabId]);
      });
    }

    function setupToolbarPanelIcon(iconData, callback) {
      if (!cachedIconPanels) {
        currentTabIcon = {};
        storage.Preference.findBy('key', 'dynamicPanels', function(pref){
          cachedIconPanels = {length:0};
          if (pref) {
            var allPanels = JSON.parse(pref.value).panels;
            _.each(allPanels, function(panel) {
              if (panel.field_type.indexOf('dntme-toolbar-button') != -1) {
                cachedIconPanels[panel.id] = panel;
                cachedIconPanels.length++;
              }
            });
          }
          if (cachedIconPanels.length > 0) {
            setupToolbarPanelIcon(iconData, callback);
          } else {
            callback();
          }
        });
      } else {
        var tabId = iconData.tabId;
        if (tabId in currentTabIcon) {
          if (currentTabIcon[tabId].host != iconData.host) {
            delete currentTabIcon[tabId];
          }
        }
        if (!(tabId in currentTabIcon)) {
          findBestToolbarIconPanel(iconData, tabId, function(){
            if (tabId in currentTabIcon && currentTabIcon[tabId].id != -1) {
              iconData.customImage = cachedIconPanels[currentTabIcon[tabId].id].image;
            }
            callback();
          });
        } else {
          if (tabId in currentTabIcon && currentTabIcon[tabId].id != -1) {
            iconData.customImage = cachedIconPanels[currentTabIcon[tabId].id].image;
          }
          callback();
        }
      }
    }

    function setupDNTMePanels(tabData, callback) {
      if (!cachedDntmePanels) {
        storage.Preference.findBy('key', 'dynamicPanels', function(pref){
          cachedDntmePanels = {length:0};
          if (pref) {
            var allPanels = JSON.parse(pref.value).panels;
            _.each(allPanels, function(panel) {
              if (panel.field_type.indexOf('dntme-panel') != -1) {
                cachedDntmePanels[panel.id] = panel;
                cachedDntmePanels.length++;
              }
            });
          }
          if (cachedDntmePanels.length > 0) {
            setupDNTMePanels(tabData, callback);
          } else {
            callback(null);
          }
        });
      } else {
        storage.Preference.findBy('key', 'new_feature_explored', function(pref){
          var newFeatureExplored = false;
          if (pref){
            newFeatureExplored = new Date(pref.value);
          }
          var bestOptions = getBestMatchingPanels(cachedDntmePanels, tabData.host, tabData.trackers.length, newFeatureExplored);
          if (bestOptions.length > 0) {
            storage.PanelActivity.addImpression({payload:{panelId:bestOptions[0].id}});
            callback(bestOptions[0]);
          } else {
            callback(null);
          }
        });
      }
    }

    function setupDNTMeGrowls(tabData, callback) {
      if (!cachedDntmeGrowls) {
        storage.Preference.findBy('key', 'dynamicPanels', function(pref){
          cachedDntmeGrowls = {length:0};
          if (pref) {
            var allPanels = JSON.parse(pref.value).panels;
            _.each(allPanels, function(panel) {
              if (panel.field_type.indexOf('dntme-growl') != -1) {
                cachedDntmeGrowls[panel.id] = panel;
                cachedDntmeGrowls.length++;
              }
            });
          }
          if (cachedDntmeGrowls.length > 0) {
            setupDNTMeGrowls(tabData, callback);
          } else {
            callback(null);
          }
        });
      } else {
        var bestOptions = getBestMatchingPanels(cachedDntmeGrowls, tabData.host, tabData.trackers.length);
        if (bestOptions.length > 0 && !(bestOptions[0].id in shownGrowls)) {
          storage.PanelActivity.addImpression({payload:{panelId:bestOptions[0].id}});
          shownGrowls[bestOptions[0].id] = true;
          callback(bestOptions[0]);
          return;
        }
        callback(null);
      }
    }

    function initInstallDate(callback) {
      if (!installDate) {
        storage.Preference.findBy('key', 'installDate', _.bind(function(pref){
          if (pref){
            installDate = parseInt(pref.value);
          } else {
            installDate = Date.now();
          }
          callback();
        }, this));
      } else {
        callback();
      }
    }

    var PanelsModule = abineCore.BaseClass.extend({

      initialize: function(options){
        this.mappingServer = options.mappingServer || new mappingServer();
        this.licenseServer = options.licenseServer || new licenseServer();
      },

      getToolbarPanel: function(iconData, callback) {
        initInstallDate(function(){
          setupToolbarPanelIcon(iconData, function(){
            callback(iconData);
          });
        });
      },

      // return action if given tab was showing a panel icon
      // also records clicks for panel
      doToolbarPanelAction: function(tabId) {
        if (cachedIconPanels && tabId in currentTabIcon && currentTabIcon[tabId].id != -1) {
          var clickedPanel = cachedIconPanels[currentTabIcon[tabId].id];
          var panelActions = clickedPanel.panel_positions[0].panel_actions;
          if (panelActions.length > 0) {

            _.each(panelActions, _.bind(function(action){
              if (action.name == "open_registration_page") {
                webapp.openHome({id: clickedPanel.id, action: action.name});
              } else if (action.name == "open_page") {
                securityManager.openPage(action.extra);
              } else if (action.name == "trigger_click") {
                storage.PanelActivity.addClick({payload:{panelId: clickedPanel.id}});
              } else if(action.name == "trigger_conversion"){
                storage.PanelActivity.addConversion({payload:{panelId: clickedPanel.id}});
              } else if(action.name == "change_show_rate"){
                this.changePanelShowRate({panelId: clickedPanel.id, extra: action.extra});
              } else if(action.name == "no_action"){

              } else if(action.name == "open_plans_page"){
                webapp.openPremium("panelID"+clickedPanel.id, null, {id: clickedPanel.id, action: action.name});
              } else if(action.name == "open_snowman_page"){
                webapp.openSnowman(null, {id: clickedPanel.id, action: action.name});
              } else if(action.name == "open_activate_cards_page"){
                webapp.openCards({id: clickedPanel.id, action: action.name});
              } else if(action.name == "open_new_premium_feature_added_page"){
                webapp.openNewFeatureAdded({id: clickedPanel.id, action: action.name});
              }

            }, this));

          }
          cachedIconPanels.length--;
          delete currentTabIcon[tabId];
          ABINE_DNTME.dnt_api.triggerUpdateIcon(tabId);
          return true;
        }
        return false;
      },


      getDNTMePanel: function(tabId, callback) {
        var tabData = ABINE_DNTME.dnt_api.getTabData(tabId);
        if (!tabData) {
          // can happen for old tabs or tabs loaded before dntme is initialized
          callback(null);
          return;
        }
        initInstallDate(function(){
          setupDNTMePanels(tabData, function(panel){
            callback(panel);
          });
        });
      },

      getDNTMeGrowl: function(tabId, callback) {
        var tabData = ABINE_DNTME.dnt_api.getTabData(tabId);
        if (!tabData) {
          // can happen for old tabs or tabs loaded before dntme is initialized
          callback(null);
          return;
        }
        initInstallDate(function(){
          setupDNTMeGrowls(tabData, function(panel){
            callback(panel);
          });
        });
      },

      changePanelShowRate: function(payload, callback) {
        callback = callback || emptyCallback;
        var panelId = payload.panelId;
        var newShowRate = payload.extra;

        storage.Preference.findBy('key', 'dynamicPanels', _.bind(function (pref) {
          var dynamicPanels = JSON.parse(pref.value);
          _.each(dynamicPanels.panels, function(panel){
            if (panel.id == panelId) {
              panel.show_rate = newShowRate;
              if (cachedDntmePanels && cachedDntmePanels[panelId])
                cachedDntmePanels[panelId].show_rate = newShowRate;
              if (cachedIconPanels && cachedIconPanels[panelId])
                cachedIconPanels[panelId].show_rate = newShowRate;
              pref.value = JSON.stringify(dynamicPanels);
            }
          });
          persistence.flush(callback);
        }, this));
      },

      processPanelData: function (newPanels, callback) {
        callback = callback || emptyCallback;
        cachedIconPanels = null;
        cachedDntmePanels = null;
        cachedDntmeGrowls = null;
        storage.DisposableEmail.index(null, _.bind(function(emails){
          var emailsGenerated = emails.length;
          // update email condition for new panels
          _.each(newPanels.panels, function(panel){
            if (emailsGenerated >= panel.masked_emails) {
              panel.masked_emails = 0;
            }
          });
          storage.Preference.findBy('key', 'dynamicPanels', _.bind(function (pref) {
            if (!pref) {
              pref = new storage.Preference({key: 'dynamicPanels', value: JSON.stringify(newPanels)});
              persistence.add(pref);
              newPanels = newPanels.panels;
            } else {
              var oldPanels;
              try {
                oldPanels = JSON.parse(pref.value).panels;
              } catch(e){}
              if (!oldPanels) oldPanels = [];
              var mergedPanels = [], donePanels = {}, newPanelIds = {};
              newPanels = newPanels.panels;
              newPanels.forEach(function (panel) {
                newPanelIds[panel.id] = 1;
              });
              _.each(oldPanels, function(oldPanel){
                if (oldPanel.id in newPanelIds) {
                  // retain old panels that are in incoming panels
                  mergedPanels.push(oldPanel);
                  donePanels[oldPanel.id] = 1;
                  if (oldPanel.field_type.indexOf('dntme') == -1) {
                    // single panel for a field for non-dntme panels
                    donePanels[oldPanel.field_type] = oldPanel.id;
                  }

                  // update email condition for old panel
                  if (emailsGenerated >= oldPanel.masked_emails) {
                    oldPanel.masked_emails = 0;
                  }
                } else {
                  // remove any panel-activity for excluded panel
                  storage.PanelActivity.findBy('panelId', oldPanel.id, _.bind(function(oldPanelActivity){
                    if (oldPanelActivity) {
                      persistence.remove(oldPanelActivity);
                    }
                  }, this));
                }
              });
              newPanels.forEach(function (newPanel) {
                if (donePanels[newPanel.id]) return;
                if (newPanel.field_type in donePanels) {
                  // replace panel
                  for (var i=0;i<mergedPanels.length;i++) {
                    if (mergedPanels[i].id == donePanels[newPanel.field_type]) {
                      mergedPanels.splice(i, 1);
                      i--;
                    }
                  }
                }
                mergedPanels.push(newPanel);
              });
              pref.value = JSON.stringify({panels: mergedPanels});
            }

            _.async.forEach(newPanels, _.bind(function(panel, cb) {
              storage.PanelActivity.findBy('panelId', panel.id, _.bind(function(panelActivity){
                if (!panelActivity) {
                  panelActivity = new storage.PanelActivity({
                    panelId: panel.id,
                    impressions: 0,
                    users: 1,
                    clicks: 0,
                    completions: 0
                  });

                  persistence.add(panelActivity);
                }
                cb();
              }, this));
            }, this), _.bind(function(){
              persistence.flush(_.bind(function () {
                callback();
              }, this));
            }, this));

          }, this));
        }, this));
      },

      setupDynamicPanels: function(user_state, callback){
        callback = callback || emptyCallback;

        this.mappingServer.getPanels({
          browser: config.browser,
          tag: config.tags[0],
          user_state: user_state?user_state:"unregistered"},
          _.bind(function(err, data){
            if(err){
              callback();
            } else {
              this.processPanelData(data.panels, callback);
            }
        },this));
      },

      panelConversionCheck: function(action, callback){
        callback = callback || emptyCallback;

        if (ABINE_DNTME.lastPanel && ABINE_DNTME.lastPanel[action]) {
          var id = ABINE_DNTME.lastPanel[action];

          delete ABINE_DNTME.lastPanel[action];
          storage.PanelActivity.findBy('panelId', id, function(panelActivity){
            if (panelActivity) {
              panelActivity.completions++;
            }
            persistence.flush(function(){callback();});
          });
        } else {
          callback();
        }
      },

      // sends non-marketing panel activity to license server
      sendPanelActivity: function(callback){
        function wrapCallback(){
          if (callback) callback();
          callback = null;
        }

        storage.Preference.batchFindBy([
            {by: 'key', value: 'shouldSendErrorReports'},
            {by: 'key', value: 'suggestProtectedEmail'},
            {by: 'key', value: 'suggestProtectedPhone'},
            {by: 'key', value: 'entitledToPhone'},
            {by: 'key', value: 'hasEnabledPayments'},
            {by: 'key', value: 'suggestProtectedCard'},
            {by: 'key', value: 'suggestRealCard'},
            {by: 'key', value: 'suggestAddresses'},
            {by: 'key', value: 'suggestStrongPassword'},
            {by: 'key', value: 'helpMeLogin'},
            {by: 'key', value: 'rememberAccounts'}
        ], _.bind(function(prefs){
        storage.ShieldedPhone.index(null, _.bind(function(phones){
          prefs['browsing'] = ABINE_DNTME.dnt_api.getDefaultBlocking();
          prefs['errors'] = (prefs.shouldSendErrorReports && prefs.shouldSendErrorReports.isTrue());
          prefs['email'] = (prefs.suggestProtectedEmail && prefs.suggestProtectedEmail.isTrue());
          prefs['phone'] = (prefs.suggestProtectedPhone && prefs.suggestProtectedPhone.isTrue() && prefs.entitledToPhone && prefs.entitledToPhone.isTrue() && phones.length > 0 && phones[0].isValidated());
          prefs['card'] = (prefs.suggestProtectedCard && prefs.suggestProtectedCard.isTrue() && prefs.hasEnabledPayments && prefs.hasEnabledPayments.isTrue());
          prefs['password'] = (prefs.suggestStrongPassword && prefs.suggestStrongPassword.isTrue());
          prefs['address'] = (prefs.suggestAddresses && prefs.suggestAddresses.isTrue());
          prefs['realcard'] = (prefs.suggestRealCard && prefs.suggestRealCard.isTrue());
          prefs['login'] = (prefs.helpMeLogin && prefs.helpMeLogin.isTrue());
          prefs['account'] = (prefs.rememberAccounts && prefs.rememberAccounts.isTrue());

          if (prefs['errors']) {
            storage.Account.index(null, _.bind(function (accounts) {
              storage.Address.index(null, _.bind(function (addresses) {
                storage.RealCard.index(null, _.bind(function (realCards) {
                  storage.Identity.index(null, _.bind(function (identities) {
                    storage.Note.index(null, _.bind(function (notes) {
                      storage.AccountHistory.index(null, _.bind(function (histories) {
                        var realcards_count = realCards.length;
                        var addresses_count = addresses.length;
                        var accounts_count = accounts.length;
                        var identities_count = identities.length;
                        var histories_count = histories.length;
                        var notes_count = notes.length;
                        var masked_email_accounts_count = 0;

                        masked_email_accounts_count = accounts.filter(function (acct) {
                          return !!(acct.email || acct.username || "").match(new RegExp('@(opayq.com|dontrack.me|abinemail.com|beconfidential.com|blurmail.net|blurme.net|maskmemail.com|maskedmails.com|maskedmail.net|oo-mail.net|goobers.net|maskmemail.net)$', 'i'));
                        }).length;

                        storage.Blacklist.index(null, _.bind(function (blacklist) {
                          var site_totals = {
                            "account": 0,
                            "address": 0,
                            "card": 0,
                            "domain": 0,
                            "email": 0,
                            "password": 0,
                            "phone": 0,
                            "real_card": 0,
                            "save_account": 0,
                            "tracker": 0
                          };
                          _.each(blacklist, function (item) {
                            _.each(item._data, function (value, key) {
                              if (value)
                                site_totals[key] = site_totals[key] + 1;
                            });
                          });
                          storage.PanelActivity.index(null, _.bind(function (panelActivities) {
                            var license_params = {
                              panel_data: {
                                "global_settings": {
                                  "account": prefs["account"] ? 1 : 0,
                                  "address": prefs["address"] ? 1 : 0,
                                  "card": prefs["card"] ? 1 : 0,
                                  "email": prefs["email"] ? 1 : 0,
                                  "password": prefs["password"] ? 1 : 0,
                                  "phone": prefs["phone"] ? 1 : 0,
                                  "real_card": prefs["realcard"] ? 1 : 0,
                                  "login": prefs["login"] ? 1 : 0,
                                  "tracker": prefs["browsing"] ? 1 : 0
                                },
                                "site_totals": {
                                  "account": site_totals["account"],
                                  "address": site_totals["address"],
                                  "card": site_totals["card"],
                                  "domain": site_totals["domain"],
                                  "email": site_totals["email"],
                                  "password": site_totals["password"],
                                  "phone": site_totals["phone"],
                                  "real_card": site_totals["real_card"],
                                  "save_account": site_totals["save_account"],
                                  "tracker": site_totals["tracker"]
                                },
                                "user_counts": {
                                  "accounts_count": accounts_count,
                                  'masked_email_accounts_count': masked_email_accounts_count,
                                  "real_cards_count": realcards_count,
                                  "addresses_count": addresses_count,
                                  "identities_count": identities_count,
                                  "notes_count": notes_count,
                                  "histories_count": histories_count
                                }
                              }
                            };

                            _.each(panelActivities, function (panelActivity) {
                              if (!panelActivity.isMarketingPanel() && panelActivity.hasData()) {
                                if (!license_params.panel_data) {
                                  license_params.panel_data = {};
                                }
                                license_params.panel_data[panelActivity.panelId] = panelActivity.toJSON();
                              }
                            });

                            if (license_params.panel_data) {
                              this.licenseServer.sendPanelActivity(license_params, function (err, data) {
                                if (err) {
                                  wrapCallback()
                                } else {
                                  _.each(panelActivities, function (panelActivity) {
                                    if (panelActivity.panelId in license_params.panel_data) {
                                      panelActivity.reset(license_params.panel_data[panelActivity.panelId]);
                                    }
                                  });

                                  persistence.flush(function () {
                                    wrapCallback();
                                  });
                                }
                              });
                            } else {
                              wrapCallback();
                            }
                          }, this));
                        }, this));
                      }, this));
                    }, this));
                  }, this));
                }, this));
              }, this));
            }, this));
          } else {
            wrapCallback();
          }
        },this));
        },this));
      }

    });

    var panelsModule = new PanelsModule({});

    modules.panelsModule = panelsModule;

    return panelsModule;

  });

ABINE_DNTME.define("abine/modules/mappings",
  ["documentcloud/underscore", "abine/core", 'abine/timer', 'storage', 'persistence',
    'abine/ajax', 'abine/config', 'abine/securityManager', 'abine/mappingServer', 'modules', "abine/localStorage"],
  function (_, abineCore, abineTimer, storage, persistence,
            abineAjax, config, securityManager, mappingServer, modules, localStorage) {

    var emptyCallback = function() {};
    var ONE_DAY_MS = 24 * 60 * 60 * 1000;

    var todayDate = function() {
      var today = new Date();
      today.setHours(0); today.setMinutes(0); today.setSeconds(0); today.setMilliseconds(0); // js dates suck
      return today;
    };

    var MappingsModule = abineCore.BaseClass.extend({

      initialize: function(options) {
        this.mappingServer = options.mappingServer || new mappingServer();
      },

      // Check for mappings update once per day
      startDailyMappingCheck: function(callback) {
        callback = callback || emptyCallback;

        storage.Preference.findBy('key', 'lastMappingCheck', _.bind(function(lastMappingCheckPref){
          if (lastMappingCheckPref && lastMappingCheckPref.value) {
            var lastCheckDate = new Date(parseInt(lastMappingCheckPref.value));
            if (lastCheckDate.valueOf() < ((new Date()).getTime() - ONE_DAY_MS) ) {
              LOG_DEBUG && ABINE_DNTME.log.debug("making daily mapping check because its been 24 hours since last time");
              this.makeDailyMappingCheck(callback);
            } else {
              LOG_INFO && ABINE_DNTME.log.info("Already checked today.");
              this.delayDailyMappingCheck(callback);
            }
          } else {
            LOG_INFO && ABINE_DNTME.log.info("never checked, checking now");
            this.makeDailyMappingCheck(callback);
          }
        },this));
      },

      forceMappingUpdate: function(doNotClear, callback) {
        if (!callback) {
          callback = doNotClear;
          doNotClear = false;
        }
        if (!doNotClear) {
          storage.Preference.removeByKey('mapping-version');
          storage.MappedForm.clearForms();
        }
        this.makeDailyMappingCheck(callback);
      },

      processMapping:function (data, callback){
        var start = Date.now();
        storage.MappedForm.clearForms();

        if (data.login_urls) {
          storage.LoginUrl.saveAll(data.login_urls);
        }

        var retainForms = [];
        _.each(data.mappings, function (form) {
          var newForm = {
            domain: form.domain,
            signature: form.signature,
            mapping_signature: form.mapping_signature,
            formType: form.formType,

            fields: form.fields
          };

          retainForms.push(newForm);
        });

        storage.MappedForm.saveAllForms(retainForms);
        LOG_DEBUG && ABINE_DNTME.log.debug(retainForms.length + " mappings. time taken " + (Date.now() - start) + "ms");

        this.writeDailyMappingCheckPref(_.bind(function () {
          this.delayDailyMappingCheck(function () {
            callback();
          });
        }, this));
      },


      // refresh panels only
      refreshPanels: function() {
        storage.Preference.batchFindBy([
          {by:'key', value: 'lastUserState'},
          {by:'key', value: 'dynamicPanels'},
          {by:'key', value: 'hasRegistered'},
          {by:'key', value: 'entitledToPhone'}
        ], _.bind(function(prefMap){

          var oldState = prefMap['lastUserState']?prefMap['lastUserState'].value:'';
          var newState = 'unregistered';

          if (prefMap['hasRegistered'] && prefMap['hasRegistered'].isTrue()) {
            if (prefMap['entitledToPhone'] && prefMap['entitledToPhone'].isTrue()) {
              newState = 'premium';
            } else {
              newState = 'registered';
            }
          }

          if (oldState != newState || !prefMap['dynamicPanels']) {
            this.makeDailyMappingCheck(true);
          }
        }, this));

      },

      updateFormRules: function() {
        try {
          var url = 'https://rules.abine.com/form-rules-'+(ABINE_DNTME.config.environment)+'.json?from=extension';
          var self = this;
          abineAjax.ajax({
            url: url,
            success: function(data) {
              try {
                data = JSON.parse(data);
                if (data && data.Version) {
                  localStorage.setItem('form-rules', JSON.stringify(data));
                  self._cachedFormRules = data;
                }
              } catch(e){}
            }
          });

        } catch(e) {}
      },

      setFormRules: function(rules) {
        this._cachedFormRules = rules;
      },

      getFormRules: function(callback) {
        if (!this._cachedFormRules) {
          var rules = localStorage.getItem('form-rules');
          if (rules) {
            try {
              rules = JSON.parse(rules);
              this._cachedFormRules = rules;
            } catch(e){}
          }
        }
        callback(this._cachedFormRules);
      },

      // send panel activity data, receive mapping and panels in one call to mapping manifest.
      makeDailyMappingCheck: function(excludeMapping, callback) {
        if (excludeMapping && excludeMapping !== true) {
          callback = excludeMapping;
          excludeMapping = false;
        }
        callback = callback || emptyCallback;

        if (ABINE_DNTME.maskme_installed) {
          callback();
          return;
        }

        LOG_INFO && ABINE_DNTME.log.info("Making request to mappings API");

        // fetch new card rules in parallel
        storage.RealCard.updateCardRules();

        this.updateFormRules();

        modules.panelsModule.sendPanelActivity(_.bind(function(){

          var params = {
            get_panels: 1,
            get_login_urls: 1,
            product: 'dntme',
            tag: ABINE_DNTME.config.tags[0],
            browser: ABINE_DNTME.config.browser
          };

          if (excludeMapping) {
            params.exclude_mapping = true;
          }

          var sendMappingManifestRequest = _.bind(function(successCallback) {

            storage.Preference.findBy('key', 'shouldSendErrorReports', _.bind(function(sendData){
              if (!sendData || sendData.isTrue()) {
                storage.Preference.findBy('key', 'mapping:mapper', _.bind(function(mapper){
                  storage.Preference.findBy('key', 'mapping:batch', _.bind(function(batch){
                    var mappingToPush = localStorage.getItem('mapping-to-push');
                    if (mappingToPush && (!securityManager.isLocked() || mapper)) {
                      try {
                        mappingToPush = JSON.parse(mappingToPush);
                        var mapping = {forms: mappingToPush};
                        if (mapper && mapper.value) {
                          mapping.mapper = mapper.value;
                        }
                        if (batch && batch.value && batch.value != 'null' && batch.value != 'undefined') {
                          mapping.batch = batch.value;
                        }
                        if (!mapping.mapper) {
                          mapping.mapper = "user-"+localStorage.getItem('device_id');
                        }
                        this.mappingServer.pushMapping(mapping, _.bind(function(err, data){
                          if (!err) {
                            localStorage.removeItem('mapping-to-push');
                          }
                        }, this));
                      } catch(e) {
                        localStorage.removeItem('mapping-to-push');
                      }
                    }
                  }, this));
                }, this));

                var feedbackToPush = localStorage.getItem('fieldData-to-push');
                if (feedbackToPush && feedbackToPush != '{}') {
                  try {
                    feedbackToPush = JSON.parse(feedbackToPush);
                    this.mappingServer.saveFieldData({"field_data":feedbackToPush, "anonId": securityManager.getAnonymousId()});
                  } catch(e){
                    // ignore errors while parsing json because of bad character encoding
                  }
                  localStorage.removeItem('fieldData-to-push');
                }

                var trackerToPush = localStorage.getItem('trackerData-to-push');
                if (trackerToPush && trackerToPush != '{}') {
                  try {
                    trackerToPush = JSON.parse(trackerToPush);
                    this.mappingServer.saveTrackerData({"t":trackerToPush, "anonId": securityManager.getAnonymousId(), "new": 1});
                  } catch(e){
                    // ignore errors while parsing json because of bad character encoding
                  }
                  localStorage.removeItem('trackerData-to-push');
                }

                var pageToPush = localStorage.getItem('pageData-to-push');
                if (pageToPush && pageToPush != '{}') {
                  try {
                    pageToPush = JSON.parse(pageToPush);
                    this.mappingServer.savePageData({"p":pageToPush, "anonId": securityManager.getAnonymousId()});
                  } catch(e){
                    // ignore errors while parsing json because of bad character encoding
                  }
                  localStorage.removeItem('pageData-to-push');
                }
              } else {
                localStorage.removeItem('pageData-to-push');
                localStorage.removeItem('trackerData-to-push');
                localStorage.removeItem('fieldData-to-push');
                localStorage.removeItem('mapping-to-push');
              }
            }, this));

            this.mappingServer.getMappingVersion(_.bind(function(err, version){
              if (err) {
                this.delayDailyMappingCheck(function(){
                  callback();
                });
              } else {
                storage.Preference.findBy('key', 'mapping-version', _.bind(function(versionPref){
                  if (!versionPref || parseInt(versionPref.value) < parseInt(version)) {
                    storage.Preference.createOrModify({key: 'mapping-version', value: version}, emptyCallback);
                    this.mappingServer.getMappings(_.bind(function(err, data){
                      if (err) {
                        this.delayDailyMappingCheck(function(){
                          callback();
                        });
                      } else {
                        if (data.new_version && data.new_version != ABINE_DNTME.config.version) {
                          // new client version available
                          storage.Preference.createOrModify({key: 'new_version', value: data.new_version}, emptyCallback);
                        } else {
                          // im latest client
                          storage.Preference.removeByKey('new_version');
                        }
                        if (data.mappings) {
                          this.processMapping(data, callback);
                        }
                        if (data.panels) {
                          // has new panels.  store current user-state to check if we need to refresh panel when user-state changes
                          storage.Preference.createOrModify({key: 'lastUserState', value: params.user_state}, _.bind(function(){
                            modules.panelsModule.processPanelData({panels: data.panels});
                          }, this));
                        }
                        if (successCallback) successCallback();
                      }
                    },this))
                  } else {
                    this.writeDailyMappingCheckPref(_.bind(function () {
                      this.delayDailyMappingCheck(function () {
                        if (successCallback) successCallback();
                      });
                    }, this));
                  }
                }, this));
              }
            },this))
          }, this);

          storage.Preference.batchFindBy([
            {by:'key', value: 'shouldSendErrorReports'},
            {by:'key', value: 'dynamicPanels'},
            {by:'key', value: 'hasRegistered'},
            {by:'key', value: 'entitledToPhone'}
          ], _.bind(function(prefMap){
            if (prefMap['hasRegistered'] && prefMap['hasRegistered'].isTrue()) {
              if (prefMap['entitledToPhone'] && prefMap['entitledToPhone'].isTrue()) {
                params.user_state = 'premium';
              } else {
                params.user_state = 'registered';
              }
            } else {
              params.user_state = 'unregistered';
            }

            if (prefMap['dynamicPanels']) {
              var curPanels = JSON.parse(prefMap['dynamicPanels'].value);
              _.each(curPanels.panels, function(panel){
                if (!params.current_panels) {
                  params.current_panels = [];
                }
                params.current_panels.push(panel.id);
              });
            }

            if (!params.current_panels) {
              params.current_panels = [-1];
            }

            if (prefMap['shouldSendErrorReports'] && prefMap['shouldSendErrorReports'].isTrue()) {
              storage.PanelActivity.index(null, _.bind(function(panelActivities){
                _.each(panelActivities, function(panelActivity){
                  if (panelActivity.isMarketingPanel() && panelActivity.hasData()) {
                    if (!params.panel_data) {
                      params.panel_data = {};
                    }
                    params.panel_data[panelActivity.panelId] = panelActivity.toJSON();
                  }
                });
                if (params.panel_data) {
                  sendMappingManifestRequest(function() {
                    // reset counts on panel activities because we just successfully posted to server
                    _.each(panelActivities, function(panelActivity){
                      if (panelActivity.panelId in params.panel_data) {
                        panelActivity.reset(params.panel_data[panelActivity.panelId]);
                      }
                    });

                    persistence.flush();
                  });
                } else {
                  sendMappingManifestRequest();
                }
              }, this));
            } else {
              sendMappingManifestRequest();
            }

          },this));

        }, this));
      },

      writeDailyMappingCheckPref: function(callback) {
        callback = callback || emptyCallback;

        storage.Preference.findOrCreate({key:"lastMappingCheck"}, function(lastMappingCheckPref){
          lastMappingCheckPref.value = Math.floor(todayDate().valueOf()); // why is this a real in Chrome db?
          lastMappingCheckPref.modifiedAt = todayDate();
          persistence.flush(function(){
            callback();
          });
        });
      },

      sendTrackerAllow: function(domain, url, tracker, version) {
        url = url.replace(/[\?].+$/, '');
        this.mappingServer.sendTrackerAllow({domain: domain, url: url, tracker: tracker, version: version});
      },

      // start up a timer so that it happens tomorrow if we're still running
      delayDailyMappingCheck: function(callback) {
        callback = callback || emptyCallback;

        // delay between 30mins and 90mins
        var wait = (30 * 60 * 1000) + (60 * 60 * 1000 * Math.random());
        if (this.dailyMappingCheckTimeout) {
          abineTimer.clearTimeout(this.dailyMappingCheckTimeout);
        }

        LOG_INFO && ABINE_DNTME.log.info("delaying mapping update check by "+(wait/60000)+" minutes");
        this.dailyMappingCheckTimeout = abineTimer.setTimeout(_.bind(this.startDailyMappingCheck, this), wait);

        callback();
      },

      getMapperDetails: function(callback) {
        this.mappingServer.getMapperDetails({}, _.bind(function(err, data) {
          if (err) {
            callback();
          } else {
            callback(data)
          }
        }, this));
      },

      getOneFormMappings: function(data, callback) {
        this.mappingServer.getOneFormMappings(data, _.bind(function(err, data) {
          if (err) {
            callback();
          } else {
            callback(data)
          }
        }, this));
      },

      sendAutofillFeedback: function(data, callback) {
        this.mappingServer.sendAutofillFeedback(data, _.bind(function(err, data) {
          if (err) {
            callback();
          } else {
            callback(data)
          }
        }, this));
      },

      sendFormBlob: function(data, callback) {
        this.mappingServer.sendFormBlob(data, _.bind(function(err, data) {
          if (err) {
            callback();
          } else {
            callback(data)
          }
        }, this));
      }
    });

    var mappingsModule = new MappingsModule({});


    modules.mappingsModule = mappingsModule;

    return mappingsModule;
  });

ABINE_DNTME.define('abine/modules/criticalProcess',
  ["documentcloud/underscore", "abine/core", 'abine/timer', 'storage', 'persistence', 'modules'],
  function(_, abineCore, abineTimer, storage, persistence, modules){

  var emptyCallback = function(){};

  var CriticalProcessModule = abineCore.BaseClass.extend({
    initialize: function(options, callback){
      callback = callback || emptyCallback;
    },

    setMessenger: function(messenger){
      this.messenger = messenger;
    },

    start: function(data, callback){
      this.messenger.broadcast('background:criticalprocess:start', data);
    },

    end: function(data, callback){
      this.messenger.broadcast('background:criticalprocess:end', data);
    },

    update: function(data, callback){
      this.messenger.broadcast('background:criticalprocess:update', data);
    },

    askForReview: function(data, callback){
      this.messenger.broadcast('background:criticalprocess:review', data);
    },

    error: function(data, callback){
      this.messenger.broadcast('background:criticalprocess:error', data);
    }
  });

  var criticalProcess = new CriticalProcessModule({});
  modules.criticalProcess = criticalProcess;

  return criticalProcess;

});

ABINE_DNTME.define('abine/modules/payments',
  ["documentcloud/underscore", "abine/core", 'abine/timer', 'storage', 'persistence', 'modules',
    'abine/paymentsServer', "abine/memoryStore"],
  function(_, abineCore, abineTimer, storage, persistence, modules, paymentsServer, memoryStore){

    var emptyCallback = function(){};

    var PaymentsModule = abineCore.BaseClass.extend({
      initialize: function(options){
        this.paymentsServer = options.paymentsServer || new paymentsServer();
      },

      retrieveCardCredit: function(callback){
        this.paymentsServer.retrieveCardCredit({}, _.bind(function(err, serverData){
          if(err){
            callback(err);
          } else {
            callback(null, serverData);
          }
        }));
      },

      retrieveCardFee: function(data, callback){
        this.paymentsServer.retrieveCardFee(data, _.bind(function(err, serverData){
          if(err){
            callback(err);
          } else {
            callback(null, serverData);
          }
        }));
      },

      verifyCardRequest: function(data, callback){
        memoryStore.get("biometricKeyToken", _.bind(function(oldToken){
          if (oldToken) {
            data.old_token = oldToken;
          }

          this.paymentsServer.verifyCardRequest(data, _.bind(function(err, serverData){
            if(err){
              callback(err);
            } else {
              callback(null, serverData);
            }
          }));
        }, this));
      }
    });

    var paymentsModule = new PaymentsModule({});
    modules.paymentsModule = paymentsModule;

    return paymentsModule;

  });

ABINE_DNTME.define('abine/modules/phone',
  ["documentcloud/underscore", "abine/core", 'abine/timer', 'storage', 'persistence', 'modules', 'abine/phoneServer'],
  function(_, abineCore, abineTimer, storage, persistence, modules, phoneServer){

    var emptyCallback = function(){};

    var PhoneModule = abineCore.BaseClass.extend({
      initialize: function(options){
        this.phoneServer = options.phoneServer || new phoneServer();
      },

      getPhone: function(data, callback) {
        callback = callback || emptyCallback;
        this.phoneServer.getPhone(data, _.bind(function(err, serverData){
          callback(err, serverData);
        }, this));
      },

      logDisposableFilled: function(id, callback) {
        callback = callback || emptyCallback;
        this.phoneServer.logDisposableFilled(id, _.bind(function(err, serverData){
          callback(err, serverData);
        }, this));
      },

      restorePhone: function(data, callback) {
        callback = callback || emptyCallback;
        this.phoneServer.restorePhone(data, _.bind(function(err, serverData){
          callback(err, serverData);
        }, this));
      }
    });

    var phoneModule = new PhoneModule({});
    modules.phoneModule = phoneModule;

    return phoneModule;

  });









ABINE_DNTME.define('modules', [], function(){
  return {};
});

ABINE_DNTME.require('abine/modules/license');
ABINE_DNTME.require('abine/modules/payments');
ABINE_DNTME.require('abine/modules/panels');
ABINE_DNTME.require('abine/modules/phone');
ABINE_DNTME.require('abine/modules/mappings');
ABINE_DNTME.require('abine/modules/criticalProcess');

ABINE_DNTME.require('abine/storage/blacklist');
ABINE_DNTME.require('abine/storage/disposableCard');
ABINE_DNTME.require('abine/storage/disposableEmail');
ABINE_DNTME.require('abine/storage/disposablePhone');
ABINE_DNTME.require('abine/storage/loginUrl');
ABINE_DNTME.require('abine/storage/extensionVersion');
ABINE_DNTME.require('abine/storage/autofillFeedback');
ABINE_DNTME.require('abine/storage/mappedForm');
ABINE_DNTME.require('abine/storage/mappedField');
ABINE_DNTME.require('abine/storage/panelActivity');
ABINE_DNTME.require('abine/storage/preference');
ABINE_DNTME.require('abine/storage/shieldedCard');
ABINE_DNTME.require('abine/storage/shieldedEmail');
ABINE_DNTME.require('abine/storage/shieldedPhone');
ABINE_DNTME.require('abine/storage/fieldData');
ABINE_DNTME.require('abine/storage/trackerData');
ABINE_DNTME.require('abine/storage/pageData');
ABINE_DNTME.require('abine/storage/tabData');
ABINE_DNTME.require('abine/storage/address');
ABINE_DNTME.require('abine/storage/identity');
ABINE_DNTME.require('abine/storage/accountHistory');
ABINE_DNTME.require('abine/storage/account');
ABINE_DNTME.require('abine/storage/companyAccount');
ABINE_DNTME.require('abine/storage/realCard');
ABINE_DNTME.require('abine/storage/globalData');
ABINE_DNTME.require('abine/storage/dntStats');
ABINE_DNTME.require('abine/storage/note');
ABINE_DNTME.require('abine/storage/trackerInfo');
ABINE_DNTME.require('abine/storage/advt');










ABINE_DNTME.context = 'BG';

ABINE_DNTME.define('aws', function() {
  ABINE_DNTME.AWS = AWS;
  return AWS;
});

// Chrome Background Script

ABINE_DNTME.require(
    ['documentcloud/underscore', 'abine/window', 'abine/backgroundMessenger', 'abine/backgroundInit',
      'abine/eventLogger', 'abine/stubs', 'abine/contextMenu', 'abine/webapp', 'abine/localStorage',
      'abine/blurstore-api'],
    function(_, abineWindow, abineBackgroundMessenger, abineBackgroundInit,
      log, abineStubs, contextMenu, webapp, localStorage) {

  ABINE_DNTME.MASKME_EXTENSION_ID = {"dpkiidbpeijnaaacjlfnijncdlkicejg": 1, "klmeacceapcofljgjhopncjdlekjjmpp": 1};

  ABINE_DNTME.userAgent = window.navigator.userAgent;

  function showToolbarPanel() {
    chrome.windows.getLastFocused({populate: true}, function(win){
      for (var i=0;i<win.tabs.length;i++) {
        if (win.tabs[i].active) {
          backgroundMessenger.trigger('toolbar:clicked', {tabId:win.tabs[i].id});
          break;
        }
      }
    });
  }
  
  function openJasmineRunner() {
    chrome.tabs.query({
      url: chrome.extension.getURL('test/spec_runner.html')
    }, function (testTabs) {
      if (testTabs.length < 1) {
        abineWindow.createTab({
          localUrl: 'test/spec_runner.html'
        });
      }
    });
  }

  // listen for maskme messages
  if (chrome.runtime && chrome.runtime.onMessageExternal) {
    chrome.runtime.onMessageExternal.addListener(
      function(request, sender, sendResponse) {
        if(sender.id in ABINE_DNTME.MASKME_EXTENSION_ID) {
          // maskme installed after dntme 3.0
          ABINE_DNTME.maskme_installed = true;
          localStorage.setItem('maskme.installed', '1');
        }
      }
    );
  }
	 
  // Create the messenger
  var backgroundMessenger = new abineBackgroundMessenger.BackgroundMessenger();
  ABINE_DNTME.backgroundMessenger = backgroundMessenger;

  abineBackgroundInit.once('storage:initialized', function(){
    // For now, just open the spec runner page when the browser action is clicked
    chrome.browserAction.onClicked.addListener(function(tab) {
      if (ABINE_DNTME.config.environment === "development") {
        //openJasmineRunner();
      }
      showToolbarPanel();
    });

    // initialize context menu
    contextMenu.contextMenuHelper.init(backgroundMessenger);

    // load content script for already opened TABs.
    chrome.tabs.query({}, function (tabs) {
      var script = {file: 'old_tab_content.js', allFrames: false, runAt: 'document_idle'};
      for (var i=0;i<tabs.length;i++) {
        try {
          if (tabs[i].url.indexOf(ABINE_DNTME.config.webAppHost) == -1 && tabs[i].url.indexOf('http') == 0) {
            chrome.tabs.executeScript(tabs[i].id, script);
          }
        } catch(e) {}
      }
    });

    // reload any open webapp tab to get auth-token
    webapp.reload();

  });

  // initialize common code across browsers
  abineBackgroundInit.initialize(backgroundMessenger);

  // Listener to inject content script
  chrome.extension.onRequest.addListener(function(request, sender) {
    if (ABINE_DNTME.maskme_installed) {
      // nothing to do when maskme is installed
      return;
    }
    if (request.eventName == "injectContentScript") {
      var files = ['almond-min.js', 'jquery-min.js', 'underscore-min.js', 'backbone-min.js', 'common.js', 'common-content.js'];
      files.push(request.payload);

      function injectScript(index) {
        if (index >= files.length) return;
        if (files[index].file)
          var script = files[index];
        else
          var script = {file: files[index], allFrames: true, runAt: 'document_idle'};

        // since chrome 39 we can target content script to specific frame id.
        if (typeof(sender.frameId) != 'undefined' && sender.frameId) {
          delete script.allFrames;
          script.frameId = sender.frameId;
        }

        chrome.tabs.executeScript(sender.tab.id, script, function() {
          injectScript(index+1);
        });
      }

      injectScript(0);

    } else if (request.eventName == "processFormsInNewFrame") {
      backgroundMessenger.send('contentManager:processNewFrame', request.frame_id, sender.tab.id);
    }
  });

  var initTime = Date.now() - ABINE_DNTME.startTime;
  LOG_DEBUG && log.debug("Chrome background initialized in " + initTime + " ms");
  
});

