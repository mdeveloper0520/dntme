/**
 * Blur Copyright (c) 2008-2015 by Abine, Inc. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Abine, Inc. ("Confidential Information"), subject
 * to the Non-Disclosure Agreement and/or License Agreement you entered
 * into with Abine. You shall use such Confidential Information only
 * in accordance with the terms of said Agreement(s). Abine makes
 * no representations or warranties about the suitability of the
 * software. The software is provided with ABSOLUTELY NO WARRANTY
 * and Abine will NOT BE LIABLE for ANY DAMAGES resulting from
 * the use of the software.
 *
 * Contact license@getabine.com with any license-related questions.
 *
 * https://www.abine.com
 * @license
 *
 */

;
;

var ABINE_DNT = ABINE_DNT || {};

ABINE_DNT.tag = 'dntme';
ABINE_DNT.version = '3.0.0.42';













ABINE_DNT.browser = 'chrome';

ABINE_DNT.require = require;
ABINE_DNT.define = define;

ABINE_DNT.define('documentcloud/underscore', function () {
  return _;
});

// Core API
// Provides core API functionality
ABINE_DNT.define("abine/dnt/core", ["documentcloud/underscore"], function (_) {

  // Backbone style events

  // A module that can be mixed in to *any object* in order to provide it with
  // custom events. You may `bind` or `unbind` a callback function to an event;
  // `trigger`-ing an event fires all callbacks in succession.
  //
  //     var object = {};
  //     _.extend(object, Backbone.Events);
  //     object.bind('expand', function(){ alert('expanded'); });
  //     object.trigger('expand');
  //
  var Events = {

    // Bind an event, specified by a string name, `ev`, to a `callback` function.
    // Passing `"all"` will bind the callback to all events fired.
    on: function (ev, callback, context) {
      var calls = this._callbacks || (this._callbacks = {});
      var list = calls[ev] || (calls[ev] = {});
      var tail = list.tail || (list.tail = list.next = {});
      tail.callback = callback;
      tail.context = context;
      list.tail = tail.next = {};
      return this;
    },

    // Remove one or many callbacks. If `callback` is null, removes all
    // callbacks for the event. If `ev` is null, removes all bound callbacks
    // for all events.
    off: function (ev, callback) {
      var calls, node, prev;
      if (!ev) {
        this._callbacks = null;
      } else if (calls = this._callbacks) {
        if (!callback) {
          calls[ev] = {};
        } else if (node = calls[ev]) {
          while ((prev = node) && (node = node.next)) {
            if (node.callback !== callback) continue;
            prev.next = node.next;
            node.context = node.callback = null;
            break;
          }
        }
      }
      return this;
    },

    // Trigger an event, firing all bound callbacks. Callbacks are passed the
    // same arguments as `trigger` is, apart from the event name.
    // Listening for `"all"` passes the true event name as the first argument.
    trigger: function (eventName) {
      var node, calls, callback, args, ev, events = ['all', eventName];
      if (!(calls = this._callbacks)) return this;
      while (ev = events.pop()) {
        if (!(node = calls[ev])) continue;
        args = ev == 'all' ? arguments : Array.prototype.slice.call(arguments, 1);
        while (node = node.next) if (callback = node.callback) callback.apply(node.context || this, args);
      }
      return this;
    },

    // Bind an event, callback is called only once and event handler is removed upon first call.
    once: function (eventName, callback, context) {
      var self = this;

      function wrapCallback() {
        self.off(eventName, wrapCallback);
        return callback.apply(this, arguments);
      }

      return this.on(eventName, wrapCallback, context);
    }

  };

  // Backbone style inheritance system

  // Shared empty constructor function to aid in prototype-chain creation.
  var ctor = function () {
  };

  // Helper function to correctly set up the prototype chain, for subclasses.
  // Similar to `goog.inherits`, but uses a hash of prototype properties and
  // class properties to be extended.
  var inherits = function (parent, protoProps, staticProps) {
    var child;

    // The constructor function for the new subclass is either defined by you
    // (the "constructor" property in your `extend` definition), or defaulted
    // by us to simply call `super()`.
    if (protoProps && protoProps.hasOwnProperty('constructor')) {
      child = protoProps.constructor;
    } else {
      child = function () {
        return parent.apply(this, arguments);
      };
    }

    // Inherit class (static) properties from parent.
    _.extend(child, parent);

    // Set the prototype chain to inherit from `parent`, without calling
    // `parent`'s constructor function.
    ctor.prototype = parent.prototype;
    child.prototype = new ctor();

    // Add prototype properties (instance properties) to the subclass,
    // if supplied.
    if (protoProps) _.extend(child.prototype, protoProps);

    // Add static properties to the constructor function, if supplied.
    if (staticProps) _.extend(child, staticProps);

    // Correctly set child's `prototype.constructor`.
    child.prototype.constructor = child;

    // Set a convenience property in case the parent's prototype is needed later.
    child.__super__ = parent.prototype;

    return child;
  };

  // The self-propagating extend function that Backbone classes use.
  var extend = function (protoProps, classProps) {
    var child = inherits(this, protoProps, classProps);
    child.extend = this.extend;
    return child;
  };

  // Base class
  var BaseClass = function BaseClass() {
    this.initialize.apply(this, arguments);
  };
  _.extend(BaseClass.prototype, Events, {
    initialize: function initialize() {
    }
  });

  // Give it inheritance
  BaseClass.extend = extend;


  // Public API
  return {
    // ### class BaseClass
    // Base class for Backbone style inheritance system. i.e.
    // var parent = new abineCore.baseClass()
    // var child = parent.extend({ ... })
    BaseClass: BaseClass
  }
});
ABINE_DNT.define('abine/dnt/config',
  ['documentcloud/underscore', 'abine/dnt/core'], function (_, core) {

    var settings = {
      defaultBlocked: true,
      useSuggestions: true,
      placeHolderButtonEnabled: false,
      trackerSettings: {},
      disabledSites: {},
      blockedSites: {}
    };

    var saveChanges = _.debounce(function () {
      config.trigger('config-changed', settings);
    }, 3000);

    function toggleTracker(name, blocked, host, hostAllowedBySuggestion) {
      function saveAndNotify() {
        config.trigger('update-content-policy-settings');
        saveChanges();
      }

      var trackerSettings = settings.trackerSettings;
      if (!trackerSettings[host])
        trackerSettings[host] = {};

      if (name == "*" && host == "*") {
        for (var tracker in trackerSettings[name]) {
          trackerSettings[name][tracker] = blocked;
        }

        for (var site in trackerSettings) {
          if (site != '*') {
            delete trackerSettings[site];
          }
        }

        settings.defaultBlocked = blocked;

        saveAndNotify();
        return;
      }

      if (name == "*") {
        trackerSettings[host] = {};

        var disabledSites = settings.disabledSites;
        var blockedSites = settings.blockedSites;

        // disable/enable site
        if (!blocked) {
          if (!hostAllowedBySuggestion) {
            disabledSites[host] = 1;
          } else if (host in disabledSites) {
            delete disabledSites[host];
          }

          trackerSettings[host]["*"] = 0;
        } else {
          if (host in disabledSites) {
            delete disabledSites[host];
          }
        }

        // handle block of a site when it was allowed by a suggestion
        if (!blocked) {
          if (host in blockedSites) {
            delete blockedSites[host];
          }
        } else {
          if (hostAllowedBySuggestion)
            blockedSites[host] = 1;
        }

        saveAndNotify();
        return;
      }

      // clear site exceptions for this new global setting
      if (host == "*") {
        for (var site in trackerSettings) {
          if (site != '*' && name in trackerSettings[site]) {
            delete trackerSettings[site][name];
          }
        }
      }

      if (blocked) {
        trackerSettings[host][name] = 1;
      } else {
        trackerSettings[host][name] = 0;
      }

      saveAndNotify();
    }

    var Config = core.BaseClass.extend({

      load: function (data) {
        for (var i in data) {
          settings[i] = data[i];
        }
      },

      saveChanges: saveChanges,

      allowTracker: function (name, host, hostAllowedBySuggestion) {
        toggleTracker(name, false, host, hostAllowedBySuggestion);
      },

      blockTracker: function (name, host, hostAllowedBySuggestion) {
        toggleTracker(name, true, host, hostAllowedBySuggestion);
      },

      getDefaultBlocking: function (block) {
        return settings.defaultBlocked;
      },

      setDefaultBlocking: function (block) {
        toggleTracker('*', block, '*')
      },

      getSuggestionsEnabled: function (val) {
        return settings.useSuggestions;
      },

      setSuggestionsEnabled: function (val) {
        settings.useSuggestions = val;
        saveChanges();
        this.trigger('update-content-policy-suggestions')
      },

      getDisableSites: function () {
        return settings.disabledSites;
      },

      getBlockedSites: function () {
        return settings.blockedSites;
      },

      getTrackerSettings: function () {
        return settings.trackerSettings;
      },

      setPlaceHolderButtonEnabled: function(value) {
        settings.placeHolderButtonEnabled = !!value;
        saveChanges();
      },

      isPlaceHolderButtonEnabled: function() {
        return settings.placeHolderButtonEnabled;
      },

      getTrackerDefaults: function () {
        var trackerSettings = settings.trackerSettings;
        if (!trackerSettings['*']) {
          trackerSettings['*'] = {};
        }

        var trackerDefaults = {};
        for (var trackerName in trackerSettings['*']) {
          trackerDefaults[trackerName] = trackerSettings['*'][trackerName];
        }
        return trackerDefaults;
      },

      setTrackerDefaults: function (trackerNames) {
        var trackerSettings = settings.trackerSettings;
        if (!trackerSettings['*']) {
          trackerSettings['*'] = {};
        }

        var changed;

        for (var trackerName in trackerNames) {
          if (trackerName in trackerSettings['*']) continue;

          trackerSettings['*'][trackerName] = settings.defaultBlocked;
          changed = true;
        }

        if (changed) {
          saveChanges();
        }

        return changed;
      }

    });

    var config = new Config();

    return  config;
  });
ABINE_DNT.define('abine/dnt/utils', [], function () {

  // EXTEND method from jquery
  var _class2type = {};

  var _type = function (obj) {
    return obj == null ?
      String(obj) :
      _class2type[ toString.call(obj) ] || "object";
  };

  var _isWindow = function (obj) {
    return obj != null && obj == obj.window;
  };

  var _isFunction = function (target) {
    return toString.call(target) === "[object Function]";
  };

  var _isArray = Array.isArray || function (obj) {
    return _type(obj) === "array";
  };

  var _isPlainObject = function (obj) {
    // Must be an Object.
    // Because of IE, we also have to check the presence of the constructor property.
    // Make sure that DOM nodes and window objects don't pass through, as well
    if (!obj || _type(obj) !== "object" || obj.nodeType || _isWindow(obj)) {
      return false;
    }

    var hasOwn = Object.prototype.hasOwnProperty;

    try {
      // Not own constructor property must be Object
      if (obj.constructor && !hasOwn.call(obj, "constructor") && !hasOwn.call(obj.constructor.prototype, "isPrototypeOf")) {
        return false;
      }
    } catch (e) {
      // IE8,9 Will throw exceptions on certain host objects #9897
      return false;
    }

    // Own properties are enumerated firstly, so to speed up,
    // if last one is own, then all properties are own.

    var key;
    for (key in obj) {
    }

    return key === undefined || hasOwn.call(obj, key);
  };

  var _extend = function () {
    var options, name, src, copy, copyIsArray, clone,
      target = arguments[0] || {},
      i = 1,
      length = arguments.length,
      deep = false;

    // Handle a deep copy situation
    if (typeof target === "boolean") {
      deep = target;
      target = arguments[1] || {};
      // skip the boolean and the target
      i = 2;
    }

    // Handle case when target is a string or something (possible in deep copy)
    if (typeof target !== "object" && !_isFunction(target)) {
      target = {};
    }

    if (length === i) {
      target = this;
      --i;
    }

    for (; i < length; i++) {
      // Only deal with non-null/undefined values
      if ((options = arguments[ i ]) != null) {
        // Extend the base object
        for (name in options) {
          src = target[ name ];
          copy = options[ name ];

          // Prevent never-ending loop
          if (target === copy) {
            continue;
          }

          // Recurse if we're merging plain objects or arrays
          if (deep && copy && ( _isPlainObject(copy) || (copyIsArray = _isArray(copy)) )) {
            if (copyIsArray) {
              copyIsArray = false;
              clone = src && _isArray(src) ? src : [];

            } else {
              clone = src && _isPlainObject(src) ? src : {};
            }

            // Never move original objects, clone them
            target[ name ] = _extend(deep, clone, copy);

            // Don't bring in undefined values
          } else if (copy !== undefined) {
            target[ name ] = copy;
          }
        }
      }
    }
    // Return the modified object
    return target;
  };
  // EXTEND method ends

  var XMLHttpFactories = [
    function () {
      return framework.extension.getRequest()
    },
    function () {
      return new XMLHttpRequest()
    }
  ];

  function createXMLHTTPObject() {
    var xmlhttp = false;
    for (var i = 0; i < XMLHttpFactories.length; i++) {
      try {
        xmlhttp = XMLHttpFactories[i]();
        // iterate only once
        XMLHttpFactories = [XMLHttpFactories[i]];
      } catch (e) {
        continue;
      }
      break;
    }
    return xmlhttp;
  }


  function sendRequest(url, callback, postData, async) {
    var req = createXMLHTTPObject();
    if (!req) return;
    async = typeof(async) == 'undefined' ? true : async;
    var method = (postData) ? "POST" : "GET";
    req.open(method, url, async);

    req.setRequestHeader("Cache-Control", "no-cache");
    req.setRequestHeader("Pragma", "no-cache");
    req.setRequestHeader("If-Modified-Since", "Sat, 1 Jan 2000 00:00:00 GMT");
    if (postData) {
      req.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
      req.setRequestHeader('Content-length', postData.length);
    }

    var sendResponse = function () {
      if (req.readyState != 4) return;
      if (req.status != 200 && req.status != 304 && req.status != 0) {
        callback(null, req);
        return;
      }
      callback(req.responseText, req);
    };

    if (async) req.onreadystatechange = sendResponse;

    req.setRequestHeader('X-DNT-Version', ABINE_DNT.version + " " + ABINE_DNT.browser + " " + ABINE_DNT.tag);

    req.send(postData);

    if (!async) sendResponse();
  }

  return {
    extend: _extend,
    sendRequest: sendRequest
  };
});
ABINE_DNT.define('abine/dnt/rules', ['abine/dnt/utils'], function (utils) {
  var Rules;

  utils.sendRequest('/api-rules.json', function (json_text) {
    Rules = JSON.parse(json_text);
  }, null, false);

  return Rules;
});
ABINE_DNT.define('abine/dnt/tabData', ['documentcloud/underscore', 'abine/dnt/core', 'abine/dnt/config'], function (_, core, config) {

  // map of tabid to tab-related-data
  var tabStorage = {};

  function triggerCountChanged(obj, tabData) {
    var num_suggested = 0;

    for (var name in tabData.trackers) {
      if (name == 'length' || tabData.trackers[name] != 2) continue;
      if (!(name in tabData.blocked)) num_suggested++;
    }
    setTimeout(function(){
      obj.trigger('update-count', tabData.tabId, tabData.disabledHere, tabData.trackers.length, tabData.blocked.length, num_suggested, tabData.host);
    }, 0);
  }

  function markUnmarkChanges(tabData, setting, oldValue) {
    if (setting in tabData.changes) {
      oldValue = tabData.changes[setting];
      delete tabData.changes[setting];
      tabData.changes.length--;
      if (tabData.changes.length == 0) {
        tabData.settingsChanged = false;
      }
    } else {
      tabData.changes[setting] = oldValue;
      tabData.changes.length++;
      tabData.settingsChanged = true;
    }
    return oldValue;
  }

  var TabData = core.BaseClass.extend({
    get: function (tabId, doNotCreate) {
      if (!tabId) return {};

      if (!(tabId in tabStorage)) {

        if (doNotCreate) return false;

        tabStorage[tabId] = {
          tabId: tabId,

          // all domains found on a page
          domains: {length: 0},

          trackers: {length: 0},
          blocked: {length: 0},

          disabledHere: false,

          // this is true if content-policy didnt see first request on a tab (happens after fresh install with some tabs open).
          reloadRequired: true,

          host: null,
          url: null,


          // keep track of changed attributes
          changes: {length: 0},
          settingsChanged: false,

          // custom tab data
          custom: {},

          // these are required to save blocked requests till content-script is ready to receive events
          contentScriptNotReady: true,
          blockedRequests: []
        };
      }
      return tabStorage[tabId];
    },

    update: function (ruleName, reqHost, blocked, allowedByTopDomain, tabId, url, type) {
      // get current tab storage
      var tabData = this.get(tabId);

      var newTracker = false;

      // save request domain
      if (reqHost && !(reqHost in tabData.domains)) {
        tabData.domains[reqHost] = 1;
        tabData.domains.length++;
      }

      if (!ruleName) return false;

      // current list of trackers found on this page
      var trackerData = tabData.trackers;

      // add it to list if it's not already there
      if (!(ruleName in trackerData)) {
        trackerData[ruleName] = (allowedByTopDomain ? 2 : 1);
        trackerData.length++;
        newTracker = true;
      }

      // current list of blocked trackers on this page
      // add it to the list if it's not already there
      if (blocked) {
        var blockedData = tabData.blocked;
        if (!(ruleName in blockedData)) {
          blockedData[ruleName] = 1;
          blockedData.length++;
        }

        if (tabData.contentScriptNotReady) {
          tabData.blockedRequests.push({name: ruleName, url: url, type: type})
        }
      }

      if (newTracker) {
        triggerCountChanged(this, tabData);
      }

      return newTracker;
    },

    markUnmarkChanges: function(tabId, setting) {
      var tabData = this.get(tabId, false);
      if (!tabData) return;

      markUnmarkChanges(tabData, setting);
    },

    setCustomData: function(tabId, name, value) {
      var tabData = this.get(tabId, false);
      if (!tabData) return;

      if (value === null) {
        if (name in tabData.custom) {
          delete tabData.custom[name];
        }
      } else {
        tabData.custom[name] = value;
      }
    },

    getCustomData: function(tabId, name) {
      var tabData = this.get(tabId, false);
      if (!tabData) return;

      if (name in tabData.custom) {
        return tabData.custom[name];
      }
      return null;
    },

    // update individual tab data when a tracker is toggled.
    updateTrackerState: function (name, host, blocked) {

      for (var tabId in tabStorage) {
        var tabData = tabStorage[tabId];
        if (host != '*') {
          if (tabData.host != host)
            continue;
        }
        var changed = false;
        if (host == '*' && name == '*') {
          markUnmarkChanges(tabData, 'global');
        } else if (name == '*') {
          changed = true;
          tabData.disabledHere = !blocked;
          var oldValue = markUnmarkChanges(tabData, 'site', _.clone(tabData.blocked));
          if (!('site' in tabData.changes)) {
            // reverted changes
            tabData.blocked = oldValue;
          } else {
            if (tabData.disabledHere) {
              tabData.toggledOff = true;
              for (var tracker in tabData.blocked) {
                if (tracker == 'length') continue;
                tabData.blocked.length--;
                delete tabData.blocked[tracker];
              }
            } else {
              tabData.toggledOff = false;
              for (var tracker in tabData.trackers) {
                if (tracker in tabData.blocked) continue;
                tabData.blocked.length++;
                tabData.blocked[tracker] = 1;
              }
            }
          }
        } else if (name in tabData.trackers) {
          if (blocked) {
            if (name in tabData.blocked)
              continue;
            tabData.blocked.length++;
            tabData.blocked[name] = 1;
            markUnmarkChanges(tabData, name);
            changed = true;
          } else {
            if (!(name in tabData.blocked))
              continue;
            tabData.blocked.length--;
            delete tabData.blocked[name];
            markUnmarkChanges(tabData, name);
            changed = true;
          }
        }

        if (changed) {
          triggerCountChanged(this, tabData);
        }
      }
    },

    clear: function (tabId) {
      if (!tabId) return;

      if (tabId in tabStorage) {
        delete tabStorage[tabId];
      }
    },

    triggerUpdateIcon: function(tabId) {
      if (tabId in tabStorage) {
        var tabData = this.get(tabId);
        triggerCountChanged(this, tabData);
      }
    },

    getTabIds: function(host) {
      var ids = [];
      for (var tabId in tabStorage) {
        var tabData = tabStorage[tabId];
        if (host != '*') {
          if (tabData.host != host)
            continue;
        }
        ids.push(tabId);
      }
      return ids;
    },

    start: function (tabId, host, url, suggestedAllow) {
      this.clear(tabId);

      var tabData = this.get(tabId);

      tabData.host = host;
      tabData.url = url;
      tabData.disabledHere = tabData.host in config.getDisableSites() ||
          (suggestedAllow && !(tabData.host in config.getBlockedSites()));

      tabData.suggestedAllow = suggestedAllow;

      delete tabData.reloadRequired;

      triggerCountChanged(this, tabData);
      return tabData;
    }
  });

  return new TabData();

});
ABINE_DNT.define('abine/dnt/baseContentPolicy',
  ['abine/dnt/core', 'abine/dnt/config', 'abine/dnt/rules', 'abine/dnt/utils'],
  function (core, config, DefaultRules, utils) {

    var jsonRules, trackerNames, suggestedAllows = {};

    var Rules = DefaultRules;

    var abNoOp = "var abNoOp=function(i,a){return (a&&a.length?a[0]:null)||true;},abSC=function(n,v){document.cookie=n+'='+v+';path=/;expires='+(new Date((new Date()).getTime()+(30000*60*60*24))).toGMTString();},abGC=function(n){return document.cookie.indexOf(n+'=')!=-1;},abAddNoOpMethods=function(o,m){m=m.split(',');for(var i=0;i<m.length;i++){if(m[i].match(/^\#/)){o['get'+m[i].substr(1)]=o['set'+m[i].substr(1)]=abNoOp;}else{o[m[i]]=abNoOp;}}};";
    abNoOp += "var abClickEvent=function(n){if(n.click){n.click();}else{var e=n.ownerDocument.createEvent('MouseEvents');e.initMouseEvent('click',true, true, n.ownerDocument.defaultView, 1,0,0,0,0,false,false,false,false,0,null);n.dispatchEvent(e);}};";

    // types of rules
    var ruleTypes = {script: 1, iframe: 1, image: 1};

    function setupTrackerNames() {
      var companies = Rules.Companies;
      trackerNames = {};
      for (var c = 0; c < companies.length; c++) {
        var company = companies[c];
        var rules = company.rules;
        for (var i = 0; i < rules.length; i++) {
          for (var type in ruleTypes) {
            if (typeof (rules[i][type]) == 'undefined')
              continue;
            trackerNames[rules[i].name] = rules[i].name;
          }
        }
      }
      return config.setTrackerDefaults(trackerNames);
    }

    function convertStringToRegexInRules() {
      var types = {script: 1, iframe: 1, image: 1};
      var companies = Rules.Companies;
      for (var i = 0; i < companies.length; i++) {
        var company = companies[i];
        for (var j = 0; j < company.rules.length; j++) {
          for (var type in types) {
            var rules = company.rules[j];
            if (type in rules) {
              if (!rules[type].pattern.source) {
                rules[type].pattern = new RegExp(rules[type].pattern, "i");
              }
              if (rules[type].allowDomain) {
                if (!rules[type].allowDomain.source) {
                  rules[type].allowDomain = new RegExp(rules[type].allowDomain, "i");
                }
              }
            }
          }
        }
      }
      if (Rules.HostingSites && !Rules.HostingSites.source) {
        Rules.HostingSites = new RegExp(Rules.HostingSites, "i");
      }
      if (Rules.CompoundRealms && !Rules.CompoundRealms.source) {
        Rules.CompoundRealms = new RegExp(Rules.CompoundRealms, "i");
      }
      if (Rules.CombinerScripts) {
        if (!Rules.CombinerScripts.source) {
          Rules.CombinerScripts = new RegExp(Rules.CombinerScripts, "i");
        }
      } else {
        Rules.CombinerScripts = /(\.js[^\/](.*\.js)+)/i;
      }

      suggestedAllows = {};
      Rules.AllowedSites = Rules.AllowedSites || [];
      for (var i=0;i<Rules.AllowedSites.length;i++) {
        suggestedAllows[Rules.AllowedSites[i]] = 1;
      }
    }

    function updateRules(newRules, force) {
      if (newRules && newRules.Version && (force || newRules.Version > Rules.Version)) {

        jsonRules = null;

        BaseContentPolicy.Rules = Rules = newRules;

        convertStringToRegexInRules();

        return true;
      }

      return false;
    }

    function getUserTopLevelDomain(hostname) {

      if (!hostname) return null;

      var parts = hostname.split(".");

      if (parts.length < 2 || hostname.match(/^[0-9\.\:]+$/)) {
        return hostname;
      }

      var realm = parts.pop();
      var baseDomain = parts.pop();
      var domain = baseDomain + "." + realm;

      if (Rules && Rules.CompoundRealms.test(domain) && parts.length > 0) {
        realm = domain;
        baseDomain = parts.pop();
        domain = baseDomain + "." + realm;
      }
      if (Rules && Rules.HostingSites.test(domain) && parts.length > 0) {
        var subDomain = parts.pop();
        domain = subDomain + "." + domain;
      }
      return domain;
    }

    function getHostname(str) {
      var re = new RegExp('^http(?:s)?\://([^/]+)', 'im');
      var parts = str.match(re);
      return parts && parts.length > 1 ? parts[1].toString() : str;
    }

    var BaseContentPolicy = core.BaseClass.extend({
      initialize: function (rules) {
        this.setupContentPolicy();

        this.setRules(rules);

        this.loadRules();
        setupTrackerNames();

        this.updateSettings();
      },

      getDomain: function (url) {
        return getUserTopLevelDomain(getHostname(url));
      },

      getHostname: function (url) {
        return getHostname(url);
      },

      getRulesVersion: function() {
        return Rules.Version;
      },

      setRules: function (newRules, force) {
        if (newRules) {
          var rulesChanged = updateRules(newRules, force);
          if (rulesChanged) {
            this.reloadRules();
          } else {
            convertStringToRegexInRules();
          }
          return rulesChanged;
        } else {
          convertStringToRegexInRules();
        }
        return false;
      },

      reloadRules: function () {
        this.loadRules();
        if (setupTrackerNames()) {
          // update content-policy if there is a change in defaults.
          this.updateSettings();
        }
      },

      getTrackerNames: function () {
        return trackerNames;
      },

      isHostAllowedBySuggestion: function(host) {
        return host in suggestedAllows;
      },

      // no-op defaults
      setupContentPolicy: function () {},
      loadRules: function () {},
      updateSettings: function () {},
      updateSuggestions: function() {},

      getRulesJson: function () {
        if (!jsonRules) {
          var rulesCopy = utils.extend(true, {}, Rules);
          var companies = rulesCopy.Companies;
          for (var i = 0; i < companies.length; i++) {
            var company = companies[i];
            for (var j = 0; j < company.rules.length; j++) {
              for (var type in ruleTypes) {
                var rules = company.rules[j];
                if (type in rules) {
                  if (rules[type].pattern && rules[type].pattern.source)
                    rules[type].pattern = rules[type].pattern.source;
                  if (rules[type].allowDomain && rules[type].allowDomain.source)
                    rules[type].allowDomain = rules[type].allowDomain.source;
                }
              }
            }
          }
          if (rulesCopy.HostingSites && rulesCopy.HostingSites.source) {
            rulesCopy.HostingSites = rulesCopy.HostingSites.source;
          }
          if (rulesCopy.CompoundRealms && rulesCopy.CompoundRealms.source) {
            rulesCopy.CompoundRealms = rulesCopy.CompoundRealms.source;
          }
          if (rulesCopy.CombinerScripts) {
            if (rulesCopy.CombinerScripts.source)
              rulesCopy.CombinerScripts = rulesCopy.CombinerScripts.source;
          } else {
            rulesCopy.CombinerScripts = (/(\.js[^\/](.*\.js)+)/i).source;
          }

          delete rulesCopy.Notifications;
          jsonRules = JSON.stringify(rulesCopy);
        }
        return jsonRules;
      }
    });

    BaseContentPolicy.Rules = Rules;
    BaseContentPolicy.abNoOp = abNoOp;

    return BaseContentPolicy;

  });
ABINE_DNT.define('abine/dnt/browserContentPolicy',
  ['abine/dnt/config', 'abine/dnt/baseContentPolicy', 'abine/dnt/tabData'],
  function (config, BaseContentPolicy, TabData) {

    var rulesDomainMap = {script: {'*': []}, image: {'*': []}, iframe: {'*': []}, object: {'*': []}, subobject: {'*': []}};
    var badTrackers = {};
    var suggestedAllowSites = {};

    function trackerIsBlocked(host, ruleName, allowByTopDomain) {
      var isBlocked = false;

      var trackerSettings = config.getTrackerSettings();

      // blocked everywhere for this tracker
      if (trackerSettings["*"] && trackerSettings["*"][ruleName] == 1 && !allowByTopDomain) {
        isBlocked = true;
      }

      // check for site specific rule
      if (trackerSettings[host] && trackerSettings[host][ruleName] == 1) {
        isBlocked = true;
      }

      if (trackerSettings[host] && trackerSettings[host][ruleName] == 0) {
        isBlocked = false;
      }

      // check for site wide all tracker rule
      if (trackerSettings[host] && trackerSettings[host]['*'] == 0) {
        isBlocked = false;
      }

      return isBlocked;
    }


    function getMatchingRule(url, host, contentType) {
      // find rules to search for using host.
      // remove subdomains one by one to get best possible match.
      // when none found, use "*" rules.

      var dMap = rulesDomainMap[contentType];
      while (true) {
        if (host in dMap) {
          var rules = dMap[host];
          for (var i = 0; i < rules.length; i++) {
            var rule = rules[i];
            if (!rule.pattern.source) {
              rule.pattern = new RegExp(rule.pattern, "i");
            }
            if (url.match(rule.pattern)) {
              return rule;
            }
          }
          if (host == "*") return null;
        }
        var idx = host.indexOf(".");
        host = idx == -1 ? "*" : host.substr(idx + 1);
      }
    }

    function shouldLoad(request) {

      // default accept request;
      var ret = {cancel: false};

      if (request.tabId == -1)
        return ret;

      var url = request.url;

      if (request.type == "main_frame") {
        var host = this.getDomain(request.url);

        var suggestedAllow = config.getSuggestionsEnabled() && host in suggestedAllowSites;

        // clear tab data and allow this request
        TabData.start(request.tabId, host, request.url, suggestedAllow);

        return ret;
      } else if (!TabData.get(request.tabId, true)) {

        // first request lost during restart (happens in opera).  so let this request pass through and initialize tab-data.
        chrome.tabs.get(request.tabId, _.bind(function(tab){
          var host = this.getDomain(tab.url);
          var suggestedAllow = config.getSuggestionsEnabled() && host in suggestedAllowSites;
          TabData.start(tab.id, host, tab.url, suggestedAllow);
        }, this));

        return ret;
      }

      var tabData = TabData.get(request.tabId);

      // allow everything when dnt is OFF on a site
      if (tabData.disabledHere)
        return ret;

      // host of tab from our map
      var host = tabData.host;
      if (!host) {
        // this can happen on an ajax request after we've loaded the add-on but haven't reloaded the page.
        return ret;
      }

      var req_host = this.getDomain(url);

      if (!req_host)
        return ret;

      var rule = null;

      var typeMap = {"sub_frame": "iframe", "script": "script", "xmlhttprequest": "object", "image": "image", "object": "object", "other": "object"};

      if (request.type != 'script' || !url.match(BaseContentPolicy.Rules.CombinerScripts))
        rule = getMatchingRule(url, this.getHostname(url), typeMap[request.type]);

      var third_party = req_host != host;

      if (rule) {

        if (rule.allowFirstParty && !third_party) {
          return ret;
        }

        var ruleName = rule.name;
        var isBlocked, allowedByTopDomain = false;

        if (config.getSuggestionsEnabled() && rule.allowDomain) {
          if (!rule.allowDomain.source)
            rule.allowDomain = new RegExp(rule.allowDomain, "i");
          if (host.match(rule.allowDomain)) {
            allowedByTopDomain = true;
          }
        }

        isBlocked = trackerIsBlocked(host, ruleName, allowedByTopDomain);

        // request needs to be blocked
        if (isBlocked) {

          // redirect or cancel request
          if (request.type == 'image') {
            ret = {redirectUrl: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAACklEQVR4nGMAAQAABQABDQottAAAAABJRU5ErkJggg=="};
          } else if (request.type == 'sub_frame') {
            ret = {redirectUrl: "about:blank"};
          } else if (request.type == 'script' && rule.stub) {
            ret = {redirectUrl: 'data:application/javascript;base64,' + getBase64EncodedStub(rule.stub, url)};
          } else {
            ret = {cancel: true};
          }

        }

        ret.tracker = ruleName;

        var self = this;
        setTimeout(function(){
          var newTracker = TabData.update(ruleName, req_host, isBlocked, allowedByTopDomain, request.tabId, url, request.type);
          self.trigger('tracker-found', request.tabId, rule.name, isBlocked, newTracker, allowedByTopDomain, url, rule.name in badTrackers);
        }, 0)
      }

      ret.third_party = third_party;

      return ret;
    }

    function getBase64EncodedStub(stub, url) {
      // insert stub script to avoid javascript errors because of absence of tracker.
      var params = "{}";
      if (url.indexOf("?") != -1) {
        params = {};
        url.replace(
          /([^?=&]+)(=([^&]*))?/g,
          function ($0, $1, $2, $3) {
            params[$1] = unescape($3);
          }
        );
        params = JSON.stringify(params);
      }

      stub = "var abPrm=" + params + ";" + BaseContentPolicy.abNoOp + stub;

      return window.btoa(stub);
    }


    var metrics = {};
    function captureMetrics(time, response, request) {
      if (request.tabId != -1) {
        var metric = metrics[request.tabId];
        if (request.type == 'main_frame') {
          if (metric) {
            var msg = metric.time.toFixed(4)+" ms total time spent for processing "+metric.requests+" requests.  ";
            delete metric.requests;
            delete metric.time;
            msg += JSON.stringify(metric);
            console.log(msg);
          }
          metrics[request.tabId] = {url: request.url, time: time, requests: 1, blocked: 0, allowed: 1, third_party: 0, third_party_blocked: 0};
        } else if (metric) {
          metric.time += time;
          metric.requests++;
          if (response.third_party) {
            metric.third_party++;
          }
        }
      } else {
        return;
      }
      var logger = console.debug;
      if (time > 10) {
        logger = console.info;
      } else if (time > 100) {
        logger = console.error;
      }
      if (response.cancel || response.redirectUrl) {
        type = 'blocked';
        if (metric) {
          metric.blocked++;
          if (response.third_party) {
            metric.third_party_blocked++;
          }
        }
      } else {
        type = 'allowed';
        if (metric) {
          metric.allowed++;
        }
      }
      if (response.tracker) {
        type += ' ' + response.tracker;
      }
      logger(time.toFixed(4) + " ms time to process request (" + type + ") " + request.type + " " + request.url + " " + request.tabId);
    }

    var ChromeContentPolicy = BaseContentPolicy.extend({

      setupContentPolicy: function () {
        var self = this;
        chrome.webRequest.onBeforeRequest.addListener(
          function () {
            var response;
            if (ABINE_DNTME._profileTrackers) {
              var request = arguments[0];
              var start = performance.now();
              response = shouldLoad.apply(self, arguments);
              var time = performance.now() - start;
              captureMetrics(time, response, request);
            } else {
              response = shouldLoad.apply(self, arguments);
            }
            delete response.tracker;
            delete response.third_party;
            return response;
          },
          {
            urls: ["http://*/*", "https://*/*"],
            types: ["main_frame", "sub_frame", "script", "xmlhttprequest", "image", "object", "other"]
          },
          ["blocking"]
        );
      },

      loadRules: function () {

        var Rules = BaseContentPolicy.Rules;
        var companies = Rules.Companies;

        // reset before setting up, in case this gets called from rules update
        rulesDomainMap = {script: {'*': []}, image: {'*': []}, iframe: {'*': []}, object: {'*': []}, subobject: {'*': []}};

        for (var c = 0; c < companies.length; c++) {
          var company = companies[c];
          var rules = company.rules;
          for (var i = 0; i < rules.length; i++) {
            for (var type in rulesDomainMap) {
              if (typeof (rules[i][type]) == 'undefined')
                continue;
              var rule = (typeof (rules[i][type]) == 'string') ? rules[i][rules[i][type]]
                : rules[i][type];

              rule['name'] = rules[i].name;
              rule['category'] = rules[i].category;
              rule['allowFirstParty'] = rules[i].allowFirstParty;

              var domains = rule.domains.split("|");
              for (var j = 0; j < domains.length; j++) {
                if (!(domains[j] in rulesDomainMap[type]))
                  rulesDomainMap[type][domains[j]] = [];
                rulesDomainMap[type][domains[j]].push(rule);
              }
            }
          }
        }

        badTrackers = {};
        if (Rules.BadTrackers) {
          for (var i=0;i<Rules.BadTrackers.length;i++) {
            badTrackers[Rules.BadTrackers[i]] = true;
          }
        }

        suggestedAllowSites = {};
        if (Rules.AllowedSites) {
          for (var i=0; i<Rules.AllowedSites.length; i++) {
            suggestedAllowSites[Rules.AllowedSites[i]] = 1;
          }
        }
      }

    });


    return ChromeContentPolicy;

  });
ABINE_DNT.define('abine/dnt/social-button-proxy', ['documentcloud/underscore', 'abine/dnt/core'], function (_, core) {
  var SocialButtonProxy = core.BaseClass.extend({
    blocked: function(tabId, trackerName, blocked, newTracker, url, image) {
      chrome.tabs.sendRequest(parseInt(tabId), {
        message: 'tracker-blocked',
        trackerName: trackerName,
        newTracker: newTracker,
        url: url,
        image: image
      });
    },

    allow: function(tabId, trackerName) {
      chrome.tabs.sendRequest(parseInt(tabId), {
        message: 'tracker-allowed',
        trackerName: trackerName
      });
    }
  });

  var socialButtonProxy = new SocialButtonProxy();

  chrome.extension.onRequest.addListener(function(request, sender, sendResponse) {
    if (request.message == 'allow-tracker') {
      socialButtonProxy.trigger(request.message, {tabId: sender.tab.id, trackerName: request.trackerName});
    }
  });

  return  socialButtonProxy;
});
ABINE_DNT.define('abine/dnt/api',
  ['abine/dnt/core', 'abine/dnt/browserContentPolicy', 'abine/dnt/config',
    'abine/dnt/tabData', 'abine/dnt/utils', 'abine/dnt/social-button-proxy',
    "documentcloud/underscore"],
  function (core, BrowserContentPolicy, config, TabData, utils, socialButtonProxy, _) {

    var contentPolicy;

    var socialPlaceHolderButtons = {
      'Facebook Connect': null,
      'Google +1': null,
      'Twitter Badge': null,
      'LinkedIn': null
    };

    var API = core.BaseClass.extend({
      initialize: function (settings, rules) {
        config.load(settings);

        contentPolicy = new BrowserContentPolicy(rules);

        config.on('update-content-policy-settings', function(){
          contentPolicy.updateSettings();
        });

        config.on('update-content-policy-suggestions', function(){
          contentPolicy.updateSuggestions();
        });

        // forward events
        config.on('config-changed', function () {
          var args = Array.prototype.slice.call(arguments);
          args.unshift('config-changed');
          this.trigger.apply(this, args);
        }, this);

        TabData.on('update-count', function () {
          var args = Array.prototype.slice.call(arguments);
          args.unshift('update-count');
          this.trigger.apply(this, args);
        }, this);

        contentPolicy.on('tracker-found', function (tabId, trackerName, blocked, newTracker, allowedByTopDomain, url, badTracker, contextId) {
          var args = Array.prototype.slice.call(arguments);
          args.unshift('tracker-found');
          this.trigger.apply(this, args);
          if (config.isPlaceHolderButtonEnabled() && blocked && trackerName in socialPlaceHolderButtons) {
            socialButtonProxy.blocked(tabId, trackerName, blocked, newTracker, url, socialPlaceHolderButtons[trackerName], contextId);
          }
        }, this);

        socialButtonProxy.on('allow-tracker', _.bind(function(message){
          var tabId = message.tabId;
          var trackerName = message.trackerName;
          var tabData = TabData.get(tabId, true);
          if (!tabData) return null;
          this.allow(trackerName, tabData.host);
        }, this));
      },

      updateRules: function (url, callback) {
        utils.sendRequest(url,
          function (json_text) {
            if (json_text && json_text.indexOf('"Companies":') != -1) {
              try {
                var rules = JSON.parse(json_text);
                // save tracker data only when its newer than current version
                if (contentPolicy.setRules(rules)) {
                  callback('updated', json_text);
                } else {
                  callback('no-change', json_text);
                }
                return;
              } catch (e) {
              }
            }
            callback('failed', json_text);
          }
        );
      },

      setupRules: function (json_text, callback) {
        if (json_text && json_text.indexOf('"Companies":') != -1) {
          try {
            var rules = JSON.parse(json_text);
            contentPolicy.setRules(rules, true);
            return;
          } catch (e) {
          }
        }
        callback('failed', json_text);
      },

      getTrackerDefaults: function () {
        return config.getTrackerDefaults();
      },

      allow: function (name, host) {
        if (!name || !host) {
          throw "Tracker name and host are required";
        }
        if (name == '*' && host == '*') {
          throw "Tracker name and host cannot be '*'";
        }
        config.allowTracker(name, host, name=='*'?contentPolicy.isHostAllowedBySuggestion(host):false);
        TabData.updateTrackerState(name, host, false);
        if (config.isPlaceHolderButtonEnabled() && name in socialPlaceHolderButtons) {
          var tabIds = TabData.getTabIds(host);
          _.each(tabIds, function(tabId){
            socialButtonProxy.allow(tabId, name);
          });
        }
      },

      block: function (name, host) {
        if (!name || !host) {
          throw "Tracker name and host are required";
        }
        if (name == '*' && host == '*') {
          throw "Tracker name and host cannot be '*'";
        }
        config.blockTracker(name, host, name=='*'?contentPolicy.isHostAllowedBySuggestion(host):false);
        TabData.updateTrackerState(name, host, true);
      },

      getDefaultBlocking: function () {
        return config.getDefaultBlocking();
      },

      setDefaultBlocking: function (block) {
        config.setDefaultBlocking(block);
        TabData.updateTrackerState('*', '*', true);
      },

      getSuggestionsEnabled: function () {
        return config.getSuggestionsEnabled();
      },

      setSuggestionsEnabled: function (block) {
        config.setSuggestionsEnabled(block);
        contentPolicy.updateSuggestions();
      },

      addListener: function () {
        this.on.apply(this, arguments);
      },

      removeListener: function () {
        this.off.apply(this, arguments);
      },

      enablePlaceHolderButton: function() {
        config.setPlaceHolderButtonEnabled(true);
      },

      disablePlaceHolderButton: function() {
        config.setPlaceHolderButtonEnabled(false);
      },

      setPlaceHolderButton: function(tracker, image, width, height, subType) {
        if (image === null) {
          delete socialPlaceHolderButtons[tracker];
        } else {
          subType = subType || '*'
          if (!socialPlaceHolderButtons[tracker]) {
            socialPlaceHolderButtons[tracker] = {};
          }
          socialPlaceHolderButtons[tracker][subType] = {
            image: image,
            width: width,
            height: height
          };
        }
      },

      getTabData: function (tabId) {
        var tabData = TabData.get(tabId, true);
        if (!tabData) return null;
        var data = {
          host: tabData.host,
          url: tabData.url,
          disabled: tabData.disabledHere,
          suggestedAllow: tabData.suggestedAllow,
          settingsChanged: tabData.settingsChanged,
          reloadRequired: tabData.reloadRequired,
          toggledOff: !!tabData.toggledOff,
          custom: []
        };
        // copy custom data
        for (var name in tabData.custom) {
          data.custom.push({name: name, value: tabData.custom[name]});
        }
        // copy trackers
        var trackers = [];
        for (var name in tabData.trackers) {
          if (name == 'length') continue;
          trackers.push({
            name: name,
            suggested: tabData.trackers[name] == 2,
            blocked: name in tabData.blocked
          });
        }
        data.trackers = trackers;
        // copy domains
        var domains = [];
        for (var domain in tabData.domains) {
          if (domain == 'length') continue;
          domains.push(domain);
        }
        data.domains = domains;

        return data;
      },

      getRulesVersion: function() {
        return contentPolicy.getRulesVersion();
      },

      triggerUpdateIcon: function(tabId) {
        TabData.triggerUpdateIcon(tabId);
      },

      markSettingsChanged: function(tabId, setting) {
        TabData.markUnmarkChanges(tabId, setting);
      },

      setCustomTabData: function(tabId, name, value) {
        TabData.setCustomData(tabId, name, value);
      },

      getCustomTabData: function(tabId, name) {
        return TabData.getCustomData(tabId, name);
      },

      getDomain: function (url) {
        return contentPolicy.getDomain(url);
      }

    });

    ABINE_DNT.API = API;

    return API;
  });
