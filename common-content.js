/**
 * Blur Copyright (c) 2008-2015 by Abine, Inc. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Abine, Inc. ("Confidential Information"), subject
 * to the Non-Disclosure Agreement and/or License Agreement you entered
 * into with Abine. You shall use such Confidential Information only
 * in accordance with the terms of said Agreement(s). Abine makes
 * no representations or warranties about the suitability of the
 * software. The software is provided with ABSOLUTELY NO WARRANTY
 * and Abine will NOT BE LIABLE for ANY DAMAGES resulting from
 * the use of the software.
 *
 * Contact license@getabine.com with any license-related questions.
 *
 * https://www.abine.com
 * @license
 *
 */

;

// pre-processing directives
if (typeof LOG_FINEST === 'undefined') LOG_FINEST = true;
if (typeof LOG_DEBUG  === 'undefined') LOG_DEBUG = true;
if (typeof LOG_INFO   === 'undefined') LOG_INFO = true;
if (typeof LOG_WARN   === 'undefined') LOG_WARN = true;
if (typeof LOG_ERROR  === 'undefined') LOG_ERROR = true;


// Firefox does not have global timer available,
// so we must have our own implementation,
// and it must work both in a plain html window, and chrome.

ABINE_DNTME.define("abine/timer",
    [],
    function() {

  var abineTimerId = 1, abineTimerMap = {};

  var ffSetTimeout = function(func, time) {
    var id = abineTimerId++;
    var timer = Cc["@mozilla.org/timer;1"].createInstance(Ci.nsITimer);
    abineTimerMap[id] = timer;
    timer.initWithCallback(function(){
        delete abineTimerMap[id];
        func();
      }, time, Ci.nsITimer.TYPE_ONE_SHOT);
    return id;
  };

  var ffClearTimer = function(id) {
    try {
      if (id in abineTimerMap){
        abineTimerMap[id].cancel();
        delete abineTimerMap[id];
      }
    } catch(e) {}
  };

  var ffSetInterval = function(func, time) {
    var id = abineTimerId++;
    var timer = Cc["@mozilla.org/timer;1"].createInstance(Ci.nsITimer);
    abineTimerMap[id] = timer;
    timer.initWithCallback(func, time, Ci.nsITimer.TYPE_REPEATING_SLACK);
    return id;
  };

  var timerAPI = {};

  if (typeof(window) != 'undefined' && typeof window.setTimeout == "function" ) {
    timerAPI.setTimeout = function() {
      if(ABINE_DNTME.log) LOG_FINEST && ABINE_DNTME.log.finest("setting timeout of " + arguments[1]);
      return window.setTimeout.apply(window, arguments);
    }
  } else {
    timerAPI.setTimeout = ffSetTimeout;
  }

  if (typeof(window) != 'undefined' && typeof window.clearTimeout == "function" ) {
    timerAPI.clearTimeout = function() {
      window.clearTimeout.apply(window, arguments);
    }
  } else {
    timerAPI.clearTimeout = ffClearTimer;
  }

  if (typeof(window) != 'undefined' && typeof window.setInterval == "function" ) {
    timerAPI.setInterval = function() {
      return window.setInterval.apply(window, arguments);
    }
  } else {
    timerAPI.setInterval = ffSetInterval;
  }

  if (typeof(window) != 'undefined' && typeof window.clearInterval == "function" ) {
    timerAPI.clearInterval = function() {
      window.clearInterval.apply(window, arguments);
    }
  } else {
    timerAPI.clearInterval = ffClearTimer;
  }

  return timerAPI;

  // wot a pain

});


ABINE_DNTME.define('abine/intl',[], function(){

  var currentLocale = navigator.language.replace(/\-.*/, '').toLowerCase();

  var bundle = null;

  var loadedLocale = {en: 1};

  var supported_locales = {en: 1, de: 1, fr: 1, it: 1, nl: 1, pl: 1, pt: 1, zh: 1};

  function setupLocale(locale, callback) {
    if (locale in supported_locales) {
      if (locale in loadedLocale) {
        bundle = BUNDLE[locale];
        currentLocale = locale;
        callback();
      } else {
        var script = document.createElement('script');
        script.setAttribute('src', 'locales/'+locale+'.js');
        script.addEventListener('load', function(){
          loadedLocale[locale] = true;
          bundle = BUNDLE[locale];
          currentLocale = locale;
          callback();
        });
        script.addEventListener('error', function(){
          setupLocale('en', callback);
        });
        document.getElementsByTagName('body')[0].appendChild(script);
      }
    } else {
      setupLocale('en', callback);
    }
  }

  return {
    setupLocale: setupLocale,
    getLang: function(){
      return currentLocale;
    },
    getText: function(name, args) {
      if (args && !(typeof(args) === 'object')) { // really a check for array but tried .constructor === Array and instanceof array were not working in FF
        args = [args];
      }

      var msg=null;
      name = name.replace(/\./g,"_");

      if (bundle) {
        try {
          msg = bundle[name] || BUNDLE['en'][name];

          if (msg && args) {
            msg = msg.replace(/\{([0-9]+)\}/g,
              function($0, $1) {
                return args[parseInt($1)]+"";
              }
            );
          }
        } catch(e) {}

      }

      if (!msg || msg == "") {
        msg = "***"+name;
      }

      return msg;
    }
  };
});
/*!
CryptoJS v3.1.2
code.google.com/p/crypto-js
(c) 2009-2013 by Jeff Mott. All rights reserved.
code.google.com/p/crypto-js/wiki/License
*/
/**
 * CryptoJS core components.
 */

var CryptoJS = CryptoJS || (function (Math, undefined) {
    /**
     * CryptoJS namespace.
     */
    var C = {};

    /**
     * Library namespace.
     */
    var C_lib = C.lib = {};

    /**
     * Base object for prototypal inheritance.
     */
    var Base = C_lib.Base = (function () {
        function F() {}

        return {
            /**
             * Creates a new object that inherits from this object.
             *
             * @param {Object} overrides Properties to copy into the new object.
             *
             * @return {Object} The new object.
             *
             * @static
             *
             * @example
             *
             *     var MyType = CryptoJS.lib.Base.extend({
             *         field: 'value',
             *
             *         method: function () {
             *         }
             *     });
             */
            extend: function (overrides) {
                // Spawn
                F.prototype = this;
                var subtype = new F();

                // Augment
                if (overrides) {
                    subtype.mixIn(overrides);
                }

                // Create default initializer
                if (!subtype.hasOwnProperty('init')) {
                    subtype.init = function () {
                        subtype.$super.init.apply(this, arguments);
                    };
                }

                // Initializer's prototype is the subtype object
                subtype.init.prototype = subtype;

                // Reference supertype
                subtype.$super = this;

                return subtype;
            },

            /**
             * Extends this object and runs the init method.
             * Arguments to create() will be passed to init().
             *
             * @return {Object} The new object.
             *
             * @static
             *
             * @example
             *
             *     var instance = MyType.create();
             */
            create: function () {
                var instance = this.extend();
                instance.init.apply(instance, arguments);

                return instance;
            },

            /**
             * Initializes a newly created object.
             * Override this method to add some logic when your objects are created.
             *
             * @example
             *
             *     var MyType = CryptoJS.lib.Base.extend({
             *         init: function () {
             *             // ...
             *         }
             *     });
             */
            init: function () {
            },

            /**
             * Copies properties into this object.
             *
             * @param {Object} properties The properties to mix in.
             *
             * @example
             *
             *     MyType.mixIn({
             *         field: 'value'
             *     });
             */
            mixIn: function (properties) {
                for (var propertyName in properties) {
                    if (properties.hasOwnProperty(propertyName)) {
                        this[propertyName] = properties[propertyName];
                    }
                }

                // IE won't copy toString using the loop above
                if (properties.hasOwnProperty('toString')) {
                    this.toString = properties.toString;
                }
            },

            /**
             * Creates a copy of this object.
             *
             * @return {Object} The clone.
             *
             * @example
             *
             *     var clone = instance.clone();
             */
            clone: function () {
                return this.init.prototype.extend(this);
            }
        };
    }());

    /**
     * An array of 32-bit words.
     *
     * @property {Array} words The array of 32-bit words.
     * @property {number} sigBytes The number of significant bytes in this word array.
     */
    var WordArray = C_lib.WordArray = Base.extend({
        /**
         * Initializes a newly created word array.
         *
         * @param {Array} words (Optional) An array of 32-bit words.
         * @param {number} sigBytes (Optional) The number of significant bytes in the words.
         *
         * @example
         *
         *     var wordArray = CryptoJS.lib.WordArray.create();
         *     var wordArray = CryptoJS.lib.WordArray.create([0x00010203, 0x04050607]);
         *     var wordArray = CryptoJS.lib.WordArray.create([0x00010203, 0x04050607], 6);
         */
        init: function (words, sigBytes) {
            words = this.words = words || [];

            if (sigBytes != undefined) {
                this.sigBytes = sigBytes;
            } else {
                this.sigBytes = words.length * 4;
            }
        },

        /**
         * Converts this word array to a string.
         *
         * @param {Encoder} encoder (Optional) The encoding strategy to use. Default: CryptoJS.enc.Hex
         *
         * @return {string} The stringified word array.
         *
         * @example
         *
         *     var string = wordArray + '';
         *     var string = wordArray.toString();
         *     var string = wordArray.toString(CryptoJS.enc.Utf8);
         */
        toString: function (encoder) {
            return (encoder || Hex).stringify(this);
        },

        /**
         * Concatenates a word array to this word array.
         *
         * @param {WordArray} wordArray The word array to append.
         *
         * @return {WordArray} This word array.
         *
         * @example
         *
         *     wordArray1.concat(wordArray2);
         */
        concat: function (wordArray) {
            // Shortcuts
            var thisWords = this.words;
            var thatWords = wordArray.words;
            var thisSigBytes = this.sigBytes;
            var thatSigBytes = wordArray.sigBytes;

            // Clamp excess bits
            this.clamp();

            // Concat
            if (thisSigBytes % 4) {
                // Copy one byte at a time
                for (var i = 0; i < thatSigBytes; i++) {
                    var thatByte = (thatWords[i >>> 2] >>> (24 - (i % 4) * 8)) & 0xff;
                    thisWords[(thisSigBytes + i) >>> 2] |= thatByte << (24 - ((thisSigBytes + i) % 4) * 8);
                }
            } else if (thatWords.length > 0xffff) {
                // Copy one word at a time
                for (var i = 0; i < thatSigBytes; i += 4) {
                    thisWords[(thisSigBytes + i) >>> 2] = thatWords[i >>> 2];
                }
            } else {
                // Copy all words at once
                thisWords.push.apply(thisWords, thatWords);
            }
            this.sigBytes += thatSigBytes;

            // Chainable
            return this;
        },

        /**
         * Removes insignificant bits.
         *
         * @example
         *
         *     wordArray.clamp();
         */
        clamp: function () {
            // Shortcuts
            var words = this.words;
            var sigBytes = this.sigBytes;

            // Clamp
            words[sigBytes >>> 2] &= 0xffffffff << (32 - (sigBytes % 4) * 8);
            words.length = Math.ceil(sigBytes / 4);
        },

        /**
         * Creates a copy of this word array.
         *
         * @return {WordArray} The clone.
         *
         * @example
         *
         *     var clone = wordArray.clone();
         */
        clone: function () {
            var clone = Base.clone.call(this);
            clone.words = this.words.slice(0);

            return clone;
        },

        /**
         * Creates a word array filled with random bytes.
         *
         * @param {number} nBytes The number of random bytes to generate.
         *
         * @return {WordArray} The random word array.
         *
         * @static
         *
         * @example
         *
         *     var wordArray = CryptoJS.lib.WordArray.random(16);
         */
        random: function (nBytes) {
            var words = [];
            for (var i = 0; i < nBytes; i += 4) {
                words.push((Math.random() * 0x100000000) | 0);
            }

            return new WordArray.init(words, nBytes);
        }
    });

    /**
     * Encoder namespace.
     */
    var C_enc = C.enc = {};

    /**
     * Hex encoding strategy.
     */
    var Hex = C_enc.Hex = {
        /**
         * Converts a word array to a hex string.
         *
         * @param {WordArray} wordArray The word array.
         *
         * @return {string} The hex string.
         *
         * @static
         *
         * @example
         *
         *     var hexString = CryptoJS.enc.Hex.stringify(wordArray);
         */
        stringify: function (wordArray) {
            // Shortcuts
            var words = wordArray.words;
            var sigBytes = wordArray.sigBytes;

            // Convert
            var hexChars = [];
            for (var i = 0; i < sigBytes; i++) {
                var bite = (words[i >>> 2] >>> (24 - (i % 4) * 8)) & 0xff;
                hexChars.push((bite >>> 4).toString(16));
                hexChars.push((bite & 0x0f).toString(16));
            }

            return hexChars.join('');
        },

        /**
         * Converts a hex string to a word array.
         *
         * @param {string} hexStr The hex string.
         *
         * @return {WordArray} The word array.
         *
         * @static
         *
         * @example
         *
         *     var wordArray = CryptoJS.enc.Hex.parse(hexString);
         */
        parse: function (hexStr) {
            // Shortcut
            var hexStrLength = hexStr.length;

            // Convert
            var words = [];
            for (var i = 0; i < hexStrLength; i += 2) {
                words[i >>> 3] |= parseInt(hexStr.substr(i, 2), 16) << (24 - (i % 8) * 4);
            }

            return new WordArray.init(words, hexStrLength / 2);
        }
    };

    /**
     * Latin1 encoding strategy.
     */
    var Latin1 = C_enc.Latin1 = {
        /**
         * Converts a word array to a Latin1 string.
         *
         * @param {WordArray} wordArray The word array.
         *
         * @return {string} The Latin1 string.
         *
         * @static
         *
         * @example
         *
         *     var latin1String = CryptoJS.enc.Latin1.stringify(wordArray);
         */
        stringify: function (wordArray) {
            // Shortcuts
            var words = wordArray.words;
            var sigBytes = wordArray.sigBytes;

            // Convert
            var latin1Chars = [];
            for (var i = 0; i < sigBytes; i++) {
                var bite = (words[i >>> 2] >>> (24 - (i % 4) * 8)) & 0xff;
                latin1Chars.push(String.fromCharCode(bite));
            }

            return latin1Chars.join('');
        },

        /**
         * Converts a Latin1 string to a word array.
         *
         * @param {string} latin1Str The Latin1 string.
         *
         * @return {WordArray} The word array.
         *
         * @static
         *
         * @example
         *
         *     var wordArray = CryptoJS.enc.Latin1.parse(latin1String);
         */
        parse: function (latin1Str) {
            // Shortcut
            var latin1StrLength = latin1Str.length;

            // Convert
            var words = [];
            for (var i = 0; i < latin1StrLength; i++) {
                words[i >>> 2] |= (latin1Str.charCodeAt(i) & 0xff) << (24 - (i % 4) * 8);
            }

            return new WordArray.init(words, latin1StrLength);
        }
    };

    /**
     * UTF-8 encoding strategy.
     */
    var Utf8 = C_enc.Utf8 = {
        /**
         * Converts a word array to a UTF-8 string.
         *
         * @param {WordArray} wordArray The word array.
         *
         * @return {string} The UTF-8 string.
         *
         * @static
         *
         * @example
         *
         *     var utf8String = CryptoJS.enc.Utf8.stringify(wordArray);
         */
        stringify: function (wordArray) {
            try {
                return decodeURIComponent(escape(Latin1.stringify(wordArray)));
            } catch (e) {
                throw new Error('Malformed UTF-8 data');
            }
        },

        /**
         * Converts a UTF-8 string to a word array.
         *
         * @param {string} utf8Str The UTF-8 string.
         *
         * @return {WordArray} The word array.
         *
         * @static
         *
         * @example
         *
         *     var wordArray = CryptoJS.enc.Utf8.parse(utf8String);
         */
        parse: function (utf8Str) {
            return Latin1.parse(unescape(encodeURIComponent(utf8Str)));
        }
    };

    /**
     * Abstract buffered block algorithm template.
     *
     * The property blockSize must be implemented in a concrete subtype.
     *
     * @property {number} _minBufferSize The number of blocks that should be kept unprocessed in the buffer. Default: 0
     */
    var BufferedBlockAlgorithm = C_lib.BufferedBlockAlgorithm = Base.extend({
        /**
         * Resets this block algorithm's data buffer to its initial state.
         *
         * @example
         *
         *     bufferedBlockAlgorithm.reset();
         */
        reset: function () {
            // Initial values
            this._data = new WordArray.init();
            this._nDataBytes = 0;
        },

        /**
         * Adds new data to this block algorithm's buffer.
         *
         * @param {WordArray|string} data The data to append. Strings are converted to a WordArray using UTF-8.
         *
         * @example
         *
         *     bufferedBlockAlgorithm._append('data');
         *     bufferedBlockAlgorithm._append(wordArray);
         */
        _append: function (data) {
            // Convert string to WordArray, else assume WordArray already
            if (typeof data == 'string') {
                data = Utf8.parse(data);
            }

            // Append
            this._data.concat(data);
            this._nDataBytes += data.sigBytes;
        },

        /**
         * Processes available data blocks.
         *
         * This method invokes _doProcessBlock(offset), which must be implemented by a concrete subtype.
         *
         * @param {boolean} doFlush Whether all blocks and partial blocks should be processed.
         *
         * @return {WordArray} The processed data.
         *
         * @example
         *
         *     var processedData = bufferedBlockAlgorithm._process();
         *     var processedData = bufferedBlockAlgorithm._process(!!'flush');
         */
        _process: function (doFlush) {
            // Shortcuts
            var data = this._data;
            var dataWords = data.words;
            var dataSigBytes = data.sigBytes;
            var blockSize = this.blockSize;
            var blockSizeBytes = blockSize * 4;

            // Count blocks ready
            var nBlocksReady = dataSigBytes / blockSizeBytes;
            if (doFlush) {
                // Round up to include partial blocks
                nBlocksReady = Math.ceil(nBlocksReady);
            } else {
                // Round down to include only full blocks,
                // less the number of blocks that must remain in the buffer
                nBlocksReady = Math.max((nBlocksReady | 0) - this._minBufferSize, 0);
            }

            // Count words ready
            var nWordsReady = nBlocksReady * blockSize;

            // Count bytes ready
            var nBytesReady = Math.min(nWordsReady * 4, dataSigBytes);

            // Process blocks
            if (nWordsReady) {
                for (var offset = 0; offset < nWordsReady; offset += blockSize) {
                    // Perform concrete-algorithm logic
                    this._doProcessBlock(dataWords, offset);
                }

                // Remove processed words
                var processedWords = dataWords.splice(0, nWordsReady);
                data.sigBytes -= nBytesReady;
            }

            // Return processed words
            return new WordArray.init(processedWords, nBytesReady);
        },

        /**
         * Creates a copy of this object.
         *
         * @return {Object} The clone.
         *
         * @example
         *
         *     var clone = bufferedBlockAlgorithm.clone();
         */
        clone: function () {
            var clone = Base.clone.call(this);
            clone._data = this._data.clone();

            return clone;
        },

        _minBufferSize: 0
    });

    /**
     * Abstract hasher template.
     *
     * @property {number} blockSize The number of 32-bit words this hasher operates on. Default: 16 (512 bits)
     */
    var Hasher = C_lib.Hasher = BufferedBlockAlgorithm.extend({
        /**
         * Configuration options.
         */
        cfg: Base.extend(),

        /**
         * Initializes a newly created hasher.
         *
         * @param {Object} cfg (Optional) The configuration options to use for this hash computation.
         *
         * @example
         *
         *     var hasher = CryptoJS.algo.SHA256.create();
         */
        init: function (cfg) {
            // Apply config defaults
            this.cfg = this.cfg.extend(cfg);

            // Set initial values
            this.reset();
        },

        /**
         * Resets this hasher to its initial state.
         *
         * @example
         *
         *     hasher.reset();
         */
        reset: function () {
            // Reset data buffer
            BufferedBlockAlgorithm.reset.call(this);

            // Perform concrete-hasher logic
            this._doReset();
        },

        /**
         * Updates this hasher with a message.
         *
         * @param {WordArray|string} messageUpdate The message to append.
         *
         * @return {Hasher} This hasher.
         *
         * @example
         *
         *     hasher.update('message');
         *     hasher.update(wordArray);
         */
        update: function (messageUpdate) {
            // Append
            this._append(messageUpdate);

            // Update the hash
            this._process();

            // Chainable
            return this;
        },

        /**
         * Finalizes the hash computation.
         * Note that the finalize operation is effectively a destructive, read-once operation.
         *
         * @param {WordArray|string} messageUpdate (Optional) A final message update.
         *
         * @return {WordArray} The hash.
         *
         * @example
         *
         *     var hash = hasher.finalize();
         *     var hash = hasher.finalize('message');
         *     var hash = hasher.finalize(wordArray);
         */
        finalize: function (messageUpdate) {
            // Final message update
            if (messageUpdate) {
                this._append(messageUpdate);
            }

            // Perform concrete-hasher logic
            var hash = this._doFinalize();

            return hash;
        },

        blockSize: 512/32,

        /**
         * Creates a shortcut function to a hasher's object interface.
         *
         * @param {Hasher} hasher The hasher to create a helper for.
         *
         * @return {Function} The shortcut function.
         *
         * @static
         *
         * @example
         *
         *     var SHA256 = CryptoJS.lib.Hasher._createHelper(CryptoJS.algo.SHA256);
         */
        _createHelper: function (hasher) {
            return function (message, cfg) {
                return new hasher.init(cfg).finalize(message);
            };
        },

        /**
         * Creates a shortcut function to the HMAC's object interface.
         *
         * @param {Hasher} hasher The hasher to use in this HMAC helper.
         *
         * @return {Function} The shortcut function.
         *
         * @static
         *
         * @example
         *
         *     var HmacSHA256 = CryptoJS.lib.Hasher._createHmacHelper(CryptoJS.algo.SHA256);
         */
        _createHmacHelper: function (hasher) {
            return function (message, key) {
                return new C_algo.HMAC.init(hasher, key).finalize(message);
            };
        }
    });

    /**
     * Algorithm namespace.
     */
    var C_algo = C.algo = {};

    return C;
}(Math));
/*!
CryptoJS v3.1.2
code.google.com/p/crypto-js
(c) 2009-2013 by Jeff Mott. All rights reserved.
code.google.com/p/crypto-js/wiki/License
*/

(function () {
    // Shortcuts
    var C = CryptoJS;
    var C_lib = C.lib;
    var WordArray = C_lib.WordArray;
    var Hasher = C_lib.Hasher;
    var C_algo = C.algo;

    // Reusable object
    var W = [];

    /**
     * SHA-1 hash algorithm.
     */
    var SHA1 = C_algo.SHA1 = Hasher.extend({
        _doReset: function () {
            this._hash = new WordArray.init([
                0x67452301, 0xefcdab89,
                0x98badcfe, 0x10325476,
                0xc3d2e1f0
            ]);
        },

        _doProcessBlock: function (M, offset) {
            // Shortcut
            var H = this._hash.words;

            // Working variables
            var a = H[0];
            var b = H[1];
            var c = H[2];
            var d = H[3];
            var e = H[4];

            // Computation
            for (var i = 0; i < 80; i++) {
                if (i < 16) {
                    W[i] = M[offset + i] | 0;
                } else {
                    var n = W[i - 3] ^ W[i - 8] ^ W[i - 14] ^ W[i - 16];
                    W[i] = (n << 1) | (n >>> 31);
                }

                var t = ((a << 5) | (a >>> 27)) + e + W[i];
                if (i < 20) {
                    t += ((b & c) | (~b & d)) + 0x5a827999;
                } else if (i < 40) {
                    t += (b ^ c ^ d) + 0x6ed9eba1;
                } else if (i < 60) {
                    t += ((b & c) | (b & d) | (c & d)) - 0x70e44324;
                } else /* if (i < 80) */ {
                    t += (b ^ c ^ d) - 0x359d3e2a;
                }

                e = d;
                d = c;
                c = (b << 30) | (b >>> 2);
                b = a;
                a = t;
            }

            // Intermediate hash value
            H[0] = (H[0] + a) | 0;
            H[1] = (H[1] + b) | 0;
            H[2] = (H[2] + c) | 0;
            H[3] = (H[3] + d) | 0;
            H[4] = (H[4] + e) | 0;
        },

        _doFinalize: function () {
            // Shortcuts
            var data = this._data;
            var dataWords = data.words;

            var nBitsTotal = this._nDataBytes * 8;
            var nBitsLeft = data.sigBytes * 8;

            // Add padding
            dataWords[nBitsLeft >>> 5] |= 0x80 << (24 - nBitsLeft % 32);
            dataWords[(((nBitsLeft + 64) >>> 9) << 4) + 14] = Math.floor(nBitsTotal / 0x100000000);
            dataWords[(((nBitsLeft + 64) >>> 9) << 4) + 15] = nBitsTotal;
            data.sigBytes = dataWords.length * 4;

            // Hash final blocks
            this._process();

            // Return final computed hash
            return this._hash;
        },

        clone: function () {
            var clone = Hasher.clone.call(this);
            clone._hash = this._hash.clone();

            return clone;
        }
    });

    /**
     * Shortcut function to the hasher's object interface.
     *
     * @param {WordArray|string} message The message to hash.
     *
     * @return {WordArray} The hash.
     *
     * @static
     *
     * @example
     *
     *     var hash = CryptoJS.SHA1('message');
     *     var hash = CryptoJS.SHA1(wordArray);
     */
    C.SHA1 = Hasher._createHelper(SHA1);

    /**
     * Shortcut function to the HMAC's object interface.
     *
     * @param {WordArray|string} message The message to hash.
     * @param {WordArray|string} key The secret key.
     *
     * @return {WordArray} The HMAC.
     *
     * @static
     *
     * @example
     *
     *     var hmac = CryptoJS.HmacSHA1(message, key);
     */
    C.HmacSHA1 = Hasher._createHmacHelper(SHA1);
}());


/*!
 CryptoJS v3.1.2
 code.google.com/p/crypto-js
 (c) 2009-2013 by Jeff Mott. All rights reserved.
 code.google.com/p/crypto-js/wiki/License
 */


ABINE_DNTME.define('cryptojs', function() {

  return {
    SHA1: function(input){
      return CryptoJS.SHA1(input).toString();
    }
  };

});


// Core API
// Provides core API functionality

ABINE_DNTME.define("abine/core",
    ["documentcloud/underscore"],
    function(_) {

  // Backbone style events

  // A module that can be mixed in to *any object* in order to provide it with
  // custom events. You may `bind` or `unbind` a callback function to an event;
  // `trigger`-ing an event fires all callbacks in succession.
  //
  //     var object = {};
  //     _.extend(object, Backbone.Events);
  //     object.bind('expand', function(){ alert('expanded'); });
  //     object.trigger('expand');
  //
  var Events = {

    // Bind an event, specified by a string name, `ev`, to a `callback` function.
    // Passing `"all"` will bind the callback to all events fired.
    on : function(ev, callback, context) {
      var calls = this._callbacks || (this._callbacks = {});
      var list  = calls[ev] || (calls[ev] = {});
      var tail = list.tail || (list.tail = list.next = {});
      tail.callback = callback;
      tail.context = context;
      list.tail = tail.next = {};
      return this;
    },

    // Remove one or many callbacks. If `callback` is null, removes all
    // callbacks for the event. If `ev` is null, removes all bound callbacks
    // for all events.
    off : function(ev, callback) {
      var calls, node, prev;
      if (!ev) {
        this._callbacks = null;
      } else if (calls = this._callbacks) {
        if (!callback) {
          calls[ev] = {};
        } else if (node = calls[ev]) {
          while ((prev = node) && (node = node.next)) {
            if (node.callback !== callback) continue;
            prev.next = node.next;
            node.context = node.callback = null;
            break;
          }
        }
      }
      return this;
    },

    // Trigger an event, firing all bound callbacks. Callbacks are passed the
    // same arguments as `trigger` is, apart from the event name.
    // Listening for `"all"` passes the true event name as the first argument.
    trigger : function(eventName) {
      var node, calls, callback, args, ev, events = ['all', eventName];
      if (!(calls = this._callbacks)) return this;
      while (ev = events.pop()) {
        if (!(node = calls[ev])) continue;
        args = ev == 'all' ? arguments : Array.prototype.slice.call(arguments, 1);
        while (node = node.next) if (callback = node.callback) callback.apply(node.context || this, args);
      }
      return this;
    },

    // Bind an event, callback is called only once and event handler is removed upon first call.
    once: function(eventName, callback, context) {
      var self = this;
      function wrapCallback() {
        self.off(eventName, wrapCallback);
        return callback.apply(this, arguments);
      }
      return this.on(eventName, wrapCallback, context);
    },

    // Bind all events, calling of any event will un-subscribe from all other events.
    // eg:  onceAny(['event1', callback1, context1], ['event2', callback2, context2], ....)
    onceAny: function() {
      var mainArguments = arguments;

      function offAll() {
        for (var i=0;i<mainArguments.length;i++) {
          // remove internal event handler
          this.off.apply(this, [mainArguments[i][0], offAll]);
          // remove input events
          this.off.apply(this, mainArguments[i]);
        }
      }

      for (var i=0;i<arguments.length;i++) {
        // hook with input arguments
        this.on.apply(this, arguments[i]);
        // add our internal event handler to remove all other event handlers
        this.on.apply(this, [arguments[i][0], offAll]);
      }

      return this;
    },

    // returns true if there is a handler for given event name
    hasListeners: function(eventName) {
      var node, calls;
      if (!(calls = this._callbacks)) return false;
      if (!(node = calls[eventName])) return false;
      while (node = node.next) {
        if (node.callback)
          return true;
      }
      return false;
    }

  };

  Events.bind = Events.on;
  Events.unbind = Events.off;

  // Backbone style inheritance system
  
  // Shared empty constructor function to aid in prototype-chain creation.
  var ctor = function(){};

  // Helper function to correctly set up the prototype chain, for subclasses.
  // Similar to `goog.inherits`, but uses a hash of prototype properties and
  // class properties to be extended.
  var inherits = function(parent, protoProps, staticProps) {
    var child;

    // The constructor function for the new subclass is either defined by you
    // (the "constructor" property in your `extend` definition), or defaulted
    // by us to simply call `super()`.
    if (protoProps && protoProps.hasOwnProperty('constructor')) {
      child = protoProps.constructor;
    } else {
      child = function(){ return parent.apply(this, arguments); };
    }

    // Inherit class (static) properties from parent.
    _.extend(child, parent);

    // Set the prototype chain to inherit from `parent`, without calling
    // `parent`'s constructor function.
    ctor.prototype = parent.prototype;
    child.prototype = new ctor();

    // Add prototype properties (instance properties) to the subclass,
    // if supplied.
    if (protoProps) _.extend(child.prototype, protoProps);

    // Add static properties to the constructor function, if supplied.
    if (staticProps) _.extend(child, staticProps);

    // Correctly set child's `prototype.constructor`.
    child.prototype.constructor = child;

    // Set a convenience property in case the parent's prototype is needed later.
    child.__super__ = parent.prototype;

    return child;
  };

  // The self-propagating extend function that Backbone classes use.
  var extend = function (protoProps, classProps) {
    var child = inherits(this, protoProps, classProps);
    child.extend = this.extend;
    return child;
  };

  // Base class
  var BaseClass = function BaseClass() {
    this.initialize.apply(this, arguments);
  };
  _.extend(BaseClass.prototype, Events, {
    initialize: function initialize() {}
  });
  
  // Give it inheritance 
  BaseClass.extend = extend;  
    
    
  // Public API
  return {
    
    // ### class BaseClass
    // Base class for Backbone style inheritance system. i.e.
    // var parent = new abineCore.baseClass()
    // var child = parent.extend({ ... }) 
    BaseClass: BaseClass,
    
    // ### mixin Events
    // Backbone-style event dispatcher. Mix in to any object that can dispatch events, i.e.
    // _.extend(myClass.prototype, abineCore.Events)
    Events: Events
    
  }

});
ABINE_DNTME.define("abine/simulateEvents", ["documentcloud/underscore"], function (_) {
  return (function () {

  var eventMatchers = {
    'HTMLEvents': /^(?:load|unload|abort|error|select|change|submit|reset|focus|blur|resize|scroll)$/,
    'MouseEvents': /^(?:click|dblclick|mouse(?:down|up|over|move|out))$/
  };

  var defaultOptions = {
    pointerX: 0,
    pointerY: 0,
    button: 0,
    ctrlKey: false,
    altKey: false,
    shiftKey: false,
    metaKey: false,
    bubbles: true,
    cancelable: true
  };

  function simulate(element, eventName) {
    var document = element.ownerDocument;
    var options = extend(defaultOptions, arguments[2] || {});
    var oEvent, eventType = null;

    for (var name in eventMatchers) {
      if (eventMatchers[name].test(eventName)) {
        eventType = name;
        break;
      }
    }

    if (!eventType)
      throw new SyntaxError('Only HTMLEvents and MouseEvents interfaces are supported');

    if (document.createEvent) {
      if (eventType == 'HTMLEvents') {
        oEvent = document.createEvent(eventType);
        oEvent.initEvent(eventName, options.bubbles, options.cancelable);
      } else {
        oEvent = new document.defaultView.MouseEvent(eventName, options)
      }
      element.dispatchEvent(oEvent);
    }
    else {
      options.clientX = options.pointerX;
      options.clientY = options.pointerY;
      var evt = document.createEventObject();
      oEvent = extend(evt, options);
      element.fireEvent('on' + eventName, oEvent);
    }
    return element;
  }

  function extend(destination, source) {
    for (var property in source)
      destination[property] = source[property];
    return destination;
  }

  function asyncFillFunctions() {
    (function() {
      function abineDispatch(el, opts, cb) {
        opts = opts || {};
        var doc = (el.ownerDocument || el);
        var evt = doc.createEvent ? doc.createEvent('CustomEvent') : doc.createEventObject();
        evt.initCustomEvent && evt.initCustomEvent(opts.type, !!opts.bubbles, !!opts.cancelable, opts.detail);
        for (var key in opts) {
          evt[key] = opts[key];
        }
        setTimeout(function () {
          try {
            el.dispatchEvent ? el.dispatchEvent(evt) : el.fireEvent("on" + opts.type, doc.createEventObject());
          } catch (e) {
            var listeners = el['listen' + opts.type];
            if (listeners) {
              for (var i = 0; i < listeners.length; ++i) {
                try {
                  listeners[i].call(el, evt);
                } catch(e){}
              }
            }
          }
          cb();
        }, 0);
        return this;
      }

      function abineTriggerDropdownChange(el, val, callback) {
        var dispatchNonJQuery = true;
        var fancified = el.className && el.className.indexOf('fancified') != -1;
        if (window.jQuery) {
          var el$ = window.jQuery(el);
          try {
            if (el$.selectBoxIt) {
              el$.selectBoxIt("selectOption", el$.val());
            } else if (el$.data("chosen") || el$.chosen) {
              // trigger event for old and new version of chosen
              el$.trigger("chosen:updated").trigger("liszt:updated");
            } else if (el$.data("chooserElement")) {
              el$.trigger("change");
            } else if (el$.fancySelect) {
              el$.get('fancySelect').select('value', el$.val());
            } else if (el$.selectBox) {
              el$.selectBox('value', el$.val());
            } else if (el$.selectric) {
              el$.selectric('refresh')
            } else if (el$.coreUISelect) {
              var coreUiSelect = el$.data('coreUISelect');
              coreUiSelect.isSelectShow = true;
              coreUiSelect.changeDropdownData();
              coreUiSelect.isSelectShow = false;
            } else if (el$.data('myJSPulldownObject')) {
              var jsPullDown = el$.data('myJSPulldownObject');
              jsPullDown.setToValue(el$.val());
            } else if (el$.fancyfields) {
              el$.setVal(el$.val());
            } else if (el$.data("select2")) {
              // nothing
            } else if (el$.data("selectize")) {
              dispatchNonJQuery = false;
              el$.data("selectize").setValue(el$.val());
            } else if (el$.hasClass("fancified")) {
              // update for other libraries will clear dropdown
              el$.trigger("update");
            } else if (el$.selectmenu) {
              var newVal = el$.val();
              try{el$.selectmenu("value", el$[0].options[0].value);}catch(e){}
              el$.selectmenu("value", newVal);
            } else if (el$.hasClass('select-hidden') && el$.next('div.select-styled').length &&
            el$.next('div.select-styled').next('.select-options').length ) {

              ulElement = el$.next('div.select-styled').next('.select-options');
              liElement = ulElement.find("li[rel='" + el$.val() + "']");

              liElement.click();
            }

            // send change event for all libraries
            el$.trigger("change");
          } catch(e){}
        }
        if (dispatchNonJQuery) {
          function fireEvent(element, event) {
            try {
              var doc = element.ownerDocument;
              if (doc.createEventObject) {
                var evt = doc.createEventObject();
                element.fireEvent('on' + event, evt);
              } else {
                evt = doc.createEvent("HTMLEvents");
                evt.initEvent(event, true, true);
                element.dispatchEvent(evt);
              }
            } catch (e) {
            }
          }

          if (fancified) {
            fireEvent(el, "update");
          }
          fireEvent(el, "change");
          fireEvent(el, "blur");
        }
        callback();
      }

      function abineTypeKey(el, charCode, newVal, cb) {
        var valBeforeType = el.value;
        abineDispatch(el, {
          type: 'keydown',
          keyCode: charCode,
          which: charCode,
          charCode: charCode,
          bubbles: true
        }, function () {
          abineDispatch(el, {
            type: 'keypress',
            keyCode: charCode,
            which: charCode,
            charCode: charCode,
            bubbles: true
          }, function () {
            setTimeout(function () {
              var valAfterType = el.value;
              if (valBeforeType == valAfterType) {
                el.value = newVal;
              }
              abineDispatch(el, {
                type: 'input',
                keyCode: charCode,
                which: charCode,
                charCode: charCode,
                bubbles: true
              }, function () {
                abineDispatch(el, {
                  type: 'keyup',
                  keyCode: charCode,
                  which: charCode,
                  charCode: charCode,
                  bubbles: true
                }, function () {
                  cb();
                });
              });
            }, 1);
          });
        });
      }

      function abineTypeString(el, str, typedSoFar, cb) {
        if (!str || str == "") {
          cb();
          return;
        }

        var charCode = str.charCodeAt(0);
        typedSoFar += str.charAt(0);
        abineTypeKey(el, charCode, typedSoFar, function () {
          abineTypeString(el, str.substring(1), typedSoFar, cb);
        });
      }

      function abineTriggerTextChange(el, val, cb) {
        if (window.abineTriggerChangeInProgress) {
          setTimeout(function(){abineTriggerTextChange(el, val, cb);}, 100);
          return;
        }

        var needsToDispatchEvents = true;

        window.abineTriggerChangeInProgress = true;
        try {
          // jquery masked input creates problem for standard fill or slow typing.
          if (window.jQuery) {
            var el$ = window.jQuery(el);
            if (el$.data('rawMaskFn') || el$.mask // jquery-maskedinput
              || el$.CardPhoneFormatting // jquery-maskedinput copied by neimanmarcus.com
            ) {
              el$.focus().val(val).trigger("input").trigger("paste");
            } else if (el$.next(".inner").find(".options").length) {

              needsToDispatchEvents = false; // event dispatches cause problems for this case
              optionsContainer = el$.next(".inner").find(".options");
              optionElements = optionsContainer.find("span")
              optionElements.each(function(index){

                optionElement = $(this)
                optionElementHtml = $(optionElement).html()
                if (optionElementHtml.toLowerCase().indexOf(val) > -1){
                  $(optionElement).click()
                }
              })
            }
          }
        } catch(e){}

        if (needsToDispatchEvents){
          abineDispatch(el, {type: 'change'}, function () {
            abineDispatch(el, {type: 'blur'}, function () {
              try {
                // trigger event for modern forms (angularjs)
                var event = new Event('input', {
                  'bubbles': true,
                  'cancelable': true
                });
                el.dispatchEvent(event);
              } catch(e){}
              window.abineTriggerChangeInProgress = false;
              cb();
            });
          });
        } else {
          window.abineTriggerChangeInProgress = false;
          cb();
        }


      }

      function abineFillTextField(el, str, cb) {
        try{el.ownerDocument.defaultView.focus();}catch(e){}
        abineDispatch(el, {type: 'focus'}, function () {
          abineDispatch(el, {type: 'click'}, function () {
            abineTypeString(el, str+"\n", "", function () {
              abineTriggerTextChange(el, str, function(){
                abineDispatch(document, {type: 'abineFilled'}, function () {
                  cb();
                });
              });
            });
          });
        });
      }

      function abineFillSelectField(el, val, skipPartial, cb) {
        var spaceAnywhereRegex = /[\s]+/g;
        var lowerVal = (val || "").toLowerCase().replace(spaceAnywhereRegex, '');
        var wrapCB = function () {
          abineDispatch(document, {type: 'abineFilled'}, function () {
            cb();
          });
        };
        var changed = false, success = false;
        var opts = el.getElementsByTagName("option");

        if (opts && opts.length > 0) {
          var partialMatchIndex = -1;
          for (var i = 0; i < opts.length; i++) {
            var optText = (opts[i].text || '').toLowerCase().replace(spaceAnywhereRegex, '');
            var optVal = (opts[i].getAttribute("value")||'').toLowerCase().replace(spaceAnywhereRegex, '');
            if (optVal == lowerVal || optText == lowerVal) {
              if (!opts[i].selected) {
                changed = true;
                opts[i].selected = true;
              }
              success = true;
              break;
            } else if (partialMatchIndex == -1 && optText.indexOf(lowerVal) != -1) {
              partialMatchIndex = i;
            }
          }
          if (!success && partialMatchIndex != -1 && !skipPartial) {
            if (!opts[partialMatchIndex].selected) {
              changed = true;
              opts[partialMatchIndex].selected = true;
              success = true;
            }
          }
        }

        el.setAttribute('abineFillResponse', success);

        if (changed) {
          abineTriggerDropdownChange(el, val, wrapCB);
        } else {
          wrapCB();
        }
      }

      function getTargetElement() {
        var elements = document.getElementsByClassName("abineFillTarget");
        if (elements.length > 0) {
          return elements[0];
        }
        for (var i=0;i<frames.length;i++) {
          try {
            var elements = frames[i].document.getElementsByClassName("abineFillTarget");
            if (elements.length > 0) {
              return elements[0];
            }
          } catch(e) {}
        }
        return null;
      }

      function createAbineFillElement() {
        var div = document.createElement('div');
        div.id = 'abineFillElement';
        if (typeof(paypal) != 'undefined') {
          div.setAttribute("data-paypal", "1");
        }
        if (typeof(OffAmazonPayments) != 'undefined') {
          div.setAttribute("data-amazon", "1");
        }
        if (typeof(MasterPass) != 'undefined') {
          div.setAttribute("data-masterpass", "1");
        }
        document.documentElement.appendChild(div);
        div.addEventListener('fill', function () {
          var el = getTargetElement();
          if (el) {
            var val = div.getAttribute('value');
            abineFillTextField(el, val, function () {
            });
          } else {
            abineDispatch(document, {type: 'abineFilled'}, function () {
            });
          }
        }, false);

        div.addEventListener('fillSelect', function () {
          var el = getTargetElement();
          if (el) {
            var val = div.getAttribute('value');
            var skipPartial = !!div.getAttribute('skipPartial');
            abineFillSelectField(el, val, skipPartial, function () {
            });
          } else {
            abineDispatch(document, {type: 'abineFilled'}, function () {
            });
          }
        });

        div.addEventListener('triggerChange', function () {
          var el = getTargetElement();
          var val = div.getAttribute('value');
          if (el) {
            if (el.nodeName.match(/select/i)) {
              abineTriggerDropdownChange(el, val, function () {});
            } else {
              abineTriggerTextChange(el, val, function(){});
            }
          }
        });
      }

      createAbineFillElement();
    })();
  }

  function getAsyncFillString() {
    return "(" + asyncFillFunctions + ")()";
  }

  var AbineSimulateEvents = {
    simulate: simulate,
    getAsyncFillString: getAsyncFillString,
    asyncFillFunctions: asyncFillFunctions
  };

  return AbineSimulateEvents;

}).call(this);
;
});
ABINE_DNTME.define("abine/filler",
  ["documentcloud/underscore", 'jquery', 'abine/timer', 'abine/simulateEvents'],
  function (_, $, abineTimer, AbineSimulateEvents) {
    var setTimeout = abineTimer.setTimeout;

    return (function() {

  // dependency: _, $, setTimeout, AbineSimulateEvents

  var countryNames = {
    "AF":'Afghanistan',
    "AL":'Albania',
    "DZ":'Algeria',
    "AS":'American Samoa',
    "AD":'Andorra',
    "AO":'Angola',
    "AI":'Anguilla',
    "AQ":'Antarctica',
    "AG":'Antigua and Barbuda',
    "AR":'Argentina',
    "AM":'Armenia',
    "AW":'Aruba',
    "AC":'Ascension Island',
    "AU":'Australia',
    "AT":'Austria',
    "AZ":'Azerbaijan',
    "BS":'Bahamas',
    "BH":'Bahrain',
    "BD":'Bangladesh',
    "BB":'Barbados',
    "BE":'Belgium',
    "BZ":'Belize',
    "BJ":'Benin',
    "BM":'Bermuda',
    "BT":'Bhutan',
    "BO":'Bolivia',
    "BA":'Bosnia and Herzegovina',
    "BW":'Botswana',
    "BV":'Bouvet Island',
    "BR":'Brazil',
    "BQ":'British Antarctic Territory',
    "IO":'British Indian Ocean Territory',
    "VG":'British Virgin Islands',
    "BN":'Brunei',
    "BG":'Bulgaria',
    "BF":'Burkina Faso',
    "BI":'Burundi',
    "KH":'Cambodia',
    "CM":'Cameroon',
    "CA":'Canada',
    "IC":'Canary Islands',
    "CT":'Canton and Enderbury Islands',
    "CV":'Cape Verde',
    "KY":'Cayman Islands',
    "CF":'Central African Republic',
    "EA":'Ceuta and Melilla',
    "TD":'Chad',
    "CL":'Chile',
    "CN":'China',
    "CX":'Christmas Island',
    "CP":'Clipperton Island',
    "CC":'Cocos [Keeling] Islands',
    "CO":'Colombia',
    "KM":'Comoros',
    "CD":'Congo [DRC]',
    "CK":'Cook Islands',
    "CR":'Costa Rica',
    "HR":'Croatia',
    "CU":'Cuba',
    "CY":'Cyprus',
    "CZ":'Czech Republic',
    "DK":'Denmark',
    "DG":'Diego Garcia',
    "DJ":'Djibouti',
    "DM":'Dominica',
    "DO":'Dominican Republic',
    "NQ":'Dronning Maud Land',
    "TL":'East Timor',
    "EC":'Ecuador',
    "EG":'Egypt',
    "SV":'El Salvador',
    "EE":'Estonia',
    "ET":'Ethiopia',
    "FK":'Falkland Islands [Islas Malvinas]',
    "FO":'Faroe Islands',
    "FJ":'Fiji',
    "FI":'Finland',
    "FR":'France',
    "GF":'French Guiana',
    "PF":'French Polynesia',
    "TF":'French Southern Territories',
    "FQ":'French Southern and Antarctic Territories',
    "GA":'Gabon',
    "GM":'Gambia',
    "GE":'Georgia',
    "DE":'Germany',
    "GH":'Ghana',
    "GI":'Gibraltar',
    "GR":'Greece',
    "GL":'Greenland',
    "GD":'Grenada',
    "GP":'Guadeloupe',
    "GU":'Guam',
    "GT":'Guatemala',
    "GG":'Guernsey',
    "GW":'Guinea-Bissau',
    "GY":'Guyana',
    "HT":'Haiti',
    "HM":'Heard Island and McDonald Islands',
    "HN":'Honduras',
    "HK":'Hong Kong',
    "HU":'Hungary',
    "IS":'Iceland',
    "IN":'India',
    "ID":'Indonesia',
    "IE":'Ireland',
    "IM":'Isle of Man',
    "IL":'Israel',
    "IT":'Italy',
    "JM":'Jamaica',
    "JP":'Japan',
    "JE":'Jersey',
    "JT":'Johnston Island',
    "JO":'Jordan',
    "KZ":'Kazakhstan',
    "KE":'Kenya',
    "KI":'Kiribati',
    "KW":'Kuwait',
    "KG":'Kyrgyzstan',
    "LA":'Laos',
    "LV":'Latvia',
    "LS":'Lesotho',
    "LY":'Libya',
    "LI":'Liechtenstein',
    "LT":'Lithuania',
    "LU":'Luxembourg',
    "MO":'Macau',
    "MK":'Macedonia [FYROM]',
    "MG":'Madagascar',
    "MW":'Malawi',
    "MY":'Malaysia',
    "MV":'Maldives',
    "ML":'Mali',
    "MT":'Malta',
    "MH":'Marshall Islands',
    "MQ":'Martinique',
    "MR":'Mauritania',
    "MU":'Mauritius',
    "YT":'Mayotte',
    "FX":'Metropolitan France',
    "MX":'Mexico',
    "FM":'Micronesia',
    "MI":'Midway Islands',
    "MD":'Moldova',
    "MC":'Monaco',
    "MN":'Mongolia',
    "ME":'Montenegro',
    "MS":'Montserrat',
    "MA":'Morocco',
    "MZ":'Mozambique',
    "NA":'Namibia',
    "NR":'Nauru',
    "NP":'Nepal',
    "NL":'Netherlands',
    "AN":'Netherlands Antilles',
    "NT":'Neutral Zone',
    "NC":'New Caledonia',
    "NZ":'New Zealand',
    "NI":'Nicaragua',
    "NE":'Niger',
    "NG":'Nigeria',
    "NU":'Niue',
    "NF":'Norfolk Island',
    "VD":'North Vietnam',
    "MP":'Northern Mariana Islands',
    "NO":'Norway',
    "OM":'Oman',
    "QO":'Outlying Oceania',
    "PC":'Pacific Islands Trust Territory',
    "PK":'Pakistan',
    "PW":'Palau',
    "PS":'Palestinian Territories',
    "PA":'Panama',
    "PZ":'Panama Canal Zone',
    "PY":'Paraguay',
    "YD":'People\'s Democratic Republic of Yemen',
    "PE":'Peru',
    "PH":'Philippines',
    "PN":'Pitcairn Islands',
    "PL":'Poland',
    "PT":'Portugal',
    "PR":'Puerto Rico',
    "QA":'Qatar',
    "RO":'Romania',
    "RU":'Russia',
    "RW":'Rwanda',
    "RE":'R&eacute;union',
    "BL":'Saint Barth&eacute;lemy',
    "SH":'Saint Helena',
    "KN":'Saint Kitts and Nevis',
    "LC":'Saint Lucia',
    "MF":'Saint Martin',
    "PM":'Saint Pierre and Miquelon',
    "VC":'Saint Vincent and the Grenadines',
    "WS":'Samoa',
    "SM":'San Marino',
    "SA":'Saudi Arabia',
    "SN":'Senegal',
    "RS":'Serbia',
    "CS":'Serbia and Montenegro',
    "SC":'Seychelles',
    "SL":'Sierra Leone',
    "SG":'Singapore',
    "SK":'Slovakia',
    "SI":'Slovenia',
    "SB":'Solomon Islands',
    "ZA":'South Africa',
    "GS":'South Georgia and the South Sandwich Islands',
    "KR":'South Korea',
    "ES":'Spain',
    "LK":'Sri Lanka',
    "SR":'Suriname',
    "SJ":'Svalbard and Jan Mayen',
    "SZ":'Swaziland',
    "SE":'Sweden',
    "CH":'Switzerland',
    "ST":'S&atilde;o Tom&eacute; and Pr&iacute;ncipe',
    "TW":'Taiwan',
    "TJ":'Tajikistan',
    "TZ":'Tanzania',
    "TH":'Thailand',
    "TG":'Togo',
    "TK":'Tokelau',
    "TO":'Tonga',
    "TT":'Trinidad and Tobago',
    "TA":'Tristan da Cunha',
    "TN":'Tunisia',
    "TR":'Turkey',
    "TM":'Turkmenistan',
    "TC":'Turks and Caicos Islands',
    "TV":'Tuvalu',
    "UM":'U.S. Minor Outlying Islands',
    "PU":'U.S. Miscellaneous Pacific Islands',
    "VI":'U.S. Virgin Islands',
    "UG":'Uganda',
    "UA":'Ukraine',
    "AE":'United Arab Emirates',
    "GB":'United Kingdom',
    "US":['United States of America', 'United States'],
    "UY":'Uruguay',
    "UZ":'Uzbekistan',
    "VU":'Vanuatu',
    "VA":'Vatican City',
    "VE":'Venezuela',
    "VN":'Vietnam',
    "WK":'Wake Island',
    "WF":'Wallis and Futuna',
    "EH":'Western Sahara',
    "YE":'Yemen',
    "ZM":'Zambia',
    "AX":'&Aring;land Islands'
  };
  var stateNames = {
    "AK":'Alaska',
    "AL":'Alabama',
    "AP":'Armed Forces Pacific',
    "AR":'Arkansas',
    "AS":'American Samoa',
    "AZ":'Arizona',
    "CA":'California',
    "CO":'Colorado',
    "CT":'Connecticut',
    "DC":'District of Columbia',
    "DE":'Delaware',
    "FL":'Florida',
    "FM":'Federated States of Micronesia',
    "GA":'Georgia',
    "GU":'Guam',
    "HI":'Hawaii',
    "IA":'Iowa',
    "ID":'Idaho',
    "IL":'Illinois',
    "IN":'Indiana',
    "KS":'Kansas',
    "KY":'Kentucky',
    "LA":'Louisiana',
    "MA":'Massachusetts',
    "MD":'Maryland',
    "ME":'Maine',
    "MH":'Marshall Islands',
    "MI":'Michigan',
    "MN":'Minnesota',
    "MO":'Missouri',
    "MP":'Northern Mariana Islands',
    "MS":'Mississippi',
    "MT":'Montana',
    "NC":'North Carolina',
    "ND":'North Dakota',
    "NE":'Nebraska',
    "NH":'New Hampshire',
    "NJ":'New Jersey',
    "NM":'New Mexico',
    "NV":'Nevada',
    "NY":'New York',
    "OH":'Ohio',
    "OK":'Oklahoma',
    "OR":'Oregon',
    "PA":'Pennsylvania',
    "PR":'Puerto Rico',
    "PW":'Palau',
    "RI":'Rhode Island',
    "SC":'South Carolina',
    "SD":'South Dakota',
    "TN":'Tennessee',
    "TX":'Texas',
    "UT":'Utah',
    "VA":'Virginia',
    "VI":'Virgin Islands',
    "VT":'Vermont',
    "WA":'Washington',
    "WV":'West Virginia',
    "WI":'Wisconsin',
    "WY":'Wyoming'
  };
  var phoneCountries = {
    "1":"US",
    "43":"AT",
    "49":"DE",
    "32":"BE",
    "45":"DK",
    "358":"FI",
    "33":"FR",
    "353":"IE",
    "39":"IT",
    "31":"NL",
    "48":"PL",
    "351":"PT",
    "27":"ZA",
    "34":"ES",
    "46":"SE",
    "44":"GB"
  };

  var monthNames = {
    'short':'jan,feb,mar,apr,may,jun,jul,aug,sep,oct,nov,dec'.split(','),
    'long':'january,february,march,april,may,june,july,august,september,october,november,december'.split(',')
  };

  var domainsToSkipTriggeringEventsOn = /macys\.com|bathandbodyworks\.com/;

  var spaceAnywhereRegex = /[\s]+/g;

  var AbineFiller = function(document, domain, asyncFillingDomainRegex, caller) {
    this.document = document;
    this.domain = domain;
    this.asyncFilling = domain && domain.match(asyncFillingDomainRegex);
    this.caller = caller;
    this.injectFormFillingScript();
  };

  AbineFiller.prototype = {

    injectFormFillingScript: function(){
      var scriptToInject = AbineSimulateEvents.getAsyncFillString();
      var scriptTag = this.document.createElement("script");
      scriptTag.textContent = scriptToInject;
      (this.document.head||this.document.documentElement).appendChild(scriptTag);
      this.document.addEventListener('abineFilled', _.bind(this.fillFieldCallback, this), true, true);
      if (!this.document.getElementById("abineFillElement")) {
        // script injection failed because of CSP, let us run simulate script in extension scope.
        try {
          AbineSimulateEvents.asyncFillFunctions()
        } catch(e){}
      }
    },

    fillFieldCallback: function(){
      if(this.currentFillCB){
        this.currentFillCB();
      }
    },

    fillFieldAsync: function(value, cb){
      this.currentFillCB = cb;
      var fillEvent = this.document.createEvent("Event");
      fillEvent.initEvent("fill", true, true);
      var fillElement = this.document.getElementById("abineFillElement");
      fillElement.setAttribute("value", value);
      fillElement.dispatchEvent(fillEvent);
    },

    triggerFieldChange: function(field, value){
      this.currentFillCB = null;
      $(field.element).addClass("abineFillTarget").attr('blurFilled', 1);
      try {
        var fillEvent = this.document.createEvent("Event");
        fillEvent.initEvent("triggerChange", true, true);
        var fillElement = this.document.getElementById("abineFillElement");
        fillElement.setAttribute("value", value);
        fillElement.removeAttribute("skipPartial");
        try {fillElement.dispatchEvent(fillEvent);}catch(e){}
      } catch(e){}
      $('.abineFillTarget', field.element.ownerDocument).removeClass('abineFillTarget');
    },

    fillSelectFieldAsync: function(value, skipPartial, cb){
      this.currentFillCB = cb;
      var fillEvent = this.document.createEvent("Event");
      fillEvent.initEvent("fillSelect", true, true);
      var fillElement = this.document.getElementById("abineFillElement");
      fillElement.setAttribute("value", value);
      if (skipPartial) {
        fillElement.setAttribute("skipPartial", '1');
      } else {
        fillElement.removeAttribute("skipPartial");
      }
      fillElement.dispatchEvent(fillEvent);
    },

    fillTextField: function(element, value) {
      element.setAttribute('mmautofilling', 'now');
      $(element).val(value);
      try {
        this.triggerKeyboardEvent("keydown", element);
        this.triggerKeyboardEvent("keyup", element);
        this.triggerKeyboardEvent("keypress", element);
        this.triggerKeyboardEvent("change", element);
      } catch(e) {}
      try {
        // trigger event for modern forms (angularjs)
        var event = new Event('input', {
          'bubbles': true,
          'cancelable': true
        });
        element.dispatchEvent(event);
      } catch(e){
      }
      element.removeAttribute('mmautofilling', 'now');
    },

    triggerKeyboardEvent: function(type, el){
      var event = this.document.createEvent("KeyboardEvent");
      if (event.initKeyboardEvent) event.initKeyboardEvent(type, true, true, null, false, false, false, false, 9, 0);
      else if (event.initKeyEvent) event.initKeyEvent(type, true, true, null, false, false, false, false, 9, 0);
      el.dispatchEvent(event);
    },

    fillFields: function(formStrategy, type, currentFieldId, values, domain, origin, callback) {
      this.formStrategy = formStrategy;

      if (type == 'current') {
        this.fillCurrentFieldAsync(currentFieldId, values, domain, origin, callback);
      } else if (type == 'email') {
        this.fillEmailFieldsAsync(currentFieldId, values, domain, origin, callback);
      } else if (type == 'password') {
        this.fillPasswordFieldsAsync(currentFieldId, values, domain, origin, callback);
      } else if (type == 'login' || type == 'login-no-submit') {
        this.fillLoginFieldsAsync(currentFieldId, values, domain, origin, callback);
      } else if (type == 'phone') {
        this.fillPhoneFieldsAsync(currentFieldId, values, domain, origin, callback);
      } else if (type == 'address') {
        this.fillAddressFieldsAsync(currentFieldId, values, domain, origin, callback);
      } else if (type == 'payment') {
        this.fillPaymentFieldsAsync(currentFieldId, values, domain, origin, _.bind(function(){
          if (values.address && this.hasVisibleField('/contact/postaladdress')) {
            this.fillAddressFieldsAsync(currentFieldId, values.address, domain, origin, callback);
          } else {
            callback();
          }
        }, this));
      } else if (type == 'identity') {
        this.fillIdentityFieldsAsync(currentFieldId, values, domain, origin, callback);
      }
    },
    _elementInDocument: function(element) {
      var doc = element.ownerDocument;
      while (element = element.parentNode) {
        if (element == doc) {
          return true;
        }
      }
      return false;
    },

    hasVisibleField: function(dataType) {
      var fields = this.formStrategy.getFields();
      var self = this;
      var field = _.find(fields, function(field){
        if (field.dataType && (field.dataType == dataType || field.dataType.indexOf(dataType+"/") != -1)) {
          return self.isVisible(field.element);
        }
        return false;
      });
      return !!field;
    },

    reloadFieldElement: function(field) {
      try {
        if (!this._elementInDocument(field.element)) {
          var document = field.element.ownerDocument;
          var newElement = null;
          // input field was replaced. try to find the new element.
          if (field.id) {
            newElement = document.getElementById(field.id);
          }
          if (!newElement && field.name) {
            var els = $('input[name=' + field.name + ']', document);
            if (els.length == 1) {
              newElement = els[0];
            }
          }
          if (!newElement && field.className) {
            var classname = "." + field.className.replace(/(^[\s]+)|([\s]+$)/g, '').replace(/[\s]+/g, '.');
            if (field.fieldType == 'select') {
              els = $('select' + classname);
            } else {
              els = $('input' + classname);
            }
            if (els.length == 1) {
              newElement = els[0];
            }
          }
          if (newElement) {
            LOG_FINEST && ABINE_DNTME.log.finest("found new element for " + field.signature);
            field.element = newElement;
          }
        }
      } catch(e) {}
    },

    reloadFormIfRequired: function(fields) {
      var reloadRequired = false;
      var self = this;
      var reloadCheckRequired = _.find(fields, function(fld){
        return !self.isVisible(fld.element);
      });
      if (reloadCheckRequired) {
        var newFields = self.formStrategy.refreshOnFormChange(fields);
        if (newFields !== fields) {
          for(var i in fields) {
            delete fields[i];
          }
          for(var i in newFields) {
            fields[i] = newFields[i];
          }
        }
      }
      return fields;
    },


    fillFieldsValuesByType: function(currentFieldId, fields, type, values, panelFillData, exactTypeMatch, fillInvisible, skipPartial, cb) {

      // for comments on the use of the function and explanation of why .series works,
      // see method fillFieldsValuesByTypeAsync
      if ($.isArray(values)){
        var this_ = this;
        _.async.series(values.map(function(value){
          return function(callback){
            this_.fillFieldsByType(currentFieldId, fields, type, value, panelFillData, exactTypeMatch, fillInvisible, skipPartial, cb)
          }
        }), cb)
      } else {
        this.fillFieldsByType(currentFieldId, fields, type, values, panelFillData, exactTypeMatch, fillInvisible, skipPartial, cb)
      }

    },

    fillFieldsByType: function(currentFieldId, fields, type, value, panelFillData, exactTypeMatch, fillInvisible, skipPartial, callback) {

      var success = false;

      callback = callback || function(){};

      if (value === null || value == '' || typeof(value) == 'undefined') {
        callback(false);
        return false;
      }

      var fieldsOfType = this.getFieldsOfType(fields, currentFieldId, exactTypeMatch, type, fillInvisible);

      var oldLength = fieldsOfType.length;
      fieldsOfType = this.removedFieldsAlreadyFilled(fieldsOfType, value);

      if (oldLength != fieldsOfType.length) {
        // at least one field already has same value
        success = true;
      }

      if (fieldsOfType.length == 0) {
        LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:fillFieldsByType no fields to fill "+type);
        callback(success);
        return success;
      }

      _(fieldsOfType).each(_.bind(function(f) {
        f.element.setAttribute("mmautofilling", "now");
        $(f.element).removeData('abineManualTyped');
        f.oldVal = $(f.element).val();
        if (f.oldVal && f.oldVal != "" && f.fieldType != "select") {
          $(f.element).val("");
        }
      }, this));

      var triggerClickEvent = false;
      if (_(["uniqlo.com"]).contains(this.domain)) {
        triggerClickEvent = true;
      }

      var self = this;
      _(fieldsOfType).each(_.bind(function(f) {

        function postFill(f) {

          // send change event only if content changes and not on list
          var triggerEvents = f.fieldType != 'select' && !self.domain.match(domainsToSkipTriggeringEventsOn) && f.oldVal != $(f.element).val();

          // for auto-fill feedback
          $(f.element).data('mmFillType', type);

          if (triggerEvents) {
            self.triggerFieldChange(f, value);
            if (triggerClickEvent) {
              try { $(f.element).click(); } catch (e) {}
            }
          }

          delete f.oldVal;

          if(panelFillData){
            f.disposableGUID = panelFillData.disposableGUID;
            self.formStrategy.triggerPanelFillEvent({
              domain: panelFillData.domain,
              origin: panelFillData.origin,
              accountId: panelFillData.accountId,
              field: f
            });
          }
        }


        if(f.fieldType != 'select'){
          var domainsToSkipFocusOn = ["tdameritrade.com"];
          // focus only if not on list
          if(domainsToSkipFocusOn.indexOf(self.domain) == -1) {
            $(f.element).focus();
          }
        }

        // we never check or mark success in this case?
        if(type && type.match("/password") && f.element.type == "text"){
          try{
            $(f.element).val(value);
          } catch (e){}

          var formEl = f.element.form;

          setTimeout(_.bind(function(f){
            var element = $("[type='password']",formEl);
            if (element.length > 0) {
              var newField = _(fields).find(function(fld){return fld.element == element[0]});
              f = newField || f;
              try {element.focus();} catch(e){}
              $(element).val(value);
              self.markFieldFilled(f);
              postFill(f);
            } else {
              postFill(f);
            }
          }, self, f), 100);
        } else {
          value = value+"";

          if (f.fieldType != 'select') {
            $(f.element).val(value);
          } else {
            success = self.setSelectValue(f, value, skipPartial);
          }

          var filledValue;
          if (f.fieldType == "select") {
            filledValue = self.getSelectValue(f.element);
          } else {
            filledValue = $(f.element).val()
          }

          if (filledValue == value) {
            success = true;

            // special case where expiry-month starts from 0.
            if (f.fieldType == 'select' && type.match(/month/i)) {

              var selectOptionValues = self.getSelectValues(f.element);
              var lastValue = selectOptionValues[selectOptionValues.length - 1];

              if (parseInt(lastValue) == 11) {
                var targetValue = parseInt(value) - 1;
                self.setSelectValue(f, targetValue, skipPartial);
              }
            }

            if (f.maxlength && value.length > f.maxlength){
              success = false;
            }
          }

          self.markFieldFilled(f);
          postFill(f);
        }

      }, this));
      _(fieldsOfType).each(_.bind(function(f) {
        f.element.removeAttribute("mmautofilling");
      }, this));

      callback(success);

      return success;
    },

    markFieldFilled: function(f) {
      $(f.element).css({
        'color': '#0000aa',
        'background-color': '#ffffee'
      });
    },

    isVisible: function(node) {
      try {
        return $(node).is(":visible") && $(node).css("visibility") != 'hidden';
      } catch(e){}
      return true;
    },

    getMonthAlternates: function (month, format, fieldType) {
      var monthAlternates = [];
      var monthInt = parseInt(month);
      monthAlternates.push(monthNames.long[monthInt - 1]);
      monthAlternates.push(monthNames.short[monthInt - 1]);
      if (monthInt < 10) {
        monthAlternates.push('0'+monthInt);
        monthAlternates.push(''+monthInt);
      } else {
        monthAlternates.push(''+monthInt);
      }
      if(format === 'MM' && fieldType =='text')
        return monthInt<10 ? ['0'+monthInt] : [monthInt];
      return monthAlternates;
    },

    postFillAsync: function(f, type, value, panelFillData, skipPartial, success, cb){
      var self = this;

      // for auto-fill feedback
      $(f.element).data('mmFillType', type);

      self.markFieldFilled(f);

      var filledValue;
      if(f.fieldType == "select"){
        filledValue = self.getSelectValue(f.element);
      } else {
        filledValue = $(f.element).val()
      }

      // this isn't always true if they format as we type!
      // should probably try stripping nonalpha characters to compare
      if (success || filledValue == value) {
        success = true;
        // special case where expiry-month starts from 0.
        if (f.fieldType == 'select' && type.match(/month/i) && !isNaN(parseInt(value))) {

          var selectOptionValues = self.getSelectValues(f.element);
          var lastValue = selectOptionValues[selectOptionValues.length - 1];

          if(parseInt(lastValue) == 11){
            var targetValue = parseInt(value) - 1;
            self.setSelectValue(f, targetValue);
          }
        }

        if (f.maxlength && value.length > f.maxlength){
          success = false;
        }

      }

      delete f.oldVal;

      if (panelFillData) {
        f.disposableGUID = panelFillData.disposableGUID;
        self.formStrategy.triggerPanelFillEvent({
          domain: panelFillData.domain,
          origin: panelFillData.origin,
          accountId: panelFillData.accountId,
          field: f
        });
      }

      cb(success);
    },

    getSelectOptions: function(el) {
      var selectize = $(el).data("selectize");

      var options = [], opts;

      if(selectize){
        opts = selectize.options;
        for(var opt in opts){
          options.push({text:opts[opt].text, value: opts[opt].value});
        }
      } else {
        opts = el.options

        // convert from HTMLOptionsCollection to regular array
        for(var i=0;i<opts.length;i++){
          options.push({text:opts[i].text, value: opts[i].value});
        }
      }

      return options;
    },

    getSelectTextValues: function(el) {
      return _.map(this.getSelectOptions(el), function(opt) { return opt.text;});
    },

    getSelectValues: function(el) {
      return _.map(this.getSelectOptions(el), function(opt) { return opt.value});
    },

    getSelectValue: function(el){
      var selectize = $(el).data("selectize");

      if (selectize) {
        return selectize.getValue();
      } else {
        return $(el).val();
      }
    },

    setSelectValue: function(field, value, skipPartial) {
      var el = field.element;
      var changed = false, success = false;
      var lowerVal = (value||"").toString().toLowerCase().replace(spaceAnywhereRegex, '');
      var opts = el.getElementsByTagName("option");
      if (opts && opts.length > 0) {
        var partialMatchIndex = -1;
        for (var i = 0; i < opts.length; i++) {
          var optText = (opts[i].text || '').toLowerCase().replace(spaceAnywhereRegex, '');
          if (opts[i].getAttribute("value") == value || optText == lowerVal) {
            if (!opts[i].selected) {
              opts[i].selected = true;
              changed = true;
            }
            success = true;
            break;
          } else if (partialMatchIndex == -1 && optText.indexOf(lowerVal) != -1) {
            partialMatchIndex = i;
          }
        }
        if (!success && partialMatchIndex != -1 && !skipPartial) {
          if (!opts[partialMatchIndex].selected) {
            changed = true;
            opts[partialMatchIndex].selected = true;
          }
          success = true;
        }
      }


      if (changed) {
        this.triggerFieldChange(field, value);
      }

      return success;
    },

    fillFieldOfTypeAsync: function(field, type, value, panelFillData, skipPartial, cb){
      var self = this;
      if (field.fieldType == 'select') {
        $(field.element).addClass("abineFillTarget");
        this.fillSelectFieldAsync(value, skipPartial, function(){
          $('.abineFillTarget', field.element.ownerDocument).removeClass('abineFillTarget');
          var success = $(field.element).attr("abineFillResponse") == 'true';
          $(field.element).removeAttr("abineFillResponse");
          self.postFillAsync(field, type, value, panelFillData, skipPartial, success, cb);
        });
      } else {
        $(field.element).addClass("abineFillTarget");
        this.fillFieldAsync(value, function(){
          $('.abineFillTarget', field.element.ownerDocument).removeClass('abineFillTarget');
          var success = $(field.element).attr("abineFillResponse") == 'true';
          $(field.element).removeAttr("abineFillResponse");
          self.postFillAsync(field, type, value, panelFillData, skipPartial, success, cb);
        });
      }
    },

    // CLT-4906
    findAndFillRemovedFieldAsync: function(nextField, type, value, panelFillData, skipPartial, cb){
      var self = this, newEl;

      if(nextField.id && nextField.id != ""){
        newEl = self.document.getElementById(nextField.id);
      }

      if(!newEl && nextField.name && nextField.name != ""){
        var newEls = self.document.getElementsByName(nextField.name)

        if(newEls.length == 1){
          newEl = newEls[0];
        }
      }

      if(newEl){
        nextField.element = newEl;
        self.fillFieldOfTypeAsync(nextField, type, value, panelFillData, skipPartial, function(success){
          cb(success);
        });
      } else {
        cb(false);
      }
    },

    fillFieldsOfTypeAsync: function(fields, type, value, panelFillData, skipPartial, overallSuccess, cb){
      var self = this, nextField = null;

      if(fields.length == 0){
        cb(overallSuccess);
        return;
      }

      nextField = fields.shift();

      self.fillFieldOfTypeAsync(nextField, type, value, panelFillData, skipPartial, function(success){
        overallSuccess = overallSuccess || success;

        // seems like overkill to have to check this on every field..not sure of a way around it?
        var document = nextField.element.ownerDocument;
        if(!$.contains(document.documentElement, nextField.element)){
          self.findAndFillRemovedFieldAsync(nextField, type, value, panelFillData, skipPartial, function(success){
            overallSuccess = overallSuccess || success;
            self.fillFieldsOfTypeAsync(fields, type, value, panelFillData, skipPartial, overallSuccess, cb);
          });
        } else {
          setTimeout(function(){
            self.fillFieldsOfTypeAsync(fields, type, value, panelFillData, skipPartial, overallSuccess, cb);
          }, 100);
        }
      });
    },

    getFieldsOfType: function (fields, currentFieldId, exactTypeMatch, type, fillInvisible) {
      var self = this;
      var fieldsOfType = _(fields).filter(function (f) {
        self.reloadFieldElement(f);
        if (type == null && f.signature == currentFieldId) {
          return true;
        }
        if (f.signature != currentFieldId) {
          // not current field
          var $el = $(f.element);
          if ($el.data('abineManualTyped')) {
            if ($el.val() != '') {
              // do not fill fields that were manually filled by user
              return false;
            } else {
              $el.removeData('abineManualTyped');
            }
          }
        }
        return (f.dataType && ((!exactTypeMatch && f.dataType.indexOf(type) != -1) || (exactTypeMatch && f.dataType == type))) && (fillInvisible || self.isVisible(f.element));
      });
      return fieldsOfType;
    },

    removedFieldsAlreadyFilled: function (fieldsOfType, value) {
      var lowerVal = (value+"").toLowerCase().replace(spaceAnywhereRegex, '');
      return _(fieldsOfType).filter(function (f) {
        if (f.fieldType == 'select' && f.element.selectedIndex >= 0) {
          var fVal = ($(f.element).val() || "").toString();
          if (fVal == value || fVal.toLowerCase().replace(spaceAnywhereRegex, '') == lowerVal) return false;
          try {
            var fText = f.element.options[f.element.selectedIndex].text;
            if (fText == value || fText.toLowerCase().replace(spaceAnywhereRegex, '') == lowerVal) return false;
          } catch (e) {
          }
        }
        return true;
      });
    },

    // This method receives either a single value or a list of possible values to try to fill the field with
    // This is useful for the the cases in which we might need to try multiple values
    // (i.e United States, United States of America)
    fillFieldsValuesByTypeAsync: function(currentFieldId, fields, type, values, panelFillData, exactTypeMatch, fillInvisible, skipPartial, cb) {

      if ($.isArray(values)){
        // async.series chains together callback methods sequentially.
        // a little hack here relies on the fact that iff the first argument passed to the callback is non-null,the chain is broken.
        var this_ = this;
        _.async.series(values.map(function(value){
          return function(callback){
            this_.fillFieldsByTypeAsync(currentFieldId, fields, type, value, panelFillData, exactTypeMatch, fillInvisible, skipPartial, callback)
          }
        }), cb)
      } else {
        this.fillFieldsByTypeAsync(currentFieldId, fields, type, values, panelFillData, exactTypeMatch, fillInvisible, skipPartial, cb)
      }
    },

    fillFieldsByTypeAsync: function(currentFieldId, fields, type, value, panelFillData, exactTypeMatch, fillInvisible, skipPartial, cb) {
      if (!this.asyncFilling) {
        return this.fillFieldsByType(currentFieldId, fields, type, value, panelFillData, exactTypeMatch, fillInvisible, skipPartial, cb);
      }

      var self = this;
      var success = false;

      cb = cb || function(){};

      if (value === null || value == '' || typeof(value) == 'undefined') {
        cb();
        return success;
      }

      value = value+"";

      var fieldsOfType = this.getFieldsOfType(fields, currentFieldId, exactTypeMatch, type, fillInvisible);

      var oldLength = fieldsOfType.length;
      fieldsOfType = this.removedFieldsAlreadyFilled(fieldsOfType, value);

      if (oldLength != fieldsOfType.length) {
        // at least one field already has same value
        success = true;
      }

      if (fieldsOfType.length == 0) {
        LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:fillFieldsByType no matching fields to fill "+type);
        cb(success);
        return success;
      }

      _(fieldsOfType).each(_.bind(function(f) {
        f.element.setAttribute("mmautofilling", "now");
        $(f.element).removeData('abineManualTyped');
        f.oldVal = $(f.element).val();
        if(f.oldVal && f.oldVal != ""){
          $(f.element).val("");
        }
      }, this));

      var self = this;

      self.fillFieldsOfTypeAsync(_.clone(fieldsOfType), type, value, panelFillData, skipPartial, success, function(success){

        _(fieldsOfType).each(function(f) {
          f.element.removeAttribute("mmautofilling");
        });

        cb(success);

      });
    },

    fillCurrentFieldAsync: function(currentFieldId, value, domain, origin, cb) {
      var fields = this.formStrategy.getFields();
      var params = {domain: domain, origin: origin};
      cb = cb || function(){};
      if (origin && origin.origin) {
        params = origin;
        params.domain = domain;
      }
      this.fillFieldsByTypeAsync(currentFieldId, fields, null, value, params, false, null, null, cb);
    },

    fillEmailFieldsAsync: function(currentFieldId, email, domain, origin, cb) {
      var fields = this.formStrategy.getFields();
      var params = {domain: domain, origin: origin};
      cb = cb || function(){};
      if (origin && origin.origin) {
        params = origin;
        params.domain = domain;
      }
      this.fillFieldsByTypeAsync(currentFieldId, fields, '/contact/email', email, params, false, null, null, cb);
    },

    fillPasswordFieldsAsync: function(currentFieldId, password, domain, origin, cb) {
      var fields = this.formStrategy.getFields();
      var params = {domain: domain, origin: origin}, exactTypeMatch = false;
      cb = cb || function(){};
      if (origin && origin.origin) {
        params = origin;
        params.domain = domain;
        exactTypeMatch = params.exactTypeMatch;
      }
      this.fillFieldsByTypeAsync(currentFieldId, fields, '/password', password, params, exactTypeMatch, null, null, cb);
    },

    fillPhoneFieldsAsync: function(currentFieldId, phone, domain, origin, cb) {
      var fields = this.formStrategy.getFields();
      var params = {domain: domain, origin: origin};
      cb = cb || function(){};

      var currentField = fields[currentFieldId];
      var suffix = '';
      if (currentField && currentField.dataType && currentField.dataType.match(/work|home|mobile/) && currentField.dataType.match(/phone/)) {
        suffix = '/'+currentField.dataType.split('/').pop();
      }

      var country_code = phone.country_code;
      var number = phone.number;
      var areaCode, lastSeven, extensionField, exchange, local;
      if (parseInt(country_code) === 1) {
        areaCode = number.substr(0,3);
        exchange = number.substr(3, 3);
        local = number.substr(6, 4);
        lastSeven = exchange + "-" + local;
      } else {
        lastSeven = "+"+country_code+ number;
      }

      var self = this, countryFilled = false;

      _.async.series([
        function(cb){
          self.fillFieldsByTypeAsync(currentFieldId, fields, '/contact/phone/countrycode'+suffix, country_code, null, null, null, true, function(filled){
            if (filled) {
              countryFilled = true;
              cb();
            } else {
              self.fillFieldsByType(currentFieldId, fields, '/contact/phone/countrycode'+suffix, phoneCountries[country_code], null, null, null, true, function(filled) {
                if (filled) {
                  countryFilled = true;
                  cb();
                } else {
                  self.fillFieldsValuesByType(currentFieldId, fields, '/contact/phone/countrycode'+suffix, countryNames[phoneCountries[country_code]], null, null, null, false, function(filled){
                    if (filled) {
                      countryFilled = true;
                      cb();
                    } else {
                      self.fillFieldsByType(currentFieldId, fields, '/contact/phone/countrycode'+suffix, country_code, null, null, null, false, function(filled){
                        countryFilled = filled;
                        cb();
                      });
                    }
                  });
                }
              });
            }
          });
        },
        function(cb){
          // If European number and no country code autofilled, prefix number with a 0
          if (!countryFilled && (country_code[0] == "3" || country_code[0] == "4")) {
            number = "0" + number;
          }
          self.fillFieldsByTypeAsync(currentFieldId, fields, '/contact/phone/full', number, params, true, null, null, function(){cb()});
        },
        function(cb){
          self.fillFieldsByTypeAsync(currentFieldId, fields, '/contact/phone'+suffix, number, params, true, null, null, function(){cb()});
        },
        function(cb){
          self.fillFieldsByTypeAsync(currentFieldId, fields, '/contact/phone/number'+suffix, lastSeven, null, null, null, null, function(){cb()});
        },
        function(cb){
          self.fillFieldsByTypeAsync(currentFieldId, fields, '/contact/phone/areacode'+suffix, areaCode, null, null, null, null, function(){cb()});
        },
        function(cb){
          self.fillFieldsByTypeAsync(currentFieldId, fields, '/contact/phone/exchange'+suffix, exchange, null, null, null, null, function(){cb()});
        },
        function(cb){
          self.fillFieldsByTypeAsync(currentFieldId, fields, '/contact/phone/local'+suffix, local, null, null, null, null, function(){cb()});
        }
      ], function(){cb()});
    },

    fillBirthDateAsync: function(currentFieldId, fields, year, month, day, params, cb) {
      var monthAlternates = this.getMonthAlternates(month);
      var self = this;
      this.fillAlternatesAsync(currentFieldId, fields, '/birthdate/birthmonth', monthAlternates, false, true, function(){
        self.fillFieldsByTypeAsync(currentFieldId, fields, '/birthdate/birthyear', year, params, false, null, null, function(){
          self.fillFieldsByTypeAsync(currentFieldId, fields, '/birthdate/birthday', day, params, false, null, null, cb);
        });
      });
    },


    fillIdentityFieldsAsync: function(currentFieldId, identity, domain, origin, cb) {
      var fields = this.formStrategy.getFields();
      var params = {domain: domain, origin: origin};

      cb = cb || function(){};

      var self = this;
      _.async.series([
        function(cb) {
          var fullName = [];
          if (identity.first_name) fullName.push(identity.first_name);
          if (identity.middle_name) fullName.push(identity.middle_name);
          if (identity.last_name) fullName.push(identity.last_name);
          self.fillFieldsByTypeAsync(currentFieldId, fields, '/nameperson/full', fullName.join(' '), params, false, null, null, function(){cb()});
        },
        function(cb) {
          self.fillFieldsByTypeAsync(currentFieldId, fields, '/nameperson/first', identity.first_name, params, false, null, null, function(){cb()});
        },
        function(cb) {
          self.fillFieldsByTypeAsync(currentFieldId, fields, '/nameperson/middle', identity.middle_name, params, false, null, null, function(){cb()});
        },
        function(cb) {
          self.fillFieldsByTypeAsync(currentFieldId, fields, '/nameperson/last', identity.last_name, params, false, null, null, function(){cb()});
        },
        function(cb) {
          self.fillFieldsByTypeAsync(currentFieldId, fields, '/person/gender', identity.gender, params, false, null, null, function(){cb()});
        },
        function(cb) {
          self.fillFieldsByTypeAsync(currentFieldId, fields, '/nameperson/friendly', identity.username, params, false, null, null, function(){cb()});
        },
        function(cb) {
          self.fillFieldsByTypeAsync(currentFieldId, fields, '/url', identity.url, params, false, null, null, function(){cb()});
        },
        function(cb) {
          self.fillFieldsByTypeAsync(currentFieldId, fields, '/company/name', identity.company, params, false, null, null, function(){cb()});
        },
        function(cb) {
          self.fillFieldsByTypeAsync(currentFieldId, fields, '/company/position', identity.designation, params, false, null, null, function(){cb()});
        },
        function(cb) {
          self.fillFieldsByTypeAsync(currentFieldId, fields, '/socialSecurityNumber', identity.ssn, params, false, null, null, function(){cb()});
        },
        function(cb) {
          self.fillFieldsByTypeAsync(currentFieldId, fields, '/driverLicense', identity.ssn, params, false, null, null, function(){cb()});
        },
        function(cb) {
          if (identity.dob_year && identity.dob_day) {
           self.fillBirthDateAsync(currentFieldId, fields, identity.dob_year, identity.dob_month, identity.dob_day, params, function(){cb()});
          } else {
            cb();
          }
        },
        function(cb) {
          if (identity.address) {
            delete identity.address.first_name;
            delete identity.address.last_name;
            if (self.hasVisibleField('/contact/postaladdress')) {
              self.fillAddressFieldsAsync(currentFieldId, identity.address, domain, origin, function(){cb()});
            } else {
              cb();
            }
          } else {
            cb();
          }
        },
        function(cb) {
          if (identity.email) {
            self.fillEmailFieldsAsync(currentFieldId, identity.email, domain, origin, function(){cb()})
          } else if (identity.masked_email) {
            self.fillEmailFieldsAsync(currentFieldId, identity.masked_email, domain, "disposable", function(){cb()})
          } else {
            cb();
          }
        },
        function(cb) {
          if (identity.password) {
            self.fillPasswordFieldsAsync(currentFieldId, identity.password, domain, {origin: "strong", exactTypeMatch: true}, function(){cb()})
          } else {
            cb();
          }
        },
        function(cb) {
          if (identity.phone) {
            self.fillPhoneFieldsAsync(currentFieldId, identity.phone, domain, origin, function(){cb()})
          } else {
            cb();
          }
        },
        function(cb) {
          // show masked card panel
          var field = _.find(fields, function(fld){
            return fld.dataType == '/financial/creditcard/number' && $(fld.element).is(":visible");
          });
          if (field) {
            if (identity.card == 'mask') {
              setTimeout(function () {
                field.element.scrollIntoView();
                $(field.element).data('abine-params', {step: "new_card"});
                $(field.element).trigger('showpanel');
              }, 100);
            }
          }
          cb();
        }
      ], function(){cb()});
    },

    fillLoginFieldsAsync: function(currentFieldId, account, domain, origin, callback) {
      var fields = this.formStrategy.getFields();
      var params = {domain: domain, origin: origin, accountId: account.accountId};
      if (origin && origin.origin) {
        params = origin;
        params.domain = domain;
      }

      var currentField = fields[currentFieldId];
      var suffix = '';
      if (currentField && currentField.dataType && currentField.dataType.indexOf('/login') != -1) {
        suffix = '/login';
      }

      var cbWrap = function(result) {
        if (domain.match(/squarespace.com/)) {
          // squarespace login form does not show panel if filled by an account with email id.
          $('input[mmautofilling]', self.document).each(function(idx,el){
            el.removeAttribute('mmautofilling');
          });
        }
        callback(result);
      };

      var self = this;
      var fillInvisibleFields = (domain in {'myclubwyndham.com':1, 'identityiq.com':1});

      self.fillFieldsByTypeAsync(currentFieldId, fields, '/contact/email'+suffix, account.email, params, null, fillInvisibleFields, null, function(){
        self.fillFieldsByTypeAsync(currentFieldId, fields, '/nameperson/friendly', account.username, params, null, fillInvisibleFields, null, function(){
          if (params.origin != 'changepassword') {
            self.fillFieldsByTypeAsync(currentFieldId, fields, '/password'+suffix, account.password, params, true, fillInvisibleFields, null, cbWrap);
          } else {
            self.fillFieldsByTypeAsync(currentFieldId, fields, '/passwordold', account.password, params, true, null, null, cbWrap);
          }
        });
      });
    },

    fillCountryFieldsAsync: function(currentFieldId, country, domain, fields, suffix, params, cb){
      var self = this;

      var fillInvisibleCountry = true;
      //
      // if(_.contains(["godiva.com", "eyebuydirect.com"], domain)){
      //   fillInvisibleCountry = true;
      // }

      function postFill(){
        var countryWait = 1;
        if(_.contains(["ebay.com"], domain)){
          countryWait = 500;
        }
        if(_.contains(["sierratradingpost.com"], domain)){
          countryWait = 1500;
        }

        setTimeout(function(){
          cb();
        }, countryWait);
      }

      if (country && country in countryNames) {
        //fill without partial logic first
        self.fillFieldsValuesByTypeAsync(currentFieldId, fields, '/contact/country'+suffix, countryNames[country], params, false, fillInvisibleCountry, true, function(filled){
          if(filled){ postFill(); return; }
          self.fillFieldsByTypeAsync(currentFieldId, fields, '/contact/countrycode'+suffix, country, params, false, fillInvisibleCountry, true, function(filled){
            if (filled){ postFill(); return; }
            self.fillFieldsByTypeAsync(currentFieldId, fields, '/contact/country'+suffix, country, params, false, fillInvisibleCountry, true, function(filled){
              if (filled){ postFill(); return; }
              self.fillFieldsByType(currentFieldId, fields, '/contact/country'+suffix, country == "US"?"USA":null, params, false, fillInvisibleCountry, true, function(filled){
                if (filled){ postFill(); return; }
                self.fillFieldsValuesByTypeAsync(currentFieldId, fields, '/contact/country'+suffix, countryNames[country], params, false, fillInvisibleCountry, null, function(filled){
                  postFill();
                });
              });
            });
          });
        });
      } else {
        self.fillFieldsByTypeAsync(currentFieldId, fields, '/contact/country'+suffix, country, params, false, fillInvisibleCountry, null, function(filled){
          postFill();
        });
      }

    },

    fillAddressFieldsAsync: function(currentFieldId, address, domain, origin, cb) {
      var fields = this.formStrategy.getFields();
      var params = {domain: domain, origin: origin};
      var cb = cb || function(){};
      var suffix = '';
      var currentField = fields[currentFieldId];
      if (currentField && currentField.dataType) {
        var parts = currentField.dataType.split('/');
        if (parts.length > 3) {
          suffix = '/'+parts.pop();
          if (!suffix.match(/bill|ship|home|business/)) {
            suffix = '';
          } else {
            LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:fillAddress filling only "+suffix+" address fields");
          }
        }
        if (suffix == '' && currentField.multipleAddress) {
          // select only current address group fields
          var newFields = {};
          _.each(fields, function(fld){
            if ('addressGroup' in fld) {
              if (fld.addressGroup === currentField.addressGroup) {
                newFields[fld.signature] = fld;
              }
              return;
            }
            newFields[fld.signature] = fld;
          });
          fields = newFields;
          LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:fillAddress filling only group "+currentField.addressGroup+" address fields");
        }
      }
      if (currentField && 'addressGroup' in currentField) {
        // you are filling address fields
        _.each(fields, function(fld){
          if ('addressGroup' in fld && fld.addressGroup === currentField.addressGroup) {
            fld.addressFilled = true;  // do not refill when card/identity is filled
          }
        });
      } else {
        // you are filling card/identity fields. so exclude address fields already filled
        var newFields = {};
        _.each(fields, function(fld){
          if (!fld.addressFilled) {
            newFields[fld.signature] = fld;
          }
          if (!fld.addressFilled && 'addressGroup' in fld) {
            fld.addressFilled = true;  // do not refill when card/identity is filled
          }
        });
        fields = newFields;
      }

      var self = this;

      var visibleStateFields = this.getFieldsOfType(fields, currentFieldId, false, "/contact/state"+suffix, false);
      var fillInvisibleState = true;
      // if there is at least one visible state field, then dont fill invisible fields
      if (visibleStateFields.length > 0) {
        fillInvisibleState = false;
      }

      // if(_.contains(["hp.com", "ebay.com", "godiva.com", "eyebuydirect.com", "fossil.com", "barnesandnoble.com", "uniqlo.com", "miniinthebox.com", "westmarine.com", "disney.go.com", "newbalance.com", "okcupid.com"], domain)){
      //   fillInvisibleState = true;
      // }

      _.async.series([
        function(cb) {
          self.fillCountryFieldsAsync(currentFieldId, address.country, domain, fields, suffix, params, function(){cb()});
        },
        function(cb) {
          fields = self.formStrategy.refreshOnFormChange(fields);
          cb();
        },
        function(cb) {
          var fullName = [];
          if (address.first_name) fullName.push(address.first_name);
          if (address.last_name) fullName.push(address.last_name);
          self.fillFieldsByTypeAsync(currentFieldId, fields, '/nameperson/full', fullName.join(' '), params, false, null, null, function(){cb()});
        },
        function(cb) {
          self.fillFieldsByTypeAsync(currentFieldId, fields, '/nameperson/first', address.first_name, params, null, null, null, function(){cb()});
        },
        function(cb) {
          self.fillFieldsByTypeAsync(currentFieldId, fields, '/nameperson/last', address.last_name, params, null, null, null, function(){cb()});
        },
        function(cb) {
          self.fillFieldsByTypeAsync(currentFieldId, fields, '/contact/postaladdress'+suffix, address.address1, params, true, null, null, function(){cb();})
        },
        function(cb) {
          if (!suffix) {
            self.fillFieldsByTypeAsync(currentFieldId, fields, '/contact/postaladdress/', address.address1, params, false, null, null, function(){cb();})
          } else {
            cb();
          }
        },
        function(cb) {
          self.fillFieldsByTypeAsync(currentFieldId, fields, '/contact/postaladdressAdditional'+suffix, address.address2, params, null, null, null, function(){cb()});
        },
        function(cb) {
          self.fillFieldsByTypeAsync(currentFieldId, fields, '/contact/city'+suffix, address.city, params, null, null, null, function(){cb()});
        },
        function(cb) {
          var zipWait = 1;
          if(_.contains(["express.com"], domain)){
            zipWait = 1500;
          }
          setTimeout(cb, zipWait);
        },
        function(cb) {
          fields = self.formStrategy.refreshOnFormChange(fields);
          self.fillFieldsByTypeAsync(currentFieldId, fields, '/contact/postalcode' + suffix, address.zip, params, null, null, null, function(){cb()});
        },
        function(cb) {
          // delay filling STATE as some sites show STATE only after country selection
          var stateWait = 500;
          if(_.contains(["express.com"], domain)){
            stateWait = 1500;
          }
          setTimeout(cb, stateWait);
        },
        function(cb) {
          fields = self.formStrategy.refreshOnFormChange(fields);
          self.fillFieldsByTypeAsync(currentFieldId, fields, '/contact/state/code'+suffix, address.state, params, false, fillInvisibleState, null, function(filled) {
            if (filled) {
              cb();
              return;
            }
            if (address.state && address.state in stateNames) {
              self.fillFieldsByTypeAsync(currentFieldId, fields, '/contact/state'+suffix, stateNames[address.state], params, false, fillInvisibleState, false, function(filled){
                if (!filled) {
                  self.fillFieldsByTypeAsync(currentFieldId, fields, '/contact/state'+suffix, address.state, params, false, fillInvisibleState, true, function(){cb()});
                } else {
                  cb();
                }
              });
            } else {
              self.fillFieldsByTypeAsync(currentFieldId, fields, '/contact/state'+suffix, address.state, params, false, fillInvisibleState, null, function(){cb()});
            }
          });
        }
      ], function(){cb()});
    },

    fillIssuedToFieldsAsync: function(currentFieldId, fields, issuedTo, cb) {
      var self = this;

      if (issuedTo) {
        self.fillFieldsByTypeAsync(currentFieldId, fields, '/financial/creditcard/issuedto', issuedTo, null, true, null, null, function(){
          var parts = issuedTo.split(' ');
          self.fillFieldsByTypeAsync(currentFieldId, fields, '/financial/creditcard/issuedto/first', parts[0], null, null, null, null, function(){
            if (parts.length > 1) {
              self.fillFieldsByTypeAsync(currentFieldId, fields, '/financial/creditcard/issuedto/last', parts[1], null, null, null, null, function(){
                cb();
              });
            } else {
              cb();
            }
          });
        });
      } else {
        cb();
      }
    },

    fillAlternatesAsync: function(currentFieldId, fields, dataType, options, exactMatch, fillInvisible, cb){
      var self = this, nextOption = null;
      if(!options || options.length == 0){
        cb();
        return;
      }
      nextOption = options.shift();
      this.fillFieldsByTypeAsync(currentFieldId, fields, dataType, nextOption, null, exactMatch, fillInvisible, null, function(filled){
        if(filled){
          cb();
        } else {
          self.fillAlternatesAsync(currentFieldId, fields, dataType, options, exactMatch, fillInvisible, cb);
        }
      });
    },

    fillPaymentFieldsAsync: function(currentFieldId, card, domain, origin, cb) {
      cb = cb || function(){};
      var params = {domain: domain, origin: origin};

      var fields = this.formStrategy.getFields();

      var number = card.number;
      var type = card.type;
      var issuedTo = ((card.first_name||'') + ' ' + (card.last_name||'')).replace(/(^[\s]+)|([\s]+$)/g, '');
      var expiryMonth = (parseInt(card.expiry_month)<10?'0':'')+parseInt(card.expiry_month);
      var expiryYear = card.expiry_year;
      var verification = card.cvc;

      var self = this;

      var cardTypes = [type], isAmex = false;
      if (type.toLowerCase().indexOf('master') != -1) {
        cardTypes = ['master', 'master card', 'mastercard'];
      } else if (type.match(/amex|american/i)) {
        cardTypes = ['american_express', 'amex', 'american express'];
        isAmex = true;
      }
      // fill type first as changing type might clear other fields
      self.fillAlternatesAsync(currentFieldId, fields, '/financial/creditcard/type', cardTypes, null, null, function(){

        self.fillFieldsByTypeAsync(currentFieldId, fields, '/financial/creditcard/number', number, params, null, null, null, function(){
          self.fillFieldsByTypeAsync(currentFieldId, fields, '/financial/creditcard/number/part1', number.substr(0,4), params, null, null, null, function(){
            self.fillFieldsByTypeAsync(currentFieldId, fields, '/financial/creditcard/number/part2', number.substr(4,4), params, null, null, null, function(){
              self.fillFieldsByTypeAsync(currentFieldId, fields, '/financial/creditcard/number/part3', number.substr(8,4), params, null, null, null, function(){
                self.fillFieldsByTypeAsync(currentFieldId, fields, '/financial/creditcard/number/part4', number.substr(12,4), params, null, null, null, function(){
                  self.fillFieldsByTypeAsync(currentFieldId, fields, '/financial/creditcard/number/amex/part1', isAmex?number.substr(0,4):null, params, null, null, null, function(){
                    self.fillFieldsByTypeAsync(currentFieldId, fields, '/financial/creditcard/number/amex/part2', isAmex?number.substr(4,6):null, params, null, null, null, function(){
                      self.fillFieldsByTypeAsync(currentFieldId, fields, '/financial/creditcard/number/amex/part3', isAmex?number.substr(10,5):null, params, null, null, null, function(){
                        var expiryWait = 500;
                        if(_.contains(["jjill.com"], domain)){
                          expiryWait = 2500;
                        }
                        if(_.contains(["bestbuy.com"], domain)){
                          self.fillFieldsByTypeAsync(currentFieldId, fields, '/financial/creditcard/verification', verification, params, null, null, null, function(){});
                          expiryWait = 1000;
                        }
                        // delay other fields as filling NUMBER will change other fields on some sites (based on detected card type)
                        setTimeout(_.bind(function(){
                          fields = self.formStrategy.refreshOnFormChange(fields);
                          var expiryMonthFields = self.getFieldsOfType(fields, currentFieldId, false, '/financial/creditcard/expirymonth', true);
                          var monthAlternates = self.getMonthAlternates(expiryMonth, expiryMonthFields && expiryMonthFields[0] && expiryMonthFields[0].label, expiryMonthFields && expiryMonthFields[0] && expiryMonthFields[0].fieldType);
                          self.fillAlternatesAsync(currentFieldId, fields, '/financial/creditcard/expirymonth', monthAlternates, false, true, function(){

                            var twoDigitYear = (parseInt(expiryYear) % 100);

                            self.fillAlternatesAsync(currentFieldId, fields, '/financial/creditcard/expiryyear', [expiryYear, twoDigitYear], true, true, function(){
                              var yearOptions = [twoDigitYear];
                              var onlySelectFields = false;
                              _.each(fields, function(field){if(field){
                                if (field.dataType && field.dataType == '/financial/creditcard/expiryyear2') {
                                  if (field.fieldType == 'select') {
                                    onlySelectFields = true;
                                  } else {
                                    onlySelectFields = false;
                                  }
                                }
                              }});
                              if (onlySelectFields) {
                                // when its a dropdown, even two digit year field will have 4 digit value
                                yearOptions.unshift(expiryYear);
                              }
                              self.fillAlternatesAsync(currentFieldId, fields, '/financial/creditcard/expiryyear2', yearOptions, false, true, function(){

                                self.fillIssuedToFieldsAsync(currentFieldId, fields, issuedTo, function(){
                                  self.fillFieldsByTypeAsync(currentFieldId, fields, '/financial/creditcard/expiry', expiryMonth+"/"+expiryYear, null, true, null, null, function(){
                                    self.fillFieldsByTypeAsync(currentFieldId, fields, '/financial/creditcard/expiry2', expiryMonth+"/"+twoDigitYear, null, true, null, null, function(){
                                      if(_.contains(["target.com"], domain)) {
                                        // without this focus stays on number field and then cvv is cleared when you click out.
                                        $('#cvv').focus();
                                      }
                                      self.fillFieldsByTypeAsync(currentFieldId, fields, '/financial/creditcard/verification', verification, params, null, null, null, cb);
                                    });
                                  });
                                });
                              });
                            });
                          });
                        }, this), expiryWait);
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
    },

    autoLogin: function(formEl, loginUrls) {
      var loginUrl = loginUrls.length > 0 ? loginUrls.at(0) : null;
      var autoLoginSubmitBtn = loginUrl ? loginUrl.get('submit_btn') : "";
      var clicked = false;
      if (autoLoginSubmitBtn && autoLoginSubmitBtn.length > 0) {
        var submitButton = $(autoLoginSubmitBtn, formEl.ownerDocument).first();
        if (submitButton.length > 0) {
          LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:loginHelper:tryToAutoFillLogins submit button from mapping clicked");
          AbineSimulateEvents.simulate(submitButton[0], 'mousedown');
          AbineSimulateEvents.simulate(submitButton[0], 'click');
          clicked = true;
        }
      }
      if (!clicked) {
        var submitButton = $(':submit', formEl).first();
        if (submitButton.length > 0) {
          LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:loginHelper:tryToAutoFillLogins submit button clicked");
          AbineSimulateEvents.simulate(submitButton[0], 'mousedown');
          AbineSimulateEvents.simulate(submitButton[0], 'click');
        } else {
          LOG_FINEST && ABINE_DNTME.log.finest("formStrategy:loginHelper:tryToAutoFillLogins form submitted");
          $(formEl).submit();
        }
      }
    }
  };


  return AbineFiller;

}).apply(this, []);
;
  });
ABINE_DNTME.define("abine/proxies",
    ["documentcloud/underscore", "abine/core", 'abine/contentMessenger', 'abine/timer'],
  function (_, abineCore, abineContentMessenger, timer) {

  var contentMessenger;

  var Proxy = abineCore.BaseClass.extend({
    initialize: function(options){
      var proxy = this;
      _.each(options.methods, function(method){
        proxy.addMethod(method);
      });

      _.each(options.asyncMethods, function(method){
        proxy.addAsyncMethod(method);
      });

      this._require = options.require;
    },

    addMethod:function (method) {
      this[method] = function () {
        var callback = function () {}, params = arguments;
        if (arguments.length > 0 && typeof(arguments[arguments.length - 1]) == 'function') {
          callback = arguments[arguments.length - 1];
          params = Array.prototype.slice.call(arguments, 0, -1);
        } else {
          params = Array.prototype.slice.call(arguments, 0);
        }
        this.call({method:method, params:params}, callback);
      };
    },

    addAsyncMethod: function(method){
      this[method] = function () {
        var callback = function () {}, params = arguments;
        if (arguments.length > 0 && typeof(arguments[arguments.length - 1]) == 'function') {
          callback = arguments[arguments.length - 1];
          params = Array.prototype.slice.call(arguments, 0, -1);
        } else {
          params = Array.prototype.slice.call(arguments, 0);
        }
        this.callAsync({method:method, params:params}, callback);
      };
    },

    call: function(payload, callback) {
      var callId = ""+Math.random();
      payload.proxyId = this._proxyId;
      payload.callId = callId;
      payload.require = this._require;
      if (callback) {
        contentMessenger.once('background:stub:response:'+callId, function(response){
          callback(response);
        });
      }
      contentMessenger.send('contentManager:proxy:call', payload);
    },

    callAsync: function(payload, callback) {
      var callId = ""+Math.random();
      payload.proxyId = this._proxyId;
      payload.callId = callId;
      payload.require = this._require;
      if (callback) {
        contentMessenger.once('background:stub:response:'+callId, function(response){
          callback(response.err, response.data);
        });
      }
      contentMessenger.send('contentManager:proxy:callAsync', payload);
    }
  });

  function connect(contentMsgr, callback) {
    if (contentMessenger) return;
    callback = callback || function(){};
    contentMessenger = contentMsgr || ABINE_DNTME.contentMessenger;
    var callId = ""+Math.random();
    contentMessenger.once('background:stub:response:'+callId, function(response){
      callback(response);
    });
    contentMessenger.send('contentManager:proxy:connect', callId);
  }

  function disconnect() {
    if (!contentMessenger) return;
    contentMessenger = null;
  }

  var securityManager = new Proxy({
    require: 'abine/securityManager',
    methods: ['isLocked', 'login', 'isInitialized', 'openPage', 'openPostInstall', 'openLink', 'refreshData', 'refreshRules',
              'resetData', 'openWindow', 'isMaskmeInstalled', 'getBillingManager']
  });

  var webapp = new Proxy({
    require: 'abine/webapp',
    methods: [
      'openHome', 'openDashboard', 'openWallet', 'openMasking', 'openEmails', 'openPhones', 'openCards', 'openLogin', 'openMaskedEmailsIn30',
      'openPremium', 'openNewFeatureAdded', 'openSnowman', 'openBlacklist', 'openSettings', 'openStats', 'openHelp', 'openPasswordSetup',
      'openDnt2UpgradePage', 'openPiwikCampaign', 'openWeeklyReport', 'openDntAnywhere', 'openForgotPassword',
      'openFAQ', 'openPage', 'openCashForCardsFaq', 'openCashForCardsSignup', 'openUnderstandingTracking', 'openTour', 'openAccounts'
    ]
  });

  var licenseModule = new Proxy({
    require: 'abine/modules/license',
    methods: [],
    asyncMethods: ['sendFeedbackEmail','refreshEntitlements', 'refreshLastFourCardDigits',
        'register', 'login', 'subscribe', 'incrementMedalsEarned', 'incrementMedalsEarnedWithCurrentMedals',
        'retrieveMedalsEarned', 'logAction', 'logSplitData', 'logPasswordFillData',
        'getGenericBiometricKey', 'logCheckout'
    ]
  });

  var paymentsModule = new Proxy({
    require: 'abine/modules/payments',
    methods: [],
    asyncMethods: ['retrieveCardCredit', 'retrieveCardFee', 'verifyCardRequest']
  });

  var panelsModule = new Proxy({
    require: 'abine/modules/panels',
    methods: [],
    asyncMethods: ['getDNTMePanel', 'changePanelShowRate', 'getDNTMeGrowl']
  });

  var pageEventsModule = new Proxy({
    require: 'abine/pageEvents',
    asyncMethods: ['processAction'],
    methods: []
  });

  var mappingsModule = new Proxy({
    require: 'abine/modules/mappings',
    asyncMethods: ['makeDailyMappingCheck', 'getOneFormMappings', 'getFormRules'],
    methods: []
  });


  var memoryStore = new Proxy({
    require: 'abine/memoryStore',
    methods: ['set'],
    asyncMethods: ['get']
  });

  return {
    Proxy: Proxy, // used in tests only

    connect: connect,
    disconnect: disconnect,

    securityManager: securityManager,
    webapp: webapp,
    licenseModule: licenseModule,
    paymentsModule: paymentsModule,
    panelsModule: panelsModule,
    pageEventsModule: pageEventsModule,
    mappingsModule: mappingsModule,
    memoryStore: memoryStore
  };
});
ABINE_DNTME.define("abine/responseCodes",[],function(){

  return {
    // CORE
    UNKNOWN_ERROR: "unknown error",
    SUCCESS: "successful",
    ERROR: "error",
    SERVICE_UNAVAILABLE_ERROR: 'service unavailable error',
    SCHEDULED_DOWNTIME: 'scheduled downtime',
    MISSING_PARAMETERS: 'missing parameters',
    UNAUTHORIZED: "unauthorized bad token",
    NO_ACCESS_FOR_FEATURE: "no access to requested feature",

    // SECURITY MANAGER
    INVALID_PASSWORD: "invalid password",
    BIOMETRIC_AUTH_FAILED: 'biometric auth failed',

    // LICENSE
    ACCOUNT_ALREADY_EXISTS: "account already exists for that email",
    SAVE_NEW_USER_ERROR: "error saving new user",
    FAILED_CREATING_TARGET_EMAIL: "failed to create target email",
    FAILED_TO_UPDATE_USER: "error updating user",
    NO_ACCOUNT_FOUND_FOR_EMAIL: "no account found for email",
    ACCOUNT_MARKED_FOR_DELETION: "account has been marked for deletion",
    ACCOUNT_HAS_BEEN_LOCKED: "account has been locked",
    ACCOUNT_HAS_BEEN_BANNED: "account has been banned",
    WRONG_PASSWORD: "wrong password",
    INVALID_EMAIL_ADDRESS: "invalid email address",
    NO_RECURLY_ACCOUNT_FOUND: "no recurly account found",
    MFA_CODE_REQUIRED: "mfa code required",

    // SYNC
    DOWNGRADE_SCHEMA_NOT_ALLOWED: 'downgrade of schema_version is not allowed',
    SAVE_CLIENT_DATA_ERROR: "errors saving client data",
    DESTROY_CLIENT_DATA_ERROR: "errors destroying client data",
    NO_CLIENT_DATA_FOUND: "no client data found",
    USER_EXISTS_FOR_CLIENT_DATA_DELETION: "cannot destroy client data when user exists",
    UPGRADE_REQUIRED: "upgrade required",

    // PHONE
    PHONE_NOT_FOUND: "phone not found",
    NO_DISPOSABLE_PHONE: "no disposable phone",
    FAILED_TO_SAVE_PHONE: "failed to save phone",
    PHONE_ALREADY_VALIDATED: "phone already validated",
    NO_NUMBERS_AVAILABLE: "no numbers available",
    FAILED_TO_SEND_VERIFICATION_CODE: "failed to send verification code",
    WRONG_VERIFICATION_CODE: "wrong verification code",
    OLD_PHONE_NOT_FOUND: "old phone not found",
    COUNTRY_NOT_SUPPORTED: "country not supported",
    COUNTRY_CHANGE_NOT_SUPPORTED: "country change not supported",
    ERROR_CREATING_DISPOSABLE: "error creating disposable",

    MISSING_USER_PARAMS_ERROR_CODE: 'missing user params',
    NO_BILLING_ACCOUNT_FOR_USER_ERROR_CODE: 'no billing account for user',
    REQUEST_ALREADY_EXISTS_ERROR_CODE: 'request already exists',
    NO_CARDS_AVAILABLE_ERROR_CODE: 'no cards available',
    REQUESTED_AMOUNT_OVER_LIMIT_ERROR_CODE: 'requested amount over limit',
    REQUESTED_AMOUNT_BELOW_MINIMUM_ERROR_CODE: 'requested amount below minimum',
    REQUESTED_AMOUNT_INVALID_ERROR_CODE: 'requested amount invalid',
    BILLING_FAILED_ERROR_CODE: 'billing failed',
    BILLING_DECLINED_ERROR_CODE: 'billing declined',
    BILLING_COMPANY_FAILED_ERROR_CODE: 'billing company failed',
    NO_CARD_FOUND_ERROR_CODE: 'no card found',
    NO_BILLING_TRANSACTION_FOUND_ERROR_CODE: 'no billing transaction found',
    FRAUD_SOFT_LIMIT_REACHED: 'fraud soft limit',
    REFUND_FAILED_ERROR_CODE: 'refund failed',
    PROVIDER_CANCEL_FAILED_ERROR_CODE: 'provider cancel failed',
    PROVIDER_READ_FAILED_ERROR_CODE: 'provider read failed',
    PROVIDER_CARD_CREATION_FAILED_ERROR_CODE: 'provider card creation failed',
    FRAUD_HARD_LIMIT_REACHED: 'fraud hard limit reached',
    CARD_ALREADY_DEACTIVATED_ERROR_CODE: 'card already deactivated',
    FRAUD_VERIFIED_SOFT_LIMIT_REACHED: 'fraud verified soft limit',
    FRAUD_PLATFORM_UNAVAILABLE: 'fraud verification unavailable',
    FRAUD_HIGH_RISK_COUNTRY_ERROR_CODE: 'high risk country ban',
    BILLING_IN_NON_US_ERROR_CODE: 'billing in non us',
    GREY_LISTED_ERROR_CODE: 'grey listed user',
    GLOBAL_SOFT_LIMIT_REACHED: 'global soft limit',
    DUPLICATE_CARD_ERROR_CODE: 'duplicate card used ban',
    REJECTED_DOMAIN_ERROR_CODE: 'domains known to not accept masked cards',
    BANNED_DOMAIN_ERROR_CODE: 'banned domain',

    // MAPPING
    // never fails!

    //SPLIT TESTS
    SPLIT_INTRUSIVE_NON_INTRUSIVE: '7',
    SPLIT_INTRUSIVE_CONTROL: '8',
    SPLIT_INTRUSIVE_ALWAYS_REG: '9',
    SPLIT_INTRUSIVE_GOT_IT_REG: '10',
    SPLIT_INTRUSIVE_COMPLETE_SETUP_BUTTON: '11',

    SPLIT_WALLET_OFF: "12",
    SPLIT_WALLET_ON: "13",

    SPLIT_ICON_PANELS_CLICK_ICON_FOR_PANEL: '32',
    SPLIT_ICON_PANELS_SHOW_ICON_WITH_NORMAL_PANELS: '33',
    SPLIT_ICON_PANELS_NORMAL: '34'
  };
});

// url helper methods

ABINE_DNTME.define("abine/url", function() {
      var CompoundRealms = /^(((co|ac|go|ed|ga|mn|nj|nc|or|ne|in|gr|ca|on|pa|qc|bc|sh|at|va|us|il|tv|me|lg|at|ny|gv|com|edu|org|net|gov|mil|biz)\...)|(163\.com)|(kiev\.ua)|(gob\.pe)|(gen\.tr)|(waw.pl)|(jus.br))$/;

var HostingSites = /^(((go|typepad|tumblr|blogspot|squarespace|wordpress|myshopify|blogfa|blog|blogs|blinkweb|ning|posterous|over-blog|mihanblog|appspot|webs|weebly|onsugar|ucoz|uk|pch|ecwid)(\.com|\.in))|(nic\.in)|((ucoz|spb|narod)\.ru)|(blog\.163\.com)|((gouv|free)\.fr)|(uol\.com\.br)|(web\.id)|(gob\.mx)|(blogg\.se)|(home.pl))$/;

var urlRegex = /^http(?:s)?\:\/\/([^/\?\#]+)/im;
var ipAddressRegex = /^[0-9\.]+(:[0-9]+)?$/;
var localhostRegex = /^localhost(:[0-9]+)?$/;

// helper method to convert input string to proper case
function toProperCase(str) {
  if (!str) return str;
  return str.toLowerCase().replace(/\w+/g, function (s) {
    return s.charAt(0).toUpperCase() + s.substr(1);
  });
}

// extracts host name from a given url
function getHostname(url) {
  var parts = url.match(urlRegex);
  return parts && parts.length > 1 ? parts[1].toString() : url;
}

// extracts Top Level Domain from a given url
function getTLD(url) {
  var hostname = getHostname(url);

  if (hostname.match(ipAddressRegex) || hostname.match(localhostRegex)) {
    return hostname;
  }

  var parts = hostname.split(".");

  if (parts.length < 2) {
    return hostname;
  }

  var realm = parts.pop();
  var baseDomain = parts.pop();
  var domain = baseDomain + "." + realm;

  if (CompoundRealms.test(domain) && parts.length > 0) {
    realm = domain;
    baseDomain = parts.pop();
    domain = baseDomain + "." + realm;
  }

  // singup.wordpress.com and new.aol.com are common to all users.  they should not be treated as hosting sites
  if (HostingSites.test(domain) && parts.length > 0 && !hostname.match(/signup.wordpress.com|new.aol.com|(oauth|www).squarespace.com|secure.onsugar.com|www.tumblr.com/i)) {
    var subDomain = parts.pop();
    domain = subDomain + "." + domain;
  }
  return domain;
}

// converts a top level domain to a meaningful site name
function getSiteName(domain) {
  domain = getTLD(domain);

  if (domain.match(ipAddressRegex)) {
    return domain;
  }

  var parts = domain.split(".");

  if (parts.length > 2) {
    var realm = parts.pop();
    var baseDomain = parts.pop();
    var domain = baseDomain + "." + realm;

    if (CompoundRealms.test(domain) && parts.length > 0) {
      realm = domain;
      baseDomain = parts.pop();
      domain = baseDomain + "." + realm;
    }

    if (HostingSites.test(domain) && parts.length > 0) {
      var subDomain = parts.pop();
      return toProperCase(baseDomain + ' ' + subDomain);
    }

    return toProperCase(baseDomain);
  }

  return toProperCase(parts[0]);
}

var AbineURL = {
  getHostname: getHostname,
  getTLD: getTLD,
  getSiteName: getSiteName
};

      return AbineURL;
    });

// # abine/window
// API for anything that needs to open new tabs, windows, or place panels in the content script

ABINE_DNTME.define("abine/window",
    ["documentcloud/underscore", "abine/timer", "jquery"],
    function(_, abineTimer, $) {

  var PANEL_HTML = "panel.html";
  var emptyCallback = function(){};

  // Keeps trying to append the given `div` element to the given `doc` body element
  var appendPanelToBody = function(doc, div, iframe, callback) {
    if (doc.documentElement) {
      doc.documentElement.appendChild(div);
    } else {
      // for some reason the body is not available yet, try again later
      abineTimer.setTimeout(function() {
        appendPanelToBody(doc, div);
      }, 100);
    }
  };

  function getStyleAttribute(styles) {
    var str = [];
    _.each(styles, function(value, key){
      str.push(key.replace(/([A-Z])/g, function($1){return "-"+$1.toLowerCase();})+":"+value);
    });
    return str.join(" !important;");
  }

  function setAttributes(element, attrs) {
    var str = [];
    _.each(attrs, function(value, key){
      element.setAttribute(key, value);
    });
  }

  // Public API
  return {

    // ## Background Scripts

    // ### function createTab(options)
    // - `localUrl` => a local url such as 'test/spec_runner.html'
    // - `url` => a fully qualified url to the outside world
    //
    // Opens a new tab with the given url. Only works in background.
    // Will check whether a tab is already open to that URL and focus to it,
    // unless force = true.
    createTab: function(options, callback) {
      callback = callback || emptyCallback;

      var url = options.localUrl ? chrome.extension.getURL(options.localUrl) : options.url;
      if (options.force) {
        chrome.tabs.create({ active: true, url: url });
        callback();
        return;
      }

      var baseUrl = url.replace(/http[s]?/i,'').replace(/\?.*/i,'').replace(/\#.*/i,'');

      function queryCallback(testTabs) {
        var tab = _.find(testTabs, function(tab){return tab.url.indexOf(baseUrl) != -1;});
        if (!tab && !options.doNotOpenNewTab) {
          chrome.tabs.create({ active: true, url: url });
          callback();
        } else if (tab) {
          chrome.tabs.update(tab.id, {
            active: !(options.doNotActivate || false),
            url: url
          });
          // check if window is minimized
          chrome.windows.get(tab.windowId, function(window){
            if (window.state == 'minimized') {
              chrome.windows.update(window.id, {state: 'normal'});
            }
            chrome.windows.update(window.id, {focused: true});
            callback();
          });
        }
      }

      chrome.tabs.query({}, queryCallback);
    },

    getWebappTabs: function(callback) {
      chrome.tabs.query({url:  ABINE_DNTME.config.protocol + ABINE_DNTME.config.webAppHost+'/*'}, function(tabs){
        var tabIds = [];
        _.each(tabs, function(tab){tabIds.push(tab.id)});
        callback(tabIds);
      });
    },

    openWindow: function(url, width, height) {
      chrome.windows.create({url: url, width: width, height: height, focused: true, type: 'popup'});
    },

    reloadTab: function(options, callback) {
      callback = callback || emptyCallback;

      var url = options.localUrl ? chrome.extension.getURL(options.localUrl) : options.url;

      var baseUrl = url.replace(/http[s]?/i,'').replace(/\?.*/i,'').replace(/\#.*/i,'');

      function queryCallback(testTabs) {
        var tab = _.find(testTabs, function(tab){return tab.url.indexOf(baseUrl) != -1;});
        if (!tab && !options.doNotOpenNewTab) {
          chrome.tabs.create({ active: true, url: url });
          callback();
        } else if (tab) {
          chrome.tabs.reload(tab.id);
          if (!options.doNotActivate) {
            chrome.tabs.update(tab.id, {
              active: true
            });
            // check if window is minimized
            chrome.windows.get(tab.windowId, function(window){
              if (window.state == 'minimized') {
                chrome.windows.update(window.id, {state: 'normal'});
              }
              chrome.windows.update(window.id, {focused: true});
              callback();
            });
          }
        }
      }

      chrome.tabs.query({}, queryCallback);
    },

    // ## Content Scripts

    // ### function createPanel(options)
    // - `localUrl` => a local url such as 'test/spec_runner.html'
    // - `url` => a fully qualified url to the outside world
    // - `document` => a document to populate, defaults to the local document
    // - 'style' => a jquery-compatible object of css styles to apply to the panel div
    // - returns - the iframe element that was appended
    //
    // Creates a new floating panel in the document with an internal iframe holding the given url.
    createPanel: function(options) {
      var url = options.localUrl ? chrome.extension.getURL(options.localUrl) : options.url;
      var doc = options.document || document || window.document;
      var panelWidth = (options.panelWidth? options.panelWidth: 368);
      if (!doc) {
        throw("Error. abine/window createPanel called without a document context.");
      }

      var id = options.id || 'abine'+Math.floor(Math.random()*100000000);
      url = (!!url) ? url : chrome.extension.getURL(PANEL_HTML)+"?"+id;

      // Build the panel html
      var iframe = doc.createElement('iframe');
      var div = doc.createElement('div');
      div.className = options.className || "abineContentPanel";
      iframe.className = "abineContentFrame";

      setAttributes(div, {
        style: getStyleAttribute(_.extend({
          backgroundColor: "transparent",
          margin: "0",
          padding: "0",
          display: "none",
          opacity: "1",
          filter: "alpha(opacity:100)"
        }, options.style))
      });

      setAttributes(iframe, {
        width: panelWidth + "px",
        allowTransparency: "true",
        frameborder: 0,
        height: options.panelHeight + "px",
        scrolling: "no",
        src: url,
        id: id,
        style: getStyleAttribute(_.extend({
            position: "relative",
            display: "block",
            background: "transparent",
            borderWidth: "0px",
            left: "0px",
            top: "0px",
            visibility: "visible",
            opacity: 1,
            filter: "alpha(opacity:100)",
            margin: '0',
            padding: '0',
            height: options.panelHeight + "px",
            width: panelWidth + "px"
          })
        )
      });

      div.appendChild(iframe);

      appendPanelToBody(doc, div, iframe);
      return iframe;
    },

    autoResizePanels: function(options){
      var doc = options.document || document || window.document;
      var body = doc.getElementsByTagName('body')[0];
      var panels;
      if (options.frameId) {
        var frame = doc.getElementById(options.frameId);
        if (frame) {
          panels = [frame.parentElement];
        }
      }
      if (!panels) {
        panels = doc.getElementsByClassName(options.className || 'abineContentPanel');
      }
      _(panels).each(function(el) {
        var iframe = $("iframe",el)[0];
        iframe.scrolling = "no";

        $(iframe).height('auto');

        var width = $(iframe).width();
        var height = options.documentHeight;

        if (options.extraWidth) width += options.extraWidth;
        if (options.extraHeight) height += options.extraHeight;

        $(iframe).width(width);
        $(iframe).height(height);

        el.style.width = width+"px";
        el.style.height = height+"px";

        // adjust drop-down LEFT to make it appear within viewport.
        try {
          var elementLeft = parseInt(el.style.left);
          var elementRight = elementLeft + el.offsetWidth;
          var frameWidth = el.ownerDocument.defaultView.innerWidth;
          var screenRight = el.ownerDocument.documentElement.scrollLeft + frameWidth;
          if (elementRight > screenRight) {
            var diff = elementRight-screenRight;
            if (elementLeft-diff > 0) {
              el.style.left = elementLeft-diff+'px';
            } else {
              el.style.left = '0px';
              if (frameWidth < (elementRight-elementLeft)) {
                // no enough space, zoom out to fit helper in available space.
                var zoom = Math.round((frameWidth/(elementRight-elementLeft))*100);
                zoom -= 1; // for border
                if (zoom < 80) zoom = 80; // < 80% will not be legible.
                iframe.style.zoom = zoom+"%";

                // TODO: send zoom level back to panel
                iframe.contentDocument.body.style.zoom = zoom+"%";
              }
            }
          }
        } catch(e){}

      });
    },

    // ### function removePanels(options)
    // - `document` - a document to purge, defaults to the local document
    //
    // Removes all iframe panels from the given document
    removePanels: function(options) {
      var doc = options.document || document || window.document;
      function removeFrame(div, iframe) {
        if (!iframe) {
          var frame$ = $('iframe', div);
          if (frame$.length > 0) {
            iframe = frame$[0];
          }
          if (!iframe) {
            return;
          }
        }
        if (iframe.id.indexOf('doNotRemove') != -1) {
          $(iframe.parentNode).hide();
        } else {
          iframe.parentNode.parentNode.removeChild(iframe.parentNode);
        }
      }
      if (options.iframe) {
        removeFrame(null, options.iframe);
        return
      }
      var panels = doc.getElementsByClassName('abineContentPanel');
      _(panels).each(function(el) {
        removeFrame(el);
      });
    }

  };

});



// Content Messenger
// Object in content scripts that can communicate with the background
// Sends messages in the same format as Backbone events.
// Each message has a
// - name
// - payload

ABINE_DNTME.define('abine/contentMessenger',
    ['documentcloud/underscore', 'abine/core'], 
    function(_, abineCore) {

  // Create a listener for all requests
  var onRequestListener = function onRequestListener(request, sender) {
    if (request.eventName == 'FWD') {
      // from content script
      if (request.payload.to == ABINE_DNTME.context) {
        this.trigger(request.payload.eventName, request.payload.payload, request.payload.from);
      }
    } else {
      this.trigger(request.eventName, request.payload);
    }
  };
  
  var destroyed = function() {
    throw("Error. Attempt to call destroyed ContentMessenger.");
  };
  
  var ContentMessenger = abineCore.BaseClass.extend({
    
    initialize: function() {
      this.listener = _.bind(onRequestListener, this);
      chrome.extension.onRequest.addListener(this.listener);
    },
    
    sendToCS: function(to, eventName, payload) {
      if (chrome && chrome.extension) {
        chrome.extension.sendRequest({
          eventName: 'FWD',
          payload: {to: to, from: ABINE_DNTME.context, eventName: eventName, payload: typeof(payload) === 'undefined'?{}:payload}
        });
      }
    },

    // ### function on(eventName, callback, binding)
    // Starts listening to events from the background script
    // Defined on parent
    
    // ### function off(eventName, callback)
    // Stops listening to events from the background script
    // Defined on parent
    
    // ### function trigger(eventName, payload)
    // Triggers all of the handlers
    // Defined on parent
    
    // ### function send()
    // - eventName - name of the event
    // - payload - JSON serializable object
    // Sends a message to the background script
    // Returns the response if there is one
    send: function(eventName, payload) {
      this.trigger(eventName, payload);
      if (chrome && chrome.extension) {
        chrome.extension.sendRequest({
          eventName: eventName,
          payload: typeof(payload) === 'undefined'?{}:payload
        });
      }
    },
    
    // ### function destroy()
    // Destroy this messenger. Cleans up all listeners.
    destroy: function() {
      this.off();
      try {chrome.extension.onRequest.removeListener(this.listener);} catch(e){}
      this.on = this.off = this.trigger = destroyed;
    }
  });
  
  // Public
  return {
    
    // ### class ContentMessenger
    ContentMessenger: ContentMessenger
  
  }
  
});
  

// # abine/assets
// API for getting resources such as images in the content script

ABINE_DNTME.define("abine/assets",
  ['wycats/handlebars'],
  function(Handlebars) {

  // ## Content Scripts

  // ### function assetUrl(localUrl)
  // Return a url that works in the content-script to access images and css files, etc.
  function assetUrl(localUrl) {
    return chrome.extension.getURL(localUrl);
  }

  function cardAssetUrl(localUrlStart, type, localUrlEnd) {
    var url = localUrlStart + type + localUrlEnd;
    return chrome.extension.getURL(url);
  }

  Handlebars.registerHelper('assetUrl', assetUrl);
  Handlebars.registerHelper('cardAssetUrl', cardAssetUrl);

  // Public API
  return {
    assetUrl: assetUrl,
    cardAssetUrl: cardAssetUrl
  };

});
ABINE_DNTME.define('abine/browserSyncStorage',
  ["documentcloud/underscore","abine/core"],
  function(_, abineCore){

    var emptyCallback = function(){};

    var BrowserSyncStorage;

    // doesn't work in teamcity because they are run in webkit and not chrome.
    if(!chrome || !chrome.storage || !chrome.storage.sync){
      BrowserSyncStorage = abineCore.BaseClass.extend({
        initialize: function(options){
          this.storage = {};
        },

        set: function(items, callback){
          _.extend(this.storage,items);
          if(callback){callback();}
        },

        remove: function(keys, callback){
          keys = typeof(keys) === "string" ? [keys] : keys;
          _.each(keys, _.bind(function(key){
            delete this.storage[key];
          },this));
          if(callback){callback();}
        },

        get: function(keys, callback){
          keys = typeof(keys) === "string" ? [keys] : keys;
          if(callback){callback(_.pick(this.storage, keys));}
        },

        clear: function(callback){
          this.storage = {};
          if(callback){callback();}
        }
      });
    } else {

      BrowserSyncStorage = abineCore.BaseClass.extend({
        initialize: function(options){
        },

        set: function(items, callback){
          chrome.storage.sync.set(items,callback);
        },

        remove: function(keys, callback){
          chrome.storage.sync.remove(keys,callback);
        },

        get: function(keys, callback){
          chrome.storage.sync.get(keys, callback);
        },

        clear: function(callback){
          chrome.storage.sync.clear(callback);
        }
      });
    }

    var browserSyncStorage = new BrowserSyncStorage({});
    return browserSyncStorage;
  });

ABINE_DNTME.define('documentcloud/backbone', ['documentcloud/underscore', 'jquery'], function(_, $){
  return Backbone;
});
ABINE_DNTME.define('wycats/handlebars',
    [], 
    function() {


      // lib/handlebars/base.js
      var Handlebars = {};

      Handlebars.VERSION = "1.0.beta.6";

      Handlebars.helpers  = {};
      Handlebars.partials = {};

      Handlebars.registerHelper = function(name, fn, inverse) {
        if(inverse) { fn.not = inverse; }
        this.helpers[name] = fn;
      };

      Handlebars.registerPartial = function(name, str) {
        this.partials[name] = str;
      };

      Handlebars.registerHelper('helperMissing', function(arg) {
        if(arguments.length === 2) {
          return undefined;
        } else {
          throw new Error("Could not find property '" + arg + "'");
        }
      });

      var toString = Object.prototype.toString, functionType = "[object Function]";

      Handlebars.registerHelper('blockHelperMissing', function(context, options) {
        var inverse = options.inverse || function() {}, fn = options.fn;


        var ret = "";
        var type = toString.call(context);

        if(type === functionType) { context = context.call(this); }

        if(context === true) {
          return fn(this);
        } else if(context === false || context == null) {
          return inverse(this);
        } else if(type === "[object Array]") {
          if(context.length > 0) {
            for(var i=0, j=context.length; i<j; i++) {
              ret = ret + fn(context[i]);
            }
          } else {
            ret = inverse(this);
          }
          return ret;
        } else {
          return fn(context);
        }
      });

      Handlebars.registerHelper('each', function(context, options) {
        var fn = options.fn, inverse = options.inverse;
        var ret = "";

        if(context && context.length > 0) {
          for(var i=0, j=context.length; i<j; i++) {
            ret = ret + fn(context[i]);
          }
        } else {
          ret = inverse(this);
        }
        return ret;
      });

      Handlebars.registerHelper('if', function(context, options) {
        var type = toString.call(context);
        if(type === functionType) { context = context.call(this); }

        if(!context || Handlebars.Utils.isEmpty(context)) {
          return options.inverse(this);
        } else {
          return options.fn(this);
        }
      });

      Handlebars.registerHelper('unless', function(context, options) {
        var fn = options.fn, inverse = options.inverse;
        options.fn = inverse;
        options.inverse = fn;

        return Handlebars.helpers['if'].call(this, context, options);
      });

      Handlebars.registerHelper('with', function(context, options) {
        return options.fn(context);
      });

      Handlebars.registerHelper('log', function(context) {
        Handlebars.log(context);
      });
      ;
      // lib/handlebars/utils.js
      Handlebars.Exception = function(message) {
        var tmp = Error.prototype.constructor.apply(this, arguments);

        for (var p in tmp) {
          if (tmp.hasOwnProperty(p)) { this[p] = tmp[p]; }
        }

        this.message = tmp.message;
      };
      Handlebars.Exception.prototype = new Error;

      // Build out our basic SafeString type
      Handlebars.SafeString = function(string) {
        this.string = string;
      };
      Handlebars.SafeString.prototype.toString = function() {
        return this.string.toString();
      };

      (function() {
        var escape = {
          "<": "&lt;",
          ">": "&gt;",
          '"': "&quot;",
          "'": "&#x27;",
          "`": "&#x60;"
        };

        var badChars = /&(?!\w+;)|[<>"'`]/g;
        var possible = /[&<>"'`]/;

        var escapeChar = function(chr) {
          return escape[chr] || "&amp;";
        };

        Handlebars.Utils = {
          escapeExpression: function(string) {
            // don't escape SafeStrings, since they're already safe
            if (string instanceof Handlebars.SafeString) {
              return string.toString();
            } else if (string == null || string === false) {
              return "";
            }

            if(!possible.test(string)) { return string; }
            return string.replace(badChars, escapeChar);
          },

          isEmpty: function(value) {
            if (typeof value === "undefined") {
              return true;
            } else if (value === null) {
              return true;
            } else if (value === false) {
              return true;
            } else if(Object.prototype.toString.call(value) === "[object Array]" && value.length === 0) {
              return true;
            } else {
              return false;
            }
          }
        };
      })();;
      // lib/handlebars/runtime.js
      Handlebars.VM = {
        template: function(templateSpec) {
          // Just add water
          var container = {
            escapeExpression: Handlebars.Utils.escapeExpression,
            invokePartial: Handlebars.VM.invokePartial,
            programs: [],
            program: function(i, fn, data) {
              var programWrapper = this.programs[i];
              if(data) {
                return Handlebars.VM.program(fn, data);
              } else if(programWrapper) {
                return programWrapper;
              } else {
                programWrapper = this.programs[i] = Handlebars.VM.program(fn);
                return programWrapper;
              }
            },
            programWithDepth: Handlebars.VM.programWithDepth,
            noop: Handlebars.VM.noop
          };

          return function(context, options) {
            options = options || {};
            return templateSpec.call(container, Handlebars, context, options.helpers, options.partials, options.data);
          };
        },

        programWithDepth: function(fn, data, $depth) {
          var args = Array.prototype.slice.call(arguments, 2);

          return function(context, options) {
            options = options || {};

            return fn.apply(this, [context, options.data || data].concat(args));
          };
        },
        program: function(fn, data) {
          return function(context, options) {
            options = options || {};

            return fn(context, options.data || data);
          };
        },
        noop: function() { return ""; },
        invokePartial: function(partial, name, context, helpers, partials, data) {
          options = { helpers: helpers, partials: partials, data: data };

          if(partial === undefined) {
            throw new Handlebars.Exception("The partial " + name + " could not be found");
          } else if(partial instanceof Function) {
            return partial(context, options);
          } else if (!Handlebars.compile) {
            throw new Handlebars.Exception("The partial " + name + " could not be compiled when running in runtime-only mode");
          } else {
            partials[name] = Handlebars.compile(partial);
            return partials[name](context, options);
          }
        }
      };

      Handlebars.template = Handlebars.VM.template;
      ;



return Handlebars

});

/**
# Duckbone Core

This file is loaded first and just establishes some namespaces and useful helpers.
*/


ABINE_DNTME.define('duckbone/core',
  ['documentcloud/underscore', 'documentcloud/backbone', 'wycats/handlebars'], 
  function(_, Backbone, Handlebars) {

  // Establish top level namespaces
  var Duckbone = {};
  Duckbone.helpers = Duckbone.helpers || {};
  Duckbone.forms = Duckbone.forms || {};
  Duckbone.TemplatesData = Duckbone.TemplatesData || {};
  Duckbone.PartialsData = Duckbone.PartialsData || {};

  // The top level Duckbone object dispatches some events
  _.extend(Duckbone, Backbone.Events);

  _.extend(Duckbone, {

    // #### function serverError
    // - message - error message text
    // - returns - nothing
    //
    // Defines default behavior for 500 errors. Log error in development mode.
    // Redirect to 500 page in production.
    serverError: function(message) {
      _.log("500 Server Error");
      if (message) _.log(message);
    },

    // #### function serverError
    // - message - error message text
    // - returns - nothing
    //
    // Default behavior for 404 errors.
    // Log error in development mode.
    // Redirect to 404 page in production.
    fileNotFound: function(message) {
      _.log("404 File Not Found");
      if (message) _.log(message);
    },

    // #### function register
    // - f - a function to register
    // - single - a boolean determining whether it should be single use, defaults to false
    // - returns - the string name of the registered function
    //
    // This util is used to simplify communication with a Flash ExternalInterface.
    // JavaScript callback functions can only be passed as string names,
    // and not as function references. This makes some things really hard,
    // and prohibits the use of anonymous functions.
    // This method wraps up a function and returns its string reference.
    register: function(f, single) {
      single = single || false;
      var funcName = _.uniqueId('regfunc');
      if (_.isUndefined(Duckbone.registeredFunctions)) {
        Duckbone.registeredFunctions = {};
      }
      if (single) {
        f = _.wrap(f, function(func) {
          func(arguments);
          delete Duckbone.registeredFunctions[funcName]
        });
      }
      Duckbone.registeredFunctions[funcName] = f;
      return "Duckbone.registeredFunctions." + funcName
    },

    // #### function wrapMethods
    // - object - the object to wrap methods on, ie a view
    // - wrapper - the wrapper function
    // - the rest of the args can be strings of method names,
    //   or a single array of the method name strings
    // - returns - nothing
    //
    // Wraps all of the given object methods in another function wrapper.
    // In the wrapper, "this" is defined as the original object.
    // For example, it's useful for wrapping all of a
    // Backbone view's event callbacks in a common wrapper.
    wrapMethods: function(object, wrapper) {
      var methods = Array.prototype.slice.call(arguments, 2);
      if (_.isArray(methods[0])) {
        methods = methods[0];
      }
      _.each(methods, function(method) {
        var wrappedFunction = object[method];
        object[method] = function() {
          var args = [wrappedFunction].concat(Array.prototype.slice.call(arguments));
          return wrapper.apply(object, args);
        };
      });
    },

    // #### function log
    // - msg - log message
    // - returns - nothing
    //
    // Suppress logs if console.log is undefined.
    // Suppress logs in production environments.
    log: function(msg) {
      if (window.console && console.log) console.log(msg);
    },

    // #### function include
    // - obj - the object to extend
    // - remaining arguments are objects from which to mix in properties and methods
    // - returns - nothing
    //
    // This is the core method used to add Duckbone mixins to new classes.
    // It extends a given object with all the properties in the passed-in object(s).
    // It works just like _.extend except that it throws an exception if you pass an undefined object,
    // ie. if you spell a mixin wrong, which you will.
    // Also calls included() on the mixin after mixing in the methods, which
    // can provide additional side-effects.
    include: function(obj) {
      if (typeof obj == "undefined") {
        throw("Attempt to include into undefined object.")
      }
      _.each(_.toArray(arguments).slice(1), function(source) {
        if (typeof source == 'undefined') {
          throw("Attempt to include undefined object.");
        }
        for (var prop in source) {
          if (prop != 'included' && source[prop] !== void 0) {
            if (!obj.hasOwnProperty(prop)) obj[prop] = source[prop];
          }
        }
        if (typeof source['included'] == 'function') source['included'].call(obj);
      });
      return obj;
    }
  }); // end _.extend

  _.extend(Duckbone.helpers, {

    dateToPrettyTimeAgo: function (date) {
      var min = Math.floor((new Date().getTime() - date.getTime()) / 60000);
      if (min < 0) min = 0; // otherwise you will get "undefined ago" if input time is less than current time
      var remainder = min || 0, prettyTime, days, hours;
      if (remainder === 0) {
        return "just now";
      }
      if (remainder >= 1440) {
        days = Math.floor(remainder / 1440);
        remainder -= days * 1440;
        prettyTime = days;
        prettyTime += (days > 1) ? " days" : " day"
      } else if (remainder >= 60) {
        hours = Math.floor(remainder / 60);
        remainder -= hours * 60;
        prettyTime = hours;
        prettyTime += (hours > 1) ? " hours" : " hour";
      } else if (remainder > 0) {
        prettyTime = remainder;
        prettyTime += (remainder > 1) ? " minutes" : " minute"
      }
      return prettyTime + " ago";
    }

  });

  // Mix in log to underscore
  _.mixin({
    log: Duckbone.log
  });
  
  // Source of a major error in handlebars. FIIIIIXXXXX
  // window.Duckbone = Duckbone;
  // Handlebars.setDuckbone(Duckbone);
  
  return Duckbone;

});
/**

This file contains two model mixins, Duckbone.AssociableModel, and Duckbone.ModelHelpers.
Typically both will be mixed into an application's base model class. Adding them directly
to Backbone.Model will also work, but is perhaps less tidy.

# Duckbone.AssociableModel

Backbone deliberately provides the flexibility for the developer to manage associated models and
collections in any way that makes sense for the application. This flexibility is the
cause of much confusion among beginning developers, and so Duckbone provides this
straightforward framework for managing Rails-like associations.

The simple philosphy behind AssociableModel is that nested objects within JSON data
(or model attributes) are transformed into fully fledged Backbone Models or Collections.
These exists as properties on the original model, in an analagous way to
`has_one` and `has_many` relationships in Rails.

In addition, further changes to those nested attributes result in changes to the associations.
But this relationship is a one way street. Once the attribute data is used to create associated
models, it is removed from the attributes. So changes to associated models are never reflected
in the attributes of the original model.

There is a clear logic behind this approach:

- It is almost always more efficient to serve serialized associations together
  in a single large JSON response.
- Saving back to the server is best done one model at a time. Other strategies involving
  `accepts_nested_attributes_for` or other ad hoc solutions are inefficient and painful.
- The Duckbone templating system does not require associations to be serialized again
  into a monolithic object, as many other templating engines do. Handlebars simply walks
  the path from one model to another as desired.
- Packing models with associations back into monolithic objects is highly prone to problematic
  circular references, and so this is simply avoided.

## Usage

AssociableModel can be mixed into all models and collections that require it,
or extend Backbone.Model and Backbone.Collection themselves if desired.

The `hasOne` and `hasMany` methods take objects from the model's attributes and
move them into a newly created model. This associated model becomes a property on the original
model object, and its name is transformed from rails_underscores to camelCase.

In almost all cases, these functions will be called from the model's initialize method.

### Syntax

Several associations can be created in each call to hasOne or hasMany. A single object is passed.
Each key in the object defines the association name.

In a call to `hasOne`, the `model` key indicates the class of the associated model.
In a call to `hasMany`, the `collection` key indicates the class of the associated collection.

If a reciprocal link is desired on the association, then the `belongsTo` key is set to the name
of the reciprocal association link.

For example:

    var BlogPost = Backbone.Model.extend({
      initialize: function() {
        this.hasOne({
          author: { model: Author },
          main_photo: { model: Photo, belongsTo: 'post' },
        });
        this.hasMany({
          comments: {
            collection: CommentsCollection,
            belongsTo: 'post' }
        });
      }
    });

In use:

    var myPost = new BlogPost({
      headline: "An Evolutionary Basis for Procrastination",
      author: {
        name: "Phillip P. Perkins, MD"
      },
      main_photo: {
        url: "http://www.example.com/sdkjh3987.jpg"
      },
      comments: [
        { user: "username1", body: "Great article!" },
        { user: "username1", body: "Waste of time!" }
      ]
    });

    myPost.get('headline') => "Scientists..."
    myPost.mainPhoto.get('url') => "http://..."
    myPost.mainPhoto.post === myPost => true
    myPost.author.get('name') => "Phillip..."
    myPost.comments.first().get('user') => "username1"
    myPost.comments.first().post === myPost => true
    etc.

### Has One Behavior

When a hasOne association is initially created, the following occurs:

- The created association name is the attribute key transformed into camelCase.
- If there is no data for the association, then a new model is not created, and the association
  is set to null.
- If there is association data in the attribute, then a new Backbone Model is created with that data.
- The original association attribute data is deleted.
- A setter function is created on the model, named "set[association name]", which should be used
  for subsequent reassignments.

When the attribute used to create the association is set again, then the association setter is called instead.

When the setter function is called, the following occurs:

- If a Backbone.Model is passed to the setter, then it replaces the association. Appropriate _add_ and
  _remove_ events are generated. For example, if `setAuthor` is called, then an _add:author_ event will be triggered.
- If an attributes object is passed to the setter, then those attributes are set on the association instead.
  If this results in a model change, then a _change_ event will be triggered, ie _change:author_.
- If the setter is called without arguments, then the association is removed, a _remove_ event triggered,
  and the property set to null.

If a belongsTo relationship is specified:

- The association _must_ also define a hasOne relationship if a belongsTo link is desired.
- A reciprocal link is created on the association back to the original model

### Has Many Behavior

When a hasMany association is initially created:

- The created association name is the attribute key transformed into camelCase.
- A new collection is created of the desired class.
- If there is association data in the attribute, then a set of new models will be added to the collection.

When the original attribute is set again (ie from a call to `model.fetch()`), then the following occurs:

- If the associated collection is a standard Backbone.Collection, then `reset()` is called with the new data.
- If the associated collection has Duckbone.CollectionHelpers included, then `freshen()` will be called instead.
  This method results in the same final state of the collection, except that a number of _add_,
  _change_, and _remove_ events are triggered instead of _reset_. Previously existing models are simply
  updated, instead of being blown away by `reset()`.

If a belongsTo relationship is specified:

- The association _must_ also define a hasOne relationship if a belongsTo link is desired.
- A reciprocal link is created on the associated collection back to the original model.
- A reciprocal link is also created on each model in the associated collection back to the original model.

### Special Cases

If the desired association should have a different name from that naturally occuring in the JSON,
then the model's `parse` method can be overridden to move that attribute.

In the event that the developer absolutely must package associations back together as JSON to save
to the server, then the model's `toJSON` method can be overridden to add the association back
into the JSON.

*/


ABINE_DNTME.define('duckbone/model',
 ['documentcloud/underscore', 'documentcloud/backbone', 'wycats/handlebars', 'duckbone/core', 'abine/eventLogger'],
 function(_, Backbone, Handlebars, Duckbone, log) {


  Duckbone.AssociableModel = {

    // ### Mixin

    // #### property isAssociableModel
    // Indicates a class with AssociableModel included
    isAssociableModel: true,

    // #### function included
    // Creates a new set function that manages the movement of attributes to associated models.
    included: function() {
      if (!this.setWithoutAssociations) {
        this.setWithoutAssociations = this.set
        this.set = setWithAssociations;
      }
    },

    // ### Association Methods

    // #### function hasOne
    // - assocs - an object describing the associations, as described above
    // - returns - nothing
    //
    // Creates a number of given hasOne associations on the model.
    // Each association will be a new Backbone.Model.
    hasOne: function(assocs) {
      _.each(assocs, function(assoc, key) {
        this._associations = this._associations || {};
        this._associations[key] = "hasOne";
        createHasOneAssociation(this, key, assoc['model'], assoc['belongsTo']);
      }, this);
    },

    // #### function hasMany
    // - assocs - an object describing the associations, as described above
    // - returns - nothing
    //
    // Creates a number of given hasMany associations on the model.
    // Each association will be a new Backbone.Collection.
    hasMany: function(assocs) {
      _.each(assocs, function(assoc, key) {
        this._associations = this._associations || {};
        this._associations[key] = "hasMany";
        createHasManyAssociation(this, key, assoc['collection'], assoc['belongsTo']);
      }, this);
    }

  };

  /**
  # Duckbone.ModelHelpers

  This set of helpers provides conveniences for many common situations not already
  covered by Backbone.Model.
  */

  Duckbone.ModelHelpers = {

    // #### function simpleSave
    // - attrs - The attributes to be saved to the server.
    // - returns - the jqXHR object
    //
    // Posts only the given data to the server, leaving existing model attributes unchanged.
    simpleSave: function(attrs) {
      var data = {}
      if (this.id) attrs.id = this.id;
      if(this.paramRoot) {
        data[this.paramRoot] = attrs;
      } else {
        data = attrs;
      }
      var json = JSON.stringify(data);
      return this.save({}, {
        contentType: 'application/json',
        data: json
      });
    },

    // #### function url
    // Overrides Backbone's default url method with a new version that respects a function-style `urlRoot`
    url : function() {
      var urlRoot = (typeof this.urlRoot == 'function') ? this.urlRoot() : this.urlRoot;
      var base = getUrl(this.collection) || urlRoot || urlError();
      if (this.isNew()) return base;
      return base + (base.charAt(base.length - 1) == '/' ? '' : '/') + encodeURIComponent(this.id);
    },

    // #### function setOne
    // - attr - the attribute to set
    // - val - the new value
    // - options - same options as for set, ie. `{silent: true}`
    //
    // Sets a single attribute on the model.
    setOne: function(attr, val, options) {
      var attrs = {};
      attrs[attr] = val;
      return this.set(attrs, options);
    },

    // #### function createGetters
    // - Each string in the argument list creates a getter function for that attribute key.
    //
    // Creates simple getter functions for all desired attributes.
    // Getters are renamed in js syntax, so for example: `image_asset_url` becomes `getImageAssetUrl`.
    createGetters: function() {
      var model = this;
      var getterList = arguments;
      var getterFunctions = {};
      _.each(getterList, function(getter) {
        getterFuncName = underToCamel(getter);
        getterFuncName = 'get' + upCaseFirstChar(getterFuncName);
        if (!model[getterFuncName]) {
          getterFunctions[getterFuncName] = function() { return this.get(getter); }
        }
      });
      _.extend(this, getterFunctions);
    },

    // #### function isValidAttribute
    // Determines if an attribute is valid to set, avoiding setting attributes to NaN or undefined.
    // In almost all cases, better compatibility with Rails will be achieved by setting attributes
    // to null instead.
    isValidAttribute: function(val) {
      return (!_.isNaN(val) && !_.isUndefined(val))
    }
  };

  // ### Internal functions

  // Do the work of creating a hasOne association
  function createHasOneAssociation(model, key, modelClass, belongsTo) {
    var setterName = underToCamel('set_'+key);
    var relName = underToCamel(key);
    var setter = function(assocModel, settingUp) { // create the setter
      if (assocModel instanceof Backbone.Model) { // Model case
        if (assocModel === model[relName]) return assocModel; // return if same
        if (model[relName]) {
          model.trigger('remove:'+relName); // trigger if something is removed
          if (belongsTo) model[relName][belongsTo] = null; // delete reciprocal
        }
        model[relName] = assocModel; // set new associated model
        if (belongsTo) callReciprocalSetter(model, model[relName], belongsTo, settingUp);
        model.trigger('add:'+relName);
      } else if (assocModel) { // Attributes Object case
        if (model[relName]) { // Existing model case
          var bubbleChange = function() {
            model.trigger('change:'+relName); // trigger change only if it changes
          };
          model[relName].on('change', bubbleChange);
          model[relName].set(assocModel); // set new attributes on the association
          model[relName].off('change', bubbleChange);
        } else { // New model case
          model[relName] = new modelClass(assocModel); // create the model with the new attributes
          if (belongsTo) callReciprocalSetter(model, model[relName], belongsTo, settingUp);
          model.trigger('add:'+relName);
        }
      } else { // Null case, remove association
        if (model[relName]) { // existing model case
          model.trigger('remove:'+relName);
          if (belongsTo) model[relName][belongsTo] = null; // destroy reciprocal association
        }
        model[relName] = null; // destroy association
      }
      // assign parent messenger to child object
      if (assocModel && model.messenger && !assocModel.messenger) {
        assocModel.messenger = model.messenger;
      }
      return assocModel;
    } // end setter
    try {
      setter.call(model, model.get(key), true); // call the setter to initialize
    } catch(e) {
      log.error('Error creating association for ' + key + ':\n' + e);
    }
    delete model.attributes[key]; // remove associations data from attributes
    model[setterName] = _.bind(setter, model); // bind setter to model
  }

  // Do the work of creating a hasMany association
  function createHasManyAssociation (model, key, assoc, belongsTo) {
    try {
      var relName = underToCamel(key);
      var relData = model.get(key);
      var collection = model[relName] = new assoc(relData); // create the collection
      delete model.attributes[key]; // delete the association data from attributes
      if (belongsTo) {
        collection[belongsTo] = model; // create reciprocal link
        collection.each(function(m) { // create reciprocal link on all models
          callReciprocalSetter(model, m, belongsTo);
        });
        collection.on('add', function (m) { // on add(), also give new models reciprocal links
          callReciprocalSetter(model, m, belongsTo);
        });
        collection.on('remove', function (m) { // on remove(), remove reciprocal links
          m[belongsTo] = null;
        });
        collection.on('reset', function(c) { // on reset(), give all new models reciprocal links
          c.each(function(m) {
            callReciprocalSetter(model, m, belongsTo);
          });
        });
      }
      // assign parent messenger to child object
      if (model.messenger && !collection.messenger) {
        collection.messenger = model.messenger;
      }

    } catch (e) {
      log.error('Error creating association for ' + key + ':\n' + e);
    }
  }

  // Call the setter on a reciprocal association to establish the belongsTo
  function callReciprocalSetter(model, assocModel, belongsTo, settingUp) {
    var setterName = 'set' + belongsTo.slice(0, 1).toUpperCase() + belongsTo.slice(1);
    if (typeof assocModel[setterName] == "function") {
      assocModel[setterName].call(assocModel, model, settingUp);
    } else if (settingUp) {
      assocModel[belongsTo] = model;
    } else { // Complain if a hasOne association has not been defined for the reciprocal link
      log.error('To use belongsTo, model must define hasOne for ' + belongsTo);
    }
  }

  // Replacement function for `Model.set` that also manages setting to associations.
  function setWithAssociations(attrs, options) {
    var nonAssocAttrs = {};
    if (attrs instanceof Backbone.Model || !this._associations) {
      nonAssocAttrs = attrs;
    } else {
      _(attrs).each(function(v, k) {
        var assocType = this._associations[k];
        if (assocType == "hasOne") {
          var setterName = underToCamel('set_' + k);
          this[setterName](v);
        } else if (assocType == "hasMany") {
          var relName = underToCamel(k);
          if (typeof this[relName].freshen == "function") {
            this[relName].freshen(v);
          } else {
            this[relName].reset(v);
          }
        } else {
          nonAssocAttrs[k] = v;
        }
      }, this);
    }
    return this.setWithoutAssociations.call(this, nonAssocAttrs, options);
  }

  // Convert `a_rails_var` to `aJavaScriptVar`
  function underToCamel(word) {
    return word.replace(/(\_[a-z])/g, function($1) {
      return $1.toUpperCase().replace('_','');
    });
  }

  // Capitalize the first character
  function upCaseFirstChar(word) {
    return word.replace(/^\w/, function($0) {
      return $0.toUpperCase();
    });
  }

  // Get a URL from a Model or Collection as a property or as a function.
  var getUrl = function(object) {
    if (!(object && object.url)) return null;
    return _.isFunction(object.url) ? object.url() : object.url;
  };

});
/**
# Duckbone.Syncable

Duckbone's sync method takes the same method signature as Backbone.sync.
It is directly pluggable into any Model or Collection.
Its default behavior is the same as that described in the Backbone documentation at
<http://documentcloud.github.com/backbone/#Sync>.

## Usage

Mix Syncable into any Model or Collection. It will call Syncable.sync
instead of Backbone.sync for all fetch, save, destroy, etc. methods.

### Create Syncable Compatible Controllers

Duckbone.sync provides additional functionality that is necessary to
properly couple a client-side application to a set of JSON-centric Rails controllers.
To implement this pattern, craft the application's controllers as follows.

- Call `respond_to :json` in the controller, and avoid mixing html and json responses in one controller.
- Respond to GET/index actions with an array of JSONified models.
- Respond to GET/show actions with a single JSON object.
  Set the Backbone.Model's `paramRoot` if you prefer to use a JSON root attribute.
- Respond to successful POST/create and PUT/update actions with the single JSONified model.
- Respond to invalid POST/create and PUT/update actions with a 422 (Unprocessable Entity)
  in the header and the model's errors JSONified in the body.
- Respond to valid DELETE/destroy actions with status 202 (No Content)
- Respond to invalid DELETE/destroy actions with status 422 (Unprocessable Entity)
- Respond to all "not found" errors with status 404
- There is no need to write new or edit actions

For example:

    def update
      @goal = goals.find(params[:id])
      if @goal.update_attributes(params[:goal])
        render :json => @goal
      else
        render :json => {:errors => @goal.errors},
          :status => :unprocessable_entity
      end
    end


### CSRF Codes

Syncable will automatically insert the page's CSRF code into every ajax request.
Be sure that they are present in the page head metadata.

### Server Errors

Syncable will trigger the following events on the top level Duckbone object
whenever server errors happen during sync.
This enables disparate parts of the application to respond to potentially fatal errors.
Any response status greater than or equal to 400 will trigger an error event.
These include:

- 'sync:400' - triggered on 400 error
- 'sync:404' - triggered on 404 not found
- 'sync:500' - triggered on 500 server error

### Ajax Request Events

While Backbone supports jQuery's well known `success` and `error` callbacks,
by themselves these are frequently inadequate for the range of functionality
that must respond to model sync behavior. Syncable uses jQuery promises on
ajax calls to bind all additional behavior. This technique is usually preferred
over the use of the success and error callbacks. Other Duckbone mixins,
notably EditableView, rely heavily on model sync events to bind complex behavior.
The following events are available on all Syncable models and collections:

- 'sync:create' - triggered at the start of a create request
- 'sync:read' - triggered at the start of a read request
- 'sync:update' - triggered at the start of an update request
- 'sync:destroy' - triggered at the start of a destroy request
- 'sync:invalid' - triggered when a request returns invalid (422)
- 'sync:error' - triggered when a request returns an error (ie 404, 500)

### URLs and Customizing Calls

The URL that will be called follows the following order of precedence:

1. The url passed to sync() in the options object
2. The url property or function on the model
3. A url derived from the urlRoot property on the model
4. A url derived from the url of the collection including the model

Note that Duckbone.ModelHelpers improves Backbone.Model's implementation of
urlRoot so that it can also be specified as a function that returns the url root.

### A Note on Security

Many client-side security issues can result from Rails' default behavior
in which JSON serialization of models is completely unconstrained.
Take care to only include information in JSON data that the current user
should have access to. Consider overriding ActiveRecord::Base's as_json()
so that it will either blow up or emit nothing without explicit work by the developer.
*/


ABINE_DNTME.define('duckbone/syncable',
['documentcloud/underscore', 'jquery', 'documentcloud/backbone', 'wycats/handlebars', 'duckbone/core'], 
function(_, $, Backbone, Handlebars, Duckbone) {
  
  Duckbone.Syncable = {

    // #### property isSyncable
    // Identifies a Model or Collection that has Syncable mixed in.
    isSyncable: true,

    // #### function sync(method, model, [options])
    // - method – the CRUD method ("create", "read", "update", or "delete")
    // - model – the model to be saved (or collection to be read)
    // - options – success and error callbacks, and all other jQuery request options
    // - returns - the jqXHR object of the request
    sync: function(method, model, options) {
      var jqXHR;
      var type = methodMap[method];

      // A model may provide a defaultSyncOptions property which is merged into the
      // jQuery request before the call-time options
      var defaultSyncOptions = model.defaultSyncOptions || {};

      // Default JSON-request options.
      var params = _.extend({
        type: type,
        dataType: 'json',
        beforeSend: function( xhr ) {
          var token = $('meta[name="csrf-token"]').attr('content');
          if (token) xhr.setRequestHeader('X-CSRF-Token', token);
        }
      }, defaultSyncOptions, options);

      // workaround IE's aggressive caching of JSON
      if ($.browser.msie) params.cache = false;

      // Ensure URL, and then append .json to it
      if (!params.url) {
        params.url = getUrl(model) || urlError();
      }

      // Ensure that we have the appropriate request data.
      if (!params.data && model && (method == 'create' || method == 'update')) {
        params.contentType = 'application/json';
        var data = {}
        if(model.paramRoot) {
          data[model.paramRoot] = model.toJSON();
        } else {
          data = model.toJSON();
        }
        params.data = JSON.stringify(data)
      }

      // Don't process data on a non-GET request.
      if (params.type !== 'GET') {
        params.processData = false;
      }

      // Add the user credentials if HTTP Auth is used
      if (authenticatedUser.username) {
        params.username = authenticatedUser.username;
        params.password = authenticatedUser.password;
        params.headers = params.headers || {};
        params.headers['Authorization'] = 'Basic ' + encode64(params.username+':'+params.password);
      }

      // Make the request.
      jqXHR = $.ajax(params);

      // Attach the params that were used to make testing easier
      jqXHR.params = params;

      // When it starts, trigger one of the crud events
      // sync:create, sync:read, sync:update, sync:delete
      model.trigger('sync:'+method.toLowerCase());

      // Trigger sync:complete when it's done regardless of outcome
      jqXHR.complete(function(response) {
        model.trigger('sync:complete', jqXHR);
      });

      // When a model is saved, trigger 'sync:success'
      // It will be added to its destinationCollection if it's not already
      jqXHR.success(function(response) {
        delete model.errors;
        model.trigger('sync:success', jqXHR);
      });

      // Broadcast errors to the whole application
      jqXHR.error(function(response) {
        if (response.status >= 400) {
          Duckbone.trigger('sync:'+response.status, jqXHR);
        }
      });

      // Bind an additional callback to set error messages on the model
      jqXHR.error(function(response, condition) {
        if (response.status == 422) {
          var data = JSON.parse(response.responseText);
          if (data.errors) {
            model.errors = data.errors;
          } else {
            model.errors = data;
          }
          model.trigger('sync:invalid', jqXHR);
        } else {
          model.trigger('sync:error', jqXHR);
        }
      });

      return jqXHR;
    }

  };

  // ### HTTP Authentication
  // These methods enable all SyncableModels to use HTTP Authentication.

  // #### setAuthenticatedUser
  // - username - HTTP Auth username
  // - password - HTTP Auth password
  //
  // Call this function with the user's credentials.
  // All subsequent requests will be signed by this user.
  Duckbone.setAuthenticatedUser = function(username, password) {
    authenticatedUser.username = username;
    authenticatedUser.password = password;
  },

  // #### removeAuthenticatedUser
  // Clear the user's credentials.
  Duckbone.removeAuthenticatedUser = function() {
    delete authenticatedUser.username;
    delete authenticatedUser.password;
  }

  // Singleton authenticatedUser which holds the credentials inside this closure
  var authenticatedUser = {};

  // Backbone internal functions used by sync ported here

  var methodMap = {
    'create': 'POST',
    'update': 'PUT',
    'delete': 'DELETE',
    'read'  : 'GET'
  };

  function getUrl(object) {
    if (!(object && object.url)) return null;
    return _.isFunction(object.url) ? object.url() : object.url;
  }

  function urlError() {
    throw new Error("A 'url' property or function must be specified");
  }

  // Base 64 Encoder used by Basic Auth. Thank you internet.
  function encode64(input) {
    var keyStr = "ABCDEFGHIJKLMNOP" + "QRSTUVWXYZabcdef" +
                 "ghijklmnopqrstuv" + "wxyz0123456789+/" + "=";
    var output = "";
    var chr1, chr2, chr3 = "";
    var enc1, enc2, enc3, enc4 = "";
    var i = 0;
    do {
      chr1 = input.charCodeAt(i++);
      chr2 = input.charCodeAt(i++);
      chr3 = input.charCodeAt(i++);
      enc1 = chr1 >> 2;
      enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
      enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
      enc4 = chr3 & 63;
      if (isNaN(chr2)) {
        enc3 = enc4 = 64;
      } else if (isNaN(chr3)) {
        enc4 = 64;
      }
      output = output +
      keyStr.charAt(enc1) +
      keyStr.charAt(enc2) +
      keyStr.charAt(enc3) +
      keyStr.charAt(enc4);
      chr1 = chr2 = chr3 = "";
      enc1 = enc2 = enc3 = enc4 = "";
    } while (i < input.length);
    return output;
  }

});
/**
 # Duckbone.CollectionHelpers

 Provides miscellaneous additions to Backbone.Collection that are
 useful in Duckbone.

 ## Usage

 Mix Duckbone.CollectionHelpers into `Backbone.Collection.prototype` or
 extend Backbone.Collection into your own base collection class. For example:

 var myapp.Collection = Backbone.Collection.extend();
 Duckbone.include(myapp.Collection, Duckbone.CollectionHelpers);

 */


ABINE_DNTME.define('duckbone/collection',
['documentcloud/underscore', 'documentcloud/backbone', 'wycats/handlebars', 'duckbone/core'],
function(_, Backbone, Handlebars, Duckbone) {

  Duckbone.CollectionHelpers = {

    // #### property hasDuckboneCollectionHelpers
    // indicates the presence of this mixin
    hasDuckboneCollectionHelpers: true,

    // #### function slice
    // - offset - the first arg is the 0-index offset into the ordered collection
    // - length - the second arg is the number of models to return, defaulting to
    //            the rest of the collection after the slice point
    // - returns - an array
    //
    // Returns an array that is a subset of the collection's models.
    // Uses the same method signature as `Array.slice`
    slice: function() {
      return this.models.slice(arguments);
    },

    // #### function toSelectOptions
    // - attr - the model attribute to use for the select option html, defaults to "title"
    // - empty - show an empty slot first
    // - returns - an Object
    //
    // Returns an object suitable for passing to a select or radio field's selectOptions.
    toSelectOptions: function(attr, empty) {
      var options = {};
      attr = attr || 'title';
      if (empty) options[' '] = ' '
      this.each(function(m) {
        options[m.id] = m.get(attr);
      });
      return options;
    },

    // Take an array of raw objects
    // If the ID matches a model in the collection, set that model
    // If the ID is not found in the collection, add it
    // If a model in the collection is no longer available, remove it

    freshen: function(objects) {
      var model;
      // Mark all for removal
      this.each(function(m) {
        m._remove = true;
      });
      // Apply each object
      _(objects).each(function(attrs) {
        model = this.get(attrs.id);
        if (model) {
          model.set(attrs); // existing model
          delete model._remove
        } else {
          this.add(attrs); // new model
        }
      }, this);
      // Now check for any that are still marked for removal
      var toRemove = this.filter(function(m) {
        return m._remove;
      })
      _(toRemove).each(function(m) {
        this.remove(m);
      }, this);
      this.trigger('freshen', this);
    },

    // Override Backbone's fetch method to add 'freshen' capability.
    // just pass freshen: true to non-destructively update a collection from
    // the server
    fetch: function(options) {
      options || (options = {});
      var collection = this;
      var success = options.success;
      options.success = function(resp, status, xhr) {
        collection[options.add ? 'add' : (options.freshen ? 'freshen' : 'reset')](collection.parse(resp, xhr), options);
        collection.trigger('sync');
        if (success) success(collection, resp);
      };
      var error = options.error;
      options.error = function(resp) {
        if (error) {
          error(collection, resp, options);
        } else {
          collection.trigger('error', collection, resp, options)
        }
      }

      return (this.sync || Backbone.sync).call(this, 'read', this, options);
    }
  }

});
/**
# Duckbone.RoutableApplication

This mixin adds a page-centric routing system to Backbone's existing URL Router.
While Backbone's default routing strategy assumes that all route actions will be
defined on the router itself, RoutableApplication instead delegates its route actions
to child views that each represent atomic "pages" in the application.

This page-centric concept should be more familiar to Rails developers, and also
greatly improves code organization for large projects with many separate page views.

The standard Backbone `routes` object is still available, but the developer will likely
find it confusing to mix the two systems together in practice.

## Usage

Mix in `Duckbone.RouteableApplication` into your top level application object, which should also
extend Backbone.Router. Then call `mapRoutes()` in `initialize()`, passing an object that
represents the routing table. Define the `routeAction` on the view class so that it calls `loadView`
on itself. For example:

    MyApplication = Backbone.Router.extend({
      initialize: function() {
        mapRoutes({
          '/home':      HomeView,
          '/posts':     PostsView,
          '/posts/:id': PostView,
          '/posts/new': NewPostView
        });
      }
    });
    Duckbone.include(MyApplication.prototype,
      Duckbone.RouteableApplication);

    PostView = Backbone.View.extend({
      initialize: function { ...etc... },
      render: function() { ...etc... }
    }, {
      routeName: 'post',
      routeAction: function(id, params) {
        var model = new Post({ id: id });
        model.fetch({
          success: function(m) {
            myapp.loadView(PostView, {model: m});
          }
        })
      }
    });

    ... etc ...

    $(function() {
      var myapp = new MyApplication();
      myapp.setContainer($('#app_container').get(0));
      Backbone.history.start();
    });
*/


ABINE_DNTME.define('duckbone/routeable_application',
['documentcloud/underscore', 'jquery', 'documentcloud/backbone', 'wycats/handlebars', 'duckbone/core'],
function(_, $, Backbone, Handlebars, Duckbone) {

  Duckbone.RouteableApplication = {

    // ### Mixin
    // Indicates the presence of this mixin
    isRouteableApplication: true,

    // ### Methods

    // #### function setContainer
    // - containerElement - sets the the application's main container to the given DOM element
    // - returns - nothing
    //
    // This method must be called before `loadView` can work.
    setContainer: function(containerElement) {
      this.mainContainer = containerElement;
    },

    // #### function mapRoutes
    // - routes - a map of route keys and views
    //
    // Each route key is defined in the same way as the Backbone routes map.
    // The value of each route key is the view class for the route.
    // Each view should have a `routeName` and a `routeAction` defined on its class.
    // If your view prototype has a `templateName`, Duckbone will fall back to using
    // it as a routeName in its absence.
    mapRoutes: function(routingTable) {
      _.each(_.keys(routingTable), function(route) {
        try {
          var routeName = routingTable[route].routeName || routingTable[route].prototype.templateName
          if (!routeName)
            throw("Missing or bad routeName for " + route);
          if (typeof routingTable[route].routeAction != 'function')
            throw("Missing or bad routeAction for " + route);
          this.route(route, // draw the normal route
            routeName,
            routingTable[route].routeAction
          );
          this.route(route+'?*params', // draw the route with a query string
            routeName,
            function() { // call the routeAction with the query params appended to arguments
              var args = _.toArray(arguments);
              var params = args.pop();
              var paramsObject = _(params.split('&')).reduce(function(memo, pair) {
                memo[pair.split('=')[0]] = pair.split('=')[1]; return memo;
              }, {});
              return routingTable[route].routeAction.apply(this, args.concat(paramsObject));
            }
          );
        } catch (e) {
          _.log(e);
          throw("Bad route for " + route);
        }
      }, this);
    },

    // #### function loadView
    // - view - the view class to load
    // - options - an options object to be passed to the view initializer
    // - returns - the view
    //
    // Each route action generally results in a call to `loadView()`.
    loadView: function(view, options) {
      if (options === undefined) options = {};
      if (this._removableFlashes) {
        this._removableFlashes.remove();
        delete this._removableFlashes;
      }
      if (this.mainView) this.mainView.remove();
      options.application = this;
      this.mainView = new view(options);
      $(this.mainView.el).appendTo(this.mainContainer);
      return this.mainView;
    },

    // #### function navigate
    // Simply overrides Backbone's navigate method with a version that clears
    // flash messages
    navigate : function(fragment, triggerRoute) {
      // Next time loadView is called, we want to nix all current flashes.
      if (this.isFlashableView) this._removableFlashes = this.activeFlashes();
      Backbone.history.navigate(fragment, triggerRoute);
    },

    // #### function bindNavigationBars
    // Resets css classes on navigation bars on every navigate event.
    // All navigation bars should be lists in a `<nav>` element,
    // and the `<li>` class should be equal to the routeName of the link destination.
    // A `current` class will be assigned to the current route.
    bindNavigationBars: function() {
      this.on('all', function(e) {
        if (e.split(':')[0] != 'route') return;
        var routeName = e.split(':')[1];
        $('nav ul li').removeClass('active');
        $('nav ul li.'+routeName).addClass('active');
      });
    }

  };


});
ABINE_DNTME.define('duckbone/view_lifecycle_extensions',
['documentcloud/underscore', 'jquery', 'documentcloud/backbone', 'wycats/handlebars', 'duckbone/core'], 
function(_, $, Backbone, Handlebars, Duckbone) {

  // This module adds default initialize(), render(), and remove() methods to any View class.
  // These methods call any lifecycle hooks on other Duckbone mixins
  // that are present on the View class. These are called in the correct order, so
  // that everything instantiates smoothly, without the developer needing to write initialize().

  // All of the Duckbone mixins ending in -View also mix in this module,
  // as a dependency in their included() callback.

  // There are several callback hooks available that can be defined on the object:

  // - beforeClone() => called before EditableView clones its model for editing
  // - afterClone() => called after EditableView clones its model for editing
  // - afterCreateForm() => called after EditableView renders its form elements
  // - afterInitialize() => called after all other initializations are complete
  // - beforeRemove() => called before the View is removed
  // - afterRemove() => called after the View is removed

  Duckbone.ViewLifecycleExtensions = {
    hasViewLifecycleExtensions: true,

    initialize: function () {

      this.application = this.options.application;
      this.messenger = this.options.messenger;

      if (this.isEditableView) {
        tryMethod(this, 'beforeClone'); // User optionally defines this
        this.cloneModelForEditing();
        tryMethod(this, 'afterClone'); // User optionally defines this
      }
      this.render();
      if (this.isStylizeableView) {
        this.applyStyles();
      }
      if (this.isBindableView) {
        this.bindAttributes();
      }
      if (this.isEditableView) {
        this.bindModelSyncEvents();
        this.createForm();
        tryMethod(this, 'afterCreateForm'); // User optionally defines this
      }
      tryMethod(this, 'afterInitialize'); // User optionally defines this
    },

    render: function() {
      if (this.isTemplateableView) {
        this.renderTemplate();
      }
      if (this.isStylizeableView) {
        this.applyStyles();
      }
      return this;
    },

    remove: function() {
      if (this.isEditableView) this.expireClone();
      tryMethod(this, 'beforeRemove'); // User optionally defines this
      this.removeWeakBindings();
      if (this.isNestableView) this.removeNestedViews();
      Backbone.View.prototype.remove.call(this);
      tryMethod(this, 'afterRemove'); // User optionally defines this
      return this;
    },

    // ### Weak Binding
    // Weak bindings are a key feature of the Duckbone view lifecycle.  Weak bindings
    // may be created against any object that responds to bind() and unbind() - most
    // notably Backbone and jQuery objects.  When a weak binding is made in the context
    // of a view, it will automatically be unbound when that view is removed to prevent
    // zombie callbacks from causing trouble.

    // #### function weakBindToModel
    // Because binding to a view's model is so common, this shortcut method is provided.
    // It has the same method signature as `model.bind()`
    // - event - event to bind to, ie 'change:foo'
    // - callback - callback function
    // - context - optional `this` context for the callback, defaults to the view
    // - returns - nothing
    //
    // Creates a binding on the view's model that is unbound when the view is removed
    weakBindToModel: function(event, callback, context) {
      this.weakBindTo(this.model, event, callback, context);
    },

    // #### function weakBindTo
    // - obj - An object responding to `bind()`, e.g. a jQuery object or anything that
    //   includes Backbone.Events
    // - event - event to bind to, ie 'change:foo'
    // - callback - callback function
    // - context - optional `this` context for the callback, defaults to the view
    // - returns - nothing
    //
    // Creates a binding on the object that is unbound when the view is removed
    weakBindTo: function(obj, event, callback, context) {
      this._weakBindings = this._weakBindings || [];
      this._weakBindings.push([obj, event, callback]);
      obj.bind.apply(obj, _.toArray(arguments).slice(1));
    },

    // #### function removeWeakBindings
    // - returns - nothing
    // Unbinds all weak bindings created by this view
    removeWeakBindings: function() {
      _.each(this._weakBindings, function(binding) {
        binding[0].unbind(binding[1],binding[2]);
      });
      delete this['_weakBindings'];
      if (this.isBindableView) this.unbindLiveTimestamps();
    }
  }

  var tryMethod = function(obj, method) {
    if (obj[method]) obj[method].call(obj);
  }

});

ABINE_DNTME.define('duckbone/handlebars_extensions',
['documentcloud/underscore', 'jquery', 'documentcloud/backbone', 'wycats/handlebars', 'duckbone/core'], 
function(_, $, Backbone, Handlebars, Duckbone) {

  // Establish some globals so that this template code can work
  // The precompiler inserts calls to ABINE_DNTME.Duckbone.Handlebars.getAttr
  
  ABINE_DNTME.Duckbone = Duckbone;
  Duckbone.Handlebars = Handlebars;
  
  // Modify getAttr to automatically find attrs in Backbone models
  Handlebars.getAttr = function(object, key) {
    if (/^[0-9]+$/.test(key)) {
      return object[parseInt(key)];
    }
    if (object[key] != undefined) {
      if (typeof object[key] == 'function') {
        return object[key].call(object);
      } else {
        return object[key];
      }
    }
    if(object.get){
      return object.get(key);
    }
  }

});

ABINE_DNTME.define('duckbone/idme_template_registry',
['documentcloud/underscore', 'jquery', 'documentcloud/backbone', 'wycats/handlebars', 'duckbone/core'], 
function(_, $, Backbone, Handlebars, Duckbone) {

  // This is an adaptation of Duckbone TemplateRegistry for handlebars templates
  // that have been compiled on the server side.

  Duckbone.TemplateRegistry = function(options) {
    options = options || {};
    this.compiledTemplates = {};
    this.templatesCompiled = false;
    this.partialsCompiled = false;
  }

  _.extend(Duckbone.TemplateRegistry.prototype, {

    refresh: function() {},

    compile: function() {
      compileTemplates(this);
    },

    get: function(src) {
      compileTemplates(this);
      return this.compiledTemplates[src]; // This may return undefined
    }
  });

  function compileTemplates (context, src) {
    if (context.templatesCompiled) return;
    
    _.each(_.keys(ABINE_DNTME.CompiledTemplates), function(tmpl_name) {
      var processed_name = tmpl_name.replace('views/', '').replace('/', '_');
      context.compiledTemplates[processed_name] = ABINE_DNTME.CompiledTemplates[tmpl_name];
    });
    context.templatesCompiled = true;
  }

});
/**
# Duckbone.NestableView

NestableView provides automated setup and teardown of child views.  You just provide
a `createChildren` method that returns an object containing all of your child view
instances by name, like this

```javascript
createChildren: function() {
  return {
    myStuff: new StuffView({model: user.stuff})
  }
}
```

NestableView works hand-in-hand with TemplateableView's {{child}} helper.

*/


ABINE_DNTME.define('duckbone/nestable_view',
['documentcloud/underscore', 'jquery', 'documentcloud/backbone', 'wycats/handlebars', 'duckbone/core'], 
function(_, $, Backbone, Handlebars, Duckbone) {
   
  Duckbone.NestableView = {

    // #### property isNestableView
    // Indicates a view with this mixin included
    isNestableView: true,

    setupNestedViews: function() {
      if (this.createChildren) {
        this.children = this.createChildren();
      }
    },

    removeNestedViews: function() {
      if (this.children) {
        _.each(this.children, function(child) {
          child.remove();
        });
        delete this.children;
      }
    }
  }

});
/**
# Duckbone.TemplateableView

This mixin enables any view to use Duckbone Handlebars templates. Most views in a
typical application should use templates and this mixin is included in Duckbone.View.

This file also defines many additional Handlebars helpers that provide additional
Duckbone-specific functionality.

## Usage

To assign a template to a view, do one of the following

- Define the View's `templateName` property. This should be the same as the file name in the
  templates folder, with slashes changed to underscores. So "posts/post" => "posts_post".
  This is the preferred usage.
- Define the View's `templateData` property as a raw string Handlebars template
- Define the View's `template` property as a compiled Handlebars template

Templates are *always* rendered in the context of the View's `model` property.

Use the Duckbone helpers to provide additional functionality:

- {{#if}} now understands functions, so you can use any function that returns a Boolean value
- {{#each}} now understands Collections, so you can iterate over a Collection's models with it
- {{attribute}} will automatically include Model attributes
- {{attr "foo"}} includes a Model attribute and also updates on any model changes
- {{child "fooView"}} includes another view

### Overriding the Default Render Function

The view lifecycle provides a default render function for you that renders the template. If you
need to do more work in render, just redefine it, calling `renderTemplate()` somewhere.

    render: function() {
      this.renderTemplate();
      // do other stuff here, like ...
      return this
    }

### Using Sub-Views

When a view includes another view, use the following technique:

- Define the subview in the parent's `createChildren` callback. The view lifecycle will call this
  automatically during renderTemplate. Assign the subview to the parent, ie `this.mySubView`
- Use the `{{child}}` helper in the parent's template, ie {{child "mySubView"}}
- Teardown is automatic

For example:

    ParentView = Duckbone.View.extend({
      templateData: '<div>Parent View</div>
        <div class="childStuff">{{child "childView"}}</div>',
      createChildren: function() {
        {
          childView: new Duckbone.View()
        }
      }
    })

*/

ABINE_DNTME.define('duckbone/templateable_view',
['documentcloud/underscore', 'jquery', 'documentcloud/backbone', 'wycats/handlebars', 'duckbone/core', 'abine/eventLogger'], 
function(_, $, Backbone, Handlebars, Duckbone, log) {

  // Create a registry that only lives in this context
  var templateRegistry = new Duckbone.TemplateRegistry();

  // Create Handlebars helpers if we're using Handlebars.
  // These helpers make working with models and collections easier.

  if (Handlebars) {

    // Fix broken if and unless behavior in the stock handlebars
    // Now #if can reference a function property that returns true or false

    Handlebars.helpers['oldIf'] = Handlebars.helpers['if']

    Handlebars.registerHelper('if', function(context, fn, inverse) {
      var condition = typeof context === "function" ? context.call(this) : context;
      return Handlebars.helpers['oldIf'].call(this, condition, fn, inverse);
    });

    // The "attr" helper renders a model's attribute,
    // and binds the view to any changes on that attribute.
    // Handlebars can automatically find attributes, so
    // this is only necessary if you want to bind to changes.
    //
    // usage: {{attr "foo"}}

    Handlebars.registerHelper('attr', function(attribute) {
      return new Handlebars.SafeString(
        '<span data-bind="attr_' + Handlebars.Utils.escapeExpression(attribute) + '">' +
        Handlebars.Utils.escapeExpression(this.get(attribute)) +
        '</span>'
      );
    });

    // The "child" helper indicates where sub-views should be included in a template
    // renderTemplate() will look for a view with this name defined as a prop on the parent view
    // and replace this element

    Handlebars.registerHelper('child', function(childName, tagName) {
      tagName = (_.isObject(tagName)) ? "div" : tagName;
      return new Handlebars.SafeString(
        '<' + tagName + ' data-child-view="' + Handlebars.Utils.escapeExpression(childName) + '"></' + tagName + '>'
      );
    });


    // Make each capable of handling collections

    Handlebars.helpers['oldEach'] = Handlebars.helpers['each']

    Handlebars.registerHelper('each', function(context, fn, inverse) {
      if (context.models) {
        context = context.models;
      }
      return Handlebars.helpers['oldEach'].call(this, context, fn, inverse);
    });

  }

  // Duckbone.TemplateableView
  // Doc above

  Duckbone.TemplateableView = {
    isTemplateableView: true,

    included: function() {
      if (!this.hasViewLifecycleExtensions) {
        Duckbone.include(this, Duckbone.ViewLifecycleExtensions);
      }
      if (!this.isNestableView) {
        Duckbone.include(this, Duckbone.NestableView);
      }
    },

    templateRegistry: templateRegistry,

    // Fetch template out of the View's templateData, options or the templateRegistry
    // - allowEmpty - if true, don't set a "missing template" template on not-found.
    getTemplate: function(templateName, allowEmpty) {
      this.templateData = this.options.templateData || this.templateData || null;
      if (this.templateData) {
        log.error("Cannot compile template.");
        throw("Cannot compile template");
        // this.template = Duckbone.Handlebars.compile(this.templateData);
      } else {
        templateName = templateName || this.options.templateName || this.templateName || "";
        this.template = this.templateRegistry.get(templateName);
        if (!this.template && !allowEmpty) {
          this.template = Duckbone.Handlebars.compile("<div>Missing Template \"" + templateName + "\"</div>");
        }
      }
      return this.template
    },

    // Fetches its template from any of the following places:
    //  this.options.template, this.template, this.templateName, this.options.templateName
    // Then it renders it in the context of its model.
    // The output replaces the entire html content of the view's container element.
    // Pass a different context param to render from a different context.
    // ie this.renderTemplate({some_different: "data"});

    // Attempts to replicate typical server-side templating behavior in which
    // errors in development will show as error messages in place in the template,
    // and errors in a production environments will result in a server error.
    // Also prints useful error messages to the console.

    renderTemplate: function(context) {

      // Set up context and template
      var context = context || this.model || {};
      if (_.isFunction(this.options.template)) {
        this.template = this.options.template;
      } else if (!this.template) {
        this.getTemplate();
      }
      if (!this.template) throw("renderTemplate called without a template.")

      // Remove child views ...
      this.removeNestedViews();

      // Unbind any previous data bindings
      if (this.isBindableView) this.unbindAttributes();

      // Render the HTML template, and if it fails, output as much info as possible
      // 500 Error on exceptions in production
      try {
        $(this.el).empty().html(this.template(context));
      } catch(e) {
        log.error("Handlebars threw an exception rendering your template:");
        log.error(e.message);
        log.error("Context:");
        log.error(context);
        $(this.el).html('<div class="duckbone_error">Handlebars exception: ' + Handlebars.Utils.escapeExpression(e.message) + '</div>');
      }

      // Establish any data-bindings that are defined by {{attr "attribute"}} helpers
      if (this.isBindableView && context instanceof Backbone.Model) {
        $(this.el).find('span[data-bind]').each(_.bind(function(i, span) {
          var attr = $(span).attr('data-bind').slice(5); // ie data-bind="attr_title"
          var binding = 'span[data-bind="attr_' + attr + '"]';
          this.bindAttribute(context, attr, binding);
        }, this));
      }

      this.setupNestedViews();

      // Include child views where they belong in the DOM
      $(this.el).find('*[data-child-view]').each(_.bind(function(i, div) {
        var childName = $(div).attr('data-child-view');
        var child = this.children[childName]
        if (child instanceof Backbone.View) {
          $(div).replaceWith(child.el); // swap placeholder for childView
          $(child.el).attr({'data-child-view': childName});
          child.delegateEvents();
        } else {
          log.error("Unknown child view " + childName);
        }
      }, this));

      // Assign a descriptive class name to the element
      var templateName = this.options.templateName || this.templateName;
      if (this.templateName) {
        $(this.el).addClass(this.templateName);
      }

      this.templateRendered = true;

      // return self, so the final call in render in coffee will return itself by convention
      return this

    },

    // Calls `renderTemplate` if it hasn't been called before.  This is useful in a
    // custom render method if you want to render a template when the view is initialized
    // and then use DOM manipulation to keep the HTML up to date in subsequent renders

    renderTemplateOnce: function (context) {
      if (!this.templateRendered) this.renderTemplate(context);
      return this;
    }

  }

});
ABINE_DNTME.define('duckbone/stylizeable_view',
['documentcloud/underscore', 'jquery', 'documentcloud/backbone', 'wycats/handlebars', 'duckbone/core'], 
function(_, $, Backbone, Handlebars, Duckbone) {

 Duckbone.StylizeableView = {
    isStylizeableView: true,

    // Also include ViewLifecycleExtensions

    included: function() {
      if (!this.hasViewLifecycleExtensions) {
        Duckbone.include(this, Duckbone.ViewLifecycleExtensions);
      }
    },

    // Apply stylesheets that are contained on the view itself
    // These only apply to elements that are children of the view
    // Add them to the styles property as a hash of jquery selectors and css
    // Use the selector 'el' to apply styles to the root element
    // 'el.className' also works
    // styles: {
    //   'el':      {'padding': '5px'},
    //   'el.hot':  {'color': 'red'},
    //   'b.money': {'color': 'green'},
    //   'li':      {'margin': '10px'}
    // }

    // Apply the CSS to children of the view. Formatted as above

    applyStyles: function(styles) {
      styles = styles || this.styles || {};
      var tagName, className
      for (var selector in styles) {
        tagName = selector.split('.')[0];
        className = selector.split('.')[1];
        if (selector == 'el') {
          $(this.el).css(styles['el']);
        } else if (tagName == 'el' && className && $(this.el).hasClass(className)) {
          $(this.el).css(styles[selector]);
        } else {
          $(this.el).find(selector).css(styles[selector]);
        }
      }
    }

  }

});
ABINE_DNTME.define('duckbone/pageable_view',
['documentcloud/underscore', 'jquery', 'documentcloud/backbone', 'wycats/handlebars', 'duckbone/core'], 
function(_, $, Backbone, Handlebars, Duckbone) {

  // PageableView works in concert with ListableView to create a container view that
  // manages a list of subviews that represent a paginated collection.
  //
  // PageableViews are TemplateableViews, so you can provide your own template if you like.
  // The default template is simply `{{child "list"}}{{child "pager"}}`.  Your custom
  // template must contain at least those elements.

  // Usage..
  /*
  myPageableView = Backbone.View.extend({
    ...
  });
  Duckbone.include(myPageableView.prototype, Duckbone.PageableView)
  */

  Duckbone.PageableView = {
    isPageableView: true,

    // Also include TemplateableView (which in turn includes ViewLifecycleExtensions)
    included: function() {
      if (!this.isTemplateableView) {
        Duckbone.include(this, Duckbone.TemplateableView);
      }
    },

    // Override getTemplate to provide a simple default
    getTemplate: function(templateName) {
        Duckbone.TemplateableView.getTemplate.call(this, templateName, true);
        this.template = this.template || Duckbone.Handlebars.compile('{{child "list"}}{{child "pager"}}');
    },

    // Create sub views for render
    createChildren: function() {
      // Create child views
      var list = this.createListView();
      var pager = this.createPagerView();

      // bind 'changePage' events to navigation state
      this.application = this.options.application || this.application;
      if (this.application) {
        this.collection.on('pageChange', function(p, options) {
          var newLocation = window.location.hash.split('?')[0] + '?';
          var urlEncodedOptions = _(_.keys(options)).map(function(o) {
            return o + '=' + options[o]
          }).join('&');
          newLocation += urlEncodedOptions;
          this.application.navigate(newLocation, false);
        }, this);
      }

      return {
        list: list,
        pager: pager
      }
    },

    // Creates a list view container for the elements
    createListView: function() {
      return new Duckbone.ListView({
        viewClass: this.viewClass,
        pageableView: this,
        tagName: 'ul',
        className: 'listable_view',
        collection: this.collection
      });
    },

    // Creates a view for the pager element
    // You can set a custom pager view class, or use the supplied default view
    createPagerView: function() {
      var pagerClass = this.options.pagerClass || this.pagerClass || Pager;
      return new pagerClass({
        collection: this.collection,
        pageableView: this
      });
    },

    showPage: function(p) {
      p = parseInt(p) || 1;
      this.collection.setPage(p);
    },

    showNextPage: function() {
      this.showPage(this.collection.currentPage+1);
    },

    showPreviousPage: function() {
      this.showPage(this.collection.currentPage-1);
    },

    // Smooth scroll up to the top of the list
    scrollToTopOfList: function() {
      var offset = $(this.children.list.el).offset().top - 20;
      $('html body').animate({ scrollTop: offset }, 400, 'swing');
    }

  }

  // Default Pager template

  var pagerTemplateData =
  '<div class="pager">&nbsp;<br/>\
    {{#if showPrevious}}\
      <a href="#" class="previous_link">&laquo;&nbsp;Previous</a>&nbsp;\
    {{else}}\
      <span class="inactive">&laquo;&nbsp;Previous</span>&nbsp;\
    {{/if}}\
    {{#each pages}}\
      {{#if currentPage}}\
        <span class="current">{{title}}</span>&nbsp;\
      {{else}}\
        <a href="#" class="page_link" data-page="{{id}}">{{title}}</a>&nbsp;\
      {{/if}}\
    {{/each}}\
    {{#if showNext}}\
      <a href="#" class="next_link">Next&nbsp;&raquo;</a>\
    {{else}}\
      <span class="inactive">Next&nbsp;&raquo;</span>\
    {{/if}}\
  </div>';

  // Default Pager view class

  Pager = Backbone.View.extend({

    el: '<div class="pager_view"></div>',
    templateData: pagerTemplateData,

    events: {
      'click a.previous_link': 'showPreviousPage',
      'click a.next_link': 'showNextPage',
      'click a.page_link': 'showPage'
    },

    initialize: function() {
      this.render();
      this.collection.on('all', this.render, this);
    },

    // Only render if pagination data is present and fetched

    render: function() {
      if (this.collection.numPages) this.renderTemplate(this.paginationData());
      if (this.collection.numPages > 1) {
        $(this.el).show();
      } else {
        $(this.el).hide();
      }
      return this;
    },

    paginationData: function() {
      var numPages = this.collection.numPages;
      var pageData = {
        showPrevious: (this.collection.currentPage > 1),
        numPages: numPages,
        showNext: (this.collection.currentPage < this.collection.numPages),
        pages: _(_.range(1, numPages+1)).map(function(i) {
          return {
            id: i,
            title: ''+i,
            currentPage: (i == this.collection.currentPage)
          }
        }, this)
      };
      return pageData;
    },

    showPage: function(e) {
      e.preventDefault();
      var p = parseInt($(e.target).data('page'));
      this.options.pageableView.showPage(p);
      this.options.pageableView.scrollToTopOfList();
      return false;
    },

    showPreviousPage: function(e) {
      e.preventDefault();
      this.options.pageableView.showPreviousPage();
      this.options.pageableView.scrollToTopOfList();
      return false;
    },

    showNextPage: function(e) {
      e.preventDefault();
      this.options.pageableView.showNextPage();
      this.options.pageableView.scrollToTopOfList();
      return false;
    }

  });

  Duckbone.include(Pager.prototype, Duckbone.TemplateableView);

});
/**
# Duckbone.PageableCollection

PageableCollection enhances a Backbone Collection so that it understands its connection to a server-paginated endpoint.
It should be used in conjunction with the Rails library found in Duckbone.PageableCollection, and either
of the standard will_paginate or kaminari gems.

## Usage

First, establish pagination in your controller action.

Then use `fetchPage()` to fetch the collection.

*/


ABINE_DNTME.define('duckbone/pageable_collection',
['documentcloud/underscore', 'jquery', 'documentcloud/backbone', 'wycats/handlebars', 'duckbone/core'], 
function(_, $, Backbone, Handlebars, Duckbone) {
   
  Duckbone.PageableCollection = {

    // #### property isPageableCollection
    // Indicates the presence of this mixin
    isPageableCollection: true,

    // #### function parse
    // - resp - the response object
    // - xhr - the jQuery XHR object
    // - returns - an object representing the parsed data
    //
    // When PageableCollection is used in a Rails controller, it wraps the response in an object
    // that contains the collection's pagination meta-data. This overridden parse method extracts
    // the meta-data into the object, and passes the records on to the normal behavior.
    //
    // An example response might look like this:
    // {num_pages: 4, limit_value: 25, current_page: 1, records: [{...}, {...}]}
    parse: function(resp, xhr)  {
      this.numPages = resp.num_pages;
      this.limitValue = resp.limit_value;
      this.currentPage = resp.current_page;
      this.totalCount = resp.total_count;
      return resp.records;
    },

    // #### function fetch
    // Delegate `fetch()` to `fetchPage()`. The normal options to fetch are discarded if passed.
    fetch: function() {
      this.fetchPage(1);
    },

    // #### function fetchPage
    // - pageNum - the page ordinal requested, begining with 1
    // - params - an object representing the query params to be passed to the server,
    //   defaulting to the params defined on the collection.params (by setParam)
    // - returns - nothing
    fetchPage: function(pageNum, params) {
      pageNum = pageNum || 1;
      params = params || _.clone(this.params) || {};
      return Backbone.Collection.prototype.fetch.call(this, {
        url: buildUrl(this, _.extend(params, {'page': pageNum})),
        success: function(c) {
          c.trigger('pageChange', c.currentPage, params);
        }
      });
    },

    // #### function setParam
    // - param - query parameter name
    // - val - query parameter value
    // - returns - nothing
    //
    // Set additonal query params on the collection, ie search string.
    // These will be sent to the server on fetch and show up in the url.
    setParam: function(param, val) {
      this.params = this.params || {};
      this.params[param] = val;
    },

    // #### function setPage
    // - p - ordinal page number requested
    // - returns - the jQuery XHR object from the resulting fetch call, or null if fetch is not called
    //
    // Sets the collection to the given page number, and fetches those models
    setPage: function(p) {
      if (p != this.currentPage && p > 0 && p <= this.numPages) return this.fetchPage(p);
    },

    // #### function nextPage
    // - returns - the jQuery XHR object from the resulting fetch call, or null if fetch is not called
    //
    // Sets the collection to the next page, and fetches those models
    nextPage: function() {
      return this.fetchPage(this.currentPage+1);
    },

    // #### function prevPage
    // - returns - the jQuery XHR object from the resulting fetch call, or null if fetch is not called
    //
    // Sets the collection to the previous page, and fetches those models
    prevPage: function() {
      return this.fetchPage(this.currentPage-1);
    },

    // #### function updateTotalCountOnCollectionEvents
    // Since the collection's `totalCount` property is only updated during fetch/parse,
    // when models are added and removed from the collection, then it is possible for this
    // number to become out of sync with the number of actual objects logically part of the collection.
    // Call this function to maintain the totalCount as models are added and removed from the collection
    // in between calls to fetch(). Since this behavior is application dependent, the use of this
    // method is left to the developer's discretion.
    updateTotalCountOnCollectionEvents: function() {
      this.on('add', function() {
        if (this.totalCount) this.totalCount += 1;
      }, this);
      this.on('remove', function() {
        if (this.totalCount) this.totalCount -= 1;
      }, this);
    }

  };

  // ### Internal

  function buildUrl(context, options) {
    options = options || {};
    var url = (typeof context.url == 'function') ? context.url() : context.url
    if (url.indexOf('?') < 0) url += '?';
    for (var o in options) {
      url += o+'='+options[o]+'&';
    }
    return url;
  }

});
/**
# Duckbone.BindableView

This mixin enables any view to selectively data-bind to changes in the model.
Many developers prefer this approach over re-rendering an entire view on each model
change event.

## Usage

Mix Duckbone.BindableView into any View that needs to use declarative data-binding
to model attributes.

### Data Binding

Data bindings should be defined in the `attributeChanges` object. The binding value
can be a string selector, in which case the html of that selector is replaced with
the model value. It can also be a callback function, which is simply executed,
in the context of the view. For example:

    attributeChanges: {
      'attribute': 'span.attribute',
      'title': 'div.title',
      'amount': function(val) {
        $(this.el).find('span.amount').html('$'+val);
      }
    }

BindableView also mixes in Duckbone.ViewlifecycleExtensions to assist in creating
and tearing down bindings. If you are using the lifecycle, then bindings will be
created and destroyed for you. If not, call `bindAttributes()` in the view's
initialize method to establish the bindings. And call `removeWeakBindings()` in the
view's remove() method.

### Weak Binding

BindableView uses "weak bindings" which are intended to be unbound when the view is
removed. This prevents zombie callbacks from wreaking havoc as a model gets passed from
view to view. All of the bindings defined in `attributeChanges` use weak binding.

*/


ABINE_DNTME.define('duckbone/bindable_view',
['documentcloud/underscore', 'jquery', 'documentcloud/backbone', 'wycats/handlebars', 'duckbone/core'], 
function(_, $, Backbone, Handlebars, Duckbone) {

  Duckbone.BindableView = {

    // ### Mixin

    // #### property isBindableView
    // Identifies a view that has BindableView mixed in.
    isBindableView: true,

    // #### function included
    // Also mix in Duckbone.ViewLifecycleExtensions
    included: function() {
      if (!this.hasViewLifecycleExtensions) {
        Duckbone.include(this, Duckbone.ViewLifecycleExtensions);
      }
    },

    // #### function bindAttributes
    // - model - the model to bind to, defaulting to `this.model`
    // - returns - nothing
    //
    // Establishes the bindings declared in `this.attributeChanges`.
    bindAttributes: function(model) {
      if (!this.attributeChanges) return;
      model = model || this.model || new Backbone.Model();
      for (var attr in this.attributeChanges) {
        this.bindAttribute(model, attr, this.attributeChanges[attr]);
      }
    },

    // #### function bindAttribute
    // - model - the model to bind to, defaulting to `this.model`
    // - attr - the model attribute to bind to
    // - binding - the jQuery selector or binding callback
    // - returns - nothing
    //
    // Bind a single attribute to the supplied model.
    // The binding param can either be a string selector,
    // in which case the html of that selector is replaced with the model value
    // or a callback function, which is simply executed, in the context of the view.
    // If the view is already rendered, then initial values will be set
    // to the current state of the model at the time of this call.
    bindAttribute: function(model, attr, binding) {
      // Get model
      model = model || this.model || new Backbone.Model();
      var attrValue = Handlebars.Utils.escapeExpression(model.get(attr));
      var handler;
      if (typeof binding == "function") {
        // The binding is a callback function. Set value now.
        binding.call(this, attrValue);
        handler = function() {
          binding.call(this, Handlebars.Utils.escapeExpression(model.get(attr)));
        }
      } else if (typeof binding == "string") {
        // The binding is a selector. Set value now.
        $(this.el).find(binding).html(attrValue);
        handler = function () {
          $(this.el).find(binding).html(Handlebars.Utils.escapeExpression(model.get(attr)));
        }
      }
      this._attributeBindings = this._attributeBindings || [];
      var e = 'change:'+attr;
      this._attributeBindings.push([model, e, handler]);
      this.weakBindTo(model, e, handler, this);
    },

    // #### function unbindAttributes
    // Unbind all previously-bound attributes.  Used internally to clean up for
    // re-rendering a template
    unbindAttributes: function() {
      if (this._attributeBindings) {
        _(this._attributeBindings).each(function(binding) {
          // There is no individual unbind for weakBindings.  The housekeeping
          // to clean up the metadata would be more expensive than attempting
          // to unbind all at teardown for most use cases, so we just do a raw
          // unbind.
          binding[0].unbind(binding[1], binding[2]);
        });
        delete this._attributeBindings;
      }
    },

    // #### function bindLiveTimestamps
    // - seconds - number of seconds to wait before updating again
    // - returns - nothing
    //
    // If you are using the {{live_timestamp}} helper, then call this function
    // to begin updating those timestamps once per minute.
    bindLiveTimestamps: function(seconds) {
      return; // need to fix timer for this to work
      seconds = seconds || 60;
      var updateTimestamps = _.bind(function() {
        $(this.el).find('span[data-live-timestamp]').each(_.bind(function(i, span) {
          var millis = parseInt($(span).attr('data-live-timestamp'));
          var pretty = Duckbone.helpers.dateToPrettyTimeAgo(new Date(millis));
          $(span).html(pretty);
        }, this))
      }, this);
      updateTimestamps();
      this._updateLiveTimeStampsInterval = setInterval(updateTimestamps, seconds * 1000);
    },

    // #### unbindLiveTimestamps
    // Stop updating timestamps.
    unbindLiveTimestamps: function() {
      return; 
      clearInterval(this._updateLiveTimeStampsInterval);
    }

  };

  // #### helper {{live\_timestamp}}
  // Creates a partial that can be used to insert a live timestamp
  // Bindable view can use this to update it once per minute
  // usage {{live_timestamp "created_at"}}
  if (Handlebars) {
    Handlebars.registerHelper("live_timestamp", function(timestamp) {
      var stamp = Duckbone.Handlebars.getAttr(this, timestamp);
      if (!stamp) return "";
      var date = new Date(stamp);
      if (!date) return "";
      return new Handlebars.SafeString(
        '<span data-live-timestamp="' + date.getTime() + '">' +
        Duckbone.helpers.dateToPrettyTimeAgo(date) + '</span>'
      );
    });
  }

});
ABINE_DNTME.define('duckbone/listable_view',
['documentcloud/underscore', 'jquery', 'documentcloud/backbone', 'wycats/handlebars', 'duckbone/core'], 
function(_, $, Backbone, Handlebars, Duckbone) {

  Duckbone.ListableView = {
    isListableView: true,

    tagName: 'ul',
    className: 'listable_view',

    // Also include ViewLifecycleExtensions

    included: function() {
      if (!this.hasViewLifecycleExtensions) {
        Duckbone.include(this, Duckbone.ViewLifecycleExtensions);
      }
      if (!this.isBindableView) {
        Duckbone.include(this, Duckbone.BindableView);
      }
      if (!this.isNestableView) {
        Duckbone.include(this, Duckbone.NestableView);
      }
    },

    render: function(force) {
      this.doc = this.doc||this.options.doc||document;
      if (!this._viewsRendered || force) {
        this.renderItems();
        if (!this.collectionEventsBound) {
          this.bindCollectionEvents();
          this.collectionEventsBound = true;
        }
        this._viewsRendered = true;
      }
      return this;
    },

    createChildren: function() {
      return _.reduce(this.collection.models, function(views, model) {
        views[model.cid] = new this.viewClass({model: model, doc: this.doc});
        return views;
      }, {}, this);
    },

    // Create a view for each model in the collection
    renderItems: function() {
      ensureViewClass(this);
      ensureCollection(this);
      // Create all of the children views
      this.setupNestedViews();
      // add their el's to a fragment first
      var fragment = this.doc.createDocumentFragment();
      _.each(this.children, function(view) {
        fragment.appendChild(view.el);
      });
      // Then add the fragment to the DOM. This cuts down on page redraws
      this.el.appendChild(fragment);
    },

    // Bind default behaviors to collection events
    // Can also override or extend behavior by calling with a hash of event names and callbacks
    // Callbacks are always called in the context of the ListableView
    // usage:
    // bindCollectionEvents({
    //  reset: function() { // do stuff }
    //  add: 'myMethod' // or a method on this class
    // });

    bindCollectionEvents: function(eventHandlers) {
      var handler;
      ensureCollection(this);
      eventHandlers = eventHandlers || this.collectionEvents || collectionEventHandlers;
      for (var event in eventHandlers) {
        handler = eventHandlers[event];
        if (typeof handler == 'string') handler = this[handler];
        if (typeof handler != 'function') {
          throw ('Duckbone.ListableView.bindCollectionEvents() called with bad handler for ' + event )
        }
        this.weakBindTo(this.collection, event, handler, this);
      }
    },

    // Remove all child views (without destroying the models in the collection)

    // Call fetch on the collection to refresh its contents
    // The reset event will cause this view to completely redraw

    refresh: function(options) {
      options = options || {};
      this.empty();
      this.collection.fetch();
    },

    // Retrieve the view for a given model

    getViewByModel: function(model) {
      return this.children[model.cid];
    },

    // Default handler for when the collection is reset

    collectionReset: function() {
      this.removeNestedViews();
      this.render(true);
    },

    // Default handler for when a model is added to the collection

    collectionAdd: function(model) {
      var view = new this.viewClass({model: model, doc: this.doc});
      this.children[model.cid] = view;
      var position = _(this.collection.models).indexOf(model);
      if (position == 0) {
        $(this.el).prepend(view.el);
      } else {
        var previousElement = $(this.el).children().eq(position-1);
        $(previousElement).after(view.el);
      }
    },

    // Default handler for when a model is removed from the collection

    collectionRemove: function(model) {
      this.children[model.cid].remove();
      delete this.children[model.cid];
    }

  };

  // Internal

  // The collection events to bind to which list methods

  var collectionEventHandlers = {
    'reset': 'collectionReset',
    'add': 'collectionAdd',
    'remove': 'collectionRemove'
  };

  // Ensure that the view has obtained its view and collection objects
  // Check in its "options" property first, then in the object itself,
  // and then default to a new object

  function ensureViewClass(context) {
    context.viewClass = context.options.viewClass || context.viewClass || new Backbone.View();
  };

  function ensureCollection(context) {
    context.collection = context.options.collection || context.collection || new Backbone.Collection();
  };

});
/**
# Duckbone.FlashableView
Enables any view or application to manage a set of flash messages.
*/


ABINE_DNTME.define('duckbone/flashable_view',
['documentcloud/underscore', 'jquery', 'documentcloud/backbone', 'wycats/handlebars', 'duckbone/core'], 
function(_, $, Backbone, Handlebars, Duckbone) {

  Duckbone.FlashableView = {

    // #### property isFlashableView
    // Indicates a view that includes this mixin.
    isFlashableView: true,

    // #### function configureFlash
    // Accepts the following options:
    // - container - the DOM element to insert flash messages into
    // - noticeTemplate - a Handlebars template or template string for the notice
    // - alertTemplate - a Handlebars template or template string for the notice
    // - noticeDuration - a default duration for a flash notice
    // - fadeDuration - fade time for clearing notices
    configureFlash: function(options) {
      if (!options.container) throw("configureFlash called without a container");
      var flashOptionsDefaults = {
        noticeDuration: 10000, fadeDuration: 300
      }
      this.flashOptions = _.extend(flashOptionsDefaults, options);
      FlashView.fadeDuration = this.flashOptions.fadeDuration;
      if (options.noticeTemplate && typeof options.noticeTemplate == 'string') {
        FlashNoticeView.prototype.templateData = options.noticeTemplate;
      } else if (typeof options.noticeTemplate == 'function') {
        FlashNoticeView.prototype.template = options.noticeTemplate
      }
      if (options.errorTemplate && typeof options.errorTemplate == 'string') {
        FlashErrorView.prototype.templateData = options.errorTemplate;
      } else if (typeof options.errorTemplate == 'function') {
        FlashErrorView.prototype.template = options.errorTemplate
      }
    },

    // #### function flashNotice
    // - message - displays the given flash message
    // - duration - flash notice fades out after the given number of ms
    // - returns - the flash notice view
    flashNotice: function(message, duration) {
      duration = duration || 10000;
      var flash = new FlashNoticeView({
        model: {message: message},
        duration: duration
      });
      $(this.flashOptions.container).append(flash.el);
      $(flash.el).hide().slideDown(300);
      return flash;
    },

    // #### function flashAlert
    // - message - displays the given flash message
    // - returns - the flash notice view
    flashAlert: function(message) {
      var flash = new FlashAlertView({
        model: {message: message}
      });
      $(this.flashOptions.container).append(flash.el);
      $(flash.el).hide().slideDown(300);
      return flash;
    },

    // #### function activeFlashes
    // - returns - a jQuery object matching all the active flash notice and alert elements
    activeFlashes: function() {
      return $(this.flashOptions.container).find('div.flash_notice, div.flash_alert');
    },

    // #### function clearFlashes
    // Clears all flash notices and alerts.
    //
    // - immediately - instantly hides the flashes.  otherwise they are
    //   faded out according to the fadeDuration
    //
    // This is called on each loadView
    clearFlashes: function(immediately) {
      if (!this.flashOptions) return;
      var targets = this.activeFlashes();
      if (immediately) {
        targets.remove();
      } else {
        targets.fadeOut(this.flashOptions.fadeDuration);
      }
    }

  };

  // ### Internal Flash Notice and Error Views

   var FlashView = Backbone.View.extend({
     events: {
       'click a.close': 'dismiss'
     },
     fadeDuration: 300,
     dismiss: function(e) {
       if (e) e.preventDefault();
       $(this.el).fadeOut(this.fadeDuration);
       _.delay(_.bind(this.remove, this), this.fadeDuration);
       return false;
     }
   });

   Duckbone.include(FlashView.prototype, Duckbone.TemplateableView);

   var genericFlashTemplate = '<div class="message">{{{message}}}</div>\
     <div class="close"><a href="#" class="close">X</a></div>';

   var FlashNoticeView = FlashView.extend({
     className: 'flash_notice',
     templateData: genericFlashTemplate,
     afterInitialize: function() {
       if (this.options.duration) {
         _.delay(_.bind(this.dismiss, this), this.options.duration);
       }
     }
   });

   var FlashAlertView = FlashView.extend({
     className: 'flash_alert',
     templateData: genericFlashTemplate
   });

});
/**
# Duckbone.BindableField

This module enables a view to act as a form element that can bind bi-directionally to a model.
For example, an input field in a form can both reflect the model's state, and also update it
based on a user's input. The abstract form field base class `Duckbone.FormFieldBase` includes
this module, and any custom form field types used by an EditableView should also include this module.
*/


ABINE_DNTME.define('duckbone/bindable_field',
['documentcloud/underscore', 'jquery', 'documentcloud/backbone', 'wycats/handlebars', 'duckbone/core'], 
function(_, $, Backbone, Handlebars, Duckbone) {

  Duckbone.BindableField = {

    // #### property isBindableField
    // Indicates a view with this mixin included
    isBindableField: true,

    // ### Model/DOM Translation
    // If `jquery.val()` does not do the right thing for your form field, then
    // override the `get` and `set` functions to move data in and out of the DOM element.
    // If `jquery.val()` works, but the data must be transformed in or out of the model,
    // then simply override `modelToDomTransform` and `domToModelTransform`.

    // #### function get
    // - returns - The current value of the form field's DOM element
    get: function() {
      var val = this.domToModelTransform($(this.el).val());
      if (this.stripWhitespace && this.options.stripWhitespace !== false) val = val.replace(/^\s+|\s+$/g, '');
      return val;
    },

    // #### function set
    // - value - the new value to set the form field's DOM element to
    // - returns - nothing
    set: function(value) {
      $(this.el).val(this.modelToDomTransform(value));
    },

    // #### function modelToDomTransform
    // - modelValue - the given model value
    // - returns - the transformed value to set on the DOM element
    modelToDomTransform: function(modelValue) {
      return (modelValue !== undefined || modelValue !== NaN) ? modelValue : "";
    },

    // #### function domToModelTransform
    // - domValue - the given DOM element value
    // - returns - the transformed value to set on the model
    domToModelTransform: function(domValue) {
      return domValue;
    },

    // ### Binding

    // #### function bindToModel
    // - attr - the model attribute to bind to, falling back first to the view's `modelAttribute`,
    //   and then to the form field's `name` attribute.
    // - returns - nothing
    //
    // This method initializes the form field's value to the model value,
    // and then continually updates on any changes to that model attribute.
    // The DOM element will only be updated if the model attribute is a valid value
    // (non-undefined, non-NaN) and different from the existing field value.
    // This behavior arrests the circular firing of change events.
    bindToModel: function(attr) {
      attr = attr || this.options.modelAttribute || this.modelAttribute || this.options.name || this.name;
      if (!attr) throw("Duckbone.BindableField.bindToModel() called without a modelAttribute");
      this.modelAttribute = attr;
      var field = this, model = this.model, changeEvent = 'change:'+attr;
      field.form.view.weakBindToModel(changeEvent, function() { // bind to model changes
        var newValue = model.get(attr);
        if (newValue !== field.get() && Duckbone.ModelHelpers.isValidAttribute(newValue))
          field.set(newValue);
      });
      field.set(model.get(attr)); // initialize field to model value
    },

    // #### function bindToDOM
    // - attr - the model attribute to bind to, falling back first to the view's `modelAttribute`,
    //   and then to the form field's `name` attribute.
    // - changeEvent - the DOM event on the view's element to bind to changes.
    //   Defaults to 'change' which works for most input-style fields.
    // - el - the DOM element to bind to, defaulting to the view's top level element
    //
    // This method initializes the model's value to the form field value,
    // and then continually updates on any further changes to the field.
    // The model will generally be set to `null` on an empty field, not undefined.
    bindToDOM: function(attr, changeEvent, el) {
      attr = attr || this.options.modelAttribute || this.modelAttribute || this.options.name || this.name;
      if (!attr) throw("Duckbone.BindableField.bindToDOM() called without a modelAttribute");
      this.modelAttribute = attr;
      var changeEvent = this.options.changeEvent || this.changeEvent || 'change';
      el = el || this.el; // by default, bind to the parent element
      var field = this, model = this.model;
      $(el).bind(changeEvent, function() { // bind to DOM changes
        modelSet(model, attr, field.get());
      });
      modelSet(model, attr, field.get()); // initialize model to field value
    }
  };

  // ### Internal Functions

  // Make sure to never set a model value to undefined.
  // Undefined attributes are dropped when serialized to JSON,
  // which will have the wrong effect on the rails side.
  // These attrs will be ignored instead of validated against a null value.
  function modelSet(model, attr, val, silent) {
    silent = silent || false;
    if (typeof val == 'undefined') val = null;
    Duckbone.ModelHelpers.setOne.call(model, attr, val, {silent: silent});
  }

});
/**
# Duckbone.ValidateableField

This module should be included in any form field view that provides validation
capabilities as part of an EditableView. It provides methods to bind, run, and
display the results of validations.

EditableView considers client-side validations to a view-level responsibility and
so the existing Backbone Model validation facilities are not used. The form and its
fields itself are validated, rather than the model, since the same model can have
different validation requirements in different editing contexts. So the model always
reflects the data entered into the view, even if that input is invalid.

### Usage

This functionality is generally called by the FormManager itself during initialization,
and as part of the process of validating an entire form before submission. However,
in special cases the developer might call `validate` or `addError` on the field as needed.
*/


ABINE_DNTME.define('duckbone/validateable_field',
['documentcloud/underscore', 'jquery', 'documentcloud/backbone', 'wycats/handlebars', 'duckbone/core'], 
function(_, $, Backbone, Handlebars, Duckbone) {

  Duckbone.ValidateableField = {

    // #### property isValidateableField
    // Indicates a view with this mixin included
    isValidateableField: true,

    // #### function bindValidation
    // - validateEvent - the DOM event to trigger form validation, defaults to _change_.
    // - el - the DOM element to bind to, defaulting to the view's top level element
    // - returns nothing
    //
    // Bind the change event to the validator
    // The "validate" option can be either a string key to an existing validator
    // or any function that returns true or false
    // The "invalidMessage" option will display in the error span
    bindValidation: function(validateEvent, el) {

      // Setup
      var method = this.options.validate || this.options.required;
      if (!method) return; // bomb out if this field is not configured to validate
      var field = this;
      validateEvent = validateEvent || this.options.validateEvent || 'change';
      el = el || this.el;

      // Fix the problem where clicking submit creates new validation elements
      // that cause the submit button to jump around on the page.
      // Record the last mouse down time, and if it was very recent,
      // then delay validation until the mouse up event.
      var mouseTarget = $(window), mousedownAt = null;
      var mousedownHandler = function() {
        mousedownAt = new Date()
      };
      var mouseupHandler = function() {
        mouseTarget.unbind('mouseup', mouseupHandler);
        field.validate();
      };
      if (this.form.view && this.form.view.isBindableView) {
        this.form.view.weakBindTo(mouseTarget, 'mousedown', mousedownHandler);
      } else {
        mouseTarget.bind('mousedown', mousedownHandler);
      }

      // Bind to the validation event
      $(el).bind(validateEvent, function() {
        if (mousedownAt && (new Date()) - mousedownAt < 10) {
          mouseTarget.bind('mouseup', mouseupHandler); // wait for mouseup to validate
        } else {
          field.validate(); // validate immediately
        }
      });
    },

    // #### function validate
    // - method - the validation method to use. Accepts any string key present in
    //   `Duckbone.forms.validators`, or a function that returns true or false.
    //  The function will be called in the context of the field, and passed its
    //  current value.
    // - silent - a boolean indicating whether to display the validation errors.
    //   Defaults to true.
    // - returns - a boolean indicating whether the field is valid or not. Fields
    //   without validation methods will always return true.
    //
    // Validates this field using either the given method, or the method previously
    // configured on the field by the form. If the field is invalid, the invalid message
    // will be added to its DOM, unless `silent` is true.
    validate: function(method, silent) {
      silent = silent || false;
      if (!silent) this.clearErrors();
      this.valid = true; // assume valid unless method fails
      if (!this.options.validate && this.options.required) {
        this.options.validate = 'required';
        this.options.invalidMessage = this.options.invalidMessage || "Required"
      }
      method = method || this.options.validate;
      if (typeof method == "string") method = Duckbone.forms.validators[method];
      if (!method) return true; // no validation
      var invalidMessage = this.options.invalidMessage || "Invalid";
      if (method.call(this, this.get()) == false) this.valid = false;
      if (!this.valid && !silent) {
        this.addError(invalidMessage);
      } else {
        var val = this.get();
        if (Duckbone.ModelHelpers.isValidAttribute(val)) this.set(val);
      }
      return this.valid;
    },

    // #### addError
    // - message - the error message to display on the field.
    // - returns - nothing
    //
    // Display a validation error on this field. The error will be added to a
    // `ul` list of errors immediately after the form field. This element may
    // be styled by the developer as needed. Custom form fields can override
    // this method to provide additional display behavior as needed.
    // If the handlebars helper {{form_error}} is used, then this method will
    // append the errors to that element.
    addError: function(message) {
      if (!this.errorList) {
        this.errorList = $('<ul>').addClass('error');
        var error_div = $(this.el).parents('form').find('div.' + this.name + '_errors').get(0);
        if (error_div) {
          $(error_div).append(this.errorList);
        } else {
          $(this.el).after(this.errorList);
        }
      }
      this.errorList.append($('<li>').html(message));
    },

    // #### clearErrors
    // Clear all error messages.
    clearErrors: function() {
      if (this.errorList) {
        this.errorList.remove();
        this.errorList = null;
      }
    }
  };

});
/**
# Form Fields

This file defines all of the typical web form fields generally used with an EditableView.
It registers the Handlebars helpers to template a form, as well as the Backbone View classes
for each field type.

### Building Form Templates

Define a form template for each EditableView using the given Handlebars helpers. It is easiest
to name each form field with the name of its corresponding model attribute. Define each
form field's options in the view's `fields` object, as described in the docs for EditableView.
For example:

    <h1>New Post</h1>
    {{#form new_post}}
      {{form_label "headline" "Post Headline"}}
      {{form_text "headline"}}
      {{form_label "body" "Post Body"}}
      {{form_textarea "body"}}
      {{form_label "category" "Post Category"}}
      {{form_select "category"}}
      {{form_submit "new_post" "Create Post"}}
    {{/form}}

### Custom Field Types

Extend the class `Duckbone.FormFieldBase` to create additional field types. Add additional
functionality to `afterInitialize` and `createChildren`. Override `get` and `set` to interact
with the model.

Study the class `Duckbone.RadioSetFormField` for an example of a complex new field type.
*/


ABINE_DNTME.define('duckbone/form_fields',
['documentcloud/underscore', 'jquery', 'documentcloud/backbone', 'wycats/handlebars', 'duckbone/core', 'abine/intl'],
function(_, $, Backbone, Handlebars, Duckbone, intl) {

  // ### Handlebars Helpers

  // #### helper {{#form}}
  // - name - the form's `name` attribute
  // - block - a block template containing the body of the form
  //
  // Block helper to create a form tag. It's name attribute will be set to the given name,
  // and its class will be set to the same. In the event that the form is named in Rails-style
  // `new_foo` or `edit_foo`, then the class `foo` will also be added.
  Handlebars.registerHelper('form', function(name, block) {
    var className = name;
    className = className.replace(/^(new_)(.*)/, "$2").replace(/^(edit_)(.*)/, "$2");
    if (className != name) className += ' ' + name;
    return '<form action="#" name="'+name+'" class="'+className+'">\n' + block(this) + '\n</form>';
  });

  // #### helper {{form\_error\_banner}}
  // - text - the text to display in the form's error banner
  //
  // This helper creates a div containing a form error banner. This banner is shown
  // whenever the form is submitted and found to be invalid.
  Handlebars.registerHelper('form_error_banner', function(text) {
    text = (typeof text == "string") ? text : "mm_duckbone_default_form_error";
    text = intl.getText(text);
    return new Handlebars.SafeString('<div class="error_banner alert alert-error" style="display: none">' + Handlebars.Utils.escapeExpression(text) + '</div>');
  });

  // #### helper {{form\_error}}
  // - name - the form field for which to display validation errors
  //
  // Explicitly place the form error el rather than in the default location.
  // Otherwise it will be implicitly placed after the element.
  Handlebars.registerHelper('form_error', function(name) {
    return new Handlebars.SafeString('<div class="'+ Handlebars.Utils.escapeExpression(name) + '_errors"></div>');
  });

  // #### helper {{form\_label}}
  // - name - the name of the associated form field
  // - text - the text for the label
  //
  // Creates an HTML `<label>` tag that is associated to the given form field.
  // If label text is omitted, then the prettified form field name is used.
  // For example, `new_account_application` will become "New Account Application".
  Handlebars.registerHelper('form_label', function(name, text_or_block) {
    var text;
    if (typeof text_or_block == 'object') {
      text = name; // text is empty. use name
      text = text.replace(/^(.)/, function(c) {
        return c.toUpperCase(); // capitalize first character
      });
      text = text.replace(/(\_[a-z])/g, function($1) {
        return $1.toUpperCase().replace('_',' '); // capitalize each next word, replace _ with space
      });
    } else if (typeof text_or_block == "function") {
      text = new Handlebars.SafeString(text_or_block(this));
    } else {
      text = intl.getText(text_or_block);
    }
    return new Handlebars.SafeString('<label data-field-name="'+Handlebars.Utils.escapeExpression(name)+'">'+Handlebars.Utils.escapeExpression(text)+'</label>');
  });

  // #### helper {{form\_text}}
  // - name - the form field name
  //
  // Create an HTML tag of type `<input type="text">`
  Handlebars.registerHelper('form_text', function(name) {
    return new Handlebars.SafeString('<input class="form_text" type="text" name="'+Handlebars.Utils.escapeExpression(name)+'"/>');
  });

  // #### helper {{form\_select}}
  // - name - the form field name
  //
  // Create an empty HTML `<select>` tag. The form field view is responsible for populating
  // the child `option` elements according to its `selectOptions` property.
  Handlebars.registerHelper('form_select', function(name) {
    return new Handlebars.SafeString('<select class="form_select" name="'+Handlebars.Utils.escapeExpression(name)+'"></select>');
  });

  // #### helper {{form\_radio\_set}}
  // - name - the form field name
  //
  // The Duckbone radio set field is a high level abstraction on top of a collection of
  // HTML `<input type="radio>"` tags, that allows the developer to treat the entire radio
  // set as a single form field. The helper creates a container which is populated by the view.
  Handlebars.registerHelper('form_radio_set', function(name) {
    return new Handlebars.SafeString('<div class="form_radio_set" name="'+Handlebars.Utils.escapeExpression(name)+'"></div>');
  });

  // #### helper {{form\_checkbox}}
  // - name - the form field name
  //
  // Create an HTML tag of type `<input type="checkbox">`.
  Handlebars.registerHelper('form_checkbox', function(name) {
    return new Handlebars.SafeString('<input class="form_checkbox" type="checkbox" name="'+Handlebars.Utils.escapeExpression(name)+'"/>');
  });

  // #### helper {{form\_textarea}}
  // - name - the form field name
  //
  // Create an HTML tag of type `<textarea>`.
  Handlebars.registerHelper('form_textarea', function(name) {
    return new Handlebars.SafeString('<textarea class="form_textarea" name="'+Handlebars.Utils.escapeExpression(name)+'"></textarea>');
  });

  // #### helper {{form\_password}}
  // - name - the form field name
  //
  // Create an HTML tag of type `<input type="password">`.
  Handlebars.registerHelper('form_password', function(name) {
    return new Handlebars.SafeString('<input class="form_password" type="password" name="'+Handlebars.Utils.escapeExpression(name)+'"></input>');
  });

  // #### helper {{form\_submit}}
  // - name - the form field name, generally set to the same as the name of the form itself
  //
  // Create an HTML tag of type `<button>` for triggering form submission.
  Handlebars.registerHelper('form_submit', function(name, text) {
    if (typeof text == 'undefined') text = "mm_duckbone_default_form_submit_text";
    if (typeof text == 'object') text = "mm_duckbone_default_form_submit_text";
    text = intl.getText(text);
    return new Handlebars.SafeString('<button class="form_submit" name="' +
      Handlebars.Utils.escapeExpression(name)+'">'+Handlebars.Utils.escapeExpression(text)+'</button>');
  });

  // #### helper {{form\_submit\_with\_spinner}}
  // - name - the form field name, generally set to the same as the name of the form itself
  //
  // Create an HTML tag of type `<button>` for triggering form submission.
  // Also creates a small invisible spinner graphic, which the form submit view can use.
  Handlebars.registerHelper('form_submit_with_spinner', function(name, text, path) {
    path = (typeof path == 'object') ? '/pages/images/ajax_loader.gif' : path;
    if (typeof text == 'undefined') text = "mm_duckbone_default_form_submit_text";
    if (typeof text == 'object') text = "mm_duckbone_default_form_submit_text";
    text = intl.getText(text);
    var out = '<button class="form_submit" name="'+Handlebars.Utils.escapeExpression(name)+'">' +
      Handlebars.Utils.escapeExpression(text)+'</button>';
    out += '<img src="' + Handlebars.Utils.escapeExpression(path) + '" class="ajax_loading ' +
      Handlebars.Utils.escapeExpression(name)+'_loading" style="display: none"/>';
    return new Handlebars.SafeString(out);
  });

  // ### Duckbone.FormFieldBase
  //
  // This provides a base class for all form fields defined in this file, or extended by the developer.
  // It extends Backbone View with BindableField and ValidateableField.
  Duckbone.FormFieldBase = Backbone.View.extend();

  _.extend(Duckbone.FormFieldBase.prototype, Duckbone.BindableField, Duckbone.ValidateableField, {

    // #### function initialize
    // - options - `Backbone.View` initialization options
    //
    // Initializes all of the functionality in BindableField and ValidateableField. Calls the hooks
    // for further extension.
    initialize: function(options) {
      this.name = this.options.name || $(this.el).attr('name');
      this.form = this.options.form;
      if (options.elAttributes) $(this.el).attr(options.elAttributes);
      this.afterInitialize(options);
      this.createChildren();
      this.bindToModel();
      this.bindToDOM();
      this.bindValidation();
    },

    // #### function afterInitialize
    // - options - `Backbone.View` initialization options
    //
    // Provides a hook for the developer to add additional functionality to subclasses.
    afterInitialize: function(options) {},

    // #### function afterInitialize
    // Provides a hook for the developer to add additional functionality to subclasses.
    // This method is used when the field must add additional elements to the DOM
    // to implement its functionality.
    createChildren: function() {}

  });

  // ### TextFormField
  Duckbone.TextFormField = Duckbone.FormFieldBase.extend({

    // #### property type
    type: "text",
    stripWhitespace: true

  });

  // ### SelectFormField
  Duckbone.SelectFormField = Duckbone.FormFieldBase.extend({

    // #### property type
    type: "select",

    // #### function createChildren
    // Adds all of the child `<option>` elements to the `<select>` element,
    // as defined by the field's `selectOptions` property.
    createChildren: function() {
      var opts = this.options.selectOptions;
      var selectOptions = (typeof opts == "function") ? opts.call(this) : opts;
      for (var opt in selectOptions) {
        var optionEl = $('<option></option>').attr({value: opt});
        optionEl.html(Handlebars.Utils.escapeExpression(selectOptions[opt]));
        $(this.el).append(optionEl);
      }
    }
  });

  // ### RadioSetFormField
  Duckbone.RadioSetFormField = Duckbone.FormFieldBase.extend({

    // #### property type
    type: "radio_set",

    // #### function createChildren
    // Adds all of the child radio button elements to the field,
    // as defined by the field's `selectOptions` property.
    createChildren: function() {
      var opts = this.options.selectOptions;
      var selectOptions = (typeof opts == "function") ? opts.call(this) : opts;
      for (var opt in selectOptions) {
        var fieldId = this.form.name + '_' + this.name + '_' + opt;
        var optionEl = $('<div><input type="radio"/><label for="' + fieldId + '" class="radio_label"></label></div>');
        $(optionEl).find('input').attr({id: fieldId, name: this.name, value: opt});
        $(optionEl).find('label').html(Handlebars.Utils.escapeExpression(this.options.selectOptions[opt]));
        $(this.el).append(optionEl);
      }
      var el = this.el;
      $(this.el).find('input').bind('change', function() { // Retrigger input changes on top level div
        $(el).trigger('change');                           // which triggers our normal bindings to happen
      });
    },

    // #### function get
    // - returns - the value of the field.
    //
    // Overrides the super to return the value of the selected radio element
    get: function() {
      return $(this.el).find('input:checked').attr('value');
    },

    // #### function set
    // - newValue - the new field value
    //
    // Overrides the super to select the matching radio element
    set: function(newValue) {
      var checkedRadio = $(this.el).find('input:checked').get(0) // Find existing selection
      if (checkedRadio) checkedRadio.checked = false; // Wipe existing selection
      var radio = $(this.el).find('input[value="'+newValue+'"]').get(0); // Find the desired new selection.
      if (radio) radio.checked = true; // Set new selection
    }

  });

  // ### CheckboxFormField
  Duckbone.CheckboxFormField = Duckbone.FormFieldBase.extend({

    // #### property type
    type: "checkbox",

    get: function() {
      return $(this.el).is(':checked');
    },

    set: function(modelValue) {
      $(this.el).attr({checked: modelValue});
    }
  });

  // ### TextAreaFormField
  Duckbone.TextAreaFormField = Duckbone.FormFieldBase.extend({

    // #### property type
    type: "textarea",
    stripWhitespace: true

  });

  // ### PasswordFormField
  Duckbone.PasswordFormField = Duckbone.FormFieldBase.extend({

    // #### property type
    type: "password",

  });

  // ### SubmitFormField
  // Does not extend FormFieldBase since it's not a BindableField
  Duckbone.SubmitFormField = Backbone.View.extend();

  _.extend(Duckbone.SubmitFormField.prototype, Duckbone.BindableField, {

    // #### property type
    type: "submit",

    // #### function initialize
    // - options - Backbone.View options
    //
    // Sets up the submit button. Pass `submitForm: false` and the button will not
    // submit the form, otherwise it will naturally submit the form.
    // Additionally, a submit form field may be defined _outside_ of the form itself,
    // and the form may be passed in as an option to enable the button to submit it.
    initialize: function(options) {
      this.form = options.form;
      this.name = options.name;
      this.spinnerEl = $(this.el).next('img.ajax_loading');
      $(this.el).bind('click', _.bind(this.bindDisableAndSpin, this));
      this.submitForm = (typeof options.submitForm == "undefined") ? true : options.submitForm;
    },

    // #### function bindDisableAndSpin
    // Bind the method `disableAndSpin` in response to a button click. If the form is valid,
    // and it calls `Model.save()` then the handler will fire.
    bindDisableAndSpin: function(e) {
      e.preventDefault();
      this.form.model.off('sync:create', this.disableAndSpin); // Don't double up bindings
      this.form.model.off('sync:update', this.disableAndSpin);
      this.form.model.on('sync:create', this.disableAndSpin, this); // Disable and spin
      this.form.model.on('sync:update', this.disableAndSpin, this); // if syncing starts
      if (this.submitForm) this.form.submit();
    },

    // #### function disableAndSpin
    // Add the class 'loading' to the button, which the developer should use to make the button
    // appear disabled. Also, this method steals all subsequent clicks on the button until the
    // sync completes, to provide double click protection.
    disableAndSpin: function() {
      this.form.model.off('sync:create', this.disableAndSpin);
      this.form.model.off('sync:update', this.disableAndSpin);
      if (this.spinnerEl) $(this.spinnerEl).fadeIn();
      $(this.el).addClass('loading');
      $(this.el).unbind('click');
      $(this.el).bind('click', this.stealClick);
      this.form.model.on('sync:complete', this.syncComplete, this);
    },

    // #### function stealClick
    stealClick: function(e) {
      e.preventDefault(); return false;
    },

    // #### function syncComplete
    // Unbind handlers and restore normal state.
    syncComplete: function() {
      $(this.el).unbind('click');
      $(this.el).removeClass('loading');
      if (this.spinnerEl) $(this.spinnerEl).fadeOut();
      $(this.el).bind('click', _.bind(this.bindDisableAndSpin, this));
    }
  });

  // ### Field Types Registry
  //
  // Create a registry of all form field types. Add additional user-defined field types here.
  Duckbone.forms.fieldTypes = {
    text: Duckbone.TextFormField,
    select: Duckbone.SelectFormField,
    radio_set: Duckbone.RadioSetFormField,
    checkbox: Duckbone.CheckboxFormField,
    textarea: Duckbone.TextAreaFormField,
    password: Duckbone.PasswordFormField,
    submit: Duckbone.SubmitFormField
  };

});
/**
# Duckbone.EditableView

This module can be added to any view that contains form elements.
It will take over the form found in its template and all of its fields,
providing most of the functionality that is needed for taking and saving user input.

## Usage

To use EditableView, take the following steps:

- Include Duckbone.Syncable in the model you are editing, either directly or through a base class.
- Use the supplied Handlebars helpers to add form fields to your template.
- Add a "fields" object to your view to define the form's behavior.
- The `initialize()` method provided by Duckbone.ViewLifecycleExtensions will take care of the rest of form setup.
- Provide additional functionality in the `afterInitialize()` method

### Defining Fields

The `fields` object is made up of keys that map to fields in your form.
The key of each field should be identical to the `name` attribute in the field DOM element.
The provided Handlebars form field helpers use this convention.

There are some standard attributes that can be added to all fields:

- `modelAttribute` - this indicates which attribute of the model this field binds to.
  It defaults to the same as the form field name, so it is best to map field names exactly
  to attribute names if possible.
- `elAttributes` - a set of html attributes that will be added to the field,
  ie. size, maxLength, placeholder, rows, cols, etc.
- `validate` - a string representing one of the validators,
  ie. 'email', 'phone', etc.
- `validate` - or a callback function returning a boolean indicating field validity
- `required` - validates that content was entered into the field

Additionally, some fields take special options. Select and Radio Set fields accept:

- `selectOptions` - an object representing ids and values of the selection options
- `selectOptions` - or a function that returns the above object

A Submit Button accepts:

- `submitForm` - a boolean that determines whether the submit button automatically
  submits the form or not. The developer may deactivate this and bind his own behavior.

### The Form Manager

The EditableView's default initializer calls `Duckbone.EditableView.createForm()` which
creates the FormManager object, `this.form`, on the view. The FormManager creates views
for each form field, and binds all event behaviors. The developer may interact with each
field individually by calling `this.form.getField("field_name")`.

### Form Binding Behavior

Each form field will automatically bind changes on itself to its model. This behavior is
defined by each form field type, and implemented by Duckbone.BindableField.

Each field will also bind its validation check, if defined. This behavior is defined on
all form field types, and implemented by Duckbone.ValidateableField.

Form submission events are captured by the EditableView's FormManager,
and `Duckbone.FormManager.submit()` is called instead. This method will first ensure that
the form is valid, and then save the model. Otherwise, invalid fields will show their errors.

To define alternate behavior on form submission, it is simplest to simply omit any submit buttons
inside the form itself, and bind form submission to another button defined outside of the form itself.

### Default Model Sync Behavior

Be default, the following events trigger these behaviors on the form:

- _form submit_ - Validations are run, and if the form is valid, `model.save()` is called
- _model sync:create_ - All validation errors are cleared
- _model sync:update_ - All validation errors are cleared
- _model sync:invalid_ - The errors object in the response is inspected and
  errors are displayed on all invalid fields
- _model sync:success_ - No default operation. The user should bind appropriate behavior,
  ie. providing feedback or navigating to the next page.

### Defining Additional Model Sync Behavior

Create a `modelSyncEvents` object, in which the keys are model events, and the values
string names of methods on the view. The callbacks will be run in the context of the view.

If you wish to redefine default behavior, the easiest approach is to
override `defaultModelSyncSavingHandler` or `defaultModelSyncInvalidHandler`.

### Cloning Models

Editable view clones the given model to create a scratch model to work with in the form.
When the model is saved, then all attributes from the scratch model are copied back to
the original model. The approach has a number of advantages:

- The original model does not change until the form is completed
- The original model is not changed if a form is abandoned
- All event handlers are bound to the clone and do not need to be cleaned up
- Temporary form values do not need to persist on the original model

This clone is created by `cloneModelForEditing()` in the default initializer created by
ViewLifecycleExtensions. If you are not using the lifecycle for some reason, then call this
method by hand.

If a model has additional properties or associations that are necessary for the form to function,
then the developer should override the model's `clone()` method so that it creates a clone with
the desired properties.

### After Save Destination

When the EditableView's model is successfully saved, the original model can be assigned to
an afterSaveDestination. The model can be added to a collection, or set as a property on another model.
Define the view's `afterSaveDestination` property as an object in which the key is either
`collection` or `model`. If it is a model, also define the `association` key with the value the
string name of the property. If there are multiple after save destinations, then an array of these
objects may also be given.

Usage examples:

    view.afterSaveDestination = {
      collection: myCollectionObject
    };
    view.afterSaveDestination = {
      model: myModelObject,
      association: 'propertyName'
    };
    view.afterSaveDestination = [
      {collection: myCollectionObject},
      {model: myModelObject, association: 'propertyName'}
    ];

*/


ABINE_DNTME.define('duckbone/editable_view',
['documentcloud/underscore', 'jquery', 'documentcloud/backbone', 'wycats/handlebars', 'duckbone/core'], 
function(_, $, Backbone, Handlebars, Duckbone) {

  Duckbone.EditableView = {

    // ### Mixin

    // #### property isEditableView
    // Indicates a view that includes EditableView
    isEditableView: true,

    // #### function included
    // Also includes ViewLifecycleExtensions, which automatically calls several of these
    // methods during the view's initialize and remove
    included: function() {
      if (!this.hasViewLifecycleExtensions) {
        Duckbone.include(this, Duckbone.ViewLifecycleExtensions);
      }
    },

    // ### Form Set Up

    // #### function createForm
    // Accepts the following options:
    //
    // - el -  the form element to use, defaults to the first form element found on the view.
    // - fields - the fields description object, defaults to `this.fields`
    // - model - the model to bind the form to, defaults to `this.model`
    // - returns - the new FormManager
    //
    // This method creates the view's FormManager which builds all of the form field views.
    createForm: function(options) {
      options = options || {};
      var formElement = options.el || $(this.el).find('form').get(0) || $(this.el).get(0);
      var fieldOptions = options.fields || this.options.fields || this.fields;
      var model = options.model || this.model;
      var form = new Duckbone.FormManager({
        el: formElement,
        fields: fieldOptions,
        model: model || new Backbone.Model(),
        view: this
      });
      this.form = this.form || form;
      return form;
    },

    // #### function bindModelSyncEvents
    // Binds the events triggered by a Syncable-enhanced Model to those default
    // behaviors defined below. Additionally binds user defined model sync events,
    // defined on the view's `modelSyncEvents` property.
    bindModelSyncEvents: function() {
      bindEvents(this, defaultModelSyncEvents);
      if (this.modelSyncEvents) bindEvents(this, this.modelSyncEvents);
    },

    // ### Model cloning

    // #### function cloneModelForEditing
    // This function clones the model for the workspace, and binds a handler to copy back
    // attributes to the original model when saved. It also sets up the model's after save destination.
    // After this method has been called, `this.model` refers to the clone, and `this.originalModel`
    // refers to the original model.
    cloneModelForEditing: function() {
      if (!this.model) throw("Found not, has your model been. Cloned, it can not be.");
      this.originalModel = this.model;
      this.model = this.originalModel.clone();
      this.weakBindToModel('sync:success', function() {
        var attrs = _.clone(this.model.attributes);
        _(this.form.fields).each(function(field) {
          if (field.options.temporary) {
            delete attrs[field.modelAttribute];
          }
        });
        this.originalModel.set(attrs);
        copyToDestination.call(this);
      }, this);
    },

    // #### function expireClone
    // The function is called by the ViewLifecycleExtensions when the view's `remove()` method
    // is called. It expires the clone and makes it unusable by replacing its get and set methods.
    // The developer should be careful not to retain references to the model clone after tearing down the
    // EditableView. It also moves the view's model reference to the originalModel, so that it is
    // accessable to any `afterRemove()` callbacks.
    expireClone: function() {
      if (!this.originalModel) return;
      this.model.clear({silent: true});
      var explode = function() {
        throw('Attempted get/set on expired clone from Duckbone.EditableView. Use original model instead.');
      }
      this.model.set = explode;
      this.model.get = explode;
      this.model = this.originalModel;
      delete this.originalModel;
    },

    // #### function resetModel
    // Discards any edits to the clone, and restore its attributes from the original model.
    resetModel: function() {
      if (!this.originalModel) throw("Cloned not, has your model been. Reset, we can not do.")
      this.model.set(this.originalModel.attributes);
    },

    // ### Default Model Sync Handlers
    // These handlers are automatically bound to the model's sync events.

    // #### function defaultModelSyncSavingHandler
    // Called on either a _sync:create_ or _sync:update_ event. The default behavior is
    // to clear all of the validation error notices on the form.
    defaultModelSyncSavingHandler: function(response) {
      $(this.el).find('div.error_banner').hide();
      _.each(this.form.fields, function(field) {
        if (field.isValidateableField) {
          field.clearErrors();
        }
      }, this);
    },

    // #### function defaultModelSyncInvalidHandler
    // Called on a _sync:invalid_ event. The default behavior is to inspect the
    // validation errors on the model, and add error notices to all invalid fields.
    // It also scrolls the page so that the top of the form is visibile,
    // in the event that it contains an error notice bar.
    defaultModelSyncInvalidHandler: function(response) {
      $(this.el).find('div.error_banner').show();
      _.each(this.model.errors, function(error, modelAttribute) {
        var field = this.form.getFieldForModelAttribute(modelAttribute);
        _.each(this.model.errors[modelAttribute], function(error) {
          if (field && field.isValidateableField) {
              field.addError(error);
          } else {
            _.log("Cannot add error for field " + modelAttribute + ": " + error);
          }
        }, this);
      }, this);
      $(this.el).find('div.error_banner').show();
      scrollToTop(this.el);
    },

    // #### function defaultModelSyncSuccessHandler
    // Called on a _sync:success_ event. No operation. Reserved for future use.
    defaultModelSyncSuccessHandler: function(response) {},

    // #### function defaultModelSyncErrorHandler
    // Called on a _sync:error_ event. No operation. Reserved for future use.
    // Note that SyncableModel will also trigger sync error events on the top level
    // Duckbone object so that the entire application may respond to fatal server errors.
    defaultModelSyncErrorHandler: function(response) {},

    // #### function defaultModelSyncCompleteHandler
    // Called on a _sync:complete_ event. No operation. Reserved for future use.
    defaultModelSyncCompleteHandler: function(response) {}

  };

  // ## FormManager
  // The Form Manager creates all of the subviews for each form field and manages their state and validation.
  // Normally this object is not used much in practice, but in a view one can
  // access individual form fields with it like: `this.form.getField('name')`.
  // The developer may also wish to explicitly set() and get() field values at times.

  // #### Constructor
  // Accepts the following options:
  //
  // - el - the DOM element to use, defaulting to the first form element in the view
  // - fields - the fields description object to use
  // - model - the Backbone Model to bind to, defaulting to a new model
  // - view - a reference to the parent view
  Duckbone.FormManager = function(options) {
    this.el = options.el || $('<form action="#"></form>').get(0);
    this.fieldOptions = _.clone(options.fields) || {};
    this.model = options.model || new Backbone.Model({});
    this.view = options.view;
    this.name = $(this.el).attr('name') || 'untitled_form';
    this.createFields();
  };

  // Mix in Backbone.Events
  _.extend(Duckbone.FormManager.prototype, Backbone.Events);

  _.extend(Duckbone.FormManager.prototype, {

    // #### function createFields
    // Create form field views for all those defined in the fields object on the view.
    createFields: function() {
      // Create all of the model fields
      var field, form = this, name;
      for (name in this.fieldOptions) {
        field = this.createField(name, this.fieldOptions[name]);
      }
      // Create the submit button fields
      $(this.el).find('button.form_submit').each(function() {
        name = $(this).attr('name');
        if (!form.fields[name]) {
          field = new Duckbone.forms.fieldTypes['submit']({
            name: name, el: this, form: form
          });
          form.fields[field.name] = field;
        }
      });
      // Assign ids and names to form labels
      $(this.el).find('label').each(function() {
        var name = $(this).attr('data-field-name');
        if (name && form.fields[name]) $(this).attr('for', idFor(form, form.fields[name]));
      });
    },

    // #### function createField
    // - name - the form field name
    // - fieldOptions - the field description object
    // - returns - the newly created form field view
    //
    // Create an individual form field view for the given field name and options.
    // The field type is either determined by the "type" attribute in options,
    // or by a heuristic in looking at the form field class attribute.
    // Fields defined by Duckbone are classed "form_[type]" by the form helpers.
    createField: function(name, fieldOptions) {
      this.fields = this.fields || {};
      var el, formFieldClass, field;
      el = $(this.el).find(selectorFor(name)).get(0);
      if (el) {
        formFieldClass = (fieldOptions['type']) ? Duckbone.forms.fieldTypes[fieldOptions['type']] : getFormFieldClass(el);
        if (formFieldClass) {
          field = new formFieldClass(_.extend(fieldOptions, {el: el, model: this.model, form: this, name: name} ));
          field.name = name;
          $(field.el).attr('id', idFor(this, field));
        } else {
          throw ("Bad form field type " + name);
        }
      } else {
        throw ("Form field " + name + " defined in view, missing in template")
      }
      this.fields[name] = field;
      return field;
    },

    // #### function getField
    // - name - the form field name
    // - returns - the form field view
    //
    // Return a field view from the form for the given name.
    // The developer may then use get() and set() on this field to manipulate its value.
    getField: function(name) {
      return this.fields[name];
    },

    // #### function getFieldForModelAttribute
    // - modelAttibute - the attribute on the model
    // - returns - the form field view bound to the given attribute
    //
    // Return the field view that corresponds to the given model attribute.
    getFieldForModelAttribute: function(modelAttribute) {
      return _.detect(this.fields, function (field) {
        return field.modelAttribute == modelAttribute;
      }, this);
    },

    // #### function validate
    // - returns - a boolean indicating the validity of the form
    //
    // Validate all fields in the form according to their validation rules.
    // After this is called, the view will have a boolean property "isValid"
    validate: function() {
      this.isValid = true
      var fieldValid;
      for (var field in this.fields) {
        fieldValid = this.fields[field].validate ? this.fields[field].validate() : true;
        if (!fieldValid) this.isValid = false;
      }
      return this.isValid;
    },

    // #### function submit
    // - options - An options object for `model.save`
    //
    // Submits the form. First validate() is called on all fields,
    // and then if the form is valid, then the model is saved to the server.
    // The given options object is passed directly to `model.save` and can contain
    // `success` and `error` callbacks. However, these callbacks are deferred one tick
    // so that all of the model sync event handlers have run first, and the callbacks
    // are called subsequent to all default form behaviors.
    submit: function(options) {
      if (this.validate()) {
        options = _.clone(options) || {};
        _.each(['success','error'], function(callbackName) {
          if (typeof options[callbackName] == 'function') {
            var originalCallback = options[callbackName];
            options[callbackName] = function(model, response) {
              _.defer(_.bind(originalCallback, model, model, response));
            }
          }
        }, this);
        this.model.save(null, options);
      } else {
        _.log("Form is invalid.")
        $(this.el).find('div.error_banner').show();
        scrollToTop(this.el);
      }
    }
  });

  // ### Internal Functions

  // Copys the model to the final place it should arrive at once it is saved,
  // as defined by the view's 'afterSaveDestination' property.
  // Called by `EditableView.cloneModelForEditing`.
  var copyToDestination = function() {
    var dests =  this.options.afterSaveDestination || this.afterSaveDestination;
    if (!dests) return;
    if (!(dests instanceof Array)) {
      dests = [dests];
    }
    _.each(dests, function(dest) {
      if (dest.collection) {
        dest.collection.add(this.originalModel);
      } else {
        var setterName = 'set' + dest.association.slice(0, 1).toUpperCase() + dest.association.slice(1);
        dest.model[setterName].call(dest.model, this.originalModel);
      }
    }, this);
  };

  // Default model sync events that are bound when bindModelSyncEvents() is called
  var defaultModelSyncEvents = {
    'sync:create': 'defaultModelSyncSavingHandler',
    'sync:update': 'defaultModelSyncSavingHandler',
    'sync:success': 'defaultModelSyncSuccessHandler',
    'sync:invalid': 'defaultModelSyncInvalidHandler',
    'sync:error': 'defaultModelSyncErrorHandler',
    'sync:complete': 'defaultModelSyncCompleteHandler'
  };

  // Binds default events to the model
  function bindEvents(view, events) {
    for (var event in events) {
      var method = view[events[event]];
      if (typeof method == 'function') view.weakBindToModel(event, method, view);
    }
  };

  // Concatenates the form and field names into an id for each form field DOM element
  function idFor(formManager, field) {
    return formManager.name + '_' + field.name;
  };

  // Returns a jQuery selector for the given form field name
  function selectorFor(name) {
    return '[name=' + name + ']';
  };

  // Inspects a DOM element and determines the form field type based upon its class name.
  // The handlebars form helpers insert a class name, ie "form_text" or "form_select"
  function getFormFieldClass(el) {
    var type = /^form_(.*)/.exec($(el).attr('class'))[1];
    return Duckbone.forms.fieldTypes[type];
  }

  // ### Scrolling

  // Time to scroll to the top of the form in milliseconds
  var SCROLL_TIME = 400;
  // Easing function to use
  var SCROLL_EASING = 'swing';
  // Number of pixels above the top of the form element we scroll to
  var SCROLL_BREATHING_ROOM = 100;

  // Smooth scrolls the browser window so that the top of the form is visible.
  // Called when a form is invalid and all errors should be shown.
  function scrollToTop(el) {
    var offset = $(el).offset().top;
    offset = (offset > SCROLL_BREATHING_ROOM) ? offset - SCROLL_BREATHING_ROOM : 0;
    $('html body').animate({ scrollTop: offset }, SCROLL_TIME, SCROLL_EASING);
  }

});
ABINE_DNTME.define('duckbone/form_validations',
['documentcloud/underscore', 'jquery', 'documentcloud/backbone', 'wycats/handlebars', 'duckbone/core'], 
function(_, $, Backbone, Handlebars, Duckbone) {

  // These validators can be used with any form field by including the fields options
  // fields: {
  //   amount: {
  //     modelAttribute: 'amount',
  //     validate: 'money'
  //   }
  // }

  // Thx to andyet/happy.js for a few of these

  Duckbone.forms = Duckbone.forms || {};
  Duckbone.forms.validators = Duckbone.forms.validators || {};

  _.extend(Duckbone.forms.validators, {

    required: function(val) {
      return !(val == undefined || val == null || val == "");
    },
    USPhone: function (val) {
      return /^\(?(\d{3})\)?[\- ]?\d{3}[\- ]?\d{4}$/.test(val)
    },
    date: function (val) {
      return /^(?:0?[1-9]|1[0-2])\/(?:0?[1-9]|[12][0-9]|3[01])\/\d{2}?\d{2}$/.test(val);
    },
    email: function (val) {
      return /^([A-Za-z0-9\._%\+\-]+)@([A-Za-z0-9\-]+\.)+([A-Za-z]{2,})$/.test(val);
    },
    numeric: function (val) {
      return (parseFloat(val) != NaN);
    }

  });

});
ABINE_DNTME.define('duckbone/simple_queue',
['documentcloud/underscore', 'documentcloud/backbone', 'wycats/handlebars', 'duckbone/core'], 
function(_, Backbone, Handlebars, Duckbone) {

  Duckbone.SimpleQueue = function(options) {
    this.interval = options.interval || 400;
    this.elements = [];
    this.callback = options.callback;
    this.running = false;
  };

  _.extend(Duckbone.SimpleQueue.prototype, {

    // Pushes a new element into the queue.
    // The queue will start as soon as an element is added.

    push: function(el) {
      this.elements.push(el);
      if (this.comparator) _(this.elements).sortBy(this.comparator);
      if (!this.running) {
        this.running = true;
        this.timer = setInterval(_.bind(this.shift, this), this.interval);
      }
    },

    // Shifts an element out of the queue. One element will be shifted every interval.
    // The element is shifted out and then the queue's callback function is called in its context.

    shift: function() {
      if (this.elements.length == 0) {
        this.running = false;
        clearInterval(this.timer);
      } else {
        var element = this.elements.shift();
        this.callback.call(element, element, this);
      }
    }
  });

});
/**
# Duckbone Base Classes

These base classes extend `Backbone.Model`, `Backbone.Collection` and `Backbone.View`
to include the most commonly used modules of Duckbone.  Most apps will
simply extend these base classes instead of the Backbone base classes directly
unless they desire a high degree of customized behavior.

This file also defines some other Duckbone core classes:

* `Duckbone.FormView`, which mixes in all the
functionality required for editing and/or saving models via web forms.
* `Duckbone.Application`, which extends the Backbone router with declarative route actions
and flash notice/alert management

*/


ABINE_DNTME.define('duckbone/base_classes',
['documentcloud/underscore', 'documentcloud/backbone', 'wycats/handlebars', 'duckbone/core', 'jquery', 'abine/timer'],
function(_, Backbone, Handlebars, Duckbone, $, timer) {

  // ### Duckbone.Model
  // extends `Backbone.Model` with `AssociableModel`, `ModelHelpers`
  // and `Syncable`
  Duckbone.Model = Backbone.Model.extend();

  Duckbone.include(Duckbone.Model.prototype,
    Duckbone.AssociableModel,
    Duckbone.ModelHelpers,
    Duckbone.Syncable
  );

  // ### Duckbone.Collection
  // extends `Backbone.Collection` with `CollectionHelpers` and `Syncable`
  Duckbone.Collection = Backbone.Collection.extend();

  Duckbone.include(Duckbone.Collection.prototype,
    Duckbone.CollectionHelpers,
    Duckbone.Syncable
  );


  // ### Duckbone.View
  // extends `Backbone.View` with `TemplateableView` and `BindableView`
  Duckbone.View = Backbone.View.extend();

  Duckbone.include(Duckbone.View.prototype,
    Duckbone.TemplateableView,
    Duckbone.BindableView
  );

  // transfer all events on this view to passed view
  Duckbone.View.prototype.transferEvents = function(events, target) {
    _.each(events, _.bind(function(event) {
      if (event != 'resize') {
        this.on(event, function (opt1, opt2) {
          target.trigger.apply(target, [event, opt1, opt2]);
        });
      } else {
        this.on(event, function(options) {
          timer.setTimeout(_.bind(function(){target.trigger.apply(target, [event, options]);}, this), 50);
        });
      }
    }, this));
  };

  // override MAKE method to use correct document object.  otherwise nested views will fail in firefox.
  Backbone.View.prototype.make = function(tagName, attributes, content) {
    var el = (this.doc||this.options.doc||document).createElement(tagName);
    if (attributes) $(el).attr(attributes);
    if (content) $(el).html(content);
    return el;
  };

  // ### Duckbone.FormView
  // further extends `Duckbone.View` with `EditableView`
  Duckbone.FormView = Duckbone.View.extend();

  Duckbone.include(Duckbone.FormView.prototype,
    Duckbone.EditableView
  );

  // ### Duckbone.ListView
  // Provides rendering of sets of submodels for lists and feeds
  Duckbone.ListView = Backbone.View.extend();

  Duckbone.include(Duckbone.ListView.prototype,
    Duckbone.ListableView, Duckbone.BindableView
  );

  // ### Duckbone.Application
  // Provides core functionality of a duckbone app
  Duckbone.Application = Backbone.Router.extend()

  Duckbone.include(Duckbone.Application.prototype,
    Duckbone.RouteableApplication,
    Duckbone.FlashableView)

});
/**
# Duckbone.Security

*/


ABINE_DNTME.define('duckbone/security',
['documentcloud/underscore', 'jquery', 'duckbone/core'],
function(_, $, Duckbone) {


  function renderOnUnlock(obj, View, args) {
    if (obj._unlockHooked) return;

    obj._unlockHooked = true;
    var fn = function(){blankWhenLocked(obj, View, args);};
    ABINE_DNTME.App.on('unlocked', fn);
    View.prototype._oldBeforeRemove = View.prototype.beforeRemove;
    View.prototype.beforeRemove = function(){
      View.prototype.beforeRemove = View.prototype._oldBeforeRemove;
      if (View.prototype.beforeRemove) {
        View.prototype.beforeRemove.apply(obj, arguments);
      }
      obj._unlockHooked = false;
      ABINE_DNTME.App.off('unlocked', fn);
    };
  }

  function blankWhenLocked(obj, View, args) {
    if (ABINE_DNTME.App.isLocked()) {
      renderOnUnlock(obj, View, args);
      return obj;
    } else {
      View.prototype._oldRender.apply(obj, args);
    }
  }

  function loginWhenLocked(View, args) {
    if (ABINE_DNTME.App.isLocked()) {
      ABINE_DNTME.App.renderPage();
    } else {
      View._oldRouteAction.apply(View, args);
    }
  }

  Duckbone.Security = {
    showBlankWhenLocked: function(View) {
      View.prototype._oldRender = View.prototype.render;
      View.prototype.render = function(){blankWhenLocked(this, View, arguments);};
    },
    showLoginWhenLocked: function(View) {
      View._oldRouteAction = View.routeAction;
      View.routeAction = function(){loginWhenLocked(View, arguments);};
    }

  };

});


























// Create a function to be called later to require duckbone in dependency order ...

ABINE_DNTME.requireDuckbone = function(callback) {

  ABINE_DNTME.require([
    'duckbone/core', 
    'duckbone/model', 
    'duckbone/syncable',
    'duckbone/collection',
    'duckbone/routeable_application',
    'duckbone/view_lifecycle_extensions',
    'duckbone/handlebars_extensions',
    'duckbone/idme_template_registry',
    'duckbone/nestable_view',
    'duckbone/templateable_view',
    'duckbone/stylizeable_view',
    'duckbone/pageable_view',
    'duckbone/pageable_collection',
    'duckbone/bindable_view',
    'duckbone/listable_view',
    'duckbone/flashable_view',
    'duckbone/bindable_field',
    'duckbone/validateable_field',
    'duckbone/form_fields',
    'duckbone/editable_view',
    'duckbone/form_validations',
    'duckbone/simple_queue',
    'duckbone/base_classes'
  ], function() {
    //console.log("Duckbone loaded in " + (Date.now() - ABINE_DNTME.startTime) + "ms");
    callback();
  });

}
;































ABINE_DNTME.define('models', [], function() {
  return {};
});

// Create a function to be called later to actually require models

ABINE_DNTME.requireModels = function(callback) {

  ABINE_DNTME.require([
    'abine/models/base',
    'abine/models/blacklist',
    'abine/models/disposableCard',
    'abine/models/disposableEmail',
    'abine/models/disposablePhone',
    'abine/models/dntStats',
    'abine/models/preference',
    'abine/models/autofillFeedback',
    'abine/models/mappedForm',
    'abine/models/pageData',
    'abine/models/mappedField',
    'abine/models/mappingMenu',
    'abine/models/shieldedCard',
    'abine/models/shieldedEmail',
    'abine/models/shieldedPhone',
    'abine/models/tabData',
    'abine/models/address',
    'abine/models/account',
    'abine/models/identity',
    'abine/models/realCard',
    'abine/models/globalData',
    'abine/models/feedback',
    'abine/models/trackerInfo',
    'abine/models/advt',
    'abine/models/extensionVersion',
    'abine/models/loginUrl'
  ], function () {
    callback();
  });

};

ABINE_DNTME.define('abine/models/base',
    ['abine/contentMessenger', 'documentcloud/backbone', 'duckbone/core', 'models'],
    function(abineContentMessenger, Backbone, Duckbone, models) {

  var messenger;

  function getMessenger() {
    if (!messenger) {
      messenger = ABINE_DNTME.contentMessenger || new abineContentMessenger.ContentMessenger({
        messengerId: "model" // id for firefox
      });
    }
    return messenger;
  }

  // A sync function for all models and collections that uses the messenger

  var syncMethod = function(method, model, options) {

    // Build payload for create and update
    var payload = options.payload||{}, eventName, responseEventName, errorEventName;
    if (model && (method == 'create' || method == 'update')) {
      payload = model.toJSON();
      delete payload.messenger;
    }

    // Build event name
    if (options.eventName) {
      eventName = options.eventName;
    } else {
      if (model.model) {
        eventName = method + ":" + model.model.storageEntity;
      } else {
        eventName = method + ":" + model.storageEntity;
      }
      if (method != "create") {
        eventName += ":" + model.id;
      }
    }
    responseEventName = 'response:' + eventName.split(':').slice(1).join(':');
    errorEventName = 'error:' + eventName.split(":").slice(1).join(":");

    var msgr = model.messenger;
    if (!msgr && model.collection && model.collection.messenger) {
      msgr = model.collection.messenger;
    }
    if (!msgr) {
      msgr = getMessenger();
    }
    // Subscribe to response
    var successHandler = function(payload) {
      msgr.off(responseEventName, successHandler);
      msgr.off(errorEventName, errorHandler);

      delete model.errors;
      options.success.call(model, payload, 200, null);
      model.trigger('sync:success', payload);

      //msgr.trigger('response:' + eventName); AL: What is this?
    };

    var errorHandler = function(payload){
      msgr.off(errorEventName, errorHandler);
      msgr.off(responseEventName, successHandler);

      delete model.errors;
      options.error.call(model, payload, 500, null);
      model.trigger('sync:error', payload);
    };

    msgr.on(responseEventName, successHandler, model);
    msgr.on(errorEventName, errorHandler, model);

    // Send message
    model.trigger('sync:' + method.toLowerCase());
    msgr.send(eventName, payload);
  };

  var isTrue = function(attributeKey){
    var attribute = this.get(attributeKey); // BL: null or undefined if it doesn't exist?
    if(attribute === "1" || attribute === "true" || attribute === true || attribute === 1){
      return true;
    } else {
      return false; // BL: will return false if null or undefined - is this desired?
    }
  };

  var isFalse = function(attributeKey){
    return !isTrue(attributeKey);
  };

  // BaseModel that all models should extend

  var BaseModel = Backbone.Model.extend({
    sync: syncMethod,
    getMessenger: getMessenger,
    isTrue: isTrue,
    isFalse: isFalse
  });

  Duckbone.include(BaseModel.prototype,
    Duckbone.AssociableModel,
    Duckbone.ModelHelpers);

  // BaseCollection that all collections should extend

  var BaseCollection = Backbone.Collection.extend({
    sync: syncMethod,
    getMessenger: getMessenger,
    
    // ### function fetchAll(options)
    // A convenience method to fetch all existing entities into this collection
    fetchAll: function(options) {
      options = options || {};
      options.eventName = ['read', this.model.prototype.storageEntity, 'all'].join(':');
      this.fetch(options);
    }
  });
  Duckbone.include(BaseCollection.prototype,
    Duckbone.CollectionHelpers);

  models.BaseModel = BaseModel;
  models.BaseCollection = BaseCollection;

  return {
    BaseModel: BaseModel,
    BaseCollection: BaseCollection
  };

});

ABINE_DNTME.define('abine/models/blacklist',
  ['documentcloud/underscore', 'models'],
  function(_, models) {

    var Blacklist = models.BaseModel.extend({
      storageEntity: 'blacklist',

      initialize: function(){
        this.messenger = this.getMessenger();
      },

      getDomain: function(){
        return this.get("domain");
      },

      getId: function(){
        return this.get("id");
      },

      fetchByDomain: function(createSiteIfNotExist, callback) {
        var self = this;
        var verb = createSiteIfNotExist ? "create" : "read";
        var domain = this.get("domain");
        if (domain) domain = domain.replace(":", "::");
        var eventName = verb + ":blacklist:domain:" + domain;
        this.sync(verb, this, {
          eventName: eventName,
          success: function(data) {
            self.set(data);
            callback(null);
          },
          error: function() {
            LOG_ERROR && ABINE_DNTME.log.error("[ERROR HANDLER]#[abine/models/site]#[loadAccounts]# error event in model.sync eventName: create/read:blacklist:domain:domain");
            callback("No blacklist");
          }
        });
      },

      emailEnabled: function() {
        return this.get('globalEmail') && !this.get('email');
      },

      phoneEnabled: function() {
        return this.get('globalPhone') && this.get('premium') && !this.get('phone');
      },

      cardEnabled: function() {
        return this.get('globalCard') && this.get('hasEnabledCard') && !this.get('card');
      },

      accountsEnabled: function() {
          return this.get('hasEnabledAccounts');
      },

      passwordsEnabled: function() {
        return this.get('globalPassword') && !this.get('password');
      },

      loginsEnabled: function() {
          return this.get('globalLogin') && !this.get('login');
      }

    });

    var BlacklistCollection = models.BaseCollection.extend({
      model: Blacklist
    });

    models.Blacklist = Blacklist;
    models.BlacklistCollection = BlacklistCollection;

    return {
      Blacklist: Blacklist,
      BlacklistCollection: BlacklistCollection
    };
  });


ABINE_DNTME.define('abine/models/disposableCard',
    ['models', 'abine/eventLogger'],
    function(models, logger) {

  var DisposableCard = models.BaseModel.extend({
    storageEntity: 'disposableCard',

    initialize: function() {
    },

    isGift: function(){
      return this.isTrue("gift");
    },

    getCreatedDate: function(){
      var arr = this.get("createdAt").substring(0, 10).split("-");
      return arr[1] + "/" + arr[2] + "/" + arr[0];
    },

    isOldMaskedCard: function(){
      return this.get("cardType") == "old_masked_card";
    },

    getBrand: function(){
      return this.get("brand").toLowerCase();
    },

    isCompany: function(){
      return this.isTrue("company");
    },

    getRecipientEmail: function(){
      return this.get("recipient_email");
    },

    getTotalFee: function(){
      return this.get("total_fee");
    },

    isActive: function() {
      return this.isTrue("active");
    },

    getID: function(){
      return this.id;
    },

    getGUID: function(){
      return this.get("guid");
    },

    getAmountRefunded: function(){
      return "$" + (this.get("amount_refunded") / 100.00).toFixed(2);
    },

    getOriginalValue: function(){
      return "$" + (this.get("original_amount") / 100.00 ).toFixed(2);
    },

    getTotalCharge: function(){
      return "$" + ((this.get("original_amount") + this.get("total_fee")) / 100.00).toFixed(2);
    },

    isAch: function(){
      return this.get("payment_source") && (this.get("payment_source") == "checking" || this.get("payment_source") == 'savings');
    },

    getCardDetails: function(){
      return this.getCardNumber() + " - " + this.getExpiry();
    },

    getLastFour: function(){
      return this.get("number").substr(this.get("number").length-4);
    },

    getCardNumber: function(){
       return this.get("number");
    },

    getSeparatedCardNumber: function(){
      var cardNumber = this.get("number");
      if (cardNumber.length == 16) {
        return (cardNumber.substring(0, 4) + ' ' + cardNumber.substring(4, 8)
            + ' ' + cardNumber.substring(8, 12) + ' ' + cardNumber.substring(12, 16));
      } else {
        return cardNumber;
      }
    },

    getSeparatedMaskedCardNumber: function(){
      var cardNumber = this.get("number");
      if (cardNumber.length == 16) {
        return (Array(4).join('**** ')) + cardNumber.substring(12, 16);
      } else {
        var type = this.get('type');
        if (type && type.match(/amex|american/i)) {
          return '**** ****** *' + cardNumber.substr(-4);
        } else {
          return (Array(cardNumber.length-3).join('*'))+cardNumber.substr(-4);
        }
      }
    },

    getMaskedVerification: function(){
      return Array(this.get('cvc').length+1).join('*');
    },

    getMaskedExpiry: function(){
      return this.getExpiry().replace(/[0-9]/g, '*');
    },

    getType: function(){
      return this.get("type").toLowerCase();
    },

    getPrettyNumber: function(){
      var pretty = "";

      for(var i=0;i<this.get("number").length;i++){
        pretty += this.get("number").charAt(i);

        if((i%4) == 3){
          pretty+= "  ";
        }
      }

      return pretty;
    },

    getCVC: function(){
      return this.get("cvc");
    },

    getExpiry: function(){
      var month = parseInt(this.get('expiry_month'));
      if (month < 10) month = '0'+month;
      return month+'/'+this.get('expiry_year');
    },

    getLabel: function(){
      var lbl = this.get('label');
      if (lbl && lbl.length > 15) lbl = lbl.substr(0, 12)+'...';
      return lbl;
    },

    getDomain: function(){
      var lbl = this.get('domain');
      if (lbl && lbl.length > 15) lbl = lbl.substr(0, 12)+'...';
      return lbl;
    },

    getProtectedNumber: function() {
      var number = this.get('number')+"";
      return number.substr(0, number.length-12).replace(/./g, 'x')+number.substr(number.length-4);
    },

    getProtectedVerification: function() {
      var number = this.get('verification')+"";
      return number.substr(0, number.length-1).replace(/./g, 'x')+number.substr(number.length-1);
    },

    toggleDisposable: function() {
      this.save({
        active: (this.get("active")===1)?0:1
      });
    }
  });

  DisposableCard.convertStringToCents = function(amountString) {
    return parseFloat(amountString).toFixed(2).toString().replace("\.", "")
  }

  var DisposableCardCollection = models.BaseCollection.extend({
    model: DisposableCard,

    fetchAllLocal: function(options) {
      options = options || {};
      if (typeof options.reset == 'undefined') options.reset = true;
      options.eventName = ['read', this.model.prototype.storageEntity, 'allLocal'].join(':');
      this.fetch(options);
    },

    fetchAllBySite: function(siteId) {
      this.sync('read', this, {
        eventName: "read:disposableCard:site:"+siteId,
        success: function(data) {
          this.reset(data);
        },
        error: function() {
          logger.error("[ERROR HANDLER][abine/models/disposableCard][fetchAllBySite]: error event in model.sync event: read:disposableCard:site:id");
        }
      });
    },

    fetchAllFromServer: function(callback){
      this.sync('read', this, {
        eventName: "read:disposableCard:server",
        success: function(data){
          this.reset(data);
          callback.success(data);
        },
        error: function(){
          callback.error();
          LOG_ERROR && ABINE_DNTME.log.error("[ERROR HANDLER]#[abine/models/disposableCard]#[fetchAllFromServer]# error event in model.sync eventName: read:disposableCard:server");
        }
      })
    },

    fetchAllByDomain: function(domain, callback) {
      if (domain) domain = domain.replace(":", "::");
      this.sync('read', this, {
        eventName: "read:disposableCard:domain:"+domain,
        success: function(data) {
          this.reset(data);
          callback();
        },
        error: function() {
          LOG_ERROR && ABINE_DNTME.log.error("[ERROR HANDLER]#[abine/models/disposableCard]#[fetchAllByDomain]# error event in model.sync eventName: read:disposableCard:domain:domain");
        }
      });
    },

    comparator: function(model){
      return (new Date()) - new Date(model.get("createdAt"));
    }
  });

  models.DisposableCard = DisposableCard;
  models.DisposableCardCollection = DisposableCardCollection;

  return {
    DisposableCard: DisposableCard,
    DisposableCardCollection: DisposableCardCollection
  };
});


ABINE_DNTME.define('abine/models/disposableEmail',
   ['models', 'documentcloud/underscore'],
   function(models, _) {

  var DisposableEmail = models.BaseModel.extend({
    storageEntity: 'disposableEmail',

    highlighted: false,

    initialize: function() {
      this.hasOne({account: {model: models.Account } });
    },

    getId: function(){
      return this.get('id');
    },

    hasWebmail: function(){
      if(this.has("webmails") && this.get("webmails").length > 0){
        return true;
      } else {
        return false;
      }
    },

    getWebmailCount: function(){
      if(this.hasWebmail()){
        return this.get("webmails").length;
      } else {
        return 0;
      }
    },

    hasAccount: function(){
      if (this.has('accountId') && this.get('accountId') != "" && this.account && this.account.accountFields) {
        return _.any(this.account.accountFields.models, function(accountField){
          return accountField.get('value') == this.getAddress();
        }, this);
      }
      return false;
    },
    
    getAddress: function() {
      return this.get('user') + "@" + this.get('domain');
    },

    getHighlightedClass: function(){
      if(this.get("highlighted")){
        return "highlighted";
      } else {
        return "";
      }
    },
    noConnectionError: function() {
      return !this.get("err");
    },

    isActive: function(){
      return (this.get("active")==1);
    },
    toggleDisposable: function(options) {
      this.save({
        active: (this.get("active")==1)?0:1
      },options);
    },
    needsTruncation: function () {
      var domain = this.getDomain();
      return domain.length > 20;
    },

    getTruncatedDomain: function () {
      var domain = this.getDomain();
      return domain.substring(0, 20) + "...";
    },

    getDomain: function() {
      if(this.attributes.label){
        return this.attributes.label;
      } else {
        return "Unknown";
      }
    }
  });

  var DisposableEmailCollection = models.BaseCollection.extend({
    model: DisposableEmail,

    interestedBroadcasts: /broadcast:saveForm:(new|update)/i,

    initialize: function() {
      this.messenger = this.getMessenger();
    },

    totalWebmails: function(){
      return _.reduce(this.models, function(memo, email){
        return memo + email.getWebmailCount();
      }, 0);
    },

    countUserCreated: function(){
      return _.reduce(this.models, function(count, email){
        if (email.getDomain() != 'abine.com')
          return count + 1;
        return count;
      }, 0);
    },

    fetchAllLocal: function(options) {
      options = options || {};
      if (typeof options.reset == 'undefined') options.reset = true;
      options.eventName = ['read', this.model.prototype.storageEntity, 'allLocal'].join(':');
      this.fetch(options);
    },

    fetchAllBySite: function(siteId) {
      this.sync('read', this, {
        eventName: "read:disposableEmail:site:"+siteId,
        success: function(data) {
          this.reset(data);
        },
        error: function() {
          LOG_ERROR && ABINE_DNTME.log.error("[ERROR HANDLER]#[abine/models/disposableEmail]#[fetchAllBySite]# error event in model.sync eventName: read:disposableEmail:site:id");
        }
      });
    },

    fetchAllFromServer: function(callback){
      this.sync('read', this, {
        eventName: "read:disposableEmail:server",
        success: function(data){
          this.reset(data);
          callback();
        },
        error: function(){
          LOG_ERROR && ABINE_DNTME.log.error("[ERROR HANDLER]#[abine/models/disposableEmail]#[fetchAllFromServer]# error event in model.sync eventName: read:disposableEmail:server");
        }
      })
    },

    fetchAllByDomain: function(domain, callback) {
      if (domain) domain = domain.replace(":", "::");
      this.sync('read', this, {
        eventName: "read:disposableEmail:domain:"+domain,
        success: function(data) {
          this.reset(data);
          callback();
        },
        error: function() {
          LOG_ERROR && ABINE_DNTME.log.error("[ERROR HANDLER]#[abine/models/disposableEmail]#[fetchAllByDomain]# error event in model.sync eventName: read:disposableEmail:domain:domain");
        }
      });
    },

    comparator: function(model){
      return (new Date()) - new Date(model.get("createdAt"));
    }
  });

  models.DisposableEmail = DisposableEmail;
  models.DisposableEmailCollection = DisposableEmailCollection;

  return {
    DisposableEmail: DisposableEmail,
    DisposableEmailCollection: DisposableEmailCollection
  };
});


ABINE_DNTME.define('abine/models/disposablePhone',
    ['models'],
    function(models) {

  var DisposablePhone = models.BaseModel.extend({
    storageEntity: 'disposablePhone',
    
    initialize: function() {
      this.hasOne({account: {model: models.Account } });
    },
    getCountryCode: function(){
      return this.get('country_code') || "1";
    },

    getAreaCode: function(){
      return this.get('number').substr(0,3);
    },

    getExchange: function() {
      return this.get('number').substr(3,3);
    },
    
    getLocal: function() {
      return this.get('number').substr(6,4);
    },

    getPrettyNumber: function() {
      if (!this.isNorthAmericaFormat())
        return this.get('number');
      return "(" + this.getAreaCode() + ") "
        + this.getExchange() + "-"
        + this.getLocal();
    },

    getFullPrettyNumber: function() {
      if (!this.isNorthAmericaFormat())
        return '+'+this.getCountryCode()+' '+this.get('number');
      return '+'+this.getCountryCode()+" (" + this.getAreaCode() + ") "
        + this.getExchange() + "-"
        + this.getLocal();
    },
    getFormattedText: function() {
      return this.getFullPrettyNumber();
    },

    isNorthAmericaFormat: function() {
      return this.getCountryCode() == "1";
    },

    isSuspended: function() {
      return this.get('isSuspended') == 'true';
    },

    toggleDisposable: function() {
      this.save({
        active: (this.get("active")===1)?0:1
      });
    },

    toFillData: function() {
      var json = this.toJSON();
      var countryCode = json.country_code;
      var number = json.number;
      var areaCode = "", lastSeven = "", exchange = "", local = "", label;
      if (countryCode == "1") {
        areaCode = number.substring(0, 3);
        exchange = number.substring(3, 6);
        local = number.substring(6, 10);
        lastSeven = exchange + "-" + local;
        label = "+1 (" + areaCode + ") " + exchange + "-" + local;
      } else {
        lastSeven = "+" + countryCode + number;
        label = "+" + countryCode + " " + number;
      }

      json.label = label;

      json.area = areaCode;
      json.exchange = exchange;
      json.local = local;
      json.last7 = lastSeven;

      return json;
    }
  });

  var DisposablePhoneCollection = models.BaseCollection.extend({
    model: DisposablePhone,

    fetchAllBySite: function(siteId) {
      this.sync('read', this, {
        eventName: "read:disposablePhone:site:"+siteId,
        success: function(data) {
          this.reset(data);
        },
        error: function() {
          LOG_ERROR && ABINE_DNTME.log.error("[ERROR HANDLER]#[abine/models/disposablePhone]#[fetchAllBySite]# error event in model.sync event: read:disposablePhone:site:id");
        }
      });
    }
  });

  models.DisposablePhone = DisposablePhone;
  models.DisposablePhoneCollection = DisposablePhoneCollection;

  return {
    DisposablePhone: DisposablePhone,
    DisposablePhoneCollection: DisposablePhoneCollection
  };
});


ABINE_DNTME.define('abine/models/dntStats',
    ['documentcloud/underscore', 'models', 'jquery', 'abine/intl'],
    function(_, models, $, intl) {

      function addCommas(nStr) {
        nStr += '';
        var x = nStr.split('.');
        var x1 = x[0];
        var x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
          x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
      }

      var DntStats = models.BaseModel.extend({
        storageEntity: 'dntStats',

        initialize: function(){
          this.messenger = this.getMessenger();
        },

        // this should go in some common code area
        addCommas: function(nStr) {
          nStr += '';
          var x = nStr.split('.');
          var x1 = x[0];
          var x2 = x.length > 1 ? '.' + x[1] : '';
          var rgx = /(\d+)(\d{3})/;
          while (rgx.test(x1)) {
              x1 = x1.replace(rgx, '$1' + ',' + '$2');
          }
          return x1 + x2;
        },

        showMedals: function() {
          return this.get('showMedals');
        },

        viewedBadge: function() {
          return this.get('viewedBadge');
        },

        canAutoShowBadge: function() {
          if (this.get('viewedBadge') || !this.get('showMedals')) return false;
          var earnedOn = this.get('earnedOn');
          return  !earnedOn || ((Date.now()-earnedOn) < 3*24*60*60*1000);
        },

        toggleViewedBadge: function() {
          this.save({
            viewedBadge: !(this.get("viewedBadge"))
          });
        },

        achievedBronze: function() {
            return (this.get("currentBadgeIndex") > -1);
        },

        achievedSilver: function() {
            return (this.get("currentBadgeIndex") > 0);
        },

        achievedGold: function() {
            return (this.get("currentBadgeIndex") > 1);
        },

        achievedPlatinum: function() {
          return (this.get("currentBadgeIndex") > 2);
        },

        getPlatinumCount: function() {
            return this.achievedPlatinum()?("x"+(this.get("currentBadgeIndex")-2)):"X";

        },

        nextMedalAtFormatted: function() {
          return this.addCommas(this.nextMedalAt());
        },
        nextMedalAt: function(){
            if (this.achievedPlatinum()){
                return this.platinumLimit*(this.get('currentBadgeIndex')-1) - this.get('totalAllTimeTrackers');
            }else if(this.achievedGold()){
                 return this.platinumLimit - this.get('totalAllTimeTrackers');
            }else if (this.achievedSilver()){
                return this.goldLimit - this.get('totalAllTimeTrackers');
            }else if (this.achievedBronze()){
                return this.silverLimit - this.get('totalAllTimeTrackers');
            }else{
                return this.bronzeLimit - this.get('totalAllTimeTrackers');
            }
         },

        getFunword: function() {
          var total = this.get('totalAllTimeTrackers');
          if (total <= 100) {
            return intl.getText('mm_dntme_funword1');
          } else if (total <= 1000) {
            return intl.getText('mm_dntme_funword2');
          } else if (total <= 10000) {
            return intl.getText('mm_dntme_funword3');
          } else {
            return intl.getText('mm_dntme_funword4');
          }
        },

        getBadgeCount: function() {
          var badge = JSON.parse(this.get('currentBadge'));
          return this.addCommas(badge.limit);
        },

        getBadgeType: function() {
          var badge = JSON.parse(this.get('currentBadge'));
          return badge.cssClass;
        },

        getAllTimeTotalTrackers: function() {
          return this.get('totalAllTimeTrackers');
        },

        formattedTotal: function() {
          return addCommas(this.get('totalAllTimeTrackers')+'');
        },

        totalGreaterThan10000: function() {
          return this.get('totalAllTimeTrackers') > 10000;
        },

        totalGreaterThan1000000: function() {
          return this.get('totalAllTimeTrackers') > 1000000;
        },

        getDailyTotalsByYear: function(year) {
          var totals = JSON.parse(this.get('dailyTrackerCounts'));
          if (year in totals) return totals[year];
          return {};
        },

        getBadTrackerDataByYear: function(year) {
          var badTrackers = JSON.parse(this.get('badTrackers'));
          if (!badTrackers.totals) return {};
          var badTrackersTotal = badTrackers.totals;
          var badTrackersDailyTotal = {};
          var years = [year, year-1];
          for (var tracker in badTrackersTotal) {
            for (var y=0;y<years.length;y++) {
              var trackerYear = tracker+'.'+years[y];
              if (!badTrackers.daily || !(trackerYear in badTrackers.daily)) continue;
              badTrackersDailyTotal[tracker] = {};
              badTrackersDailyTotal[tracker][years[y]] = badTrackers.daily[trackerYear];
            }
          }

          var links = {};
          return {totals: badTrackersTotal, daily: badTrackersDailyTotal, links: links}
        }
      });

      models.DntStats = DntStats;

      return {
        DntStats: DntStats
      };
   });

ABINE_DNTME.define('abine/models/preference',
   ["documentcloud/underscore", "models"],
   function(_, models) {

  // Preference Model

  var Preference = models.BaseModel.extend({
    storageEntity: 'preference',

    initialize: function() {},

    // Normalize boolean preferences in and out of a string field

    parse: function(payload) {
      if (payload.value == "true") {
        payload.value = true;
      } else if (payload.value == "false") {
        payload.value = false;
      }
      return payload;
    },

    toJSON: function() {
      var payload = _.clone(this.attributes);
      if (payload.value === true) {
        payload.value = "true";
      } else if (payload.value === false) {
        payload.value = "false";
      }
      return payload;
    }


  });

  // Preference Collection

  var PreferenceCollection = models.BaseCollection.extend({

    model: Preference,

    interestedBroadcasts: /broadcast:preference:reload/i,

    parse: function(response, xhr) {
      return _(response).map(function(payload) {
        if (payload.value == "true") {
          payload.value = true;
        } else if (payload.value == "false") {
          payload.value = false;
        }
        return payload;
      });
    },

    restoreDefaults: function(callback){
      this.sync('update', this, {
        eventName: 'update:preference:restore',
        success:_.bind(function(data){
          this.reset(this.parse(data));
          callback();
        },this),
        error: function(e){
          callback(e);
        }
      });
    },
    
    getPrefFromStorage: function(key, callback) {
      this.sync('read', this, {
        eventName: "read:preference:name:" + key,
        success: function(data) {
          if (_.isArray(data) && data.length === 0) {
            callback(null);
          } else if (_.isObject(data)) {
            callback(new Preference(data));
          } else {
            callback(null);
          }
        },
        error: function() {
          LOG_ERROR && ABINE_DNTME.log.error("[ERROR HANDLER]#[abine/models/preference]#[getPrefFromStorage]# error event in model.sync eventName: read:preference:name:key");
          callback(null);
        }
      });
    },

    isPrefTrue: function(key) {
      var pref = _(this.models).find(function(p) {
        return (p.get('key') === key);
      });
      return (pref) ? pref.isTrue('value') : false;
    },
    
    getPref: function(key) {
      var pref = _(this.models).find(function(p) {
        return (p.get('key') === key);
      });
      return (pref) ? pref.get('value') : null;
    },

    isMapper: function() {
      return true;
      var pref = _(this.models).find(function(p) {
        return (p.get('key') in {'mapping:mapper':1, 'mapping:reviewer':1, 'autofill:mapper': 1});
      });
      return !!pref;
    },

    setPref: function(key, value) {
      var pref = _(this.models).find(function(p) {
        return (p.get('key') === key);
      });
      if (pref){
        pref.save({value:value});
      } else {
        pref = new models.Preference();
        pref.save({key: key, value: value});
        this.add(pref);
      }
    },
    
    removePref: function(key) {
      var pref = _(this.models).find(function(p) {
        return (p.get('key') === key);
      });
      if (pref){
        pref.destroy();
        this.remove(pref);
      }
    },

    hasPref: function(key) {
      var pref = _(this.models).find(function(p) {
        return (p.get('key') === key);
      });
      return !!pref;
    },

    getSplitTest: function(key) {
      var splitTests =  this.getPref('splitTests') || {};
      try{
        splitTests = JSON.parse(splitTests);
      } catch (e){}
      return splitTests[key] || "default";
    }

  });

  models.Preference = Preference;
  models.PreferenceCollection = PreferenceCollection;

  return {
    Preference: Preference,
    PreferenceCollection: PreferenceCollection
  };

});


ABINE_DNTME.define('abine/models/loginUrl',
  ['documentcloud/underscore', 'models'],
  function(_, models) {

    var LoginUrl = models.BaseModel.extend({
      storageEntity: 'loginUrl'
    });

    var LoginUrlCollection = models.BaseCollection.extend({
      model: LoginUrl,

      initialize: function() {
      },
      fetchAllByDomain: function(domain, cb) {
        if (domain) domain = domain.replace(":", "::");
        this.sync('read', this, {
          eventName: "read:loginUrl:site:"+domain,
          success: function(data) {
            this.reset(data);
            cb();
          },
          error: function() {
            LOG_ERROR && ABINE_DNTME.log.error("[ERROR HANDLER]#[abine/models/loginUrl]#[fetchAllByDomain]# error event in read:loginUrl:site:domain");
            cb(true);
          }
        });
      }
    });

    models.LoginUrl = LoginUrl;
    models.LoginUrlCollection = LoginUrlCollection;

    return {
      LoginUrl: LoginUrl,
      LoginUrlCollection: LoginUrlCollection
    };
  });


ABINE_DNTME.define('abine/models/extensionVersion',
  ['documentcloud/underscore', 'models'],
  function(_, models) {

    var ExtensionVersion = models.BaseModel.extend({
      storageEntity: 'extensionVersion',

      initialize: function(){
        this.messenger = this.getMessenger();
      }
    });

    models.ExtensionVersion = ExtensionVersion;

    return {
      ExtensionVersion: ExtensionVersion
    };
  }
);

ABINE_DNTME.define('abine/models/autofillFeedback',
  ['documentcloud/underscore', 'models'],
  function(_, models) {

    var AutofillFeedback = models.BaseModel.extend({
      storageEntity: 'autofillFeedback'
    });

    models.AutofillFeedback = AutofillFeedback;

    return {
      AutofillFeedback: AutofillFeedback
    };
  });


ABINE_DNTME.define('abine/models/mappedForm',
  ['documentcloud/underscore', 'models'],
  function(_, models) {

    var MappedForm = models.BaseModel.extend({
      storageEntity: 'mappedForm',

      initialize: function() {
        this.hasMany({fields: {collection: models.MappedFieldCollection}});
      },

      searchSignature: function(cb){
        this.sync('read', this, {
          eventName: "read:mappedForm:signature:"+this.get("signature"),
          success:_.bind(function(data) {
            if(!data){
              cb(true);
            } else{
              this.set(data);
              cb();
            }
          },this),
          error: function() {
            LOG_ERROR && ABINE_DNTME.log.error("[ERROR HANDLER]#[abine/models/mappedForm]#[searchSignature]# error event in searchSignature");
            cb(true);
          }
        });
      }
    });

    var MappedFormCollection = models.BaseCollection.extend({
      model: MappedForm,

      initialize: function(options) {
        this.messenger = options.messenger;
      },
      fetchAllByDomain: function(domain, cb) {
        if (domain) domain = domain.replace(":", "::");
        this.sync('read', this, {
          eventName: "read:mappedForm:site:"+domain,
          success: function(data) {
            this.reset(data);
            cb();
          },
          error: function() {
            LOG_ERROR && ABINE_DNTME.log.error("[ERROR HANDLER]#[abine/models/mappedForm]#[fetchAllByDomain]# error event in read:mappedForm:site:domain");
            cb(true);
          }
        });
      },
      fetchLocalAllByDomain: function(domain, cb) {
        if (domain) domain = domain.replace(":", "::");
        this.sync('read', this, {
          eventName: "read:mappedForm:local:"+domain,
          success: function(data) {
            this.reset(data);
            cb();
          },
          error: function() {
            LOG_ERROR && ABINE_DNTME.log.error("[ERROR HANDLER]#[abine/models/mappedForm]#[fetchAllByDomain]# error event in read:mappedForm:site:domain");
            cb(true);
          }
        });
      }
    });

    models.MappedForm = MappedForm;
    models.MappedFormCollection = MappedFormCollection;

    return {
      MappedForm: MappedForm,
      MappedFormCollection: MappedFormCollection
    };
  });


ABINE_DNTME.define('abine/models/mappedField',
  ['documentcloud/underscore', 'models'],
  function(_, models) {

    var MappedField = models.BaseModel.extend({
      storageEntity: 'mappedField',

      initialize: function() {
      }
    });

    var MappedFieldCollection = models.BaseCollection.extend({
      model: MappedField,

      initialize: function() {
      }
    });

    models.MappedField = MappedField;
    models.MappedFieldCollection = MappedFieldCollection;

    return {
      MappedField: MappedField,
      MappedFieldCollection: MappedFieldCollection
    };
  });


ABINE_DNTME.define('abine/models/mappingMenu',
    ['documentcloud/underscore', 'models'],
    function (_, models) {

      var MENU = {
  "loginForm": [
    "dont_show",
    "fill_logins",
    "correct_login",
    "mapping",
    "list_accounts",
    "list_emails",
    "list_phones",
    "report"
  ],
  "registrationForm": [
    "dont_show",
    "register",
    "login_on_register",
    "mapping",
    "list_emails",
    "list_phones",
    "report"
  ],
  "checkoutForm": [
    "dont_show",
    "wallet",
    "mapping",
    "list_cards",
    "list_emails",
    "list_phones",
    "report"
  ],
  "identityForm": [
    "dont_show",
    "identity",
    "mapping",
    "list_identities",
    "list_emails",
    "list_phones",
    "list_phones",
    "report"
  ],
  "addressForm": [
    "dont_show",
    "address",
    "mapping",
    "list_addresses",
    "list_emails",
    "list_phones",
    "report"
  ],
  "phoneForm": [
    "dont_show",
    "phone",
    "mapping",
    "list_phones",
    "report"
  ],
  "otherForm": [
    "dont_show",
    "mapping",
    "report"
  ],
  "mapping": {
    "id": "mapping",
    "name": "Fill something different than {data_type}",
    "alternate_name": "Fill unknown field with",
    "children": [
      {
        "ref": "register",
        "name": "Fill Email, Username, or Password"
      },
      {
        "ref": "wallet",
        "name": "Fill Credit Cards or Masked Cards"
      },
      {
        "ref": "address",
        "name": "Fill Addresses"
      },
      {
        "ref": "phone",
        "name": "Fill Phones"
      },
      {
        "ref": "others"
      }
    ]
  },
  "more": {
    "id": "more",
    "name": "More",
    "action": "show_more"
  },
  "report": {
    "id": "report",
    "name": "Report a problem (improve Blur) >",
    "action": "report_problem"
  },
  "dont_show_extension": {
    "id": "dont_show",
    "name": "Don't show again here",
    "panel_title": "Blur sees: <u>{data_type}</u> on <u class=formType>{form_type}</u> Page",
    "action": "dont_show_here"
  },
  "dont_show": {
    "id": "dont_show",
    "name": "Don't show again here?",
    "panel_title": "Blur sees: <u>{data_type}</u> on <u class=formType>{form_type}</u> Page",
    "children": [
      {
        "id": "field",
        "name": "Don't show again on this field",
        "action": "dont_show_on_field"
      },
      {
        "id": "form",
        "name": "Don't show again on this form",
        "action": "dont_show_on_form"
      },
      {
        "id": "site",
        "name": "Don't show again on this {app}",
        "action": "dont_show_on_site"
      }
    ]
  },
  "fill_logins": {
    "id": "accounts",
    "name": "Fill existing account info",
    "when": "account_has_data",
    "children": [
      {
        "id": "{account}",
        "name": "{label}",
        "panel_title": "Select existing account info to fill",
        "entities": "account",
        "children": [
          {
            "id": "{email}",
            "name": "{email}",
            "label": "Email",
            "entity": "account",
            "action": "fill",
            "type": "/contact/email",
            "formType": "loginForm"
          },
          {
            "id": "{username}",
            "name": "{username}",
            "label": "Username",
            "entity": "account",
            "action": "fill",
            "type": "/nameperson/friendly",
            "formType": "loginForm"
          },
          {
            "id": "{password}",
            "name": "Password for {label}",
            "label": "Password",
            "entity": "account",
            "value": "{password}",
            "action": "fill",
            "type": "/password",
            "formType": "loginForm"
          }
        ]
      }
    ]
  },
  "correct_login": {
    "id": "correct",
    "name": "Correct how the <b>{data_type}</b> is filled",
    "children": [
      {
        "id": "email",
        "name": "Email",
        "type": "/contact/email",
        "formType": "loginForm"
      },
      {
        "id": "username",
        "name": "Username",
        "type": "/nameperson/friendly",
        "formType": "loginForm"
      },
      {
        "id": "password",
        "name": "Password",
        "type": "/password",
        "formType": "loginForm"
      },
      {
        "id": "otp",
        "name": "One Time Password",
        "type": "/auth/otp"
      }
    ]
  },
  "list_accounts": {
    "id": "list_accounts",
    "name": "Add/Edit My Accounts",
    "action": "show_list_accounts"
  },
  "list_emails": {
    "id": "list_emails",
    "name": "Add/Edit My Emails",
    "forType": "email",
    "action": "show_list_identities"
  },
  "list_phones": {
    "id": "list_phones",
    "name": "Add/Edit My Phones",
    "forType": "phone",
    "action": "show_list_identities"
  },
  "list_cards": {
    "id": "list_cards",
    "name": "Add/Edit My Cards",
    "action": "show_list_cards"
  },
  "list_addresses": {
    "id": "list_addresses",
    "name": "Add/Edit My Addresses",
    "action": "show_list_addresses"
  },
  "list_identities": {
    "id": "list_identities",
    "name": "Add/Edit My Identities",
    "action": "show_list_identities"
  },
  "register": {
    "id": "register",
    "name": "Correct how the <b>{data_type}</b> is filled",
    "panel_title": "Blur sees: <u>{data_type}</u> on <u class=formType>{form_type}</u> Page",
    "children": [
      {
        "id": "mask_email",
        "name": "Fill New Masked Email",
        "label": "Email",
        "panel_title": "Blur sees: <u>{data_type}</u> on <u class=formType>{form_type}</u> Page",
        "action": "mask_email",
        "type": "/contact/email"
      },
      {
        "id": "real_email",
        "name": "Fill My Email ({email})",
        "action": "real_email"
      },
      {
        "id": "password1",
        "name": "Fill New Strong Password",
        "type": "/password/differentSpecialChars",
        "action": "strong_password"
      },
      {
        "id": "security",
        "name": "Fill New Custom Password",
        "label": "Password",
        "children": [
          {
            "id": "password2",
            "name": "Limited special chars (.-_)",
            "label": "Password with limited special chars",
            "type": "/password/differentSpecialChars",
            "action": "strong_password"
          },
          {
            "id": "password3",
            "name": "No special characters",
            "label": "Password without special",
            "type": "/password/noSpecialChars",
            "action": "strong_password"
          },
          {
            "id": "password4",
            "name": "No numbers",
            "label": "Password without numbers",
            "type": "/password/noNumbers",
            "action": "strong_password"
          }
        ]
      },
      {
        "id": "username",
        "name": "Fill Username",
        "label": "Username",
        "type": "/nameperson/friendly"
      }
    ]
  },
  "login_on_register": {
    "id": "login",
    "name": "Login fields on register form",
    "children": [
      {
        "id": "loginemail",
        "name": "Login Username or Email",
        "panel_title": "Fill <u>email/password</u> Login info on this combined form",
        "type": "/contact/email/login"
      },
      {
        "id": "loginpassword",
        "name": "Login Password",
        "type": "/password/login"
      }
    ]
  },
  "wallet": {
    "id": "cards",
    "name": "Correct how the <b>{data_type}</b> is filled",
    "when": "card_has_data",
    "children": [
      {
        "id": "mask",
        "name": "Fill Masked Card",
        "panel_title": "Blur sees: <u>{data_type}</u> on <u class=formType>{form_type}</u> Page",
        "action": "mask_card"
      },
      {
        "id": "{card}",
        "name": "Fill {label} (x{last_4})",
        "page_title": "Select existing card info to fill",
        "entities": "card",
        "children": [
          {
            "id": "number",
            "name": "Fill Card # - <small>x{last_4}</small>",
            "page_title": "Fill {label} (x{last_4})",
            "entity": "card",
            "children": [
              {
                "id": "number",
                "name": "Fill Card # - <small>x{last_4}</small>",
                "label": "Card Number",
                "page_title": "Fill Full Card #",
                "entity": "card",
                "value": "{number}",
                "action": "fill",
                "type": "/financial/creditcard/number"
              },
              {
                "id": "part1",
                "name": "Fill Card # - first 4",
                "label": "Card Number Part 1",
                "entity": "card",
                "value": "{first_4}",
                "action": "fill",
                "type": "/financial/creditcard/part1"
              },
              {
                "id": "part2",
                "name": "Fill Card # - second 4",
                "label": "Card Number Part 2",
                "entity": "card",
                "value": "{second_4}",
                "action": "fill",
                "type": "/financial/creditcard/part2"
              },
              {
                "id": "part3",
                "name": "Fill Card # - third 4",
                "label": "Card Number Part 3",
                "entity": "card",
                "value": "{third_4}",
                "action": "fill",
                "type": "/financial/creditcard/part3"
              },
              {
                "id": "part4",
                "name": "Fill Card # - last 4",
                "label": "Card Number Part 4",
                "entity": "card",
                "value": "{last_4}",
                "action": "fill",
                "type": "/financial/creditcard/part4"
              },
              {
                "id": "amex1",
                "name": "Fill Card # - first 4",
                "label": "Amex Number Part 1",
                "entity": "card",
                "value": "{amex_4}",
                "type": "/financial/creditcard/amex/part1"
              },
              {
                "id": "amex2",
                "name": "Fill Card # - second 6",
                "label": "Amex Number Part 2",
                "entity": "card",
                "value": "{amex_6}",
                "type": "/financial/creditcard/amex/part2"
              },
              {
                "id": "amex3",
                "name": "Fill Card # - last 4",
                "label": "Amex Number Part 3",
                "entity": "card",
                "value": "{amex_5}",
                "type": "/financial/creditcard/amex/part3"
              }
            ]
          },
          {
            "id": "holder_name",
            "name": "Card holder name - <small>{full_name}</small>",
            "entity": "card",
            "children": [
              {
                "id": "issued",
                "name": "Full Name - <small>{full_name}</small>",
                "label": "Card Holder Name",
                "value": "{full_name}",
                "page_title": "Fill {label} (x{last_4})",
                "entity": "card",
                "action": "fill",
                "type": "/financial/creditcard/issuedto"
              },
              {
                "id": "first",
                "name": "First Name - <small>{first_name}</small>",
                "label": "Card Holder First Name",
                "value": "{first_name}",
                "entity": "card",
                "action": "fill",
                "type": "/financial/creditcard/issuedto/first"
              },
              {
                "id": "last",
                "name": "Last Name - <small>{last_name}</small>",
                "label": "Card Holder Last Name",
                "value": "{last_name}",
                "entity": "card",
                "action": "fill",
                "type": "/financial/creditcard/issuedto/last"
              }
            ]
          },
          {
            "id": "cvv",
            "name": "Fill CVV/CVC/CVV2",
            "label": "CVV/CVC/CVV2",
            "value": "{cvc}",
            "entity": "card",
            "action": "fill",
            "type": "/financial/creditcard/verification"
          },
          {
            "id": "expiry",
            "name": "Fill Expiry Date - <small>{expiry}</small>",
            "label": "Card Expiry (mm/yyyy)",
            "value": "{expiry}",
            "entity": "card",
            "action": "fill",
            "type": "/financial/creditcard/expiry"
          },
          {
            "id": "expiry2",
            "name": "Fill Expiry Date - <small>{expiry2}</small>",
            "label": "Card Expiry (mm/yy)",
            "value": "{expiry2}",
            "entity": "card",
            "action": "fill",
            "type": "/financial/creditcard/expiry2"
          },
          {
            "id": "month",
            "name": "Fill Expiry month - <small>{expiry_month}</small>",
            "label": "Card Expiry Month",
            "value": "{expiry_month}",
            "entity": "card",
            "action": "fill",
            "type": "/financial/creditcard/expirymonth"
          },
          {
            "id": "year",
            "name": "Fill Expiry year - <small>{expiry_year}</small>",
            "label": "Card Expiry Year (yyyy)",
            "value": "{expiry_year}",
            "entity": "card",
            "action": "fill",
            "type": "/financial/creditcard/expiryyear"
          },
          {
            "id": "year2",
            "name": "Fill Expiry year - <small>{expiry_year2}</small>",
            "label": "Card Expiry Year (yy)",
            "value": "{expiry_year2}",
            "entity": "card",
            "action": "fill",
            "type": "/financial/creditcard/expiryyear2"
          },
          {
            "id": "type",
            "name": "Fill Card type - <small>{type}</small>",
            "label": "Card Type",
            "value": "{type}",
            "entity": "card",
            "action": "fill",
            "type": "/financial/creditcard/type"
          },
          {
            "id": "address",
            "name": "Fill Card Billing Address - <small>{address1}</small>",
            "entity": "card",
            "children": [
              {
                "id": "address1",
                "name": "Fill Address Line 1 - <small>{address1}</small> ",
                "label": "Address Line 1",
                "value": "{address1}",
                "entity": "card",
                "action": "fill",
                "type": "/contact/postaladdress/billing"
              },
              {
                "id": "address2",
                "name": "Fill Address Line 2 - <small>{address2}</small>",
                "label": "Address Line 2",
                "value": "{address2}",
                "entity": "card",
                "action": "fill",
                "type": "/contact/postaladdressAdditional/billing"
              },
              {
                "id": "address3",
                "name": "Fill Address Line 3 - <small>{address3}</small>",
                "label": "Address Line 3",
                "value": "{address3}",
                "entity": "card",
                "action": "fill",
                "type": "/contact/postaladdress3/billing"
              },
              {
                "id": "city",
                "name": "Fill City - <small>{city}</small>",
                "label": "City",
                "value": "{city}",
                "entity": "card",
                "action": "fill",
                "type": "/contact/city/billing"
              },
              {
                "id": "state",
                "name": "Fill State - <small>{state}</small>",
                "label": "State",
                "value": "{state}",
                "action": "fill",
                "entity": "card",
                "type": "/contact/state/billing"
              },
              {
                "id": "statecode",
                "name": "Fill State Code - <small>{state}</small>",
                "label": "State Code",
                "value": "{state}",
                "entity": "card",
                "action": "fill",
                "type": "/contact/state/code/billing"
              },
              {
                "id": "country",
                "name": "Fill Country - <small>{country}</small>",
                "label": "Country",
                "value": "{country}",
                "entity": "card",
                "action": "fill",
                "type": "/contact/country/billing"
              },
              {
                "id": "zip",
                "name": "Fill Zip/Postal Code - <small>{zip}</small>",
                "label": "Zip/Postal Code",
                "entity": "card",
                "value": "{zip}",
                "action": "fill",
                "type": "/contact/postalcode/billing"
              },
              {
                "id": "zip+",
                "name": "Fill Zip+4 - <small>{zip4}</small>",
                "label": "Zip+4",
                "entity": "card",
                "value": "{zip4}",
                "action": "fill",
                "type": "/contact/zip4/billing"
              }
            ]
          }
        ]
      },
      {
        "ref": "list_cards"
      }
    ]
  },
  "identity": {
    "id": "identity",
    "name": "Correct how the <b>{data_type}</b> is filled",
    "when": "identity_has_data",
    "children": [
      {
        "id": "{identity}",
        "name": "Fill {label} - <small>{full_name}</small>",
        "page_title": "Select existing identity info to fill",
        "entities": "identity",
        "children": [
          {
            "id": "full",
            "name": "Full Name - <small>{full_name}</small>",
            "label": "Full Name",
            "value": "{full_name}",
            "entity": "identity",
            "action": "fill",
            "type": "/nameperson/full"
          },
          {
            "id": "first",
            "name": "Fill First name - <small>{first_name}</small>",
            "label": "First Name",
            "value": "{first_name}",
            "entity": "identity",
            "action": "fill",
            "type": "/nameperson/first"
          },
          {
            "id": "middle",
            "name": "Fill Middle name - <small>{middle_name}</small>",
            "label": "Middle Name",
            "value": "{middle_name}",
            "entity": "identity",
            "action": "fill",
            "type": "/nameperson/middle"
          },
          {
            "id": "last",
            "name": "Fill Last name - <small>{last_name}</small>",
            "label": "Last Name",
            "value": "{last_name}",
            "entity": "identity",
            "action": "fill",
            "type": "/nameperson/last"
          },
          {
            "id": "email",
            "name": "Fill Email - <small>{real_email}</small>",
            "value": "{real_email}",
            "entity": "identity",
            "action": "fill",
            "type": "/contact/email"
          },
          {
            "id": "username",
            "name": "Fill Username - <small>{username}</small>",
            "label": "Username",
            "value": "{username}",
            "entity": "identity",
            "action": "fill",
            "type": "/nameperson/friendly"
          },
          {
            "id": "dob",
            "name": "Date of birth - <small>{dob_month}/{dob_day}/{dob_year}</small>",
            "entity": "identity",
            "children": [
              {
                "id": "month",
                "name": "Birth month - <small>{dob_month}</small>",
                "label": "Birth month",
                "value": "{dob_month}",
                "entity": "identity",
                "action": "fill",
                "type": "/birthdate/birthmonth"
              },
              {
                "id": "day",
                "name": "Birth day - <small>{dob_day}</small>",
                "label": "Birth day",
                "value": "{dob_day}",
                "entity": "identity",
                "action": "fill",
                "type": "/birthdate/birthday"
              },
              {
                "id": "year",
                "name": "Birth year - <small>{dob_year}</small>",
                "label": "Birth year",
                "value": "{dob_year}",
                "entity": "identity",
                "action": "fill",
                "type": "/birthdate/birthyear"
              },
              {
                "id": "mmddyyyy1",
                "name": "Birth Date - <small>{dob_month}-{dob_day}-{dob_year}</small>",
                "label": "Birth Date",
                "value": "{dob_month}-{dob_day}-{dob_year}",
                "entity": "identity",
                "action": "fill",
                "type": "/birthdate/MM-DD-YYYY"
              },
              {
                "id": "mmddyyyy2",
                "name": "Birth Date - <small>{dob_month}/{dob_day}/{dob_year}</small>",
                "label": "Birth Date",
                "value": "{dob_month}/{dob_day}/{dob_year}",
                "entity": "identity",
                "action": "fill",
                "type": "/birthdate/MM/DD/YYYY"
              },
              {
                "id": "ddmmyyyy1",
                "name": "Birth Date - <small>{dob_day}-{dob_month}-{dob_year}</small>",
                "label": "Birth Date",
                "value": "{dob_day}-{dob_month}-{dob_year}",
                "entity": "identity",
                "action": "fill",
                "type": "/birthdate/DD-MM-YYYY"
              },
              {
                "id": "ddmmyyyy2",
                "name": "Birth Date - <small>{dob_day}/{dob_month}/{dob_year}</small>",
                "label": "Birth Date",
                "value": "{dob_day}/{dob_month}/{dob_year}",
                "entity": "identity",
                "action": "fill",
                "type": "/birthdate/DD/MM/YYYY"
              }
            ]
          },
          {
            "id": "company",
            "name": "Fill Company - <small>{company}</small>",
            "value": "{company}",
            "entity": "identity",
            "action": "fill",
            "type": "/company/name"
          },
          {
            "id": "position",
            "name": "Company Position",
            "value": "{designation}",
            "entity": "identity",
            "action": "fill",
            "type": "/company/position"
          },
          {
            "id": "website",
            "name": "Website Url",
            "value": "{url}",
            "entity": "identity",
            "action": "fill",
            "type": "/website"
          },
          {
            "id": "ssn",
            "name": "SSN #",
            "value": "{ssn}",
            "entity": "identity",
            "action": "fill",
            "type": "/socialSecurityNumber"
          },
          {
            "id": "dl",
            "name": "Driver License",
            "value": "{driver_license}",
            "entity": "identity",
            "action": "fill",
            "type": "/driverLicense"
          }
        ]
      },
      {
        "ref": "list_identities"
      }
    ]
  },
  "address": {
    "id": "address",
    "name": "Correct how the <b>{data_type}</b> is filled",
    "children": [
      {
        "id": "{address}",
        "name": "Fill {label} - <small>{address1}</small>",
        "page_title": "Select existing address info to fill",
        "entities": "address",
        "children": [
          {
            "id": "address1",
            "name": "Fill Address Line 1 - <small>{address1}</small> ",
            "page_title": "Fill Address - <small>{address1}</small>",
            "value": "{address1}",
            "entity": "address",
            "action": "fill",
            "type": "/contact/postaladdress"
          },
          {
            "id": "address2",
            "name": "Fill Address Line 2 - <small>{address2}</small>",
            "value": "{address2}",
            "entity": "address",
            "action": "fill",
            "type": "/contact/postaladdressAdditional"
          },
          {
            "id": "address3",
            "name": "Fill Address Line 3 - <small>{address3}</small>",
            "value": "{address3}",
            "entity": "address",
            "action": "fill",
            "type": "/contact/postaladdress3"
          },
          {
            "id": "city",
            "name": "Fill City - <small>{city}</small>",
            "value": "{city}",
            "entity": "address",
            "action": "fill",
            "type": "/contact/city"
          },
          {
            "id": "state",
            "name": "Fill State - <small>{state}</small>",
            "value": "{state}",
            "action": "fill",
            "entity": "address",
            "type": "/contact/state"
          },
          {
            "id": "statecode",
            "name": "Fill State Code - <small>{state}</small>",
            "value": "{state}",
            "entity": "address",
            "action": "fill",
            "type": "/contact/state/code"
          },
          {
            "id": "country",
            "name": "Fill Country - <small>{country}</small>",
            "value": "{country}",
            "entity": "address",
            "action": "fill",
            "type": "/contact/country"
          },
          {
            "id": "zip",
            "name": "Fill Zip/Postal Code - <small>{zip}</small>",
            "label": "Zip/Postal Code",
            "entity": "address",
            "value": "{zip}",
            "action": "fill",
            "type": "/contact/postalcode"
          },
          {
            "id": "zip+",
            "name": "Fill Zip+4 - <small>{zip4}</small>",
            "label": "Zip+4",
            "entity": "address",
            "value": "{zip4}",
            "action": "fill",
            "type": "/contact/zip4"
          }
        ]
      },
      {
        "id": "mask_address",
        "name": "Fill Masked Card Address - <small>280 Summer St</small>",
        "children": [
          {
            "id": "address1",
            "name": "Fill Address Line 1 - <small>280 Summer St</small> ",
            "label": "Address Line 1",
            "page_title": "Fill Masked Card Address - <small>280 Summer St, 02210</small>",
            "value": "280 Summer St",
            "action": "fill",
            "type": "/contact/postaladdress"
          },
          {
            "id": "city",
            "name": "Fill City - <small>Boston</small>",
            "label": "City",
            "value": "Boston",
            "action": "fill",
            "type": "/contact/city"
          },
          {
            "id": "state",
            "name": "Fill State - <small>MA</small>",
            "label": "State",
            "value": "MA",
            "action": "fill",
            "type": "/contact/state"
          },
          {
            "id": "country",
            "name": "Fill Country - <small>US</small>",
            "label": "Country",
            "value": "US",
            "action": "fill",
            "type": "/contact/country"
          },
          {
            "id": "zip",
            "name": "Fill Zip/Postal Code - <small>02210</small>",
            "label": "Zip/Postal Code",
            "value": "02210",
            "action": "fill",
            "type": "/contact/postalcode"
          }
        ]
      },
      {
        "ref": "list_addresses"
      }
    ]
  },
  "phone": {
    "id": "phone",
    "name": "Correct how the <b>{data_type}</b> is filled",
    "children": [
      {
        "id": "{phone}",
        "name": "{label}",
        "page_title": "Select existing phone info to fill",
        "entities": "phone",
        "children": [
          {
            "id": "full",
            "name": "Phone - <small>{number}</small>",
            "label": "Phone Number (xxx-yyy-zzzz)",
            "entity": "phone",
            "value": "{number}",
            "action": "fill",
            "type": "/contact/phone"
          },
          {
            "id": "area",
            "name": "Phone areacode - <small>{area}</small>",
            "label": "Phone Areacode (xxx)",
            "entity": "phone",
            "value": "{area}",
            "action": "fill",
            "type": "/contact/phone/areacode"
          },
          {
            "id": "exchange",
            "name": "Phone exchange - <small>{exchange}</small>",
            "label": "Phone Exchange (yyy)",
            "entity": "phone",
            "value": "{exchange}",
            "action": "fill",
            "type": "/contact/phone/exchange"
          },
          {
            "id": "local",
            "name": "Phone local - <small>{local}</small>",
            "label": "Phone Local (zzzz)",
            "entity": "phone",
            "value": "{local}",
            "action": "fill",
            "type": "/contact/phone/local"
          },
          {
            "id": "number",
            "name": "Phone last 7 - <small>{last7}</small>",
            "label": "Phone Last 7 (yyy-zzzz)",
            "entity": "phone",
            "value": "{last7}",
            "action": "fill",
            "type": "/contact/phone/number"
          },
          {
            "id": "country",
            "name": "Phone country code - <small>{country_code}</small>",
            "label": "Phone Country Code",
            "entity": "phone",
            "value": "{country_code}",
            "action": "fill",
            "type": "/contact/phone/countrycode"
          },
          {
            "id": "ext",
            "name": "Phone extension",
            "label": "Phone Extension",
            "entity": "phone",
            "value": "{ext}",
            "action": "fill",
            "type": "/contact/phone/extension"
          }
        ]
      },
      {
        "id": "list",
        "name": "Add/Edit My Phones",
        "action": "show_list_identities"
      }
    ]
  },
  "others": {
    "id": "others",
    "name": "Others",
    "children": [
      {
        "id": "name_types",
        "name": "Name",
        "children": [
          {
            "id": "full",
            "name": "Full Name",
            "type": "/nameperson/full"
          },
          {
            "id": "first",
            "name": "First Name",
            "type": "/nameperson/first"
          },
          {
            "id": "last",
            "name": "Last Name",
            "type": "/nameperson/last"
          },
          {
            "id": "middle",
            "name": "Middle Name",
            "type": "/nameperson/middle"
          },
          {
            "id": "prefix",
            "name": "Title (Mr/Mrs/Ms)",
            "type": "/nameperson/prefix"
          },
          {
            "id": "suffix",
            "name": "Suffix (Jr/Sr/II)",
            "type": "/nameperson/suffix"
          }
        ]
      },
      {
        "id": "dob",
        "name": "Date of birth",
        "children": [
          {
            "id": "age",
            "name": "Age",
            "type": "/birthdate/age"
          },
          {
            "id": "month",
            "name": "Birth month",
            "type": "/birthdate/birthmonth"
          },
          {
            "id": "day",
            "name": "Birth day",
            "type": "/birthdate/birthday"
          },
          {
            "id": "year",
            "name": "Birth year",
            "type": "/birthdate/birthyear"
          },
          {
            "id": "mmddyyyy1",
            "name": "Birth Date (MM-DD-YYYY)",
            "type": "/birthdate/MM-DD-YYYY"
          },
          {
            "id": "mmddyyyy2",
            "name": "Birth Date (MM/DD/YYYY)",
            "type": "/birthdate/MM/DD/YYYY"
          },
          {
            "id": "ddmmyyyy1",
            "name": "Birth Date (DD-MM-YYYY)",
            "type": "/birthdate/DD-MM-YYYY"
          },
          {
            "id": "ddmmyyyy2",
            "name": "Birth Date (DD/MM/YYYY)",
            "type": "/birthdate/DD/MM/YYYY"
          }
        ]
      },
      {
        "id": "gender",
        "name": "Gender",
        "type": "/person/gender"
      },
      {
        "id": "website",
        "name": "Website Url",
        "type": "/website"
      },
      {
        "id": "company",
        "name": "Company Name",
        "type": "/company/name"
      },
      {
        "id": "position",
        "name": "Company Position",
        "type": "/company/position"
      },
      {
        "id": "ssn",
        "name": "SSN #",
        "type": "/socialSecurityNumber"
      },
      {
        "id": "dl",
        "name": "Driver License",
        "type": "/driverLicense"
      },
      {
        "id": "ff",
        "name": "Frequent Flier #",
        "type": "/loyalty/flier"
      },
      {
        "id": "lc",
        "name": "Loyalty Card #",
        "type": "/loyalty/card"
      },
      {
        "id": "captcha",
        "name": "Captcha",
        "type": "/captcha"
      },
      {
        "id": "otp",
        "name": "One Time Password",
        "type": "/auth/otp"
      },
      {
        "id": "hint",
        "name": "Password Hint",
        "type": "/auth/hint"
      },
      {
        "id": "question",
        "name": "Security Question",
        "type": "/auth/question"
      },
      {
        "id": "answer",
        "name": "Security Answer",
        "type": "/auth/answer"
      },
      {
        "id": "comment",
        "name": "Comment",
        "type": "/comment"
      },
      {
        "id": "subject",
        "name": "Subject",
        "type": "/subject"
      },
      {
        "id": "message",
        "name": "Message",
        "type": "/message"
      }
    ]
  },
  "change": {
    "id": "change",
    "name": "Fill Change Password Form",
    "forForm": "changePassword",
    "children": [
      {
        "id": "email",
        "name": "Email",
        "type": "/contact/email"
      },
      {
        "id": "password1",
        "name": "Old Password",
        "type": "/passwordold"
      },
      {
        "id": "password2",
        "name": "New Password",
        "children": [
          {
            "id": "password1",
            "name": "Password",
            "type": "/password"
          },
          {
            "id": "password2",
            "name": "Limited special chars (.-_)",
            "label": "Password with .-_",
            "type": "/password/differentSpecialChars"
          },
          {
            "id": "password3",
            "name": "No special characters",
            "label": "Password without special",
            "type": "/password/noSpecialChars"
          },
          {
            "id": "password4",
            "name": "No numbers",
            "label": "Password without numbers",
            "type": "/password/noNumbers"
          }
        ]
      }
    ]
  },
  "address_mapping": {
    "id": "correct",
    "name": "Correct an Address Auto-fill Problem",
    "children": [
      {
        "id": "default",
        "name": "Default Address",
        "children": [
          {
            "id": "address1",
            "name": "Address Line 1",
            "type": "/contact/postaladdress"
          },
          {
            "id": "address2",
            "name": "Address Line 2",
            "type": "/contact/postaladdressAdditional"
          },
          {
            "id": "address3",
            "name": "Address Line 3",
            "type": "/contact/postaladdress3"
          },
          {
            "id": "city",
            "name": "City",
            "type": "/contact/city"
          },
          {
            "id": "state",
            "name": "State",
            "type": "/contact/state"
          },
          {
            "id": "statecode",
            "name": "State Code",
            "type": "/contact/state/code"
          },
          {
            "id": "country",
            "name": "Country",
            "type": "/contact/country"
          },
          {
            "id": "zip",
            "name": "Zip/Postal Code",
            "type": "/contact/postalcode"
          },
          {
            "id": "zip+",
            "name": "Zip+4",
            "type": "/contact/zip4"
          }
        ]
      },
      {
        "id": "billing",
        "name": "Billing Address",
        "children": [
          {
            "id": "address1",
            "name": "Address Line 1",
            "type": "/contact/postaladdress/billing"
          },
          {
            "id": "address2",
            "name": "Address Line 2",
            "type": "/contact/postaladdressAdditional/billing"
          },
          {
            "id": "address3",
            "name": "Address Line 3",
            "type": "/contact/postaladdress3/billing"
          },
          {
            "id": "city",
            "name": "City",
            "type": "/contact/city/billing"
          },
          {
            "id": "state",
            "name": "State",
            "type": "/contact/state/billing"
          },
          {
            "id": "statecode",
            "name": "State Code",
            "type": "/contact/state/code/billing"
          },
          {
            "id": "country",
            "name": "Country",
            "type": "/contact/country/billing"
          },
          {
            "id": "zip",
            "name": "Zip/Postal Code",
            "type": "/contact/postalcode/billing"
          },
          {
            "id": "zip+",
            "name": "Zip+4",
            "type": "/contact/zip4/billing"
          }
        ]
      },
      {
        "id": "shipping",
        "name": "Shipping Address",
        "children": [
          {
            "id": "address1",
            "name": "Address Line 1",
            "type": "/contact/postaladdress/shipping"
          },
          {
            "id": "address2",
            "name": "Address Line 2",
            "type": "/contact/postaladdressAdditional/shipping"
          },
          {
            "id": "address3",
            "name": "Address Line 3",
            "type": "/contact/postaladdress3/shipping"
          },
          {
            "id": "city",
            "name": "City",
            "type": "/contact/city/shipping"
          },
          {
            "id": "state",
            "name": "State",
            "type": "/contact/state/shipping"
          },
          {
            "id": "statecode",
            "name": "State Code",
            "type": "/contact/state/code/shipping"
          },
          {
            "id": "country",
            "name": "Country",
            "type": "/contact/country/shipping"
          },
          {
            "id": "zip",
            "name": "Zip/Postal Code",
            "type": "/contact/postalcode/shipping"
          },
          {
            "id": "zip+",
            "name": "Zip+4",
            "type": "/contact/zip4/shipping"
          }
        ]
      },
      {
        "id": "business",
        "name": "Business Address",
        "children": [
          {
            "id": "address1",
            "name": "Address Line 1",
            "type": "/contact/postaladdress/business"
          },
          {
            "id": "address2",
            "name": "Address Line 2",
            "type": "/contact/postaladdressAdditional/business"
          },
          {
            "id": "address3",
            "name": "Address Line 3",
            "type": "/contact/postaladdress3/business"
          },
          {
            "id": "city",
            "name": "City",
            "type": "/contact/city/business"
          },
          {
            "id": "state",
            "name": "State",
            "type": "/contact/state/business"
          },
          {
            "id": "statecode",
            "name": "State Code",
            "type": "/contact/state/code/business"
          },
          {
            "id": "country",
            "name": "Country",
            "type": "/contact/country/business"
          },
          {
            "id": "zip",
            "name": "Zip/Postal Code",
            "type": "/contact/postalcode/business"
          },
          {
            "id": "zip+",
            "name": "Zip+4",
            "type": "/contact/zip4/business"
          }
        ]
      },
      {
        "id": "home",
        "name": "Home Address",
        "children": [
          {
            "id": "address1",
            "name": "Address Line 1",
            "type": "/contact/postaladdress/home"
          },
          {
            "id": "address2",
            "name": "Address Line 2",
            "type": "/contact/postaladdressAdditional/home"
          },
          {
            "id": "address3",
            "name": "Address Line 3",
            "type": "/contact/postaladdress3/home"
          },
          {
            "id": "city",
            "name": "City",
            "type": "/contact/city/home"
          },
          {
            "id": "state",
            "name": "State",
            "type": "/contact/state/home"
          },
          {
            "id": "statecode",
            "name": "State Code",
            "type": "/contact/state/code/home"
          },
          {
            "id": "country",
            "name": "Country",
            "type": "/contact/country/home"
          },
          {
            "id": "zip",
            "name": "Zip/Postal Code",
            "type": "/contact/postalcode/home"
          },
          {
            "id": "zip+",
            "name": "Zip+4",
            "type": "/contact/zip4/home"
          }
        ]
      }
    ]
  },
  "phone_mapping": {
    "id": "correct",
    "name": "Correct an Phone Auto-fill Problem",
    "children": [
      {
        "id": "default",
        "name": "Default Phone",
        "children": [
          {
            "id": "full",
            "name": "Phone (xxx-yyy-zzzz)",
            "type": "/contact/phone"
          },
          {
            "id": "area",
            "name": "Phone areacode (xxx)",
            "type": "/contact/phone/areacode"
          },
          {
            "id": "exchange",
            "name": "Phone exchange (yyy)",
            "type": "/contact/phone/exchange"
          },
          {
            "id": "local",
            "name": "Phone local (zzzz)",
            "type": "/contact/phone/local"
          },
          {
            "id": "number",
            "name": "Phone last 7 (yyy-zzzz)",
            "type": "/contact/phone/number"
          },
          {
            "id": "country",
            "name": "Phone country code",
            "type": "/contact/phone/countrycode"
          },
          {
            "id": "ext",
            "name": "Phone extension",
            "type": "/contact/phone/extension"
          }
        ]
      },
      {
        "id": "home",
        "name": "Home Phone",
        "children": [
          {
            "id": "full",
            "name": "Phone (xxx-yyy-zzzz)",
            "type": "/contact/phone/home"
          },
          {
            "id": "area",
            "name": "Phone areacode (xxx)",
            "type": "/contact/phone/areacode/home"
          },
          {
            "id": "exchange",
            "name": "Phone exchange (yyy)",
            "type": "/contact/phone/exchange/home"
          },
          {
            "id": "local",
            "name": "Phone local (zzzz)",
            "type": "/contact/phone/local/home"
          },
          {
            "id": "number",
            "name": "Phone last 7 (yyy-zzzz)",
            "type": "/contact/phone/number/home"
          },
          {
            "id": "country",
            "name": "Phone country code",
            "type": "/contact/phone/countrycode/home"
          },
          {
            "id": "ext",
            "name": "Phone extension",
            "type": "/contact/phone/extension/home"
          }
        ]
      },
      {
        "id": "work",
        "name": "Work Phone",
        "children": [
          {
            "id": "full",
            "name": "Phone (xxx-yyy-zzzz)",
            "type": "/contact/phone/work"
          },
          {
            "id": "area",
            "name": "Phone areacode (xxx)",
            "type": "/contact/phone/areacode/work"
          },
          {
            "id": "exchange",
            "name": "Phone exchange (yyy)",
            "type": "/contact/phone/exchange/work"
          },
          {
            "id": "local",
            "name": "Phone local (zzzz)",
            "type": "/contact/phone/local/work"
          },
          {
            "id": "number",
            "name": "Phone last 7 (yyy-zzzz)",
            "type": "/contact/phone/number/work"
          },
          {
            "id": "country",
            "name": "Phone country code",
            "type": "/contact/phone/countrycode/work"
          },
          {
            "id": "ext",
            "name": "Phone extension",
            "type": "/contact/phone/extension/work"
          }
        ]
      },
      {
        "id": "mobile",
        "name": "Mobile Phone",
        "children": [
          {
            "id": "full",
            "name": "Phone (xxx-yyy-zzzz)",
            "type": "/contact/phone/mobile"
          },
          {
            "id": "area",
            "name": "Phone areacode (xxx)",
            "type": "/contact/phone/areacode/mobile"
          },
          {
            "id": "exchange",
            "name": "Phone exchange (yyy)",
            "type": "/contact/phone/exchange/mobile"
          },
          {
            "id": "local",
            "name": "Phone local (zzzz)",
            "type": "/contact/phone/local/mobile"
          },
          {
            "id": "number",
            "name": "Phone last 7 (yyy-zzzz)",
            "type": "/contact/phone/number/mobile"
          },
          {
            "id": "country",
            "name": "Phone country code",
            "type": "/contact/phone/countrycode/mobile"
          },
          {
            "id": "ext",
            "name": "Phone extension",
            "type": "/contact/phone/extension/mobile"
          }
        ]
      },
      {
        "id": "fax",
        "name": "Fax Phone",
        "children": [
          {
            "id": "full",
            "name": "Phone (xxx-yyy-zzzz)",
            "type": "/contact/phone/fax"
          },
          {
            "id": "area",
            "name": "Phone areacode (xxx)",
            "type": "/contact/phone/areacode/fax"
          },
          {
            "id": "exchange",
            "name": "Phone exchange (yyy)",
            "type": "/contact/phone/exchange/fax"
          },
          {
            "id": "local",
            "name": "Phone local (zzzz)",
            "type": "/contact/phone/local/fax"
          },
          {
            "id": "number",
            "name": "Phone last 7 (yyy-zzzz)",
            "type": "/contact/phone/number/fax"
          },
          {
            "id": "country",
            "name": "Phone country code",
            "type": "/contact/phone/countrycode/fax"
          },
          {
            "id": "ext",
            "name": "Phone extension",
            "type": "/contact/phone/extension/fax"
          }
        ]
      }
    ]
  }
};

      var dataTypeMapping = null;

      function loadDataTypeNames(node) {
        if (node["children"]) {
          for (var i=0;i<node.children.length;i++) {
            loadDataTypeNames(node.children[i]);
          }
        } else if (node["type"]) {
          if (dataTypeMapping[node["type"]]) {
            return;
          }
          var label;
          if (node["label"])
            label = node["label"];
          else
            label = node["name"];
          label = label.replace(/\([^\)]+\)/g, '').replace(/^[\s]+|[\s]+$/g, '');
          if (label.indexOf("{") != -1) {
            return;
          }
          dataTypeMapping[node["type"]] = label;
        }
      }

      function getDataTypeName(data_type) {
        if (dataTypeMapping == null) {
          dataTypeMapping = {};
          for (var key  in MENU) {
            if (!(MENU[key].length >= 0)) {
              loadDataTypeNames(MENU[key]);
            }
          }
        }
        return dataTypeMapping[data_type] || "Unknown";
      }

      function getFormTypeName(type) {
        if (type == null) {
          return "Other";
        }
        var types = {
          "loginForm": "Login",
          "registrationForm": "Register",
          "checkoutForm": "Checkout",
          "addressForm": "Address",
          "changePasswordForm": "Change Password"
        };
        if (type in types) {
          return types[type];
        }
        return "Other";
      }

      function getEntitiesMenu(root, labels, child, id, entities) {
        var menus = [];
        if (!entities) return menus;
        var newLabels = {};
        for (var i in labels) {
          newLabels[i] = labels[i];
        }
        var idx=0;
        for (var i=0;i<entities.length;i++) {
          var entity = entities.at(i);
          var jsonObject = entity.toFillData();
          var menu = {};
          setMenuLabel(labels, child, jsonObject, menu);
          if (menu.label.indexOf('{') != -1) {
            continue;
          }
          setPanelTitle(labels, child, menu);
          menu.path = root + "." + id + "." + idx;
          menu.hasChildren = true;
          menus.push(new MappingMenu(menu));
          idx++;
        }
        return menus;
      }



      function getEntityMenu(root, labels, child, id, entity, entityIndex) {
        var menu = {};
        var jsonObject = entity.toFillData();

        setMenuLabel(labels, child, jsonObject, menu);

        if (menu.label.indexOf('{') != -1) {
          return null;
        }
        menu.path = root + "." + id + "." + entityIndex;
        if (child["children"]) {
          menu.hasChildren = true;
        }
        setPanelTitle(labels, child, menu);
        setMenuAttributes(child, menu);
        if (menu.value != null) {
          menu.value = substituteLabels(jsonObject, menu.value);
          if (menu.value.indexOf('{') != -1) {
            return null;
          }
        }
        return new MappingMenu(menu);
      }

      function getMenu(root, panelDisabledHere, labels, child, id) {
        var menu = {};
        menu.label = substituteLabels(labels, child["name"]);
        if (menu.label.indexOf('{') != -1 && child["alternate_name"]) {
          menu.label = substituteLabels(labels, child["alternate_name"]);
        }
        if (menu.label.indexOf('{') != -1) {
          return null;
        }
        menu.path = root + "." + id;

        setPanelTitle(labels, child, menu);

        if (child["children"]) {
          menu.hasChildren = true;
        } else {
          setMenuAttributes(child, menu);
          if (panelDisabledHere && menu.label.indexOf("never show auto-fill here") != -1) {
            // panel disabled here, so change menu to enable here
            menu.label = "Auto-fill disabled here, please enable.<br/>(show auto-fill here)";
            menu.action = "show_here";
          }
        }
        return new MappingMenu(menu);
      }

      function setMenuAttributes(child, menu) {
        if (child["action"]) {
          menu.action = child["action"];
        }
        if (child["type"]) {
          menu.type = child["type"];
        }
        if (child["formType"]) {
          menu.formType = child["formType"];
        }
        if (child["value"]) {
          menu.value = child["value"];
        }
      }

      function setPanelTitle(labels, child, menu) {
        if (child["panel_title"]) {
          menu.panelTitle = substituteLabels(labels, child["panel_title"]);
          if (menu.panelTitle.indexOf("{") != -1) {
            menu.panelTitle = menu.panelTitle.replace(/\{[^\}]+\}/g, "Unknown");
          }
        }
      }

      function setMenuLabel(labels, child, jsonObject, menu) {
        menu.label = substituteLabels(labels, child["name"]);
        menu.label = substituteLabels(jsonObject, menu.label);
        if (menu.label.indexOf('{') != -1 && child["alternate_name"]) {
          menu.label = substituteLabels(labels, child["alternate_name"]);
          menu.label = substituteLabels(jsonObject, menu.label);
        }
      }


      var addressMatcher = /postal|city|state|contact\/country/i;
      var identityMatcher = /(nameperson\/(first|last|middle|full))|social|company|dob|driver|website/i;
      var cardMatcher = /financial/i;
      var phoneMatcher = /phone/i;
      var loginMatcher = /\/email|\/friendly|\/password/i;

      function getGroupName(data_type, form_type) {
        if (data_type.match(addressMatcher)) {
          return "Address";
        } else if (data_type.match(identityMatcher)) {
          return "Name";
        } else if (data_type.match(cardMatcher)) {
          return "Card";
        } else if (data_type.match(phoneMatcher)) {
          return "Phone";
        } else if (data_type.match(loginMatcher)) {
          if (form_type == "loginForm") {
            return "Login";
          } else if (form_type == "registrationForm") {
            return "Register";
          }
        }
        return null;
      }

      function adjustReference(rootNode, child) {
        if (child["ref"]) {
          var nameOverride = null;
          if (child["name"]) {
            nameOverride = child["name"];
          }
          child = rootNode[child["ref"]];
          if (nameOverride != null) {
            child["name"] = nameOverride;
          }
        }
        return child;
      }


      function substituteLabels(labels, str) {
        for (var label in labels) {
          if (!labels[label]) continue;
          if (label.indexOf('{') == -1) {
            var pattern = "{"+label+"}";
          } else {
            var pattern = label;
          }
          var pattern = pattern.replace(/[\{\}]/g, '\\$&');
          str = str.replace(new RegExp(pattern, 'g'), labels[label])
        }
        return str;
      }

      var MappingMenu = models.BaseModel.extend({

        initialize: function () {
        }
      });

      var MappingMenuCollection = models.BaseCollection.extend({
        model: MappingMenu,

        initialize: function () {
        },

        loadMenu: function (root, fieldType, formType, panelDisabledHere, allData) {
          var labels = {};

          var entityIndex = -1;
          if (root.match(/.*\.[0-9]+$/)) {
            var lastDot = root.lastIndexOf(".");
            entityIndex = parseInt(root.substr(lastDot + 1));
            root = root.substring(0, lastDot);
          }
          if (fieldType != null) {
            labels["{data_type}"] = getDataTypeName(fieldType);
          }
          if (formType != null) {
            labels["{form_type}"] = getFormTypeName(formType);
          }

          if (fieldType != null && formType != null) {
            var groupName = getGroupName(fieldType, formType);
            if (groupName != null) {
              labels["{group}"] = groupName;
            }
          }
          labels["{app}"] = "site";
          if (allData.email) {
            labels["{email}"] = allData.email;
          }

          var expandMore = root.indexOf(".more") != -1;

          var list = [];
          var path = root.replace(".more", "").split(/\./);

          var rootNode = MENU;
          var node = {};
          var rootObj = {children: []};
          node["root"] = rootObj;
          if (fieldType != null) {
            if (fieldType.match(addressMatcher)) {
              formType = "addressForm";
            } else if (fieldType.match(phoneMatcher))  {
              formType = "phoneForm";
            } else if (fieldType.match(cardMatcher))  {
              formType = "checkoutForm";
            } else if (fieldType.match(identityMatcher))  {
              formType = "identityForm";
            }
          }
          if (!rootNode[formType]) {
            formType = "otherForm";
          }


          // set first level menu
          var items = rootNode[formType];
          for (var i=0;i<items.length;i++) {
            var name = items[i];
            if (name == "more" && expandMore) {
              // exclude more
              continue;
            }
            if (name == "dont_show") name = "dont_show_extension";
            var nodeToAdd = rootNode[name];
            if (nodeToAdd["forType"] && (fieldType == null || !fieldType.match(new RegExp(nodeToAdd["forType"], "i")))) {
              continue;
            }
            rootObj["children"].push(nodeToAdd);
            if (!expandMore && name == "more") {
              break;
            }
          }


          for (var i=0;i<path.length;i++) {
            var part = path[i];
            if (node[part]) {
              node = node[part];
            } else if (node["children"]) {
              var children = node["children"];
              for (var j=0;j<children.length;j++) {
                var child = children[j];
                child = adjustReference(rootNode, child);
                if (child["id"] == part) {
                  node = child;
                }
              }
            }
          }


          if (node["children"]) {
            var children = node["children"];
            for (var i=0;i<children.length;i++) {
              var child = children[i];
              child = adjustReference(rootNode, child);
              var id = child["id"];
              if (child["when"]) {
                var when = child["when"];
                if (when.indexOf("has_data") != -1 && allData[when.replace("_has_data", "")].length == 0) {
                  continue;
                }
              }
              // for now do not show mask my card.
              if (child.action == 'mask_card') {
                continue;
              }
              if (child["entities"]) {
                var menus = getEntitiesMenu(root, labels, child, id, allData[child["entities"]]);
                list = list.concat(menus);
                allData["CurrentEntity"] = null;
              } else if (child["entity"]) {
                var entities = allData[child["entity"]];
                var entity;
                if (entityIndex == -1) {
                  entity = allData["CurrentEntity"];
                } else {
                  entity = entities.at(entityIndex);
                  allData["CurrentEntity"] = entity;
                }
                var menu = getEntityMenu(root, labels, child, id, entity, entityIndex);
                if (menu != null) {
                  list.push(menu);
                }
              } else {
                var menu = getMenu(root, panelDisabledHere, labels, child, id);
                if (menu != null) {
                  list.push(menu);
                }
              }
            }
          }

          for (var i=0;i<list.length;i++) {
            list[i].set('idx', i);
          }

          this.reset(list);
        }

      });

      models.MappingMenu = MappingMenu;
      models.MappingMenuCollection = MappingMenuCollection;

      return {
        MappingMenu: MappingMenu,
        MappingMenuCollection: MappingMenuCollection
      };
    });


ABINE_DNTME.define('abine/models/shieldedCard',
    ['models','documentcloud/underscore', 'abine/eventLogger'],
    function(models,_, logger) {

  var ShieldedCard = models.BaseModel.extend({
    storageEntity: 'shieldedCard',

    interestedBroadcasts: /broadcast:billinginfo:updated/i,

    generateDisposable: function() {
      return new models.DisposableCard({
        shielded_card_id: this.get("id")
      });
    },

    getName: function(){
      return this.get("first_name") + " " + this.get("last_name");
    },

    getExpiry: function(){
      return parseInt(this.get("month")) + " / " + parseInt(this.get("year"));
    },

    getFirstName: function(){
      return this.get("first_name").toUpperCase();
    },

    getLastName: function(){
      return this.get("last_name").toUpperCase();
    },

    getLastFour: function(){
      return this.get("last_four");
    },

    getProtectedNumber: function() {
      if (!this.get('number')) return '---';
      var number = this.get('number')+"";
      return number.substr(0, number.length-4).replace(/./g, 'x')+number.substr(number.length-4);
    },

    getProtectedVerification: function() {
      if (!this.get('number')) return '---';
      var number = this.get('verification')+"";
      return number.substr(0, number.length-1).replace(/./g, 'x')+number.substr(number.length-1);
    }

  });

  var ShieldedCardCollection = models.BaseCollection.extend({
    model: ShieldedCard,

    getPrimaryCardName: function(){
      if(this.models.length > 0){
        return this.models[0].getFirstName + " " + this.models[0].getLastName()
      } else {
        return "";
      }
    },

    getPrimaryCardLastFour: function(){
      if(this.models.length > 0){
        return this.models[0].getLastFour();
      } else {
        return "";
      }
    },

    getPrimaryCardExpiry: function(){
      if(this.models.length > 0){
        return this.models[0].get
      } else {
        return "";
      }
    }
  });

  models.ShieldedCard = ShieldedCard;
  models.ShieldedCardCollection = ShieldedCardCollection;

  return {
    ShieldedCard: ShieldedCard,
    ShieldedCardCollection: ShieldedCardCollection
  };
});


ABINE_DNTME.define('abine/models/shieldedEmail',
   ['models'],
   function(models) {

  // ShieldedEmail Model

  var AltEmail = models.BaseModel.extend({
  });

  var ShieldedEmail = models.BaseModel.extend({
    storageEntity: 'shieldedEmail',

    initialize: function(options) {
      this.messenger = options.messenger || this.getMessenger();
      this.hasMany({
        altEmails: { collection: AltEmailCollection }
      });
    },

    generateDisposable: function(label, source) {
      return new models.DisposableEmail({
        targetGuid: this.get("guid"),
        label: label,
        source: source
      });
    },

    getAddress: function() {
      return this.get('email');
    },

    isValidated: function(){
      return this.get("active") == 1;
    },

    resendValidationEmail: function(callback){
      this.sync('update', this, {
        eventName: "update:shieldedEmail:resendValidationEmail:" + this.get("id"),
        success: function(data) {
          callback(data);
        },
        error: function(err) {
          LOG_ERROR && ABINE_DNTME.log.error("[ERROR HANDLER]#[abine/models/shieldedEmail]#[resendValidationEmail]# error event in model.sync eventName: update:shieldedEmail:resendValidationEmail:id");
          callback(err);
        }
      });
    }
  });

  // ShieldedEmail Collection

  var ShieldedEmailCollection = models.BaseCollection.extend({
    model: ShieldedEmail,
    interestedBroadcasts: /broadcast:register:success/i
  });

  var AltEmailCollection = models.BaseCollection.extend({
    model: AltEmail
  });

  models.ShieldedEmail = ShieldedEmail;
  models.ShieldedEmailCollection = ShieldedEmailCollection;

  return {
    ShieldedEmail: ShieldedEmail,
    ShieldedEmailCollection: ShieldedEmailCollection
  };
});


ABINE_DNTME.define('abine/models/shieldedPhone',
    ['models','documentcloud/underscore'],
    function(models,_) {

  var ShieldedPhone = models.BaseModel.extend({
    storageEntity: 'shieldedPhone',

    generateDisposable: function() {
      return new models.DisposablePhone({
        shielded_phone_id: this.get("id")
      });
    },

    hasNumber: function(){
      if(this.has("number") && this.get("number") != ""){
        return true;
      } else {
        return false;
      }
    },

    registerNumber: function(options){
      this.sync('create', this, {
        eventName: "create:shieldedPhone",
        success: options.success,
        error: options.error
      });
    },

    verifySecret: function(options){
      this.sync('update', this, {
        eventName: "update:shieldedPhone:secret",
        success: options.success,
        error: options.error
      });
    },

    getCountryCode: function(){
      return this.get('country_code') || "1";
    },

    getAreaCode: function(){
      return this.get('number').substr(0,3);
    },

    getExchange: function() {
      return this.get('number').substr(3,3);
    },
    
    getLocal: function() {
      return this.get('number').substr(6,4);
    },
    
    getPrettyNumber: function() {
      if (this.isNorthAmericaFormat()){
        return "(" + this.getAreaCode() + ") "
          + this.getExchange() + "-"
          + this.getLocal();
      } else if (this.isEuropeanFormat()){
        return "0" + this.get('number');
      } else {
        return this.get('number');
      }
    },

    getFullPrettyNumber: function() {
      if (!this.isNorthAmericaFormat())
        return '+'+this.getCountryCode()+' '+this.get('number');
      return '+'+this.getCountryCode()+" (" + this.getAreaCode() + ") "
        + this.getExchange() + "-"
        + this.getLocal();
    },

    isNorthAmericaFormat: function() {
      return this.getCountryCode() == "1";
    },

    isEuropeanFormat: function() {
      var firstDigit = this.getCountryCode()[0];
      return ((firstDigit == "3") || (firstDigit == "4"));
    },

    isTextAllowed: function() {
      return this.getCountryCode() in {"1":1,"44":1};
    },

    isValidated: function(){
      return this.get("is_validated") == "true";
    }
  });

  var ShieldedPhoneCollection = models.BaseCollection.extend({
    model: ShieldedPhone
  });

  models.ShieldedPhone = ShieldedPhone;
  models.ShieldedPhoneCollection = ShieldedPhoneCollection;

  return {
    ShieldedPhone: ShieldedPhone,
    ShieldedPhoneCollection: ShieldedPhoneCollection
  };
});


ABINE_DNTME.define('abine/models/tabData',
   ['documentcloud/underscore', 'models', 'jquery', 'abine/intl'],
   function(_, models, $, intl) {

   var PageTracker = models.BaseModel.extend({
     storageEntity: 'pageTracker',

     hasSuggested: function(){return true;}
   });

   var PageTrackerCollection = models.BaseCollection.extend({
     model: PageTracker,

     getBlocked: function() {
       var blocked = 0;
       for (var i=0;i<this.length;i++) {
         if (this.at(i).get('blocked') == 1) {
           blocked++;
         }
       }
       return blocked;
     },

     getAllowed: function() {
       var allowed = 0;
       for (var i=0;i<this.length;i++) {
         if (this.at(i).get('blocked') != 1) {
           allowed++;
         }
       }
       return allowed;
     }
   });

   var BlacklistOrPreference = models.BaseModel.extend({
     storageEntity: 'blacklistOrPreference'
   });

   var CustomTabData = models.BaseModel.extend({
     storageEntity: 'customTabData'
   });

   var CustomTabDataCollection = models.BaseCollection.extend({
     model: CustomTabData,

     isFeedbackShown: function() {
       for (var i=0;i<this.length;i++) {
         if (this.at(i).get('name') == 'feedbackShown') {
           return this.at(i).get('value');
         }
       }
       return false;
     },

     changedFeature: function() {
       for (var i=0;i<this.length;i++) {
         if (this.at(i).get('name') == 'changedFeature') {
           var feature = this.at(i).get('value');

           // if its an individual tracker skip trying
           // to render text
           if (feature == '') {
             break;
           }
           var changedFeatureOffStatus = this.changedFeatureOffStatus();
           var onOff = changedFeatureOffStatus? 'off':'on'

           return intl.getText('mm_dntme_toggled_' + feature + '_' + onOff);
         }
       }
       return null;
     },

     changedFeatureOffStatus: function() {
       for (var i=0;i<this.length;i++) {
         if (this.at(i).get('name') == 'changedFeatureOffStatus') {
           return this.at(i).get('value');
         }
       }
       return null;
     },

     setCustomTabData: function(name, value) {
       var model = null;
       for (var i=0;i<this.length;i++) {
         if (this.at(i).get('name') == name) {
           model = this.at(i);
         }
       }
       if (!model) {
         model = new CustomTabData();
         this.add(model);
       }
       model.save({name: name, value: value});
     }

   });

   var TabData = models.BaseModel.extend({
     storageEntity: 'tabData',

     initialize: function(options) {
       this.messenger = options.messenger || this.getMessenger();
       this.hasMany({
         trackers: { collection: PageTrackerCollection },
         custom: {collection: CustomTabDataCollection }
       });
       this.hasOne({
         blacklist: {model: models.Blacklist},
         dntStats: {model: models.DntStats}
       });
     },

     hasFiveOrMoreTrackers: function() {
       return this.trackers.length >= 5;
     },

     hasMobileAppAndDropboxEnabled: function() {
       return this.get('hasMobileApp') && this.get('dropboxEnabled') && this.get('entitledToSync');
     },

     fetchTrackers: function(options) {
       options = options || {};
       options.eventName = 'read:'+this.storageEntity+':trackers';
       this.fetch(options);
     }
   });

   models.TabData = TabData;
   models.CustomTabData = CustomTabData;
   models.BlacklistOrPreference = BlacklistOrPreference;

   return {
     TabData: TabData
   };
});


ABINE_DNTME.define('abine/models/pageData',
  ['documentcloud/underscore', 'models'],
  function(_, models) {

    var PageData = models.BaseModel.extend({
      storageEntity: 'pageData'
    });

    models.PageData = PageData;

    return {
      PageData: PageData
    };
  });


ABINE_DNTME.define('abine/models/address',
   ['models', 'documentcloud/underscore'],
   function(models, _) {

  var Address = models.BaseModel.extend({
    storageEntity: 'address',
    getFormattedText: function() {
      var str = [this.get('address1'), this.get('address2'), this.get('city'), this.get('state'), this.get('zip')].join(",");
      str = str.replace(/(\s*,\s*)+/g, ', ');
      return str;
    },
    toFillData: function() {
      return this.toJSON();
    }
  });

  var AddressCollection = models.BaseCollection.extend({
    model: Address,

    initialize: function() {
      this.messenger = this.getMessenger();
    }
  });

  models.Address = Address;
  models.AddressCollection = AddressCollection;

  return {
    Address: Address,
    AddressCollection: AddressCollection
  };
});


ABINE_DNTME.define('abine/models/account',
   ['models', 'documentcloud/underscore'],
   function(models, _) {

  var Account = models.BaseModel.extend({
    storageEntity: 'account',
    getFormattedText: function() {
      var str = [];
      if (this.get('email')) {
        str.push('Email: '+this.get('email'));
      }
      if (this.get('username')) {
        str.push('Username: '+this.get('username'));
      }
      if (this.get('password')) {
        str.push('Password: '+this.get('password'));
      }
      return str.join('\n');
    },
    getAddress: function() {
      var label = this.get('label');
      if (label && label.length > 0 && label != this.get('domain')) {
        return label;
      }
      var email = this.get('email');
      if (email && email.length > 0) {
        return email;
      }
      var username = this.get('username');
      if (username && username.length > 0) {
        return username;
      }
      var password = this.get('password');
      if (password && password.length > 0) {
        return '- Only Password -';
      }
      return '- Not set -';
    },
    toFillData: function() {
      return {
        "label": this.getAddress(),
        "email": this.get('email'),
        "username": this.get('username'),
        "password": this.get('password')

      };
    }
  });

  var AccountCollection = models.BaseCollection.extend({
    model: Account,

    initialize: function() {
      this.messenger = this.getMessenger();
    },

    fetchAllByDomain: function(domain, callback) {
      if (domain) domain = domain.replace(":", "::");
      this.sync('read', this, {
        eventName: "read:account:domain:"+domain,
        success: function(data) {
          this.reset(data);
          callback();
        },
        error: function() {
          LOG_ERROR && ABINE_DNTME.log.error("[ERROR HANDLER]#[abine/models/account]#[fetchAllByDomain]# error event in model.sync eventName: read:account:domain:domain");
        }
      });
    },

      fetchLoginsByDomain: function(domain, callback) {
          if (domain) domain = domain.replace(":", "::");
          this.sync('read', this, {
              eventName: "read:logins:domain:"+domain,
              success: function(data) {
                  this.reset(data);
                  callback();
              },
              error: function() {
                  LOG_ERROR && ABINE_DNTME.log.error("[ERROR HANDLER]#[abine/models/account]#[fetchAllByDomain]# error event in model.sync eventName: read:account:domain:domain");
              }
          });
      },

      fetchAccountsByPassword: function(password, callback) {
          this.sync('read', this, {
              eventName: "read:accounts:password:"+password,
              success: function(data) {
                  this.reset(data);
                  callback();
              },
              error: function() {
                  LOG_ERROR && ABINE_DNTME.log.error("[ERROR HANDLER]#[abine/models/account]#[fetchAllByDomain]# error event in model.sync eventName: read:account:domain:domain");
              }
          });
      }
  });

  models.Account = Account;
  models.AccountCollection = AccountCollection;

  return {
    Account: Account,
    AccountCollection: AccountCollection
  };
});


ABINE_DNTME.define('abine/models/identity',
   ['models', 'documentcloud/underscore', 'abine/intl', 'abine/assets'],
   function(models, _, intl, assets) {

  var Identity = models.BaseModel.extend({
    storageEntity: 'identity',

    getLabel: function(){
      var lbl = this.get('label');
      if (lbl && lbl.length > 15) lbl = lbl.substr(0, 12)+'...';
      return lbl;
    },

    toFillData: function() {
      var json = this.toJSON();
      json.full_name = (this.get('first_name')+" "+this.get('last_name')).replace(/^[\s]+|[\s]+$/g, '').replace(/[\s]+$/g, ' ');
      if (json.email && json.email.indexOf('@') != -1) {
        json.real_email = json.email;
      }
      return json;
    }

  });

  var IdentityCollection = models.BaseCollection.extend({
    model: Identity,

    initialize: function() {
      this.messenger = this.getMessenger();
    }
  });

  models.Identity = Identity;
  models.IdentityCollection = IdentityCollection;

  return {
    Identity: Identity,
    IdentityCollection: IdentityCollection
  };
});


ABINE_DNTME.define('abine/models/realCard',
   ['models', 'documentcloud/underscore', 'abine/intl', 'abine/assets'],
   function(models, _, intl, assets) {

  var RealCard = models.BaseModel.extend({
    storageEntity: 'realCard',

    getLastFour: function(){
      return this.get("number").substr(this.get("number").length-4);
    },

    getNonDefaultLabel: function() {
      var label = this.get("label");
      if (label && label.toLowerCase() == (this.getType()+' '+this.getLastFour()).toLowerCase()) {
        return null;
      }
      return label;
    },

    labelChangeRequired: function() {
      var label = this.get("label");
      if (label && (label.length > 15 || label.toLowerCase() == (this.getType()+' '+this.getLastFour()).toLowerCase())) {
        return true;
      }
      return false;
    },

    getLabelOrLastFour: function() {
      if (this.getNonDefaultLabel()) {
        return intl.getText('mm_payment_helper_useCardName', [this.getLabel()]);
      } else {
        var displayType = this.getDisplayType();
        if (displayType) {
          return intl.getText('mm_payment_helper_useCardName', [displayType + '-' + this.getLastFour()]);
        } else {
          return intl.getText('mm_payment_helper_useCardName', ['x' + this.getLastFour()]);
        }
      }
    },

    getCardNumber: function(){
      return this.get("number");
    },

    getSeparatedCardNumber: function(){
      var cardNumber = this.get("number");
      if (cardNumber.length == 16) {
        return (cardNumber.substring(0, 4) + ' ' + cardNumber.substring(4, 8)
          + ' ' + cardNumber.substring(8, 12) + ' ' + cardNumber.substring(12, 16));
      } else if (this.get('type') && this.get('type').match(/amex|american/i)) {
        return cardNumber.substr(0, 4) + ' ' + cardNumber.substr(4, 6) + ' ' + cardNumber.substr(10);
      } else {
        return cardNumber;
      }
    },

    getSeparatedMaskedCardNumber: function(){
      var cardNumber = this.get("number");
      if (cardNumber.length == 16) {
        return (Array(4).join('**** ')) + cardNumber.substring(12, 16);
      } else {
        var type = this.get('type');
        if (type && type.match(/amex|american/i)) {
          return '**** ****** *' + cardNumber.substr(-4);
        } else {
          return (Array(cardNumber.length-3).join('*'))+cardNumber.substr(-4);
        }
      }
    },

    getMaskedVerification: function(){
      return Array(this.get('cvc').length+1).join('*');
    },

    getMaskedExpiry: function(){
      return this.getExpiry().replace(/[0-9]/g, '*');
    },

    getBrandImage: function() {
      var brand = this.getBrand();
      if(!brand || !brand.match(/asd/)){
        brand = 'no_match';
      }
      return assets.assetUrl("/pages/images/blank_cards/" + brand + ".png");
    },

    getTypeImage: function() {
      var type = this.getType();
      if (!type.match(/american_express|diners_club|discover|jcb|laser|maestro|mastercard|visa/)) {
        type = 'no_match';
      }
      return assets.assetUrl("/pages/images/credit_cards/"+type+".png");
    },

    getAddress: function() {
      var address = this.get('address');
      if (!address) return null;
      var str = [address.address1, address.address2, address.city, address.state, address.zip].join(",");
      str = str.replace(/(\s*,\s*)+/g, ', ');
      return str;
    },
    getFormattedText: function() {
      var str = [
        this.get('first_name')+' '+this.get('last_name'),
        this.getPrettyNumber(),
        'Exp: '+this.getExpiry()+' CVC: '+this.getCVC(),
        this.getAddress()
      ].join('\n');
      str = str.replace(/[\n]+/g, '\n');
      return str;
    },

    getAddress1: function() {
      var address = this.get('address');
      if (!address) return null;
      return address.address1.replace(/(\s*,\s*)+/g, ', ');
    },

    getAddress2: function() {
      var address = this.get('address');
      if (!address) return null;
      return address.address2.replace(/(\s*,\s*)+/g, ', ');
    },

    getCityState: function() {
      var address = this.get('address');
      if (!address) return null;
      var str = [address.city, address.state].join(",");
      str = str.replace(/(\s*,\s*)+/g, ', ');
      return str;
    },

    getPostal: function() {
      var address = this.get('address');
      if (!address) return null;
      return address.zip.replace(/(\s*,\s*)+/g, ', ');
    },

    getType: function(){
      return this.get("type").toLowerCase();
    },

    getDisplayType: function(){
      var type = this.getType();
      if (type.match(/visa/)) {
        return 'Visa';
      } else if (type.match(/master/)) {
        return 'Master';
      } else if (type.match(/amex|american/)) {
        return 'Amex';
      } else if (type.match(/diner/)) {
        return 'Diners';
      } else if (type.match(/discover/)) {
        return 'Discover';
      } else if (type.match(/jcb/)) {
        return 'Jcb';
      } else if (type.match(/laser/)) {
        return 'Lazer';
      } else if (type.match(/maestro/)) {
        return 'Maestro';
      }
      return null;
    },

    getBrand: function(){
      return this.get("brand").toLowerCase();
    },

    getPrettyNumber: function(){
      var pretty = "";

      for(var i=0;i<this.get("number").length;i++){
        pretty += this.get("number").charAt(i);

        if((i%4) == 3){
          pretty+= "  ";
        }
      }

      return pretty;
    },

    getCVC: function(){
      return this.get("cvc");
    },

    getExpiry: function(){
      var month = parseInt(this.get('expiry_month'));
      if (month < 10) month = '0'+month;
      return month+'/'+this.get('expiry_year');
    },

    getLabel: function(){
      var lbl = this.get('label');
      if (lbl && lbl.length > 15) lbl = lbl.substr(0, 12)+'...';
      return lbl;
    },

    getProtectedNumber: function() {
      var number = this.get('number')+"";
      return number.substr(0, number.length-12).replace(/./g, 'x')+number.substr(number.length-4);
    },

    getProtectedVerification: function() {
      var number = this.get('verification')+"";
      return number.substr(0, number.length-1).replace(/./g, 'x')+number.substr(number.length-1);
    },

    isRealCard: function(){
      return this.get("cardType") == "real_card";
    },

    isOfferCard: function(){
      return this.get("cardType") == "offer";
    },

    isNewMaskedCard: function(){
      return this.get("cardType") == "new_masked_card";
    },

    isNewRealCard: function(){
      return this.get("cardType") == "new_real_card";
    },

    toFillData: function() {
      var json = this.toJSON();
      var number = json.number;
      json.last_4 = number.substr(number.length-4);
      if (number.length == 16) {
        json.first_4 = number.substring(0, 4);
        json.second_4 = number.substring(4, 8);
        json.third_4 = number.substring(8, 12);
      } else if (json.type && json.type.match(/ame/i)) {
        json.amex_4 = number.substring(0, 4);
        json.amex_6 = number.substring(4, 10);
        json.amex_5 = number.substring(10, 15);
      }
      if (json.expiry_year && json.expiry_month) {
        json.expiry = json.expiry_month+"/"+json.expiry_year;
        json.expiry2 = json.expiry_month+"/"+json.expiry_year.substring(2);
      }
      if (json.expiry_year) {
        json.expiry_year2 = json.expiry_year.substring(2);
      }
      json.full_name = (json.first_name+" "+json.last_name).replace(/^[\s]+|[\s]+$/g, '').replace(/[\s]+$/g, ' ');
      if (this.get('address') != null) {
        json.address = this.get('address');
      }

      return json;
    }

  });

  var RealCardCollection = models.BaseCollection.extend({
    model: RealCard,

    initialize: function() {
      this.messenger = this.getMessenger();
    },

    fetchOfferCardsForDomain: function(domain, cb) {
      if (domain) domain = domain.replace(":", "::");
      this.sync('read', this, {
        eventName: "read:realCard:offers:"+domain,
        success: function(data) {
          this.reset(data);
          cb();
        },
        error: function() {
          LOG_ERROR && ABINE_DNTME.log.error("[ERROR HANDLER]#[abine/models/realCard]#[fetchAllForDomain]# error event in read:realCard:offers:domain");
          cb(true);
        }
      });
    },

    fetchAllForDomain: function(domain, cb) {
      if (domain) domain = domain.replace(":", "::");
      this.sync('read', this, {
        eventName: "read:realCard:site:"+domain,
        success: function(data) {
          this.reset(data);
          cb();
        },
        error: function() {
          LOG_ERROR && ABINE_DNTME.log.error("[ERROR HANDLER]#[abine/models/realCard]#[fetchAllForDomain]# error event in read:realCard:site:domain");
          cb(true);
        }
      });
    }

  });

  models.RealCard = RealCard;
  models.RealCardCollection = RealCardCollection;

  return {
    RealCard: RealCard,
    RealCardCollection: RealCardCollection
  };
});


ABINE_DNTME.define('abine/models/globalData',
   ['documentcloud/underscore', 'models', 'jquery','abine/intl'],
   function(_, models, $, intl) {


   var GlobalTracker = models.BaseModel.extend({
     storageEntity: 'globalTracker',

     hasSuggested: function(){return false;}
   });

   var GlobalTrackerCollection = models.BaseCollection.extend({
     model: GlobalTracker
   });

   var GlobalPreference = models.BaseModel.extend({
     storageEntity: 'globalPreference',

     getLabel: function() {
       return intl.getText('mm_dntme_'+this.get('name').toLowerCase());
     },

     getId: function() {
       return this.get('name').toLowerCase();
     },

     getTooltip: function() {
       return intl.getText('mm_dntme_help_'+this.get('name').toLowerCase());
     }
   });

   var GlobalPreferenceCollection = models.BaseCollection.extend({
     model: GlobalPreference
   });

   var GlobalFeature = models.BaseModel.extend({
     storageEntity: 'globalFeature',

     getLabel: function() {
       //{browsing:1,email:1,card:1,phone:1,suggestStrongPassword:1,suggestAddresses:1, suggestRealCard:1, search:1};
       var name = this.get('name');
       switch(name){
         case 'suggestStrongPassword':
           return intl.getText('mm_dntme_accounts_password');
         case 'suggestAddresses':
           return intl.getText('mm_dntme_wallet_address');
         case 'suggestRealCard':
           return intl.getText('mm_dntme_wallet_real_card');
         case 'rememberCards':
           return intl.getText('mm_dntme_wallet_save_card');
         case 'suggestIdentities':
           return intl.getText('mm_dntme_wallet_identity');
         case 'browsing':
           return intl.getText('mm_dntme_tracking_blocker');
         case 'email':
           return intl.getText('mm_dntme_masking_email');
         case 'phone':
           return intl.getText('mm_dntme_masking_phone');
         case 'card':
           return intl.getText('mm_dntme_masking_card');
         case 'search':
           return intl.getText('mm_dntme_masking_search');
         case 'helpMeLogin':
           return intl.getText('mm_dntme_accounts_help_login');
         case 'submitAccount':
           return intl.getText('mm_dntme_accounts_submit_account');
         case 'rememberAccounts':
           return intl.getText('mm_dntme_accounts_save_account');
         case 'showCheckoutPanelGrowl':
           return intl.getText('mm_dntme_showcheckoutpanelgrowl');
       };

//       return intl.getText('mm_dntme_dont_track_'+this.get('name').toLowerCase());
     },

     isBrowsing: function() {
       return this.get('name') == 'browsing';
     },

     getTooltip: function() {
       var onOff = this.get('value')?'off':'on';
       if(this.isBrowsing()){
         return intl.getText('mm_dntme_help_what_will_happen_'+ onOff + '_' + this.get('name').toLowerCase(), "all sites");
       }
       return intl.getText('mm_dntme_help_what_will_happen_'+ onOff + '_' + this.get('name').toLowerCase());
     }
   });

   var GlobalFeatureCollection = models.BaseCollection.extend({
     model: GlobalFeature
   });

   var GlobalData = models.BaseModel.extend({
     storageEntity: 'globalData',

     initialize: function(options) {
       this.messenger = options.messenger || this.getMessenger();
       this.hasMany({
         trackers: {collection: GlobalTrackerCollection},
         preferences: {collection: GlobalPreferenceCollection},
         globalFeatures: {collection: GlobalFeatureCollection},
         blacklist: {collection: models.BlacklistCollection}
       });
     }
   });

   models.GlobalData = GlobalData;

   return {
     GlobalData: GlobalData
   };
});


ABINE_DNTME.define('abine/models/feedback',
  ['documentcloud/underscore', 'documentcloud/backbone', 'models'],
  function(_, Backbone, models) {

    var Feedback = models.BaseModel.extend({

      initialize: function() {

      },

      getSuccessLeadMsg: function(){
        return this.attributes.leadMsg;
      },

      getSuccessMsg: function(){
        return this.attributes.successMsg;
      },

      clone: function() {
        var clone = new this.constructor(this);
        clone.user = this.user;
        return clone;
      }

    });

    models.Feedback = Feedback;

    return {
      Feedback: Feedback
    };
  });


ABINE_DNTME.define('abine/models/trackerInfo',
  ['documentcloud/underscore', 'models'],
  function(_, models) {

    var TrackerInfo = models.BaseModel.extend({
      storageEntity: 'trackerInfo',

      initialize: function(){
        this.messenger = this.getMessenger();
      }
    });
    var TrackerInfoCollection = models.BaseCollection.extend({
      model: TrackerInfo,
      fetchAllForDomains: function(domains, callback) {
        this.sync('read', this, {
          eventName: "read:trackerInfo:index",
          payload: {domains: domains},
          success: function(data) {
            this.reset(data);
            callback();
          },
          error: function() {
            LOG_ERROR && ABINE_DNTME.log.error("[ERROR HANDLER]#[abine/models/disposableEmail]#[fetchAllByDomain]# error event in model.sync eventName: read:disposableEmail:domain:domain");
          }
        });
      }
    });

    models.TrackerInfo = TrackerInfo;
    models.TrackerInfoCollection = TrackerInfoCollection;

    return {
      TrackerInfo: TrackerInfo,
      TrackerInfoCollection: TrackerInfoCollection
    };
  }
);

ABINE_DNTME.define('abine/models/advt',
  ['documentcloud/underscore', 'models'],
  function(_, models) {

    var Advt = models.BaseModel.extend({
      storageEntity: 'advt',

      initialize: function(){
        this.messenger = this.getMessenger();
      }
    });

    var AdvtCollection = models.BaseCollection.extend({
      model: Advt,
      fetchAllForQuery: function(query, url, callback) {
        this.sync('read', this, {
          eventName: "read:advt:index",
          payload: {query: query, url: url},
          success: function(data) {
            this.reset(data);
            callback();
          },
          error: function() {
            LOG_ERROR && ABINE_DNTME.log.error("[ERROR HANDLER]#[abine/models/advt]#[fetchAllByQuery]# error event in model.sync eventName: read:advt:index");
          }
        });
      }
    });

    models.Advt = Advt;
    models.AdvtCollection = AdvtCollection;

    return {
      Advt: Advt,
      AdvtCollection: AdvtCollection
    };
  }
);


















